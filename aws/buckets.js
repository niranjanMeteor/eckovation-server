var buckets = {
	PROFILE_PIC							: "eckppic",
	GROUP_PIC								: "eckgpic",
	CHAT_PIC								: "eckcpic",
	DOC                     : "eckdoc",
	AUDIO										: "eckaudio",
	VIDEO										: "eckvideo",
	PLUGIN_MEDIA						: "pluginmedia",
	FEE_RECEIPT             : "pathshalareceipts"
};

module.exports = buckets;
