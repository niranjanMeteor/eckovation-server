var FileTypes             = require('./file_types.js');
var FileModes             = require('./file_modes.js');

exports.isValidMode = function(file_type, mode){
	switch(file_type){
		case FileTypes.PDF:
			return mode == FileModes.DOC;
		break;

		case FileTypes.MP4:
			return mode == FileModes.VIDEO;
		break;

		case FileTypes.GP3:
			return mode == FileModes.AUDIO;
		break;

		case FileTypes.M4A:
			return mode == FileModes.AUDIO;
		break;

		case FileTypes.JPG:
			return mode == FileModes.IMAGE;
		break;

		case FileTypes.PNG:
			return mode == FileModes.IMAGE;
		break;

		case FileTypes.JPEG:
			return mode == FileModes.IMAGE;
		break;

		case FileTypes.PNG:
			return mode == FileModes.IMAGE;
		break;
	}
	return false;
};

exports.isValidDocFile = function(filetype, filename){
	if(filetype != FileTypes.PDF){
		return false;
	}
	try{
		var filesplit = filename.split(".");
		if(filesplit.length != 2){
			return false;
		}
		if(!filesplit[0].trim()){
			return false;
		}
		if(filesplit[1].trim() != FileTypes.PDF){
			return false;
		}
		return true;
	} catch(e){
		console.log(e);
		console.log('invalid filetype or filename arguments supplied');
		console.log('filename : '+filename+' , filetype : '+filetype);
		return false;
	}
};

exports.isValidVideoFile = function(filetype, filename){
	if(filetype != FileTypes.MP4){
		return false;
	}
	try{
		var filesplit = filename.split(".");
		if(filesplit.length != 2){
			return false;
		}
		if(!filesplit[0].trim()){
			return false;
		}
		if(filesplit[1].trim() != FileTypes.MP4){
			return false;
		}
		return true;
	} catch(e){
		console.log(e);
		console.log('invalid filetype or filename arguments supplied');
		console.log('filename : '+filename+' , filetype : '+filetype);
		return false;
	}
};

exports.isValidPpicFile = function(filetype, filename){
	if(filetype != FileTypes.JPG && filetype != FileTypes.JPEG){
		return false;
	}
	try{
		var filesplit = filename.split(".");
		if(filesplit.length != 2){
			return false;
		}
		var name = filesplit[0].trim();
		var extn = filesplit[1];
		if(!name){
			return false;
		}
		if(extn == FileTypes.JPG){
			return true;
		}
		return false;
	} catch(e){
		console.log(e);
		console.log('invalid filetype or filename arguments supplied');
		console.log('filename : '+filename+' , filetype : '+filetype);
		return false;
	}
};

exports.isValidGpicFile = function(filetype, filename){
	if(filetype != FileTypes.JPG && filetype != FileTypes.JPEG){
		return false;
	}
	try{
		var filesplit = filename.split(".");
		if(filesplit.length != 2){
			return false;
		}
		var name = filesplit[0].trim();
		var extn = filesplit[1];
		if(!name){
			return false;
		}
		if(extn == FileTypes.JPG || extn == FileTypes.JPEG){
			return true;
		}
		return false;
	} catch(e){
		console.log(e);
		console.log('invalid filetype or filename arguments supplied');
		console.log('filename : '+filename+' , filetype : '+filetype);
		return false;
	}
};

exports.isValidCpicFile = function(filetype, filename){
	if(filetype != FileTypes.JPG && filetype != FileTypes.JPEG && filetype != FileTypes.PNG){
		return false;
	}
	try{
		var filesplit = filename.split(".");
		if(filesplit.length != 2){
			return false;
		}
		var name = filesplit[0].trim();
		var extn = filesplit[1];
		if(!name){
			return false;
		}
		if(extn == FileTypes.JPG || extn == FileTypes.JPEG || extn == FileTypes.PNG){
			return true;
		}
		return false;
	} catch(e){
		console.log(e);
		console.log('invalid filetype or filename arguments supplied');
		console.log('filename : '+filename+' , filetype : '+filetype);
		return false;
	}
};
