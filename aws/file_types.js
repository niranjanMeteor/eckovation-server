var filetypes = {
	PDF                 : "pdf",
	MP4                 : "mp4",
	GP3                 : "3gp",
	M4A                 : "m4a",
	JPG                 : "jpg",
	PNG                 : "png",
	JPEG                : "jpeg",
	PNG                 : "png"
};

module.exports = filetypes;