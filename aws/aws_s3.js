var crypto 						   = require('crypto');
var AwsConfigs           = require('./aws_configs.js');
var FileSizeRange        = require('./file_size_ranges.js');
var AwsBuckets           = require('./buckets.js');

exports.getCredentials = function(bucket, mimetype, filetype){
	var filesize_range = FileSizeRange.getRange(filetype, bucket);
  var acl;
  if(bucket == AwsBuckets.CHAT_PIC){
    acl = 'authenticated-read';
  } else {
    acl = 'public-read';
  }
	var s3Policy = {
    'expiration': getExpiryTime(),
    'conditions': [
    	{'bucket': bucket},
      ['starts-with', "$key", ""],
      {'acl': acl},
      ['starts-with', '$Content-Type', mimetype],
      ['content-length-range',filesize_range.min,filesize_range.max],
      {'success_action_status' : '201'}
    ]
  };
  var stringPolicy = JSON.stringify(s3Policy);
  var base64Policy = new Buffer(stringPolicy, 'utf-8').toString('base64');
  var signature    = crypto.createHmac('sha1', AwsConfigs.AWS_SECRET_KEY)
                      .update(new Buffer(base64Policy, 'utf-8')).digest('base64');
  var s3Credentials = {
    s3Policy        : base64Policy,
    s3Signature     : signature,
    AWSAccessKeyId  : AwsConfigs.AWS_ACCESS_KEY
  };
  return s3Credentials;
};

function getExpiryTime(bucket){
  var expiryWindow = 10;
  switch(bucket){
    case AwsBuckets.CHAT_PIC:
      expiryWindow = AwsConfigs.CPIC_EXPIRY_TIME;
    break;
    case AwsBuckets.GROUP_PIC:
      expiryWindow = AwsConfigs.GPIC_EXPIRY_TIME;
    break;
    case AwsBuckets.PROFILE_PIC:
      expiryWindow = AwsConfigs.PPIC_EXPIRY_TIME;
    break;
    case AwsBuckets.DOC:
      expiryWindow = AwsConfigs.DOC_EXPIRY_TIME;
    break;
    case AwsBuckets.AUDIO:
      expiryWindow = AwsConfigs.AUDIO_EXPIRY_TIME;
    break;
    case AwsBuckets.VIDEO:
      expiryWindow = AwsConfigs.VIDEO_EXPIRY_TIME;
    break;
  }
  var _date = new Date();
  return '' + (_date.getFullYear()) + '-' + (_date.getMonth() + 1) + '-' +
          (_date.getDate()) + 'T' + (_date.getHours()) + ':' + (_date.getMinutes() + expiryWindow) + ':' +'00.000Z';
};