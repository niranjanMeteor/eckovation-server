var AwsConfigs           = require('./aws_configs.js');
var FileTypes            = require('./file_types.js');
var AwsBuckets           = require('./buckets.js');

exports.getRange = function(file_type, bucket){
	switch(file_type){
		case FileTypes.PDF:
			return {min:AwsConfigs.DOC_MIN_SIZE,max:AwsConfigs.DOC_MAX_SIZE};
		break;

		case FileTypes.MP4:
			return {min:AwsConfigs.VIDEO_MIN_SIZE,max:AwsConfigs.VIDEO_MAX_SIZE};
		break;

		case FileTypes.GP3:
			return {min:AwsConfigs.AUDIO_MIN_SIZE,max:AwsConfigs.AUDIO_MAX_SIZE};
		break;

		case FileTypes.M4A:
			return {min:AwsConfigs.AUDIO_MIN_SIZE,max:AwsConfigs.AUDIO_MAX_SIZE};
		break;

		case FileTypes.JPG:
			switch(bucket){
				case AwsBuckets.CHAT_PIC:
					return {min:AwsConfigs.CPIC_MIN_SIZE,max:AwsConfigs.CPIC_MAX_SIZE};
				break;
				case AwsBuckets.GROUP_PIC:
					return {min:AwsConfigs.GPIC_MIN_SIZE,max:AwsConfigs.GPIC_MAX_SIZE};
				break;
				case AwsBuckets.PROFILE_PIC:
					return {min:AwsConfigs.PPIC_MIN_SIZE,max:AwsConfigs.PPIC_MAX_SIZE};
				break;
			}
			return {min:1024,max:50*1024};
		break;

		case FileTypes.JPEG:
			switch(bucket){
				case AwsBuckets.CHAT_PIC:
					return {min:AwsConfigs.CPIC_MIN_SIZE,max:AwsConfigs.CPIC_MAX_SIZE};
				break;
				case AwsBuckets.GROUP_PIC:
					return {min:AwsConfigs.GPIC_MIN_SIZE,max:AwsConfigs.GPIC_MAX_SIZE};
				break;
				case AwsBuckets.PROFILE_PIC:
					return {min:AwsConfigs.PPIC_MIN_SIZE,max:AwsConfigs.PPIC_MAX_SIZE};
				break;
			}
			return {min:1024,max:50*1024};
		break;

		case FileTypes.PNG:
			switch(bucket){
				case AwsBuckets.CHAT_PIC:
					return {min:AwsConfigs.CPIC_MIN_SIZE,max:AwsConfigs.CPIC_MAX_SIZE};
				break;
				case AwsBuckets.GROUP_PIC:
					return {min:AwsConfigs.GPIC_MIN_SIZE,max:AwsConfigs.GPIC_MAX_SIZE};
				break;
				case AwsBuckets.PROFILE_PIC:
					return {min:AwsConfigs.PPIC_MIN_SIZE,max:AwsConfigs.PPIC_MAX_SIZE};
				break;
			}
			return {min:1024,max:50*1024};
		break;
	}
	return {min:1024,max:5*1000*1024};
};