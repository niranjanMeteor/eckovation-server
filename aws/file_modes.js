var filemodes = {
	IMAGE               : "image",
	AUDIO               : "audio",
	DOC                 : "doc",
	VIDEO               : "video"
};

module.exports = filemodes;