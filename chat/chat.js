var socketio            = require('socket.io');
var mongoose            = require('mongoose');
var validator           = require('validator');
var redis               = require('redis');
var _                   = require('underscore');
var moment              = require('moment');
var jwt                 = require('jsonwebtoken');
var io;

var RabbitMqPublish     = require('../rabbitmq/publisher/publisher.js');
var RabbitMqQueues      = require('../rabbitmq/queues.js');
var tokenValidator      = require('../utility/token_validator.js');
var configs             = require('../utility/configs.js');
var Events              = require('../utility/event_names.js');
var AppClients          = require('../utility/app_clients.js');
var AccountInfo         = require('../utility/account_info.js');
var errorCodes          = require('../utility/errors.js');
var RedisPrefixes       = require('../utility/redis_prefix.js');
var UserInfoCache       = require('../utility/user_info_cache.js');
var GroupInfoCache      = require('../utility/group_info_cache.js');
var log4jsLogger        = require('../loggers/log4js_module.js');
var log4jsConfigs       = require('../loggers/log4js_configs.js');
var UnsecureAccounts    = require('../utility/auth/unsecure_accounts.js');
var MailClient          = require('../utility/mail/mail_client.js');

var process_gm          = require('./process_gm.js');
var web_chat            = require('./web_chat.js');
var redisAdapter        = require('socket.io-redis');

var redis_client        = redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
    console.log("Error " + err);
});

var redis_pub           = redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_pub.auth(configs.REDIS_PASS);
var redis_sub           = redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST,{ detect_buffers: true });
redis_sub.auth(configs.REDIS_PASS);

var SOCKET_PREFIX       = "s";
var S_COUNT_PREFIX      = "seen_count";

var rmc                 = require("./../utility/redis_mongo_cache.js")(redis_client);
var rcq                 = require("./../utility/redis_chat_queue.js")(redis_client);
var rrq                 = require("./../utility/redis_rabbit_queue.js")(redis_client);
var rhmset              = require("./../utility/redis_hmset.js")(redis_client);

require('../models/profiles.js');
require('../models/accounts.js');
require('../models/messages.js');
require('../models/groupmembers.js');
require('../models/lastseen.js');
require('../models/groups.js');
require('../models/statuses.js');
require('../models/initlog.js');

var Profile             = mongoose.model('Profile'); 
var Account             = mongoose.model('Account');
var GroupMember         = mongoose.model('GroupMember');
var Group               = mongoose.model('Group');
var Message             = mongoose.model('Message');
var LastSeen            = mongoose.model('LastSeen');
var Status              = mongoose.model('Status');
var Initlog             = mongoose.model('Initlog');

var INITIALIZE          = "init"; 
var GROUP_MESSAGE       = "g_m"; 
var GROUP_MESSAGE_W     = "g_m_w"; 
var ACKNOWLEDGEMENT     = "gm_ack"; 
var CORRECTIVE_ACKNOLEDGEMENT = "gm_ackc"; 
var SEEN                = "seen"; 
var ADMIN_SEEN          = "ad_seen"; 
var ADMIN_SEEN_ACK      = "ad_seen_ack"; 
var LAST_SEEN           = "lastseen"; 
var SEEN_KEY_FIRST_FIELD= "created";
var PID_SEEN            = "1";
var PLUGIN_ACK          = "plg_ack";

var logger              = log4jsLogger.getLogger(log4jsConfigs.CATEGORY_SOCKET);
var packetLogger        = log4jsLogger.getLogger(log4jsConfigs.CATEGORY_EVENT_SOCKET_PACKET);
var secureGroupLogger   = log4jsLogger.getLogger(log4jsConfigs.CATEGORY_SECURE_GROUP);
var paidGroupLogger     = log4jsLogger.getLogger(log4jsConfigs.CATEGORY_GROUPJOIN_PAID);
var loggerSocketConn    = log4jsLogger.getLogger(log4jsConfigs.CATEGORY_SOCKET_CONN);
var redis_seen_list_len = configs.SEEN_LIST_LIMIT;
var CLIENT_AID_SOCKETS  = {};
var CLIENT_LAST_SOCKET  = {};

var LOGGER_APP_ENV      = configs.LOGGER_APP_ENV;
var MESSAGE_TYPE_FOR_SEEN_KEY = [Message.MESSAGE_TYPE.text,Message.MESSAGE_TYPE.audio,Message.MESSAGE_TYPE.video,Message.MESSAGE_TYPE.image];

module.exports.listen = function(app) {
    io = socketio(app,{
        adapter: redisAdapter({ pubClient: redis_pub, subClient: redis_sub })
    });

    io.use(function(socket, next){
        var tim             = new Date().getTime();
        var utim            = socket.handshake.query.utm;
        var account_id      = socket.handshake.query.aid;
        var machine_token   = socket.handshake.query.tokn;
        var eventWithId     = 'hk_'+account_id;
        var data            = {
            aid : account_id,
            tokn: machine_token
        };
        
        // if(utim && CLIENT_LAST_SOCKET[account_id] && CLIENT_LAST_SOCKET[account_id].utim > utim){
        //     loggerSocketConn.error({"e":eventWithId,"r": (new Date().getTime()-tim), "d":data, "er":"invalid_UTIM","s":socket.id,"utm":utim,"outm":CLIENT_LAST_SOCKET[account_id].utim});            
        //     socket.error("Invalid request");
        //     socket.disconnect();
        //     return next(new Error("Invalid request"));
        // }

        // if(!machine_token){
        //     logger.error({"e":eventWithId,"r": (new Date().getTime()-tim), "data":data, "er":"invalid_token"});
        //     socket.error("Invalid request");
        //     socket.disconnect();
        //     return next(new Error("Invalid Token"));
        // }
        if(!validator.isMongoId(account_id)){   
            logger.error({"e":eventWithId,"r": (new Date().getTime()-tim), "d":data, "er":"invalid_AID","s":socket.id,"utm":utim});            
            socket.error("Invalid request");
            socket.disconnect();
            return next(new Error("Not a Mongo ID"));       
        }
        if(machine_token == 5 || machine_token == '5' || machine_token == ''){
            UnsecureAccounts.check(account_id, null, function(err,isUnsecure){
                if(err){
                    logger.error({"e":eventWithId+'_USC',"r": (new Date().getTime()-tim), "d":data, "er":"UnsecureAccounts_err","s":socket.id,"utm":utim});
                    socket.error("Invalid request");
                    socket.disconnect();
                    return next(new Error("Invalid request"));
                }
                if(!isUnsecure){
                    logger.error({"e":eventWithId+'_USC',"r": (new Date().getTime()-tim), "d":data, "er":"UnsecureAccounts_account_not_found","s":socket.id,"utm":utim});
                    socket.error("Invalid request");
                    socket.disconnect();
                    return next(new Error("Invalid request"));
                }
                checkAcntValidityFrmCache(account_id,null,function(err,account){
                    if(err){
                        logger.error({"e":eventWithId+'_USC',"r": (new Date().getTime()-tim), "data":data, "er":err,"ep":"checkAcntValidityFrmCache_err_USC","s":socket.id,"utm":utim});
                        socket.error("Invalid request");
                        socket.disconnect();
                        return next(new Error("Invalid request"));
                    }
                    // if(utim){
                    //     if(!CLIENT_AID_SOCKETS[account_id]){
                    //         CLIENT_AID_SOCKETS[account_id] = [];
                    //         CLIENT_AID_SOCKETS[account_id].push(socket.id);
                    //     } else if(CLIENT_AID_SOCKETS[account_id].indexOf(socket.id) < 0){
                    //         CLIENT_AID_SOCKETS[account_id].push(socket.id);
                    //     }
                    //     CLIENT_LAST_SOCKET[account_id] = {
                    //         tm  : tim,
                    //         sid : socket.id,
                    //         utim: utim
                    //     };
                    // }
                    logger.info({"e":eventWithId+'_USC',"r": (new Date().getTime()-tim),"s":socket.id,"utm":utim});
                    return next();
                });
            });
        } else {
            jwt.verify(machine_token, configs.JWT_ACCESS_TOKEN_PRIVATE_KEY, function(err, decoded) { 
                if(err){
                    logger.error({"e":eventWithId+'_SC',"r": (new Date().getTime()-tim), "d":data,"er":err,"s":socket.id,"utm":utim});
                    socket.error("Expired Token");
                    socket.disconnect();
                    return next(new Error("Expired Token"));
                }
                if(decoded.id != account_id){
                    logger.error({"e":eventWithId+'_SC',"r": (new Date().getTime()-tim), "d":data,"er":"token_aid_mismatch","s":socket.id,"utm":utim});
                    socket.error("Invalid request");
                    socket.disconnect();
                    return next(new Error("Invalid request"));
                }
                checkAcntValidityFrmCache(account_id,socket.id,function(err,account){
                    if(err){
                        logger.error({"e":eventWithId+'_SC',"r": (new Date().getTime()-tim), "data":data, "er":err,"ep":"checkAcntValidityFrmCache_err_SC","s":socket.id,"utm":utim});
                        socket.error("Invalid request");
                        socket.disconnect();
                        return next(new Error("Invalid request"));
                    }
                    // if(utim){
                    //     if(!CLIENT_AID_SOCKETS[account_id]){
                    //         CLIENT_AID_SOCKETS[account_id] = [];
                    //         CLIENT_AID_SOCKETS[account_id].push(socket.id);
                    //     } else if(CLIENT_AID_SOCKETS[account_id].indexOf(socket.id) < 0){
                    //         CLIENT_AID_SOCKETS[account_id].push(socket.id);
                    //     }
                    //     CLIENT_LAST_SOCKET[account_id] = {
                    //         tm  : tim,
                    //         sid : socket.id,
                    //         utim: utim
                    //     };
                    // }
                    logger.info({"e":eventWithId+'_SC',"r": (new Date().getTime()-tim),"s":socket.id,"utm":utim});
                    return next();
                });
            });
        }
    });

    io.on('connection',function(socket){
        
        handle_initialize(socket);
        handle_acknowledgement(socket);
        handle_corrective_acknowledgement(socket);
        handle_group_message(socket);
        handle_lastseen(socket);
        handle_seen(socket);
        handle_seen_acknowledgement(socket);
        handle_plugin(socket);
        handle_disconnect(socket);

        web_chat(socket);
    });

    return io;
}

var handle_initialize = function(socket) {
    socket.on(INITIALIZE,function(data,fn) {
        var tim            = new Date().getTime();
        var utim           = data.utm;
        var account_id     = data.aid;
        var version        = typeof(data.ver) !== 'undefined' ? data.ver : '-';
        var eventWithId    = INITIALIZE+'_'+account_id;
       
        // if(utim && CLIENT_LAST_SOCKET[account_id] && CLIENT_LAST_SOCKET[account_id].utim < utim){
        //     loggerSocketConn.error({"e":eventWithId,"r": (new Date().getTime()-tim), "data":data,"s":socket.id,"utm":utim,"msg":"invalid_UTIM","outm":CLIENT_LAST_SOCKET[account_id].utim});
        //     socket.disconnect();
        //     return fn({ "success" : 0});
        // }

        var socketRooms    = socket.rooms;
        var num_of_rooms   = socketRooms.length;
        for(var i=0; i<num_of_rooms; i++){
            socket.leave(socketRooms[i]);
        }

        delete socketRooms;

        rmc.queryById("Account",account_id,function(err,obj){
            if(err){ 
                console.trace(err); 
                errorAndActivityLog(INITIALIZE, account_id, (new Date().getTime()), tim, data.cl, version, 500, 'server_error', data, socket.id);
                return fn({ "success" : 0});
            }
            if(!obj){
                logger.error({"e":eventWithId,"r": (new Date().getTime() - tim),"er":"unverified_account", "data":data});
                return fn({ "success" : 0});
            }
            AccountInfo.updateVersionAndClientSeen(account_id, data.cl, version, tim);
            UserInfoCache.getUserProfilesPartial(account_id,function(err, profiles) {
                if(err){ 
                    console.trace(err);
                    errorAndActivityLog(INITIALIZE, account_id, (new Date().getTime()), tim, data.cl, version, 500, 'server_error', data, socket.id); 
                    return fn({ "success" : 0});
                }
                var num_profiles = profiles.length;
                for(var i=0; i<num_profiles; i++) {
                    socket.join(profiles[i]["_id"]);
                }
                socket.join(account_id);
                UserInfoCache.getUserGroupMemPartial(account_id, function(err,groupmembers){
                    if(err) { 
                        console.trace(err);
                        errorAndActivityLog(INITIALIZE, account_id, (new Date().getTime()), tim, data.cl, version, 500, 'server_error', data, socket.id); 
                        return fn({ "success" : 0});
                    }
                    if(groupmembers.length > 0){
                        handleGroupRoomsJoinOrLeave(groupmembers, socket, tim);
                    }
                    var toDo = 2;
                    var done = 0;
                    publishProfileForEmitRcq(profiles, account_id, function(err,resp){
                        if(err){
                            logger.error({"e":eventWithId,"r":(new Date().getTime()-tim),"er":err});
                            // errorAndActivityLog(INITIALIZE, account_id, (new Date().getTime()), tim, data.cl, version, 500, 'server_error', data, socket.id); 
                            return fn({ "success" : 0});
                        }
                        done++;
                        if(done == toDo){
                            successAndActivityLog(INITIALIZE, account_id, (new Date().getTime()), tim, data.cl, version, socket.id, utim);
                            // if(utim){
                            //     disconnectUselessSockets(account_id, socket.id);
                            // }   
                            return fn({ "success" : 1});
                        }
                    });
                    publishAccountForEmitRcq(account_id, function(err,resp){
                        if(err){
                            logger.error({"e":eventWithId,"r":(new Date().getTime()-tim),"er":err});
                            // errorAndActivityLog(INITIALIZE, account_id, (new Date().getTime()), tim, data.cl, version, 500, 'server_error', data, socket.id); 
                            return fn({ "success" : 0});
                        }
                        done++;
                        if(done == toDo){
                            successAndActivityLog(INITIALIZE, account_id, (new Date().getTime()), tim, data.cl, version, socket.id, utim);
                            // if(utim){
                            //     disconnectUselessSockets(account_id, socket.id);
                            // }
                            return fn({ "success" : 1});
                        }
                    });
                });
            });
        });
    });
};

var handle_corrective_acknowledgement = function(socket) {
    socket.on(CORRECTIVE_ACKNOLEDGEMENT,function(data,cb){
        var tim = new Date().getTime();
        var mid = data.mid;
        var rid = data.rid;
        rcq.remove(rid,mid,function(err,res){
            if(err){
                logger.error({"e":CORRECTIVE_ACKNOLEDGEMENT+'_'+mid,"r": (new Date().getTime()-tim),"d":data,"s":socket.id,"er":err});
                return cb(null,{success : false});
            }
            if(res == 1){
                logger.info({"e":CORRECTIVE_ACKNOLEDGEMENT,"r": (new Date().getTime()-tim),"s":socket.id});
                return cb(null,{success : true});
            } else {
                logger.warn({"e":CORRECTIVE_ACKNOLEDGEMENT+'_'+mid,"r": (new Date().getTime()-tim),"s":socket.id,"d":data,"er":"key_rem_failed"}); 
                return cb(null,{success : true});
            }
        });
    });
};

var handle_acknowledgement = function(socket) {
    socket.on(ACKNOWLEDGEMENT,function(data,cb) {
        var tim1 = new Date().getTime();
        var mid = data.mid;
        var rid = data.rid;
        // When Profile related activities like profile name edit , profile pic edit or remve are being
        // acknowledged from client , they do not know that for whiich pid of their account's , they
        // have acknowledge it to , so they send aid as raid parameter isntead of rid which is generally
        // the pid for which original message was sent.
        if("raid" in data) {
            rmc.queryById("Message",mid,function(err,message){
                if(err){
                    logger.error({"e":ACKNOWLEDGEMENT,"r": (new Date().getTime()-tim1),"d":data,"s":socket.id,"er":err});  
                    return cb(err); 
                }
                if(!message){
                    MailClient.informTechTeam("ACKNOWLEDGEMENT__MID_NOT_FOUND",data);
                    logger.error({"e":ACKNOWLEDGEMENT,"r": (new Date().getTime()-tim1),"d":data,"s":socket.id,"er":"mid_not_found"});
                    return cb(null,{ success : true });
                }
                var action_type = parseInt(message.actn.t);
                if(action_type === Message.ACTION_TYPE.PROFILE_DELETE){
                    rcq.remove(data.raid,mid,function(err,resp){
                        if(err){
                            logger.error({"e":ACKNOWLEDGEMENT,"r": (new Date().getTime()-tim1),"d":data,"s":socket.id,"er":err}); 
                            return cb(err);
                        }
                        if(resp == 1){
                            logger.info({"e":ACKNOWLEDGEMENT,"r": (new Date().getTime()-tim1),"s":socket.id});
                            return cb(null,{ success : true });
                        } else {
                            logger.warn({"e":ACKNOWLEDGEMENT,"r": (new Date().getTime()-tim1),"s":socket.id,"d":data,"er":"key_from_aid_rem_failed"});
                            return cb(null,{ success : false });
                        }
                    });
                } else {
                    UserInfoCache.getUserProfilesPartial(data.raid,function(err, profiles) {
                        if(err){
                            logger.error({"e":ACKNOWLEDGEMENT,"r": (new Date().getTime()-tim1),"d":data,"s":socket.id,"er":err}); 
                            return cb(err);
                        }
                        var pids = [];
                        var num_profs = profiles.length;
                        for(var i=0; i<num_profs; i++){
                            pids.push(profiles[i]._id);
                        }
                        delete profiles;
                        rcq.removeall(pids,mid,function(err,resp) {
                            if(err){
                                logger.error({"e":ACKNOWLEDGEMENT,"r": (new Date().getTime()-tim1),"d":data,"s":socket.id,"er":err}); 
                                return cb(err);
                            }
                            if(resp > 0){
                                logger.info({"e":ACKNOWLEDGEMENT,"r": (new Date().getTime()-tim1),"s":socket.id});
                                return cb(null,{ success : true });
                            } else {
                                logger.warn({"e":ACKNOWLEDGEMENT,"r": (new Date().getTime()-tim1),"s":socket.id,"d":data,"er":"rem_pids_mid_failed"});
                                return cb(null,{ success : false });
                            }
                        });
                    });
                }
            });
        } else {
            rcq.remove(rid,mid,function(err,res){
                if(err){
                    logger.error({"e":ACKNOWLEDGEMENT,"r": (new Date().getTime()-tim1),"d":data,"s":socket.id,"er":err});  
                    return cb(err);
                }
                var time = moment()+'';
                if(res > 0){
                    Status.update({
                        pid : rid,
                        mid : mid
                    },{
                        rtim  : time
                    },{
                        safe:true,
                        multi:false,
                        upsert:true
                    },function(err,updated_status) {
                        if(err){ 
                            logger.error({"e":ACKNOWLEDGEMENT,"r": (new Date().getTime()-tim1),"d":data,"s":socket.id,"er":err}); 
                            return cb(err);
                        }
                        logger.info({"e":ACKNOWLEDGEMENT,"r": (new Date().getTime()-tim1),"s":socket.id});
                        return cb(null,{ success : true });
                    });
                } else {
                    logger.warn({"e":ACKNOWLEDGEMENT,"r": (new Date().getTime()-tim1),"s":socket.id,"d":data,"er":"rem_rid_from_mid_failed"});
                    return cb(null,{ success : true });
                }
            });
        }
    });
};

var handle_group_message = function(socket) {
    socket.on(GROUP_MESSAGE,function(data,fn) {

        var tim         = new Date().getTime();
        var eventWithId = GROUP_MESSAGE+'_'+data.mid;
        
        if(!(data.pid && data.gid)){
            logger.error({"e":eventWithId,"r": (new Date().getTime()-tim),"s":socket.id,"d":data,"er":"invalidPacket"});
            return fn(null,getErrorObject("invalid_parameters"));
        }
        if(!validator.isValidMessageId(data.mid)) { 
            logger.error({"e":eventWithId,"r": (new Date().getTime()-tim),"s":socket.id,"d":data,"er":"invalidMid"});
            return fn(null,getErrorObject("invalid_parameters")); 
        }
        if(!validator.isValidMessageType(data.type)) { 
            logger.error({"e":eventWithId,"r": (new Date().getTime()-tim),"s":socket.id,"d":data,"er":"invalidMsgTyp"});
            return fn(null,getErrorObject("invalid_parameters")); 
        }
        if(data.body.length > configs.MAX_TEXT_MESSAGE_LENGTH) {
            logger.error({"e":eventWithId,"r": (new Date().getTime()-tim),"s":socket.id,"d":data,"er":"MaxTextLen"});
            return fn(null,getErrorObject("invalid_parameters"));
        }        
        GroupInfoCache.getMongoGroup(data.gid,function(err,group){
            if(err){ 
                logger.error({"e":eventWithId,"r": (new Date().getTime()-tim),"s":socket.id,"d":data,"er":err});
                return fn(null,getErrorObject("server_error"));
            }
            if(!group){
                logger.error({"e":eventWithId,"r": (new Date().getTime()-tim),"s":socket.id,"d":data,"er":"group_not_found"});
                return fn(null,getErrorObject("server_error"));
            }
            UserInfoCache.getMongoGrpMem(data.pid, data.gid, function(err,grpMem){
                if(err){ 
                    logger.error({"e":eventWithId,"r": (new Date().getTime()-tim),"s":socket.id,"d":data,"er":err});
                    return fn(null,getErrorObject("server_error")); 
                }
                if(!grpMem){
                    logger.error({"e":eventWithId,"r": (new Date().getTime()-tim),"s":socket.id,"d":data,"er":"grpMem_not_found"});
                    return fn(null,getErrorObject("not_a_member"));
                }
                if(group.req_sub && group.req_sub ==  true){ 
                    if(!grpMem.sub_ed && !grpMem.sub_st){
                        logger.error({"e":eventWithId,"r": (new Date().getTime()-tim),"s":socket.id,"d":data,"er":"user_subscription_required"});
                        paidGroupLogger.error({"e":eventWithId,"tim": Date(),"d":data,"er":"user_subscription_required","p":{gpaid:group.req_sub,grpmem:grpMem}});
                        return fn(null,getErrorObject("user_subscription_required"));
                    }
                    if(grpMem.sub_ed < tim){
                        logger.error({"e":eventWithId,"r": (new Date().getTime()-tim),"s":socket.id,"d":data,"er":"user_subscription_ended"});
                        logger.error({"e":eventWithId,"tim": Date(),"d":data,"er":"user_subscription_ended","p":{gpaid:group.req_sub,sub_st:grpMem.sub_st,sub_ed:grpMem.sub_ed}});
                        return fn(null,getErrorObject("user_subscription_ended"));
                    }
                }
                if(grpMem.type == GroupMember.TYPE.BANNED){
                    logger.error({"e":eventWithId,"r": (new Date().getTime()-tim),"s":socket.id,"d":data,"er":"banned_mem"});
                    return fn(null,getErrorObject("groupmessage_not_allowed"));
                }
                rhmset.get(RedisPrefixes.SOCKET_ACCOUNT_TOKEN+socket.id+'','aid',function(err,socket_aid){
                    if(err){
                        logger.error({"e":eventWithId,"r": (new Date().getTime()-tim),"s":socket.id,"d":data,"er":err});
                        return fn(null,getErrorObject("server_error"));   
                    }
                    if(group.secr && (socket_aid != grpMem.aid)){
                        secureGroupLogger.error({"e":eventWithId,"msg":"secure_group_token_packet_aid_mismatch","t":tim, "d":data,"s":socket.id,"s_aid":socket_aid,"p_aid":grpMem.aid});
                        logger.error({"e":eventWithId,"r": (new Date().getTime()-tim),"s":socket.id,"d":data,"er":"sec_grp_wout_tkn"});
                        return fn(null,getErrorObject("server_error"));
                    }
                    // packetLogger.info({"event":"g_m","in_pack":data});
                    data.mt   = grpMem.type;
                    data.utim = data.tim;
                    data.tim  = tim;
                    data.gtyp = group.gtyp;
                    data.gnam = group.name;
                    if(data.tokn){
                        delete data["tokn"];
                    }
                    process_gm.mobile(data, function(err,resp){
                        if(err){ 
                            logger.error({"e":eventWithId,"r": (new Date().getTime()-tim),"s":socket.id,"d":data,"er":err});
                            return fn(null,resp) 
                        };
                        if(!resp){
                            logger.warn({"e":eventWithId,"r": (new Date().getTime()-tim),"s":socket.id,"d":data,"er":"mid_alrady_exists"});
                            return fn(null,{ success: true, tim : tim });
                        }
                        UserInfoCache.getMongoProfile(data.pid, function(err,profile){
                            if(err){ 
                                logger.warn({"e":eventWithId,"r": (new Date().getTime()-tim),"s":socket.id,"er":err}); 
                            }
                            if(profile){ 
                                data.r = profile.role;
                            } else {
                                logger.warn({"e":eventWithId,"r": (new Date().getTime()-tim),"s":socket.id,"er":"profile_not_found"});
                            }
                            // packetLogger.info({"event":"g_m","out_pack":data});
                            var room      = data.gid;
                            var room_web  = data.gid+"_w";
                            socket.to(room).emit(GROUP_MESSAGE,data);
                            socket.to(room_web).emit(GROUP_MESSAGE_W,data);
                            var done = 0;
                            var toDo = 2;
                            createSeenKeyForGM(eventWithId, parseInt(data.type), data.mid, tim, function(err,resp){
                                if(err){
                                    logger.warn({"e":eventWithId,"mid":data.mid,"mthd":"createSeenKeyForGM","err":err});
                                }
                                done++;
                                if(done == toDo){
                                    logger.info({"e":eventWithId,"r": (new Date().getTime()-tim),"s":socket.id});
                                    return fn(null,{success : true, tim : tim});
                                }
                            });
                            checkAndEnqueueMediaConversion(data.type, data.mid, data.body, tim, function(err,resp){
                                if(err){
                                    logger.warn({"e":eventWithId,"mthd":"checkAndEnqueueMediaConversion","mid":data.mid,"err":err});
                                }
                                done++;
                                if(done == toDo){
                                    logger.info({"e":eventWithId,"r": (new Date().getTime()-tim),"s":socket.id});
                                    return fn(null,{success : true, tim : tim});
                                }
                            });
                        });
                    });
                });
            });
        });
    });
};

var handle_seen = function(socket) {
    socket.on(SEEN, function(data,fn) {
        var seen_time = new Date().getTime();
        var time = moment()+'';
        var pid = data.pid;
        var mid = data.mid;
        var gid = data.gid;

        rhmset.keyexists(RedisPrefixes.SEEN+mid, pid, function(err,seen_exists){
            if(err){ 
                logger.error({"e":SEEN,"r": (new Date().getTime()-seen_time),"s":socket.id,"d":data,"er":err});
                return fn(null,{success:false}); 
            }
            if(seen_exists) { 
                logger.warn({"e":SEEN,"r": (new Date().getTime()-seen_time),"s":socket.id,"d":data,"er":"seen_exists"}); 
                return fn(null,{success:true}); 
            }
            rhmset.setSeenJob(RedisPrefixes.STATUS_UPDATE_JOB, mid+pid, mid, gid, pid, time, seen_time, function(err,seen_job){
                if(err){ 
                    logger.error({"e":SEEN,"r": (new Date().getTime()-seen_time),"s":socket.id,"d":data,"er":err});
                    return fn(null,{success:false}); 
                }
                if(!seen_job) { 
                    logger.warn({"e":SEEN,"r": (new Date().getTime()-seen_time),"s":socket.id,"d":data,"er":"setSeenJob_not_done"});
                    return fn(null,{success:false}); 
                }
                var status_obj = new Buffer(JSON.stringify({
                    gid    : gid,
                    pid    : pid,
                    mid    : mid,
                    stim   : time,
                    tim    : seen_time
                }));
                RabbitMqPublish.publish("",RabbitMqQueues.STATUS_UPDATE,status_obj);
                checkIfPidOwnsMid(mid, pid, function(err,pidOwnsMid){
                    if(err){
                        logger.error({"e":SEEN,"r": (new Date().getTime()-seen_time),"s":socket.id,"d":data,"er":err});
                        return fn(null,{success:false});
                    }
                    if(pidOwnsMid){ 
                        logger.info({"e":SEEN,"r": (new Date().getTime()-seen_time),"s":socket.id,"msg":"pidOwnsMid"});
                        return fn(null,{success:true}); 
                    }
                    rhmset.exists(RedisPrefixes.SEEN+mid, function(err,midKeyExists){
                        if(err){
                            logger.error({"e":SEEN,"r": (new Date().getTime()-seen_time),"s":socket.id,"d":data,"er":err}); 
                            return fn(null,{success:false});
                        }
                        if(!midKeyExists) {
                            logger.warn({"e":SEEN,"r": (new Date().getTime()-seen_time),"s":socket.id,"d":data,"msg":"seen_after_expiry"});
                            return fn(null,{success:true}); 
                        }
                        rhmset.set(RedisPrefixes.SEEN, mid, pid, seen_time, function(err,seenAdded){
                            if(err){
                                logger.error({"e":SEEN,"r": (new Date().getTime()-seen_time),"s":socket.id,"d":data,"er":err}); 
                                return fn(null,{success:false});
                            }
                            if(!seenAdded) {
                                logger.error({"e":SEEN,"r": (new Date().getTime()-seen_time),"s":socket.id,"d":data,"er":"hmsetaddition_failed"}); 
                                return fn(null,{success:false});
                            }
                            incrSeenCountAndSendPkt(mid, pid, socket, function(err,resp){
                                if(err){ 
                                    logger.error({"e":SEEN,"r": (new Date().getTime()-seen_time),"s":socket.id,"d":data,"er":"seen_count_incr_failed"}); 
                                    return fn(null,{success:false});
                                }
                                logger.info({"e":SEEN,"r": (new Date().getTime()-seen_time),"s":socket.id});
                                return fn(null,{success:true});
                            });
                        });
                    });
                });
            });
        });
    });
};

var handle_seen_acknowledgement = function(socket) {
    socket.on(ADMIN_SEEN_ACK,function(data,fn) {
        var tim1      = new Date().getTime();
        var admin_pid = data.apid;
        var mid       = data.mid;
        rmc.queryById("Message",mid,function(err,msg){
            if(err){ 
                logger.error({"e":ADMIN_SEEN_ACK,"r": (new Date().getTime()-tim1),"s":socket.id,"d":data,"er":err});
                return fn(null,{ success: false }); 
            }
            if(!msg){ 
                MailClient.informTechTeam("ADMIN_SEEN__MID_NOT_FOUND",data);
                logger.error({"e":ADMIN_SEEN_ACK,"r": (new Date().getTime()-tim1),"s":socket.id,"d":data,"er":"mid_not_found"});
                return fn(null,{ success: false }); 
            }
            UserInfoCache.getMongoGrpMem(admin_pid, msg.to, function(err, isAdmin){
                if(err){ 
                    logger.error({"e":ADMIN_SEEN_ACK,"r": (new Date().getTime()-tim1),"s":socket.id,"d":data,"er":err});
                    return fn(null,{ success: false }); 
                }
                if(!isAdmin){ 
                    logger.error({"e":ADMIN_SEEN_ACK,"r": (new Date().getTime()-tim1),"s":socket.id,"d":data,"er":"not_member"}); 
                    return fn(null,{ success: false }); 
                }
                if(isAdmin.type != GroupMember.TYPE.ADMIN){ 
                    logger.error({"e":ADMIN_SEEN_ACK,"r": (new Date().getTime()-tim1),"s":socket.id,"d":data,"er":"not_admin"}); 
                    return fn(null,{ success: false }); 
                }
                rhmset.removefield(RedisPrefixes.SEEN_COUNT+admin_pid, mid, function(err,remStatus){
                    if(err){ 
                        logger.error({"e":ADMIN_SEEN_ACK,"r": (new Date().getTime()-tim1),"s":socket.id,"d":data,"er":err});
                        return fn(null,{ success: false });
                    }
                    if(remStatus == 1){
                        logger.info({"e":ADMIN_SEEN_ACK,"r": (new Date().getTime()-tim1),"s":socket.id}); 
                        return fn(null,{ success: true });
                    } else {
                        logger.error({"e":ADMIN_SEEN_ACK,"r": (new Date().getTime()-tim1),"s":socket.id,"d":data,"er":"hmset_seen_count_removal_failed"});
                        return fn(null,{ success: false });
                    }
                });
            });
        });
    });
};

var handle_plugin = function(socket) {
    socket.on(PLUGIN_ACK, function(data,fn) {
        console.log("packet recieved for plugin ack");
        console.log(data);
        var tim   = new Date().getTime();
        var id    = data.id;
        var state = data.state;
        var gid   = data.gid;
        var aid   = data.aid;  
        var eventWithId = "plg_ack:"+aid;
  
        if(!id){
            logger.error({"e":eventWithId,"r": (new Date().getTime()-tim),"s":socket.id,"d":data,"er":"invalidID","id":id});
            return fn(null,{success:true});
        }  
        if(!gid){
            logger.error({"e":eventWithId,"r": (new Date().getTime()-tim),"s":socket.id,"d":data,"er":"invalidGID","gid":gid});
            return fn(null,{success:true});
        }
        if(!aid){
            logger.error({"e":eventWithId,"r": (new Date().getTime()-tim),"s":socket.id,"d":data,"er":"invalidAID","aid":aid});
            return fn(null,{success:true});
        }
        if(state == null || state == "undefined"){
            logger.error({"e":eventWithId,"r": (new Date().getTime()-tim),"s":socket.id,"d":data,"er":"invalidSTATE","state":state});
            return fn(null,{success:true});
        } 

        rhmset.get(RedisPrefixes.PLUGIN_STATE+aid, gid, function(err,plugin_state){
            if(err){ 
                logger.error({"e":eventWithId,"r": (new Date().getTime()-tim),"s":socket.id,"d":data,"er":err});
                return fn(null,{success:false}); 
            }
            if(!plugin_state) { 
                logger.warn({"e":eventWithId,"r": (new Date().getTime()-tim),"s":socket.id,"d":data,"er":"plugin_ack_already_removed"}); 
                console.log("plugin state not found");
                console.log(data);
                return fn(null,{success:true}); 
            }
            var data = JSON.parse(plugin_state);
            if(id != data.id || state != data.state){
                logger.error({"e":eventWithId,"r": (new Date().getTime()-tim),"s":socket.id,"d":data,"er":"hmset_plugin_ack_id_state_not_matching"});
                console.log("invalid packet for plugin ack");
                console.log(data);
                return fn(null,{ success: false });
            }
            rhmset.removefield(RedisPrefixes.PLUGIN_STATE+aid, gid, function(err,remStatus){
                if(err){ 
                    logger.error({"e":eventWithId,"r": (new Date().getTime()-tim),"s":socket.id,"d":data,"er":err});
                    return fn(null,{ success: false });
                }
                if(remStatus == 1){
                    logger.info({"e":eventWithId,"r": (new Date().getTime()-tim),"s":socket.id}); 
                    console.log("successfully acknowledged the data remstatus : 1");
                    console.log(data);
                    return fn(null,{ success: true });
                } else {
                    logger.error({"e":eventWithId,"r": (new Date().getTime()-tim),"s":socket.id,"d":data,"er":"hmset_plugin_ack_removal_failed"});
                    console.log("packet could not be acknowledged remstatus : 0");
                    console.log(data);
                    return fn(null,{ success: false });
                }
            });
        });
    });
};


var handle_lastseen = function(socket) {

    socket.on(LAST_SEEN,function() {
        var tim = new Date().getTime();
        return printSuccessLogger(LAST_SEEN, (new Date().getTime()), tim);
    });
};

var handle_disconnect = function(socket) {
    socket.on('disconnect',function(){
        // logger.info({"e":"discntng_e","v":io.sockets.sockets});
        socket.disconnect();
        // logger.info({"e":"discntng_f","v":io.sockets.sockets});
        rhmset.exists(RedisPrefixes.SOCKET_ACCOUNT_TOKEN+socket.id+'',function(err,resp){
            if(err){
                return logger.error({"e":"dcnt","msg":"socket_token_exists_error","err":err,"s":socket.id,"cnt":io.engine.clientsCount});
            }
            if(!resp){
                return logger.info({"e":"dcnt_t-","s":socket.id,"cnt":io.engine.clientsCount});
            }
            rhmset.removekey(RedisPrefixes.SOCKET_ACCOUNT_TOKEN+socket.id+'',function(err,resp){
                if(err){
                    return logger.error({"e":"dcnt_t+","msg":"socket_token_remove_error","err":err,"s":socket.id,"cnt":io.engine.clientsCount});
                }
                if(!resp){
                    return logger.error({"e":"dcnt_t+","msg":"socket_token_remove_failure","s":socket.id,"cnt":io.engine.clientsCount});
                }
                return logger.info({"e":"dcnt_t+","s":socket.id,"cnt":io.engine.clientsCount});
            });
        });
    });
};

var cookieParser = function(cookie) {
    var cookieObject = {};
    var cookieArray = cookie.split(';');
    for(var i =0; i<cookieArray.length; i++) {
        cookieArray[i]=cookieArray[i].trim();
        cookieBreakup = cookieArray[i].split("=");
        cookieObject[cookieBreakup[0].trim()]=cookieBreakup[1].trim();
    }
    return cookieObject;
};


validator.extend('validateToken', function (value) {
    return tokenValidator(value);
});

validator.extend('isValidMessageType', function(value) {
    if(_.values(Message.MESSAGE_TYPE).indexOf(Number(value)) == -1) return false;
    return true;
});

validator.extend('isValidMessageId', function(value) {
    if(!value) return false;
    if(typeof(value) != 'string') return false;
    if(value.length != 64) return false;
    return true;
});

function disconnectUselessSockets(aid, sid){
    var count = 0;
    if(!CLIENT_AID_SOCKETS[aid] || !CLIENT_LAST_SOCKET[aid] || (CLIENT_LAST_SOCKET[aid].sid != sid)){
        loggerSocketConn.warn({"e":'dcnt_init_'+aid,"s":sid,"d_cnt":"last_socketid_mismatch"});
        return;
    }
    var uselessSids    = CLIENT_AID_SOCKETS[aid];
    var num_sids       = uselessSids.length;
    for(var i=0; i<num_sids; i++){
        if(uselessSids[i] != sid){
            if(io.sockets.connected[uselessSids[i]]){
                io.sockets.connected[uselessSids[i]].disconnect();
                count++;
            }
        }
        CLIENT_AID_SOCKETS[aid].splice(i,1);
    }
    logger.info({"e":'init_scln_'+aid,"s":sid,"d_cnt":count});
    return;
}

function publishAccountForEmitRcq(aid, cb){
    var rcq_acnt_obj = new Buffer(JSON.stringify({
        aid  : aid
    }));
    RabbitMqPublish.publishRcq("",RabbitMqQueues.EMIT_RCQ,rcq_acnt_obj,function(err,resp){
        if(err){
            return cb(err,null);
        }
        if(resp){
            console.log('Successfully published Rabbit emit rcq from account queue for aid : '+aid);
            return cb(null,true);
        }
        console.log('Could not publish Rabbit emit rcq from account queue for aid : '+aid);
        delete rcq_acnt_obj;
        return cb("account_emit_ecq_failed_to_publish_for_aid_:"+aid,resp);
    });
}

function publishProfileForEmitRcq(profiles, aid, cb){
    var num_profs = profiles.length;
    var attempted = 0;
    var done = 0;
    var rcq_obj;
    for(var i=0; i<num_profs; i++) {
        rcq_obj = new Buffer(JSON.stringify({
            pid  : profiles[i]._id+'',
            r    : profiles[i].role,
            aid  : aid
        }));
        RabbitMqPublish.publishRcq("",RabbitMqQueues.EMIT_RCQ,rcq_obj,function(err,resp){
            if(err){
                return cb(err,null);
            }
            ++attempted;
            if(resp){
                ++done;
            }
            if(attempted == num_profs){
                console.log('Out of total '+num_profs+' profiles , rabbitmq published emit rcq for : '+done+' profiles');
                return cb(null,true);
            } 
        });
    }
    delete rcq_obj;
}

function createSeenKeyForGM(eventWithId, msg_type, mid, tim, cb){
    if(MESSAGE_TYPE_FOR_SEEN_KEY.indexOf(msg_type) < 0){
        return cb("message_type_not_in_MESSAGE_TYPE_FOR_SEEN_KEY_for_group_message_"+mid,null);
    }
    rhmset.set(RedisPrefixes.SEEN,mid,SEEN_KEY_FIRST_FIELD,tim,function(err,iskeySet){
        if(err){ 
            return cb(err,null);
        }
        if(!iskeySet){ 
            logger.warn({"e":eventWithId,"r": (new Date().getTime()-tim),"mid":mid,"wrn":"seen_key_not_created"});
            return cb("seen_key_not_created_for_group_message_"+mid,null);
        }
        return cb(null,true);
    });
}

function checkAndEnqueueMediaConversion(type, mid, body, tim, cb){
    if(type == Message.MESSAGE_TYPE.audio || type == Message.MESSAGE_TYPE.video){
        rhmset.setMediaJob(RedisPrefixes.MEDIA_CONVERSION_JOB, mid, tim, function(err,media_job){
            if(err){ 
                console.log(err);
                console.trace("media job failed"); 
                return cb(err,null); 
            }
            if(!media_job){ 
                console.trace("Media job Enque failed"); 
                console.log('mid : '+mid+' , type : '+type);
                console.log('body : '+body);
                return cb("media_job_not_set_for_mid_"+mid,null); 
            }
            var media_job = new Buffer(JSON.stringify({
                mid    : mid,
                tim    : tim,
                body   : body
            }));
            RabbitMqPublish.publish("",RabbitMqQueues.MEDIA_CONVERSION,media_job);
            delete media_job;
            return cb(null,true);
        });
    } else {
        return cb(null,true);
    }
}

function getErrorObject(errIndex) {
    return {
        code    : errorCodes[errIndex][0],
        message : errorCodes[errIndex][1],
        success : false
    };
}

function handleGroupRoomsJoinOrLeave(user_groups, socket, tim){
    var num_groups = user_groups.length;
    for(var i=0; i < num_groups; i++) {
        if(user_groups[i].gpaid && (!user_groups[i].sub_ed || user_groups[i].sub_ed < tim)){
            continue;
        }
        if(user_groups[i].type !== GroupMember.TYPE.BANNED){
            socket.join(user_groups[i].gid);
        }
    }
    return;
}

function leaveGroupRoom(socket, gid){
    return socket.leave(gid);
}

function joinGroupRoom(socket, gid) {
    return socket.join(gid);
}

function checkIfPidOwnsMid(mid, pid, cb){
    rmc.queryById("Message",mid,function(err,message){
        if(err) { 
            console.trace(err); 
            return cb(true,null); 
        }
        if(!message) { 
            console.trace('No Message entry found for the mid : '+mid);  
            return cb(true,null);
        }
        if(message.from == pid) { 
            return cb(null,true);
        }
        return cb(null,false); 
    });
}

function incrSeenCountAndSendPkt(mid, pid, socket, cb){
    rhmset.len(RedisPrefixes.SEEN+mid, function(err, totalCounts){
        if(err) { 
            console.trace(err); 
            return cb(true,false);
        }
        var countToUpdate = totalCounts;// - 1;
        rmc.queryById("Message",mid,function(err,msg){
            if(err) { 
                console.trace(err); 
                return cb(true,false);
            }
            if(!msg) { 
                console.trace('No Message entry found for mid : '+mid); 
                return cb(true,false);
            }
            GroupInfoCache.getGroupAdmins(msg.to,function(err, admins) {
                if(err) { 
                    console.trace(err); 
                    return cb(true,false); 
                }
                if(admins.length == 0) { 
                    console.trace('handle seen : No Admins found to update seen count'); 
                    return cb(true,false);
                }
                var num_admins = admins.length;
                var attempted = 0;
                for(var i=0; i<num_admins; i++){
                    setSeenCountAndEmitPkt(admins[i].pid, mid, pid, countToUpdate, socket, function(err, resp){
                        ++attempted;
                        if(err) { 
                            console.trace(err); 
                        }
                        if(attempted == num_admins){
                            return cb(null,true);
                        }
                    });
                }
            });
        });
    });
}

function setSeenCountAndEmitPkt(admin_prof_id, mid, pid, count, socket, cb){
    var admin_pid = admin_prof_id+'';
    rhmset.set(RedisPrefixes.SEEN_COUNT, admin_pid, mid, count, function(err, seenCountUpdated){
        if(err) { 
            console.trace(err); 
            return cb(true,false); 
        }
        if(!seenCountUpdated) { 
            console.trace('Seen Count could not be updated for mid : '+mid+' , admin pid : '+admin_prof_id+' , pid : '+pid); 
            return cb(true,false);
        }
        var packet = {mid : mid, pid : pid, apid : admin_pid, count : count};
        if(admin_pid == pid){
            socket.emit(ADMIN_SEEN, packet);
        } else {
            socket.to(admin_pid).emit(ADMIN_SEEN, packet);
        }
        return cb(null,true);
    });
}

function checkAcntValidityFrmCache(aid, socket_id, cb){
    rmc.queryById("Account",aid,function(err,account){
        if(err){
            return cb(err,false);
        }
        if(account == null){
            return cb("account_not_found",false);
        }
        if(account.vrfy == false){
            return cb("unverifed_account",false);
        }
        if(!socket_id){
            return cb(null,true);
        }
        var key = RedisPrefixes.SOCKET_ACCOUNT_TOKEN+socket_id+'';
        rhmset.setSocketToken(key,"aid",aid,function(err,resp){
            if(err){
                return cb("setSocketToken_redis_err",false);
            }
            if(!resp){
                return cb("socket_token_mapping_failed",false);
            }
            return cb(null,true);
        });
    });
}

function errorAndActivityLog(event_name, aid, etim, stim, cl, vrsn, ecode, emsg, data, socket_id){
    return logger.error({"e":event_name+'_'+aid,"r": (etim-stim), "data":data,"s":socket_id});
    // if(typeof(cl) === 'undefined'){
    //     cl = AppClients.ANDROID;
    // }
    // var actv_obj = new Buffer(JSON.stringify({
    //     aid   : aid,
    //     cl    : cl,
    //     vrsn  : vrsn,
    //     nm    : event_name,
    //     stts  : ecode,
    //     msg   : emsg,
    //     tim   : stim
    // }));
    // RabbitMqPublish.publish("",RabbitMqQueues.USER_ACTIVITY, actv_obj);
}

function successAndActivityLog(event_name, aid, etim, stim, cl, vrsn, socket_id, utim){
    return logger.info({"e":event_name+'_'+aid,"r": (etim-stim),"s":socket_id,"utm":utim,"vrsn":vrsn});
    // if(typeof(cl) === 'undefined'){
    //     cl = AppClients.ANDROID;
    // }
    // var actv_obj = new Buffer(JSON.stringify({
    //     aid   : aid,
    //     cl    : cl,
    //     vrsn  : vrsn,
    //     nm    : event_name,
    //     stts  : 200,
    //     msg   : 'success',
    //     tim   : stim
    // }));
    // RabbitMqPublish.publish("",RabbitMqQueues.USER_ACTIVITY, actv_obj);
}

function printErrorLogger(event_name, etim, stim, data){
    if(LOGGER_APP_ENV == 'ci' || LOGGER_APP_ENV == 'dapi'){
        return  logger.error({"e":event_name,"r": (etim-stim), "data":data});
    } else {
        return logger.error({"e":event_name,"r": (etim-stim)});
    }
}

function printSuccessLogger(event_name, etim, stim){
    return logger.info({"e":event_name,"r": (etim-stim)});
}

function printWarnLogger(event_name, etim, stim, data){
    if(LOGGER_APP_ENV == 'ci' || LOGGER_APP_ENV == 'dapi'){
        return logger.warn({"e":event_name,"r": (etim-stim), "data":data});
    } else {
        return logger.warn({"e":event_name,"r": (etim-stim)});
    }
}
