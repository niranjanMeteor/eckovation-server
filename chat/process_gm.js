var redis                       = require('redis');
var mongoose                    = require('mongoose');

var configs                     = require('../utility/configs.js');
var errorCodes                  = require('../utility/errors.js');
var SnsNotifyMsg                = require('../utility/sns_notify_msg.js');
var RabbitMqPublish             = require('../rabbitmq/publisher/publisher.js');
var RabbitMqQueues              = require('../rabbitmq/queues.js');
var RedisPrefixes               = require('../utility/redis_prefix.js');
var GroupInfoCache              = require('../utility/group_info_cache.js');
var log4jsLogger                = require('../loggers/log4js_module.js');
var log4jsConfigs               = require('../loggers/log4js_configs.js');

var redis_client                = redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
    console.log("Error " + err);
});

var rmc                         = require("./../utility/redis_mongo_cache.js")(redis_client);
var rcq                         = require("./../utility/redis_chat_queue.js")(redis_client);
var rhmset                      = require("./../utility/redis_hmset.js")(redis_client);

require('../models/messages.js');
require('../models/groupmembers.js');
require('../models/groups.js');
require('../models/statuses.js');

var Message                     = mongoose.model('Message');
var GroupMember                 = mongoose.model('GroupMember');
var Group                       = mongoose.model('Group');
var Status                      = mongoose.model('Status');

var RABBIT_GROUP_MESSAGE_QUEUE  = RabbitMqQueues.GROUP_MESSAGE;
var loggerProcessGM             = log4jsLogger.getLogger(log4jsConfigs.CATEGORY_PROCESS_GM);

function prepareMessageData(data) {
    var message = new Message({
        _id     : data.mid,
        body    : data.body,
        type    : data.type,
        from    : data.pid,
        pnam    : data.pnam,
        to      : data.gid,
        tim     : data.tim,
        utim    : data.utim,
        ttyp    : Message.MESSAGE_RECIPIENT_TYPE.GROUP,
        actn    : data.actn,
        spcf    : data.spcf,
        pi      : data.pi
    });
    return message;
}

function getErrorObject(errIndex) {
    return {
        code    : errorCodes[errIndex][0],
        message : errorCodes[errIndex][1],
        success : false
    };
}

//the main difference between process_mobile, process_web is only that,
//in process_web we queue a message for the person who posted it
//in process_mobile we queue a message for everyone except the person who sent it

exports.mobile = function(data, cb){
    if(!isUserAdminInOneWayOrGrpIsTwoWay(data.gtyp, data.type, data.mt)){
        loggerProcessGM.error({"t":"m","er":"ineligible_to_group_post","d":data});
        return cb("isUserAdminInOneWayOrGrpIsTwoWay_failed",getErrorObject("group_one_way"));
    }
    Message.findOne({
        _id : data.mid
    },function(err,existingMessage){
        if(err){ 
            loggerProcessGM.error({"t":"m","er":err,"msg":"msg_find_err","d":data}); 
            return cb(true, getErrorObject("server_error"));
        }                
        if(existingMessage){
            loggerProcessGM.error({"t":"m","er":"mid_already_exist","d":data});
            return cb(null,false);
        }
        var message = prepareMessageData(data);
        message.save(function(err,r){
            if(err){ 
                loggerProcessGM.error({"t":"m","er":"message_save_error","d":data});
                return cb(true, getErrorObject("server_error"));
            }
            var notify_msg;
            if(typeof(data.sns_msg) !== 'undefined'){
                notify_msg = data.sns_msg;
            } else {
                notify_msg = SnsNotifyMsg.prepNotifyMsg(data, data.gnam);
            }
            var gm_obj = new Buffer(JSON.stringify({
                pid    : data.pid,
                mid    : data.mid,
                gid    : data.gid,
                msg    : notify_msg,
                tim    : data.tim
            }));
            RabbitMqPublish.publishGroupMsg("",RABBIT_GROUP_MESSAGE_QUEUE,gm_obj,function(err,resp){
                if(err){
                    loggerProcessGM.error({"t":"m","er":err,"msg":"message_rabitmq_puvlish_error","d":data});
                    return cb(err,false);
                }
                if(!resp){
                    loggerProcessGM.error({"t":"m","er":"message_rabitmq_publish_failed","d":data});
                    return cb(err,false);
                }
                return cb(null,true);
            });
        });
    });
};

exports.web = function(data, cb){
    if(!isUserAdminInOneWayOrGrpIsTwoWay(data.gtyp, data.type, data.mt)){
        loggerProcessGM.error({"t":"w","er":"ineligible_to_group_post","d":data}); 
        return cb(true,getErrorObject("group_one_way"));
    }
    Message.findOne({
        _id : data.mid
    },function(err,existingMessage){
        if(err){ 
            loggerProcessGM.error({"t":"w","er":err,"msg":"msg_find_err","d":data}); 
            return cb(true, getErrorObject("server_error"));
        }                
        if(existingMessage){
            loggerProcessGM.error({"t":"w","er":"mid_already_exist","d":data});
            return cb(null,false);
        }
        var message = prepareMessageData(data);
        message.save(function(err,r){
            if(err){ 
                loggerProcessGM.error({"t":"w","er":"message_save_error","d":data}); 
                return cb(true, getErrorObject("server_error"));
            }
            var notify_msg;
            if(typeof(data.sns_msg) !== 'undefined'){
                notify_msg = data.sns_msg;
            } else {
                notify_msg = SnsNotifyMsg.prepNotifyMsg(data, data.gnam);
            }
            var gm_obj = new Buffer(JSON.stringify({
                pid    : data.pid,
                mid    : data.mid,
                gid    : data.gid,
                msg    : notify_msg,
                tim    : data.tim
            }));
            RabbitMqPublish.publishGroupMsg("",RABBIT_GROUP_MESSAGE_QUEUE,gm_obj,function(err,resp){
                if(err){
                    loggerProcessGM.error({"t":"w","er":err,"msg":"message_rabitmq_puvlish_error","d":data});
                    return cb(err,false);
                }
                if(!resp){
                    loggerProcessGM.error({"t":"w","er":"message_rabitmq_publish_failed","d":data});
                    return cb(err,false);
                }
                return cb(null,true);
            });
        });
    });
};

function isUserAdminInOneWayOrGrpIsTwoWay(gtype, msg_type, mem_type){
    if(gtype === Group.GROUP_TYPE.TWO_WAY) {
        return true;
    } else if(gtype === Group.GROUP_TYPE.ONE_WAY){
        if(mem_type === GroupMember.TYPE.BANNED){
            return false;
        } else if(msg_type === Message.MESSAGE_TYPE.notification || mem_type === GroupMember.TYPE.ADMIN){
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

module.exports.isUserAdminInOneWayOrGrpIsTwoWay = isUserAdminInOneWayOrGrpIsTwoWay;
