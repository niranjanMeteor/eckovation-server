var mongoose            = require('mongoose');
var kue                 = require('kue');
var configs             = require('./../utility/configs.js');

require('../models/initlog.js');

var Initlog             = mongoose.model('Initlog');

var init_log_queue      = kue.createQueue({
  prefix: 'q',
  redis: {
    port: configs.REDIS_PORT,
    host: configs.REDIS_HOST,
    db: configs.REDIS_OTP_QUEUE_DB,
    auth : configs.REDIS_PASS
  }
});

init_log_queue.process(configs.REDIS_INIT_LOG_QUEUE_IDEN,configs.REDIS_OTP_QUEUE_CONCURRENCY,function(job,done){
  var initlog = new Initlog(job.data);
  initlog.save(function(err,r){
    if(err) { console.trace(err); }
    done();
  });

});

init_log_queue.insertObject = function(account_id,tim,head,version) {
    init_log_queue.create(configs.REDIS_INIT_LOG_QUEUE_IDEN,{
        aid     : account_id,
        tim     : tim,
        head    : head,
        ver     : version
    }).priority("low").attempts(configs.REDIS_INIT_LOG_REATTEMPT_CNT).backoff( {delay: configs.REDIS_INIT_LOG_REATTEMPT_DELAY, type:'fixed'} ).save(function(queue_error){
      if(queue_error) {
        console.log(queue_error);
      }
    });
};

module.exports = init_log_queue;