var _                         = require('underscore');
var mongoose                  = require('mongoose');
var validator                 = require('validator');
var redis                     = require('redis');
var moment                    = require('moment');
var process_gm                = require('./process_gm.js');

var RabbitMqPublish           = require('../rabbitmq/publisher/publisher.js');
var RabbitMqQueues            = require('../rabbitmq/queues.js');
var configs                   = require('../utility/configs.js');
var AppClients                = require('../utility/app_clients.js');
var AccountInfo               = require('../utility/account_info.js');
var RedisPrefixes             = require('../utility/redis_prefix.js');
var errorCodes                = require('../utility/errors.js');
var SnsNotifyMsg              = require('../utility/sns_notify_msg.js');
var RedisPrefixes             = require('../utility/redis_prefix.js');
var UserInfoCache             = require('../utility/user_info_cache.js');
var log4jsLogger              = require('../loggers/log4js_module.js');
var log4jsConfigs             = require('../loggers/log4js_configs.js');
var GroupInfoCache           = require('../utility/group_info_cache.js');

var redis_client              = redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
    console.log("Error " + err);
});

var rhmset                     = require("./../utility/redis_hmset.js")(redis_client);
var rmc                         = require("./../utility/redis_mongo_cache.js")(redis_client);

require('../models/profiles.js');
require('../models/accounts.js');
require('../models/messages.js');
require('../models/groupmembers.js');
require('../models/lastseen.js');
require('../models/groups.js');
require('../models/statuses.js');
require('../models/initlog.js');

var Profile                    = mongoose.model('Profile'); 
var Account                    = mongoose.model('Account');
var GroupMember                = mongoose.model('GroupMember');
var Group                      = mongoose.model('Group');
var Message                    = mongoose.model('Message');
var LastSeen                   = mongoose.model('LastSeen');
var Status                     = mongoose.model('Status');
var Initlog                    = mongoose.model('Initlog');


var INITIALIZE                 = "init_w"; //{ aid : "", version : "" }
var GROUP_MESSAGE              = "g_m";
var GROUP_MESSAGE_WEB          = "g_m_w"; //{ mid: "", body: "", type: "", pid: "", pnam: "", gid: "", tim: "" }
var ACKNOWLEDGEMENT            = "gm_ack_w"; //{mid:"",rid:""}
var CORRECTIVE_ACKNOLEDGEMENT  = "gm_ackc_w"; //{mid:"",rid:""}

var SEEN                       = "seen"; //{pid:"",mid:""}
var ADMIN_SEEN                 = "ad_seen";
var ADMIN_SEEN_ACK             = "ad_seen_ack";
var LAST_SEEN                  = "lastseen";
var SEEN_KEY_FIRST_FIELD       = "created";
var PID_SEEN                   = "1";

var logger                     = log4jsLogger.getLogger(log4jsConfigs.CATEGORY_SOCKET);
var packetLogger               = log4jsLogger.getLogger(log4jsConfigs.CATEGORY_EVENT_SOCKET_PACKET);
var secureGroupLogger          = log4jsLogger.getLogger(log4jsConfigs.CATEGORY_SECURE_GROUP);
var MESSAGE_TYPE_FOR_SEEN_KEY = [Message.MESSAGE_TYPE.text,Message.MESSAGE_TYPE.audio,Message.MESSAGE_TYPE.video,Message.MESSAGE_TYPE.image];


function getErrorObject(errIndex) {
  return {
      code    : errorCodes[errIndex][0],
      message : errorCodes[errIndex][1],
      success : false
  };
}

var handle_initialize = function(event_name,socket) {
  socket.on(INITIALIZE,function(data,fn) {
    var tim            = new Date().getTime();
    var account_id     = data.aid;
    var version        = typeof(data.ver) !== 'undefined' ? data.ver : '-';
    var eventWithId    = INITIALIZE+'_'+account_id;

    var socketRooms    = socket.rooms;
    var num_of_rooms   = socketRooms.length;

    for(var i=0; i<num_of_rooms; i++)
      socket.leave(socketRooms[i]);

    delete socketRooms;

    rmc.queryById("Account",account_id,function(err,obj){
      if(err){ 
        logger.error({"e":eventWithId,"r": (new Date().getTime()-tim), "data":data, "er":"account_find_server_error","sid":socket.id}); 
        return fn({ "success" : 0});
      }
      if(!obj){
        logger.error({"e":eventWithId,"r": (new Date().getTime()-tim), "data":data, "er":"unverified_account"});
        return fn({ "success" : 0});
      }
      AccountInfo.updateVersionAndClientSeen(account_id, AppClients.WEB, version, tim);
      UserInfoCache.getUserGroupMemPartial(account_id, function(err,user_groups){
        if(err){ 
          logger.error({"e":eventWithId,"r": (new Date().getTime()-tim), "data":data, "er":"getUserGroupMemPartial_error","sid":socket.id});
          return fn({ "success" : 0});
        }
        if(user_groups.length > 0){
          handleGroupRoomsJoinOrLeave(user_groups, socket, tim);
        }
        delete user_groups;
        logger.info({"e":eventWithId,"r": (new Date().getTime()-tim),"sid":socket.id});
        return fn({ "success" : 1});
      });
    });
  });
};

var handle_group_message = function(event_name,socket) {
  socket.on(GROUP_MESSAGE_WEB,function(data,fn){

    var tim = new Date().getTime();
    var eventWithId = GROUP_MESSAGE_WEB+'_'+data.mid;
        
    if(!(data.pid && data.gid)){
      logger.error({"e":eventWithId,"r": (new Date().getTime()-tim),"s":socket.id,"d":data,"er":"invalidPacket"});
      return next(new Error("Invalid Message packet"));
    }
    if(!validator.isValidMessageId(data.mid)) { 
      logger.error({"e":eventWithId,"r": (new Date().getTime()-tim),"s":socket.id,"d":data,"er":"invalidMid"});
      return next(new Error("Not a Valid Message")); 
    }
    if(!validator.isValidMessageType(data.type)) { 
      logger.error({"e":eventWithId,"r": (new Date().getTime()-tim),"s":socket.id,"d":data,"er":"invalidMsgTyp"});
      return next(new Error("Not a Valid Message")); 
    }
    if(data.body.length > configs.MAX_TEXT_MESSAGE_LENGTH) {
      logger.error({"e":eventWithId,"r": (new Date().getTime()-tim),"s":socket.id,"d":data,"er":"MaxTextLen"});
      return fn("message exceeds maximum length");
    }
    GroupInfoCache.getMongoGroup(data.gid,function(err,group){
      if(err){ 
        logger.error({"e":eventWithId,"r": (new Date().getTime()-tim),"s":socket.id,"d":data,"er":err});
        return fn(null,getErrorObject("server_error"));
      }
      if(!group){
        logger.error({"e":eventWithId,"r": (new Date().getTime()-tim),"s":socket.id,"d":data,"er":"group_not_found"});
        return fn(null,getErrorObject("server_error"));
      }
      UserInfoCache.getMongoGrpMem(data.pid, data.gid, function(err,grpMem){
        if(err){ 
          logger.error({"e":eventWithId,"r": (new Date().getTime()-tim),"s":socket.id,"d":data,"er":err});
          return fn(null,getErrorObject("server_error")); 
        }
        if(!grpMem){
          logger.error({"e":eventWithId,"r": (new Date().getTime()-tim),"s":socket.id,"d":data,"er":"grpMem_not_found"});
          return fn(null,getErrorObject("not_a_member"));
        }
        if(grpMem.type == GroupMember.TYPE.BANNED){
          logger.error({"e":eventWithId,"r": (new Date().getTime()-tim),"s":socket.id,"d":data,"er":"banned_mem"});
          return fn(null,getErrorObject("groupmessage_not_allowed"));
        }
        rhmset.get(RedisPrefixes.SOCKET_ACCOUNT_TOKEN+socket.id+'','aid',function(err,socket_aid){
          if(err){
            logger.error({"e":eventWithId,"r": (new Date().getTime()-tim),"s":socket.id,"d":data,"er":err});
            return fn(null,getErrorObject("server_error"));   
          }
          if(group.secr && (socket_aid != grpMem.aid)){
            secureGroupLogger.error({"e":eventWithId,"msg":"secure_group_token_packet_aid_mismatch","t":tim, "d":data,"s":socket.id,"s_aid":socket_aid,"p_aid":grpMem.aid});
            logger.error({"e":eventWithId,"r": (new Date().getTime()-tim),"s":socket.id,"d":data,"er":"sec_grp_wout_tkn"});
            return fn(null,getErrorObject("server_error"));
          }
          data.mt   = grpMem.type;
          data.utim = data.tim;
          data.tim  = tim;
          data.gtyp = group.gtyp;
          data.gnam = group.name;
          if(data.tokn){
            delete data["tokn"];
          }
          process_gm.web(data, function(err,resp){
            if(err){ 
              logger.error({"e":eventWithId,"r": (new Date().getTime()-tim),"s":socket.id,"d":data,"er":err});
              return fn(null,resp) 
            };
            if(!resp){
              logger.warn({"e":eventWithId,"r": (new Date().getTime()-tim),"s":socket.id,"d":data,"er":"mid_alrady_exists"});
              return fn(null,{ success: true, tim : tim });
            }
            UserInfoCache.getMongoProfile(data.pid, function(err,profile){
              if(err){ 
                logger.warn({"e":eventWithId,"r": (new Date().getTime()-tim),"s":socket.id,"er":err}); 
              }
              if(profile){ 
                data.r = profile.role;
              } else {
                logger.warn({"e":eventWithId,"r": (new Date().getTime()-tim),"s":socket.id,"er":"profile_not_found"});
              }
              // packetLogger.info({"event":"g_m","out_pack":data});
              var room      = data.gid;
              var room_web  = data.gid+"_w";
              socket.to(room).emit(GROUP_MESSAGE,data);
              socket.to(room_web).emit(GROUP_MESSAGE_WEB,data);
              var done = 0;
              var toDo = 2;
              createSeenKeyForGM(eventWithId, parseInt(data.type), data.mid, tim, function(err,resp){
                if(err){
                  logger.warn({"e":eventWithId,"mid":mid,"mthd":"createSeenKeyForGM","err":err});
                }
                done++;
                if(done == toDo){
                  logger.info({"e":eventWithId,"r": (new Date().getTime()-tim),"s":socket.id});
                  return fn(null,{success : true, tim : tim});
                }
              });
              checkAndEnqueueMediaConversion(data.type, data.mid, data.body, tim, function(err,resp){
                if(err){
                  logger.warn({"e":eventWithId,"mthd":"checkAndEnqueueMediaConversion","mid":mid,"err":err});
                }
                done++;
                if(done == toDo){
                  logger.info({"e":eventWithId,"r": (new Date().getTime()-tim),"s":socket.id});
                  return fn(null,{success : true, tim : tim});
                }
              });
            });
          });
        });
      });
    });
  });
};

function handleGroupRoomsJoinOrLeave(user_groups, socket, tim){
  var num_groups = user_groups.length;
  for(var i=0; i < num_groups; i++) {
    if(user_groups[i].gpaid && (!user_groups[i].sub_ed || user_groups[i].sub_ed < tim)){
      continue;
    }
    if(user_groups[i].type !== GroupMember.TYPE.BANNED){
      socket.join(user_groups[i].gid+'_w');
    }
  }
  return;
}

function createSeenKeyForGM(eventWithId, msg_type, mid, tim, cb){
  if(MESSAGE_TYPE_FOR_SEEN_KEY.indexOf(msg_type) < 0){
    return cb("message_type_not_in_MESSAGE_TYPE_FOR_SEEN_KEY_for_group_message_"+mid,null);
  }
  rhmset.set(RedisPrefixes.SEEN,mid,SEEN_KEY_FIRST_FIELD,tim,function(err,iskeySet){
    if(err){ 
      return cb(err,null);
    }
    if(!iskeySet){ 
      logger.warn({"e":eventWithId,"r": (new Date().getTime()-tim),"mid":mid,"wrn":"seen_key_not_created"});
      return cb("seen_key_not_created_for_group_message_"+mid,null);
    }
    return cb(null,true);
  });
}

function checkAndEnqueueMediaConversion(type, mid, body, tim, cb){
  if(type == Message.MESSAGE_TYPE.audio || type == Message.MESSAGE_TYPE.video){
    rhmset.setMediaJob(RedisPrefixes.MEDIA_CONVERSION_JOB, mid, tim, function(err,media_job){
      if(err){ 
        console.log(err);
        console.trace("media job failed"); 
        return cb(err,null); 
      }
      if(!media_job){ 
        console.trace("Media job Enque failed"); 
        console.log('mid : '+mid+' , type : '+type);
        console.log('body : '+body);
        return cb("media_job_not_set_for_mid_"+mid,null); 
      }
      var media_job = new Buffer(JSON.stringify({
        mid    : mid,
        tim    : tim,
        body   : body
      }));
      RabbitMqPublish.publish("",RabbitMqQueues.MEDIA_CONVERSION,media_job);
      delete media_job;
      return cb(null,true);
    });
  } else {
      return cb(null,true);
  }
}

var handle_acknowledgement = function(event_name,socket) {
  socket.on(event_name,function(data,fn) {
    
  });
};

var obj = function(socket) {
  handle_initialize(INITIALIZE,socket);
  handle_group_message(GROUP_MESSAGE_WEB,socket);
  handle_acknowledgement(ACKNOWLEDGEMENT,socket);
};

module.exports = obj;