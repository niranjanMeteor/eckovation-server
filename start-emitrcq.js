require('shelljs/global');
var app 												= require('express')();
var fs 													= require('fs');
var moment											= require('moment');
var configs 										= require('./utility/configs.js');

var dirname 										= __dirname;
var server_conf_filename 				= configs.RABBIT_EMITRCQ_SERVICE_NAME+'.conf'
var server_conf_full_filename 	= dirname+'/'+server_conf_filename;
var logDir 											= configs.RABBIT_EMITRCQ_SERVICE_LOG_DIR;
var logFile 										= logDir+'/emitrcq-'+moment().format()+'.log';
var NodeStackSize               = configs.NODE_SERVER_STACK_SIZE_MAX;

rm('-rf',server_conf_full_filename);

var data = 'limit nofile 65000 65000\n\n';
    data += 'start on runlevel [2345]\n\n';
		data += 'setuid '+configs.RABBIT_EMITRCQ_SERVICE_USER+'\n\n';
		data += 'respawn\n\n';

if (app.get('env') !== 'debug') {
	data += 'pre-start script\n';
	data += ' exec /usr/bin/node '+dirname+'/rabbitmq/notify/emitrcq_restart.js 2>&1\n';
	data += 'end script\n\n';
}

data += 'exec /usr/bin/node --stack-size='+NodeStackSize+' '+dirname+'/rabbitmq/worker/emit_rcq.js >> '+logFile+' 2>&1\n\n';

if (app.get('env') !== 'debug') {
	// data += 'pre-stop script\n';
	// data += ' exec /usr/bin/node '+dirname+'/utility/notify_server/crash.js\n';
	// data += 'end script\n\n';

	data += 'post-stop script\n';
	data += ' exec /usr/bin/node '+dirname+'/rabbitmq/notify/emitrcq_crash.js 2>&1\n';
	data += 'end script\n\n';
}

fs.appendFile(server_conf_full_filename, data, function(err){
	if (err) {
		console.trace(err);
		return;
	}
	mkdir('-p', logDir);

	// TO BE THOUGHT WHETEHR PERMISSONS LESSAR THATN 777 CAN BE GIVEN
	chmod(777, logDir);

	cp('-f',server_conf_full_filename, '/etc/init/'+server_conf_filename);
	var command = 'service '+configs.RABBIT_EMITRCQ_SERVICE_NAME+' start';
	exec(command, function(status, output) {
	  console.log('Exit status:', status);
	  console.log('Program output:', output);
	});
});