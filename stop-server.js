require('shelljs/global');

var server_num = process.argv[2];
if(!server_num || isNaN(server_num)){
	console.log("Server Number required in Stop Server Command");
	process.exit(0);
}
server_num = server_num.trim();
var VALID_SERVER_NUMBERS = ['1','2','3','4','5'];
if(VALID_SERVER_NUMBERS.indexOf(server_num) < 0){
	console.log('Invalid Server Number to Stop Server');
	process.exit(0);
}

exec('service eckovation-server-'+server_num+' stop', function(status, output) {
  console.log('Exit status:', status);
  console.log('Program output:', output);
});