/*  ************************* APP BASIC PACKAGES *******************************    */
var express           = require('express');
var mongoose          = require('mongoose');
var expressValidator  = require('express-validator');
var path              = require('path');
var favicon           = require('serve-favicon');
var cookieParser      = require('cookie-parser');
var bodyParser        = require('body-parser');
var fs                = require('fs');
var morgan            = require('morgan');
var cors              = require('cors');
var compression       = require('compression')
var _                 = require('underscore');
var os                = require('os');
/*  ****************************************************************************    */





/*  ***************************** APP ROUTES  **********************************    */
var account           = require('./routes/account');
var accountdata       = require('./routes/accountdata');
var profile           = require('./routes/profile');
var group             = require('./routes/group');
var groupmember       = require('./routes/groupmember');
var message           = require('./routes/message');
var media             = require('./routes/media');
var ci                = require('./routes/ci');
var auth              = require('./routes/auth');
var groupfeed         = require('./routes/groupfeed');
var plugin            = require('./routes/plugin');
var userqueue         = require('./routes/userqueue');
var payment           = require('./routes/payment');
var transaction       = require('./routes/transaction');
var eckovation        = require('./routes/eckovation');
var clog              = require('./routes/clog');
var thirdpartyapi     = require('./routes/thirdpartyapi');
var razorpay          = require('./routes/razorpay');
var groupplugin       = require('./routes/groupplugin');
var fee               = require('./routes/fee');
/*  ****************************************************************************    */




/*  ***************************** APP MODELS  **********************************    */
require('./models/accountdata.js');
var AccountData       = mongoose.model('AccountData');
/*  ****************************************************************************    */




/*  ************************* APP BASIC UTILITIES *******************************    */
var configs           = require('./utility/configs.js');
var tokenValidator    = require('./utility/token_validator.js');
var constants         = require('./utility/constants.js');
var AppClients        = require('./utility/app_clients.js');
var MailClient        = require('./utility/mail/mail_client.js');
var MobileValidator   = require('./utility/phone_number/phone_validations.js');
/*  *****************************************************************************    */


var app               = express();



/*  *************************** APP TRUST PROXIES *******************************    */
//enable only particular ips(of nginx servers) under trust proxy for logging client ip address
app.set('trust proxy',function(ip){
  if(ip === configs.NGINX_MAIN_WEB_SERVER || ip === configs.NGINX_API_PATHSHALA_SERVER) return true;
  else return false;
});
/*  *****************************************************************************    */





/*  **************************** APP MORGAN LOGER *******************************    */
var morganLogFullFileName = getMorganLoggerFileName();
var accessLogStream       = fs.createWriteStream(morganLogFullFileName, {flags: 'a'});
app.use(morgan('{"remoteAddr":":remote-addr","remoteUser":":remote-user","date":":date","method":":method","url":":url","httpVersion":":http-version","status":":status","resp_cont_len":":res[content-length]","referrer":":referrer","user_agent":":user-agent","response_time":":response-time"}', {stream: accessLogStream}));
/*  ****************************************************************************    */





/*  **************************** APP DATABASE CONNECTION *******************************    */
var dbConnectionAttempts  = 0;
connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  var subject = 'NODE SERVER => MongoDb Connection Failure'
  var mbody = '<h4>'+ Date()  +': MongoDb Connection Failed to connect Due to Follwoing error</h4>';
  mbody += '<h5>'+err+'</h5>';
  if(dbConnectionAttempts == configs.DB_CONNECTION_RETTEMPT_LIMIT_NODE){
    MailClient.inform_db_crash(subject, mbody, function(err){
      process.exit(0);
    });
  } else {
    dbConnectionAttempts++;
    connectMongoDb();
  }
});
mongoose.connection.on('connected', function(){
  console.log('Successfully Opened MOngoDb Connection');
  dbConnectionAttempts = 0;
});
/*  ************************************************************************************    */



// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

//  uncomment after placing your favicon in /public
//  app.use(favicon(__dirname + '/public/favicon.ico'));

// create application/json parser 
app.use(bodyParser.json());
// create application/x-www-form-urlencoded parser 
app.use(bodyParser.urlencoded({ extended: false }));



/*  ******************************** APP VALIDATORS ***********************************    */
app.use(expressValidator({
  customValidators: {
    isValidCountryCode: function(c_code) {
      return MobileValidator.checkCountryCode(c_code);
    },
    validateToken : function(value) {
      return tokenValidator(value);
    },
    isValidDeviceToken : function(value) {
      if(!value) return false;
      if(typeof(value) != 'string') return false;
      if(value.length < 32) return false;
      return true;
    },
    isValidMongoId : function(value) {
      var regex = /^[0-9a-f]{24}$/;
      return regex.test(value);
    },
    isValidRole : function(value) {
      if(!value) return false;
      if(_.values(constants.ROLE).indexOf(Number(value)) == -1) return false;
      return true;
    },
    isValidEmail : function(value){
      if(!value) return false;
      var value = value.trim();
      var email_regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      return email_regex.test(value);
    },
    isValidClass : function(value) {
      if(_.values(constants.CLASSES).indexOf(Number(value)) == -1) return false;
      return true;
    },
    isValidURL : function(value) {
      return constants.VALID_URL_REGEX.test(value);
    },
    isValidGroupCode : function(value) {
      if(value) {
        value = value.toString();
        if(value.length != configs.G_CODE_LENGTH) return false;
        return true;
      } else {
        return false;
      }
    },
    isValidBooleanString : function(value) {
      if(!(value=="true"||value=="false")) return false;
      return true;
    },
    isValidMediaType : function(value) {
      var type   = value.split("/")[0].toUpperCase();
      var format = value.split("/")[1].toLowerCase();
      if(_.keys(constants.VALID_MEDIA_TYPE).indexOf(type) == -1) return false;
      if(_.property(type)(constants.VALID_MEDIA_TYPE).indexOf(format) == -1) return false;
      return true;
    },
    isValidImageSize : function(value) {
      if(Number(value) > configs.VALID_IMAGE_SIZE) return false;
      return true;
    },
    isValidClient : function(value) {
      if(!value) return false;
      return (value == AppClients.MOBILE) || (value == AppClients.WEB) || 
              (value == AppClients.ANDROID) || (value == AppClients.IOS) || (value == AppClients.WINDOWS);
    },
    isValidVersion : function(value) {
      var components = value.split(".");
      if ( components.length < 2 || components.length > 3 ) return false;
      for(var i=0; i<components.length; i++) {
        if ( isNaN(components[i]) ) return false;
        if ( parseInt(components[i]) < 0 ) return false;
      }
      return true;
    },
    isValidIndianMobileNum : function(p_no, c_code) {
      return MobileValidator.checkIndian(p_no, c_code);
    },
    isValidMobileNum : function(p_no, c_code){
      return MobileValidator.checkGeneric(p_no, c_code);
    },
    validateAccountType : function(value) {
      return (value == AccountData.TYPE.IOS) || (value == AccountData.TYPE.ANDROID) || 
                (value == AccountData.TYPE.WEB) || (value ==AccountData.TYPE.WINDOWS);
    },
    isValidTokenSecretKey : function(value){
      if(!value) return false;
      if(value.length < 24) return false;
      return true;
    },
    isValidDeviceId : function(value){
      if(!value) return false;
      if(value.length < 16) return false;
      return true;
    },
    isValidIPP : function(value){
      if(!value) return false;
      if(isNaN(value)) return false;
      if(value > configs.MESSAGE_LOAD_IPP_LIMIT) return false;
      return true;
    },
    isValidRcmdIPP : function(value){
      if(!value) return false;
      if(isNaN(value)) return false;
      if(value > configs.RECOMMENDATION_GROUPS_LENGTH_LIMIT) return false;
      return true;
    },
    isValidTokenClient : function(value) {
      if(!value) return false;
      return (value == AppClients.WEB) || (value == AppClients.ANDROID) ||
                 (value == AppClients.IOS) || (value == AppClients.WINDOWS);
    },
    validateDeviceType : function(value) {
      if(!value) return false;
      return (value == AppClients.WEB) || (value == AppClients.ANDROID) ||
                 (value == AppClients.IOS) || (value == AppClients.WINDOWS);
    },
    isValidGroupType : function(value){
      if(!value) return false;
      if(parseInt(value) == 1 || parseInt(value) == 0) return true;
      return false;
    },
    isValidPrev : function(value){
      if(!value) return false;
      if(isNaN(value)) return false;
      if(value > configs.MESSAGE_LOAD_PREV_LIMIT) return false;
      return true;
    },
    isValidAccessToken : function(value){
      if(!value) return false;
      if(typeof(value) !== 'string') return false;
      return true;
    },
    isValidRefreshToken : function(value){
      if(!value) return false;
      if(typeof(value) !== 'string') return false;
      return true;
    }
 }
}));
/*  **********************************************************************************    */



app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,content-encoding');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

// we are not using cookies for now
app.use(cookieParser());

// to serve the static files, from the server
app.use(express.static(path.join(__dirname, 'web')));

app.use(cors());

// disable etag for request caching
app.set('etag',false);

// app.use(compression());




/*  ***************************** APP ROUTE BINDING *********************************    */
app.use('/account',account);
app.use('/profile',profile);
app.use('/group',group);
app.use('/groupmember',groupmember);
app.use('/message',message);
app.use('/media',media);
app.use('/accountdata',accountdata);
app.use('/auth',auth);
app.use('/groupfeed',groupfeed);
app.use('/plugin',plugin);
app.use('/userqueue',userqueue);
app.use('/payment',payment);
app.use('/transaction',transaction);
app.use('/eckovation',eckovation);
app.use('/clog',clog);
app.use('/thirdpartyapi',thirdpartyapi);
app.use('/razorpay',razorpay);
app.use('/groupplugin',groupplugin);
app.use('/fee',fee);
if(validationsForCiApis()){
  app.use('/ci',ci);
}
/*  **********************************************************************************    */

app.get('/proxy.html', function(req,res,next){
  res.set("KeepAlive", false);
  res.status(200).send('<!DOCTYPE HTML><script src="//cdn.eckovation.com/xdomain/xdomain.min.js" data-master="http://*.pathshala.eckovation.com"></script>');
  return;
});

/*  ******************************** ERROR HANDLER ***********************************    */
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  res.status(err.status || 500).json({});
});
/*  **********************************************************************************    */


function validationsForCiApis(){
  var hostname = os.hostname();
  if(hostname == "ci" || hostname == "dapi" || hostname == "Akshats-MacBook-Pro.local" || hostname == "niranjan-pc") {
    if(process.env.NODE_ENV == "ci" ||  process.env.NODE_ENV == "dapi"){
      return true;
    }
  } else {
    return false;
  }
}

function getMorganLoggerFileName(){
  var morganLogDirectory= configs.MORGAN_LOG_DIRECTORY;
  var current_date      = new Date();
  var file_date_ext     = current_date.getFullYear()+'_'+(current_date.getMonth()+1)+'_'+current_date.getDate();
  var fullFileName      = morganLogDirectory + '/access-'+file_date_ext+'.log';
  return fullFileName;
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : configs.MONGO_SERVER_KEEPALIVE
      }
    }
  };
  if(!configs.MONGO_UNAME || configs.MONGO_UNAME == ""){
    options.server.poolSize = configs.MONGO_POOLSIZE;
    console.log('[[[[[[[    NO MONGO_UNAME FOUND IN CONFIGS, WILL CONNNECT WITHOUT REPLICA OPTIONS     ]]]]]]]]]');
    console.log('[[[[[[[    MONGODB CONNECTION POOL_SIZE : '+configs.MONGO_POOLSIZE+'     ]]]]]]]]]');
    return mongoose.connect("mongodb://"+configs.MONGO_HOST+":"+configs.MONGO_PORT+"/"+configs.DB_NAME,options);
  }
  options.replset = {
    rs_name : configs.MONGO_REPLICA_SET_NAME,
    socketOptions : {
      keepAlive : configs.MONGO_REPLICA_KEEPALIVE
    },
    poolSize : configs.MONGO_POOLSIZE
  };
  var connect_string = "mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@";
  var mongo_hosts    = configs.MONGO_HOST;
  var mongo_port     = configs.MONGO_PORT;
  for(var i=0; i<mongo_hosts.length; i++){
    if(i > 0)
      connect_string += ",";
    connect_string += mongo_hosts[i]+":"+mongo_port;
  }
  connect_string += "/"+configs.DB_NAME;
  console.log('[[[[[[[     CONNECTING MONGODB WITH REPLICA_SET OPTIONS     ]]]]]]]]]');
  console.log('[[[[[[[     MONGODB CONNECTION POOL_SIZE : '+configs.MONGO_POOLSIZE+'      ]]]]]]]]]');
  return mongoose.connect(connect_string,options);
}

module.exports = app;
