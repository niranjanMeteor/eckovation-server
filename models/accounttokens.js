var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;
 /*
 *	cat : Created At; uat : Updated At
 *	Both are declared as numbers in order to simplify comparisons
 * 	gid AND pid must be Unique
 */

/*
 *Account Tokens
 */
var accounttokens = new mongoose.Schema({
	aid 	: { type : ObjectId, required : true },			//  Account Id
	udid 	: String,			                              //  Unique Device Identifier
	clnt 	: String,			                              //  Type of Client (ANDROID,IOS,WINDOWS,WEB,etc)
	tokn  : String,                                   //   refresh token issued
	clv   : String,																		//  client version
	vrsn  : String,																		//	app version  
	skey  : String,                                   //  secret key specific per issued per device id
	act		: { type : Boolean, required : true },      // set to true is this refresh token is active in use
	revk	: { type : Boolean, required : true },      //  set to True if this refresh token is revoked
	cat 	: Date,                                     //  CreatedAt Timestamp
	uat   : Date																		  //  UpdatedAt timestamp
});

/**
hooks
**/
accounttokens.pre("save",function(next){
	var timestamp = new Date;
	if(!this.cat) this.cat = timestamp;
	if(!this.uat) this.uat = timestamp;
	next();
});

accounttokens.index({aid : 1, udid : 1, clnt : 1, act : 1, tokn : 1, revk : 1});

/**
statics
**/
accounttokens.statics = {
	CLIENT : {
	    ANDROID      : "A",
	    IOS          : "I",
	    WINDOWS      : "WN",
	    WEB					 : "W",
	    MOBILE       : "M"
	},
	cast : function(obj) {
		if(obj.hasOwnProperty("clnt")) { obj["clnt"] = parseInt(obj["clnt"]); }
		else if(obj.hasOwnProperty("act")) { obj["act"] = (obj["act"] === 'true'); }
		else if(obj.hasOwnProperty("revk")) { obj["revk"] = (obj["revk"] === 'true'); }
		return obj;
	},
}
	

mongoose.model('AccountToken',accounttokens);

