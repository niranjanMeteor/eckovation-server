var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;

var usersubscription = new mongoose.Schema(
	{
		customer_id: { type : ObjectId, required : true },
		aid 	     : { type : ObjectId, required : true },
		gid 	     : { type : ObjectId, required : true },        							
		pid 	     : { type : ObjectId, required : true },        							
		act        : { type : Boolean, required : true },     
		stim       : { type : Number, required : true },
		etim       : { type : Number, required : true },
		plugin_id  : { type : ObjectId, required : true },			   							
		identifier : { type : String, required : true },
		pkg_id 	   : { type : ObjectId, required : false },
		trxn_id    : { type : String, required : false }
	},
	{ 
		timestamps : true
	}
);

usersubscription.index({gid:1,pid:1,plugin_id:1,aid:1,pkg_id:1,act:1});
usersubscription.index({customer_id:1,act:1});
usersubscription.index({identifier:1,act:1});
usersubscription.index({pid:1,gid:1,act:1});

mongoose.model('UserSubscription',usersubscription);