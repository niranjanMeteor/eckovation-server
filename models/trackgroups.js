var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;

 /*
  *Track Groups 
  */
var trackgroups = new mongoose.Schema({
	gid		: { type : ObjectId, required : false, default : null },			//school id
	cat 	: Date
});

trackgroups.statics = {
	cast : function(obj) {
		return obj;
	}
}

trackgroups.pre("save",function(next){
	if(!this.cat) this.cat = new Date;
	next();
});

mongoose.model('TrackGroup',trackgroups);