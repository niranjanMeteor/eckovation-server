var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;

var appplugin = new mongoose.Schema(
	{
		name 					: { type : String, required : true },          // name of the plugin
		type          : { type : Number, default :1 },
		catg          : Number,
		desc 					: String,                                      // description of plugin
		thumb_url 		: String,
		showcase_url  : String,
		home_url 			: String,                                      // home url of the plugin
		private_key 	: String,                                      // encryption private key for this plugin
		act  					: { type : Boolean, required : true },
		creatby				: String,                                      // user name who registered the plugin
		aid           : ObjectId,
		pid           : ObjectId,
		acnt_dtl      : Object,
		status        : Number                                       // status of the plugin in the group
	},
	{ 
		timestamps : true
	}
);

appplugin.statics = {
	TYPE : {
    PLUGIN_PACKAGE   : 1,
    GROUPJOIN        : 2
	},
	STATUS : {
		PENDING_APPROVAL : 1,
		APPROVED         : 2,
		REJECTED         : 3,
		ACTIVE           : 4,
		INACTIVE         : 5
	},
	STATUS_VALUES : {
		PENDING_APPROVAL : "PENDING APPROVAL",
		APPROVED         : "APPROVED",
		REJECTED         : "REJECTED",
		ACTIVE           : "ACTIVE",
		INACTIVE         : "INACTIVE"
	},
	CATEGORY : {
		VIDEO     : 1,
		TEST      : 2,
		FEE       : 3
	},
	CATEGORY_NAME : {
		VIDEO     : "Video",
		TEST      : "Quiz",
		FEE       : "Fee"
	}
};

appplugin.index({act:1});
appplugin.index({status:1});
appplugin.index({catg:1});

mongoose.model('AppPlugin',appplugin,'plugins');