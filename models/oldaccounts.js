var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;
 /*
  *Old Accounts 
  */
var oldaccount = new mongoose.Schema({
	aid 	: { type : ObjectId, required : true },			//All Old Account Id's which aer considered unsecure for Application
	cat 	: Date
});

oldaccount.pre("save",function(next){
	if(!this.cat) this.cat = new Date;
	next();
});

mongoose.model('OldAccount',oldaccount);