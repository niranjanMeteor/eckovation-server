var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;

var pluginidentifier = new mongoose.Schema(
	{
		plugin_id  : { type : ObjectId, required : true },			   							// Plugin Id
		gid 	     : { type : ObjectId, required : true },        							// Group Id
		pid 	     : { type : ObjectId, required : true },        							// Profile Id
		act        : { type : Boolean, required : true },     // identifer is active in the group or not
		identifier : { type : String, required : true },
	},
	{ 
		timestamps : true
	}
);

pluginidentifier.index({plugin_id:1,gid:1,pid:1,act:1});
pluginidentifier.index({plugin_id:1,identifier:1,act:1});

mongoose.model('PluginIdentifier',pluginidentifier);