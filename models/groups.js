var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;

 /*
  *Groups 
  */
var groups = new mongoose.Schema({
	code 	       : { type : String, required : true, index:true },								//Group Code | UNIQUE
	name 	       : { type : String, required : true }, 								          //Group Name
	gpic 	   		 : { type : String, required : false },								          //Group Pic URL
	act		       : { type : Boolean, required : true },								          //true : active & false : deleted
	sid		       : { type : ObjectId, required : false, default : null },			  //school id
	clss 	       : { type : Number, required : false, default : null },				  //class
	gtyp	       : { type : Boolean, required : false, default : false },			  //type of group false=One Way AND true=Two Way
	secr         : { type : Boolean, required : false, default : false },        // True means group can be joined by latest version only
	plgn         : { type : Boolean, required : false, default : false },        // True means group has atleast one plugin active
	req_sub      : { type : Boolean, default : false },     
	plugin_id    : { type : ObjectId, required : false },                        // plugin id if group join requires subscription
	showcase_url : String,
	promo_url    : { type : Array},
	cpid         : { type : ObjectId, required : true},                          // created by PID
	caid         : { type : ObjectId, required : true},                          // created by AID
	crole        : { type : String, required : true },                           // creater ROLE
	i_gm_sms     : Boolean,
	gm_sms_api   : Object,
	i_brd        : { type : Boolean, required : false, default : false },
	cat 	       : Date,
	uat          : Date
});

groups.index({act:1,plgn:1});
groups.index({code:1,act:1});
groups.index({catg:1,act:1});

groups.statics = {
	cast : function(obj) {
		if(obj.hasOwnProperty("act")) { obj["act"] = (obj["act"] === 'true'); }
		else if(obj.hasOwnProperty("clss")) { obj["clss"] = parseInt(obj["clss"]); }
		else if(obj.hasOwnProperty("gtyp")) { obj["gtyp"] = (obj["gtyp"] === 'true'); }
		return obj;
	},
	GROUP_TYPE : {
		ONE_WAY : false,
		TWO_WAY : true
	},
}

groups.pre("save",function(next){
	if(!this.cat) this.cat = new Date;
	next();
});

mongoose.model('Group',groups);