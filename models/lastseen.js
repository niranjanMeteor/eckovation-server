var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var ObjectId  = Schema.ObjectId;

 /*
 *	cat : Created At; uat : Updated At
 *	Both are declared as numbers in order to simplify comparisons
 */

/*
 *Last Seens
 */
var lastseens = new mongoose.Schema({
	aid 	: ObjectId,			//Account ID
	seen 	: Date,			//Last Seen
	cat 	: Date,
});

lastseens.pre("save",function(next){
	if(!this.cat) this.cat = new Date;
	next();
});


var LastSeen = mongoose.model('LastSeen',lastseens);