var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;

var clientlog = new mongoose.Schema(
	{
		aid 			: { type : ObjectId, required : true },          // name of the plugin
		ut        : { type : Number, required : true },
		e         : { type : String, required : true },
		d  				: Object,
		l         : String,
		st        : Number
	},
	{ 
		timestamps : true
	}
);

clientlog.index({aid:1});
clientlog.index({aid:1,st:1});
clientlog.index({aid:1,ut:1});
clientlog.index({aid:1,e:1});
clientlog.index({ut:1});
clientlog.index({e:1});
clientlog.index({st:1});

mongoose.model('ClientLog',clientlog);