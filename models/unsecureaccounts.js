var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;

 /*
  * Unsecured Accounts 
  */
var unsecureaccount = new mongoose.Schema({
	aid : { type : ObjectId, required : true, index: { unique: true }},
	ack : { type : Boolean, required : true },		// this one time update route will be called or not
	at  : String,																	//	generated access token for the user
	rt  : String,																	// generated refresh token for the user
	skey: String,                                 // secret key for the access token
	tgtim  : Date,                              // token generation time
	cat : Date,
	uat : Date
});

unsecureaccount.pre("save",function(next){
	if(!this.cat) this.cat = new Date();
	next();
});

unsecureaccount.index({aid : 1, ack : 1});

mongoose.model('UnsecureAccount',unsecureaccount);