var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;

var contactlog = new mongoose.Schema(
	{
		aid  : { type : ObjectId, required : true },       
		n  	 : String,
		e    : String,
		p    : String,
		l    : String,
		ut   : { type : Number, required : true },
		st   : Number
	},
	{ 
		timestamps : true
	}
);

contactlog.index({aid:1});
contactlog.index({aid:1,ut:1});
contactlog.index({aid:1,st:1});
contactlog.index({aid:1,p:1,e:1});
contactlog.index({n:1});
contactlog.index({e:1});
contactlog.index({p:1});
contactlog.index({ut:1});
contactlog.index({st:1});

mongoose.model('ContactLog',contactlog);