var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;
 /*
  *Account Data 
  */
var accountdata = new mongoose.Schema({
	aid 	: ObjectId,			// Group Code
	dtyp  : { type : String, required : true }, 			// device type 
	dtkn  : { type : String, required : true },  		  // Device token
	clv   : String,																		// client version
	vrsn  : String,																		//	app version
	earn  : String,    																// end point ARN
	act   : { type : Boolean, required : true },      // act is TRUE  and FALSE
	stim  : Number,    																// last sent time
	dblc  : Number,                                   //  EndPoint Disable Count
	cat 	: Date,
	uat   : Date
});

accountdata.pre("save",function(next){
	if(!this.cat) this.cat = new Date;
	if(!this.uat) this.uat = new Date;
	next();
});

accountdata.index({earn:1});
accountdata.index({aid:1,act:1});
accountdata.index({dtyp:1,dtkn:1});
accountdata.index({dtkn:1,act:1,aid:1});
/**
statics
**/
accountdata.statics = {
	TYPE : {
	    ANDROID      : "A",
	    IOS          : "I",
	    WINDOWS      : "WN",
	    WEB          : "W"
	},
	cast : function(obj) {
		if(obj.hasOwnProperty("dtyp")) { obj["dtyp"] = parseInt(obj["dtyp"]); }
		else if(obj.hasOwnProperty("act")) { obj["act"] = (obj["act"] === 'true'); }
		else if(obj.hasOwnProperty("stim")) { obj["stim"] = parseInt(obj["stim"]); }
		else if(obj.hasOwnProperty("dblc")) { obj["dblc"] = parseInt(obj["dblc"]); }
		return obj;
	},
}

mongoose.model('AccountData',accountdata);