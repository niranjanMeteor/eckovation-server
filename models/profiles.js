var mongoose = require('mongoose');

var Schema  = mongoose.Schema;
var ObjectId  = Schema.ObjectId;
 /*
 *  cat : Created At; uat : Updated At
 *  Both are declared as numbers in order to simplify comparisons
 */
 
 /*
  *Profiles
  */
var profiles = new mongoose.Schema({
  aid      : ObjectId,              //acount id, MongoID
  sid      : {type : ObjectId, required : false, default : null},        //School id, MongoID
  nam      : {type : String, required : true},           //Person's profile name
  cnam     : {type : String, required : false, default : null},         //Child Name only in the case of PARENT PROFILE
  role     : {type : Number, required : true, default : -1},          //Role Type of person 1=Teacher; 2=parent; 3=student
  clss     : {type : Number, required : false, default : -1},       //Class of student or teacher, can be blank.
  ppic     : {type : String, required : false},   //profile pic URL
  act      : { type : Boolean, required : true },
  cat      : Date
});

profiles.pre("save",function(next){
  if(!this.cat) this.cat = new Date;
  next();
});

profiles.index({_id:1,act:1});
profiles.index({aid:1,act:1});

profiles.statics = {
  TYPE : {
    TEACHER  : 1,
    PARENT   : 2,
    STUDENT  : 3
  },
  cast : function(obj) {
    if(obj.hasOwnProperty("role")) { obj["role"] = parseInt(obj["role"]); }
    else if(obj.hasOwnProperty("clss")) { obj["clss"] = parseInt(obj["clss"]); }
    else if(obj.hasOwnProperty("act")) { obj["act"] = (obj["act"] === 'true'); }
    return obj;
  },
}

mongoose.model('Profile',profiles);