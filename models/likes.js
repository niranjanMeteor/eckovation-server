var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;

/*
 *LIKES
 */

var likes = new mongoose.Schema({
	mid 			: { type : String, required : true },
	pid 			: { type : ObjectId, required : true},
	cat 			: Date
});

likes.pre("save",function(next){
	if(!this.cat) this.cat = new Date;
	next();
});

mongoose.model('Like',likes);