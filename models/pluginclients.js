var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;

var pluginclient = new mongoose.Schema({
	aid       : {type : ObjectId, required : true},
	plugin_id : {type : ObjectId, required : true},
	client_id : {type : String, required : true},
	client_sec: {type : String, required : true},
	revk      : {type : Boolean, default : false},
	cat 	    : Date
});

pluginclient.index({client_id:1,client_sec:1,plugin_id:1});

pluginclient.pre("save",function(next){
	if(!this.cat) this.cat = new Date;
	next();
});

mongoose.model('PluginClient',pluginclient);