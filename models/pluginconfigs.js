var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;

var pluginconfig = new mongoose.Schema(
	{
		plugin_id : {type : ObjectId, required : true},
		host      : {type : String, required : true},
		port      : {type : String, required : true},
		api_path  : {type : String, required : true},
		api_key   : {type : String, required : true},
		req_exp   : {type : Number, required : false},
	},
	{
		timestamps : true
	}
);

pluginconfig.index({plugin_id:1});

mongoose.model('PluginConfig',pluginconfig);