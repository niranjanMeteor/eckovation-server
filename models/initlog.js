var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var ObjectId  = Schema.ObjectId;
 /*
 *	cat : Created At
 *	Both are declared as numbers in order to simplify comparisons
 */

/*
 *Messages
 */
var InitlogSchema = new Schema({
	aid 	: ObjectId,
	tim 	: Number,
	head 	: Object,
	ver		: String,
	cat 	: Date,
});



InitlogSchema.statics = {
	cast : function(obj) {
		if(obj.hasOwnProperty("ttyp")) { obj["ttyp"] = (obj["ttyp"] === 'true'); }
		else if(obj.hasOwnProperty("type")) { obj["type"] = parseInt(obj["type"]); }
		else if(obj.hasOwnProperty("tim")) { obj["tim"] = parseInt(obj["tim"]); }
		return obj;
	},
};

InitlogSchema.pre("save",function(next){
	if(!this.cat) this.cat = new Date;
	next();
});

var Initlog = mongoose.model('Initlog',InitlogSchema);
