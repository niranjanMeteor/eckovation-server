var mongoose = require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId  = Schema.ObjectId;
var Mixed 		= Schema.Types.Mixed;
 /*
 *	cat : Created At
 *	Both are declared as numbers in order to simplify comparisons
 */

/*
 *Messages
 */
var MessageSchema = new Schema({
	_id 			: String,
	body 			: String,	                                     //	Message Body can be 60000 characters.
	type 			: Number,
	from 			: ObjectId,	                                   //	Sender's ID
	pnam 			: String,
	to 				: ObjectId,	                                   //	Receiver's ID
	spcf  		: [ObjectId],                                  // Array of ID's of specific user 
	actn  		: {t : Number, d : Mixed},
	ttyp 			: Boolean, 	                                   //	to's type
	pi    		: Number,                                      // ppic identifier
	tim 			: Number,
	utim  		: Number,
	idel  		: {type : Boolean, default : false},           // delete flag is true , if admin deletes the message
	obody 		: String,                                      // content of body before the message was deleted
	ignr      : {type : Boolean},
	nemit     : { type: Number },
	cat 			: Date
});

MessageSchema.index({to:1,type:1,idel:1});
MessageSchema.index({to:1,type:1,ignr:1});
MessageSchema.index({_id:1,tim:1,idel:1});
MessageSchema.index({tim:1});
MessageSchema.index({idel:1});
MessageSchema.index({nemit:1});

MessageSchema.statics = {
	MESSAGE_TYPE : {
		text 		: 1,
		image 		: 2,
		audio 		: 3,
		video 		: 4,
		notification: 5,
		like		: 6,
		seen		: 7,
		admin   : 8,
		invisible : 9
	},
	MESSAGE_RECIPIENT_TYPE : {
		PERSON : true,
		GROUP  : false
	},
	ACTION_TYPE : {
		GROUP_NAME_EDIT 	: 1,
		GROUP_PIC_EDIT  	: 2,
		GROUP_PIC_REM   	: 3,
		GROUP_ADMIN_ADD 	: 4,
		GROUP_ADMIN_REM		: 5,
		GROUP_LEAVE				: 6,
		PROFILE_NAME_EDIT : 7,
		PROFILE_PIC_EDIT	: 8,
		PROFILE_PIC_REM		: 9,
		PROFILE_DELETE    : 10,
		GROUP_JOIN        : 11,
		GROUP_CREATE      : 12,
		GROUP_MEMBER_BAN  : 13,
	  GROUP_MEMBER_UNBAN: 14,
	  GROUP_MEMBER_DEL  : 15,
	  GROUP_DELETE      : 16,
	  PROFILE_CREATE    : 17,
	  GROUP_TYPE_EDIT   : 18,
	  DELETE_MESSAGE    : 19
	},
	cast : function(obj) {
		if(obj.hasOwnProperty("ttyp")) { obj["ttyp"] = (obj["ttyp"] === 'true'); }
		else if(obj.hasOwnProperty("type")) { obj["type"] = parseInt(obj["type"]); }
		else if(obj.hasOwnProperty("tim")) { obj["tim"] = parseInt(obj["tim"]); }
		return obj;
	},
};

MessageSchema.pre("save",function(next){
	if(!this.cat) this.cat = new Date;
	next();
});

var Message = mongoose.model('Message',MessageSchema);
