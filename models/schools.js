var mongoose = require('mongoose');

 /*
 *	cat : Created At; uat : Updated At
 *	Both are declared as numbers in order to simplify comparisons
 */
 
 /*
  * School list
  */
 var schools = new mongoose.Schema({
 	sn : String,			//School Name
 	type : Number,			//Type of institution 1=School; 2=College; 3=Private Institutions
 	cat : Date,
 	uat : Date 	
 });
