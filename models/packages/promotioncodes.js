var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;

var promotioncode = new mongoose.Schema(
	{
		code       : { type : String,   required : true },
		desc       : { type : String,   required : true },
		type       : { type : Number,   required : true },
		valuetype  : { type : Number,   required : true },
		value      : { type : String,   required : true },
		act        : { type : Boolean,  required : true }, 
		expirydate : { type : Number,   required : true },
		gid 	     : { type : ObjectId, required : false },
		plugin_id  : { type : ObjectId, required : false }
	},
	{ 
		timestamps : true
	}
);

promotioncode.statics = {
	TYPE : {
		GENERIC               : 1,
		GROUP_SPECIFIC        : 2,
		GROUP_PLUGIN_SPECIFIC : 3,
		PLUGIN_SPECIFIC       : 4 
	},
	VALUE_TYPE : {
		DISCOUNT_PERCENTAGE : 1,
		NET_DISCOUNT        : 2
	}
};

promotioncode.index({code:1,type:1});
promotioncode.index({code:1,act:1,type:1,gid:1,plugin_id:1});

mongoose.model('PromotionCode',promotioncode);