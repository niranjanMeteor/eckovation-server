var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;

var grouppluginpackage = new mongoose.Schema(
	{
		gid 	             : { type : ObjectId, required : true },        
		plugin_id 	       : { type : ObjectId, required : true },			   
		act                : { type : Boolean,  required : true },         
		package_name       : { type : String,   required : true },
		package_desc       : { type : String,   required : false },
		package_price      : { type : String,   required : true },
		acc                : { type : String,   required : true },    //  acc = 1 is LIMITED , acc=2 is UNLIMITED
		duration_unit      : { type : String,   required : true },
		duration_base      : { type : String,   required : true },
		package_duration   : { type : Number,   required : false },
		is_free            : { type : Boolean,  required : false },
		free_duration      : { type : Number,   required : false },
		promo_code         : { type : String,   required : false }
	},
	{ 
		timestamps : true
	}
);

grouppluginpackage.index({gid:1,plugin_id:1,act:1});
grouppluginpackage.index({promo_code:1});

mongoose.model('GroupPluginPackage',grouppluginpackage);