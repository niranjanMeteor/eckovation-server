var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var ObjectId  = Schema.ObjectId;


/*
 *STATUSES
 */

var statuses = new Schema({
	pid : { type : ObjectId, required : true },				//Profile ID
	mid : { type : String, required : true },				//Message ID
	gid : { type : ObjectId, required : true },				//Group ID
	stts: { type : Number, default : 0 },					// 1=>received by server 2=>received by user 3=>seen by user
	tim : { type : Number , required : true},
	stim: Date,                                   // user seen time
	rtim: Date,                                   // user recieved time
	// dtim: { type : Number , required : false, default : null },
	cat : Date
});

statuses.index({mid:1,pid:1,stim:1});
statuses.index({mid:1,stim:1});

statuses.statics = {
	MESSAGE_STATUS : {
		SERVER_RECEIVED 			: 1,
		USER_RECEIVED	 			: 2,
		USER_SEEN 					: 3
	},
	cast : function(obj) {
		if(obj.hasOwnProperty("stts")) { obj["stts"] = parseInt(obj["stts"]); }
		else if(obj.hasOwnProperty("tim")) { obj["tim"] = parseInt(obj["tim"]); }
		return obj;
	},
};


statuses.pre("save",function(next){
	if(!this.cat) this.cat = new Date;
	next();
});

var Status = mongoose.model('Status',statuses);