var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;

 /*
  *Push Notify Response message id 
  */
  
var pushnotify = new mongoose.Schema({
	adid 	: { type : ObjectId, required : true },			// AccountData Id
	rqid  : String,																		// request id returned in aws sns publish response
	mgid  : String,  																	// message id returned in aws sns publish response
	pyld  : String,																		// payload sent for the push notification of above request id
	cat 	: Date
});

pushnotify.pre("save",function(next){
	if(!this.cat) this.cat = new Date;
	next();
});

mongoose.model('PushNotify',pushnotify);