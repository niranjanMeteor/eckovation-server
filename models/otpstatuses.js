var mongoose = require('mongoose');

var Schema 	= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;

/*
*One Time Passwords
*/
var OtpStatusSchema = new Schema({
	oid   : ObjectId,														//	otp id 
	aid   : ObjectId,														//  account id
	engn  : String,                             // sms service provider
	stts  : String,                             // msg provider service status
	resp  : {type : Object},										// response from the sms api calls
	cat 	: Date
});

OtpStatusSchema.pre("save",function(next){
	if(!this.cat) this.cat = new Date;
	next();
});

mongoose.model('OtpStatus',OtpStatusSchema);
