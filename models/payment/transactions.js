var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;

var transaction = new mongoose.Schema(
	{
		_id                : {type : String,   required : true},
		aid                : {type : ObjectId, required : true}, 
		customer_id        : {type : ObjectId, required : true},       
		pid                : {type : ObjectId, required : true},
		gid                : {type : ObjectId, required : true},
		payment_mode       : {type : String,   required : true},
		module             : {type : Number,   required : false},
		activity           : {type : Number,   required : false},
		cl                 : {type : String,   required : false},
		plugin_id          : {type : ObjectId, required : false},
		package_id         : {type : ObjectId, required : false},
		identifier         : {type : String,   required : false},
		razorpay_id        : {type : String,   required : false},
		actual_amount      : {type : String,   required : true},
		actual_amount_p    : {type : String,   required : false},
		coupon_code        : {type : String,   required : false},
		final_amount       : {type : String,   required : true},
		final_amount_p     : {type : String,   required : false},
		status             : {type : String,   required : true},
		gateway            : {type : Number,   required : true, default : 1},
		gateway_status     : {type : String,   required : false},
		gateway_respcode   : {type : String,   required : false},
		tim 	             : {type : Number,   required : true},
		comment            : {type : String,   required : false},
		failure_reason     : {type : String,   required : false},
		rejection_reason   : {type : String,   required : false}
	},
	{
		timestamps : true
	}
);

transaction.index({customer_id:1});
transaction.index({aid:1});
transaction.index({pid:1});
transaction.index({gid:1});
transaction.index({plugin_id:1});
transaction.index({package_id:1});
transaction.index({identifier:1});
transaction.index({status:1});
transaction.index({amount:1});
transaction.index({razorpay_id:1});
transaction.index({cl:1});

transaction.statics = {
	ACTIVITY : {
		PLUGIN_PACKAGE 			: 1,
		GROUP_JOIN     			: 2
	},
	MODULE : {
		VIDEO 			: 1,
		TEST     		: 2,
		FEE         : 3
	},
	GATEWAY : {
		PAYTM    : 1,
		RAZORPAY : 2
	},
	STATUS : {
		INITIATED						: 'INITIATED',
		IN_PROGRESS         : 'IN_PROGRESS',
		SUCCESS             : 'SUCCESS',
		FAILED              : 'FAILED',
		REJECTED            : 'REJECTED',
		TXN_SUCCESS         : 'TXN_SUCCESS',
		TXN_FAILURE         : 'TXN_FAILURE',
		OPEN                : 'OPEN'
	},
	GATEWAY_STATUS : {
		NO_NEED     : 'NO_NEED',
		SUBMITTED   : 'SUBMITTED',
		PENDING     : 'PENDING',
		TXN_SUCCESS : 'TXN_SUCCESS',
		TXN_FAILURE : 'TXN_FAILURE',
		OPEN        : 'OPEN'
	},
	PAYMENT_MODE : {
		CREDIT_CARD 				: "CC",
		DEBIT_CARD  				: "DC",
		NET_BANKING 				: "NB",
		PAYTM_WALLET				: "PTMW",
		OTHER_WALLET        : "OW"
	}
};


mongoose.model('Transaction',transaction);