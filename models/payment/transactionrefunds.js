var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;

var transactionrefund = new mongoose.Schema(
	{
		trxn_id           : {type : String, required : true},
		trxn_amount       : {type : Number,      required : true},
		to_refund_amount  : {type : Number,      required : true},
		refunded_amount   : {type : Number,      required : true},
		initiated_by      : {type : ObjectId, required : true},
		initiated_on      : Date,
		closed_on         : Date,
		is_rejected       : {type : Boolean, required : false},
		rejected_on       : Date,
    rejection_reason  : String,
		tim 	            : {type : Number,      required : true},
		final_comment     : String
	},
	{
		timestamps : true
	}
);

transaction.index({trxn_id:1});
transaction.index({initiated_by:1});
transaction.index({initiated_on:1});


mongoose.model('TransactionRefund',transactionrefund);