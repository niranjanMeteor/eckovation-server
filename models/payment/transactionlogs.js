var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;

var transactionlog = new mongoose.Schema(
	{
		trxn_id           : {type : String, required : true},
		log               : {type : String,   required : true},
		gateway_log       : {type : String,   required : false},
		status            : {type : String,   required : false},
		gateway_status    : {type : String,   required : false},
		gateway_respcode  : {type : String,   required : false}
	},
	{
		timestamps : true
	}
);

transactionlog.index({trxn_id:1});
transactionlog.index({status:1});


mongoose.model('TransactionLog',transactionlog);