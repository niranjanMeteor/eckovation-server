var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;

var gatewayresponse = new mongoose.Schema(
	{
		trxn_id  : {type : String, required : true},
		resp     : {type : Object, required : true},
		module   : {type : Number, required : true}
	},
	{
		timestamps : true
	}
);

gatewayresponse.index({trxn_id:1});

gatewayresponse.statics = {
	MODULE : {
		VIDEO 			: 1,
		TEST     		: 2,
		FEE         : 3
	}
};

mongoose.model('GatewayResponse',gatewayresponse);