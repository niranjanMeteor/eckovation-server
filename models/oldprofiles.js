var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;
 /*
  *Old Profiles 
  */
var oldprofile = new mongoose.Schema({
	pid 	: { type : ObjectId, required : true },			//All Old profile Id's which are considered unsecure for Application
	cat 	: Date
});

oldprofile.pre("save",function(next){
	if(!this.cat) this.cat = new Date;
	next();
});

mongoose.model('OldProfile',oldprofile);