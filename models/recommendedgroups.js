var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;
 /*
 *	cat : Created At; uat : Updated At
 *	Both are declared as numbers in order to simplify comparisons
 */

 /*
  *Groups 
  */
var recommendedgroup = new mongoose.Schema({
	gid 	: { type : ObjectId, required : true },
	code 	: { type : String, required : true },
	cl    : String,
	catg  : Number,
	ucatg : Number,
	tim   : Number,
	cat 	: Date
});

recommendedgroup.statics = {
	CLIENT : {
    ANDROID      : "A",
    IOS          : "I",
    WINDOWS      : "WN",
    WEB					 : "W",
    MOBILE       : "M"
	},
	CATEGORY : {
    OTHERS      : 0,
    DL          : 1,
    PB          : 2,
    CH          : 3,
    PUBLIC      : 4
	},
	UPPER_CATEGORY : {
		NEW_JOINEE  : 0,
		K10         : 1,
		ELVN_TWLTH  : 2,
		BTECH       : 3,
		CAT         : 4
	},
	cast : function(obj) {
		if(obj.hasOwnProperty("act")) { obj["act"] = (obj["act"] === 'true'); }
		return obj;
	}
}

recommendedgroup.index({catg : 1});
recommendedgroup.index({cl : 1});
recommendedgroup.index({code : 1, gid : 1, cl:1});
recommendedgroup.index({tim : 1});
recommendedgroup.index({gid : 1});

recommendedgroup.pre("save",function(next){
	if(!this.cat) this.cat = new Date;
	if(!this.tim) this.tim = new Date().getTime();
	next();
});

mongoose.model('RecommendedGroup',recommendedgroup);