var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;
 /*
  *SignUp Otp Call
  */
var otpcall = new mongoose.Schema({
	aid 	: ObjectId,			                            //    Account Id
	cl    : { type : String, required : true }, 			//    cleint type 
	udid  : { type : String, required : true },  		  //    Device Id
	act   : { type : Boolean, required : true },      //    act TRUE ,FALSE
	vrsn  : String,																		//    app version
	ctim  : Number,    																//    time of last call sent in milliseconds
	atmp  : Number,                                   //    number of attempts for this account id till now
	cat 	: Date,
	uat   : Date
});

otpcall.pre("save",function(next){
	if(!this.cat) this.cat = new Date;
	if(!this.uat) this.uat = new Date;
	next();
});

otpcall.index({aid : 1,act : 1});
otpcall.index({dtyp : 1,dtkn : 1});
/**
statics
**/
otpcall.statics = {
	TYPE : {
    ANDROID      : "A",
    IOS          : "I",
    WINDOWS      : "WN",
    WEB          : "W"
	},
	cast : function(obj) {
		if(obj.hasOwnProperty("dtyp")) { obj["dtyp"] = parseInt(obj["dtyp"]); }
		else if(obj.hasOwnProperty("act")) { obj["act"] = (obj["act"] === 'true'); }
		else if(obj.hasOwnProperty("stim")) { obj["stim"] = parseInt(obj["stim"]); }
		else if(obj.hasOwnProperty("atmp")) { obj["atmp"] = parseInt(obj["atmp"]); }
		return obj;
	},
}

mongoose.model('OtpCall',otpcall);