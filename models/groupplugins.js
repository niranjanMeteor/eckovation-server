var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;

var groupplugin = new mongoose.Schema(
	{
		plgnid 	         : { type : ObjectId, required : true },			  // Plugin Id
		gid 	           : { type : ObjectId, required : true },        // Group Id
		act              : { type : Boolean, default : false },         // plugin is active in the group or not
		req_subscription : { type : Boolean, default : false },
		type             : { type : Number, default : 1 },
		promo_url        : { type : Array},
		status           : Number                                       // status of the plugin in the group
	},
	{ 
		timestamps : true
	}
);

groupplugin.statics = {
	TYPE : {
		PLUGIN_PACKAGE 			: 1,
		GROUP_JOIN     			: 2
	},
	STATUS : {
		PENDING_APPROVAL : 1,
		APPROVED         : 2,
		REJECTED         : 3,
		ACTIVE           : 4,
		INACTIVE         : 5,
		DELETED          : 6
	},
	STATUS_VALUES : {
		PENDING_APPROVAL : "PENDING APPROVAL",
		APPROVED         : "APPROVED",
		REJECTED         : "REJECTED",
		ACTIVE           : "ACTIVE",
		INACTIVE         : "INACTIVE",
		DELETED          : "DELETED"
	}
};

groupplugin.index({gid:1,act:1,plgnid:1,type:1,status:1});
groupplugin.index({gid:1,act:1,plgnid:1,status:1});
groupplugin.index({gid:1,act:1,type:1,status:1});
groupplugin.index({gid:1,act:1,status:1});
groupplugin.index({gid:1,status:1});
groupplugin.index({status:1});

mongoose.model('GroupPlugin',groupplugin);