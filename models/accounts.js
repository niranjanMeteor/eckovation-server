var mongoose = require('mongoose');

var Schema = mongoose.Schema;

/*
 * Account
 */
var AccountSchema = new Schema({
	ccod 			: {type : String, required : true},			// Country code
	m 				: {type : String, required : true},			// phone number
	e 				: {type : Array, required : false},		  // email id
	vrfy 			: {type : Boolean, required : true},		// verification boolean
	vrfy_at 	: Date,                                 // verification date time
	vrsn			: String,                               // corresponds to app version for ANDROID account
	iosv  		: String,                               // corresponds to app version for IOS account
	webv  		: String,                               // corresponds to app version for WEB account
	wndv  		: String,                               // corresponds to app version for WINDOWS account
	lsnm  		: Date,                                 // Last Seen for MOBILE or ANDROID account
	lsnw  		: Date,                                 // Last Seen for WEB accout
	lsni  		: Date,                                 // Last Seen for IOS account                               
	lsnwn 		: Date,                                 // Last Seen for WINDOWS account
	rf_link   : String,
	cat 			: Date,
},{
	toObject : {getters : true}
});

AccountSchema.index({m : 1, code :1});
AccountSchema.index({vrfy : 1});

/**
statics
**/
AccountSchema.statics = {
	VERIFICATION : {
		VERIFIED 	: true,
		NOT_VERIFIED 	: false,
	},
	cast : function(obj) {
		if(obj.hasOwnProperty("vrfy")) { obj["vrfy"] = (obj["vrfy"] === 'true'); }
		return obj;
	},
};

var Account = mongoose.model('Account',AccountSchema);


AccountSchema.virtual("cat_ms").get(function(){
	return this.cat.getTime();
});

/**
hooks
**/
AccountSchema.pre('save', function(next){
	var self = this;

	Account.findOne({
		m : this.m
	},function(err,results){
		if(err){
			next(err);
		} else if(results) {
			self.invalidate('m','mobile number must be unique');
			next(new Error('mobile number must be unique'));
		} else {
			if(!self.cat) self.cat = new Date;
			next();
		}
	});
});