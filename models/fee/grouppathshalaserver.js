var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;

var grouppathshalaserver = new mongoose.Schema(
	{
		gid            : {type : ObjectId, required : true},
		gcode          : {type : String, required : true},
		inst_name      : {type : String, required : true},
		host           : {type : String, required : true},
		port           : {type : String, required : true},
		fee_dtl_url    : {type : String, required : true},
		fee_cb_url     : {type : String, required : true},
		client_id      : {type : String, required : true},
		client_sec     : {type : String, required : true},
		act            : {type : Boolean, required : true}
	},
	{
		timestamps : true
	}
);

grouppathshalaserver.index({gid:1});
grouppathshalaserver.index({gcode:1});
grouppathshalaserver.index({gid:1,act:true});
grouppathshalaserver.index({gcode:1,act:true});

mongoose.model('GroupPathshalaServer',grouppathshalaserver);