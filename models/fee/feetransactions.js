var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;

var feetransaction = new mongoose.Schema(
	{
		aid                : {type : ObjectId, required : true}, 
		pid                : {type : ObjectId, required : true},
		gid                : {type : ObjectId, required : true},
		gcode              : {type : String,   required : true},
		plugin_id          : {type : ObjectId, required : true},
		cl                 : {type : String,   required : true},
		i_brd              : {type : Boolean,   required : true},
		order_id           : {type : String,   required : false},
		a_amount           : {type : String,   required : false},
		f_amount           : {type : String,   required : false},
		a_amount_p         : {type : String,   required : false},
		f_amount_p         : {type : String,   required : false},
		server_id          : {type : String,   required : false},
		payee_id           : {type : String,   required : false},
		payee_name         : {type : String,   required : false},
		payee_email        : {type : String,   required : false},
		p_payee            : {type : Array,    required : false},
		p_fee              : {type : Array,    required : false},
		inst_name          : {type : String,   required : false},
		inst_id            : {type : String,   required : false},
		payee_email        : {type : String,   required : false},
		month              : {type : String,   required : false},
		month_v            : {type : String,   required : false},
		year               : {type : String,   required : false},
		commission         : {type : String,   required : false},
		discount           : {type : String,   required : false},
		act                : {type : Boolean,  required : true},
		status             : {type : String,   required : true},
		tim 	             : {type : Number,   required : true},
		comment            : {type : String,   required : false},
	},
	{
		timestamps : true
	}
);

feetransaction.index({aid:1});
feetransaction.index({pid:1});
feetransaction.index({gid:1});
feetransaction.index({gcode:1});
feetransaction.index({plugin_id:1});
feetransaction.index({month:1});
feetransaction.index({year:1});
feetransaction.index({status:1});
feetransaction.index({a_amount:1});
feetransaction.index({f_amount:1});
feetransaction.index({tim:1});
feetransaction.index({cl:1});
feetransaction.index({inst_name:1});

feetransaction.statics = {
	STATUS : {
		INITIATED						: 'INITIATED',
		SUCCESS             : 'SUCCESS',
		FAILED              : 'FAILED',
		REJECTED            : 'REJECTED',
		OPEN                : 'OPEN'
	},
	INSTITUTE_STATUS : {
		BOARDED         : 1,
		NOT_BOARDED     : 2
	},
	INSTITUTE_STATUS_VALUES : {
		BOARDED         : "BOARDED",
		NOT_BOARDED     : "NOT_BOARDED"
	},
	FEE_EXTRA_CHARGE_TYPE : {
		NET_AMOUNT   : 1,
		PERCENTAGE   : 2
	}
};


mongoose.model('FeeTransaction',feetransaction);