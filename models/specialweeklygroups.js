var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;
 /*
 *	cat : Created At; uat : Updated At
 *	Both are declared as numbers in order to simplify comparisons
 */

 /*
  *Groups 
  */
var specialweeklygroups = new mongoose.Schema({
	gid 	: { type : ObjectId, required : true },
	code 	: { type : String, required : true },	
	recv	: {type : Array, required : true},
	catg  : Number,
	cat 	: Date
});

specialweeklygroups.index({gid : 1});

specialweeklygroups.statics = {
	cast : function(obj) {
		if(obj.hasOwnProperty("act")) { obj["act"] = (obj["act"] === 'true'); }
		return obj;
	}
}

specialweeklygroups.pre("save",function(next){
	if(!this.cat) this.cat = new Date;
	next();
});

mongoose.model('SpecialWeeklyGroup',specialweeklygroups);