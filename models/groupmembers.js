var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;

/*
 *Group Members
 */
var groupmembers = new mongoose.Schema({
	gid 	    : { type : ObjectId, required : true },			            // Group Id
	pid 	    : { type : ObjectId, required : true },			            // Profile ID
	aid 	    : { type : ObjectId, required : true },			            // Account ID
	type 	    : { type : Number, required : true },			              // Type of member 1=Admin; 2=Member; 3=Banned
	act		    : { type : Boolean, required : true },
	gdel      : { type : Boolean, required : true, default : false},  // gdel true implies the corresponding group is deleted
	rcmd      : { type : Boolean, default : false },
	gpaid     : { type : Number, required : false },
	sub_st    : { type : Number, default : 0 },                        // Subscription Start time if Group is Paid
	sub_ed    : { type : Number, default : 0 },                        // Subscription End Time if Group is Paid
	ordr_id   : { type : String, required : false },	
	plugin_id : { type : ObjectId, required : false },
	pamt      : { type : Number, required : false },
	muttm     : { type : Number, default : 0 },                        // mute time duration
	muttp     : String,                                               // mute type
	pnam      : String,
	cat 	    : Date
});

groupmembers.index({gid : 1,pid : 1,act : 1,gdel : 1});
groupmembers.index({gid : 1,aid : 1,act : 1,gdel : 1});
groupmembers.index({aid : 1,act : 1,gdel : 1});
groupmembers.index({gid : 1,act : 1,gdel : 1,type : 1});
groupmembers.index({gid : 1,act : 1,gdel : 1,type : 1,muttm : 1});
groupmembers.index({pid : 1,act : 1,gdel : 1});
groupmembers.index({gid : 1,act : 1});
groupmembers.index({pnam : 1});
groupmembers.index({cat : 1});
/**
hooks
**/
groupmembers.pre("save",function(next){
	if(!this.cat) this.cat = new Date;
	next();
});

/**
statics
**/
groupmembers.statics = {
	TYPE : {
    ADMIN           : 1,
    MEMBER          : 2,
    BANNED          : 3
	},
	MUTE_TYPE : {
		SHORT  : "S",
		MEDIUM : "M",
		LONG   : "L"
	},
	MUTE_TIMING : {
		"S" : 8*3600*1000,
		"M" : 7*24*3600*1000,
		"L" : 30*24*3600*1000
	},
	cast : function(obj) {
		if(obj.hasOwnProperty("type")) { obj["type"] = parseInt(obj["type"]); }
		else if(obj.hasOwnProperty("act")) { obj["act"] = (obj["act"] === 'true'); }
		else if(obj.hasOwnProperty("gdel")) { obj["gdel"] = (obj["gdel"] === 'true'); }
		return obj;
	},
}
	

mongoose.model('GroupMember',groupmembers);

