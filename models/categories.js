var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;

var category = new mongoose.Schema(
	{
		name 					: { type : String, required : true },          // name of the category
		desc 					: String,                                      // description of category
		num           : Number,                                      // unique category number
		act  					: { type : Boolean, required : true },
		creatby				: String                                    // user name who registered the plugin
	},
	{ 
		timestamps : true
	}
);

category.index({num:1});
category.index({name:1,act:1});
category.index({act:1});

category.statics = {
	MAJOR_TYPE : {
    NEW_JOINEE      : 1
	}
};

mongoose.model('Category',category);