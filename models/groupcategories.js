var mongoose 	= require('mongoose');

var Schema 		= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;

var groupcategory = new mongoose.Schema(
	{
		gid 	        : { type : ObjectId, required : true },			   // Group Id
		catgid 	      : { type : ObjectId, required : true },			   // Category Id
		catgnm        : { type : Number, required : true },
		desc 					: String,                                      // description
		act  					: { type : Boolean, required : true },         // relation is active or not
		creatby				: String                                       // user name who registered the plugin
	},
	{ 
		timestamps : true
	}
);

groupcategory.index({gid:1,act:1});
groupcategory.index({catgid:1,act:1});
groupcategory.index({catgid:1,gid:1,act:1});
groupcategory.index({gid:1,catgid:1,act:1});

mongoose.model('GroupCategory',groupcategory);