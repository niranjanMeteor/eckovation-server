var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var ObjectId  = Schema.ObjectId;

/*
 *Account Last Seens
 */
var useractivity = new mongoose.Schema({
	aid 	: ObjectId,			                        //  Account ID
	cl    : String,                               //  Type of Client (ANDROID,IOS,WINDOWS,WEB,etc) 
	vrsn	: String,                               //  Version fo the app user did the activity
	enam  : String,                               //  name of the activity user was performing
	avtd  : {type : Object},            //  data related to the user activity
	stts  : Number,
	emsg  : String,                               //  Status string
	tim   : Number,
	cat   : Date
});

var UserActivity = mongoose.model('UserActivity',useractivity);