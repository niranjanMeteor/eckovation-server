var mongoose = require('mongoose');

var Schema 	= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;

/*
*otp call statuses
*/
var OtpCallStatus = new Schema({
	oclid : ObjectId,                         //  otp call id
	otpid : ObjectId,									        //	otp id 
	aid   : ObjectId,                         // account id
	engn  : String,                           // call service provider,
	resp  : {type : Object},                  // caller service repsonse
	cat 	: Date,
	uat   : Date
});

OtpCallStatus.pre("save",function(next){
	if(!this.cat) this.cat = new Date;
	if(!this.uat) this.uat = new Date;
	next();
});

mongoose.model('OtpCallStatus',OtpCallStatus);