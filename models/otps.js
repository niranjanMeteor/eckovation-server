var mongoose = require('mongoose');

var Schema 	= mongoose.Schema;
var ObjectId 	= Schema.ObjectId;

/*
*One Time Passwords
*/
var OtpSchema = new Schema({
	code 	: {type : String, required : true},	//OTP 
	gnat 	: Number,				//Generated at
	smst 	: {type : Number, required : true}, 		//sms status//1.queued 2.delivered 3.failed
	cdst 	: {type : Number, required : true},			//code status//1.notused, 2. invalid, 3. used
	aid 	: ObjectId,			//Account ID
	cat 	: Date

});

OtpSchema.pre("save",function(next){
	if(!this.cat) this.cat = new Date;
	next();
});

OtpSchema.index({aid : 1, cdst : 1, code : 1});

OtpSchema.statics = {
	SMS_STATUS : {
		NOT_SENT	: 1,
		QUEUED 		: 2,
		FAILED 		: 3,
		DELIVERED 	: 4,
	},
	STATUS : {
		NOTUSED 	: 1,
		INVALID		: 2,
		USED 		: 3
	},
	cast : function(obj) {
		if(obj.hasOwnProperty("smst")) { obj["smst"] = parseInt(obj["smst"]); }
		else if(obj.hasOwnProperty("cdst")) { obj["cdst"] = parseInt(obj["cdst"]); }
		else if(obj.hasOwnProperty("gnat")) { obj["gnat"] = parseInt(obj["gnat"]); }
		return obj;
	},
};


mongoose.model('Otp',OtpSchema);
