var mongoose            = require('mongoose');
var configs             = require('./../utility/configs.js');
var readline 			      = require('readline');
var moment				      = require('moment-timezone');

require('../models/groupmembers.js');
require('../models/groups.js');
require('../models/profiles.js');
require('../models/accounts.js');
require('../models/messages.js');
require('../models/statuses.js');

var Group 				      = mongoose.model('Group');
var GroupMember 		    = mongoose.model('GroupMember');
var Profile 				    = mongoose.model('Profile');
var Account 				    = mongoose.model('Account');
var Message 				    = mongoose.model('Message');
var Status 				      = mongoose.model('Status');

var rL = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

var remaining = 0;
var replicated = 0;
var skip_value = 0;

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});
mongoose.connection.on('connected', function(){
  console.log('Successfully Opened MOngoDb Connection');
  console.log('------------------------------------MONTHLY DATA REPORT STARTED------------------------------------')
	startMonthlyDataReport();
});

function startMonthlyDataReport(){
	rL.question('Enter the Start Date (YYYY-MM-DD) : ', function(start){
		if(isValidDateFormat(start)){
			rL.question('Enter the End Date (YYYY-MM-DD) : ', function(end){
				if(isValidDateFormat(end)){
					var datetime = getStartAndEndDateString(start,end);
					console.log('\n');
					console.log('    START DATETIME  ==   '+datetime.start);
					console.log('    END DATETIME    ==   '+datetime.end);
					console.log('\n');
					generateData(datetime);
				} else {
					console.log('Oops , you entered a wrong option , Please enter a right option');
					return startMonthlyDataReport();
				}
			});
		} else {
			console.log('Oops , you entered a wrong option , Please enter a right option');
			return startMonthlyDataReport();
		}
	});
}

function generateData(datetime){
	var toBeDoneItems = 7;
	var done          = 0;
	var data = {};
	getNewAccounts(datetime,function(err,new_accounts){
		if(err){
			console.trace(err);
			endScript();
		}
		data.new_accounts = new_accounts;
		done++;
		if(done == toBeDoneItems){
			console.log(data);
			endScript();
		}
	});
	getNewProfiles(datetime,function(err,new_profiles){
		if(err){
			console.trace(err);
			endScript();
		}
		data.new_profiles = new_profiles;
		done++;
		if(done == toBeDoneItems){
			console.log(data);
			endScript();
		}
	});
	getWeeklyActiveUsers(datetime,function(err,weekly_active){
		if(err){
			console.trace(err);
			endScript();
		}
		data.weekly_active = weekly_active;
		done++;
		if(done == toBeDoneItems){
			console.log(data);
			endScript();
		}
	});
	getMonthlyActiveUsers(datetime,function(err,monthly_active){
		if(err){
			console.trace(err);
			endScript();
		}
		data.monthly_active = monthly_active;
		done++;
		if(done == toBeDoneItems){
			console.log(data);
			endScript();
		}
	});
	getTotalActiveGroups(datetime,function(err,active_groups){
		if(err){
			console.trace(err);
			endScript();
		}
		data.active_groups = active_groups;
		done++;
		if(done == toBeDoneItems){
			console.log(data);
			endScript();
		}
	});
	getTotalMsgsExchanged(datetime,function(err,msgs_exch){
		if(err){
			console.trace(err);
			endScript();
		}
		data.msgs_exch = msgs_exch;
		done++;
		if(done == toBeDoneItems){
			console.log(data);
			endScript();
		}
	});
	getTotalMsgsDelivered(datetime,function(err,msgs_dlvrd){
		if(err){
			console.trace(err);
			endScript();
		}
		data.msgs_dlvrd = msgs_dlvrd;
		done++;
		if(done == toBeDoneItems){
			console.log(data);
			endScript();
		}
	});
}

function getNewAccounts(datetime,cb){
	Account.count({
		cat :{
			$gte : datetime.start,
			$lte : datetime.end
		}
	},function(err,accounts){
		if(err){
			return cb(err,null);
		}
		return cb(null,accounts);
	});
}

function getNewProfiles(datetime,cb){
	Profile.count({
		cat :{
			$gte : datetime.start,
			$lte : datetime.end
		}
	},function(err,profiles){
		if(err){
			return cb(err,null);
		}
		return cb(null,profiles);
	});
}

function getWeeklyActiveUsers(datetime,cb){
	var toBeDone = 5;
	var done = 0;
	var weekly_active_data = [];
	for(var i=1; i<6; i++){
		var start = datetime["week"+i];
		var end;
		if(i == 5){
			end = datetime.end;
		} else {
			end   = datetime["week"+(i+1)];
		}
		getActiveUsersCountInWeek(i,start,end,function(err,resp){
			if(err){
				return cb(err,null);
			}
			done++;
			weekly_active_data.push(resp);
			if(done == toBeDone){
				return cb(null,weekly_active_data);
			}
		});
	}
}

function getActiveUsersCountInWeek(week_num,week_start,week_end,cb){
	Message.distinct('from',{
    cat :{
			$gte : week_start,
			$lt :  week_end
		}
  },function(err,monthly_active){
    if(err){
			return cb(err,null);
		}
		return cb(null,{week:week_num,week_start:week_start,week_end:week_end,act_users:monthly_active.length});
  });
}

function getMonthlyActiveUsers(datetime,cb){
	Message.distinct('from',{
    cat :{
			$gte : datetime.start,
			$lt : datetime.end
		}
  },function(err,monthly_active){
    if(err){
			return cb(err,null);
		}
		return cb(null,monthly_active.length);
  });
}

function getTotalActiveGroups(datetime,cb){
	var msgTypes = [Message.MESSAGE_TYPE.audio,
									Message.MESSAGE_TYPE.text,
									Message.MESSAGE_TYPE.video,
									Message.MESSAGE_TYPE.image];
	Message.distinct('to',{
    type : { $in : msgTypes },
    cat :{
			$gte : datetime.start,
			$lte : datetime.end
		}
  },function(err,active_groups){
    if(err){
			return cb(err,null);
		}
		return cb(null,active_groups.length);
  });
}

function getTotalMsgsExchanged(datetime,cb){
	Status.count({
		tim :{
			$gte : datetime.start_mili,
			$lte : datetime.end_mili
		}
	},function(err,msgs_exch){
		if(err){
			return cb(err,null);
		}
		return cb(null,msgs_exch);
	});
}

function getTotalMsgsDelivered(datetime,cb){
	Status.count({
		rtim : { $exists : true},
		rtim :{
			$gte : datetime.start,
			$lte : datetime.end
		}
	},function(err,msgs_dlvrd){
		if(err){
			return cb(err,null);
		}
		return cb(null,msgs_dlvrd);
	});	
}


function getStartAndEndDateString(start, end){
	// var start_date = 'ISODate("'+start+'T00:00:00:000Z")';
	// var end_date   = 'ISODate("'+end+'T023:59:59:000Z")';

	var start_time 	= moment(start, ["YYYY-MM-DD"]).tz(configs.MOMENT_TIMEZONE).valueOf();
	var end_time 		= moment(end, ["YYYY-MM-DD"]).tz(configs.MOMENT_TIMEZONE).endOf("day").valueOf();

	start_time      = new Date(start_time);
	end_time        = new Date(end_time);

	var start_mili  = start_time.getTime();
	var end_mili    = end_time.getTime();

	var weeKMilliSeconds = 7*24*60*60*1000;

	var week1StartMilli    = start_mili;
	var week1Start         = new Date(start_mili);

	var week2StartMilli    = week1StartMilli+weeKMilliSeconds;
	var week2Start         = new Date(week2StartMilli);

	var week3StartMilli    = week2StartMilli+weeKMilliSeconds;
	var week3Start         = new Date(week3StartMilli);

	var week4StartMilli    = week3StartMilli+weeKMilliSeconds;
	var week4Start         = new Date(week4StartMilli);

	var week5StartMilli    = week4StartMilli+weeKMilliSeconds;
	var week5Start         = new Date(week5StartMilli);

	var datetime = {
		start : start_time,
		end   : end_time,
		start_mili : start_mili,
		end_mili : end_mili,
		week1 : week1Start,
		week1_mili : week1StartMilli,
		week2 : week2Start,
		week2_mili : week2StartMilli,
		week3 : week3Start,
		week3_mili : week3StartMilli,
		week4 : week4Start,
		week4_mili : week4StartMilli,
		week5 : week5Start,
		week5_mili : week5StartMilli
	};
	return datetime;
}

function isValidDateFormat(input){
	if(!input) return false;
	var parts = input.split('-');
	if(parts.length != 3) return false;
	if(!isValidYear(parts[0])) return false;
	if(!isValidMonth(parts[1])) return false;
	if(!isValidDay(parts[2])) return false;
	return true;
}

function isValidYear(year){
	if(!year) return false;
	var input = parseInt(year);
	if(input == 2015 || input == 2016) return true;
	return false;
}

function isValidMonth(month){
	if(!month) return false;
	var input = parseInt(month);
	if(input<1 || input>12) return false;
	return true;
}

function isValidDay(day){
	if(!day) return false;
	var input = parseInt(day);
	if(input < 1 || input > 31) return false;
	return true;
}

function endScript(){
	console.log('\n');
	console.log('------------------MONTHLY DATA REPORT ENDED---------------------');
	console.log('\n');
	process.exit(0);
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : configs.MONGO_SERVER_KEEPALIVE
      }
    }
  };
  if(!configs.MONGO_UNAME || configs.MONGO_UNAME == ""){
    options.server.poolSize = configs.MONGO_POOLSIZE;
    console.log('[[[[[[[    NO MONGO_UNAME FOUND IN CONFIGS, WILL CONNNECT WITHOUT REPLICA OPTIONS     ]]]]]]]]]');
    console.log('[[[[[[[    MONGODB CONNECTION POOL_SIZE : '+configs.MONGO_POOLSIZE+'     ]]]]]]]]]');
    return mongoose.connect("mongodb://"+configs.MONGO_HOST+":"+configs.MONGO_PORT+"/"+configs.DB_NAME,options);
  }
  options.replset = {
    rs_name : configs.MONGO_REPLICA_SET_NAME,
    socketOptions : {
      keepAlive : configs.MONGO_REPLICA_KEEPALIVE
    },
    poolSize : configs.MONGO_POOLSIZE,
    readPreference: 'secondaryPreferred',
    strategy: "ping",
    slaveOk: true
  };
  var connect_string = "mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@";
  var mongo_hosts    = configs.MONGO_HOST;
  var mongo_port     = configs.MONGO_PORT;
  for(var i=0; i<mongo_hosts.length; i++){
    if(i > 0)
      connect_string += ",";
    connect_string += mongo_hosts[i]+":"+mongo_port;
  }
  connect_string += "/"+configs.DB_NAME;
  console.log('[[[[[[[     CONNECTING MONGODB WITH REPLICA_SET OPTIONS     ]]]]]]]]]');
  console.log('[[[[[[[     MONGODB CONNECTION POOL_SIZE : '+configs.MONGO_POOLSIZE+'      ]]]]]]]]]');
  return mongoose.connect(connect_string,options);
}
