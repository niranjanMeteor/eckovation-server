var mongoose            = require('mongoose');
var configs             = require('./../utility/configs.js');

var connect = function () {
  var options = {
    server : {
      socketOptions : {
        keepAlive : 120,
        connectTimeoutMS: 30000
      },
      poolSize : 5
    }
  };
  if(configs.MONGO_UNAME!="") {
    mongoose.connect("mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@"+configs.MONGO_HOST+"/"+configs.DB_NAME,options);
  } else {
    mongoose.connect("mongodb://"+configs.MONGO_HOST+"/"+configs.DB_NAME,options);
  }
}
connect();
mongoose.connection.on('error',console.log);
mongoose.connection.on('disconnected', connect);

require('../models/profiles.js');
require('../models/accounts.js');
require('../models/statuses.js');

var Profile         = mongoose.model('Profile');
var Account             = mongoose.model('Account');
var Status              = mongoose.model('Status');

var mid = 'c78ef317a7ce6a6edc4816f035b7ee576cc4d3f82675d7ee08d2ace07458c0ee';
getAccountStatusReport(mid);
function getAccountStatusReport(mid){
  Status.distinct('pid',{
    mid : mid,
    stim: { $exists : true }
  },function(err,pids){
    if(err){
      console.trace(err);
      return;
    }
    if(pids.length == 0){
      console.log('no pids found for this mid');
      return;
    }
    console.log('Num of Profile seen this message : '+pids.length);
    Profile.distinct('aid',{
      _id : { $in : pids },
      act : true
    },function(err,aids){
      if(err){
        console.trace(err);
        return;
      }
      if(aids.length == 0){
        console.log('No account ids found for the profiles');
        return;
      }
      console.log('Num of accounts seen this message : '+aids.length);
      Account.count({
        _id : { $in : aids },
        bulk: { $exists : true },
        $or : [
          {
            lsnm : { $exists : true}
          },
          {
            lsni : { $exists : true}
          },
          {
            lsnw : { $exists : true}
          }
        ]
      },function(err,bAccounts){
        if(err){
          console.trace(err);
          return;
        }
        console.log('First Result , Accounts with bulk=1 and active are  : '+bAccounts);
      });
      Account.count({
        _id : { $in : aids },
        bulk: { $exists : false },
        $or : [
          {
            lsnm : { $exists : true}
          },
          {
            lsni : { $exists : true}
          },
          {
            lsnw : { $exists : true}
          }
        ]
      },function(err,nAccounts){
        if(err){
          console.trace(err);
          return;
        }
        console.log('Second Result , Accounts which are native and active are  : '+nAccounts);
      });
    });
  });
}