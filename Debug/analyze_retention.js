var momentTZ				    = require('moment-timezone');
var moment				      = require('moment');
var mongoose            = require('mongoose');
var _                   = require('underscore');
var readline 			      = require('readline');

var configs             = require('./../utility/configs.js');

var connect = function () {
  var options = {server : {socketOptions : {keepAlive : 1}}};
  if(configs.MONGO_UNAME!="") {
    mongoose.connect("mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@"+configs.MONGO_HOST+"/"+configs.DB_NAME,options);
  } else {
    mongoose.connect("mongodb://"+configs.MONGO_HOST+"/"+configs.DB_NAME,options);
  }
}
connect();
mongoose.connection.on('error',console.log);
mongoose.connection.on('disconnected', connect);

require('./../models/accounts.js');
var Account 					   = mongoose.model('Account');

var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

var allemails = [];

rl.question("Please enter your start-end & analytics end date in IST in (hh:mm:ss mm:dd:yy-hh:mm:ss mm:dd:yy-hh:mm:ss mm:dd:yy)? : ", function(dates) {
	var start = dates.split("-")[0].trim();
	var end = dates.split("-")[1].trim();

	var start_ts = momentTZ(start,"HH:mm:ss DD:MM:YYYY").tz(configs.MOMENT_TIMEZONE).valueOf();
	var end_ts = momentTZ(end,"HH:mm:ss DD:MM:YYYY").tz(configs.MOMENT_TIMEZONE).valueOf();

	var start_rev = momentTZ(start_ts).tz(configs.MOMENT_TIMEZONE).format();
	var end_rev = momentTZ(end_ts).tz(configs.MOMENT_TIMEZONE).format();

	var analytics_start = momentTZ(dates.split("-")[2].trim(),"HH:mm:ss DD:MM:YYYY").tz(configs.MOMENT_TIMEZONE).valueOf();

	analytics_start = new Date(analytics_start);

	Account.find({
		cat : {
			$gte : new Date(start_ts),
			$lt  : new Date(end_ts),
		},
	},function(err,accs) {
		console.log("Okay, so we got " + accs.length + " accounts between " + start_rev + " , " + end_rev);

		if(accs.length == 0) {
			console.log("Since there were no signups at that time, so bye!");
			process.exit();
		}

		var accs_ids = [];
		accs.forEach(function(acc){
			accs_ids.push(acc._id);
		});

		Account.find({
			_id : {$in : accs_ids },
			$or : [
						{ 
							lsnm : {
								$gte : analytics_start
							}
						},
						{
							lsnw : {
								$gte : analytics_start
							}
						},
						{
							lsni : {
								$gte : analytics_start
							}
						},
						{
							lsnwn : {
								$gte : analytics_start
							}
						}
					]
		},function(err,retained_accs){
				console.log("So, have about " + retained_accs.length + " retained accounts! That's about " + (100*(retained_accs.length)/(accs.length)) + "%");
		});
	});

	rl.close();
});