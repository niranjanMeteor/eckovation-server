var mongoose            = require('mongoose');
var configs             = require('./../utility/configs.js');
var readline 			      = require('readline');

require('../models/groups.js');
require('../models/privilegegroups.js');

var Group 				      = mongoose.model('Group');
var PrivilegeGroup 			= mongoose.model('PrivilegeGroup');


var rL = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

var options             = {server : {socketOptions : {keepAlive : 1}}};
mongoose.connect("mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@"+configs.MONGO_HOST+"/"+configs.DB_NAME,options, function(err){
	if(err) {
		console.log(err);
		console.trace(err);
	} else {
		console.log('Connected to mongodb!');
		console.log('------------------------------------Privilege Group SCRIPT STARTED------------------------------------')
		console.log('Program will give you a chance to create a new privileged group or to remove an already existing group from the privileged groups');
		startPrivilegeGroupUpdation();
	}
});

var ADD_NEW_PRIVILEGED_GROUP = 1;
var REMOVE_PRIVILEGED_GROUP  = 2;

function startPrivilegeGroupUpdation(){
	rL.question("Enter your option ( 0 to Exit, 1 to add, 2 to remove ) : ", function(answer) {
		if(isNaN(answer)) {
			console.log('Oops , you entered a wrong option , Please enter a right option');
			return startPrivilegeGroupUpdation();
		}
		switch(parseInt(answer)){
			case 1:
				addNewPrivilegeGroup();
			break;

			case 2:
				removeFromPrivilegedGroups();
			break;

			case 0:
				endScript();
			break;

			default:
				console.log('Oops , you entered a wrong option , Please enter a right option');
				startPrivilegeGroupUpdation();
		}
	});
}

function addNewPrivilegeGroup(){
	rL.question("Enter the group code to add new privileged group ( Or 0 to Exit ) : ", function(code) {
		if(code == 0 || code == '0')
			endScript();
		if(isNaN(code)) {
			console.log('Oops , you entered a wrong option , Please enter a right option');
			return addNewPrivilegeGroup();
		}
		Group.findOne({
			code : code
		},function(err,group){
			if(err) { console.trace(err); return; }
			if(!group) { 
				console.log('Unfortunately , the group code you entered is not valid , Please try a different group code');
				return addNewPrivilegeGroup();
			}
			PrivilegeGroup.findOne({
				code : code,
				gid  : group._id
			},function(err,privilege_group_existing){
				if(err) { console.trace(err); return; }
				if(privilege_group_existing) { 
					console.log('Looks Like , the group code you entered is already a privileged group, you may try a different group code');
					return addNewPrivilegeGroup();
				}
				getConfirmationFromUser(group, ADD_NEW_PRIVILEGED_GROUP, function(confirm){
					if(confirm == 1){
						new_privilege_group = new PrivilegeGroup({
							gid : group._id,
							code: code
						});
						new_privilege_group.save(function(err, inserted_privilege_group){
							if(err) { console.trace(err); return; }
							if(!inserted_privilege_group) {
								console.trace('Privilege group record could not be created'); 
								return endScript(); 
							}
							console.log('Congratulations , you have successfully added a new Privileged Group');
							endScript();
						});
					} else {
						console.log('Oops, Looks Like you were unsure of the group , try a different group code');
						return addNewPrivilegeGroup();
					}
				});
			});
		});
	});
}

function removeFromPrivilegedGroups(){
	rL.question("Enter the group code to remove a privileged group ( Or 0 to Exit ) : ", function(code) {
		if(code == 0 || code == '0')
			endScript();
		if(isNaN(code)) {
			console.log('Oops , you entered a wrong option , Please enter a right option');
			return removeFromPrivilegedGroups();
		}
		Group.findOne({
			code : code
		},function(err,group){
			if(err) { console.trace(err); return; }
			if(!group) { 
				console.log('Unfortunately , the group code you entered is not valid , Please try a different group code');
				return removeFromPrivilegedGroups();
			}
			PrivilegeGroup.findOne({
				code : code,
				gid  : group._id
			},function(err,privilege_group_existing){
				if(err) { console.trace(err); return; }
				if(!privilege_group_existing) { 
					console.log('Looks Like , the group code you entered is not a privileged group, you may enter a different group code');
					return removeFromPrivilegedGroups();
				}
				getConfirmationFromUser(group, REMOVE_PRIVILEGED_GROUP, function(confirm){
					if(confirm == 1){
						PrivilegeGroup.remove({
							gid : group._id,
							code: code
						},function(err, removed_privilege_group){
							if(err) { console.trace(err); return; }
							if(!removed_privilege_group) {
								console.trace('Privilege group record could not be removed'); 
								return endScript(); 
							}
							console.log('Congratulations , you have successfully removed a group from Privileged Groups');
							endScript();
						});
					} else {
						console.log('Oops, Looks Like you were unsure of the group , try a different group code');
						return removeFromPrivilegedGroups();
					}
				});
			});
		});
	});
}

function getConfirmationFromUser(group, action, cb){
	if(action == ADD_NEW_PRIVILEGED_GROUP){
		console.log('The Group that you want to add to Privileged Groups is following');
		console.log(group);
		console.log('Are you sure that you wanna add this group into privileged groups');
	} else if(action == REMOVE_PRIVILEGED_GROUP){
		console.log('The Group that you want to remove from Privileged Groups is following');
		console.log(group);
		console.log('Are you sure that you wanna remove this group from privileged groups');
	}
	rL.question("Press 1 to confirm , 2 if unsure ( Or 0 to Exit ) : ", function(confirmation) {
		if(confirmation == 0 || confirmation == '0')
			endScript();
		if(isNaN(confirmation)) {
			console.log('Fuck off you Loser , looks like you have to come to waste your and our time');
			endScript();
		}
		return cb(parseInt(confirmation));
	});
}

function endScript(){
	console.log('----------------------Privilege Group Updation Script ended------------------------');
	process.exit(0);
}

