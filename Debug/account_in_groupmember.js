var mongoose            = require('mongoose');
var configs             = require('./../utility/configs.js');
var readline 			      = require('readline');

require('../models/groupmembers.js');
require('../models/groups.js');
require('../models/profiles.js');

var Group 				      = mongoose.model('Group');
var GroupMember 		    = mongoose.model('GroupMember');
var Profile 				    = mongoose.model('Profile');


var rL = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

var remaining = 0;
var replicated = 0;
var skip_value = 0;

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});
mongoose.connection.on('connected', function(){
  console.log('Successfully Opened MOngoDb Connection');
  console.log('------------------------------------Profile Account Id Replication SCRIPT STARTED------------------------------------')
	console.log('Program will give you a chance to replicate the profile account ids groupmembers collection ');
	Profile.count({},function(err, count){
		if(err) { console.trace(err); return; }
		console.log('Total number of profile account ids to migrate : '+count);
		remaining = count;
		startGroupMemberUpdation(skip_value, replicated, remaining);
	});
});

function startGroupMemberUpdation(skip_value ,replicated, remaining){
	if(remaining <= 0){
		endScript();
	}
	rL.question('Enter number of profiles names you wanna replicate, (replicated:'+replicated+' , remaining:'+remaining+') : ', function(answer) {
		if(!isNaN(answer)) {
			var limit = parseInt(answer);
			if(limit > remaining || limit <= 0) {
				console.log('Oops , you have entered an incorrect value , Please enter a correct value');
				return startGroupMemberUpdation(skip_value, replicated, remaining);
			}
			add_account_ids(skip_value, limit, function(err,resp){
				if(err){ console.log('Sorry the account_id updation was not successful'); return ; }
				replicated = replicated + resp.success;
				remaining  = remaining - resp.done;
				skip_value = skip_value + limit;
				return startGroupMemberUpdation(skip_value, replicated, remaining);
			})
		} else {
			console.log('Oops , you entered a wrong option , Please enter a right option');
			return startGroupMemberUpdation(skip_value, replicated, remaining);
		}
	});
}

function add_account_ids(skip_value, limit ,cb){
	var done = 0;
	var success = 0;
	Profile.find({},{
		_id : 1,
		aid : 1
	},{
		sort : {
			_id : 1
		},
		skip : skip_value,
		limit : limit
	},function(err, profiles){
		if(err) { console.trace(err); return cb(true, false); }
		if(profiles.length == 0) { console.log('No Profiles found to add aid'); return cb(true,false); }
		total_profiles = profiles.length;
		console.log('total number fo pids to process : '+total_profiles);
		for(var i=0; i<total_profiles; i++){
			updateGroupMembersWithNames(profiles[i]._id ,profiles[i].aid, function(err,resp){
				++done;
				if(resp) {
					++success;
				}
				if(done == limit) {
					console.log('Out of total processing of '+limit+' profiles , succcessfully migrated were : '+success);
					return cb(null, {done:done,success:success});
				}
			});
		}
	});
}

function updateGroupMembersWithNames(pid , aid, cb){
	// var aid = aid+'';
	GroupMember.update({
		pid : pid,
	},{
		aid : aid
	},{
		multi : true
	},function(err, updateGrpMemName){
		if(err) { console.trace(err); return cb(true,false);}
		if(updateGrpMemName) { console.log('updated success for pid : '+pid+' , with aid : '+aid); return cb(null,true); }
	});
}

function endScript(){
	console.log('----------------------Profile Account Id Replication SCRIPT ended------------------------');
	process.exit(0);
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : 120
      }
    },
    replset : {
      rs_name : configs.MONGO_REPLICA_SET_NAME,
      socketOptions : {
        keepAlive : 120
      }
    }
  };

  if(configs.MONGO_UNAME != "") {
    var connect_string = "mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@";
    var mongo_hosts = configs.MONGO_HOST;
    var mongo_port = configs.MONGO_PORT;
    for(var i=0; i<mongo_hosts.length; i++){
      if(i > 0)
        connect_string += ",";
      connect_string += mongo_hosts[i]+":"+mongo_port;
    }
    connect_string += "/"+configs.DB_NAME,options;
    mongoose.connect(connect_string);
    // mongoose.connect( "mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@"+"127.0.0.1"+":"+configs.MONGO_PORT+"/"+configs.DB_NAME,options);
  } else {
    mongoose.connect("mongodb://"+configs.MONGO_HOST+"/"+configs.DB_NAME,options);
  }
}