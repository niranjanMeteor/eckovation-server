var mongoose                = require('mongoose');
var redis                   = require('redis');
var readline                = require('readline');
var moment                  = require('moment-timezone');
var configs                 = require('./../utility/configs.js');
var RedisPrefixes           = require('./../utility/redis_prefix.js');
var Actions                 = require('./../utility/action_names.js');

var redis_client            = redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
  console.trace("Redis Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});
mongoose.connection.on('connected', function(){
  console.log('Successfully Opened MOngoDb Connection');
  startScript();
  startUserTracing();
});

require('../models/accounts.js');
require('../models/profiles.js');
require('../models/groups.js');
require('../models/groupmembers.js');
require('../models/messages.js');
require('../models/statuses.js');
var Account               = mongoose.model('Account');
var Profile               = mongoose.model('Profile');
var Group                 = mongoose.model('Group');
var GroupMember           = mongoose.model('GroupMember');
var Message               = mongoose.model('Message');
var Status                = mongoose.model('Status');

var rcq                   = require("./../utility/redis_chat_queue.js")(redis_client);
var rhmset                = require("./../utility/redis_hmset.js")(redis_client);

var rL = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

var account_id;
var result_data = {};
var profilesById = [];
var current_time = moment().tz(configs.TRENDING_GROUPS_TIMEZONE).valueOf();
var timeLastFifteeenDays;// = new Date(moment(current_time-(15*24*3600*1000)));
// console.log('Time Last 15 Days ');
// console.log(timeLastFifteeenDays);
var TARGET_MSG_TYPES = [Message.MESSAGE_TYPE.audio,Message.MESSAGE_TYPE.text,Message.MESSAGE_TYPE.video,Message.MESSAGE_TYPE.image];

function startUserTracing(){
  rL.question("Enter User Mobile Number (10 digit authentic mobile number) : ", function(mobile_number) {
    if(isNaN(mobile_number)) {
      console.log('Oops , you entered a wrong option , Please enter a right option');
      return startUserTracing();
    }
    if(!isValidMobileNumber(mobile_number)){
      console.log('Ooops, The Mobile number you entered is not a valid Indian Mobile Number');
      return startUserTracing();
    }
    return ActivateUserTrace(mobile_number);
  });
}

function ActivateUserTrace(mobile_number){
  rL.question("Enter Number of Days You wanna trace back this User (max Limit is 30 days) : ", function(days) {
    if(isNaN(days)) {
      console.log('Oops , you entered a wrong option , Please enter a right option');
      return ActivateUserTrace(mobile_number);
    }
    var numDays = parseInt(days);
    if(numDays <=0 ){
      console.log('Ooops, You are not allowed to trace about FUTURE');
      return ActivateUserTrace(mobile_number);
    }
    if(numDays > 30){
      console.log('Ooops, You are not allowed to trace for more than 30 days back');
      return ActivateUserTrace(mobile_number);
    }
    timeLastFifteeenDays = new Date(moment(current_time-(numDays*24*3600*1000)));
    getAccountInfoWithLastSeens(mobile_number,function(err,acc_info){
      if(err){
        console.trace(err);
        return endScript();
      }
      account_id          = acc_info.aid;
      result_data.account = acc_info;
      getProfilesInfo(account_id, function(err,prof_info){
        if(err){
          console.trace(err);
          return endScript();
        }
        result_data.profiles = prof_info.data;
        if(prof_info.pids.length == 0){
          return endScript();
        }
        var pids = prof_info.pids;
        var actPidsLen    = prof_info.data.active.length;
        var inactPidsLen  = prof_info.data.inactive.length;
        for(var i=0; i<actPidsLen; i++){
          profilesById[prof_info.data.active[i]._id+''] = prof_info.data.active[i];
        }
        for(var j=0; j<inactPidsLen; j++){
          profilesById[prof_info.data.inactive[j]._id+''] = prof_info.data.inactive[j];
        }
        var toBeDone = 4;
        var done = 0;
        getGroupsInfo(pids,function(err,grps_info){
          if(err){
            console.trace(err);
            return endScript();
          }
          done++;
          result_data.groups = grps_info;
          // result_data.groups = JSON.stringify(grps_info);
          if(done == toBeDone){
            return endScript();
          }
        });
        getMessagesInfo(pids,function(err,msg_info){
          if(err){
            console.trace(err);
            return endScript();
          }
          done++;
          result_data.messages = msg_info;
          // result_data.messages = JSON.stringify(msg_info);
          if(done == toBeDone){
            return endScript();
          }
        });
        getRcqInfo(pids,function(err,rcq_info){
          if(err){
            console.trace(err);
            return endScript();
          }
          done++;
          result_data.rcQ = rcq_info;
          // result_data.rcQ = JSON.stringify(rcq_info);
          if(done == toBeDone){
            return endScript();
          }
        });
        getAdminSeenInfo(pids,function(err,adminq_info){
          if(err){
            console.trace(err);
            return endScript();
          }
          done++;
          result_data.adminQ = adminq_info;
          // result_data.adminQ = JSON.stringify(adminq_info);
          if(done == toBeDone){
            return endScript();
          }
        });
      });
    });
  });
}

function getAccountInfoWithLastSeens(mobile_number,cb){
  var account_info = {};
  account_info.app_version = {};
  account_info.last_seens  = {};
  Account.findOne({
    m : mobile_number
  },{},{lean:true},function(err,account){
    if(err){
      return cb(err,null);
    }
    if(!account){
      console.log('Sorry account not found');
      return cb(true,null);
    }
    account_info.aid  = account._id;
    account_info.created_on = account.cat;
    account_info.emails = account.e;
    account_info.verify_status = account.vrfy ? 'verified' : 'unverified';

    if(typeof(account.vrsn) !== 'undefined')
      account_info.app_version.android = account.vrsn;
    if(typeof(account.iosv) !== 'undefined')
      account_info.app_version.ios = account.iosv;
    if(typeof(account.webv) !== 'undefined')
      account_info.app_version.web = account.webv;
    if(typeof(account.wndv) !== 'undefined')
      account_info.app_version.windows = account.wndv;

    if(typeof(account.lsnm) !== 'undefined')
      account_info.last_seens.android = account.lsnm;
    if(typeof(account.lsni) !== 'undefined')
      account_info.last_seens.ios = account.lsni;
    if(typeof(account.lsnw) !== 'undefined')
      account_info.last_seens.web = account.lsnw;
    if(typeof(account.lsnwn) !== 'undefined')
      account_info.last_seens.windows = account.lsnwn;

    return cb(null,account_info);
  });
}

function getProfilesInfo(aid, cb){
  var profile_info = {};
  var pids         = [];
  profile_info.active_profiles_count   = 0;
  profile_info.inactive_profiles_count = 0;
  profile_info.active   = [];
  profile_info.inactive = [];

  Profile.find({
    aid : aid
  },{
    nam : 1,
    cnam: 1,
    ppic: 1,
    role: 1,
    act : 1,
    cat : 1
  },function(err,profiles){
    if(err){
      return cb(err,null);
    }
    if(profiles.length == 0){
      console.log('Ooops , No Profiles exists for user ');
      return cb(null,{data:profile_info, pids:pids});
    }
    var num_profs = profiles.length;
    var a_count   = 0;
    var in_count  = 0;
    for(var i=0; i<num_profs; i++){
      pids.push(profiles[i]._id);
      if(profiles[i].act === true){
        profile_info.active[a_count] = profiles[i];
        a_count++;
      } else {
        profile_info.inactive[in_count] = profiles[i];
        in_count++;
      }
    }
    profile_info.active_profiles_count = a_count;
    profile_info.inactive_profiles_count = in_count;
    return cb(null,{data:profile_info, pids:pids});
  });
}

function getGroupsInfo(pids, cb){
  var group_info   = {};
  var gids         = [];
  var groupById    = [];
  group_info.admin_groups_count     = 0;
  group_info.member_groups_count    = 0;
  group_info.banned_groups_count    = 0;
  group_info.left_groups_count      = 0;
  group_info.admin     = [];
  group_info.member    = [];
  group_info.banned    = [];
  group_info.left      = [];

  GroupMember.find({
    pid : { $in : pids },
    cat : { $gte : timeLastFifteeenDays}
  },{
    gid : 1,
    act : 1,
    type: 1
  },{lean:true},function(err,groupmems){
    if(err){
      return cb(err,null);
    }
    if(groupmems.length == 0){
      return cb(null,group_info)
    }
    var num_groupmems = groupmems.length;
    for(var j=0; j<num_groupmems; j++){
      if(gids.indexOf(groupmems[j].gid+'') < 0)
        gids.push(groupmems[j].gid+'');
    }
    Group.find({
      _id : { $in : gids }
    },{
      code : 1,
      name : 1,
      gpic : 1,
      gtyp : 1,
      cat  : 1
    },function(err,groups){
      if(err){
        return cb(err,null);
      }
      if(groups.length == 0){
        console.log('Oops , there is some database malfunction since some gids from groupmembers coudl nto be found in  grousp table');
        return cb(true,null);
      }
      var num_groups = groups.length;
      for(var m=0; m<num_groups; m++){
        groupById[groups[m]._id+''] = groups[m];
      }
      var a_count  = 0;
      var m_count  = 0;
      var l_count  = 0;
      var b_count  = 0;
      var left_groups = [];
      for(var i=0; i<num_groupmems; i++){
        if(groupmems[i].act){
          if(groupmems[i].type === GroupMember.TYPE.ADMIN){
            group_info.admin[a_count]            = groupById[groupmems[i].gid];
            a_count++;
          } else if(groupmems[i].type === GroupMember.TYPE.MEMBER){
            group_info.member[m_count] = groupById[groupmems[i].gid];
            m_count++;
          } else if(groupmems[i].type === GroupMember.TYPE.BANNED){
            group_info.banned[b_count] = groupById[groupmems[i].gid];
            b_count++;
          }
        } else if(left_groups.indexOf(groupmems[i].gid) < 0){
          left_groups.push(groupmems[i].gid);
        }
      }
      // group_info.admin = JSON.parse(JSON.stringify(group_info.admin));
      group_info.admin_groups_count     = a_count;
      group_info.member_groups_count    = m_count;
      group_info.banned_groups_count    = b_count;
      
      getGroupLeaveTimings(left_groups,pids,groupById,function(err,leftGroupsInfo){
        if(err){
          return cb(err,null);
        }
        group_info.left              = leftGroupsInfo.lGroups;
        group_info.left_groups_count = leftGroupsInfo.lCount;
        return cb(null,group_info);
      });
    });
  });
}

function getMessagesInfo(pids, cb){
  var message_info = {};
  message_info.created_count                = 0;
  message_info.recieved_count               = 0;
  message_info.seen_count                   = 0;
  message_info.last_few_messages            = [];
  message_info.last_recieved_message        = {};
  message_info.last_seen_message            = {};
  message_info.average_received_delay       = 0;
  message_info.average_seen_delay           = 0;

  var toBeDone = 4;
  var done     = 0;
  Message.count({
    from: { $in : pids },
    cat : { $gte : timeLastFifteeenDays},
    type: { $in : TARGET_MSG_TYPES }
  },function(err,cr_msg_counts){
    if(err){
      return cb(err,null);
    }
    message_info.created_count     = cr_msg_counts;
    if(cr_msg_counts > 0){
      Message.find({
        from : { $in : pids },
        cat : { $gte : timeLastFifteeenDays},
        type: { $in : TARGET_MSG_TYPES }
      },{},{
        skip : (cr_msg_counts-1)
      },function(err,last_few_msgs){
        if(err){
          return cb(err,null);
        }
        message_info.last_few_messages = last_few_msgs;
        done++;
        if(done == toBeDone){
          return cb(null,message_info);
        }
      });
    } else {
      done++;
      if(done == toBeDone){
        return cb(null,message_info);
      }
    }
  });
  Status.count({
    pid : { $in : pids},
    rtim: {$exists : true},
    cat : { $gte : timeLastFifteeenDays}
  },function(err,recieved_count){
    if(err){
      return cb(err,null);
    }
    message_info.recieved_count = recieved_count;
    if(recieved_count > 0){
      Status.find({
        pid : { $in : pids},
        rtim: {$exists : true},
        cat : { $gte : timeLastFifteeenDays}
      },{},{
        skip: (recieved_count-1)
      },function(err,last_recieved){
        if(err){
          return cb(err,null);
        }
        last_recieved = last_recieved[0];
        Message.findOne({
          _id : last_recieved.mid
        },function(err,last_recv_msg){
          if(err){
            return cb(err,null);
          }
          message_info.last_recieved_message = last_recv_msg;
          done++;
          if(done == toBeDone){
            return cb(null,message_info);
          }
        });
      });
    } else {
      done++;
      if(done == toBeDone){
        return cb(null,message_info);
      }
    }
  });
  Status.count({
    pid : { $in : pids},
    stim: {$exists : true},
    cat : { $gte : timeLastFifteeenDays}
  },function(err,seen_count){
    if(err){
      return cb(err,null);
    }
    message_info.seen_count = seen_count;
    if(seen_count > 0){
      Status.find({
        pid : { $in : pids},
        stim: {$exists : true},
        cat : { $gte : timeLastFifteeenDays}
      },{},{
        skip: (seen_count-1)
      },function(err,last_seen){
        if(err){
          return cb(err,null);
        }
        last_seen = last_seen[0];
        Message.findOne({
          _id : last_seen.mid
        },function(err,last_seen_msg){
          if(err){
            return cb(err,null);
          }
          message_info.last_seen_message = last_seen_msg;
          done++;
          if(done == toBeDone){
            return cb(null,message_info);
          }
        });
      });
    } else {
      done++;
      if(done == toBeDone){
        return cb(null,message_info);
      }
    }
  });
  getAverageMessageDelayData(pids, function(err,delayData){
    if(err){
      return cb(err,null);
    }
    var OneMinute = 60*1000;
    if(typeof(delayData.r_delay) !== 'undefined'){
      message_info.average_received_delay = parseInt(delayData.r_delay / OneMinute)+' Minutes';
    } else {
      message_info.average_received_delay = 'NA';
    }
    if(typeof(delayData.r_delay) !== 'undefined'){
      message_info.average_seen_delay = parseInt(delayData.s_delay / OneMinute)+' Minutes';
    } else {
      message_info.average_seen_delay = 'NA';
    }
    done++;
    if(done == toBeDone){
      return cb(null,message_info);
    }
  });
}

function getRcqInfo(pids, cb){
  var rcq_info          = {};
  rcq_info.total        = 0;
  rcq_info.perPidQueLen = [];
  var toBeDone          = pids.length;
  var done              = 0;
  for(var i=0; i<toBeDone; i++){
    getCardinalityOfPidQueue(pids[i],function(err,pidAndQueLen){
      if(err){
        return cb(err,null);
      }
      done++;
      var key = pidAndQueLen.pid+'('+profilesById[pidAndQueLen.pid].nam+')';
      rcq_info.perPidQueLen[key] = pidAndQueLen.qLen;
      rcq_info.total += pidAndQueLen.qLen;
      if(done == toBeDone){
        return cb(null,rcq_info);
      }
    });
  }
}

function getAdminSeenInfo(pids, cb){
  var adminq_info          = {};
  adminq_info.total        = 0;
  adminq_info.perPidQueLen = [];
  var toBeDone          = pids.length;
  var done              = 0;
  for(var i=0; i<toBeDone; i++){
    getLengthOfSeenQueue(pids[i],function(err,pidAndQueLen){
      if(err){
        return cb(err,null);
      }
      done++;
      if(pidAndQueLen.qLen > 0){
        var key = pidAndQueLen.pid+'('+profilesById[pidAndQueLen.pid].nam+')';
        adminq_info.perPidQueLen[key] = pidAndQueLen.qLen;
        adminq_info.total += pidAndQueLen.qLen;
      }
      if(done == toBeDone){
        return cb(null,adminq_info);
      }
    });
  }
}

function getCardinalityOfPidQueue(pid, cb){
  rcq.cardinality(pid,function(err,perPidLen){
     if(err){
      return cb(err,null);
     } 
     return cb(null,{ pid:pid, qLen:perPidLen });
  });
}

function getLengthOfSeenQueue(pid, cb){
  var key = RedisPrefixes.SEEN_COUNT+pid;
  rhmset.exists(key,function(err,resp){
    if(err){
      return cb(err,null);
    }
    if(resp){
      rhmset.len(key,function(err,perPidQueLen){
        if(err){
          return cb(err,null);
        } 
        return cb(null, { pid:pid, qLen:perPidQueLen });
      });
    } else {
      return cb(null, { pid:pid, qLen:0 });
    }
  });
}

function getAverageMessageDelayData(pids, cb){
  var message_delay_info                = {};
  var avg_srv_rcv_delay                 = 0;
  message_delay_info.avg_usr_rcv_delay  = 0;
  message_delay_info.avg_usr_seen_delay = 0;
  var toBeDone = 2;
  var done = 0;
  getAverageRecievedDelay(pids,function(err,avgRecvDelay){
    if(err){
      return cb(err,null);
    }
    message_delay_info.r_delay = avgRecvDelay;// - avg_srv_rcv_delay;
    done++;
    if(done == toBeDone){
      return cb(null, message_delay_info);
    }
  });
  getAverageSeenDelay(pids,function(err,avgSeenDelay){
    if(err){
      return cb(err,null);
    }
    message_delay_info.s_delay = avgSeenDelay;// - avg_srv_rcv_delay;
    done++;
    if(done == toBeDone){
      return cb(null, message_delay_info);
    }
  });
}

function getAverageRecievedDelay(pids, cb){
  var average_recvd_delay = 0;
  Status.aggregate([
    {
      $match :{
        pid  : { $in : pids },
        rtim : {$exists:true},
        cat  : { $gte: timeLastFifteeenDays }
      }
    },
    {
      $group :{
        _id : "$pid",
        avgRtDelay : {
          $avg : {
            $subtract : ["$rtim","$cat"]
          }
        },
        count : {
          $sum : 1
        }
      }
    }
  ],function(err,recdDelayResp){
    if(err){
      return cb(err,null);
    }
    var num_rcvd = recdDelayResp.length;
    if(num_rcvd == 0){
      return cb(null,average_recvd_delay);
    }
    var sum = 0;
    var total = 0;
    for(var i=0; i<num_rcvd; i++){
      sum += (recdDelayResp[i].avgRtDelay * recdDelayResp[i].count);
      total += recdDelayResp[i].count;
    }
    if(total > 0){
      average_recvd_delay = parseInt((sum / total));
    } else {
      average_recvd_delay = 0;
    }
    return cb(null,average_recvd_delay);
  });
}

function getAverageSeenDelay(pids, cb){
  var average_seen_delay = 0;
  Status.aggregate([
    {
      $match :{
        pid  : { $in : pids },
        stim : {$exists:true},
        rtim : {$exists:true},
        cat  : { $gte: timeLastFifteeenDays }
      }
    },
    {
      $group :{
        _id : "$pid",
        avgSnDelay : {
          $avg : {
            $subtract : ["$stim","$rtim"]
          }
        },
        count : {
          $sum : 1
        }
      }
    }
  ],function(err,seenDelayResp){
    if(err){
      return cb(err,null);
    }
    var num_seen = seenDelayResp.length;
    if(num_seen == 0){
      return cb(null,average_seen_delay);
    }
    var sum = 0;
    var total = 0;
    for(var i=0; i<num_seen; i++){
      sum += (seenDelayResp[i].avgSnDelay * seenDelayResp[i].count);
      total += seenDelayResp[i].count;
    }
    if(total > 0){
      average_seen_delay = parseInt((sum / total));
    } else {
      average_seen_delay = 0;
    }
    return cb(null,average_seen_delay);
  });
}

function getGroupLeaveTimings(gids, pids, groupById, cb){
  var leave_groups_info       = {};
  leave_groups_info.lCount    = 0;
  leave_groups_info.lGroups   = [];

  Message.find({
    to  : { $in : gids },
    from: { $in : pids },
    'actn.t' : Actions.GROUP_LEAVE
  },{},{
    sort : {
      tim : 1
    }
  },function(err,total_left){
    if(err){
      return cb(err,null);
    }
    var num_left = total_left.length;
    var leave_count = 0;
    for(var i=0; i<num_left; i++){
      leave_groups_info.lGroups[leave_count] = {};
      leave_groups_info.lGroups[leave_count].group      = groupById[total_left[i].to];
      leave_groups_info.lGroups[leave_count].left_time  = total_left[i].cat;
      leave_groups_info.lGroups[leave_count] = JSON.stringify(leave_groups_info.lGroups[leave_count]);
      leave_count++;
    }
    
    leave_groups_info.lCount = leave_count;
    return cb(null,leave_groups_info);
  });
}

function isValidMobileNumber(value){
  if(value.length != 10) return false;
  var indian_mobile_num_regex = /^[789]\d{9}$/;
  return indian_mobile_num_regex.test(value);
}

function startScript(){
  console.log('-------------------------------User Trace Begins----------------------------------');
  console.log('You will get a chance to observe various activities and recent data for the user');
}

function endScript(){
  // result_data = JSON.stringify(result_data);
  console.log('***********************************GOLD DUST****************************************');
  console.log(result_data);
  console.log('*************************************************************************************');
  console.log('We hope you were able to get all information that you were looking, Thanks very much for using our servide');
  console.log('-------------------------------User Trace Finished----------------------------------');
  process.exit(0);
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : 120
      }
    },
    replset : {
      rs_name : configs.MONGO_REPLICA_SET_NAME,
      socketOptions : {
        keepAlive : 120
      }
    }
  };

  if(configs.MONGO_UNAME != "") {
    var connect_string = "mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@";
    var mongo_hosts = configs.MONGO_HOST;
    var mongo_port = configs.MONGO_PORT;
    for(var i=0; i<mongo_hosts.length; i++){
      if(i > 0)
        connect_string += ",";
      connect_string += mongo_hosts[i]+":"+mongo_port;
    }
    connect_string += "/"+configs.DB_NAME,options;
    mongoose.connect(connect_string);
    // mongoose.connect( "mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@"+"127.0.0.1"+":"+configs.MONGO_PORT+"/"+configs.DB_NAME,options);
  } else {
    mongoose.connect("mongodb://"+configs.MONGO_HOST+"/"+configs.DB_NAME,options);
  }
}