var mongoose                = require('mongoose');
var redis                   = require('redis');
var configs                 = require('./../utility/configs.js');

var redis_client            = redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
  console.trace("Redis Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});
mongoose.connection.on('connected', function(){
  console.log('Successfully Opened MOngoDb Connection');
  startAccountProfileAndGmsMismatch();
});

require('../models/accounts.js');
require('../models/profiles.js');
require('../models/groupmembers.js');
var Account               = mongoose.model('Account');
var Profile               = mongoose.model('Profile');
var GroupMember           = mongoose.model('GroupMember');

var UserInfoCache         = require('./../utility/user_info_cache.js');

var faultyAccounts        = [];
var toDO                  = 0; 
var done                  = 0;

function startAccountProfileAndGmsMismatch(){
  Account.find({
    vrfy : true
  },function(err,accounts){
    if(err){
      console.trace(err);
      process.exit(0);
    }
    var total_accounts = accounts.length;
    toDo = total_accounts;
    if(total_accounts == 0){
      console.log('No accounts with vrfy as true exists in database');
    }
    console.log('total_accounts with vrfy true from db : '+total_accounts);
    for(var i=0; i<total_accounts; i++){
      checkProfileAndGmCache(accounts[i]._id,function(err,resp){
        if(err){
          console.trace(err);
          process.exit(0);
        }
        done++;
        if(resp.faulty){
          faultyAccounts.push({
            aid   : resp.aid,
            redis : resp.redis,
            db    : resp.db
          });
        }
        if(done == toDo){
          return printFaultyData();
        }
      });
    }
  });
} 

function checkProfileAndGmCache(aid, cb){
  var redis_nP, redis_nG, db_nP, db_nG;
  var done = 0;
  var toDo = 3;
  Profile.find({
    aid : aid,
    act : true
  },{
    _id : 1
  },function(err,dProfiles){
    if(err){
      return cb(err,null);
    }
    if(dProfiles.length == 0){
      return cb(null,{faulty:false});
    }
    db_nP = dProfiles.length;
    UserInfoCache.getUserGroupMemPartial(aid, function(err,rGms){
      if(err){
        return cb(err,null);
      }
      done++;
      redis_nG = rGms.length;
      if(done == toDo){
        return checkConsistencyData(aid, redis_nP, redis_nG, db_nP, db_nG, cb);
      }
    });
  });
  UserInfoCache.getUserProfilesPartial(aid,function(err, rProfiles){
    if(err){
      return cb(err,null);
    }
    done++;
    redis_nP = rProfiles.length;
    if(done == toDo){
      return checkConsistencyData(aid, redis_nP, redis_nG, db_nP, db_nG, cb);
    }
  });
  GroupMember.count({
    aid : aid,
    act : true,
    gdel: false
  },function(err,dGms){
    if(err){
      return cb(err,null);
    }
    done++;
    db_nG = dGms;
    if(done == toDo){
      return checkConsistencyData(aid, redis_nP, redis_nG, db_nP, db_nG, cb);
    }
  });
}

function checkConsistencyData(aid, redis_nP, redis_nG, db_nP, db_nG, cb){
  if(redis_nP != db_nP || redis_nG != db_nG){
    return cb(null,{
      faulty : true,
      aid    : aid,
      redis : {
        nP : redis_nP,
        nG : redis_nG
      },
      db : {
        nP : db_nP,
        nG : db_nG
      }
    });
  } else {
    return cb(null,{faulty:false});
  }
}

function printFaultyData(){
  var num_faulty_data = faultyAccounts.length;
  if(num_faulty_data == 0){
    console.log('No Faulty Data found');
    process.exit(0);
  } else {
    console.log('\n');
    console.log('---------------- Faulty Data Following -----------------');
    console.log('\n');
  }
  for(var j=0; j<num_faulty_data; j++){
    console.log(''+j+'   :  '+JSON.stringify(faultyAccounts[j]));
  }
  console.log('\n');
  process.exit(0);
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : 120
      }
    }
  };
  if(!configs.MONGO_UNAME || configs.MONGO_UNAME == ""){
    return mongoose.connect("mongodb://"+configs.MONGO_HOST+":"+configs.MONGO_PORT+"/"+configs.DB_NAME,options);
  }
  // if(configs.MONGO_UNAME == "eck_user_test"){
  //   return mongoose.connect("mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@"+"127.0.0.1"+":"+configs.MONGO_PORT+"/"+configs.DB_NAME,options);
  // }
  options.replset = {
    rs_name : configs.MONGO_REPLICA_SET_NAME,
    socketOptions : {
      keepAlive : 120
    }
  };
  var connect_string = "mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@";
  var mongo_hosts    = configs.MONGO_HOST;
  var mongo_port     = configs.MONGO_PORT;
  for(var i=0; i<mongo_hosts.length; i++){
    if(i > 0)
      connect_string += ",";
    connect_string += mongo_hosts[i]+":"+mongo_port;
  }
  connect_string += "/"+configs.DB_NAME,options;
  return mongoose.connect(connect_string);
}