var moment				      = require('moment');
var momentTZ				    = require('moment-timezone');
var mongoose            = require('mongoose');
var _                   = require('underscore');
var readline 			      = require('readline');

var configs             = require('./../utility/configs.js');

var connect = function () {
  var options = {server : {socketOptions : {keepAlive : 1}}};
  if(configs.MONGO_UNAME!="") {
    mongoose.connect("mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@"+configs.MONGO_HOST+"/"+configs.DB_NAME,options);
  } else {
    mongoose.connect("mongodb://"+configs.MONGO_HOST+"/"+configs.DB_NAME,options);
  }
}
connect();
mongoose.connection.on('error',console.log);
mongoose.connection.on('disconnected', connect);

require('./../models/profiles.js');
require('./../models/groups.js');
require('./../models/accounts.js');
require('./../models/groupmembers.js');
require('./../models/messages.js');
require('./../models/statuses.js');


var Profile            	 = mongoose.model('Profile');
var GroupMember 			   = mongoose.model('GroupMember');
var Group 					     = mongoose.model('Group');
var Account 					   = mongoose.model('Account');
var Message					= mongoose.model('Message');
var Status					= mongoose.model('Status');
var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.question("Please provide me with mids in CSV : ", function(answer) {
	var mids = answer.split(",");
	var _mids = [];
	mids.forEach(function(mid){
			mid = mid.trim();
			_mids.push(mid);
	});
	mids = _mids;

	var mid_process_count = 0;

	mids.forEach(function(mid){
		Message.findOne({_id:mid},function(err,res){
			if(err) {
				console.log("There was an error fetching the message");
				process.exit();
			}

			var mid_date = res["cat"];
			var _pids = [];

			Status.find({mid:mid,rtim:{"$exists":false},stim:{"$exists":false}},function(err,stses) {
				if (err) {
					console.log("There was an error in fetching statuses");
					process.exit();
				}

				if(stses.length <= 0) {
					console.log("Congrats! this mid : " + res["_id"] + " was delivered to everyone in the group!");
					return;
				}

				stses.forEach(function(sts){
					_pids.push(sts["pid"]);
				});


				var _aids = [];
				Profile.find({_id:{$in:_pids}},function(err,prfls){
					prfls.forEach(function(prfl){
						_aids.push(prfl["aid"]);
					});

					Account.find({_id: {$in  : _aids},$or:[{lsnm:{$gte : new Date(mid_date)}},{lsni:{$gte : new Date(mid_date)}}]},function(err,accs){
						if(err) {
							console.log("some error in aid fetching");
							console.log(err);
							process.exit();
						}

						if(accs.length == 0) {
							console.log("Found no faulty records : " + res["_id"]);
						} else {
							console.log("Found faulty records : " + res["_id"]);
							console.log(res);
							console.log("Faulty accounts are as below:");
							console.log(accs);
						}
					});

				});
			});
		});
	});
	
	rl.close();
});
