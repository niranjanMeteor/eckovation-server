var momentTZ				    = require('moment-timezone');
var moment				      = require('moment');
var mongoose            = require('mongoose');
var _                   = require('underscore');
var readline 			      = require('readline');

var configs             = require('./../utility/configs.js');

var connect = function () {
  var options = {server : {socketOptions : {keepAlive : 1}}};
  if(configs.MONGO_UNAME!="") {
    mongoose.connect("mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@"+configs.MONGO_HOST+"/"+configs.DB_NAME,options);
  } else {
    mongoose.connect("mongodb://"+configs.MONGO_HOST+"/"+configs.DB_NAME,options);
  }
}
connect();
mongoose.connection.on('error',console.log);
mongoose.connection.on('disconnected', connect);

require('./../models/accounts.js');
require('./../models/groups.js');
require('./../models/groupmembers.js');

var Account 					   	= mongoose.model('Account');
var Group 								= mongoose.model('Group');
var GroupMember						= mongoose.model('GroupMember');

var AID_QUERY_SPEED = 10; //millisecond
var AID_QUERY_CHUNK = 500;
var GLOBAL_AID_INFO_OBJECT = {};
var GLOBAL_FINAL_REPORT_OBJECT = {};

var AID_REPORT = {
	"join_groups"					: 0,
	"join_rcmd_groups"		: 0,
	"create_admin_groups" : 0,

	"join_one_way_groups" : 0,
	"join_two_way_groups"	: 0,

	"join_rcmd_groups_one_way" : 0,
	"join_rcmd_groups_two_way" : 0,

	"create_admin_groups_one_way" : 0,
	"create_admin_groups_two_way" : 0,
};

var WHOLE_DATA_REPORT = {
   "total_signups"         : 0,
   "join_group_after_signup" : 0,
   "join_group_after_signup_one_way" : 0,
   "join_group_after_signup_two_way" : 0,


   "join_rcmd_group_after_signup" : 0,
   "join_rcmd_group_after_signup_one_way" : 0,
   "join_rcmd_group_after_signup_two_way" : 0,

   "create_or_admin_group_after_signup" : 0,
   "create_or_admin_group_after_signup_one_way" : 0,
   "create_or_admin_group_after_signup_two_way" : 0,
};

var init_time = moment().tz(configs.MOMENT_TIMEZONE).subtract(moment.duration(6, 'M')).valueOf();
var end_time = moment().tz(configs.MOMENT_TIMEZONE).valueOf();

init_time = new Date(init_time);
end_time = new Date(end_time);

console.log("Start Date : " + (init_time));
console.log("End Date : " + (end_time));

var chunky = function(arr,groupsize){
    var sets = [], chunks, i = 0;
    chunks = arr.length / groupsize;

    while(i < chunks){
        sets[i] = arr.splice(0,groupsize);
				i++;
    }
	
    return sets;
};

var query_multiple_aids = function(aids,cb) {
	console.log("Going to query next batch of AIDS " + aids.length);

	GroupMember.find({
			aid : {$in:aids},
			act : true,
			gdel: false
		},function(err,gms){
			if(err) { cb(err,null); return; }

			var gids = [];
			gms.forEach(function(gm){
				gids.push(gm["gid"]);
			});

			var gms_by_aid = {};
			gms.forEach(function(gm){
				if(!gms_by_aid[gm["aid"]]) {
					gms_by_aid[gm["aid"]] = [];
				}
				gms_by_aid[gm["aid"]].push(gm);
			});

			Group.find({
				_id: {$in : gids}
			},function(err,groups){
				if(err) { return cb(err,null); }

				groups_by_gid = {};

				groups.forEach(function(grp){
						groups_by_gid[grp["_id"]] = grp;
				});

				for(var indx in aids) {
					var aid = aids[indx];

					if(gms_by_aid[aid]) {
						//this means, this aid joined something
						var gms_for_aid = gms_by_aid[aid];
						GLOBAL_AID_INFO_OBJECT[aid] = fill_report_for_an_aid_with_data(aid,gms_for_aid,groups_by_gid);
					} else {
						//this means this aid did not join anything
						GLOBAL_AID_INFO_OBJECT[aid] = _.clone(AID_REPORT);
					}
				}

				cb(null,"done");
			});
	});
}

var fill_report_for_an_aid_with_data = function(aid,gms_for_aid,groups_by_gid) {
	var my_report = _.clone(AID_REPORT);

	gms_for_aid.forEach(function(gm_for_aid){
		if(gm_for_aid["rcmd"]) {
			my_report["join_rcmd_groups"]++;

			if(groups_by_gid[gm_for_aid["gid"]]["gtyp"]) {
				my_report["join_rcmd_groups_two_way"]++;
			} else {
				my_report["join_rcmd_groups_one_way"]++;
			}
		}

		if(gm_for_aid["type"] == 1) {
			my_report["create_admin_groups"]++;

			if(groups_by_gid[gm_for_aid["gid"]]["gtyp"]) {
				my_report["create_admin_groups_two_way"]++;
			} else {
				my_report["create_admin_groups_one_way"]++;
			}
		}

		if(gm_for_aid["type"] != 1) {
			my_report["join_groups"]++;

			if(groups_by_gid[gm_for_aid["gid"]]["gtyp"]) {
				my_report["join_two_way_groups"]++;
			} else {
				my_report["join_one_way_groups"]++;
			}
		}
	});

	return my_report;
}

var make_collated_report = function(individual_aid_reports) {
	var final_report = _.clone(WHOLE_DATA_REPORT);

	var total_aids = 0;

	var total_join_groups = 0;
	var total_join_one_way_groups = 0;
	var total_join_two_way_groups = 0;

	var total_join_rcmd_groups = 0;
	var total_join_rcmd_groups_one_way = 0;
	var total_join_rcmd_groups_two_way = 0;

	var total_create_admin_groups = 0;
	var total_create_admin_groups_one_way = 0;
	var total_create_admin_groups_two_way = 0;


	for (var aid in individual_aid_reports) {
		var report = individual_aid_reports[aid];
		total_aids++;

		//ToDo :: fix this crap
		if(report["join_groups"] >= 1) {
			total_join_groups++;
		}

		if(report["join_rcmd_groups"] >= 1) {
			total_join_rcmd_groups++;
		}

		if(report["create_admin_groups"] >= 1) {
			total_create_admin_groups++;
		}

		if(report["join_one_way_groups"] >= 1) {
			total_join_one_way_groups++;
		}

		if(report["join_two_way_groups"] >= 1) {
			total_join_two_way_groups++;
		}

		if(report["join_rcmd_groups_one_way"] >= 1) {
			total_join_rcmd_groups_one_way++;
		}

		if(report["join_rcmd_groups_two_way"] >= 1) {
			total_join_rcmd_groups_two_way++;
		}

		if(report["create_admin_groups_one_way"] >= 1) {
			total_create_admin_groups_one_way++;
		}

		if(report["create_admin_groups_two_way"] >= 1) {
			total_create_admin_groups_two_way++;
		}
	}

	final_report["total_signups"] = "100%("+total_aids+")";
	final_report["join_group_after_signup"] = percentage(total_join_groups,total_aids)+" (" + total_join_groups + ")";
	final_report["join_group_after_signup_one_way"] = percentage(total_join_one_way_groups,total_join_groups)+" (" + total_join_one_way_groups + ")";
	final_report["join_group_after_signup_two_way"] = percentage(total_join_two_way_groups,total_join_groups)+" (" + total_join_two_way_groups + ")";


	final_report["join_rcmd_group_after_signup"] = percentage(total_join_rcmd_groups,total_aids)+" (" + total_join_rcmd_groups + ")";
	final_report["join_rcmd_group_after_signup_one_way"] = percentage(total_join_rcmd_groups_one_way,total_join_rcmd_groups)+" (" + total_join_rcmd_groups_one_way + ")";
	final_report["join_rcmd_group_after_signup_two_way"] = percentage(total_join_rcmd_groups_two_way,total_join_rcmd_groups)+" (" + total_join_rcmd_groups_two_way + ")";

	final_report["create_or_admin_group_after_signup"] = percentage(total_create_admin_groups,total_aids)+" (" + total_create_admin_groups + ")";
	final_report["create_or_admin_group_after_signup_one_way"] = percentage(total_create_admin_groups_one_way,total_create_admin_groups)+" (" + total_create_admin_groups_one_way + ")";
	final_report["create_or_admin_group_after_signup_two_way"] = percentage(total_create_admin_groups_two_way,total_create_admin_groups)+" (" + total_create_admin_groups_two_way + ")";

	return final_report;
}

var percentage = function(data,total_data) {
	return Math.floor((100*data/total_data)) + "%";
}

var query_aid_count_sequentially = function(aids,aids_per_loop,cb) {
	var aid_chunks = chunky(aids,aids_per_loop);

	var index = 0;
	var max_index = aid_chunks.length;
	var total_count = 0;

	var cb1 = function(err,result) {
		if(err) {
			cb(err,null);
			return;
		}

		index++;

		if(index >= max_index) {
			cb(null,result);
		} else {
			setTimeout(function(){
				query_multiple_aids(aid_chunks[index],cb1);
			},AID_QUERY_SPEED);
		}
	};


	setTimeout(function(){
		query_multiple_aids(aid_chunks[index],cb1);
	},AID_QUERY_SPEED);
};

Account.find({
		vrfy: true,
		cat : {$gte : init_time,$lte : end_time},
		$or : [
			{ 
				lsnm : {
					$gte : init_time
				}
			},
			{
				lsnw : {
					$gte : init_time
				}
			},
			{
				lsni : {
					$gte : init_time
				}
			},
			{
				lsnwn : {
					$gte : init_time
				}
			}
		]
},{_id:1},function(err,accs){
	if(err) { console.log(err); process.exit(); }

	var aids = [];

	accs.forEach(function(acc){
		aids.push(acc["_id"]);
	});

	console.log("Okay then, we are going to process about " + aids.length + " account ids!");
	console.log("We will be doing this at a query speed of " + AID_QUERY_CHUNK + " per " + AID_QUERY_SPEED + " ms");

	query_aid_count_sequentially(aids,AID_QUERY_CHUNK,function(err,result){
		if(err) { console.log(err); process.exit(); }

		console.log("We are done processing about " + aids.length + " account ids!");
		// console.log(GLOBAL_AID_INFO_OBJECT);

		GLOBAL_FINAL_REPORT_OBJECT = make_collated_report(GLOBAL_AID_INFO_OBJECT);

		console.log(GLOBAL_FINAL_REPORT_OBJECT);
	});
});