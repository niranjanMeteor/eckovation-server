var mongoose                = require('mongoose');
var redis                   = require('redis');
var configs                 = require('./../utility/configs.js');

var redis_client            = redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
  console.trace("Redis Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});
mongoose.connection.on('connected', function(){
  console.log('Successfully Opened MOngoDb Connection');
  startAccountMismatch();
});

require('../models/accounts.js');
require('../models/profiles.js');
var Account               = mongoose.model('Account');
var Profile               = mongoose.model('Profile');

var rmc                   = require("./../utility/redis_mongo_cache.js")(redis_client);
var rhmset                = require("./../utility/redis_hmset.js")(redis_client);

var faultyAccounts        = [];
var toDO                  = 0; 
var done                  = 0;

function startAccountMismatch(){
  Account.find({
    vrfy:true
  },{
    _id : 1,
    m   : 1,
    vrfy: 1
  },function(err,accounts){
    if(err){
      console.trace(err);
      process.exit(0);
    }
    var total_accounts = accounts.length;
    toDO = total_accounts;
    if(total_accounts == 0){
      console.log('No accounts with vrfy as true exists in database');
    }
    console.log('total_accounts with vrfy true from db : '+total_accounts);
    for(var i=0; i<total_accounts;i++){
      checkInRedisRmcCache(accounts[i],function(err,resp){
        if(err){
          console.trace(err);
          process.exit(0);
        }
        done++
        if(resp.uvrfy){
          faultyAccounts.push(resp.account);
        }
        if(done == toDO){
          return printFaultyData();
        }
      });
    }
  });
}

function checkInRedisRmcCache(account, cb){
  rmc.queryById("Account",account._id+'',function(err,rmc_account){
    if(err){
      return cb(err,null);
    }
    if(!rmc_account){
      return cb(null,{uvrfy:false});
    }
    if(rmc_account.vrfy == false || rmc_account.vrfy === false || rmc_account.vrfy == 'false' || rmc_account.vrfy === 'false' || rmc_account.vrfy == null || rmc_account.vrfy == ''){
      return cb(null,{uvrfy:true,account:rmc_account});
    } else {
      return cb(null,{uvrfy:false});
    }
  });
}

function printFaultyData(){
  var num_faulty_data = faultyAccounts.length;
  if(num_faulty_data == 0){
    console.log('No Faulty Data found');
    process.exit(0);
  } else {
    console.log('\n');
    console.log('---------------- Faulty Data Following -----------------');
    console.log('\n');
  }
  for(var j=0; j<num_faulty_data; j++){
    console.log(''+j+'   :  '+JSON.stringify(faultyAccounts[j]));
  }
  console.log('\n');
  process.exit(0);
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : 120
      }
    }
  };
  if(!configs.MONGO_UNAME || configs.MONGO_UNAME == ""){
    return mongoose.connect("mongodb://"+configs.MONGO_HOST+":"+configs.MONGO_PORT+"/"+configs.DB_NAME,options);
  }
  // if(configs.MONGO_UNAME == "eck_user_test"){
  //   return mongoose.connect("mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@"+"127.0.0.1"+":"+configs.MONGO_PORT+"/"+configs.DB_NAME,options);
  // }
  options.replset = {
    rs_name : configs.MONGO_REPLICA_SET_NAME,
    socketOptions : {
      keepAlive : 120
    }
  };
  var connect_string = "mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@";
  var mongo_hosts    = configs.MONGO_HOST;
  var mongo_port     = configs.MONGO_PORT;
  for(var i=0; i<mongo_hosts.length; i++){
    if(i > 0)
      connect_string += ",";
    connect_string += mongo_hosts[i]+":"+mongo_port;
  }
  connect_string += "/"+configs.DB_NAME,options;
  return mongoose.connect(connect_string);
}