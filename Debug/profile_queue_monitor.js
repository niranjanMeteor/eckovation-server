var mongoose            = require('mongoose');
var redis               = require('redis');
var moment					    = require('moment');
var configs             = require('./../utility/configs.js');
var readline 			      = require('readline');

require('../models/profiles.js');
var Profile             = mongoose.model('Profile');

var redis_client        = redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });
redis_client.on("error", function (err) {
    console.log("Error " + err);
});

var rcq                 = require("./../utility/redis_chat_queue.js")(redis_client);
var options             = {server : {socketOptions : {keepAlive : 1}}};

var rL = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

var max_score     = new Date().getTime();

mongoose.connect("mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@"+configs.MONGO_HOST+"/"+configs.DB_NAME,options, function(err){
	if(err) {
		console.log(err);
		console.trace(err);
	} else {
		console.log('Connected to mongodb!');
		console.log('------------------------------------QUEUE MONITOR SCRIPT STARTED------------------------------------')
		console.log('Program will fetch status of queues for recent Profiles Created in steps :)');
		welcomeScriptDialogue();
		startQueueMonitoring(0);
	}
});

function startQueueMonitoring(skip_value){
	rL.question("Enter the number of profiles (between 1 and 200) you wanna scan  or ( 0 to Exit ) : ", function(answer) {
		var limit_of_scan = parseInt(answer);
		if(limit_of_scan == 0) {
			rL.close();
			endQueueMonitorScript();
		}
		scanProfileIdsForQueues(skip_value, limit_of_scan, function(err,resp){
			if(err) { 
				rL.close();
				endQueueMonitorScript();
			}
			if(resp) {
				skip_value = skip_value + limit_of_scan;
				startQueueMonitoring(skip_value);
			} else {
				rL.close();
				endQueueMonitorScript();
			}
		});
	});
}

function scanProfileIdsForQueues(skip_value, limit_of_scan, cb) {
	var total = 0;
	Profile.find({},{},{ 
		sort : { cat : -1 },
		skip : skip_value,
		limit: limit_of_scan
	},function(err, profiles) {
		if(err) { console.trace(err); cb(true,false); return; }
		if(!profiles || profiles.length == 0) { console.log('No More Profiles to scan'); cb(null,false); return; }
		var total_profiles = profiles.length;
		for(var i=0; i<profiles.length; i++){
			countProfileRcq(profiles[i]._id, profiles[i].cat, function(err,resp){
				if(resp) {
					total = total + 1;
					if(total == total_profiles) {
						if(total_profiles < limit_of_scan){
							console.log('--------No Profiles left to Scan , hence script will be terminatd');
							endQueueMonitorScript();
						} else cb(null,true);
					}
				}
			});
		}
	});
}

function countProfileRcq(pid, creation_time, cb) {
	var min_score = new Date(creation_time).getTime();
	rcq.count(pid, min_score, max_score, function(err,queue_count){
		if(err) { 
			console.log('Reporting Error from Redis Rcq operation for fetching queues for Pid : '+pid); 
			cb(null,true);
			return; 
		}
		console.log('Pid : '+pid+'  ,  Queue Count : '+queue_count);
		cb(null,true);
	});
}

function welcomeScriptDialogue(){
	var script_welcome_dialogue = 'ADVICE TO NOTE :: Higher the number of profile to scan you will choose , ';
	script_welcome_dialogue += 'it will utilise system RAM in that order and MongoDB will be also be occupied in that order.';
	script_welcome_dialogue += 'For eficiently running this script without hampering the Node server performance and MongoDB occupancy , choose somethign between 100 to 150';
	console.log(script_welcome_dialogue);
}

function endQueueMonitorScript(){
	console.log('------------------------------------QUEUE MONITOR SCRIPT ENDED------------------------------------')
	process.exit(0);
}
