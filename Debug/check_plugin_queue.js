var mongoose            = require('mongoose');
var readline 			      = require('readline');
var redis               = require('redis');

var configs             = require('./../utility/configs.js');
var RedisPrefixes       = require('./../utility/redis_prefix.js');

var redis_client        = redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
    console.log("Error " + err);
});

var rhmset              = require("./../utility/redis_hmset.js")(redis_client);

require('../models/accounts.js');
require('../models/groups.js');
require('../models/plugins.js');
var Account             = mongoose.model('Account');
var Group 				      = mongoose.model('Group');
var AppPlugin           = mongoose.model('AppPlugin');

var rL = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});
mongoose.connection.on('connected', function(){
  console.log('Connected to mongodb!');
	console.log('------------------------------------PLUGIN GROUP SCRIPT STARTED------------------------------------')
	console.log('Program will give you a chance to view from account queue about plugin objects');
  startPluginStatusCheck();
});

var rhmset                      = require("./../utility/redis_hmset.js")(redis_client);

function startPluginStatusCheck(){
  rL.question("Enter target mobile number : ", function(answer) {
    if(isNaN(answer) && answer.length != 10) {
      console.log('Oops , you entered a wrong user mobile number , Please enter a right option');
      return startPluginStatusCheck();
    }
    var mobile_number = answer.trim();
    Account.findOne({
      m : mobile_number
    },function(err,account){
      if(err){
        console.trace(err);
        return process.exit(0);
      }
      if(!account){
        console.log('Account not found , try a different number');
        return startPluginStatusCheck();
      }
      if(!account.vrfy){
        console.log('Account is not verified yet , try a different number');
        return startPluginStatusCheck();
      }
      aid = account._id+'';
      rhmset.getall(RedisPrefixes.PLUGIN_STATE+aid, function(err,plugin_states){
        if(err){ 
          return cb(err);
        }
        if(!plugin_states){ 
          console.log('******No plugin Messages in Account Plugin Queue***** : '+aid);
          return cb(null); 
        }
        console.log("************** Following Plugin States found for Account Plugin for aid : "+aid+' **********');
        for(var key in plugin_states){
          data = JSON.parse(plugin_states[key]);
          console.log("GID : "+key);
          console.log(JSON.parse(plugin_states[key]));
        }
        console.log('\n');
        console.log('******** Fetching Plugin Status was success, Thanks for using this script, hope to see you soon *********');
        return process.exit(0);
      });
    });
  });
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : 120
      }
    }
  };
  if(!configs.MONGO_UNAME || configs.MONGO_UNAME == ""){
    return mongoose.connect("mongodb://"+configs.MONGO_HOST+":"+configs.MONGO_PORT+"/"+configs.DB_NAME,options);
  }
  options.replset = {
    rs_name : configs.MONGO_REPLICA_SET_NAME,
    socketOptions : {
      keepAlive : 120
    }
  };
  var connect_string = "mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@";
  var mongo_hosts    = configs.MONGO_HOST;
  var mongo_port     = configs.MONGO_PORT;
  for(var i=0; i<mongo_hosts.length; i++){
    if(i > 0)
      connect_string += ",";
    connect_string += mongo_hosts[i]+":"+mongo_port;
  }
  connect_string += "/"+configs.DB_NAME,options;
  return mongoose.connect(connect_string);
}