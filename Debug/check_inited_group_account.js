var moment				= require('moment-timezone');
var mongoose            = require('mongoose');
var _                   = require('underscore');
var readline 			= require('readline');

var configs             = require('./../utility/configs.js');

var connect = function () {
  var options = {server : {socketOptions : {keepAlive : 1}}};
  if(configs.MONGO_UNAME!="") {
    mongoose.connect("mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@"+configs.MONGO_HOST+"/"+configs.DB_NAME,options);
  } else {
    mongoose.connect("mongodb://"+configs.MONGO_HOST+"/"+configs.DB_NAME,options);
  }
}
connect();
mongoose.connection.on('error',console.log);
mongoose.connection.on('disconnected', connect);

var active_initlog_group_users = require('../analytics/activeInitlogGroupUsers.js');

var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.question("Please tell me duration, for inited accs. (eg. x weeks or y days) | group id: ", function(answer) {
	var weeks 	= 0;
	var days 	= 0;

	var time = answer.split("|")[0]; 
	var gid = answer.split("|")[1];

	if(time.indexOf("weeks") > -1) {
		weeks = parseInt(time.split(" ")[0]);
	} else if(time.indexOf("days") > -1) {
		days = parseInt(time.split(" ")[0]);
	}

	var start_time         	= new Date().getTime();
	var end_time         	= new Date().getTime();

	if(weeks > 0) {
		start_time = moment().tz(configs.MOMENT_TIMEZONE).startOf('isoweek').subtract((weeks-1)*7,'day').valueOf();
	} else if (days > 0) {
		start_time = moment().tz(configs.MOMENT_TIMEZONE).subtract(days,'day').valueOf();
	}

	active_initlog_group_users(gid,start_time,end_time,function(err,res){
		if(err) { return console.trace(err); }

		var total_accounts = res.total;
		var active_aids = res.active.length;

		if(active_aids == 1) {
			stuff = "was "+active_aids+" account";
		} else {
			stuff = "were "+active_aids+" accounts";
		}

		var percent = Math.round((active_aids/total_accounts)*100);

		console.log("There " + stuff + " which 'inited' since " + time + " ago untill now. Thats about "+percent+"% of the group strength!");
	});

	rl.close();
});