var mongoose            = require('mongoose');
var configs             = require('../utility/configs.js');
var moment              = require('moment-timezone');


var connect = function () {
  var options = {
    server : {
      socketOptions : {
        keepAlive : 120
      }
    },
    // replset : {
    //   rs_name : configs.MONGO_REPLICA_SET_NAME,
    //   socketOptions : {
    //     keepAlive : 120
    //   }
    // }
  };

  if(configs.MONGO_UNAME != "") {
    // var connect_string = "mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@";
    // var mongo_hosts = configs.MONGO_HOST;
    // var mongo_port = configs.MONGO_PORT;
    // for(var i=0; i<mongo_hosts.length; i++){
    //   if(i > 0)
    //     connect_string += ",";
    //   connect_string += mongo_hosts[i]+":"+mongo_port;
    // }
    // connect_string += "/"+configs.DB_NAME,options;
    // mongoose.connect(connect_string);
    mongoose.connect("mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@"+"127.0.0.1"+":"+configs.MONGO_PORT+"/"+configs.DB_NAME,options);
  } else {
    mongoose.connect("mongodb://"+configs.MONGO_HOST+"/"+configs.DB_NAME,options);
  }
}
connect();
mongoose.connection.on('error',console.log);
mongoose.connection.on('disconnected', connect);

require('../models/groups.js');
require('../models/messages.js');
var Group         = mongoose.model('Group');
var Message         = mongoose.model('Message');

var current_month = moment().tz(configs.TRENDING_GROUPS_TIMEZONE).startOf('month').valueOf()-1;
var janEnd = new Date(moment(current_month).subtract(0, 'months'));//.format();
var janStart = new Date(moment(current_month).subtract(1, 'months')+1);//.format();

var decEnd = janStart;//.format();
var decStart = new Date(moment(current_month).subtract(2, 'months')+1);//.format();

var novEnd = new Date(moment(current_month).subtract(2, 'months'));//.format();
var novStart = new Date(moment(current_month).subtract(3, 'months')+1);//.format();

console.log('Jan start  :  '+janStart);
console.log('Jan end   :  '+janEnd);
console.log('Dec Start   :  '+decStart);
console.log('Dec End    :  '+decEnd);
console.log('Nov Start  :  '+novStart);
console.log('Nov End   :   '+novEnd);

novGroups();
decGroups();
janGroups();


var msgTypes = [Message.MESSAGE_TYPE.audio,Message.MESSAGE_TYPE.text,Message.MESSAGE_TYPE.video,Message.MESSAGE_TYPE.image];
function novGroups(){
  Message.distinct('to',{
    type : { $in : msgTypes },
    cat : {
      $gte : novStart
    },
    cat : {
      $lte : novEnd
    }
  },function(err,novGroups){
    if(err){
      console.trace(err);
      return;
    }
    console.log('Active Grps Till November,2015 : '+novGroups.length);
  });
}

function decGroups(){
  Message.distinct('to',{
    type : { $in : msgTypes },
    cat : {
      $gte : decStart
    },
    cat : {
      $lte : decEnd
    }
  },function(err,decGroups){
    if(err){
      console.trace(err);
      return;
    }
    console.log('Active Grps Till December,2015 : '+decGroups.length);
  });
}

function janGroups(){
  Message.distinct('to',{
    type : { $in : msgTypes },
    cat : {
      $gte : janStart
    },
    cat : {
      $lte : janEnd
    }
  },function(err,janGroups){
    if(err){
      console.trace(err);
      return;
    }
    console.log('Active Grps Till Jan,2016 : '+janGroups.length);
  });
}
