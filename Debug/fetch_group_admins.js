var moment				      = require('moment-timezone');
var mongoose            = require('mongoose');
var _                   = require('underscore');
var readline 			      = require('readline');

var configs             = require('./../utility/configs.js');

var connect = function () {
  var options = {server : {socketOptions : {keepAlive : 1}}};
  if(configs.MONGO_UNAME!="") {
    mongoose.connect("mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@"+configs.MONGO_HOST+"/"+configs.DB_NAME,options);
  } else {
    mongoose.connect("mongodb://"+configs.MONGO_HOST+"/"+configs.DB_NAME,options);
  }
}
connect();
mongoose.connection.on('error',console.log);
mongoose.connection.on('disconnected', connect);

require('./../models/profiles.js');
require('./../models/groups.js');
require('./../models/accounts.js');
require('./../models/groupmembers.js');
require('./../models/messages.js');

var Profile            	 = mongoose.model('Profile');
var GroupMember 			   = mongoose.model('GroupMember');
var Group 					     = mongoose.model('Group');
var Account 					   = mongoose.model('Account');

var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

var countGroupMembers = function(gids,cb) {
	var segg = {};
	GroupMember.find({
		gid 	: {$in : gids},
		act   : true,
		gdel  : false
	},function(err,gms){
		if(err) { return cb(err,null); }

		gms.forEach(function(gm){
			if(!segg[gm["gid"]]) {
				segg[gm["gid"]] = 0;
			}

			segg[gm["gid"]]++;
		});

		cb(null,segg);
	});
};

rl.question("Please tell me duration, for inited accs. (eg. x weeks or y days) : ", function(answer) {
	var weeks 	= 0;
	var days 	= 0;

	if(answer.indexOf("weeks") > -1) {
		weeks = parseInt(answer.split(" ")[0]);
	} else if(answer.indexOf("days") > -1) {
		days = parseInt(answer.split(" ")[0]);
	}

	var start_time         	= new Date().getTime();
	var end_time         	= new Date().getTime();

	if(weeks > 0) {
		start_time = moment().tz(configs.MOMENT_TIMEZONE).startOf('isoweek').subtract((weeks-1)*7,'day').valueOf();
	} else if (days > 0) {
		start_time = moment().tz(configs.MOMENT_TIMEZONE).subtract(days,'day').valueOf();
	}

	var ret = {};

	Group.find({
		cat : {
			$gte : start_time,
			$lt  : end_time,
		},
		act : true
	},function(err,grps){
		var gids = [];

		var grps_by_id = {};

		grps.forEach(function(grp){
			gids.push(grp._id);
			grps_by_id[grp._id] = grp;
		});

		var pids = [];

		GroupMember.find({
			gid 	: {$in : gids},
			type 	: GroupMember.TYPE.ADMIN,
			act   : true,
			gdel  : false
		},function(err,gms){

			gms.forEach(function(gm){
				pids.push(gm.pid);
			});

			Profile.find({
				_id : {$in : pids},
				act : true
			},function(err,prfs){
				var prfs_by_id = {};
				var aids = [];
				prfs.forEach(function(prf){
					prfs_by_id[prf._id] = prf;

					aids.push(prf.aid);
				});

				Account.find({
					_id : {$in : aids}
				},function(err,accs){
					var accs_by_id = {};
					accs.forEach(function(acc){
						accs_by_id[acc._id] = acc;
					});

					gms.forEach(function(gm){
						var v = [grps_by_id[gm.gid]["name"],prfs_by_id[gm.pid]["nam"],accs_by_id[prfs_by_id[gm.pid]["aid"]]["m"]];
						ret[gm.gid] = v;
					});

					countGroupMembers(gids,function(err,gid_m_counts){
						if(err) { console.log("Error occurred!"); return console.log(err);}

						for(var gid in gid_m_counts) {
							var cnt = gid_m_counts[gid];
							ret[gid].push(cnt);
						}

						for(var gid in ret) {
							console.log(ret[gid].join(","));
						}

						process.exit();
					});
				});
			});
		});
	});

	rl.close();
});