var mongoose            = require('mongoose');
var readline 			      = require('readline');
var configs             = require('./../utility/configs.js');
var RcmdGrpsCategory    = require('./../utility/rcmd_groups/category.js');

require('../models/groups.js');
require('../models/groupmembers.js');
require('../models/recommendedgroups.js');

var Group 				      = mongoose.model('Group');
var GroupMember 				= mongoose.model('GroupMember');
var RecommendedGroup 		= mongoose.model('RecommendedGroup');


var rL = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

var options             = {server : {socketOptions : {keepAlive : 1}}};
mongoose.connect("mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@"+configs.MONGO_HOST+"/"+configs.DB_NAME,options, function(err){
	if(err) {
		console.log(err);
		console.trace(err);
	} else {
		console.log('Connected to mongodb!');
		console.log('------------------------------------Recommended Group SCRIPT STARTED------------------------------------')
		console.log('Program will give you a chance to create a new Recommended group or to remove an already existing group from the Recommended groups');
		startRecommendedGroupUpdation();
	}
});

var ADD_NEW_RECOMMENDED_GROUP = 1;
var REMOVE_RECOMMENDED_GROUP  = 2;
var CATEGORIES                = getRcmdGroupsCategories();//['0','1','2','3','4'];

function startRecommendedGroupUpdation(){
	rL.question("Enter your option (0 to Exit, 1 to add, 2 to remove, 3 to View) : ", function(answer) {
		if(isNaN(answer)) {
			console.log('Oops , you entered a wrong option , Please enter a right option');
			return startRecommendedGroupUpdation();
		}
		switch(parseInt(answer)){
			case 1:
				selectCategoryType(function(err,resp){
					if(err){
						return endScript();
					}
					return addNewRecommendedGroup(resp.category);
				});
			break;

			case 2:
				selectCategoryType(function(err,resp){
					if(err){
						return endScript();
					}
					return removeFromRecommendedGroups(resp.category);
				});
			break;

			case 3:
				viewRecommendedGroups(function(err,resp){
					if(err){
						return endScript();
					}
					return startRecommendedGroupUpdation();
				});
			break;

			case 0:
				endScript();
			break;

			default:
				console.log('Oops , you entered a wrong option , Please enter a right option');
				return startRecommendedGroupUpdation();
		}
	});
}

function addNewRecommendedGroup(category){
	rL.question("Enter the group code to add new Recommended group ( Or 0 to Exit ) : ", function(code) {
		if(code == 0 || code == '0')
			endScript();
		if(isNaN(code)) {
			console.log('Oops , you entered a wrong option , Please enter a right option');
			return addNewRecommendedGroup(category);
		}
		Group.findOne({
			code : code,
		},function(err,group){
			if(err) { console.trace(err); return; }
			if(!group) { 
				console.log('Unfortunately , the group code you entered is not valid , Please try a different group code');
				return addNewRecommendedGroup(category);
			}
			if(group.act === false) { 
				console.log('Unfortunately , the group you want to recommend si nto active anymore , Please try a different group code');
				return addNewRecommendedGroup(category);
			}
			if(group.gpic == null || typeof(group.gpic) === 'undefined') { 
				console.log('Unfortunately , the group does not have any group pic , Please try a different group code');
				return addNewRecommendedGroup(category);
			}
			RecommendedGroup.findOne({
				code : code,
				gid  : group._id,
				catg   : category
			},function(err,rcmd_group_existing){
				if(err) { console.trace(err); return; }
				if(rcmd_group_existing) { 
					console.log('Looks Like , the group code you entered is already a Recommended group, you may try a different group code');
					return addNewRecommendedGroup(category);
				}
				getConfirmationFromUser(group, ADD_NEW_RECOMMENDED_GROUP, function(confirm){
					if(confirm == 1){
						new_rcmd_group = new RecommendedGroup({
							gid : group._id,
							code: code,
							catg  : category
						});
						new_rcmd_group.save(function(err, inserted_rcmd_group){
							if(err) { console.trace(err); return; }
							if(!inserted_rcmd_group) {
								console.trace('Recommended group record could not be created'); 
								return endScript(); 
							}
							console.log('Congratulations , you have successfully added a new Recommended Group');
							endScript();
						});
					} else {
						console.log('Oops, Looks Like you were unsure of the group , try a different group code');
						return addNewRecommendedGroup(category);
					}
				});
			});
		});
	});
}

function removeFromRecommendedGroups(category){
	rL.question("Enter the group code to remove a Recommended group ( Or 0 to Exit ) : ", function(code) {
		if(code == 0 || code == '0')
			endScript();
		if(isNaN(code)) {
			console.log('Oops , you entered a wrong option , Please enter a right option');
			return removeFromRecommendedGroups(category);
		}
		Group.findOne({
			code : code
		},function(err,group){
			if(err) { console.trace(err); return; }
			if(!group) { 
				console.log('Unfortunately , the group code you entered is not valid , Please try a different group code');
				return removeFromRecommendedGroups(category);
			}
			RecommendedGroup.findOne({
				code : code,
				gid  : group._id,
				catg   : category
			},function(err,rcmd_group_existing){
				if(err) { console.trace(err); return; }
				if(!rcmd_group_existing) { 
					console.log('Looks Like , the group code you entered is not a privileged group, you may enter a different group code');
					return removeFromRecommendedGroups(category);
				}
				getConfirmationFromUser(group, REMOVE_RECOMMENDED_GROUP, function(confirm){
					if(confirm == 1){
						RecommendedGroup.remove({
							gid : group._id,
							code: code,
							catg  : category
						},function(err, removed_rcmd_group){
							if(err) { console.trace(err); return; }
							if(!removed_rcmd_group) {
								console.trace('Recommended group record could not be removed'); 
								return endScript(); 
							}
							console.log('Congratulations , you have successfully removed a group from Recommended Groups');
							endScript();
						});
					} else {
						console.log('Oops, Looks Like you were unsure of the group , try a different group code');
						return removeFromRecommendedGroups(category);
					}
				});
			});
		});
	});
}

function viewRecommendedGroups(cb){
	selectCategoryType(function(err,resp){
		if(err){
			return cb(true,false);
		}
		return cb(null,resp);
	});
}

function getConfirmationFromUser(group, action, cb){
	if(action == ADD_NEW_RECOMMENDED_GROUP){
		console.log('The Group that you want to add to Recommended Groups is following');
		console.log(group);
		console.log('Are you sure that you wanna add this group into Recommended groups');
	} else if(action == REMOVE_RECOMMENDED_GROUP){
		console.log('The Group that you want to remove from Recommended Groups is following');
		console.log(group);
		console.log('Are you sure that you wanna remove this group from Recommended groups');
	}
	rL.question("Press 1 to confirm , 2 if unsure ( Or 0 to Exit ) : ", function(confirmation) {
		if(confirmation == 0 || confirmation == '0')
			endScript();
		if(isNaN(confirmation)) {
			console.log('Fuck off you Loser , looks like you have to come to waste your and our time');
			endScript();
		}
		return cb(parseInt(confirmation));
	});
}

function selectCategoryType(cb){
	console.log('---------------------------------');
	for(var key in RcmdGrpsCategory){
		console.log('  '+key+'  :  '+RcmdGrpsCategory[key]);
	}
	console.log('\n');
	rL.question("Select your category for above categories (enter the integer values) : ", function(option) {
		if(CATEGORIES.indexOf(option) < 0) {
			console.log('Oops , you entered a wrong option , Please enter a right option');
			return selectCategoryType(cb);
		}
		getAlreadyExistingRecommendedGrps(parseInt(option),function(err,groups){
			if(err) {
				console.trace(err);
				return cb(true,false); 
			}
			if(groups == null){
				console.log('Currently there are no groups in Recommendations for category : '+option);
				return cb(null,{category:parseInt(option)});
			}
			printRecommendedGroupList(parseInt(option), groups);
			return cb(null,{category:parseInt(option)});
		});
	});
}

function getAlreadyExistingRecommendedGrps(category, cb){
	RecommendedGroup.find({
		catg : category
	},function(err,rGroups){
		if(err){
			console.trace(err);
			return cb(true,false);
		}
		if(rGroups.length == 0){
			return cb(null,null);
		}
		var gids = [];
		for(var i=0; i<rGroups.length;i++){
			if(gids.indexOf(rGroups[i].gid) < 0){
				gids.push(rGroups[i].gid);
			}
		}
		Group.find({
			_id : { $in : gids }
		},{
			name : 1,
			code : 1,
			gpic : 1,
			act  : 1,
			gtyp : 1,
			_id  : 1
		},function(err,groups){
			if(err){
				console.trace(err);
				return cb(true,false);
			}
			if(groups.length == 0){
				return cb(null,null);
			}
			getGroupmembersCountForGroups(gids,function(err,gms){
				if(err){
					console.trace(err);
					return cb(true,false);
				}
				for(var j=0; j<groups.length; j++){
					groups[j] = groups[j].toJSON();
					groups[j].mc = gms[groups[j]._id]; 
				}
				return cb(null,groups);
			});
		});
	});
}

function getGroupmembersCountForGroups(gids, cb){
	var total = gids.length;
	var done  = 0;
	var gmsCount = [];
	for(var k=0; k<total; k++){
		getGrpMemCount(gids[k],function(err,countObj){
			if(err){
				console.trace(err);
				return cb(true,false);
			}
			done++;
			gmsCount[countObj.gid] = countObj.count;
			if(done == total){
				return cb(null,gmsCount);
			}
		});
	}
}
function getGrpMemCount(gid, cb){
	GroupMember.count({
		gid : gid,
		act : true,
		gdel: false
	},function(err,count){
		if(err){
			console.trace(err);
			return cb(true,false);
		}
		return cb(null,{gid:gid,count:count});
	});
}

function getRcmdGroupsCategories(){
	var categories = [];
	for(var key in RcmdGrpsCategory){
		categories.push(RcmdGrpsCategory[key]+'');
	}
	return categories;
}

function printRecommendedGroupList(category, groups){
	console.log("\n");
	console.log('For Category : '+category+', Existing Recommended Groups are following')
	console.log('-----------------------------------------------------------------------');
	for(var m=0; m<groups.length; m++){
		var group = groups[m];
		console.log('{ Gname:'+group.name+', code:'+group.code+', act:'+group.act+', members:'+group.mc+', Gtype:'+group.gtyp+' }');
	}
	console.log('-----------------------------------------------------------------------');
	console.log("\n");
	return;
}

function endScript(){
	console.log('----------------------Recommended Group Updation Script ended------------------------');
	process.exit(0);
}

