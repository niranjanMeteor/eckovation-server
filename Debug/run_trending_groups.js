var _            				= require('underscore');
var mongoose            = require('mongoose');
var moment					    = require('moment-timezone');
var configs             = require('./../utility/configs.js');
var fs                  = require('fs');
var readline 			      = require('readline');
var Actions 						= require('../utility/action_names.js');

require('../models/accounts.js');
require('../models/groups.js');
require('../models/groupmembers.js');
require('../models/profiles.js');
require('../models/messages.js');
require('../models/statuses.js');

var Account 				    = mongoose.model('Account');
var Group 				      = mongoose.model('Group');
var GroupMember 				= mongoose.model('GroupMember');
var Profile 						= mongoose.model('Profile');
var Message     				= mongoose.model('Message');
var Status     				  = mongoose.model('Status');

var SIZE_OF_TREND       = configs.TRENDING_GROUPS_SIZE;
var alpha_power   			= configs.TRENDING_GROUPS_ALPHA_COEFFICIENT;
var beta_power    			= configs.TRENDING_GROUPS_BETA_COEFFICIENT;
var duration_array      = [1,2,3,4,5];

exports.runTrendingGroupsAlgorithm = function(input, cb){
	var appWide = {};
	var duration = parseInt(input);
	var startAndEndTime;
	if(duration_array.indexOf(duration) < 0) { 
		console.trace('Sorry , the duration is invalid'); 
		return cb(true, null);
	} else {
		welcomeScriptDialogue();
		startAndEndTime = getStartAndEndTimeForDuration(duration);
		console.log('Duration under operation is as follows');
		console.log(startAndEndTime);
		getAppWideTotalGrpsCount(startAndEndTime, function(err,total_groups_count){
			if(err) { console.trace(err); return cb(true, null); }
			if(!total_groups_count) { console.trace('No groups in this duration'); return cb(true, null); }
			appWide.totalGrpsCount = total_groups_count;
			getAppWideAvgGrpMemSize(appWide, startAndEndTime, function(err,grp_mem_size){
				if(err) { console.trace(err); return cb(true, null); }
				if(!grp_mem_size) { console.trace('No groups in this duration'); return cb(true, null); }
				appWide.avgGroupSize = grp_mem_size;
				if(ifAllAppWideParamsSet(appWide)) {
					scanGroupsForTrendCoefficients(appWide, startAndEndTime, function(err, resp){
						if(err) return cb(true, null);
						if(resp.length == 0) { console.log('No data for Groups Found'); return cb(true,null); }
						return prepareGrpAnalysisReport(appWide, resp, cb);
					});
				}
			});
		});
		getAppWideNewMemsAddedCount(startAndEndTime, function(err,total_new_mems_count){
			if(err) { console.trace(err); return cb(true, null); }
			appWide.newGrpMemsAddedCount = total_new_mems_count;
			if(ifAllAppWideParamsSet(appWide)) {
				scanGroupsForTrendCoefficients(appWide, startAndEndTime, function(err, resp){
					if(err) return cb(true, null);
					if(resp.length == 0) { console.log('No data for Groups Found'); return cb(true,null); }
					return prepareGrpAnalysisReport(appWide, resp, cb);
				});
			}
		});
		getAppWideMemsLeftCount(startAndEndTime, function(err,total_mems_left_count){
			if(err) { console.trace(err); return cb(true, null); }
			appWide.grpMemsLeftCount = total_mems_left_count;
			if(ifAllAppWideParamsSet(appWide)) {
				scanGroupsForTrendCoefficients(appWide,startAndEndTime,function(err, resp){
					if(err) return cb(true, null);
					if(resp.length == 0) { console.log('No data for Groups Found'); return cb(true,null); }
					return prepareGrpAnalysisReport(appWide, resp, cb);
				});
			}
		});
		getAppWideTotalSeenCount(startAndEndTime, function(err,total_seen_counts){
			if(err) { console.trace(err); return cb(true, null); }
			appWide.totalSeenCount = total_seen_counts;
			if(ifAllAppWideParamsSet(appWide)) {
				scanGroupsForTrendCoefficients(appWide,startAndEndTime,function(err, resp){
					if(err) return cb(true, null);
					if(resp.length == 0) { console.log('No data for Groups Found'); return cb(true,null); }
					return prepareGrpAnalysisReport(appWide, resp, cb);
				});
			}
		});
		getAppWideTotalMsgsCount(startAndEndTime, function(err,total_msgs_count){
			if(err) { console.trace(err); return cb(true, null); }
			appWide.totalMsgsCount = total_msgs_count;
			if(ifAllAppWideParamsSet(appWide)) {
				scanGroupsForTrendCoefficients(appWide,startAndEndTime,function(err, resp){
					if(err) return cb(true, null);
					if(resp.length == 0) { console.log('No data for Groups Found'); return cb(true,null); }
					return prepareGrpAnalysisReport(appWide, resp, cb);
				});
			}
		});
	}
}

function ifAllAppWideParamsSet(appWide){
	return ((typeof(appWide.totalGrpsCount) !== "undefined") && (typeof(appWide.avgGroupSize) !== "undefined")
		&& (typeof(appWide.newGrpMemsAddedCount) !== "undefined") && (typeof(appWide.grpMemsLeftCount) !== "undefined") 
		&& (typeof(appWide.totalSeenCount) !== "undefined") && (typeof(appWide.totalMsgsCount) !== "undefined"));
}

function scanGroupsForTrendCoefficients(appWide, startAndEndTime, cb) {
	var total_exec = 0;
	var groups_analytics_data = [];
	Group.find({
		act : true
	},{
		name : 1,
		cat  : 1,
		gtyp : 1
	},function(err, groups){
		if(err) { console.trace(err); return cb(true,null); }
		if(groups.length == 0) { console.trace('No groups found to find any trend'); return cb(true,null); }
		var total_grps_count = groups.length;
		console.log('total groups count : '+total_grps_count);
		for(var i=0; i<total_grps_count; i++) {
			findCoefficientsForGroup(appWide, startAndEndTime, groups[i]._id, groups[i].name, groups[i].gtyp, groups[i].cat,function(err,data){
				if(err) return cb(true,null);
				if(data) {
					groups_analytics_data.push(data);
				}
				total_exec++;
				if(total_exec == total_grps_count)
					return cb(null, groups_analytics_data);
			});
		}
		groups = null;
	}).read('s');
}

function findCoefficientsForGroup(appWide, startAndEndTime, gid, gname, gtyp, createdOn, cb) {
	var age = moment(createdOn).tz(configs.TRENDING_GROUPS_TIMEZONE).fromNow();
	var score_object;
	var group_id = gid+'';
	var data = {
		id   : group_id,
		gnam : gname,
		cat  : age
	};
	if(gtyp){
		data.gtyp = '2way';
	} else {
		data.gtyp = '1way';
	}
	findSizeCoefficient(appWide, startAndEndTime, group_id, function(err,resp){
		if(err) return cb(true,null);;
		data.Xg = resp.size_coefficient;
		data.group_size = resp.group_size;
		data.active_users = resp.active_users;
		if((typeof(data.Xg) !== "undefined") && (typeof(data.Yg) !== "undefined") && (typeof(data.Zg) !== "undefined")) {
			score_object = calculateScoreForGroup(data);
			data.alpha_coefficient = score_object.alpha_coefficient;
			data.beta_coefficient  = score_object.beta_coefficient;
			data.score             = score_object.score;
			return cb(null, data);
		}
	});
	findGrowthCoefficient(appWide, startAndEndTime, group_id, function(err,resp){
		if(err) return cb(true,null);;
		data.Yg = resp.growth_coefficient;
		data.group_new_mems = resp.new_mems;
		data.group_left_mems = resp.left_mems;
		if((typeof(data.Xg) !== "undefined") && (typeof(data.Yg) !== "undefined") && (typeof(data.Zg) !== "undefined")) {
			score_object = calculateScoreForGroup(data);
			data.alpha_coefficient = score_object.alpha_coefficient;
			data.beta_coefficient  = score_object.beta_coefficient;
			data.score             = score_object.score;
			return cb(null, data);
		}
	});
	findEngagementCoefficient(appWide, startAndEndTime, group_id, function(err,resp){
		if(err) return cb(true,null);;
		data.Zg = resp.engagement_coefficient;
		data.grp_total_seens = resp.grp_seens;
		data.grp_total_messages = resp.grp_msgs_posted;
		data.grp_total_dlvry = resp.grp_dlvry;
		if((typeof(data.Xg) !== "undefined") && (typeof(data.Yg) !== "undefined") && (typeof(data.Zg) !== "undefined")) {
			score_object = calculateScoreForGroup(data);
			data.alpha_coefficient = score_object.alpha_coefficient;
			data.beta_coefficient  = score_object.beta_coefficient;
			data.score             = score_object.score;
			return cb(null, data);
		}
	});
}

function calculateScoreForGroup(data){
	var alpha_coefficient = powerFunction( (data.Xg * data.Yg), alpha_power);
	var beta_coefficient  = powerFunction( (data.Xg * data.Zg), beta_power);
	var score = alpha_coefficient + beta_coefficient;
	return {alpha_coefficient : alpha_coefficient, beta_coefficient : beta_coefficient, score : score};
}

function powerFunction(input, power){
	if(power == 1) return input;
	if(power == 0) return 1;
	return Math.pow(input, power);
}

function findSizeCoefficient(appWide, startAndEndTime, group_id, cb) {
	var size_coefficient;
	GroupMember.count({
		gid : group_id,
		act : true,
		gdel: false
	},function(err, group_size){
		if(err) { console.trace(err); return cb(true,null); }
		var query1 = GroupMember.distinct('pid',{
			gid : group_id,
			act : true,
			gdel: false
		});

		var cb1 = function(err, pids){
			if(err){ console.trace(err);return cb(true,null); }

			var query2 = Profile.distinct('aid',{
				_id : { $in : pids },
				act : true
			});

			var cb2 = function(err, aids){
				if(err){ console.trace(err);return cb(true,null); }
				pids = null;
				Account.count({
					_id : { $in : aids },
					$or : [
						{
							lsnm : {
								$gte : startAndEndTime.start_time,
								$lte : startAndEndTime.end_time
							}
						},
						{
							lsnw : {
								$gte : startAndEndTime.start_time,
								$lte : startAndEndTime.end_time
							}
						},
						{
							lsni : {
								$gte : startAndEndTime.start_time,
								$lte : startAndEndTime.end_time
							}
						}
					]
				},function(err, active_users){
					if(err){ console.trace(err);return cb(true,null); }
					aids = null;
					return cb(null, {group_size : group_size, active_users : active_users, size_coefficient : 1});
				}).read('s');
			};

			query2.read('s').exec(cb2);
		};

		query1.read('s').exec(cb1);
		//size_coefficient = calculateSizeCoefficient(appWide, group_size);
		// cb(null, {group_size : group_size, size_coefficient : 1});
	}).read('s');
}

function findGrowthCoefficient(appWide, startAndEndTime, group_id, cb) {
	var growth_coefficient;
	GroupMember.count({
		gid : group_id,
		act : true,
		gdel: false,
		cat : {
			$gte : startAndEndTime.start_time,
			$lte : startAndEndTime.end_time
		}
	},function(err,total_new_mems){
		if(err) { console.trace(err); return cb(true,null); }

		var query2 = Message.distinct('from',{
			to : group_id,
			'actn.t' : Actions.GROUP_LEAVE,
			cat : {
				$gte : startAndEndTime.start_time,
				$lte : startAndEndTime.end_time
			}
		});

		var cb2 = function(err,total_left){
			if(err) { console.trace(err); return cb(true,null); }
			var total_left_count = total_left.length;
			total_left = null;
			growth_coefficient = calculateGrowthCoefficient(appWide, total_new_mems, total_left_count);
			return cb(null, {new_mems : total_new_mems, left_mems : total_left_count, growth_coefficient : growth_coefficient});
		};

		query2.read('s').exec(cb2);
	}).read('s');
}

function findEngagementCoefficient(appWide, startAndEndTime, group_id, cb) {
	var engagement_coefficient;
	var query1 = Message.distinct('_id',{
		to : group_id,
		type : {$in : [Message.MESSAGE_TYPE.text,Message.MESSAGE_TYPE.audio,Message.MESSAGE_TYPE.video,Message.MESSAGE_TYPE.image]},
		cat : {
			$gte : startAndEndTime.start_time,
			$lte : startAndEndTime.end_time
		}
	});

	var cb1 = function(err,messages){
		if(err) { console.trace(err); return cb(true,null); }
		var total_messages = messages.length;
		if(total_messages == 0){
			return cb(null, {grp_msgs_posted : 0, grp_dlvry : 0, grp_seens : 0, engagement_coefficient : 0});
		}
		Status.count({
			mid : {$in : messages},
			stim: { $exists : true },
		},function(err,seen_count){
			if(err) { console.trace(err); return cb(true,null); }
			Status.count({
				mid : {$in : messages},
				rtim: { $exists : true },
			},function(err,dlvry_count){
				if(err) { console.trace(err); return cb(true,null); }
				messages = null;
				engagement_coefficient = calculateEngagementCoefficient(appWide, seen_count, total_messages);
				return cb(null, {
					grp_msgs_posted : total_messages, 
					grp_dlvry : dlvry_count, 
					grp_seens : seen_count, 
					engagement_coefficient : engagement_coefficient
				});
			}).read('s');
		}).read('s');
	};

	query1.read('s').exec(cb1);
}

function calculateSizeCoefficient(appWide, group_size) {
	if(appWide.avgGroupSize == 0) return 0;
	return (group_size / appWide.avgGroupSize);
}

function calculateGrowthCoefficient(appWide, total_new_mems, total_mems_left) {
	var grpParameter = total_new_mems - total_mems_left;
	var appParameter = appWide.newGrpMemsAddedCount - appWide.grpMemsLeftCount;
	if(appParameter == 0) return 0;
	return ( grpParameter / appParameter );
}

function calculateEngagementCoefficient(appWide, total_seens, total_messages_posted) {
	if(total_messages_posted == 0 || appWide.totalMsgsCount == 0 || appWide.totalSeenCount == 0) return 0;
	var grpParameter = (total_seens / total_messages_posted);
	var appParameter = (appWide.totalSeenCount / appWide.totalMsgsCount);
	return ( grpParameter / appParameter );
}

function getAppWideTotalGrpsCount(startAndEndTime, cb) {
	Group.count({
		act : true,
		cat : {
			//$gte : startAndEndTime.start_time,
			$lte : startAndEndTime.end_time
		}
	},function(err,total_groups_count){
		if(err) {console.trace(err); return cb(true,null); }
		return cb(null,total_groups_count);
	}).read('s');
}

function getAppWideAvgGrpMemSize(appWide, startAndEndTime, cb) {
	if(appWide.totalGrpsCount == 0) return cb(null, 0);
	GroupMember.count({
		gdel: false,
		act : true,
		cat : {
			//$gte : startAndEndTime.start_time,
			$lte : startAndEndTime.end_time
		}
	},function(err, members_count){
		if(err) {console.trace(err); return cb(true,null); }
		avgGroupSize = (members_count / appWide.totalGrpsCount);
		return cb(null, avgGroupSize);
	}).read('s');
}

function getAppWideNewMemsAddedCount(startAndEndTime, cb) {
	GroupMember.count({
		gdel : false,
		act  : true,
		cat  : {
			$gte : startAndEndTime.start_time,
			$lte : startAndEndTime.end_time
		}
	},function(err, newMemsAddedCount){
		if(err) {console.trace(err); return cb(true,null); }
		return cb(null, newMemsAddedCount);
	}).read('s');
}

function getAppWideMemsLeftCount(startAndEndTime, cb) {
	Message.count({
		type : Message.MESSAGE_TYPE.notification,
		'actn.t' : Actions.GROUP_LEAVE,
		cat : {
			$gte : startAndEndTime.start_time,
			$lte : startAndEndTime.end_time
		}
	},function(err, memsLeftCount){
		if(err) {console.trace(err); return cb(true,null); }
		return cb(null, memsLeftCount);
	}).read('s');
}

function getAppWideTotalSeenCount(startAndEndTime, cb) {
	Status.count({
		stim : {$exists : true},
		cat : {
			//$gte : startAndEndTime.start_time,
			$lte : startAndEndTime.end_time
		}
	},function(err,total_seen_counts){
		if(err) { console.trace(err); return cb(true,null); }
		return cb(null, total_seen_counts);
	}).read('s');
}

function getAppWideTotalMsgsCount(startAndEndTime, cb) {
	Message.count({
		type : {$in : [Message.MESSAGE_TYPE.text,Message.MESSAGE_TYPE.audio,Message.MESSAGE_TYPE.video,Message.MESSAGE_TYPE.image]},
		cat : {
			//$gte : startAndEndTime.start_time,
			$lte : startAndEndTime.end_time
		}
	},function(err,total_msg_counts){
		if(err) { console.trace(err); return cb(true,null); }
		return cb(null, total_msg_counts);
	}).read('s');
}

function getStartAndEndTimeForDuration(duration) {
	var current_time = moment().tz(configs.TRENDING_GROUPS_TIMEZONE).valueOf();
	var startAndEndTime = {
		end_time : current_time
	};
	switch(duration){
		case 1:
			startAndEndTime.start_time = moment().tz(configs.TRENDING_GROUPS_TIMEZONE).startOf('day').valueOf();//.format();
		break;

		case 2:
			startAndEndTime.start_time = moment().tz(configs.TRENDING_GROUPS_TIMEZONE).startOf('week').valueOf();//.format();
		break;

		case 3:
			var current_week = moment().tz(configs.TRENDING_GROUPS_TIMEZONE).startOf('week').valueOf();
			startAndEndTime.start_time = moment(current_week).subtract(1, 'weeks');//.format();
			startAndEndTime.end_time = moment(current_week-1);//.format();
		break;

		case 4:
			startAndEndTime.start_time = moment().tz(configs.TRENDING_GROUPS_TIMEZONE).startOf('month').valueOf();//.format();
		break;

		case 5:
			var current_month = moment().tz(configs.TRENDING_GROUPS_TIMEZONE).startOf('month').valueOf();
			startAndEndTime.start_time = moment(current_month).subtract(1, 'months');//.format();
			startAndEndTime.end_time = moment(current_month-1);//.format();
		break;
	}
	startAndEndTime.start_time = new Date(startAndEndTime.start_time);
	startAndEndTime.end_time = new Date(startAndEndTime.end_time);
	return startAndEndTime;
}

function prepareGrpAnalysisReport(appWide, groups_analytics_data, cb) {
	groups_analytics_data.sort(function(a, b){
	 return b.score-a.score;
	});
	var final_group_trend;
	if(groups_analytics_data.length > SIZE_OF_TREND){
		final_group_trend = groups_analytics_data.splice(0,SIZE_OF_TREND);
	} else {
		final_group_trend = groups_analytics_data;
	}
	groups_analytics_data = null;
	var num_of_trending_grps = final_group_trend.length;
	for(var m=0; m<num_of_trending_grps; m++){
		if((final_group_trend[m].score == 0.0)){
			final_group_trend = final_group_trend.splice(0,m);
			break;
		}
	}
	if(num_of_trending_grps == 0) {
		console.log('No Groups FOund Trending');
		return cb(null,null);
	}
	var grp_admin_pids, grp_gid_pids_admins ,grp_distinct_mems, grp_profile_names;
	var toDo = 2;
	var done = 0;
	getAllGroupAdminIds(final_group_trend,function(err,group_and_admins){
		if(err){ 
			return cb(true, null); 
		}
		grp_gid_pids_admins = group_and_admins.gidAdminPids;
		grp_admin_pids    = group_and_admins.adminPids;
		getGroupAdminNames(grp_admin_pids, function(err, profile_names){
			if(err){
				return cb(true, null);
			}
			done++;
			grp_profile_names = profile_names;
			if(done == toDo){
				return buildFinalTrendingGroupData(appWide, final_group_trend, grp_profile_names, grp_gid_pids_admins, grp_distinct_mems, cb);
			}
		});
	});
	getDistinctMemsCountForAllGrps(final_group_trend,function(err,distinct_mems){
		if(err){ 
			return cb(true, null); 
		}
		done++;
		grp_distinct_mems = distinct_mems
		if(done == toDo){
			return buildFinalTrendingGroupData(appWide, final_group_trend, grp_profile_names, grp_gid_pids_admins, grp_distinct_mems, cb);
		}
	});
}

function buildFinalTrendingGroupData(appWide, final_group_trend, profile_names, grp_gid_pid_admin, grp_distinct_mems, cb){
	console.log('--------------------App Wide AVG Data--------------------');
	console.log(appWide);
	var num_of_trending_grps = final_group_trend.length;
	for(var k=0; k<num_of_trending_grps; k++){
		final_group_trend[k].score = final_group_trend[k].score.toFixed(3);
		final_group_trend[k].Xg    = final_group_trend[k].Xg.toFixed(3);
		final_group_trend[k].Yg    = final_group_trend[k].Yg.toFixed(3);
		final_group_trend[k].Zg    = final_group_trend[k].Zg.toFixed(3);


		var names = [];
		var all_admins = grp_gid_pid_admin[final_group_trend[k].id];
		for(var i = 0; i < all_admins.length; i++) {
			names.push(profile_names[all_admins[i]]);
		}
		final_group_trend[k].admin_name        = names.join(",");
		final_group_trend[k].distinct_accounts = grp_distinct_mems[final_group_trend[k].id];

		final_group_trend[k].act_users_percent = calculateActiveUsersPercent(final_group_trend[k].active_users, final_group_trend[k].group_size);
		final_group_trend[k].avg_seen_percent  = calculateAvgSeenPercent(final_group_trend[k].grp_total_messages, final_group_trend[k].grp_total_seens, final_group_trend[k].group_size);
		final_group_trend[k].avg_dlvry_percent = calculateAvgDlvryPercent(final_group_trend[k].grp_total_messages, final_group_trend[k].grp_total_dlvry, final_group_trend[k].group_size);
	}
	console.log('Output trending groups list');
	console.log(final_group_trend);
	endTrendingScript();
	return cb(null, final_group_trend);
}

function getAllGroupAdminIds(final_group_trend, cb){
	var num_of_trending_grps = final_group_trend.length;
	var done = 0;
	var gid_and_admin_pid = [];
	var admin_pids        = [];
	for(var i=0; i<num_of_trending_grps; i++){
		getTheOldestAdminsActiveInGroup(final_group_trend[i].id, function(err, resp){
			if(err){ 
				return cb(true, null); 
			}
			done++;
			gid_and_admin_pid[resp.gid] = resp.pids;
			admin_pids = _.union(admin_pids,resp.pids);

			if(done == num_of_trending_grps){
				return cb(null,{gidAdminPids:gid_and_admin_pid,adminPids:admin_pids});
			}
		});
	}
}

function getTheOldestAdminsActiveInGroup(gid, cb){
	GroupMember.find({
		gid : gid,
		act : true,
		gdel: false,
		type: GroupMember.TYPE.ADMIN
	}, function(err,admins) {
		if(err) { console.trace(err); return cb(true,null); }
		if(!admins) { console.trace('Admin Not Found for group ID : '+gid); return cb(true,null); }

		var pids = [];
		admins.forEach(function(admin){
			pids.push(admin.pid+'');
		});

		return cb(null,{gid : gid, pids : pids});
	});
}

function getTheOldestAdminActiveInGroup(gid, cb){
	GroupMember.findOne({
		gid : gid,
		act : true,
		gdel: false,
		type: GroupMember.TYPE.ADMIN
	}, function(err,oldest_admin) {
		if(err){ 
			console.trace(err); 
			return cb(true,null); 
		}
		if(!oldest_admin){ 
			console.trace('Admin Not Found for group ID : '+gid); 
			return cb(true,null); 
		}
		return cb(null,{gid : gid, pid : oldest_admin.pid+''});
	});
}

function getGroupAdminNames(pids, cb) {
	Profile.find({
		_id : { $in : pids }
	},function(err, oldest_admin_profiles){
		if(err){ 
			return cb(true, null); 
		}
		var oldest_admin_names = [];
		for(var i=0; i<oldest_admin_profiles.length; i++){
			oldest_admin_names[oldest_admin_profiles[i]._id] = oldest_admin_profiles[i].nam;
		}
		return cb(null, oldest_admin_names);
	});
}

function getDistinctMemsCountForAllGrps(final_group_trend, cb){
	var num_of_trending_grps = final_group_trend.length;
	var done = 0;
	var group_distinct_mems = [];
	for(var i=0; i<num_of_trending_grps; i++){
		getDistinctMemsCountForGroup(final_group_trend[i].id, function(err, resp){
			if(err){ 
				return cb(true, null); 
			}
			done++;
			group_distinct_mems[resp.gid] = resp.count;
			if(done == num_of_trending_grps){
				return cb(null,group_distinct_mems);
			}
		});
	}
}

function getDistinctMemsCountForGroup(gid, cb){
	GroupMember.distinct('aid',{
		gid : gid,
		act : true,
		gdel: false
	},function(err,grpMems){
		if(err){
			return cb(err,null);
		}
		return cb(null,{gid:gid,count:grpMems.length});
	});
}

function calculateAvgSeenPercent(msgs_count, seen_count, size){
	if(msgs_count == 0 || seen_count == 0 || size == 0){
		return 0;
	} else {
		var seen_per_msg = seen_count / msgs_count;
		return (100 *(seen_per_msg / size)).toFixed(2);
	}
}

function calculateAvgDlvryPercent(msgs_count, dlvry_count, size){
	if(msgs_count == 0 || dlvry_count == 0 || size == 0){
		return 0;
	} else {
		var dlvry_per_msg = dlvry_count / msgs_count;
		return (100 *(dlvry_per_msg / size)).toFixed(2);
	}
}

function calculateActiveUsersPercent(active_users, size){
	if(active_users == 0 || size == 0){
		return 0;
	} else {
		return (100 * (active_users / size)).toFixed(2);
	}
}

function welcomeScriptDialogue(){
	console.log('------------------------------------TRENDING GROUPS SCRIPT STARTED------------------------------------')
	console.log('Program will show which groups are trending in the specified duration');
}

function endTrendingScript(){
	console.log('------------------------------------TRENDING GROUPS SCRIPT ENDED------------------------------------')
}
