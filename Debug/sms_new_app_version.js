var app           			= require('express')();
var mongoose 						= require('mongoose');
var http 							  = require('http');
var configs           = require('./../utility/configs.js');
var SmsEngine           = require('./../utility/sms_engines.js');
var SmsEngineConfigs    = require('./../utility/sms_engines_config.js');
var mongoose            = require('mongoose');
var https               = require('https');
var readline 			      = require('readline');

var PROTOCOL 					  = "http";
var httpUrl             = "http";
var httpsUrl            = "https";

require('./../models/accounts.js');
var Account 					= mongoose.model('Account');

var AppVersionToCompare = "1.15";
var sms_engines 			= [SmsEngine.MSG_91];
var upgradeMessage    = "Thanks for using Eckovation. Upgrade the app now to experience new features. https://play.google.com/store/apps/details?id=com.eckovation";

var options = {server : {socketOptions : {keepAlive : 1}}};
mongoose.connect("mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@"+configs.MONGO_HOST+"/"+configs.DB_NAME,options, function(err){
	if(err) {
		console.trace(err);
		console.trace(err);
	} else {
		console.log('Connected to mongodb!');
		sendSmsToAllInactiveAndOldAppUsers();
	}
});

var rL = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

function sendSmsToAllInactiveAndOldAppUsers(){
	var totalUserToUpgrade = 0;
	var mobilesToUpgrade = [];
	Account.find({},function(err,app_accounts){
		if(err) { console.trace(err); return; }
		if(app_accounts.length == 0) { console.trace('No accounts found in the app'); return; }
		for(var i=0; i<app_accounts.length; i++){
			if(app_accounts[i].lsnm && ifAppVersionForUpgrade(app_accounts[i].vrsn)){
				totalUserToUpgrade++;
				mobilesToUpgrade.push(app_accounts[i].m);
			}
		}
		if(mobilesToUpgrade.length > 0){
			console.log('There are a Total of '+totalUserToUpgrade+' users to upgrade their app');
		} else {
			console.log('No Users Require to upgrade their app');
			process.exit(0);
		}
		rL.question("Do you wanna send sms to all "+totalUserToUpgrade+" users : ", function(action){
			var reaction = action.toUpperCase();
			if(reaction == "YES") {
				for(var j=0; j<mobilesToUpgrade.length; j++){
					sendSmsAboutAppUpgrade(mobilesToUpgrade[j]);
				}
			} else{
				console.log("You Rejected to notify Users about app upgrade");
			}
		});
	});
}

function ifAppVersionForUpgrade(version){
	var required   = AppVersionToCompare.split(".");
	var components = version.split(".");

	if( (components.length < 2) || (isNaN(components[0])) ) return true;

  if ( (parseInt(components[0]) <= parseInt(required[0])) && 
  			(parseInt(components[1]) < parseInt(required[1])) ) {
		return true;
  } else return false;
}



function sendSmsAboutAppUpgrade(phone) {
	//http://smsalertbox.com/api/sms.php?uid=616b73686174676f656c&pin=52d3b166edf7f&sender=ECKEDU&route=5&tempid=2&mobile=9800125211&message=Your%20OTP%20is%20100.%20Please%20enter%20it%20in%20your%20app.%20Thanks%20for%20downloading%20Eckovation.&pushid=1
	//https://control.msg91.com/api/sendhttp.php?authkey=YourAuthKey&mobiles=919999999990,919999999999&message=message&sender=ECKVTN&route=1&country=0
	for(var i=0; i<sms_engines.length; i++){
		var urlAndProtocol = getSmsSendingUrl(sms_engines[i], phone);
		//console.log('Sms Sending url :');
		//console.log(urlAndProtocol);
		callSmsUrl(phone, urlAndProtocol, sms_engines[i]);
	}
}

function getSmsSendingUrl(engine, phone){
	var params          = {};
	var smsConfigs      = SmsEngineConfigs(engine);
	var HOST            = smsConfigs.HOST;
	var API_PATH        = smsConfigs.SEND_API_PATH;
	var PROTOCOL        = smsConfigs.PROTOCOL;

	params.sender       = smsConfigs.SENDER_ID;
	params.route        = smsConfigs.ROUTE;
	params.message      = upgradeMessage;

	switch(engine){
		case SmsEngine.MSG_91:
			params.mobiles  = phone;
			params.authkey  = smsConfigs.AUTH_KEY;
			params.country  = 91;
			params.response = "json";
		break;

		case SmsEngine.ALERTBOX:
			params.mobile   = phone;
			params.uid      = smsConfigs.USERNAME;
			params.pin      = smsConfigs.PASSWORD;
			params.tempid   = smsConfigs.TEMPID;
			params.pushid   = smsConfigs.PUSHID;
		break;
	}
	params["message"]   = encodeURIComponent(params["message"]);

	var tmp = new Array();
	for(var key in params) {
		tmp.push(key+"="+params[key]);
	}
	var query_string = tmp.join("&");
	var url = PROTOCOL+"://"+HOST+API_PATH+"?"+query_string;

	return {protocol : PROTOCOL, smsUrl : url};
}

function callSmsUrl(phone, urlAndProtocol, engine){
	if(urlAndProtocol.protocol == httpUrl){
		http.get(urlAndProtocol.smsUrl,function(response){
			response.setEncoding('utf8');
			response.on("data",function(data) {
				if(data){
					if(checkResponseStatus(data,engine)){
						console.log('http sms sending was successfull with engine :'+engine+' for Mobile Num : '+phone);
					} else {
						console.log('sms sending was unsuccessfull for engine :'+engine+' for Mobile Num : '+phone);
					}
				} else {
					console.log('Response Data not found for sms sending with engine :'+engine+' for Mobile Num : '+phone);
				}
			});
		});
	} else if(urlAndProtocol.protocol == httpsUrl){
		https.get(urlAndProtocol.smsUrl,function(response){
			response.setEncoding('utf8');
			response.on("data",function(data) {
				if(data){
					data = JSON.parse(data);
					if(checkResponseStatus(data,engine)){
						console.log('sms sending was successfull for engine :'+engine+' for Mobile Num : '+phone);
					} else {
						console.log('sms sending was unsuccessfull for engine :'+engine+' for Mobile Num : '+phone);
					}
				} else {
					console.log('Response Data not found for sms sending with engine :'+engine+' for Mobile Num : '+phone);
				}
			});
		});
	}
}

function checkResponseStatus(response,engine){
	switch(engine){
		case SmsEngine.MSG_91:
			if(response.type == "success")
				return true;
			else return false;

		case SmsEngine.ALERTBOX:
			var regex = /^[0-9]+$/;
			return regex.test(response);
	}
}