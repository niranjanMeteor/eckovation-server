var socketio            = require('socket.io');
var mongoose            = require('mongoose');
var validator           = require('validator');
var redis               = require('redis');
var _                   = require('underscore');
var readline 			= require('readline');
var tokenValidator      = require('./../utility/token_validator.js');
var configs             = require('./../utility/configs.js');

var connect = function () {
  var options = {server : {socketOptions : {keepAlive : 1}}};
  if(configs.MONGO_UNAME!="") {
    mongoose.connect("mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@"+configs.MONGO_HOST+"/"+configs.DB_NAME,options);
  } else {
    mongoose.connect("mongodb://"+configs.MONGO_HOST+"/"+configs.DB_NAME,options);
  }
}
connect();
mongoose.connection.on('error',console.log);
mongoose.connection.on('disconnected', connect);

var redis_client        = redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
    console.log("Error " + err);
});

require('../models/profiles.js');
require('../models/accounts.js');
require('../models/messages.js');

var Account             = mongoose.model('Account');
var Profile             = mongoose.model('Profile');
var Message             = mongoose.model('Message');

var SOCKET_PREFIX       = "s";
var S_COUNT_PREFIX      = "seen_count";

var rmc                 = require("./../utility/redis_mongo_cache.js")(redis_client);
var rcq                 = require("./../utility/redis_chat_queue.js")(redis_client);
var rsk                 = require("./../utility/redis_socket_key.js")(redis_client,SOCKET_PREFIX);
var rseencount          = require("./../utility/redis_key_store.js")(redis_client,S_COUNT_PREFIX);
var rcustomq            = require("./../utility/redis_custom_queue.js")(redis_client);

var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.question("Please enter the mobile number queue investigate: ", function(mobile) {
	Account.findOne({
		m : mobile
	},function(err,acc) {
		if(err || !acc) { console.log("That was an invalid phone number! Exiting!"); process.exit(); return; }

		Profile.find({
			aid : acc._id
		},function(err,prfs) {
			if(err) { console.log("No profiles exist for this phone number! Exiting!"); process.exit(); return; }

			var inactive_prfs = 0;
			prfs.forEach(function(prf){
				if(!prf.act) {
					inactive_prfs++;
				}
			});

			console.log("There are " + prfs.length + " profiles with (" + (inactive_prfs) + "[inact]," + (prfs.length - inactive_prfs) + "[act]) profiles");

			scan_profile_queues(prfs,function(){
				dig_deeper();
			});
		});
	});
});

var dig_deeper = function() {
	rl.question("Want to dig in deeper into a profile?: ", function(pid) {
		scan_profile_queue(pid,function(){
			dig_deeper();
		});
	});
};

var scan_profile_queue = function(pid,cb) {

	rcq.getall(pid,function(err,msgs){
		if(err) {
			console.trace(err);
			return;
		}
		var mids = [];
		for(var i = 0; i < msgs.length; i++) {
			mids.push(msgs[i]);
		}

		Profile.find({
			_id : pid
		},function(err,prf){
			if(err) {
				console.log(err);
				return;
			}

			console.log("The profile looks like below: ");
			console.log(prf);

			Message.find({
				_id : {$in : mids}
			},function(err,res){
				if(err) {
					console.trace(err);
					return;
				}

				console.log("The message output is as follows: ");
				console.log(JSON.parse(JSON.stringify(res)));
				console.log("\r\r\r\r");

				cb();
			});

		})

	});
};

var scan_profile_queues = function(prfs,cb) {
	var indx = 0;

	prfs.forEach(function(prf){
		rcq.getall(prf._id,function(err,profile_msgs){
			if(err) {
				console.trace(err);
				return;
			}

			var count = profile_msgs.length;
			console.log("There are about "+ count + " messages in the "+ prf.nam + " ("+ prf._id +")"+ " user's queue");

			indx++;
			if(indx == prfs.length) {
				cb();
			}
		});
	});	
};
