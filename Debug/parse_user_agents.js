var readline 			      = require('readline');
var fs 									= require('fs');

var stream = fs.createReadStream('/home/niranjan/Desktop/useragents/agents.log');
var rl = readline.createInterface({
  input: stream,
  output: process.stdout,
  terminal :false
});

var AllUserAgents       = [];

rl.on('line', function(line){
  var lineContent = line.split("user_agent")[1].split(":")[1].split('"')[1];
  if(AllUserAgents.indexOf(lineContent) < 0)
  	AllUserAgents.push(lineContent);
}).on('close',function(){
	printAgents();
});

var printAgents = function(){
	// console.log('-----------------job completed-----------------');
	console.log(AllUserAgents);
}