var moment				      = require('moment-timezone');
var mongoose            = require('mongoose');
var _                   = require('underscore');
var readline 			      = require('readline');

var configs             = require('./../utility/configs.js');

var connect = function () {
  var options = {server : {socketOptions : {keepAlive : 1}}};
  if(configs.MONGO_UNAME!="") {
    mongoose.connect("mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@"+configs.MONGO_HOST+"/"+configs.DB_NAME,options);
  } else {
    mongoose.connect("mongodb://"+configs.MONGO_HOST+"/"+configs.DB_NAME,options);
  }
}
connect();
mongoose.connection.on('error',console.log);
mongoose.connection.on('disconnected', connect);

var active_initlog_users = require('../analytics/activeInitlogUsers.js');

var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.question("Please tell me duration, for inited accs. (eg. x weeks or y days) : ", function(answer) {
	var weeks 	= 0;
	var days 	= 0;

	if(answer.indexOf("weeks") > -1) {
		weeks = parseInt(answer.split(" ")[0]);
	} else if(answer.indexOf("days") > -1) {
		days = parseInt(answer.split(" ")[0]);
	}

	var start_time         = new Date().getTime();
	var end_time         	 = new Date().getTime();

	if(weeks > 0) {
		start_time = moment().tz(configs.MOMENT_TIMEZONE).startOf('isoweek').subtract((weeks-1)*7,'day').valueOf();
	} else if (days > 0) {
		start_time = moment().tz(configs.MOMENT_TIMEZONE).subtract(days,'day').valueOf();
	}

	active_initlog_users(start_time,end_time,function(err,logs){
		if(err) { return console.trace(err); }

		if(logs.length == 1) {
			stuff = "was "+logs.length+" account";
		} else {
			stuff = "were "+logs.length+" accounts";
		}

		console.log("There " + stuff + " which 'inited' since " + answer + " ago untill now");
	});
	rl.close();
});