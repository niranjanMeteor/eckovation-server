var mongoose                = require('mongoose');
var redis                   = require('redis');
var readline                = require('readline');
var configs                 = require('./../utility/configs.js');
var RedisPrefixes           = require('./../utility/redis_prefix.js');

var redis_client            = redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
  console.trace("[[[[[[[ Redis Connection Error ]]]]]]]");
  console.trace(err);
  console.log('[[[[[[ Shutting Down the User Trace ]]]]]]');
  console.log('[[[[[[ User Session Terminated ]]]]]]');  
  process.exit(0);
});

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("[[[[[[ MongoDb Connection Error ]]]]]]");
  console.trace(err);
  console.log('[[[[[[ Shutting Down the User Trace ]]]]]]');
  console.log('[[[[[[ User Session Terminated ]]]]]]');  
  process.exit(0);
});
mongoose.connection.on('connected', function(){
  console.log('\n');
  console.log('[[[[[[[ Successfully Opened MOngoDb Connection ]]]]]]]');
  console.log('\n');
  startScript();
  // startScanningUnverifiedAccountsRcqs();
  startScanningVerifiedAccountsRcqs()
});

require('../models/accounts.js');
require('../models/profiles.js');
var Account               = mongoose.model('Account');
var Profile               = mongoose.model('Profile');

var rhmset                = require("./../utility/redis_hmset.js")(redis_client);
var rcq                   = require("./../utility/redis_chat_queue.js")(redis_client);

var rL = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

function startScript(){
  console.log('[[[[[[[ RCQ Clreance Service Started ]]]]]]]');
  console.log('\n');
}

// var TARGET_TIME = 'ISODate("2016-06-22T00:00:00.000Z")';
var TARGET_TIME  =  ( (new Date().getTime()) - 40*24*3600*1000);
var TARGET_NUMBER_OF_PIDS = 50000;
var DIRECT = false;
var DRY    = true;

if(process.argv[2] && process.argv[2] == 'direct') {
  DIRECT = true;
  console.log('\n');
  console.log('[[[[[[[[[[  SCRIPT MODE = DIRECT ]]]]]]]]]]');
  console.log('\n');
} else {
  console.log('\n');
  console.log('[[[[[[[[[[  SCRIPT MODE = NON-DIRECT ]]]]]]]]]]');
  console.log('\n');
}

if(process.argv[3] && process.argv[3] == 'real') {
  DRY = false;
  console.log('\n');
  console.log('[[[[[[[[[[  SCRIPT RUNNNING MODE = REAL WITH DATA DELETION]]]]]]]]]]');
  console.log('\n');
} else {
  console.log('\n');
  console.log('[[[[[[[[[[  SCRIPT RUNNING MODE =  DRY ]]]]]]]]]]');
  console.log('\n');
}

function startScanningUnverifiedAccountsRcqs(){
  Account.find({
    $or : [
      {
        vrfy : false
      },
      {
        vrfy : true,
        lsnm : {$exists : false},
        lsni : {$exists : false},
        lsnw : {$exists : false}
      }
    ]
  },{
    _id : 1
  },function(err,accounts){
    if(err){
      console.trace(err);
      process.exit(0);
    }
    var total = accounts.length;
    if(total == 0){
      console.log('[[[[[[[ No Unverified Accounts Found ]]]]]]]');
      console.log('[[[[[[[ Thanks For Using Our Service ]]]]]]]');
      process.exit(0);
    } else {
      console.log('[[[[[[[ Total UnVerified Accounts In Database  : '+total+' ]]]]]]]');
    }
    processAidQueue(accounts, function(err,resp){
      if(err){
        console.trace(err);
        return process.exit(0);
      }    
      var aids = [];
      for(var i=0; i<total; i++){
        aids.push(accounts[i]._id+'');
      }
      accounts = null;
      Profile.find({
        aid : { $in : aids }
      },{
        _id : 1
      },function(err,profiles){
        if(err){
          console.trace(err);
          process.exit(0);
        }
        aids = null;
        var total_p = profiles.length;
        if(total_p == 0){
          console.log('[[[[[[[ No Profiles Found to Process ]]]]]]]');
          console.log('[[[[[[[ Thanks For Using Our Service ]]]]]]]');
          process.exit(0);
        }
        var rcqLength = 0;
        var done = 0;
        var targetPids = [];
        for(var i=0; i<total_p; i++){
          countProfileRcqLength(profiles[i]._id,function(err,resp){
            if(err){
              console.trace(err);
              process.exit(0);
            }
            done++;
            if(resp.c > 0){
              rcqLength += resp.c;
              targetPids.push(resp.pid);
            }
            if(done == total_p){
              if(targetPids.length == 0){
                console.log('[[[[[[[ No Target Profiles Found for RCQ Clreance ]]]]]]]');
                console.log('[[[[[[[ Thanks For Using Our Service ]]]]]]]');
                process.exit(0);
              }
              if(rcqLength == 0){
                console.log('[[[[[[[ Total Target Profiles : '+targetPids.length+' ]]]]]]]');
                console.log('[[[[[[[ No Rcq Object in target Profiles RCQ Clreance ]]]]]]]');
                console.log('[[[[[[[ Thanks For Using Our Service ]]]]]]]');
                process.exit(0);
              }
              profiles = null;
              return interactForProfileQueueDeletion(targetPids, rcqLength);
            }
          });
        }
      });
    });
  });
}

function startScanningVerifiedAccountsRcqs(){
  Account.find({
    vrfy : true,
  },{
    _id  : 1,
    lsnm : 1,
    lsni : 1
  },function(err,accounts){
    if(err){
      console.trace(err);
      process.exit(0);
    }
    var total = accounts.length;
    if(total == 0){
      console.log('[[[[[[[ No Unverified Accounts Found ]]]]]]]');
      console.log('[[[[[[[ Thanks For Using Our Service ]]]]]]]');
      process.exit(0);
    } else {
      console.log('[[[[[[[ Total Verified Accounts In Database  : '+total+' ]]]]]]]');
    }
    var filter_accounts = [];
    var account,time_c;
    for(var i=0; i<total; i++){
      account = accounts[i];
      if(account.lsnm){
        time_c = new Date(account.lsnm);
        time_c = time_c.getTime();
        if(time_c > TARGET_TIME){
          continue;
        }
      }
      if(account.lsni){
        time_c = new Date(account.lsni);
        time_c = time_c.getTime();
        if(time_c > TARGET_TIME){
          continue;
        }
      }
      filter_accounts.push(account);
    }
    accounts = null;
    var total = filter_accounts.length;
    if(total == 0){
      console.log('[[[[[[[ No Unverified Accounts With 1 Month Expiry Found ]]]]]]]');
      console.log('[[[[[[[ Thanks For Using Our Service ]]]]]]]');
      process.exit(0);
    } else {
      console.log('[[[[[[[ Total Expired Accounts Count : '+total+' ]]]]]]]');
    }
    processAidQueue(filter_accounts, function(err,resp){
      if(err){
        console.trace(err);
        return process.exit(0);
      }    
      var aids = [];
      for(var i=0; i<total; i++){
        aids.push(filter_accounts[i]._id+'');
      }
      filter_accounts = null;
      Profile.find({
        aid : { $in : aids }
      },{
        _id : 1
      },function(err,profiles){
        if(err){
          console.trace(err);
          process.exit(0);
        }
        aids = null;
        var total_p = profiles.length;
        if(total_p == 0){
          console.log('[[[[[[[ No Profiles Found to Process ]]]]]]]');
          console.log('[[[[[[[ Thanks For Using Our Service ]]]]]]]');
          process.exit(0);
        }
        var rcqLength = 0;
        var done = 0;
        var targetPids = [];
        for(var i=0; i<total_p; i++){
          countProfileRcqLength(profiles[i]._id,function(err,resp){
            if(err){
              console.trace(err);
              process.exit(0);
            }
            done++;
            if(resp.c > 0){
              rcqLength += resp.c;
              targetPids.push(resp.pid);
            }
            if(done == total_p){
              if(targetPids.length == 0){
                console.log('[[[[[[[ No Target Profiles Found for RCQ Clreance ]]]]]]]');
                console.log('[[[[[[[ Thanks For Using Our Service ]]]]]]]');
                process.exit(0);
              }
              if(rcqLength == 0){
                console.log('[[[[[[[ Total Target Profiles : '+targetPids.length+' ]]]]]]]');
                console.log('[[[[[[[ No Rcq Object in target Profiles RCQ Clreance ]]]]]]]');
                console.log('[[[[[[[ Thanks For Using Our Service ]]]]]]]');
                process.exit(0);
              }
              profiles = null;
              console.log('[[[[[[[[[ Before Deletion Aid Queue Acuumulation Info for Unverified Accounts ]]]]]]]]]');
              console.log('Before Deletion Target accounts With Accunulated Aid-Q  :  '+targetPids.length);
              console.log('\n');
              if(targetPids.length > TARGET_NUMBER_OF_PIDS){
                targetPids = targetPids.splice(0,TARGET_NUMBER_OF_PIDS);
              }
              return interactForProfileQueueDeletion(targetPids, rcqLength);
            }
          });
        }
      });
    });
  });
}

function processAidQueue(accounts, cb){
  var aidQueueLenth = 0;
  var done          = 0;
  var targetAids    = [];
  var total         = accounts.length;
  for(var i=0; i<total; i++){
    countAidQueueLength(accounts[i]._id,function(err,resp){
      if(err){
        console.trace(err);
        process.exit(0);
      }
      done++;
      if(resp.c > 0){
        aidQueueLenth += resp.c;
        targetAids.push(resp.aid);
      }
      if(done == total){
        if(targetAids.length == 0){
          console.log('[[[[[[[ No Target accounts Found for AID Queue Clreance ]]]]]]]');
          console.log('[[[[[[[ We will Now Move to Profile RCQ clearance ]]]]]]]');
          console.log('\n');
          console.log('\n');
          return cb(null,true);
        }
        if(aidQueueLenth == 0){
          console.log('[[[[[[[ Total Target accounts : '+total+' ]]]]]]]');
          console.log('[[[[[[[ No Object in target accounts Queue Clreance ]]]]]]]');
          console.log('[[[[[[[ We will Now Move to Profile RCQ clearance ]]]]]]]');
          console.log('\n');
          console.log('\n');
          return cb(null,true);
        }
        accounts = null;
        return interactForAidQueueDeletion(targetAids, aidQueueLenth, cb);
      }
    });
  }
}

function interactForAidQueueDeletion(accounts, aidQueueLenth, cb){
  var done    = 0;
  var success = 0;
  var errors  = 0;
  var total   = accounts.length;
  console.log('[[[[[[[[[ Aid Queue Acuumulation Info for Unverified Accounts ]]]]]]]]]');
  console.log('Total Target accounts With Accunulated Aid-Q  :  '+total);
  console.log('Total Length of Unverified Accounts Aid-Q  :  '+aidQueueLenth);
  console.log('\n');
  if(DIRECT){
    console.log('[[[[[[[[ Below are Dropped Account Queues ]]]]]]]]]');
    console.log('\n');
    for(var i=0; i<total; i++){
      removeAidQueue(accounts[i],function(err,resp){
        if(err){
          console.trace(err);
          process.exit(0);
        }
        done++;
        if(resp){
          success++;
        } else {
          errors++;
        }
        if(done == total){
          console.log('\n');
          console.log('Total Target Aids for Aid Queue Clearance : '+total);
          console.log('Total Aids Processed : '+done);
          console.log('Total Success : '+success);
          console.log('Total Errors : '+errors);
          console.log('\n');
          console.log('\n');
          return cb(null,true);
        }
      });
    }
  } else {
    rL.question("Do you Want to Clear the above Useless Redis Aid Queue Storage(1 : YES, 2 : EXIT) : ", function(answer){
      if(isNaN(answer)){
        console.log('Invalid Option, Please enter a valid option');
        return interactForAidQueueDeletion(accounts, aidQueueLenth, cb);
      }
      answer = parseInt(answer.trim());
      if(answer < 1 || answer > 2){
        console.log('Invalid Option, Please enter a valid option');
        return interactForAidQueueDeletion(accounts, aidQueueLenth, cb);
      }
      if(answer != 1){
        console.log('You have opted for not removing the useless bundle of data piling in Redis Ram storage');
        console.log('[[[[[[[ Thanks For Using Our Service ]]]]]]]');
        console.log('\n');
        return process.exit(0);
      }
      console.log('\n');
      for(var i=0; i<total; i++){
        removeAidQueue(accounts[i],function(err,resp){
          if(err){
            console.trace(err);
            process.exit(0);
          }
          done++;
          if(resp){
            success++;
          } else {
            errors++;
          }
          if(done == total){
            console.log('\n');
            console.log('Total Target Aids for Aid Queue Clearance : '+total);
            console.log('Total Aids Processed : '+done);
            console.log('Total Success : '+success);
            console.log('Total Errors : '+errors);
            console.log('\n');
            console.log('\n');
            return cb(null,true);
          }
        });
      }
    });
  }
}

function interactForProfileQueueDeletion(profiles, rcqLength){
  var total   = profiles.length;
  var done    = 0;
  var success = 0;
  var errors  = 0;
  console.log('\n');
  console.log('[[[[[[[[[ RCQ Acuumulation Info Profile for Unverified Accounts ]]]]]]]]');
  console.log('Total Target Profiles With Accunulated RCQ  :  '+total);
  console.log('Total Length of Unverified Accounts RCQ  :  '+rcqLength);
  console.log('\n');
  if(DIRECT){
    console.log('[[[[[[[[ Below are Dropped Profile Queues ]]]]]]]]]');
    console.log('\n');
    for(var i=0; i<total; i++){
      removeProfileRcq(profiles[i],function(err,resp){
        if(err){
          console.trace(err);
          process.exit(0);
        }
        done++;
        if(resp){
          success++;
        } else {
          errors++;
        }
        if(done == total){
          console.log('\n');
          console.log('Total Target Pids for RCQ Clearance : '+total);
          console.log('Total Pids Processed : '+done);
          console.log('Total Success : '+success);
          console.log('Total Errors : '+errors);
          console.log('\n');
          console.log('[[[[[[[ Thanks For Using Our Service ]]]]]]]');
          console.log('\n');
          return process.exit(0);
        }
      });
    }
  } else {
    rL.question("Do you Want to Clear the above Useless Redis RCQ Storage(1 : YES, 2 : EXIT) : ", function(answer){
      if(isNaN(answer)){
        console.log('Invalid Option, Please enter a valid option');
        return interactForProfileQueueDeletion(profiles, rcqLength);
      }
      answer = parseInt(answer.trim());
      if(answer < 1 || answer > 2){
        console.log('Invalid Option, Please enter a valid option');
        return interactForProfileQueueDeletion(profiles, rcqLength);
      }
      if(answer != 1){
        console.log('You have opted for not removing the useless bundle of data piling in Redis Ram storage');
        console.log('[[[[[[[ Thanks For Using Our Service ]]]]]]]');
        console.log('\n');
        return process.exit(0);
      }
      console.log('\n');
      for(var i=0; i<total; i++){
        removeProfileRcq(profiles[i],function(err,resp){
          if(err){
            console.trace(err);
            process.exit(0);
          }
          done++;
          if(resp){
            success++;
          } else {
            errors++;
          }
          if(done == total){
            console.log('\n');
            console.log('Total Target Pids for RCQ Clearance : '+total);
            console.log('Total Pids Processed : '+done);
            console.log('Total Success : '+success);
            console.log('Total Errors : '+errors);
            console.log('\n');
            console.log('[[[[[[[ Thanks For Using Our Service ]]]]]]]');
            console.log('\n');
            return process.exit(0);
          }
        });
      }
    });
  }
}

function countProfileRcqLength(pid, cb){
  rcq.cardinality(pid,function(err,resp){
    if(err){
      return cb(err,null);
    }
    // console.log('Pid : '+pid+', Cnt : '+resp);
    return cb(null,{c:resp,pid:pid});
  });
}

function removeProfileRcq(pid, cb){
  if(DRY){
    console.log('Drp_Pid : '+pid);
    return cb(null,true);
  }
  rcq.removekey(pid,function(err,resp){
    if(err){
      return cb(err,null);
    }
    if(resp){
      console.log('Drp_Pid : '+pid);
      return cb(null,true);
    }
    return cb(null,false);
  });
}

function countAidQueueLength(aid, cb){
  rcq.cardinality(aid,function(err,resp){
    if(err){
      return cb(err,null);
    }
    // console.log('Aid : '+aid+', Cnt : '+resp);
    return cb(null,{c:resp,aid:aid});
  });
}

function removeAidQueue(aid, cb){
  if(DRY){
    console.log('Drp_Aid : '+aid);
    return cb(null,true);
  }
  rcq.removekey(aid,function(err,resp){
    if(err){
      return cb(err,null);
    }
    if(resp){
      console.log('Drp_Aid : '+aid);
      return cb(null,true);
    }
    return cb(null,false);
  });
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : configs.MONGO_SERVER_KEEPALIVE
      }
    }
  };
  if(!configs.MONGO_UNAME || configs.MONGO_UNAME == ""){
    options.server.poolSize = configs.MONGO_POOLSIZE;
    console.log('[[[[[[[    NO MONGO_UNAME FOUND IN CONFIGS, WILL CONNNECT WITHOUT REPLICA OPTIONS     ]]]]]]]]]');
    console.log('[[[[[[[    MONGODB CONNECTION POOL_SIZE : '+configs.MONGO_POOLSIZE+'     ]]]]]]]]]');
    return mongoose.connect("mongodb://"+configs.MONGO_HOST+":"+configs.MONGO_PORT+"/"+configs.DB_NAME,options);
  }
  options.replset = {
    rs_name : configs.MONGO_REPLICA_SET_NAME,
    socketOptions : {
      keepAlive : configs.MONGO_REPLICA_KEEPALIVE
    },
    poolSize : configs.MONGO_POOLSIZE
  };
  var connect_string = "mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@";
  var mongo_hosts    = configs.MONGO_HOST;
  var mongo_port     = configs.MONGO_PORT;
  for(var i=0; i<mongo_hosts.length; i++){
    if(i > 0)
      connect_string += ",";
    connect_string += mongo_hosts[i]+":"+mongo_port;
  }
  connect_string += "/"+configs.DB_NAME;
  console.log('[[[[[[[     CONNECTING MONGODB WITH REPLICA_SET OPTIONS     ]]]]]]]]]');
  console.log('[[[[[[[     MONGODB CONNECTION POOL_SIZE : '+configs.MONGO_POOLSIZE+'      ]]]]]]]]]');
  return mongoose.connect(connect_string,options);
}
