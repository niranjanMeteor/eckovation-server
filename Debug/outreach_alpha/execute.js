var mongoose            = require('mongoose');
var configs             = require('./../../utility/configs.js');
var readline 			= require('readline');
var fs 					= require('fs');

require('./../../models/accounts.js');
require('./../../models/profiles.js');
require('./../../models/groups.js');
require('./../../models/groupmembers.js');
require('./../../models/privilegegroups.js');

var Account 				    = mongoose.model('Account');
var Profile 				    = mongoose.model('Profile');
var Group 				      	= mongoose.model('Group');
var GroupMember 		      	= mongoose.model('GroupMember');
var PrivilegeGroup 				= mongoose.model('PrivilegeGroup');


var rL = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

var options             = {server : {socketOptions : {keepAlive : 1}}};
mongoose.connect("mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@"+configs.MONGO_HOST+"/"+configs.DB_NAME,options, function(err){
	if(err) {
		console.log(err);
		console.trace(err);
	} else {
		console.log('Connected to mongodb!');
		console.log('------------------------------------Outreach Alpha SCRIPT STARTED------------------------------------')
		console.log('Outreach Alpha Mission has started');
		startOutreachalpha();
	}
});

var otp_verified = 0;

var startOutreachalpha = function() {
	rL.question("Enter the school code ( Or 0 to Exit ) : ", function(code) {
		if(code == 0 || code == '0') {
			endScript();
		}
		code = code.trim().toLowerCase();

		rL.question("Which group you want to subscribe them into? ( Or 0 to Exit ) : ", function(gcode) {
			if(gcode ==0 || gcode == '0') {
				endScript();
			}

			fs.readFile("./data/statuses.csv","utf8",function(err,status_data){
				if(err) throw err;

				var ts = (Date.now() / 1000 | 0);
				var statusMatrix = statusDataFromStatusesCsv(status_data,ts,60);

				fs.readFile("./data/"+code+"_export_allstudents.txt",'utf8',function(err,data) {
					if(err) throw err;
					var final_data = extractFinalDataFromRawData(statusMatrix,data);

					for(var indx in final_data) {
						var data = final_data[indx];

						if(statusMatrix[data.mobile] == 'Delivered') {
							final_data[indx]['verified'] = 1;
							otp_verified++;
						} else {
							final_data[indx]['verified'] = 0;
						}
					}
					console.log("Total number of delivered mobile numbers: " + otp_verified);

					var is_execute = false;

					if(process.argv[2]=="-mode=run") {
						console.log("Now making data changes!!!!");
						is_execute = true;
					}
					if(is_execute) {
						console.log('\nI AM GOING TO MAKE CHANGES IN THE DATABASE!\n');
					}

					final_data = preProcessData(final_data);

					if(process.argv[2]=="-mode=verify") {
						console.log("Now verifying inserted data!!!!");
						is_execute = false;
						verifyDataFromDatabase(final_data,gcode,function(okay,total_profiles,total){
							console.log("Correctly inserted records : " + okay);
							console.log("Total awesome profiles : " + total_profiles);
							console.log("Total to be scanned records: " + total);

							process.exit();
						});
					} else if (is_execute) {
						console.log("Going ahead to create : " + final_data.length + " accounts!");

						insertAccountDataIntoDatabase(final_data,gcode,is_execute,function(final_with_acc_data){
							if(final_with_acc_data.length == 0) {
								console.log("It seems you did not make any accounts yet! So creating no profiles!")
								process.exit();
							} else {
								console.log("Going ahead to create : " + final_with_acc_data.length + " profiles!");
								insertProfileAndGroupMemberDataIntoDatabase(final_with_acc_data,gcode,is_execute);
							}
						});
					}
				});
			});
		});
	});
};

var postInsertDataIntoDatabase = function(data) {
	console.log("Total New Accounts To Be Made : " + data.newAccounts);

	if(data["newProfiles"]) {
		console.log("Total New Profiles added : " + data.newProfiles);
	}

	if(data["newGroupMembers"]) {
		console.log("Total New GroupMembers added : " + data.newGroupMembers);
	}

	process.exit();
};

var verifyDataFromDatabase = function(finalData,gcode,cb) {
	var okay_records 	= 0;
	var total_records 	= 0;
	var okay_total_profiles = 0;

	finalData.forEach(function(data){
		Group.findOne({
			code : gcode,
		},function(err,grp) {
			if(err) {
				console.log("Wrong Group Code!");
				process.exit();
			}

			Account.findOne({
					m : data.mobile,
			},function(err,acc){
				var role = 2;
				if(data.parent == "----") {
					role = 3;
				}

				if(acc) {
					Profile.find({
						aid : acc._id,
						// role : role,
						act : true,
					},function(err,prfls){

						var prfl_ids = [];
						prfls.forEach(function(prfl){
							prfl_ids.push(prfl._id);
						});

						if(prfls) {

							if(prfls.length > 1) {
								console.log("with " + prfls.length + " profiles!");
								console.log(data);
							}

							GroupMember.find({
								gid : grp._id,
								pid : {$in : prfl_ids},
								// pid : prfl_ids[0],
								act : true,
							},function(err,gms){
								if(gms.length == 1) {
									okay_records++;
									okay_total_profiles++;
									total_records++;

									if(total_records == finalData.length) {cb(okay_records,okay_total_profiles,total_records);}
								} else {
									console.log("no or less gms! gms = " + gms.length + " profile ids = " + prfl_ids.join(",") + " group id = " + grp._id);
									console.log(data);
									total_records++;

									if(total_records == finalData.length) {cb(okay_records,okay_total_profiles,total_records);}
								}
							});
						} else {
							console.log("no profile!");
							console.log(data);

							total_records++;

							if(total_records == finalData.length) {cb(okay_records,okay_total_profiles,total_records);}
						}
					});
				} else {
					total_records++;
					if(total_records == finalData.length) {cb(okay_records,okay_total_profiles,total_records);}
				}
			});
		});
	});
};

var preProcessData = function(finalData) {
	var number_hash = {};
	var arr = [];
	var duplicate_numbers = 0;
	var total_profiles = 0;

	finalData.forEach(function(data){
		if(number_hash[data.mobile]) {
			duplicate_numbers++;
			
			/*if(!number_hash[data.mobile]["extra"]) {
				number_hash[data.mobile]["extra"] = [];
			}
			number_hash[data.mobile]["extra"].push(data);
			total_profiles++;*/
		}

		if(!number_hash[data.mobile]) {
			number_hash[data.mobile] = data;
			total_profiles++;
		}
	});

	for(var nmbr in number_hash) {
		arr.push(number_hash[nmbr]);
	}

	console.log("There were about " + duplicate_numbers + " duplicate numbers! Not clubbing them! Finally there will be " + total_profiles + " profiles!");

	return arr;
};

var insertAccountDataIntoDatabase = function(finalData,gcode,is_execute,cb) {
	var final_data_with_account_data = [];

	var total_processed = 0;

	finalData.forEach(function(data) {
		Group.findOne({
			code : gcode,
		},function(err,group){
			if(err) {
				console.log("Failed to find that valid group!");
				process.exit();
			}

			if(!group) {
				console.log("That's not a valid group!");
				console.log("Exiting!");
				process.exit();
			}

			Account.findOne({
				m : data.mobile,
			},function(err,old_account) {
				if(!old_account) {
				//make new account
					if(is_execute) {
						var is_verified = false;
						is_verified = (data.verified == 1) ? true : false;

						var new_account = new Account({
							ccod : '91',
							m 	 : data.mobile,
							vrfy : is_verified,
							vrsn : 'a'
						});

						new_account.save(function(err,new_account) {
							if(err) {
								console.log("Failed to save this account");
								console.log(err);
								console.log(data);
								console.log(new_account);
								return;
							}

							final_data_with_account_data.push({
								aid 	: new_account._id,
								parent 	: data.parent,
								kid 	: data.kid,
							});

							if(data.extra) {
								data.extra.forEach(function(extra_data){
									final_data_with_account_data.push({
										aid 	: new_account._id,
										parent 	: extra_data.parent,
										kid 	: extra_data.kid,
									});
								});
							}

							total_processed++;
							if(total_processed == finalData.length) {
								cb(final_data_with_account_data);
							}
						});
					}
				} else {
					final_data_with_account_data.push({
						aid 	: old_account._id,
						parent 	: data.parent,
						kid 	: data.kid,
					});

					total_processed++;
					if(total_processed == finalData.length) {
						cb(final_data_with_account_data);
					}
				}
			});
		});
	});
};

var insertProfileAndGroupMemberDataIntoDatabase = function(finalData,gcode,is_execute) {
	/*
	final_data = [{
		parent 	: --,
		kid 	: --,
		mobile 	: --,
		verified: --,
		extra   : [], //extra profiles if required
	}]
	*/

	var newGroupMembers = 0;
	var newProfiles = 0;
	var newAccounts = 0;
	var accounts_process = 0;

	Group.findOne({
		code : gcode,
	},function(err,group){
		if(err) {
			console.log("Failed to find the group! Exiting!");
			return;
		}

		finalData.forEach(function(data) {
			var new_profile = new Profile({
				aid : data.aid,
				sid : null,
				nam : data.parent,
				cnam: data.kid,
				role : 2,
				clss : null,
				ppic : '',
				act : true
			});

			if(data.parent == "----") {
				new_profile = new Profile({
					aid : data.aid,
					sid : null,
					nam : data.kid,
					cnam : null,
					role : 3,
					clss : null,
					ppic : '',
					act : true
				});
			}
			new_profile.save(function(err,new_profile){
				if(err) { console.log("Failed to save this profiel!"); console.log(new_profile); return; }

				var new_member = new GroupMember({
					gid : group._id,
					pid : new_profile._id,
					type : 2,
					act : true
				});

				new_member.save(function(err){
					if(err) { console.log("Failed to make this profile a member!"); console.log(new_member); return; }

					newAccounts++;
					newProfiles++;
					newGroupMembers++;


					accounts_process++;

					if(accounts_process == finalData.length) {
						postInsertDataIntoDatabase({
							newAccounts : newAccounts,
							newGroupMembers : newGroupMembers,
							newProfiles : newProfiles,
						});
					}
				});
			});
		});
	});
}

function statusDataFromStatusesCsv(data,current_time,day_limit) {
	var lines = data.split("\n");
	var matrix = {};

	for(var inx in lines) {
		var line = lines[inx];
		var splits = line.split(",");

		var mobile 		= splits[2];
		mobile = mobile.replace("\"","");
		mobile = mobile.replace("\"","");

		var status 		= splits[3];
		status = status.replace("\"","");
		status = status.replace("\"","");

		var timestamp 	= splits[4];
		timestamp = timestamp.replace("\"","");
		timestamp = timestamp.replace("\"","");
		timestamp = parseInt(timestamp);

		if(current_time - timestamp <  day_limit*(24*3600)) {
			if(status == 'Delivered') {
				matrix[mobile] = status;
			}
		}
	}

	return matrix;
}

function extractFinalDataFromRawData(statusMatrix,data) {
	var cnt = 0;
	var multi_mobile = 0;
	var multi_mobile_2 = 0;
	var multi_mobile_3 = 0;

	var final_data = [];
	//obj = {parent:"",kid:"",mobile:""}

	var lines = data.split("\r");

	for(var inx in lines) {
		var splits = lines[inx].split("\t");
		//splits = [class_section,reg_id,name,father's name,mobiles]
		var class_section 	= splits[0];
		var reg_id 			= splits[1];
		var name 			= splits[2];
		var father_name 	= splits[3];
		var mobiles			= splits[4];
		var mother_name		= splits[5];

		splits[0] 			= class_section.trim();

		if(mobiles == "") { continue; }

		if(mobiles.split(",").length > 1) {
			multi_mobile++;
		}
		if(mobiles.split(",").length > 2) {
			multi_mobile_2++;
		}
		if(mobiles.split(",").length > 3) {
			multi_mobile_3++;
		}

		for(var inx_1 in mobiles.split(",")) {
			var mobile = mobiles.split(",")[inx_1];
			mobile = mobile.replace("\"","");
			mobile = mobile.replace("'","");
			var parent_name = father_name;

			if(!validateMobile(mobile)) { continue;	}
			if(statusMatrix[mobile]!='Delivered') {continue;}

			if(inx_1 == 1) {
				parent_name = mother_name;
			}

			if(inx_1 == 0 || inx_1 == 1) {
				final_data.push({
					parent : toTitleCase(parent_name),
					kid    : toTitleCase(name),
					mobile : mobile,
				});
			} else if(inx_1 == 2) {
				final_data.push({
					parent : "----",
					kid    : toTitleCase(name),
					mobile : mobile,
				});
			}
		}

		cnt++;
	}

	console.log("Total number of accounts: " + final_data.length);
	console.log("Total number of repeat account (2 phones): " + (multi_mobile-multi_mobile_2-multi_mobile_3));
	console.log("Total number of repeat account (3 phones): " + (multi_mobile_2-multi_mobile_3));
	console.log("Total number of repeat account (4 phones): " + multi_mobile_3);

	return final_data;
}

function validateMobile(mobile) {
	if(!mobile || mobile.length != 10 || !(/^([0-9]{10})$/.test(mobile))) {
		return false;
	}

	return true;
}

function endScript(){
	console.log('----------------------Outreach Alpha Mission finished!------------------------');
	process.exit(0);
}

function toTitleCase(str) {
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}