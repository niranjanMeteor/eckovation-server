var mongoose            = require('mongoose');
var configs             = require('./../utility/configs.js');
var readline 			      = require('readline');

require('../models/groupmembers.js');
require('../models/groups.js');
require('../models/profiles.js');

var Group 				      = mongoose.model('Group');
var GroupMember 		    = mongoose.model('GroupMember');
var Profile 				    = mongoose.model('Profile');


var rL = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

var remaining = 0;
var replicated = 0;
var skip_value = 0;

var options             = {server : {socketOptions : {keepAlive : 1}}};
mongoose.connect("mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@"+configs.MONGO_HOST+"/"+configs.DB_NAME,options, function(err){
	if(err) {
		console.log(err);
		console.trace(err);
	} else {
		console.log('Connected to mongodb!');
		console.log('------------------------------------Profile Name Replication SCRIPT STARTED------------------------------------')
		console.log('Program will give you a chance to replicate the profile names form profile collection to groupmembers collection ');
		CheckAccountConsistency();
		CheckProfileConsistency();
		CheckGroupConsistency();
		CheckGroupMemberConsistency();
		CheckMessageConsistency();
		CheckStatusConsistency();
	}
});

function CheckAccountConsistency(){
	var total_account_records          = 0;
	var mobile_number_field_not_exists = 0;
	var mobile_number_field_is_null    = 0;
	var version_field_not_exists      = 0;
	var version_field_is_null         = 0;
	var done = 0;
	var queries = 5;
	Account.count({},function(err,accounts){
		if(err) {
			console.trace(err);
			return;
		}
		total_account_records = accounts;
		++done;
		if(done == queries){
			accountResult(
				total_account_records,
				mobile_number_field_not_exists,
				mobile_number_field_is_null,
				version_field_not_exists,
				version_field_is_null
			);
		}
	});
	Account.count({
		m : {$exists:false}
	},function(err,acc_no_m){
		if(err) {
			console.trace(err);
			return;
		}
		mobile_number_field_not_exists = acc_no_m;
		++done;
		if(done == queries){
			accountResult(
				total_account_records,
				mobile_number_field_not_exists,
				mobile_number_field_is_null,
				version_field_not_exists,
				version_field_is_null
			);
		}
	});
	Account.count({
		m : null
	},function(err,acc_m_null){
		if(err) {
			console.trace(err);
			return;
		}
		mobile_number_field_is_null = acc_m_null;
		++done;
		if(done == queries){
			accountResult(
				total_account_records,
				mobile_number_field_not_exists,
				mobile_number_field_is_null,
				version_field_not_exists,
				version_field_is_null
			);
		}
	});
	Account.count({
		vrsn : {$exists:false}
	},function(err,acc_no_vrsn){
		if(err) {
			console.trace(err);
			return;
		}
		version_field_not_exists = acc_no_vrsn;
		++done;
		if(done == queries){
			accountResult(
				total_account_records,
				mobile_number_field_not_exists,
				mobile_number_field_is_null,
				version_field_not_exists,
				version_field_is_null
			);
		}
	});
	Account.count({
		vrsn : null
	},function(err,acc_vrsn_null){
		if(err) {
			console.trace(err);
			return;
		}
		version_field_is_null = acc_vrsn_null;
		++done;
		if(done == queries){
			accountResult(
				total_account_records,
				mobile_number_field_not_exists,
				mobile_number_field_is_null,
				version_field_not_exists,
				version_field_is_null
			);
		}
	});
}

function CheckProfileConsistency(){
	
}

function CheckGroupConsistency(){
	
}

function CheckGroupMemberConsistency(){
	
}

function CheckStatusConsistency(){
	
}

function accountResult(
				total_account_records,
				mobile_number_field_not_exists,
				mobile_number_field_is_null,
				version_field_not_exists,
				version_field_is_null
){
	console.log('Account Data Validation Script Result');
	console.log('Total Records in Accounts collection : '+total_account_records);
	console.log('Mobile Number field is absent in num of records : '+mobile_number_field_not_exists);
	console.log('Mobile Number field is NULL in num of records : '+mobile_number_field_is_null);
	console.log('Version field is absent in num of records : '+version_field_not_exists);
	console.log('Version field is NULL in num of records : '+version_field_is_null);
	console.log('Account Data Validation Script Result Ends');
	return;
}