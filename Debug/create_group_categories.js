var mongoose            = require('mongoose');
var readline 			      = require('readline');

var configs             = require('./../utility/configs.js');

require('../models/groups.js');
require('../models/categories.js');
require('../models/groupcategories.js');

var Group 				      = mongoose.model('Group');
var Category          = mongoose.model('Category');
var GroupCategory     = mongoose.model('GroupCategory');

var rL = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});
mongoose.connection.on('connected', function(){
  console.log('Connected to mongodb!');
	console.log('------------------------------------PLUGIN GROUP SCRIPT STARTED------------------------------------')
	console.log('Program will give you a chance to create a new group plugins or to remove an already existing group from the group plugins');
  startGroupCategoryOperations();
});

function startGroupCategoryOperations(){
  rL.question("Enter your option (0 to Exit, 1 to AddNewCatg, 2 to AddNewGrpInCatg, 3 to remGrpFrmCatg) : ", function(answer) {
    if(isNaN(answer)) {
      console.log('Oops , you entered a wrong option , Please enter a right option');
      return startRecommendedGroupUpdation();
    }
    switch(parseInt(answer)){
      case 1:
        return AddNewCatg();
      break;

      case 2:
        return AddNewGrpInCatg();
      break;

      case 3:
        return remGrpFrmCatg();
      break;

      case 0:
        endScript();
      break;

      default:
        console.log('Oops , you entered a wrong option , Please enter a right option');
        return startGroupCategoryOperations();
    }
  });
}

function AddNewCatg(){
  rL.question("Enter the Category Name ( Or 0 to Exit ) : ", function(name) {
    if(!name || name == ''){
      console.log('Oops , you entered a wrong name , Please enter a right one');
      return AddNewCatg();
    }
    if(name == '0' || name == 0){
      process.exit(0);
    }
    Category.findOne({
      name : name,
      act  : true
    },function(err,existing){
      if(err){
        console.trace(err);
        process.exit(0);
      }
      if(existing){
        console.log('Oops, Category with this name already existing, please select a different name');
        return AddNewCatg();
      }
      rL.question("Enter the Category Description ( Or 0 to Exit ) : ", function(desc) {
        if(!desc || desc == ''){
          console.log('Oops , you entered a wrong Description , Please enter a right one');
          return AddNewCatg();
        }
        Category.count({},function(err,count){
          if(err){
            console.trace(err);
            process.exit(0);
          }
          var new_category = new Category({
            name : name,
            desc : desc,
            num  : count+1,
            act  : true,
            creatby : "Niranjan"
          });
          new_category.save(function(err,saved_category){
            if(err){
              console.trace(err);
              process.exit(0);
            }
            console.log('Congratulations, your category has been created');
            console.log('Category ID : '+new_category._id);
            console.log('Unique Category Number : '+(count+1));
            return startGroupCategoryOperations();
          });
        });
      });
    });
  });
}

function AddNewGrpInCatg(){
  rL.question("Enter the group_code and catg_num in comma seperated ( Or 0 to Exit ) : ", function(code_catgnum) {
    if(code_catgnum == 0 || code_catgnum == '0')
      endScript();
    if(!code_catgnum) {
      console.log('Oops , you entered a wrong option, Please enter a right option');
      return AddNewGrpInCatg();
    }
    code_catgnum = code_catgnum.trim();
    var values  = code_catgnum.split(",");
    var code    = values[0];
    code = code.trim();
    if(isNaN(code) || code.length < 6) {
      console.log('Oops , you entered a wrong group_code option, Please enter a right group_code');
      return AddNewGrpInCatg();
    }
    var catgnum = values[1];
    catgnum     = catgnum.trim();
    if(!catgnum || catgnum.length > 1000) {
      console.log('Oops , you entered a wrong Category Num, Please enter a right Category Num');
      return AddNewGrpInCatg();
    }
    var tim = new Date().getTime();
    Group.findOne({
      code : code
    },function(err,group){
      if(err) { 
        console.trace(err); 
        process.exit(0);
      }
      if(!group) { 
        console.log('Unfortunately, the group code you entered is not valid, Please try a different group code');
        return AddNewGrpInCatg();
      }
      if(group.act === false) { 
        console.log('Unfortunately, the group you want to remove is not active anymore, Please try a different group code');
        return AddNewGrpInCatg();
      }
      if(group.gpic == null || typeof(group.gpic) === 'undefined') { 
        console.log('Unfortunately, the group does not have any group pic, Please try a different group code');
        return AddNewGrpInCatg();
      }
      Category.findOne({
        num : catgnum,
        act : true
      },function(err,existing){
        if(err){
          console.trace(err);
          process.exit(0);
        }
        if(!existing){
          console.log('Oops, Category Num Does Not Exists');
          return AddNewGrpInCatg();
        }
        GroupCategory.findOne({
          gid    : group._id,
          catgid : existing._id,
          act    : true 
        },function(err,existing_groupCategory){
          if(err) { 
            console.trace(err); 
            process.exit(0);
          }
          if(existing_groupCategory){
            console.log('Looks like this group is already added in the given category, try a diffrent one');
            return AddNewGrpInCatg();
          }
          var new_group_category = new GroupCategory({
            gid    : group._id,
            catgid : existing._id,
            catgnm : existing.num,
            act    : true,
            creatby: "Niranjan"
          });
          new_group_category.save(function(err,created_groupCategory){
            if(err){
              console.trace(err);
              process.exit(0);
            }
            console.log('Congratulations, your group has been added in the category');
            return startGroupCategoryOperations();
          });
        });
      });
    });
  });
}

function remGrpFrmCatg(){
  rL.question("Enter the group_code and catg_num in comma seperated ( Or 0 to Exit ) : ", function(code_catgnum) {
    if(code_catgnum == 0 || code_catgnum == '0')
      endScript();
    if(!code_catgnum) {
      console.log('Oops , you entered a wrong option, Please enter a right option');
      return remGrpFrmCatg();
    }
    code_catgnum = code_catgnum.trim();
    var values  = code_catgnum.split(",");
    var code    = values[0];
    code = code.trim();
    if(isNaN(code) || code.length < 6) {
      console.log('Oops , you entered a wrong group_code option, Please enter a right group_code');
      return remGrpFrmCatg();
    }
    var catgnum = values[1];
    catgnum     = catgnum.trim();
    if(!catgnum || catgnum.length < 1000) {
      console.log('Oops , you entered a wrong Category Num, Please enter a right Category Num');
      return remGrpFrmCatg();
    }
    var tim = new Date().getTime();
    Group.findOne({
      code : code
    },function(err,group){
      if(err) { 
        console.trace(err); 
        process.exit(0);
      }
      if(!group) { 
        console.log('Unfortunately, the group code you entered is not valid, Please try a different group code');
        return remGrpFrmCatg();
      }
      if(group.act === false) { 
        console.log('Unfortunately, the group you want to remove is not active anymore, Please try a different group code');
        return remGrpFrmCatg();
      }
      if(group.gpic == null || typeof(group.gpic) === 'undefined') { 
        console.log('Unfortunately, the group does not have any group pic, Please try a different group code');
        return remGrpFrmCatg();
      }
      Category.findOne({
        num : catgnum,
        act : true
      },function(err,existing){
        if(err){
          console.trace(err);
          process.exit(0);
        }
        if(!existing){
          console.log('Oops, Category Num Does Not Exists');
          return remGrpFrmCatg();
        }
        GroupCategory.findOne({
          gid    : group._id,
          catgid : existing._id,
          act    : true 
        },function(err,existing_groupCategory){
          if(err) { 
            console.trace(err); 
            process.exit(0);
          }
          if(!existing_groupCategory){
            console.log('Looks like this group has already been removed from the category, try a diffrent one');
            return remGrpFrmCatg();
          }
          GroupCategory.update({
            gid    : group._id,
            catgid : existing._id,
            act    : true 
          },{
            act : false
          },function(err,existing_groupCategory){
            if(err) { 
              console.trace(err); 
              process.exit(0);
            }
            console.log('Congratulations, your group has been removed from the category');
            return startGroupCategoryOperations();
          });
        });
      });
    });
  });
}

function endScript(){
  console.log('Thanks For using our service, Hope to serve you soon');
  return process.exit(0);
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : 120
      }
    }
  };
  if(!configs.MONGO_UNAME || configs.MONGO_UNAME == ""){
    return mongoose.connect("mongodb://"+configs.MONGO_HOST+":"+configs.MONGO_PORT+"/"+configs.DB_NAME,options);
  }
  options.replset = {
    rs_name : configs.MONGO_REPLICA_SET_NAME,
    socketOptions : {
      keepAlive : 120
    }
  };
  var connect_string = "mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@";
  var mongo_hosts    = configs.MONGO_HOST;
  var mongo_port     = configs.MONGO_PORT;
  for(var i=0; i<mongo_hosts.length; i++){
    if(i > 0)
      connect_string += ",";
    connect_string += mongo_hosts[i]+":"+mongo_port;
  }
  connect_string += "/"+configs.DB_NAME;
  return mongoose.connect(connect_string,options);
}