var socketio            = require('socket.io');
var mongoose            = require('mongoose');
var validator           = require('validator');
var kue                 = require('kue');
var redis               = require('redis');
var _                   = require('underscore');
var readline            = require('readline');
var tokenValidator      = require('./../utility/token_validator.js');
var configs             = require('./../utility/configs.js');
var RedisPrefixes       = require('../utility/redis_prefix.js');
var ClientType          = require('../utility/client_type.js');
var UserRedisKeys       = require('../utility/user_redis_keys.js');

var connect = function () {
  var options = {server : {socketOptions : {keepAlive : 1}}};
  if(configs.MONGO_UNAME!="") {
    mongoose.connect("mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@"+configs.MONGO_HOST+"/"+configs.DB_NAME,options);
  } else {
    mongoose.connect("mongodb://"+configs.MONGO_HOST+"/"+configs.DB_NAME,options);
  }
}
connect();
mongoose.connection.on('error',console.log);
mongoose.connection.on('disconnected', connect);

var redis_client        = redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
    console.log("Error " + err);
});

require('../models/profiles.js');
require('../models/accounts.js');
require('../models/messages.js');

var Account             = mongoose.model('Account');
var Profile             = mongoose.model('Profile');
var Message             = mongoose.model('Message');

var SOCKET_PREFIX       = "s";
var S_COUNT_PREFIX      = "seen_count";

var rmc                 = require("./../utility/redis_mongo_cache.js")(redis_client);
var rcq                 = require("./../utility/redis_chat_queue.js")(redis_client);
var rsk                 = require("./../utility/redis_socket_key.js")(redis_client,SOCKET_PREFIX);
var rseencount          = require("./../utility/redis_key_store.js")(redis_client,S_COUNT_PREFIX);
var rcustomq            = require("./../utility/redis_custom_queue.js")(redis_client);
var rhmset              = require("./../utility/redis_hmset.js")(redis_client);

var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.question("Please enter the mobile number queue investigate: ", function(mobile) {
    Account.findOne({
        m : mobile
    },function(err,acc) {
        if(err || !acc) { console.log("That was an invalid phone number! Exiting!"); process.exit(); return; }

        var account_id = acc._id;
        getUserProfilesPartial(account_id,function(err,profiles){
            if(err) {
                console.log("I got nothing from the "+RedisPrefixes.USER_PROF_GMS+" cache!");
                process.exit();
                return;
            }

            console.log("I got following from user profile cache!\n");
            console.log(profiles);

            if(!profiles) {
                console.log("I got no profiles so not proceeding further!");
            } else {
                getUserGroupMemPartial(account_id, function(err,gms){
                    console.log("I got following from group member cache!\n");
                    console.log(gms);
                    process.exit();
                });
            }
        })
    });
});

function getUserProfilesPartial(account_id, cb){
    var key = RedisPrefixes.USER_PROF_GMS+account_id+'';
    rhmset.get(key,UserRedisKeys.PROFILES,function(err,rProfiles){
        if(err) { 
            return cb(err,null); 
        }

        var toReturn = JSON.parse(rProfiles);
        return cb(null, toReturn);
    });
}

function getUserGroupMemPartial(account_id, all_profile_ids, cb){
    var key = RedisPrefixes.USER_PROF_GMS+account_id+'';
    rhmset.get(key,UserRedisKeys.GROUPS,function(err,rGroups){
        if(err) { 
            return cb(err,null); 
        }
        var toReturn = JSON.parse(rGroups);
        return cb(null, toReturn);
    });
}
