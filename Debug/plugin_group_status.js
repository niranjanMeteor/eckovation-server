var mongoose            = require('mongoose');
var readline 			      = require('readline');
var redis               = require('redis');

var configs             = require('./../utility/configs.js');
var RedisPrefixes       = require('./../utility/redis_prefix.js');
var Events              = require('./../utility/event_names.js');

var redis_client        = redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
    console.log("Error " + err);
});

var rhmset              = require("./../utility/redis_hmset.js")(redis_client);

require('../models/accounts.js');
require('../models/groups.js');
require('../models/groupmembers.js');
require('../models/plugins.js');
require('../models/groupplugins.js');
var Account             = mongoose.model('Account');
var Group 				      = mongoose.model('Group');
var GroupMember         = mongoose.model('GroupMember');
var AppPlugin           = mongoose.model('AppPlugin');
var GroupPlugin         = mongoose.model('GroupPlugin');

var rL = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});
mongoose.connection.on('connected', function(){
  console.log('Connected to mongodb!');
	console.log('------------------------------------PLUGIN GROUP SCRIPT STARTED------------------------------------')
	console.log('Program will give you a chance to create a new group plugins or to remove an already existing group from the group plugins');
  startGroupPluginUpdation();
});

var rhmset                      = require("./../utility/redis_hmset.js")(redis_client);
var io                          = require('socket.io-emitter')(redis_client);

var ADD_NEW_GROUP_PLUGIN        = 1;
var REMOVE_GROUP_PLUGIN         = 2;

function startGroupPluginUpdation(){
  console.log('\n');
  console.log('****************************** MAIN MENU OPTIONS *******************************');
  console.log('\n');
  rL.question("Enter your option (1 to Enable, 2 to Disable, 3 to View, 0 to Exit) : ", function(answer) {
    if(isNaN(answer)) {
      console.log('Oops , you entered a wrong option , Please enter a right option');
      return startGroupPluginUpdation();
    }
    switch(parseInt(answer)){
      case 1:
        return addNewGroupPlugin();
      break;

      case 2:
        return removeGroupPlugin();
      break;

      case 3:
        return viewGroupPlugins();
      break;

      case 0:
        return process.exit(0);
      break;

      default:
        console.log('Oops , you entered a wrong option , Please enter a right option');
        return startGroupPluginUpdation();
    }
  });
}

function addNewGroupPlugin(){
  rL.question("Enter the group_code and plugin_id in comma seperated ( Or 0 to Exit ) : ", function(code_plugin) {
    if(code_plugin == 0 || code_plugin == '0')
      endScript();
    if(!code_plugin) {
      console.log('Oops , you entered a wrong option, Please enter a right option');
      return addNewGroupPlugin();
    }
    code_plugin = code_plugin.trim();
    var values  = code_plugin.split(",");
    var code    = values[0];
    code = code.trim();
    if(isNaN(code) || code.length < 6) {
      console.log('Oops , you entered a wrong group_code option, Please enter a right group_code');
      return addNewGroupPlugin();
    }
    var plugin_id = values[1];
    plugin_id     = plugin_id.trim();
    if(!plugin_id || plugin_id.length < 24) {
      console.log('Oops , you entered a wrong plugin_id, Please enter a right plugin_id');
      return addNewGroupPlugin();
    }
    var tim = new Date().getTime();
    Group.findOne({
      code : code
    },function(err,group){
      if(err) { 
        console.trace(err); 
        return; 
      }
      if(!group) { 
        console.log('Unfortunately, the group code you entered is not valid, Please try a different group code');
        return addNewGroupPlugin();
      }
      if(group.act == false) { 
        console.log('Unfortunately, the group you want to activate plugin is not active anymore, Please try a different group code');
        return addNewGroupPlugin();
      }
      AppPlugin.findOne({
        _id : plugin_id 
      },function(err, plugin){
        if(err) { 
          console.trace(err); 
          return; 
        }
        if(!plugin) { 
          console.log('Unfortunately, the plugin id that you entered is not valid');
          return addNewGroupPlugin();
        }
        if(plugin.act == false) { 
          console.log('Unfortunately, the plugin id that you entered is not active anymore, Please try a different group code');
          return addNewGroupPlugin();
        }
        getConfirmationFromUser(group, plugin, ADD_NEW_GROUP_PLUGIN, function(confirm){
          if(confirm == 1){
            var key = RedisPrefixes.GROUPS;
            GroupPlugin.update({
              gid    : group._id,
              plgnid : plugin_id
            },{
              $set : {
                act : true,
                status : GroupPlugin.STATUS.ACTIVE
              },
              $setOnInsert : {
                gid    : group._id,
                plgnid : plugin_id
              }
            },{
              upsert : true,
              setDefaultsOnInsert: true
            },function(err,created_group_plugin){
              if(err){
                console.trace(err);
                process.exit(0);
              }
              Group.update({
                _id : group._id
              },{
                plgn : true
              },function(err,updated_group){
                if(err){
                  console.trace(err);
                  process.exit(0);
                }
                rhmset.removefield(key, group._id+'', function(err,secure_cache_rem){
                  if(err){
                    console.trace(err);
                    process.exit(0);
                  }
                  informGroupMembersAboutPlugin(group._id, tim, 1, function(err,resp){
                    if(err){
                      console.trace(err);
                      process.exit(0);
                    }
                    console.log('Successfully informed all groupmembers');
                    console.log('Congratulations, you have successfully enabled a group plugin');
                    return addNewGroupPlugin();
                  });
                });
              });
            });
          } else {
            console.log('Oops, Looks Like you were unsure of the group, try a different group code');
            return addNewGroupPlugin();
          }
        });
      });
    });
  });
}

function removeGroupPlugin(){
  rL.question("Enter the group_code and plugin_id in comma seperated ( Or 0 to Exit ) : ", function(code_plugin) {
    if(code_plugin == 0 || code_plugin == '0')
      endScript();
    if(!code_plugin){
      console.log('Oops , you entered a wrong option, Please enter a right option');
      return removeGroupPlugin();
    }
    code_plugin = code_plugin.trim();
    var values  = code_plugin.split(",");
    var code    = values[0];
    code = code.trim();
    if(isNaN(code) || code.length < 6) {
      console.log('Oops , you entered a wrong group_code option, Please enter a right group_code');
      return removeGroupPlugin();
    }
    var plugin_id = values[1];
    plugin_id     = plugin_id.trim();
    if(!plugin_id || plugin_id.length < 24){
      console.log('Oops , you entered a wrong plugin_id, Please enter a right plugin_id');
      return removeGroupPlugin();
    }
    var tim = new Date().getTime();
    Group.findOne({
      code : code
    },function(err,group){
      if(err) { 
        console.trace(err); 
        return; 
      }
      if(!group){ 
        console.log('Unfortunately, the group code you entered is not valid, Please try a different group code');
        return removeGroupPlugin();
      }
      if(group.act == false){ 
        console.log('Unfortunately, the group you want to remove from Secure is not active anymore, Please try a different group code');
        return removeGroupPlugin();
      }
      if(!group.plgn){ 
        console.log('Unfortunately, the group code you entered DOES NOT have any active plugins, Please try a different group code');
        return removeGroupPlugin();
      }
      AppPlugin.findOne({
        _id : plugin_id 
      },function(err, plugin){
        if(err) { 
          console.trace(err); 
          return; 
        }
        if(!plugin) { 
          console.log('Unfortunately, the plugin id that you entered is not valid');
          return removeGroupPlugin();
        }
        if(plugin.act == false) { 
          console.log('Unfortunately, the plugin id that you entered is not active anymore, Please try a different group code');
          return removeGroupPlugin();
        }
        GroupPlugin.findOne({
          gid    : group._id,
          plgnid : plugin_id,
          act    : true 
        },function(err,if_groupplugin){
          if(err) { 
            console.trace(err); 
            return; 
          }
          if(!if_groupplugin){
            console.log('Unfortunately, this plugin is not active in this group at the moment, try a different one');
            return removeGroupPlugin();
          }
          GroupPlugin.count({
            gid    : group._id,
            act    : true,
            status : GroupPlugin.STATUS.ACTIVE
          },function(err,count_group_plugins){
            if(err) { 
              console.trace(err); 
              return; 
            }
            getConfirmationFromUser(group, plugin, REMOVE_GROUP_PLUGIN, function(confirm){
              if(confirm == 1){
                var key = RedisPrefixes.GROUPS;
                GroupPlugin.update({
                  gid    : group._id,
                  plgnid : plugin_id
                },{
                  act    : false,
                  status : GroupPlugin.STATUS.DELETED
                },function(err,created_group_plugin){
                  if(err){
                    console.trace(err);
                    process.exit(0);
                  }
                  if(count_group_plugins == 1){
                    Group.update({
                      _id : group._id
                    },{
                      plgn : false
                    },function(err,group_updated){
                      if(err){
                        console.trace(err);
                        process.exit(0);
                      }
                      console.log('This was Last plugin in this group and hence group db row also updated');
                      rhmset.removefield(key, group._id+'', function(err,secure_cache_rem){
                        if(err){
                          console.trace(err);
                          process.exit(0);
                        }
                        informGroupMembersAboutPlugin(group._id, tim, 0, function(err,resp){
                          if(err){
                            console.trace(err);
                            process.exit(0);
                          }
                          console.log('Congratulations, you have successfully disabled a group plugin');
                          return removeGroupPlugin();
                        });
                      });
                    });
                  } else {
                    rhmset.removefield(key, group._id+'', function(err,secure_cache_rem){
                      if(err){
                        console.trace(err);
                        process.exit(0);
                      }
                      informGroupMembersAboutPlugin(group._id, tim, 1, function(err,resp){
                        if(err){
                          console.trace(err);
                          process.exit(0);
                        }
                        console.log('Congratulations, you have successfully disabled a group plugin');
                        return removeGroupPlugin();
                      });
                    });
                  }
                });
              } else {
                console.log('Oops, Looks Like you were unsure of the group, try a different group code');
                return removeGroupPlugin();
              }
            });
          });
        });
      });
    });
  });
}
function viewGroupPlugins(){
  process.exit(0);
}

function informGroupMembersAboutPlugin(gid, tim, state, cb){
  var packet = {
    id    : tim,
    gid   : gid,
    state : state
  };
  GroupMember.distinct('aid',{
    gid : gid,
    act : true,
    gdel: false
  },function(err,members){
    if(err){
      return cb(err,null);
    }
    var total = members.length;
    if(total == 0){
      return cb(null,true);
    }
    var done = 0;
    for(var i=0; i<total; i++){
      addInQueueAndEmit(members[i], gid, packet, function(err,resp){
        if(err){
          console.trace(err);
        }
        done++;
        if(done == total){
          console.log('To Emit packets for groupmembers : '+total);
          console.log('Successfully Emitted packets for groupmembers : '+done);
          return cb(null,true);
        }
      });
    }
  });
}

function addInQueueAndEmit(aid, gid, packet, cb){
  Account.findOne({
    _id : aid,
    vrfy: true,
    $or : [
      {
        vrsn: {$gte : configs.TARGET_VERSION_PLUGIN_ANDROID}
      },
      {
        iosv: {$gte : configs.TARGET_VERSION_PLUGIN_IOS}
      }
    ]
  },function(err,account){
    if(err){
      return cb(err,null);
    }
    if(!account){
      return cb(null,false);
    }
    var rPacket = JSON.stringify(packet);
    rhmset.set(RedisPrefixes.PLUGIN_STATE, aid, gid, rPacket, function(err,resp){
      if(err){
        return cb(err,null);
      }
      if(resp){
        io.to(aid).emit(Events.PLUGIN,packet);
        console.log('Following packet emitted for aid : '+aid);
        console.log(packet);
        return cb(null,true);
      }
      return cb(null,false);
    });
  });
}


function getConfirmationFromUser(group, plugin, action, cb){
  if(action == ADD_NEW_GROUP_PLUGIN){
    console.log('The Group in which you want to enable plugin is following');
    console.log(group);
    console.log('The Plugin that you want to activate');
    console.log(plugin);
    console.log('Are you sure that you wanna enable above plugin in given group');
  } else if(action == REMOVE_GROUP_PLUGIN){
    console.log('The Group in which you want to disable plugin is following');
    console.log(group);
    console.log('The Plugin that you want to deactivate');
    console.log(plugin);
    console.log('Are you sure that you wanna disable above plugin in given group');
  }
  rL.question("Press 1 to confirm , 2 if unsure ( Or 0 to Exit ) : ", function(confirmation) {
    if(confirmation == 0 || confirmation == '0')
      endScript();
    if(isNaN(confirmation)) {
      console.log('Fuck off you Loser , looks like you have to come to waste your and our time');
      endScript();
    }
    return cb(parseInt(confirmation));
  });
}

function endScript(){
  console.log('Thanks For using our service, Hope to serve you soon');
  return process.exit(0);
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : 120
      }
    }
  };
  if(!configs.MONGO_UNAME || configs.MONGO_UNAME == ""){
    return mongoose.connect("mongodb://"+configs.MONGO_HOST+":"+configs.MONGO_PORT+"/"+configs.DB_NAME,options);
  }
  options.replset = {
    rs_name : configs.MONGO_REPLICA_SET_NAME,
    socketOptions : {
      keepAlive : 120
    }
  };
  var connect_string = "mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@";
  var mongo_hosts    = configs.MONGO_HOST;
  var mongo_port     = configs.MONGO_PORT;
  for(var i=0; i<mongo_hosts.length; i++){
    if(i > 0)
      connect_string += ",";
    connect_string += mongo_hosts[i]+":"+mongo_port;
  }
  connect_string += "/"+configs.DB_NAME,options;
  return mongoose.connect(connect_string);
}

