var mongoose            = require('mongoose');
var configs             = require('./../utility/configs.js');
var readline 			      = require('readline');
var util                = require('util');
var colors              = require('colors/safe');
var Table               = require('cli-table');

require('../models/groups.js');
require('../models/groupmembers.js');
require('../models/accounts.js');
require('../models/profiles.js');
require('../models/messages.js');

var Group 				      = mongoose.model('Group');
var GroupMember 			  = mongoose.model('GroupMember');
var Account 				    = mongoose.model('Account');
var Profile 				    = mongoose.model('Profile');
var Message 				    = mongoose.model('Message');


var rL = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});
mongoose.connection.on('connected', function(){
  console.log('Successfully Opened MOngoDb Connection');
  startScript();
  startMemberAnalysis();
});

var member_info                    = {};
var timePeriodEndMili   = new Date().getTime();
var timeperiodStartMili = timePeriodEndMili - (15*24*60*60*1000);
var timePeriod = {
	start : new Date(timeperiodStartMili),
	end   : new Date(timePeriodEndMili)
};

function startMemberAnalysis(){
	rL.question(colors.blue("Enter the group code to analyse Member List ( Or 0 to Exit ) : "), function(code) {
		if(code == 0 || code == '0')
			endScript();
		if(isNaN(code)) {
			console.log('Oops , you entered a wrong option , Please enter a right option');
			return startMemberAnalysis();
		}
		Group.findOne({
			code : code
		},function(err,group){
			if(err){ 
				console.trace(err); 
				return endScript(); 
			}
			if(!group) { 
				console.log('Unfortunately , the group code you entered is not valid , Please try a different group code');
				return startMemberAnalysis();
			}
			
		  initializeData();

			var pids = [];
			var aids = [];
			var profilesById      = [];
			var accountsById      = [];
			var groupmembersByPid = [];

			GroupMember.find({
				gid : group._id,
				act : true,
				gdel: false
			},function(err,groupmembers){
				if(err){
					console.trace(err);
					return endScript(); 
				}
				var total_gms_rows = groupmembers.length;
				if(total_gms_rows == 0){
					console.log('No Members present in the group');
					return endScript(); 
				}
				for(var i=0; i<total_gms_rows; i++){
					if(pids.indexOf(groupmembers[i].pid) < 0)
						pids.push(groupmembers[i].pid);
					groupmembersByPid[groupmembers[i].pid+''] = groupmembers[i];
				}

				member_info.TOTAL_GROUPMEMBERS = total_gms_rows;

				Profile.find({
					_id : { $in : pids}
				},{
					aid : 1,
					nam : 1,
					role: 1,
					act : 1,
					cat : 1
				},function(err,profiles){
					if(err){
						console.trace(err);
						return endScript(); 
					}
					var total_prof_rows = profiles.length;
					if(total_prof_rows == 0){
						console.log('No Profiles found for this group members');
						return endScript(); 
					}
					if(total_gms_rows != total_prof_rows){
						console.log('Total pids in group memebrs were not found in profile collection');
						return endScript();
					}

					for(var j=0; j<total_prof_rows; j++){
						profilesById[profiles[j]._id+''] = profiles[j];
						if(aids.indexOf(profiles[j].aid) < 0)
							aids.push(profiles[j].aid);
					}
					Account.find({
						_id : { $in : aids }
					},{
						m   : 1,
						cat : 1,
						vrsn: 1,
						iosv: 1,
						webv: 1,
						lsnm: 1,
						lsnw: 1,
						lsnwn: 1,
						lsni: 1
					},function(err,accounts){
						if(err){
							console.trace(err);
							return endScript(); 
						}
						var total_accs_rows = accounts.length;
						if(total_accs_rows == 0){
							console.log('No Account found for profiles of group members');
							return endScript(); 
						}

						member_info.TOTAL_UNIQUE_ACCOUNTS = total_accs_rows;

						for(var m=0; m<total_accs_rows; m++){
							accountsById[accounts[m]._id+''] = accounts[m];
						}

						profileActiveAnalysis(pids,group._id,function(err,pidActInfo){
							if(err){
								console.trace(err);
								return endScript();
							}
							for(var key in pidActInfo){
								if(pidActInfo[key].app)
									member_info.TOTAL_ACTIVE_PROFILES_OF_THIS_GROUP_ACROSS_APP++;
								if(pidActInfo[key].group)
									member_info.TOTAL_ACTIVE_MEMBERS++;
							}
							roleBasedAnalysis(total_gms_rows, profilesById, accountsById, groupmembers, groupmembersByPid);

							deviceBasedAnalysis(total_prof_rows, profiles, accountsById, groupmembersByPid, pidActInfo);

							// console.log("*********************** ANALYSIS STARTED NOW *************************");
							// console.log(member_info);
							DataInFormOfTerminalShell();

						});
					});
				});
			});
		});
	});
}

function profileActiveAnalysis(pids, gid, cb){
	var toBeDone = pids.length;
	var done     = 0;
	var gid = gid+'';
	var actInfo  = [];
	for(var n=0; n<toBeDone; n++){
		pidActInfo(pids[n],gid,function(err,resp){
			if(err){
				return cb(err,null);
			}
			done++;
			actInfo[resp.pid] = resp.act_info;
			if(done == toBeDone){
				return cb(null,actInfo);
			}
		});
	}
}

function pidActInfo(pid, gid, cb){
	var isActiveOnGrp  = false;
	var isActiveOnApp  = false;
	Message.findOne({
		from : pid,
		cat  : {
			$gt : timePeriod.start,
			$lt : timePeriod.end
		}
	},function(err,appMsg){
		if(err){
			return cb(err,null)
		}
		if(!appMsg){
			return cb(null,{pid:pid,act_info:{app:isActiveOnApp,group:isActiveOnGrp}});
		}
		isActiveOnApp = true;
		Message.findOne({
			from : pid,
			to   : gid,
			cat  : {
				$gt : timePeriod.start,
				$lt : timePeriod.end
			}
		},function(err,grpMsg){
			if(err){
				return cb(err,null)
			}
			if(!grpMsg){
				return cb(null,{pid:pid,act_info:{app:isActiveOnApp,group:isActiveOnGrp}});
			}
			isActiveOnGrp = true;
			return cb(null,{pid:pid,act_info:{app:isActiveOnApp,group:isActiveOnGrp}});
		});
	});
}

function roleBasedAnalysis(total_gms_rows, profilesById, accountsById, groupmembers, groupmembersByPid){
	for(var n=0; n<total_gms_rows; n++){
		var profile  = profilesById[groupmembers[n].pid+''];
		var account  = accountsById[profile.aid+''];
		var grpMem   = groupmembersByPid[profile._id+''];
		var grpMemObj = {
			name      : profile.nam,
			mobile    : account.m,
			acc_cat   : account.cat,
			prof_cat  : profile.cat,
			grp_join_at   : grpMem.cat
		};

		switch(groupmembers[n].type){

			case GroupMember.TYPE.MEMBER:
				member_info.TYPE_MEMBER.total++;
				member_info.TYPE_MEMBER.members.push(grpMemObj);
			break;

			case GroupMember.TYPE.ADMIN:
				member_info.TYPE_ADMIN.total++;
				member_info.TYPE_ADMIN.members.push(grpMemObj);
			break;

			case GroupMember.TYPE.BANNED:
				member_info.TYPE_BANNED.total++;
				member_info.TYPE_BANNED.members.push(grpMemObj);
			break;
		}
	}
}

function deviceBasedAnalysis(total_prof_rows, profiles, accountsById, groupmembersByPid, pidActInfo){
	for(var m=0; m<total_prof_rows; m++){
		var profile = profiles[m];
		var pid     = profile._id+'';
		var account = accountsById[profile.aid+''];
		var grpMem  = groupmembersByPid[profile._id+''];
		var grpMemObj = {
			name      : profile.nam,
			mobile    : account.m,
			acc_cat   : account.cat,
			prof_cat  : profile.cat,
			grp_join_at : grpMem.cat
		};

		switch(profile.role){

			case Profile.TYPE.TEACHER:
				member_info.teachers.total++;
				if(pidActInfo[pid].group){
					member_info.teachers.active++;
				}
				member_info.teachers.members.push(grpMemObj);
				if(typeof(account.iosv) !== 'undefined'){
					member_info.teachers.ios++;
					member_info.ios.total++;
					member_info.ios.teachers.total++;
					member_info.ios.teachers.members.push(grpMemObj);
					if(pidActInfo[pid].group){
						member_info.ios.active++;
						member_info.ios.teachers.active++;
					}
				}
				if(typeof(account.vrsn) !== 'undefined'){
					member_info.teachers.android++;
					member_info.android.total++;
					member_info.android.teachers.total++;
					member_info.android.teachers.members.push(grpMemObj);
					if(pidActInfo[pid].group){
						member_info.android.active++;
						member_info.android.teachers.active++;
					}
				}
				if(typeof(account.webv) !== 'undefined'){
					member_info.teachers.web++;
					member_info.web.total++;
					member_info.web.teachers.total++;
					member_info.web.teachers.members.push(grpMemObj);
					if(pidActInfo[pid].group){
						member_info.web.active++;
						member_info.web.teachers.active++;
					}
				}
			break;

			case Profile.TYPE.STUDENT:
				member_info.students.total++;
				if(pidActInfo[pid].group){
					member_info.students.active++;
				}
				member_info.students.members.push(grpMemObj);
				if(typeof(account.iosv) !== 'undefined'){
					member_info.students.ios++;
					member_info.ios.total++;
					member_info.ios.students.total++;
					member_info.ios.students.members.push(grpMemObj);
					if(pidActInfo[pid].group){
						member_info.ios.active++;
						member_info.ios.students.active++;
					}
				}
				if(typeof(account.vrsn) !== 'undefined'){
					member_info.students.android++;
					member_info.android.total++;
					member_info.android.students.total++;
					member_info.android.students.members.push(grpMemObj);
					if(pidActInfo[pid].group){
						member_info.android.active++;
						member_info.android.students.active++;
					}
				}
				if(typeof(account.webv) !== 'undefined'){
					member_info.students.web++;
					member_info.web.total++;
					member_info.web.students.total++;
					member_info.web.students.members.push(grpMemObj);
					if(pidActInfo[pid].group){
						member_info.web.active++;
						member_info.web.students.active++;
					}
				}
			break;

			case Profile.TYPE.PARENT:
				member_info.parents.total++;
				if(pidActInfo[pid].group){
					member_info.parents.active++;
				}
				member_info.parents.members.push(grpMemObj);
				if(typeof(account.iosv) !== 'undefined'){
					member_info.parents.ios++;
					member_info.ios.total++;
					member_info.ios.parents.total++;
					member_info.ios.parents.members.push(grpMemObj);
					if(pidActInfo[pid].group){
						member_info.ios.active++;
						member_info.ios.parents.active++;
					}
				}
				if(typeof(account.vrsn) !== 'undefined'){
					member_info.parents.android++;
					member_info.android.total++;
					member_info.android.parents.total++;
					member_info.android.parents.members.push(grpMemObj);
					if(pidActInfo[pid].group){
						member_info.android.active++;
						member_info.android.parents.active++;
					}
				}
				if(typeof(account.webv) !== 'undefined'){
					member_info.parents.web++;
					member_info.web.total++;
					member_info.web.parents.total++;
					member_info.web.parents.members.push(grpMemObj);
					if(pidActInfo[pid].group){
						member_info.web.active++;
						member_info.web.parents.active++;
					}
				}
			break;
		}
	}
}

function initializeData(){
	member_info.TOTAL_GROUPMEMBERS     = 0;
  member_info.TOTAL_UNIQUE_ACCOUNTS  = 0;
  member_info.TOTAL_ACTIVE_PROFILES_OF_THIS_GROUP_ACROSS_APP  = 0;
  member_info.TOTAL_ACTIVE_MEMBERS   = 0;

  member_info.TYPE_MEMBER            = {};
  member_info.TYPE_ADMIN             = {};
  member_info.TYPE_BANNED            = {};
  member_info.TYPE_MEMBER.total      = 0;
  member_info.TYPE_ADMIN.total       = 0;
  member_info.TYPE_BANNED.total      = 0;
  member_info.TYPE_MEMBER.members    = [];
  member_info.TYPE_BANNED.members    = [];
  member_info.TYPE_ADMIN.members     = [];

  member_info.teachers               = {};
  member_info.parents                = {};
  member_info.students               = {};
  member_info.teachers.total         = 0;
  member_info.parents.total          = 0;
  member_info.students.total         = 0;
  member_info.teachers.active        = 0;
  member_info.parents.active         = 0;
  member_info.students.active        = 0;

  member_info.teachers.android       = 0;
  member_info.parents.android        = 0;
  member_info.students.android       = 0;
  member_info.teachers.ios           = 0;
  member_info.parents.ios            = 0;
  member_info.students.ios           = 0;
  member_info.teachers.web           = 0;
  member_info.parents.web            = 0;
  member_info.students.web           = 0;

  member_info.teachers.members       = [];
  member_info.parents.members        = [];
  member_info.students.members       = [];

  member_info.android                = {};
  member_info.ios                    = {};
  member_info.web                    = {};
  member_info.android.total          = 0;
  member_info.ios.total              = 0;
  member_info.web.total              = 0;
  member_info.android.active          = 0;
  member_info.ios.active              = 0;
  member_info.web.active              = 0;

  member_info.android.teachers       = {};
  member_info.ios.teachers           = {};
  member_info.web.teachers           = {};
  member_info.android.students       = {};
  member_info.ios.students           = {};
  member_info.web.students           = {};
  member_info.android.parents        = {};
  member_info.ios.parents            = {};
  member_info.web.parents            = {};

  member_info.android.teachers.total    = 0;
  member_info.ios.teachers.total        = 0;
  member_info.web.teachers.total        = 0;
  member_info.android.students.total    = 0;
  member_info.ios.students.total        = 0;
  member_info.web.students.total        = 0;
  member_info.android.parents.total     = 0;
  member_info.ios.parents.total         = 0;
  member_info.web.parents.total         = 0;

  member_info.android.teachers.active    = 0;
  member_info.ios.teachers.active        = 0;
  member_info.web.teachers.active        = 0;
  member_info.android.students.active    = 0;
  member_info.ios.students.active        = 0;
  member_info.web.students.active        = 0;
  member_info.android.parents.active     = 0;
  member_info.ios.parents.active         = 0;
  member_info.web.parents.active         = 0;

  member_info.android.teachers.members       = [];
  member_info.ios.teachers.members           = [];
  member_info.web.teachers.members           = [];
  member_info.android.students.members       = [];
  member_info.ios.students.members           = [];
  member_info.web.students.members           = [];
  member_info.android.parents.members        = [];
  member_info.ios.parents.members            = [];
  member_info.web.parents.members            = [];
}

function startScript(){
  console.log(colors.blue('-------------------------------Member List Analysis----------------------------------'));
  console.log('\n');
}

function DataInFormOfTerminalShell(){
	displayOptions();
	rL.question(colors.rainbow("Apna option daalo BE***CH*D : "), function(code) {
		if(code == 0 || code == '0')
			endScript();
		if(isNaN(code)) {
			console.log('');
			console.log(colors.rainbow('You Moron, you entered a wrong option , BE***CH**D dhang ka option daal'));
			return DataInFormOfTerminalShell();
		}
		code = parseInt(code);
		if(code < 0 || code > 13){
			console.log('');
			console.log(colors.rainbow('Fuck you Nalle, you entered a wrong option , MA***CH**D dhang ka option daal'));
			return DataInFormOfTerminalShell();
		}
		displayDataForThisTerminalOption(code);
	});
}

function displayDataForThisTerminalOption(code){
	switch (code) {

		case 0:
			endScript();
		break;

		case 1:
			console.log('');
			console.log('                '+colors.red.underline('                TOTAL GROUP MEMBERS COUNT              '));
			console.log('');
			console.log('     '+colors.red(member_info.TOTAL_GROUPMEMBERS));
			return DataInFormOfTerminalShell();
		break;

		case 2:
			console.log('');
			console.log('                '+colors.red.underline('                TOTAL UNIQUE MEMBERS COUNT                '));
			console.log('');
			console.log('     '+colors.red(member_info.TOTAL_UNIQUE_ACCOUNTS));
			return DataInFormOfTerminalShell();
		break;

		case 3:
			console.log('');
			console.log('                '+colors.red.underline('               TOTAL ACTIVE GROUP MEMBERS COUNT            '));
			console.log('');
			console.log('     '+colors.red(member_info.TOTAL_ACTIVE_MEMBERS));
			return DataInFormOfTerminalShell();
		break;

		case 4:
			console.log('');
			console.log('                '+colors.red.underline('            TOTAL COUNT OF MEMBERS OF THIS GROUP ACTIVE ACROSS APP           '));
			console.log('');
			console.log('     '+colors.red(member_info.TOTAL_ACTIVE_PROFILES_OF_THIS_GROUP_ACROSS_APP));
			return DataInFormOfTerminalShell();
		break;

		case 5:
			console.log('');
			console.log('                '+colors.red.underline('              TOTAL NORMAL MEMBERS IN GROUP              '));
			console.log('');
			var tableData = getTabularData(member_info.TYPE_MEMBER.members);
			console.log(tableData);
			console.log('');
			console.log(colors.green('         TOTAL NORMAL GROUP MEMBERS COUNT      ==    '+member_info.TYPE_MEMBER.total));
			// console.log(util.inspect(member_info.TYPE_MEMBER, false, null));
			return DataInFormOfTerminalShell();
		break;

		case 6:
			console.log('');
			console.log('                '+colors.red.underline('              TOTAL ADMIN MEMBERS IN GROUP                 '));
			console.log('');
			var tableData = getTabularData(member_info.TYPE_ADMIN.members);
			console.log(tableData);
			console.log('');
			console.log(colors.green('         TOTAL ADMIN GROUP MEMBERS COUNT      ==    '+member_info.TYPE_ADMIN.total));
			// console.log(util.inspect(member_info.TYPE_ADMIN, false, null));
			return DataInFormOfTerminalShell();
		break;

		case 7:
			console.log('');
			console.log('                '+colors.red.underline('                 TOTAL BANNED MEMBERS IN GROUP              '));
			console.log('');
			var tableData = getTabularData(member_info.TYPE_BANNED.members);
			console.log(tableData);
			console.log('');
			console.log(colors.green('         TOTAL BANNED GROUP MEMBERS COUNT      ==    '+member_info.TYPE_BANNED.total));
			// console.log(util.inspect(member_info.TYPE_BANNED, false, null));
			return DataInFormOfTerminalShell();
		break;

		case 8:
			console.log('');
			console.log('                '+colors.red.underline('                     TEACHERS IN GROUP                     '));
			console.log('');
			var tableData = getTabularData(member_info.teachers.members);
			console.log(tableData);
			console.log('');
			console.log(colors.green('         TOTAL TEACHERS IN GROUP      ==    '+member_info.teachers.total));
			console.log('');
			console.log(colors.green('         TOTAL ACTIVE TEACHERS      ==    '+member_info.teachers.active));
			console.log('');
			console.log(colors.green('         TOTAL TEACHERS ON ANDROID      ==    '+member_info.teachers.android));
			console.log('');
			console.log(colors.green('         TOTAL TEACHERS ON IOS      ==    '+member_info.teachers.ios));
			console.log('');
			console.log(colors.green('         TOTAL TEACHERS ON WEB      ==    '+member_info.teachers.web));
			// console.log(util.inspect(member_info.teachers, false, null));
			return DataInFormOfTerminalShell();
		break;

		case 9:
			console.log('');
			console.log('                '+colors.red.underline('                     STUDENTS IN GROUP                     '));
			console.log('');
			var tableData = getTabularData(member_info.students.members);
			console.log(tableData);
			console.log('');
			console.log(colors.green('         TOTAL STUDENTS IN GROUP      ==    '+member_info.students.total));
			console.log('');
			console.log(colors.green('         TOTAL ACTIVE STUDENTS      ==    '+member_info.students.active));
			console.log('');
			console.log(colors.green('         TOTAL STUDENTS ON ANDROID      ==    '+member_info.students.android));
			console.log('');
			console.log(colors.green('         TOTAL STUDENTS ON IOS      ==    '+member_info.students.ios));
			console.log('');
			console.log(colors.green('         TOTAL STUDENTS ON WEB      ==    '+member_info.students.web));
			// console.log(util.inspect(member_info.students, false, null));
			return DataInFormOfTerminalShell();
		break;

		case 10:
			console.log('');
			console.log('                '+colors.red.underline('                     PARENTS IN GROUP                  '));
			console.log('');
			var tableData = getTabularData(member_info.parents.members);
			console.log(tableData);
			console.log('');
			console.log(colors.green('         TOTAL PARENTS IN GROUP      ==    '+member_info.parents.total));
			console.log('');
			console.log(colors.green('         TOTAL ACTIVE PARENTS      ==    '+member_info.parents.active));
			console.log('');
			console.log(colors.green('         TOTAL PARENTS ON ANDROID      ==    '+member_info.parents.android));
			console.log('');
			console.log(colors.green('         TOTAL PARENTS ON IOS      ==    '+member_info.parents.ios));
			console.log('');
			console.log(colors.green('         TOTAL PARENTS ON WEB      ==    '+member_info.parents.web));
			// console.log(util.inspect(member_info.parents, false, null));
			return DataInFormOfTerminalShell();
		break;

		case 11:
			console.log('');
			console.log('                '+colors.red.underline('                    MEMBERS ON ANDROID                  '));
			console.log('');
			var tableDataT = getTabularData(member_info.android.teachers.members);
			var tableDataS = getTabularData(member_info.android.students.members);
			var tableDataP = getTabularData(member_info.android.parents.members);
			console.log('');
			console.log(colors.green('         TOTAL ANDROID MEMBERS IN GROUP      ==    '+member_info.android.total));
			console.log('');
			console.log(colors.green('         TOTAL ACTIVE ANDROID MEMBERS IN GROUP      ==    '+member_info.android.active));
			console.log('');
			console.log(colors.rainbow('                                            TEACHERS'));
			console.log(tableDataT);
			console.log('');
			console.log(colors.green('         TOTAL TEACHERS ON ANDROID      ==    '+member_info.android.teachers.total));
			console.log('');
			console.log(colors.green('         TOTAL ACTIVE TEACHERS ON ANDROID      ==    '+member_info.android.teachers.active));
			console.log('');
			console.log(colors.rainbow('                                            STUDENTS'));
			console.log(tableDataS);
			console.log('');
			console.log(colors.green('         TOTAL STUDENTS ON ANDROID      ==    '+member_info.android.students.total));
			console.log('');
			console.log(colors.green('         TOTAL ACTIVE STUDENTS ON ANDROID      ==    '+member_info.android.students.active));
			console.log('');
			console.log(colors.rainbow('                                            PARENTS'));
			console.log(tableDataP);
			console.log('');
			console.log(colors.green('         TOTAL PARENTS ON ANDROID      ==    '+member_info.android.parents.total));
			console.log('');
			console.log(colors.green('         TOTAL ACTIVE PARENTS ON ANDROID      ==    '+member_info.android.parents.active));
			// console.log(util.inspect(member_info.android, false, null));
			return DataInFormOfTerminalShell();
		break;

		case 12:
			console.log('');
			console.log('                '+colors.red.underline('                    MEMBERS ON IOS                       '));
			console.log('');
			var tableDataT = getTabularData(member_info.ios.teachers.members);
			var tableDataS = getTabularData(member_info.ios.students.members);
			var tableDataP = getTabularData(member_info.ios.parents.members);
			console.log('');
			console.log(colors.green('         TOTAL IOS MEMBERS IN GROUP      ==    '+member_info.ios.total));
			console.log('');
			console.log(colors.green('         TOTAL ACTIVE IOS MEMBERS IN GROUP      ==    '+member_info.ios.active));
			console.log('');
			console.log(colors.rainbow('                                            TEACHERS'));
			console.log(tableDataT);
			console.log('');
			console.log(colors.green('         TOTAL TEACHERS ON IOS      ==    '+member_info.ios.teachers.total));
			console.log('');
			console.log(colors.green('         TOTAL ACTIVE TEACHERS ON IOS      ==    '+member_info.ios.teachers.active));
			console.log('');
			console.log(colors.rainbow('                                            STUDENTS'));
			console.log(tableDataS);
			console.log('');
			console.log(colors.green('         TOTAL STUDENTS ON IOS      ==    '+member_info.ios.students.total));
			console.log('');
			console.log(colors.green('         TOTAL ACTIVE STUDENTS ON IOS      ==    '+member_info.ios.students.active));
			console.log('');
			console.log(colors.rainbow('                                            PARENTS'));
			console.log(tableDataP);
			console.log('');
			console.log(colors.green('         TOTAL PARENTS ON IOS      ==    '+member_info.ios.parents.total));
			console.log('');
			console.log(colors.green('         TOTAL ACTIVE PARENTS ON IOS      ==    '+member_info.ios.parents.active));
			// console.log(util.inspect(member_info.ios, false, null));
			return DataInFormOfTerminalShell();
		break;

		case 13:
			console.log('');
			console.log('                '+colors.red.underline('                    MEMBERS ON WEB                       '));
			console.log('');
			var tableDataT = getTabularData(member_info.web.teachers.members);
			var tableDataS = getTabularData(member_info.web.students.members);
			var tableDataP = getTabularData(member_info.web.parents.members);
			console.log('');
			console.log(colors.green('         TOTAL WEB MEMBERS IN GROUP      ==    '+member_info.web.total));
			console.log('');
			console.log(colors.green('         TOTAL ACTIVE WEB MEMBERS IN GROUP      ==    '+member_info.web.active));
			console.log('');
			console.log(colors.rainbow('                                            TEACHERS'));
			console.log(tableDataT);
			console.log('');
			console.log(colors.green('         TOTAL TEACHERS ON WEB      ==    '+member_info.web.teachers.total));
			console.log('');
			console.log(colors.green('         TOTAL ACTIVE TEACHERS ON WEB      ==    '+member_info.web.teachers.active));
			console.log('');
			console.log(colors.rainbow('                                            STUDENTS'));
			console.log(tableDataS);
			console.log('');
			console.log(colors.green('         TOTAL STUDENTS ON WEB      ==    '+member_info.web.students.total));
			console.log('');
			console.log(colors.green('         TOTAL ACTIVE STUDENTS ON WEB      ==    '+member_info.web.students.active));
			console.log('');
			console.log(colors.rainbow('                                            PARENTS'));
			console.log(tableDataP);
			console.log('');
			console.log(colors.green('         TOTAL PARENTS ON WEB      ==    '+member_info.web.parents.total));
			console.log('');
			console.log(colors.green('         TOTAL ACTIVE PARENTS ON WEB      ==    '+member_info.web.parents.active));
			// console.log(util.inspect(member_info.web, false, null));
			return DataInFormOfTerminalShell();
		break;
	}
}

function displayOptions(){
	console.log('\n');
	console.log('    '+colors.blue.underline('Option Num')+'      '+colors.blue.underline('     Option Description                                   '));
	console.log('');
	console.log(colors.blue('        1      :        TOTAL GROUP MEMBERS COUNT    '));
	console.log(colors.blue('        2      :        TOTAL UNIQUE ACCOUNTS COUNT    '));
	console.log(colors.blue('        3      :        TOTAL ACTIVE GROUP MEMBERS COUNT    '));
	console.log(colors.blue('        4      :        TOTAL PROFILES OF THIS GROUP ACTIVE ON ACROSS APP    '));
	console.log(colors.blue('        5      :        TOTAL GROUP MEMBERS OF TYPE MEMBER    '));
	console.log(colors.blue('        6      :        TOTAL GROUP MEMBERS OF TYPE ADMIN    '));
	console.log(colors.blue('        7      :        TOTAL GROUP MEMBERS OF TYPE BANNED    '));
	console.log(colors.blue('        8      :        ANALYZE DATA OF TEACHERS  '));
	console.log(colors.blue('        9      :        ANALYZE DATA OF STUDENTS  '));
	console.log(colors.blue('        10     :        ANALYZE DATA OF PARENTS  '));
	console.log(colors.blue('        11     :        ANALYZE DATA OF ANDROID MEMBERS  '));
	console.log(colors.blue('        12     :        ANALYZE DATA OF IOS MEMBERS  '));
	console.log(colors.blue('        13     :        ANALYZE DATA OF WEB MEMBERS  '));
	console.log(colors.blue('        0      :        EXIT THIS MIGHTLY PROGRAM    '));
	console.log('\n');
}

function getTabularData(data){
	var table = new Table({
		head : ['Name','Mobile','Account_Created_At','Profile_Created_At','Group_Joined_At'],
		colWidths : [15,15,25,25,25]
	});
	var total = data.length;
	for(var i=0; i<total; i++){
		var member = data[i];
		var tableRow = [];
		tableRow[0] = member['name'];
		tableRow[1] = member['mobile'];
		tableRow[2] = member['acc_cat'];
		tableRow[3] = member['prof_cat'];
		tableRow[4] = member['grp_join_at'];
		table.push(tableRow);
	}
	return table.toString();
}

function endScript(){
	console.log(colors.blue('----------------------Member List Analysis Script ended------------------------'));
	console.log('\n');
	process.exit(0);
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : 120
      }
    },
    replset : {
      rs_name : configs.MONGO_REPLICA_SET_NAME,
      socketOptions : {
        keepAlive : 120
      }
    }
  };

  if(configs.MONGO_UNAME != "") {
    var connect_string = "mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@";
    var mongo_hosts = configs.MONGO_HOST;
    var mongo_port = configs.MONGO_PORT;
    for(var i=0; i<mongo_hosts.length; i++){
      if(i > 0)
        connect_string += ",";
      connect_string += mongo_hosts[i]+":"+mongo_port;
    }
    connect_string += "/"+configs.DB_NAME,options;
    mongoose.connect(connect_string);
    // mongoose.connect( "mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@"+"127.0.0.1"+":"+configs.MONGO_PORT+"/"+configs.DB_NAME,options);
  } else {
    mongoose.connect("mongodb://"+configs.MONGO_HOST+"/"+configs.DB_NAME,options);
  }
}

