var socketio            = require('socket.io');
var mongoose            = require('mongoose');
var validator           = require('validator');
var kue                 = require('kue');
var redis               = require('redis');
var _                   = require('underscore');
var readline 			= require('readline');

var tokenValidator      = require('./../utility/token_validator.js');
var configs             = require('./../utility/configs.js');

var redis_client        = redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
    console.log("Error " + err);
});

var SOCKET_PREFIX       = "s";
var S_COUNT_PREFIX      = "seen_count";
var ADMIN_SEEN_PREFIX   = "seen";

var rmc                 = require("./../utility/redis_mongo_cache.js")(redis_client);
var rcq                 = require("./../utility/redis_chat_queue.js")(redis_client);
var rsk                 = require("./../utility/redis_socket_key.js")(redis_client,SOCKET_PREFIX);
var rseencount          = require("./../utility/redis_key_store.js")(redis_client,S_COUNT_PREFIX);
var rcustomq            = require("./../utility/redis_custom_queue.js")(redis_client);

var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.question("Please enter the account id you want to investigate: ", function(aid) {
	scan_account_queue(aid);
	rl.close();
});

var scan_account_queue = function(aid) {
	rcustomq.scanall(ADMIN_SEEN_PREFIX+":"+aid,function(err,seens){
		if(err) {
			console.trace(err);
			return;
		}

		console.log("There are about "+seens[1].length/2 + " messages in the user's queue");

		console.log("\nThe dump is as follows");
		// console.log(JSON.stringify(seens));
	});
};