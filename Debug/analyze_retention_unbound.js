var momentTZ				    = require('moment-timezone');
var moment				      = require('moment');
var mongoose            = require('mongoose');
var _                   = require('underscore');
var readline 			      = require('readline');

var configs             = require('./../utility/configs.js');

var connect = function () {
  var options = {server : {socketOptions : {keepAlive : 1}}};
  if(configs.MONGO_UNAME!="") {
    mongoose.connect("mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@"+configs.MONGO_HOST+"/"+configs.DB_NAME,options);
  } else {
    mongoose.connect("mongodb://"+configs.MONGO_HOST+"/"+configs.DB_NAME,options);
  }
}
connect();
mongoose.connection.on('error',console.log);
mongoose.connection.on('disconnected', connect);

require('./../models/accounts.js');
require('./../models/groupmembers.js');
require('./../models/groups.js');
var Account 					   	= mongoose.model('Account');
var GroupMember 				 	= mongoose.model('GroupMember');
var Group 								= mongoose.model('Group');

var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

var allemails = [];
if(process.argv.length > 2) {
	execute(process.argv[2]);
} else {
	rl.question("Please enter your boundary date in IST (hh:mm:ss dd:mm:yy) ? : ", function(dates) {
		execute(dates);
	});
}

function execute (dates) {
	var start = dates.split("-")[0].trim();

	var start_ts = momentTZ(start,"HH:mm:ss DD:MM:YYYY").tz(configs.MOMENT_TIMEZONE).valueOf();
	// var end_ts = momentTZ(end,"HH:mm:ss DD:MM:YYYY").tz(configs.MOMENT_TIMEZONE).valueOf();

	var start_rev = momentTZ(start_ts).tz(configs.MOMENT_TIMEZONE).format();
	// var end_rev = momentTZ(end_ts).tz(configs.MOMENT_TIMEZONE).format();

	Account.find({
			$and : [
						{
							$or		: [ { lsnm	: { $exists:false } }, {lsnm : { $lt: new Date(start_ts) } } ]
						},
						{
							$or		: [ { lsni	: { $exists:false } }, {lsni : { $lt: new Date(start_ts) } } ]
						},
						{
							$or		: [ { lsnw	: { $exists:false } }, {lsnw : { $lt: new Date(start_ts) } } ]
						},
						{
							$or		: [ { lsnm	: { $exists:true } }, { lsni	: { $exists:true } }, { lsnw	: { $exists:true } } ]
						}
					]
	},{_id:1,m:1,lsnm:1,lsnw:1,lsni:1,e:1},function(err,accs) {
		if(!accs || accs.length == 0) {
					console.log("no valid accounts found for any account! So leaving!");
					process.exit();
		}

		console.log("Okay, so we got " + accs.length + " accounts who were live before " + start_rev);

		if(accs.length == 0) {
			console.log("There was no one who was live before this time, so bye!");
			process.exit();
		}

		var final_data = {};
		var acc_ids = [];
		accs.forEach(function(acc){
			acc_ids.push(acc["_id"])

			final_data[acc._id] = {};
			final_data[acc._id]["_id"] = acc["_id"];
			final_data[acc._id]["m"] = acc["m"];
			final_data[acc._id]["e"] = acc["e"];
			final_data[acc._id]["grps"] = [];
			final_data[acc._id]["gnames"] = [];

			var dah = [];
			if(acc["lsnm"]) {
				dah.push(acc["lsnm"]);
			}

			if(acc["lsni"]) {
				dah.push(acc["lsni"]);
			}

			if(acc["lsnw"]) {
				dah.push(acc["lsnw"]);
			}

			final_data[acc._id]["time"] = dah.join(",");
		});

		GroupMember.find({
			aid : {$in : acc_ids},
			act : true
		},function(err,gms){
				if(!gms || gms.length == 0) {
					console.log("no subscribed groups found for any account!");

					output_final_data(final_data,function(){
						console.log("All finished! Exiting!");
						process.exit();
					});

					process.exit();
				}

				var gids = [];

				gms.forEach(function(gm){
					gids.push(gm.gid + "");
				});

				gids = _.uniq(gids);

				Group.find({
					_id : {$in : gids}
				},{_id:1,name:1},function(err,grps){
					var grps_by_ids = {};

					grps.forEach(function(grp){
						grps_by_ids[grp._id] = grp;
					});

					var gms_by_aid = {};
					gms.forEach(function(gm){
							if(!gms_by_aid[gm["aid"]+""]) {
								gms_by_aid[gm["aid"]+""] = [];
							}
							gms_by_aid[gm["aid"]+""].push(gm);
					});

					accs.forEach(function(acc){
						if(gms_by_aid[acc._id + ""]){
							gms_by_aid[acc._id + ""].forEach(function(gm){	
								final_data[acc._id+""]["gnames"].push(grps_by_ids[gm.gid]["name"]);
							});
						}
					});

					output_final_data(final_data,function(){
						console.log("All finished! Exiting!");
						process.exit();
					});
				});
		});
	});

	rl.close();
};


function output_final_data(final_data,cb) {
	for(var aid in final_data) {
		var data = final_data[aid];
		console.log(data["m"] + " | " + data["e"].join(",") + " | " + data["gnames"].join(",") + " | " + data["time"]);
	}

	cb();
}
