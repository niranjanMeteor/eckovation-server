var fs = require('fs')
    , util = require('util')
    , stream = require('stream')
    , es = require('event-stream');

var moment                                            = require('moment-timezone');
var nodemailer                              = require('nodemailer');
var smtpTransport                         = require('nodemailer-smtp-transport');
var configs                                           = require('./../../utility/configs.js');
var os                = require('os');

var lineNr = 0;

/*
[2016-06-04 07:19:18.042] [ERROR] Socket - { e: 'hk_56fe573672d7cc76202738a6_SC',
  r: 1,
  d:
   { aid: '56fe573672d7cc76202738a6',
     tokn: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjU2ZmU1NzM2NzJkN2NjNzYyMDI3MzhhNiIsImNsIjoiQSIsImhjb2RlIjoxMjk0MTY5Nzc1NDQzMTUxLCJpYXQiOjE0NjUwMjQwNzUsImV4cCI6MTQ2NTAyNDEzNX0.YlHNGMp6a4zxnXlsVo8XEMZvz-tupCTEJxSU6R8HGUQ' },
  er:
   { [TokenExpiredError: jwt expired]
     name: 'TokenExpiredError',
     message: 'jwt expired',
     expiredAt: Sat Jun 04 2016 07:08:55 GMT+0000 (UTC) },
  s: 'wKh8HPDYWkiZUJ8BAAAB' }
[2016-06-04 07:19:18.783] [INFO] Socket - { e: 'hk_56fe573672d7cc76202738a6_SC',
  r: 4,
  s: 'nAf_W_M8SSJm8HfPAAAC' }
*/

var determine_latest_socket_log_file_name = function(dir) {
	var files = fs.readdirSync(dir);
	var time_log_name = {};
	var max_time = 0;

	for(var i = 0; i < files.length; i++) {
		var file_name = files[i];
		//events-2016_4_18.log
		var match = file_name.match(/events-(\d\d\d\d)_(.*)_(.*)\.log/);

		var yyyy = match[1];
		var mm = match[2];
		var dd = match[3];
		var datetime = new Date(yyyy+'-'+mm+'-'+dd).getTime();

		time_log_name[datetime+''] = files[i];

		if(datetime > max_time) {
			max_time = datetime;
		}
	}

	return time_log_name[max_time + '']
}

var BASE_DIR = false;
var LOG_FILE = false;

if(process.argv.length == 4) {
	BASE_DIR = process.argv[2].split("=")[1];
	LOG_FILE = process.argv[3].split("=")[1];
} else if (process.argv.length == 3) {
	BASE_DIR = process.argv[2].split("=")[1];
}

if(!BASE_DIR) {
	BASE_DIR = "/var/log/eckovation/socket";
}

var final_result = {};
final_result["SKIPPED"] = 0;

if(LOG_FILE!=false) {
	file_name = LOG_FILE;
} else {
	file_name = determine_latest_socket_log_file_name(BASE_DIR);
}


console.log("Going to scan " + file_name);

var s = fs.createReadStream(BASE_DIR+"/"+file_name)
		.pipe(es.split())
		.pipe(es.mapSync(function(line){
				//console.log(line);
				identify_log_header(line);
			})
			.on('error',function(){
	        		console.log('Error while reading file.');
		    	})
			.on('end', function(){
        			console.log('Read entire file.');
				console.log(final_result);

				var body = "<div>So basically I analyzed : " + BASE_DIR + "/" +file_name+"\n" + "</div>";
				body += "<pre>" + JSON.stringify(final_result,null,8)+"</pre>";

				inform_technical_team(body);
	    		})
		);

//var buffer = [];
//matches [2016-06-04 07:19:18.783]
var date_header_regex_identifier = /(\[\d{4}\-\d{2}\-\d{2}\s\d{2}\:\d{2}\:\d{2}\.\d{3}]).*/;
var only_date_identifier	 = /(\d\d\d\d\-\d\d\-\d\d).*/;
var event_name_identifier	 = /.*e: \'(.*)\'.*/;
//matches ERROR or INFO
var socket_event_result		 = /.*\[(.*)\] Socket.*/;
var identify_log_header = function(line) {
	var regex = new RegExp(date_header_regex_identifier);
	if(regex.test(line)) {
		var x = line.match(date_header_regex_identifier);
		if(x.length > 1) {
			//it has date heade
			var date_header  = line;
			var only_date; 
			try {
				only_date = date_header.match(only_date_identifier)[1];
			} catch(e) {
				console.log("Skipping a record. Couldn't parse the date");
				console.log(line);
				final_result["SKIPPED"]++;
				return;
			}

			var event_result;
			try {
				event_result = date_header.match(socket_event_result)[1];
			} catch(e) {
				console.log("Skipping a record.Could'nt parse the socket result.");
                                console.log(line);
                                final_result["SKIPPED"]++;
                                return;
			}

			var event_name;
			try {
				event_name = date_header.match(event_name_identifier)[1];
			} catch(e) {
				console.log("Skipping a record.Could'nt parse the event name");
				console.log(line);
                                final_result["SKIPPED"]++;
                                return;
			}

			event_name = refine_event_name(event_name);

			if(!final_result[only_date]) {final_result[only_date] = {}; }
			if(!final_result[only_date][event_name]) {final_result[only_date][event_name] = {}; }
			if(!final_result[only_date][event_name][event_result]) { final_result[only_date][event_name][event_result] = 0; }
			final_result[only_date][event_name][event_result]++;
		}
	}
}

//gm_ackc_21fa7c6bc6aaaf7e275b45dc93d2fde3ff90e81cb8242d047036b638e0cf6527 => gm_ackc
//init_56fe573672d7cc76202738a6 => init
//gm_ack_21fa7c6bc6aaaf7e275b45dc93d2fde3ff90e81cb8242d047036b638e0cf6527 => gm_ack
//and others as it is
var all_event_names = ["hk","init","g_m_w","g_m","gm_ackc","gm_ack","ad_seen_ack","ad_seen","lastseen","seen","created","disconnect","error"]
var refine_event_name = function(event_name) {

	for(var x = 0; x < all_event_names.length; x++) {
		if(event_name.indexOf(all_event_names[x])!=-1) {
			return all_event_names[x];
		}
	}

	if(event_name.indexOf("dcnt_") != -1) {
		return "dcnt_t+";
	}

	return event_name;
}

//[2016-06-04 07:19:18.042] [ERROR] Socket - { e: 'hk_56fe573672d7cc76202738a6_SC', => { e: 'hk_56fe573672d7cc76202738a6_SC',
var remove_everything_before_bracket_header = function(header) {
	var splits = header.split("{");
	if(splits.length < 2) { return false; }

	return "{" + splits[1];
}

var inform_technical_team = function(body) {
	var transporter = nodemailer.createTransport(smtpTransport({
	  host          : configs.ANALYTICS_SMTP_HOST,
	  port          : configs.ANALYTICS_SMTP_PORT,
	  secure                : true,
	  auth          : {
	      user: configs.ANALYTICS_SMTP_USERNAME,
	      pass: configs.ANALYTICS_SMTP_PASSWORD
	  }
	}));

	var mailOptions = {
		from: configs.TECH_FROM, // sender address
                to: configs.TECH_TO, // list of receivers
                subject: "(" + os.hostname() + ') Daily Socket Requests Analytics Report : '+ moment().tz(configs.MOMENT_TIMEZONE).format('dddd, Do of MMMM, YYYY'), // Subject line
                html: body, // plaintext body
        };
        transporter.sendMail(mailOptions, function(error, info){
        	if(error){
			return console.log(error);
		}
		console.log('\n\nMail Analytics\n**** *********')
		console.log(info);
		process.exit(0);
		return;
	});
}
