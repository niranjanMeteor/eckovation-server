var mongoose                = require('mongoose');
var redis                   = require('redis');
var readline                = require('readline');
var configs                 = require('./../utility/configs.js');
var RedisPrefixes           = require('./../utility/redis_prefix.js');

var redis_client            = redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
  console.trace("Redis Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});
mongoose.connection.on('connected', function(){
  console.log('Successfully Opened MOngoDb Connection');
  startScript();
  startUserAdminQueueTrace();
});

require('../models/accounts.js');
require('../models/profiles.js');
var Account               = mongoose.model('Account');
var Profile               = mongoose.model('Profile');

var rhmset                = require("./../utility/redis_hmset.js")(redis_client);

var rL = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

function startUserAdminQueueTrace(){
  rL.question("Enter User Mobile Number (10 digit authentic mobile number, 0 to EXIT) : ", function(mobile_number) {
    if(isNaN(mobile_number)) {
      console.log('Oops , you entered a wrong option , Please enter a right option');
      return startUserAdminQueueTrace();
    }
    if(mobile_number == 0){
      return endScript();
    }
    if(!isValidMobileNumber(mobile_number)){
      console.log('Ooops, The Mobile number you entered is not a valid Indian Mobile Number');
      return startUserAdminQueueTrace();
    }
    return getAdminQueueInfo(mobile_number);
  });
}

function getAdminQueueInfo(mobile_number){
  Account.findOne({
    m : mobile_number
  },function(err,account){
    if(err){
      console.trace(err);
      return;
    }
    if(!account){
      console.log('Sorry account for this mobile number does not exist on Eckovation');
      return startUserAdminQueueTrace();
    }
    Profile.find({
      aid : account._id
    },{
      _id : 1,
      nam : 1,
      role: 1,
      ppic: 1,
      act : 1,
      cat : 1
    },function(err,profiles){
      if(err){
        console.trace(err);
        return;
      }
      if(profiles.length == 0){
        console.log('Sorry no profiles exist for this mobile number does not exist on Eckovation');
        return startUserAdminQueueTrace();
      }
      return prepareAdminSeenInfo(profiles);
    });
  });
}

function prepareAdminSeenInfo(profiles, cb){
  var adminq_info                 = {};
  adminq_info.total_size          = 0;
  adminq_info.active_queue_size   = 0;
  adminq_info.inactive_queue_size = 0;
  adminq_info.active_queues       = [];
  adminq_info.inactive_queues     = [];
  var toBeDone          = profiles.length;
  var done              = 0;
  var pids              = [];
  var profilesById      = [];
  for(var j=0; j<toBeDone; j++){
    pids.push(profiles[j]._id);
    profilesById[profiles[j]._id+''] = profiles[j];
  }
  for(var i=0; i<toBeDone; i++){
    getLengthOfSeenQueue(pids[i],function(err,pidAndQueLen){
      if(err){
        console.trace(err);
        return;
      }
      done++;
      if(pidAndQueLen.qLen > 0){
        var key = pidAndQueLen.pid+'('+profilesById[pidAndQueLen.pid].nam+',role : '+profilesById[pidAndQueLen.pid].role+')';
        if(profilesById[pidAndQueLen.pid].act){
          adminq_info.active_queues[key] = pidAndQueLen.qLen;
          adminq_info.active_queue_size += pidAndQueLen.qLen;
          adminq_info.total_size += pidAndQueLen.qLen;
        } else {
          adminq_info.inactive_queues[key] = pidAndQueLen.qLen;
          adminq_info.inactive_queue_size += pidAndQueLen.qLen;
          adminq_info.total_size += pidAndQueLen.qLen;
        }
      }
      if(done == toBeDone){
        console.log(adminq_info);
        return endScript();
      }
    });
  }
}

function getLengthOfSeenQueue(pid, cb){
  var key = RedisPrefixes.SEEN_COUNT+pid;
  rhmset.exists(key,function(err,resp){
    if(err){
      return cb(err,null);
    }
    if(resp){
      rhmset.len(key,function(err,perPidQueLen){
        if(err){
          return cb(err,null);
        } 
        return cb(null, { pid:pid, qLen:perPidQueLen });
      });
    } else {
      return cb(null, { pid:pid, qLen:0 });
    }
  });
}

function isValidMobileNumber(value){
  if(value.length != 10) return false;
  var indian_mobile_num_regex = /^[789]\d{9}$/;
  return indian_mobile_num_regex.test(value);
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : 120
      }
    },
    replset : {
      rs_name : configs.MONGO_REPLICA_SET_NAME,
      socketOptions : {
        keepAlive : 120
      }
    }
  };

  if(configs.MONGO_UNAME != "") {
    var connect_string = "mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@";
    var mongo_hosts = configs.MONGO_HOST;
    var mongo_port = configs.MONGO_PORT;
    for(var i=0; i<mongo_hosts.length; i++){
      if(i > 0)
        connect_string += ",";
      connect_string += mongo_hosts[i]+":"+mongo_port;
    }
    connect_string += "/"+configs.DB_NAME,options;
    mongoose.connect(connect_string);
    // mongoose.connect( "mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@"+"127.0.0.1"+":"+configs.MONGO_PORT+"/"+configs.DB_NAME,options);
  } else {
    mongoose.connect("mongodb://"+configs.MONGO_HOST+"/"+configs.DB_NAME,options);
  }
}

function startScript(){
  console.log('-------------------------------User ADMIN QUEUE Trace Begins----------------------------------');
  console.log('You will get a chance to observe admin queues length for all profiles for a User');
}

function endScript(){
  console.log('We hope you were able to get all information that you were looking, Thanks very much for using our service');
  console.log('-------------------------------User ADMIN QUEUE Trace Finished----------------------------------');
  process.exit(0);
}