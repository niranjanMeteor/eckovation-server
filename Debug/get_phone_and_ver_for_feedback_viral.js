var moment				      = require('moment');
var momentTZ				    = require('moment-timezone');
var mongoose            = require('mongoose');
var _                   = require('underscore');
var readline 			      = require('readline');

var configs             = require('./../utility/configs.js');

var connect = function () {
  var options = {server : {socketOptions : {keepAlive : 1}}};
  if(configs.MONGO_UNAME!="") {
    mongoose.connect("mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@"+configs.MONGO_HOST+"/"+configs.DB_NAME,options);
  } else {
    mongoose.connect("mongodb://"+configs.MONGO_HOST+"/"+configs.DB_NAME,options);
  }
}
connect();
mongoose.connection.on('error',console.log);
mongoose.connection.on('disconnected', connect);

require('./../models/profiles.js');
require('./../models/groups.js');
require('./../models/accounts.js');
require('./../models/groupmembers.js');
require('./../models/messages.js');

var Profile            	 = mongoose.model('Profile');
var GroupMember 			   = mongoose.model('GroupMember');
var Group 					     = mongoose.model('Group');
var Account 					   = mongoose.model('Account');

var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

var countGroupMembers = function(gids,cb) {
	var segg = {};
	GroupMember.find({
		gid 	: {$in : gids},
		act   : true,
		gdel  : false
	},function(err,gms){
		if(err) { return cb(err,null); }

		gms.forEach(function(gm){
			if(!segg[gm["gid"]]) {
				segg[gm["gid"]] = 0;
			}

			segg[gm["gid"]]++;
		});

		cb(null,segg);
	});
};

rl.question("Please provide me with CSV of Group Codes : ", function(answer) {
	var codes = answer.split(",");
	var _codes = [];
	codes.forEach(function(code){
			code = code.trim();
			_codes.push(code);
	});
	codes = _codes;

	Group.find({
		code : {$in : codes}
	},function(err,grps){
		if(err || grps.length == 0) {
				console.log("No groups found. Exiting...");
				process.exit();
		}

		var gids = [];

		var grps_by_id = {};

		grps.forEach(function(grp){
			gids.push(grp._id);
			grps_by_id[grp._id] = grp;
		});

		GroupMember.find({
			cat 	: {$gt:new Date('2016-07-25T00:05:30.000Z')},
			gid 	: {$in : gids},
		//	act   : true,
			gdel  : false
		},function(err,gms){
			if(err || gms.length == 0) {
				console.log("No members found. Exiting...");
				process.exit();
			}

			var pids = [];
			var aids = [];
			gms.forEach(function(gm){
				pids.push(gm.pid);
				aids.push(gm.aid + "");
			});

			aids = _.uniq(aids);

			var aids_wise_gm_count = [];
			var processed_aids = [];

			aids.forEach(function(aid){
				GroupMember.count({
					"aid" : aid
				},function(err,gms_count){
					if(err) {
						console.log(err);
						return;
					}

					aids_wise_gm_count.push({
						"aid" : aid,
						"count": gms_count
					});

					processed_aids.push(aid);

					if(processed_aids.length >= aids.length) {
						aids_wise_gm_count = _.sortBy(aids_wise_gm_count,function(o){
								return o.count
						});

						console.log(aids_wise_gm_count);
						console.log("Exiting..");
						process.exit();
					}
				});
			});	
		});
	});

	rl.close();
});
