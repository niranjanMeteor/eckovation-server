var mongoose            = require('mongoose');
var redis               = require('redis');
var moment					    = require('moment-timezone');
var configs             = require('./../utility/configs.js');
var fs                  = require('fs');

require('../models/profiles.js');
var Profile             = mongoose.model('Profile');

var redis_client        = redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });
redis_client.on("error", function (err) {
    console.log("Error " + err);
});

var rcq                 = require("./../utility/redis_chat_queue.js")(redis_client);
var options             = {server : {socketOptions : {keepAlive : 1}}};

mongoose.connect("mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@"+configs.MONGO_HOST+"/"+configs.DB_NAME,options, function(err){
	if(err) {
		console.log(err);
		console.trace(err);
	} else {
		console.log('Connected to mongodb!');
	}
});


exports.check_profile_msg_status = function() {
	Profile.find({act:true}, function(err, profiles) {
		if (err) {
			console.log('Error From Db Query');
			console.trace(err);
		} else {
				var filename = __dirname+"/trash/Profile_Analytics_"+moment().format();
				console.log('filename',filename);
				for (var i = 0; i < profiles.length; i++) {
					callAsyncFunction(profiles[i]._id, i, filename);
				}
		}
	});
};

function callAsyncFunction(profileId, i, filename) {
	setTimeout(function() {
		scan_profile_pending_queue(profileId, function(err, output) {
			if (err) {
				console.trace(err);
			} else {
				console.log(output);
				fs.appendFile(filename+'', output+'\n', function (err) {
					if (err) {
						console.log('Writing to Analytics file failed');
					}
				});
			}
		});
	}, i*100);
};

function scan_profile_pending_queue(profile_id, cb) {
	rcq.getall(profile_id,function(err,profile_msgs){
		if(err) {
			cb(err,null);
		} else {
			cb(null,'profile_id:'+profile_id+',Messages:'+profile_msgs.length);
		}
	});
};