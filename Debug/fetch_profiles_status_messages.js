var moment				      = require('moment-timezone');
var mongoose            = require('mongoose');
var _                   = require('underscore');
var readline 			      = require('readline');
var configs             = require('./../utility/configs.js');

var connect = function () {
  var options = {server : {socketOptions : {keepAlive : 1}}};
  if(configs.MONGO_UNAME!="") {
    mongoose.connect("mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@"+configs.MONGO_HOST+"/"+configs.DB_NAME,options);
  } else {
    mongoose.connect("mongodb://"+configs.MONGO_HOST+"/"+configs.DB_NAME,options);
  }
}
connect();
mongoose.connection.on('error',console.log);
mongoose.connection.on('disconnected', connect);

require('./../models/profiles.js');
require('./../models/groups.js');
require('./../models/accounts.js');
require('./../models/groupmembers.js');
require('./../models/messages.js');
require('./../models/statuses.js');

var Profile            	= mongoose.model('Profile');
var GroupMember 			  = mongoose.model('GroupMember');
var Group 					    = mongoose.model('Group');
var Account 				    = mongoose.model('Account');
var Message 				    = mongoose.model('Message');
var Status 					    = mongoose.model('Status');

var active_initlog_users = require('../analytics/activeInitlogUsers.js');

var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.question("Please tell me the message which you like to investigate : ", function(mid) {
	get_total_count(mid,function(names){
		console.log("The combined list of all the profile names who should have received this message are as follows");
		console.log(names);

		console.log("Total maximum possible receiver count : " + names.length);

		get_ideal_member_count_that_time(mid,function(cnt){
			console.log("Total ideal possible receiver count : " + cnt);

			process.exit();
			return;
		});
	});

	rl.close();
});

var get_ideal_member_count_that_time = function(mid,cb) {
	Message.findOne({
		_id : mid
	},function(err,msg){
		var msg_cat = msg["cat"];

		GroupMember.find({
			pid : {$ne : msg.from},
			gid : msg.to,
			cat : {$lte : msg_cat},
			act : true,
			gdel: false
		},function(err,gms){
			cb(gms.length);
		});
	});
}

var get_total_count = function(mid,cb) {
	Status.find({
		mid : mid,
	},function(err,stts){
		var pids = [];

		var stts_by_pid = {};

		stts.forEach(function(status){
			pids.push(status["pid"]);

			stts_by_pid[status["pid"]] = status;
		});

		Profile.find({
			_id : {
				$in : pids,
			},
			act : true
		},function(err,prfls){
			var names = [];

			prfls.forEach(function(prfl){
				names.push(prfl["_id"]+","+prfl["nam"]+","+stts_by_pid[prfl["_id"]]["stts"]);
			});

			cb(names);
		});
	});
};