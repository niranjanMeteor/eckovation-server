var moment				      = require('moment-timezone');
var mongoose            = require('mongoose');
var _                   = require('underscore');
var readline 			      = require('readline');

var configs             = require('./../utility/configs.js');

var regions             = require('./../utility/check_region_by_mobile.js');

require('../models/accounts.js');
require('../models/profiles.js');
require('../models/groups.js');
require('../models/groupmembers.js');
require('../models/messages.js');
require('../models/statuses.js');
var Account               = mongoose.model('Account');
var Profile               = mongoose.model('Profile');
var Group                 = mongoose.model('Group');
var GroupMember           = mongoose.model('GroupMember');
var Message               = mongoose.model('Message');
var Status                = mongoose.model('Status');

var connect = function () {
  var options = {server : {socketOptions : {keepAlive : 1}}};
  if(configs.MONGO_UNAME!="") {
    mongoose.connect("mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@"+configs.MONGO_HOST+"/"+configs.DB_NAME,options);
  } else {
    mongoose.connect("mongodb://"+configs.MONGO_HOST+"/"+configs.DB_NAME,options);
  }
}
connect();
mongoose.connection.on('error',console.log);
mongoose.connection.on('disconnected', connect);

var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.question("Please tell me date in dd/mm/yyy: ", function(answer) {
	var dd = answer.trim().split("/")[0];
	var mm = answer.trim().split("/")[1];
	var yy = answer.trim().split("/")[2];

	var start_time 	= moment(dd+"-"+mm+"-"+yy, ["DD-MM-YYYY"]).tz(configs.MOMENT_TIMEZONE).valueOf();
	var end_time 		= moment(dd+"-"+mm+"-"+yy, ["DD-MM-YYYY"]).tz(configs.MOMENT_TIMEZONE).endOf("day").valueOf();

	var count = {};
	count["Unknown"] = 0;

	Account.find({
		vrfy : true,
		cat  : {
			'$gte' 	: new Date(start_time),
			'$lte'	: new Date(end_time)
		}
	},function(err,accs){
		if(err) {console.log(err);process.exit();return;}

		if(!accs) {
			console.log("Sorry did not find anything!");
			process.exit();
			return;
		}
		console.log("Processing data...");
		accs.forEach(function(acc){
			if(regions[acc["m"].substr(0,4)]) {
				if(!count[regions[acc["m"].substr(0,4)]]) { count[regions[acc["m"].substr(0,4)]] = 0; }
				count[regions[acc["m"].substr(0,4)]]++;
			} else {
				count["Unknown"]++;
			}
		});
		console.log("Here are the results for the above duration: ");
		console.log(count);
		console.log("Total accounts: " + accs.length)
		process.exit();
	});

	rl.close();
});

var processTimeFromDDMMYYYY = function(dd,mm,yy) {
	return ;
}

var processTime = function(str) {
	var weeks 		= 0;
	var days 			= 0;
	var start_time 	= new Date().getTime();
	if((str.indexOf("weeks") || str.indexOf("week")) > -1) {
		weeks = parseInt(str.split(" ")[0]);
	} else if((str.indexOf("days") || str.indexOf("day")) > -1) {
		days = parseInt(str.split(" ")[0]);
	}
	if(weeks > 0) {
		start_time = moment().tz(configs.MOMENT_TIMEZONE).startOf('isoweek').subtract((weeks-1)*7,'day').valueOf();
	} else if (days > 0) {
		start_time = moment().tz(configs.MOMENT_TIMEZONE).subtract(days,'day').valueOf();
	}

	return start_time;
}