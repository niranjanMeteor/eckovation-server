var mongoose                = require('mongoose');
var readline                = require('readline');

var configs                 = require('./../utility/configs.js');
var UnsecureAccountsModule  = require('./../utility/auth/unsecure_accounts.js');

require('../models/accounts.js');
require('../models/profiles.js');
require('../models/unsecureaccounts.js');

var Account 				    = mongoose.model('Account');
var Profile             = mongoose.model('Profile');
var UnsecureAccount     = mongoose.model('UnsecureAccount');

var rL = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

var remaining = 0;
var replicated = 0;
var skip_value = 0;
var non_bulk_accounts = [];
var inconsistent_aids = [];
var inconsistent_pids = [];

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});
mongoose.connection.on('connected', function(){
  console.log('Successfully Opened MOngoDb Connection');
  console.log('------------------------------------Unsecured Account Migration SCRIPT STARTED------------------------------------')
	console.log('Program will give you a chance to migrate the unsecured accounts');
  startUnsecureAccountMenu();
});

function startUnsecureAccountMenu(){
  rL.question('Enter your option, ( 1 : Migrate, 2 : Consistency, 0 : Exit ) : ', function(answer) {
    if(!isNaN(answer)) {
      var limit = parseInt(answer);
      switch(parseInt(answer)){
        case 1:
          startMigration();
        break;

        case 2:
          viewConsistencyInUnsecureList();
        break;

        // case 3:
        //   makeUnsecureListConsistent();
        // break;

        case 0:
          endScript();
        break;

        default:
          console.log('Oops , you entered a wrong option , Please enter a right option');
          startUnsecureAccountMenu();
      }
    }
  });
}

function startMigration(){
  Account.count({
    lsnm : {$exists:true},
    lsni : {$exists:false}
  },function(err, count){
    if(err) { console.trace(err); return; }
    console.log('Total number of account ids to migrate : '+count);
    remaining = count;
    replicated = 0;
    skip_value = 0;
    startUnsecureAccountsUpdation(skip_value, replicated, remaining);
  });
}

function startUnsecureAccountsUpdation(skip_value ,replicated, remaining){
  if(remaining <= 0){
    console.log('\n');
    console.log('--------------- Now will migrate all Unsecure Profiles ----------------');
    console.log('\n');
    remaining  = 0;
    replicated = 0;
    skip_value = 0;
    Profile.count({
      aid : {$in : non_bulk_accounts}
    },function(err, count){
      if(err){ 
        console.trace(err); 
        return; 
      }
      console.log('Total number of profiles to migrate : '+count);
      remaining = count;
      startUnsecureProfileUpdation(skip_value, replicated, remaining);
    });
    return;
  }
  rL.question('Enter number of accounts you wanna migrated, (replicated:'+replicated+' , remaining:'+remaining+') : ', function(answer) {
    if(!isNaN(answer)) {
      var limit = parseInt(answer);
      if(limit > remaining || limit <= 0) {
        console.log('Oops , you have entered an incorrect value , Please enter a correct value');
        return startUnsecureAccountsUpdation(skip_value, replicated, remaining);
      }
      migrateToUnsecuredAccounts(skip_value, limit, function(err,resp){
        if(err){ 
          console.log('Sorry the account_id migration was not successful'); 
          return; 
        }
        replicated = replicated + resp.success;
        remaining  = remaining - resp.done;
        skip_value = skip_value + limit;
        return startUnsecureAccountsUpdation(skip_value, replicated, remaining);
      });
    } else {
      console.log('Oops , you entered a wrong option , Please enter a right option');
      return startUnsecureAccountsUpdation(skip_value, replicated, remaining);
    }
  });
}

function migrateToUnsecuredAccounts(skip_value, limit ,cb){
  var done = 0;
  var success = 0;
  // console.log('skip value is '+skip_value);
  // console.log('limit is '+limit);
  Account.find({
    lsnm : {$exists:true},
    lsni : {$exists:false}
  },{
    _id : 1
  },{
    sort : {
      _id : 1
    },
    skip : skip_value,
    limit : limit
  },function(err, accounts){
    if(err){ 
      console.trace(err); 
      return cb(true, false); 
    }
    if(accounts.length == 0){ 
      console.log('No accounts found to migrate to unsecured accounts'); 
      return cb(true,false); 
    }
    total_accounts = accounts.length;
    console.log('total number fo aids to process : '+total_accounts);
    var client = null;
    for(var i=0; i<total_accounts; i++){
      if(non_bulk_accounts.indexOf(accounts[i]._id+'')<0){
        non_bulk_accounts.push(accounts[i]._id+'');
      }
      UnsecureAccountsModule.addAcc(accounts[i]._id, client, function(err,resp){
        if(err){
          console.trace(err);
        }
        ++done;
        if(resp){
          ++success;
        }
        if(done == limit) {
          console.log('Out of total processing of '+limit+' accounts , succcessfully migrated were : '+success);
          return cb(null, {done:done,success:success});
        }
      });
    }
  });
}

function startUnsecureProfileUpdation(skip_value ,replicated, remaining){
  if(remaining <= 0){
    endScript();
  }
  rL.question('Enter number of Profiles you wanna migrated, (replicated:'+replicated+' , remaining:'+remaining+') : ', function(answer) {
    if(!isNaN(answer)) {
      var limit = parseInt(answer);
      if(limit > remaining || limit <= 0) {
        console.log('Oops , you have entered an incorrect value , Please enter a correct value');
        return startUnsecureProfileUpdation(skip_value, replicated, remaining);
      }
      migrateToUnsecuredProfiles(skip_value, limit, function(err,resp){
        if(err){ console.log('Sorry the profiles migration was not successful'); return ; }
        replicated = replicated + resp.success;
        remaining  = remaining - resp.done;
        skip_value = skip_value + limit;
        return startUnsecureProfileUpdation(skip_value, replicated, remaining);
      });
    } else {
      console.log('Oops , you entered a wrong option , Please enter a right option');
      return startUnsecureProfileUpdation(skip_value, replicated, remaining);
    }
  });
}

function migrateToUnsecuredProfiles(skip_value, limit ,cb){
  var done = 0;
  var success = 0;
  Profile.find({
    aid : {$in : non_bulk_accounts}
  },{
    _id : 1,
  },{
    sort : {
      _id : 1
    },
    skip : skip_value,
    limit : limit
  },function(err, profiles){
    if(err) { console.trace(err); return cb(true, false); }
    if(profiles.length == 0) { console.log('No Profiles found to migrate'); return cb(true,false); }
    total_profiles = profiles.length;
    console.log('total number fo pids to process : '+total_profiles);
    for(var i=0; i<total_profiles; i++){
      UnsecureAccountsModule.addProfile(profiles[i].aid ,profiles[i]._id, function(err,resp){
        ++done;
        if(resp) {
          ++success;
        }
        if(done == limit) {
          console.log('Out of total processing of '+limit+' profiles , succcessfully migrated were : '+success);
          return cb(null, {done:done,success:success});
        }
      });
    }
  });
}

function viewConsistencyInUnsecureList(){
  inconsistent_aids = [];
  inconsistent_pids = [];
  UnsecureAccount.find({},{
    aid : 1,
    ack : 1,
    _id : 0
  },function(err,unsecure_accounts){
    if(err){
      console.trace(err);
      return endScript();
    }
    if(unsecure_accounts.length == 0){
      console.log('Sorry , no account found in Unseure List');
      return startUnsecureAccountMenu();
    }
    var aids = [];
    var total_unsecure_aid_DB_count   = unsecure_accounts.length;
    var total_unsecure_pid_DB_count   = 0;
    var total_unsecure_aid_DB_not_ack = 0;
    var total_unsecure_aid_redis      = 0;
    var total_unsecure_pid_redis      = 0;
    var totalTobeDone = 2;
    var done = 0;
    findAidCountUnsecureRedis(unsecure_accounts, total_unsecure_aid_DB_count, function(err,resp){
      if(err){
        console.trace(err);
        return endScript();
      }
      done++;
      total_unsecure_aid_DB_not_ack = resp.total_unsecure_aid_DB_not_ack
      total_unsecure_aid_redis      = resp.total_unsecure_aid_redis;
      if(done == totalTobeDone){
        return displayConsistencyData(
          total_unsecure_aid_DB_count,
          total_unsecure_pid_DB_count,
          total_unsecure_aid_DB_not_ack,
          total_unsecure_aid_redis,
          total_unsecure_pid_redis
        );
      }
    });
    findPidCountUnseureRedis(unsecure_accounts, total_unsecure_aid_DB_count, function(err,resp){
      if(err){
        console.trace(err);
        return endScript();
      }
      done++;
      total_unsecure_pid_redis = resp.total_unsecure_pid_redis;
      total_unsecure_pid_DB_count = resp.total_unsecure_pids;
      if(done == totalTobeDone){
        return displayConsistencyData(
          total_unsecure_aid_DB_count,
          total_unsecure_pid_DB_count,
          total_unsecure_aid_DB_not_ack,
          total_unsecure_aid_redis,
          total_unsecure_pid_redis
        );
      }
    });
  });
}

function findAidCountUnsecureRedis(unsecure_accounts, count, cb){
  var total_unsecure_aid_DB_not_ack = 0;
  var total_unsecure_aid_redis = 0;
  var done = 0;
  var totalToDo = 0;
  for(var i=0; i<count; i++){
    if(!unsecure_accounts[i].ack){
      totalToDo++;
      total_unsecure_aid_DB_not_ack++;
      checkIfIdInRedisList(unsecure_accounts[i].aid+'',null,function(err,resp){
        if(err){
          console.trace(err);
          return endScript();
        }
        done++;
        if(resp.status){
          total_unsecure_aid_redis++;
        } else {
          inconsistent_aids.push(resp.aid);
        }
        if(done == totalToDo){
          return cb(null,{
            total_unsecure_aid_DB_not_ack:total_unsecure_aid_DB_not_ack,
            total_unsecure_aid_redis:total_unsecure_aid_redis
          });
        }
      });
    }
  }
  if(totalToDo == 0){
    return cb(null,{
      total_unsecure_aid_DB_not_ack:0,
      total_unsecure_aid_redis:0
    });
  }
}

function findPidCountUnseureRedis(unsecure_accounts, count, cb){
  var aids = [];
  var total_unsecure_pid_redis = 0;
  for(var i=0; i<count; i++){
    if(!unsecure_accounts[i].ack){
      aids.push(unsecure_accounts[i].aid+'');
    }
  }
  if(aids.length == 0){
    return cb(null,{
      total_unsecure_pids:0,
      total_unsecure_pid_redis:0
    });
  }
  Profile.distinct('_id',{
    aid : { $in : aids},
    act : true
  },function(err,pids){
    if(err){
      console.trace(err);
      return endScript();
    }
    var total_unsecure_pids = pids.length;
    var done = 0;
    for(var i=0; i<total_unsecure_pids; i++){
      checkIfIdInRedisList(null,pids[i]+'',function(err,resp){
        if(err){
          console.trace(err);
          return endScript();
        }
        done++;
        if(resp.status){
          total_unsecure_pid_redis++;
        } else {
          inconsistent_pids.push(resp.pid);
        }
        if(done == total_unsecure_pids){
          return cb(null,{
            total_unsecure_pids:total_unsecure_pids,
            total_unsecure_pid_redis:total_unsecure_pid_redis
          });
        }
      });
    }
  });
}

function checkIfIdInRedisList(aid, pid, cb){
  UnsecureAccountsModule.check(aid, pid, function(err,resp){
    if(err){
      return cb(err,null);
    }
    return cb(null,{aid:aid,pid:pid,status:resp});
  });
}

function displayConsistencyData(aid_db, pid_db, aid_db_not_ack, aid_redis, pid_redis){
  console.log('\n');
  console.log('------------------ CONSISTENCY RESULTS --------------------');
  console.log('Total Unsecure Accounts in DB  :  '+aid_db);
  console.log('Total Unsecure Profiles in DB (act:true)  :  '+pid_db);
  console.log('Total Unsecure Accounts in DB UnAcknowledged Till Now  :  '+aid_db_not_ack);
  console.log('Total Unsecure Accounts in Redis  :  '+aid_redis);
  console.log('Total Unsecure Profiles in Redis  :  '+pid_redis);
  console.log('\n');
  if(aid_db_not_ack != aid_redis || pid_db != pid_redis){
    console.log('Seems That there is some InConsistency regarding Unsecure Accounts and profiles between DB and Redis');
    return consistencyOptions();
  } else {
    return startUnsecureAccountMenu();
  }
}

function consistencyOptions(){
  rL.question('Do you wanna create Consistency, ( 1 : MakeConsistent, 2 : Go to Main Menu, 0 : Exit ) : ', function(answer) {
    if(!isNaN(answer)) {
      var limit = parseInt(answer);
      switch(parseInt(answer)){
        case 1:
          makeUnsecureListConsistentForDbAndRedis();
        break;

        case 2:
          startUnsecureAccountMenu();
        break;

        case 0:
          endScript();
        break;

        default:
          console.log('Oops , you entered a wrong option , Please enter a right option');
          consistencyOptions();
      }
    }
  });
}

function makeUnsecureListConsistentForDbAndRedis(){
  var done = 0;
  var toDo = 0;
  var ifAccToDo = false;
  var ifProfToDo = false;
  if(inconsistent_aids.length > 0){
    toDo++;
    ifAccToDo = true;
  }
  if(inconsistent_pids.length > 0){
    toDo++;
    ifProfToDo = true;
  }
  if(toDo == 0){
    console.log('No Inconsistent records found to update');
    return startUnsecureAccountMenu();
  }
  if(ifAccToDo){
    makeAccountsConsistent(function(err,resp_acc){
      if(err){
        console.trace(err);
        return endScript();
      }
      done++;
      console.log('\n');
      console.log('---- Total Inconsistent Accounts To DO  -----    :  '+inconsistent_aids.length);
      console.log('---- Total Inconsistent Accounts Succesfully done -----    :  '+resp_acc);
      if(done == toDo){
        return startUnsecureAccountMenu();
      }
    });
  }
  if(ifProfToDo){
    makeProfilesConsistent(function(err,resp_prof){
      if(err){
        console.trace(err);
        return endScript();
      }
      done++;
      console.log('\n');
      console.log('---- Total Inconsistent Profiles To DO  -----    :  '+inconsistent_pids.length);
      console.log('---- Total Inconsistent Profiles Succesfully done -----    :  '+resp_prof);
      if(done == toDo){
        return startUnsecureAccountMenu();
      }
    });
  }
}

function makeAccountsConsistent(cb){
  var totalToDo = inconsistent_aids.length;
  var done = 0;
  var success = 0;
  for(var i=0; i<totalToDo; i++){
    UnsecureAccountsModule.addAccRedis(inconsistent_aids[i],function(err,resp){
      if(err){
        console.trace(err);
        return endScript();
      }
      if(resp){
        success++;
      }
      done++;
      if(done == totalToDo){
        return cb(null,success);
      }
    });
  }
}

function makeProfilesConsistent(cb){
  var totalToDo = inconsistent_pids.length;
  var done = 0;
  var success = 0;
  for(var i=0; i<totalToDo; i++){
    UnsecureAccountsModule.addProfRedis(inconsistent_pids[i],function(err,resp){
      if(err){
        console.trace(err);
        return endScript();
      }
      if(resp){
        success++;
      }
      done++;
      if(done == totalToDo){
        return cb(null,success);
      }
    });
  }
}

function endScript(){
  console.log('----------------------Unsecured Accounts and Profile Migration SCRIPT ended------------------------');
  process.exit(0);
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : 120
      }
    }
  };
  if(!configs.MONGO_UNAME || configs.MONGO_UNAME == ""){
    return mongoose.connect("mongodb://"+configs.MONGO_HOST+":"+configs.MONGO_PORT+"/"+configs.DB_NAME,options);
  }
  options.replset = {
    rs_name : configs.MONGO_REPLICA_SET_NAME,
    socketOptions : {
      keepAlive : 120
    }
  };
  var connect_string = "mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@";
  var mongo_hosts    = configs.MONGO_HOST;
  var mongo_port     = configs.MONGO_PORT;
  for(var i=0; i<mongo_hosts.length; i++){
    if(i > 0)
      connect_string += ",";
    connect_string += mongo_hosts[i]+":"+mongo_port;
  }
  connect_string += "/"+configs.DB_NAME,options;
  return mongoose.connect(connect_string);
}
