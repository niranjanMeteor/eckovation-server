var mongoose                = require('mongoose');
var readline                = require('readline');
var configs                 = require('./../utility/configs.js');

var rL = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});
mongoose.connection.on('connected', function(){
  console.log('Successfully Opened MOngoDb Connection');
  startAccountUpdateCheck();
});

require('../models/accounts.js');
require('../models/unsecureaccounts.js');
require('../models/accounttokens.js');
var Account               = mongoose.model('Account');
var UnsecureAccount       = mongoose.model('UnsecureAccount');
var AccountToken          = mongoose.model('AccountToken');

var TargetVersion                    = '3.9.000';
var toDO                             = 3; 
var done                             = 0;
var totalAccountsRecordsInDb         = 0;
var totalAccountsWithTargetVrsn      = 0;
var totalAccountsRecordsInUnsecureDb = 0;
var totalAccountsInUnsecureDbMatched = 0;
var totalAccountsInUnsecureAckFalse  = 0;
var totalAccountsInUnsecureAckFalseNoToken = 0;
var accountsInUnsecureAckFalseNoToken  = [];
var totalAccountsInUnsecureAckTrue   = 0;
var totalAccountsInUnsecureNoAT      = 0;
var totalAccountsInUnsecureNoRT      = 0;
var totalAccountsInUnsecureNoSecret  = 0

function startAccountUpdateCheck(){
  Account.count({
    vrfy : true
  },function(err,total_accounts_db_count){
    if(err){
      console.trace(err);
      process.exit(0);
    }
    done++;
    totalAccountsRecordsInDb = total_accounts_db_count;
    if(done == toDO){
      return printData();
    }
  });
  UnsecureAccount.count({},function(err,total_unsecure_accounts_db_count){
    if(err){
      console.trace(err);
      process.exit(0);
    }
    done++;
    totalAccountsRecordsInUnsecureDb = total_unsecure_accounts_db_count;
    if(done == toDO){
      return printData();
    }
  });
  Account.distinct('_id',{
    vrsn : TargetVersion
  },function(err,aids){
    if(err){
      console.trace(err);
      process.exit(0);
    }
    var total_accounts = aids.length;
    totalAccountsWithTargetVrsn = total_accounts;
    if(total_accounts == 0){
      console.log('No accounts found on the target version : '+TargetVersion);
    }
    UnsecureAccount.find({
      aid : {$in : aids}
    },function(err, unsecure_accounts){
      if(err){
        console.trace(err);
        process.exit(0);
      }
      var total_unsecure_accounts = unsecure_accounts.length;
      totalAccountsInUnsecureDbMatched = total_unsecure_accounts;
      if(total_unsecure_accounts == 0){
        console.log('Seems that the UnsecureAccount collection is empty');
      }
      var ackFalseAids = [];
      for(var j=0; j<total_unsecure_accounts; j++){
        if(unsecure_accounts[j].ack){
          totalAccountsInUnsecureAckTrue++;
        } else {
          totalAccountsInUnsecureAckFalse++;
          ackFalseAids.push(unsecure_accounts[j].aid+'');
        }
        if(!unsecure_accounts[j].at){
          totalAccountsInUnsecureNoAT++;
        }
        if(!unsecure_accounts[j].rt){
          totalAccountsInUnsecureNoRT++;
        }
        if(!unsecure_accounts[j].skey){
          totalAccountsInUnsecureNoSecret++;
        }
      }
      ackFalseCheckForReinstall(ackFalseAids,function(err){
        if(err){
          console.trace(err);
          process.exit(0);
        }
        done++;
        if(done == toDO){
          return printData();
        }
      });
    });
  });
}

function ackFalseCheckForReinstall(ackFalseAids, cb){
  if(ackFalseAids.length == 0){
    return cb(null);
  }
  AccountToken.distinct('aid',{
    aid : {$in : ackFalseAids},
    tokn: {$exists : true},
    act : true
  },function(err,account_tokens){
    if(err){
      return cb(err);
    }
    totalAccountsInUnsecureAckFalseNoToken =  ackFalseAids.length - account_tokens.length;
    for(var i=0; i<ackFalseAids.length; i++){
      if(account_tokens.indexOf(ackFalseAids[i]) < 0){
        accountsInUnsecureAckFalseNoToken.push(ackFalseAids[i]);
      }
    }
    return cb(null);
  });
}

function printData(){
  console.log('\n');
  console.log('____________________ Results for Accounts Update Checks for Version : '+TargetVersion+' ___________________');
  console.log('\n');
  console.log('1 . Total Verified Accounts Entries in Account collection             :  '+totalAccountsRecordsInDb);
  console.log('2 . Total Accounts Entries in UnsecureAccount collection              :  '+totalAccountsRecordsInUnsecureDb);
  console.log('3 . Total Accounts on version : '+TargetVersion+'                                   :  '+totalAccountsWithTargetVrsn);
  console.log('4 . Total Accounts on version : '+TargetVersion+' in UnsecureAccounts               :  '+totalAccountsInUnsecureDbMatched);
  console.log('5 . Total Accounts on version : '+TargetVersion+' in UnsecureAccounts  Ack=False    :  '+totalAccountsInUnsecureAckFalse);
  console.log('5 . Total Accounts on version : '+TargetVersion+' in UnsecureAccounts  Ack=False, No Token    :  '+totalAccountsInUnsecureAckFalseNoToken);
  console.log('6 . Total Accounts on version : '+TargetVersion+' in UnsecureAccounts  Ack=True     :  '+totalAccountsInUnsecureAckTrue);
  console.log('7 . Total Accounts on version : '+TargetVersion+' in UnsecureAccounts  No RT        :  '+totalAccountsInUnsecureNoRT);
  console.log('8 . Total Accounts on version : '+TargetVersion+' in UnsecureAccounts  No AT        :  '+totalAccountsInUnsecureNoAT);
  console.log('9 . Total Accounts on version : '+TargetVersion+' in UnsecureAccounts  No Secret    :  '+totalAccountsInUnsecureNoSecret);
  console.log('\n');

  if(totalAccountsInUnsecureAckFalseNoToken){
    rL.question('Do you want print Accounts on '+TargetVersion+' With Ack in UnsecureList and No AccountToken, ( 1 : print 0 : Exit ) : ', function(answer) {
      if(isNaN(answer)) {
        console.log('Fuck off you loser, looks you wanna waster our time');
        console.log('\n');
        process.exit(0);
      }
      if(!(answer == '0' || answer == '1')){
        console.log('Fuck off you loser, looks you wanna waster our time');
        console.log('\n');
        process.exit(0);
      }
      if(answer == '0'){
        console.log('\n');
        process.exit(0);
      }
      if(answer == '1'){
        console.log('\n');
        console.log(JSON.stringify(accountsInUnsecureAckFalseNoToken));
      }
      console.log('\n');
      return process.exit(0);
    });
  } else {
    process.exit(0);
  }
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : 120
      }
    }
  };
  if(!configs.MONGO_UNAME || configs.MONGO_UNAME == ""){
    return mongoose.connect("mongodb://"+configs.MONGO_HOST+":"+configs.MONGO_PORT+"/"+configs.DB_NAME,options);
  }
  options.replset = {
    rs_name : configs.MONGO_REPLICA_SET_NAME,
    socketOptions : {
      keepAlive : 120
    }
  };
  var connect_string = "mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@";
  var mongo_hosts    = configs.MONGO_HOST;
  var mongo_port     = configs.MONGO_PORT;
  for(var i=0; i<mongo_hosts.length; i++){
    if(i > 0)
      connect_string += ",";
    connect_string += mongo_hosts[i]+":"+mongo_port;
  }
  connect_string += "/"+configs.DB_NAME,options;
  return mongoose.connect(connect_string);
}



