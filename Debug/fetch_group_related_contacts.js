var moment				      = require('moment');
var momentTZ				    = require('moment-timezone');
var mongoose            = require('mongoose');
var _                   = require('underscore');
var readline 			      = require('readline');

var configs             = require('./../utility/configs.js');

var connect = function () {
  var options = {server : {socketOptions : {keepAlive : 1}}};
  if(configs.MONGO_UNAME!="") {
    mongoose.connect("mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@"+configs.MONGO_HOST+"/"+configs.DB_NAME,options);
  } else {
    mongoose.connect("mongodb://"+configs.MONGO_HOST+"/"+configs.DB_NAME,options);
  }
}
connect();
mongoose.connection.on('error',console.log);
mongoose.connection.on('disconnected', connect);

require('./../models/profiles.js');
require('./../models/groups.js');
require('./../models/accounts.js');
require('./../models/groupmembers.js');
require('./../models/messages.js');
require('./../models/contactlogs.js');

var Profile            	 = mongoose.model('Profile');
var GroupMember 			   = mongoose.model('GroupMember');
var Group 					     = mongoose.model('Group');
var Account 					   = mongoose.model('Account');
var ContactLog 					 = mongoose.model('ContactLog');

var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

var processInput = function(answer) {
	var codes = answer.split(",");
	var _codes = [];
	codes.forEach(function(code){
			code = code.trim();
			_codes.push(code);
	});
	codes = _codes;

	Group.find({
		code : {$in : codes}
	},function(err,grps){
			if(err || grps.length == 0) {
					console.log("No groups found. Exiting...");
					process.exit();
			}

			var gids = [];

			var grps_by_id = {};

			grps.forEach(function(grp){
				gids.push(grp._id);
				grps_by_id[grp._id] = grp;
			});

			GroupMember.find({
				gid 	: {$in : gids},
				gdel  : false
			},function(err,gms){
				if(err || gms.length == 0) {
					console.log("No members found. Exiting...");
					process.exit();
				}

				processAllMembersSequentially(gms,function(err,data){
					if(err) { console.log(err); process.exit(); }
					console.log("All emails outputted, finally!");
					process.exit();
				});

			}).read('s');

			rl.close();
	});
}

if(process.argv.length >= 3) {
	console.log("Found an input," + process.argv[2]);
	processInput(process.argv[2]);
} else {
	rl.question("Please provide me with CSV of Group Codes : ", function(answer) {
		processInput(answer);
	});
}

var processAllMembersSequentially = function(gms,cb) {
	var processed_gms = [];
	

	var cb1 = function(err,datas){
		if(err){  return cb(err,null); }

		datas.forEach(function(data){
				console.log(data);
		});

		if(gms.length == 0) {
			cb(null,{});
		} else {
			var gm_ = gms.shift();
			processSingleMember([gm_["aid"]],[gm_["pid"]],cb1);
		}
	};

	if(gms.length == 0) {
		return cb(null,{});
	}

	var gm = gms.shift();
	processSingleMember([gm["aid"]],[gm["pid"]],cb1);
}

var processSingleMember = function(aids,pids,cb) {
			Profile.find({
				_id : {$in : pids}
			},function(err,profiles){
					if(err) { console.log(err); return cb(err,null); }

					var profiles_by_ids = {};
					var pids_by_aids = {};

					profiles.forEach(function(profile){
						profiles_by_ids[profile["_id"]] = profile;

						if(!pids_by_aids[profile["aid"]]) {
							pids_by_aids[profile["aid"]] = [];
						}

						pids_by_aids[profile["aid"]].push(profile["_id"]);
					});

					var data = [];

					ContactLog.find({
						aid : {$in : aids}
						//$or : [{lsnm: { $gt:d }},{lsni: {$gt:d }}]
					},function(err,contactlogs){
							if(err) { console.log(err); return cb(err,null); }

							contactlogs.forEach(function(contactlog){
									var fetched_from_pid = profiles_by_ids[pids_by_aids[contactlog["aid"]][0]]["nam"];

									if(contactlog["e"]!==null) {
										data.push(fetched_from_pid + "," + contactlog["n"]+","+contactlog["e"]);
									}
							});
							// // console.log("The phone numbers are as follows: ");
							// contactlogs.forEach(function(contactlog){
							// 		if(contactlog["p"]!==null) {
							// 			// console.log(contactlog["p"]);
							// 		}
							// });

							cb(null,data);
					}).read('s');
			}).read('s');
}