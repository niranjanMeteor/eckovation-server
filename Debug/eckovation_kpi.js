var mongoose            = require('mongoose');
var configs             = require('./../utility/configs.js');
var readline 			      = require('readline');
var moment				      = require('moment-timezone');

require('../models/groupmembers.js');
require('../models/groups.js');
require('../models/profiles.js');
require('../models/accounts.js');
require('../models/messages.js');
require('../models/statuses.js');
require('../models/groupplugins.js');

var Group 				      = mongoose.model('Group');
var GroupMember 		    = mongoose.model('GroupMember');
var Profile 				    = mongoose.model('Profile');
var Account 				    = mongoose.model('Account');
var Message 				    = mongoose.model('Message');
var Status 				      = mongoose.model('Status');
var GroupPlugin 				= mongoose.model('GroupPlugin');

var rL = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});
var KpiData = {
	total_groups : '',
	active_groups: '',
	group_with_active_plugins : '',
	one_way_groups : {
		total : '',
		average_messages : {
			total   : '',
			daily   : '',
			weekly  : '',
			monthly : ''
		}
	},
	two_way_groups : {
		total : '',
		average_messages : {
			total   : '',
			daily   : '',
			weekly  : '',
			monthly : ''
		}
	},
	user : {
		daily_active : '',
		monthly_active : '',
		monthly_live   : ''
	}
};
connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});
mongoose.connection.on('connected', function(){
  console.log('Successfully Opened MOngoDb Connection');
  console.log('------------------------------------ECKOVATION KPI SCRIPT STARTED------------------------------------')
	generateEckovationKpi();
});

// node --stack-size=65500 Debug/eckovation_kpi.js > /home/ubuntu/kpidata_2016_08_12_2.log 2>&1 &

var START_TIME                     = '2016-08-01';
var END_TIME                       = '2016-08-12';
var ACTIVE_GROUP_START_TIME        = '2016-08-05';
var ACTIVE_ACCOUNT_START_TIME      = '2016-08-05';

function generateEckovationKpi(){
	// rL.question('Enter the Start Date (YYYY-MM-DD) : ', function(start){
	// 	if(isValidDateFormat(start)){
	// 		rL.question('Enter the End Date (YYYY-MM-DD) : ', function(end){
	// 			if(isValidDateFormat(end)){
	// 				var datetime = getStartAndEndDateString(start,end);
	// 				console.log('\n');
	// 				console.log('    START DATETIME  ==   '+datetime.start);
	// 				console.log('    END DATETIME    ==   '+datetime.end);
	// 				console.log('\n');
	// 				generateData(datetime);
	// 			} else {
	// 				console.log('Oops , you entered a wrong End Date , Please enter a right option');
	// 				console.log('\n');
	// 				return startMonthlyDataReport();
	// 			}
	// 		});
	// 	} else {
	// 		console.log('Oops , you entered a wrong Start Date , Please enter a right option');
	// 		console.log('\n');
	// 		return generateEckovationKpi();
	// 	}
	// });
	var datetime = getStartAndEndDateString(START_TIME,END_TIME);
	console.log('\n');
	console.log('    START DATETIME  ==   '+datetime.start);
	console.log('    END DATETIME    ==   '+datetime.end);
	console.log('    ACTIVE_GROUP DATETIME    ==   '+datetime.active_group_time);
	console.log('    ACTIVE_ACCOUNT DATETIME    ==   '+datetime.active_account_time);
	console.log('\n');
	generateData(datetime);
}

function generateData(datetime){
	var toBeDone = 5;
	var done     = 0;
	getGroupsAvgData(datetime, function(err,resp){
		if(err){
			console.trace(err);
			endScript();
		}
		done++;
		if(done == toBeDone){
			console.log(KpiData);
			endScript();
		}
	});
	getActiveGroupsCount(datetime, function(err,resp){
		if(err){
			console.trace(err);
			endScript();
		}
		done++;
		if(done == toBeDone){
			console.log(KpiData);
			endScript();
		}
	});
	getGroupPluginsData(function(err,resp){
		if(err){
			console.trace(err);
			endScript();
		}
		done++;
		if(done == toBeDone){
			console.log(KpiData);
			endScript();
		}
	});
	getActiveUserCount(datetime, function(err,resp){
		if(err){
			console.trace(err);
			endScript();
		}
		done++;
		if(done == toBeDone){
			console.log(KpiData);
			endScript();
		}
	});
	getLiveUserCount(datetime, function(err,resp){
		if(err){
			console.trace(err);
			endScript();
		}
		done++;
		if(done == toBeDone){
			console.log(KpiData);
			endScript();
		}
	});
}

function getGroupsAvgData(datetime, cb){
	Group.find({
		act : true,
	},{
		_id : 1,
		gtyp: 1
	},function(err,groups){
		if(err){
			return cb(err,null);
		}
		var total_groups = groups.length;
		if(total_groups == 0){
			return cb('No Groups Fund for this Interval',null);
		}
		KpiData.total_groups = total_groups;
		var oneWayGrps = [];
		var twoWayGrps = [];
		for(var i=0; i<total_groups; i++){
			if(groups[i].gtyp == Group.GROUP_TYPE.ONE_WAY){
				oneWayGrps.push(groups[i]._id+'');
			} else if(groups[i].gtyp == Group.GROUP_TYPE.TWO_WAY){
				twoWayGrps.push(groups[i]._id+'');
			}
		}
		groups = null;
		KpiData.one_way_groups.total = oneWayGrps.length;
		KpiData.two_way_groups.total = twoWayGrps.length;

		var toBeDone = 2;
		var done     = 0;

		collectAverageMessageData(oneWayGrps, datetime, function(err,one_way_grp_msg_data){
			if(err){
				return cb(err,null);
			}
			KpiData.one_way_groups.average_messages.total   = one_way_grp_msg_data;
			KpiData.one_way_groups.average_messages.daily   = (one_way_grp_msg_data / datetime.nDays).toFixed(2);
			KpiData.one_way_groups.average_messages.weekly  = (one_way_grp_msg_data / datetime.nWeeks).toFixed(2);
			KpiData.one_way_groups.average_messages.monthly = (one_way_grp_msg_data / datetime.nMonths).toFixed(2);
			done++;
			if(done == toBeDone){
				return cb(null,true);
			}
		});
		oneWayGrps = null;
		collectAverageMessageData(twoWayGrps, datetime, function(err,two_way_grp_msg_data){
			if(err){
				return cb(err,null);
			}
			KpiData.two_way_groups.average_messages.total   = two_way_grp_msg_data;
			KpiData.two_way_groups.average_messages.daily   = (two_way_grp_msg_data / datetime.nDays).toFixed(2);
			KpiData.two_way_groups.average_messages.weekly  = (two_way_grp_msg_data / datetime.nWeeks).toFixed(2);
			KpiData.two_way_groups.average_messages.monthly = (two_way_grp_msg_data / datetime.nMonths).toFixed(2);
			done++;
			if(done == toBeDone){
				return cb(null,true);
			}
		});
		twoWayGrps = null;
	}).read('secondary');
}

function getActiveGroupsCount(datetime, cb){
	Message.distinct('to',{
		cat :{
			$gte : datetime.active_group_time
		}
		// type : {$in : [Message.MESSAGE_TYPE.image,Message.MESSAGE_TYPE.audio,Message.MESSAGE_TYPE.text,Message.MESSAGE_TYPE.video]}
	},function(err,msg_groups){
		if(err){
			return cb(err,null);
		}
		Status.distinct('gid',{
			stim : {$exists:true},
			stim : {
				$gte : datetime.active_group_time
			}
		},function(err,seen_groups){
			if(err){
				return cb(err,null);
			}
			var total_msg_grps  = msg_groups.length;
			var total_seen_grps = seen_groups.length;
			var biggerArray     = [];
			var temp;
			for(var i=0; i<total_msg_grps; i++){
				temp = msg_groups[i]+'';
				if(biggerArray.indexOf(temp) < 0){
					biggerArray.push(temp);
				}
			}
			msg_groups = null;
			for(var j=0; j<total_seen_grps; j++){
				temp = seen_groups[j]+'';
				if(biggerArray.indexOf(temp) < 0){
					biggerArray.push(temp);
				}
			}
			seen_groups = null;
			KpiData.active_groups = biggerArray.length;
			biggerArray = null;
			return cb(null,true);
		}).read('secondary');
	}).read('secondary');
}

function getActiveUserCount(datetime, cb){
	Status.distinct('pid',{
		stim : {$exists:true},
		stim : {
			$gte : datetime.active_account_time
		}
	},function(err,status_pids){
		if(err){
			return cb(err,null);
		}
		if(status_pids.length == 0){
			KpiData.user.daily_active   = 0;
			KpiData.user.monthly_active = 0;
			return cb(null,true);
		}
		Profile.distinct('aid',{
			_id : { $in : status_pids }
		},function(err,accounts){
			if(err){
				return cb(err,null);
			}
			KpiData.user.daily_active   = accounts.length;
			KpiData.user.monthly_active = accounts.length;
			accounts = null;
			return cb(null,true);
		});
		status_pids = null;
	}).read('secondary');
}

function getLiveUserCount(datetime, cb){
	Account.count({
		vrfy: true,
		$or : [
			{lsnm :{
				$gte : datetime.active_account_time,
			}},
			{lsni :{
				$gte : datetime.active_account_time,
			}},
			{lsnw :{
				$gte : datetime.active_account_time,
			}}
		]
	},function(err,accounts){
		if(err){
			return cb(err,null);
		}
		KpiData.user.monthly_live = accounts;
		return cb(null,accounts);
	});
}

function getGroupPluginsData(cb){
	GroupPlugin.distinct('gid',{
		act : true
	},function(err,grp_plugins){
		if(err){
			return cb(err,null);
		}
		KpiData.group_with_active_plugins = grp_plugins.length;
		return cb(null,true);
	}).read('secondary');
}

function collectAverageMessageData(gids, datetime, cb){
	Message.count({
		to : { $in : gids },
		cat :{
			$gte : datetime.start,
			$lte : datetime.end
		},
		type : {$in : [Message.MESSAGE_TYPE.image,Message.MESSAGE_TYPE.audio,Message.MESSAGE_TYPE.text,Message.MESSAGE_TYPE.video]}
	},function(err,num_msgs){
		if(err){
			return cb(err,null);
		}
		return cb(null,num_msgs);
	}).read('secondary');
}

function getStartAndEndDateString(start, end){
	var start_time 	          = moment(start, ["YYYY-MM-DD"]).tz(configs.MOMENT_TIMEZONE).valueOf();
	var end_time 		          = moment(end, ["YYYY-MM-DD"]).tz(configs.MOMENT_TIMEZONE).endOf("day").valueOf();
	var active_group_time 		= moment(ACTIVE_GROUP_START_TIME, ["YYYY-MM-DD"]).tz(configs.MOMENT_TIMEZONE).valueOf();
	var active_account_time 	= moment(ACTIVE_ACCOUNT_START_TIME, ["YYYY-MM-DD"]).tz(configs.MOMENT_TIMEZONE).valueOf();

	start_time          = new Date(start_time);
	end_time            = new Date(end_time);
	active_group_time   = new Date(active_group_time);
	active_account_time = new Date(active_account_time);

	var start_mili  = start_time.getTime();
	var end_mili    = end_time.getTime();
	var diff        = end_mili - start_mili;
	var oneDay      = 24*60*60*1000;
	var oneWeek     = 7*24*60*60*1000;
	var oneMonth    = 30*24*60*60*1000;
	var nDays       = parseInt(diff / oneDay);
	var nWeeks      = parseInt(diff / oneWeek);
	var nMonths     = parseInt(diff / oneMonth);
	if((diff % oneWeek) !=0 )
		nWeeks++;
	if((diff % oneMonth) != 0)
		nMonths++;

	var datetime = {
		start      : start_time,
		end        : end_time,
		start_mili : start_mili,
		end_mili   : end_mili,
		nDays      : nDays,
		nWeeks     : nWeeks,
		nMonths    : nMonths,
		active_group_time : active_group_time,
		active_account_time : active_account_time
	};
	return datetime;
}

function isValidDateFormat(input){
	if(!input) return false;
	var parts = input.split('-');
	if(parts.length != 3) return false;
	if(!isValidYear(parts[0])) return false;
	if(!isValidMonth(parts[1])) return false;
	if(!isValidDay(parts[2])) return false;
	return true;
}

function isValidYear(year){
	if(!year) return false;
	var input = parseInt(year);
	if(input == 2015 || input == 2016) return true;
	return false;
}

function isValidMonth(month){
	if(!month) return false;
	var input = parseInt(month);
	if(input<1 || input>12) return false;
	return true;
}

function isValidDay(day){
	if(!day) return false;
	var input = parseInt(day);
	if(input < 1 || input > 31) return false;
	return true;
}

function endScript(){
	console.log('\n');
	console.log('------------------  ECKOVATION KPI  ---------------------');
	console.log('\n');
	process.exit(0);
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : configs.MONGO_SERVER_KEEPALIVE
      }
    }
  };
  if(!configs.MONGO_UNAME || configs.MONGO_UNAME == ""){
    options.server.poolSize = configs.MONGO_POOLSIZE;
    console.log('[[[[[[[    NO MONGO_UNAME FOUND IN CONFIGS, WILL CONNNECT WITHOUT REPLICA OPTIONS     ]]]]]]]]]');
    console.log('[[[[[[[    MONGODB CONNECTION POOL_SIZE : '+configs.MONGO_POOLSIZE+'     ]]]]]]]]]');
    return mongoose.connect("mongodb://"+configs.MONGO_HOST+":"+configs.MONGO_PORT+"/"+configs.DB_NAME,options);
  }
  options.replset = {
    rs_name : configs.MONGO_REPLICA_SET_NAME,
    socketOptions : {
      keepAlive : configs.MONGO_REPLICA_KEEPALIVE
    },
    poolSize       : configs.MONGO_POOLSIZE,
    readPreference : 'secondaryPreferred',
    strategy       : "ping",
    slaveOk        : true
  };
  var connect_string = "mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@";
  var mongo_hosts    = configs.MONGO_HOST;
  var mongo_port     = configs.MONGO_PORT;
  for(var i=0; i<mongo_hosts.length; i++){
    if(i > 0)
      connect_string += ",";
    connect_string += mongo_hosts[i]+":"+mongo_port;
  }
  connect_string += "/"+configs.DB_NAME;
  console.log('[[[[[[[     CONNECTING MONGODB WITH REPLICA_SET OPTIONS     ]]]]]]]]]');
  console.log('[[[[[[[     MONGODB CONNECTION POOL_SIZE : '+configs.MONGO_POOLSIZE+'      ]]]]]]]]]');
  return mongoose.connect(connect_string,options);
}

