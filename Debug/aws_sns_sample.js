var AWS                       = require('aws-sdk');
var mongoose                  = require('mongoose');
var configs                   = require('../utility/configs.js');
var ActionNames               = require('../utility/action_names.js');

AWS.config.update({
  accessKeyId: configs.AWS_SNS_ACCESS_KEY,
  secretAccessKey: configs.AWS_SNS_SECRET_KEY,
  "region": configs.AWS_REGION,
  "output" : configs.AWS_OUTPUT 
});

var sns                       = new AWS.SNS();

require('../models/accountdata.js');
require('../models/pushnotify.js');

var AccountData               = mongoose.model('AccountData');
var PushNotify                = mongoose.model('PushNotify');

var api_key1                  = 'AIzaSyBh2KY_eIH1-I_4C71KbEGJE0OxNMwdAGs';
var api_key2                  = 'AIzaSyB6VGRVNRjtbNC1HvXS0vTxaDtarFCWO-o';
var application_arn           = configs.APPLICATION_ARN;

var params = {
  Name: 'com.eckovation1_sample',
  Platform: 'GCM',
  Attributes: {
    //PlatformPrincipal: principal,
    PlatformCredential: api_key1
  }
};

var application_arn = 'arn:aws:sns:ap-southeast-1:279388982033:app/GCM/com.eckovation1_sample';
var sample_token = 'APA91bEEFT9Yrw-cd7-Zpv6ba0ohNWtW8Y_SZ0fiQmNcqh1ILg4d1uM47YI3VJyIV8gN4xcXPHNx_mtTdZvekI-ZTXLpMCqjgnWX0xuDX79lleCWfEtfE0WGNEgq9VQcGK5539mA9Inx';

//var app_arn = data.PlatformApplicationArn;
// var endPointArn = 'arn:aws:sns:ap-southeast-1:279388982033:endpoint/GCM/eckovation-app-production/f6aab4c9-0049-360e-9b2b-896bbae31671';
var endPointArn='arn:aws:sns:ap-southeast-1:279388982033:endpoint/GCM/eckovation-app-production/6e17f005-b31c-3ea4-88a6-08f7d162fc3d';
 // var endPointArn='arn:aws:sns:ap-southeast-1:279388982033:endpoint/GCM/eckovation-app-production/f4b56d9e-e48d-365d-9462-6d0e17d3e1c5';
  var endPointArnIOS = 'arn:aws:sns:ap-southeast-1:279388982033:endpoint/APNS/EckovationIosProduction/ad2e7eec-90f5-3543-ab90-941084c72ee9';
  var endPointArnT = 'arn:aws:sns:ap-southeast-1:279388982033:endpoint/GCM/com.eckovation1_sample/947421a4-2c08-3c5b-9c29-97138adf584e';
  var endPointArnW = 'arn:aws:sns:ap-southeast-1:279388982033:endpoint/WNS/EckovationWindowsPhoneProduction/738b683e-6e0c-3f68-949c-9fc829df1635';
  // sns.createPlatformEndpoint({
  //   PlatformApplicationArn: application_arn,
  //   Token: sample_token
  // }, function(err, data) {
  //   if (err) { console.log(err.stack); return; }
  //   var endPointArn = data.EndpointArn;
  //   console.log('end point arn');
  //   console.log(endPointArn);
  var text = 'ye duniya eckovation ki hai';
  
  console.log(text.length);

    var payload = {
      // default: text,
      // GCM: {
      //   data: {
      //     // message: 'Shop your requirements today at eckovation',
      //     //t : 2,
      //     t : 1,
      //     b : "hi this is Niranjan",
      //     // url: 'www.eckovation.com',
      //     // com : {
      //     //   a : 1,
      //     //   b : 2
      //     // }
      //   }
      // }
      APNS: {
        aps: {
          alert: 'This is a AWS SNS test 3',
          sound: 'default',
          badge: 1,
          //"content-available":1
        }
      }
    };
    var payload2 = {
      default: 'niranjan says hi to arpit sabherwal',
      GCM: {
        data: {
          message: 'Shop your requirements today',
          t : 3,
          b : "hi this is body"
          //url: 'www.amazon.com'
        }
      }
      // APNS_SANDBOX: {
      //   aps: {
      //     alert: '101Hello World',
      //     sound: 'default',
      //     badge: 1,
      //     "content-available":1
      //   }
      // }
    };
    var payload3 = {
      //default: 'niranjan333: 2015 Greetings\njitergfhgkhsdfl',
      GCM: {
        data: {
          message: 'Shop your requirements today'
          //url: 'www.amazon.com'
        }
      }
      // APNS_SANDBOX: {
      //   aps: {
      //     alert: '102Hello World',
      //     sound: 'default',
      //     badge: 1,
      //     "content-available":1
      //   }
      // }
    };
    var new_text = 
        "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
        "<wp:Notification xmlns:wp=\"WPNotification\">" +
           "<wp:Toast>" +
           "<wp:Text1><I am String 1></ltwp:Text1>" +
           "<wp:Text2><I am String 2></wp:Text2>" +
           "</ltwp:Toast>" +
        "</wp:Notification>";

    var mpns_payload = {
      default : "",
      MPNS : new_text
      // "MPNS" : "<?xml version=\"1.0\" encoding=\"utf-8\"?><wp:Notification xmlns:wp=\"WPNotification\"><wp:Tile><wp:Count>23</wp:Count><wp:Title>This is a tile notification</wp:Title></wp:Tile></wp:Notification>"
    };
    mpns_payload.MPNS = JSON.stringify(mpns_payload.MPNS);
    mpns_payload      = JSON.stringify(mpns_payload);
    // payload.GCM = JSON.stringify(payload.GCM);
    payload.APNS = JSON.stringify(payload.APNS);
    payload = JSON.stringify(payload);
    // payload2.GCM = JSON.stringify(payload2.GCM);
    // payload2 = JSON.stringify(payload2);
    // payload3.GCM = JSON.stringify(payload3.GCM);
    // payload3 = JSON.stringify(payload3);
    console.log('sending push');
    console.log(mpns_payload);
    sns.publish({
      Message: mpns_payload,
      MessageStructure: 'json',
      TargetArn: endPointArnW
    }, function(err, data) {
      if (err) { console.log(err.stack); return; }
      console.log(data);
    });
  //});
//});

