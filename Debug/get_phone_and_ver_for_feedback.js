var moment				      = require('moment');
var momentTZ				    = require('moment-timezone');
var mongoose            = require('mongoose');
var _                   = require('underscore');
var readline 			      = require('readline');

var configs             = require('./../utility/configs.js');

var connect = function () {
  var options = {server : {socketOptions : {keepAlive : 1}}};
  if(configs.MONGO_UNAME!="") {
    mongoose.connect("mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@"+configs.MONGO_HOST+"/"+configs.DB_NAME,options);
  } else {
    mongoose.connect("mongodb://"+configs.MONGO_HOST+"/"+configs.DB_NAME,options);
  }
}
connect();
mongoose.connection.on('error',console.log);
mongoose.connection.on('disconnected', connect);

require('./../models/profiles.js');
require('./../models/groups.js');
require('./../models/accounts.js');
require('./../models/groupmembers.js');
require('./../models/messages.js');

var Profile            	 = mongoose.model('Profile');
var GroupMember 			   = mongoose.model('GroupMember');
var Group 					     = mongoose.model('Group');
var Account 					   = mongoose.model('Account');

var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

var countGroupMembers = function(gids,cb) {
	var segg = {};
	GroupMember.find({
		gid 	: {$in : gids},
		act   : true,
		gdel  : false
	},function(err,gms){
		if(err) { return cb(err,null); }

		gms.forEach(function(gm){
			if(!segg[gm["gid"]]) {
				segg[gm["gid"]] = 0;
			}

			segg[gm["gid"]]++;
		});

		cb(null,segg);
	});
};

rl.question("Please provide me with CSV of Group Codes : ", function(answer) {
	var codes = answer.split(",");
	var _codes = [];
	codes.forEach(function(code){
			code = code.trim();
			_codes.push(code);
	});
	codes = _codes;

	Group.find({
		code : {$in : codes}
	},function(err,grps){
		if(err || grps.length == 0) {
				console.log("No groups found. Exiting...");
				process.exit();
		}

		var gids = [];

		var grps_by_id = {};

		grps.forEach(function(grp){
			gids.push(grp._id);
			grps_by_id[grp._id] = grp;
		});

		GroupMember.find({
			gid 	: {$in : gids},
		//	act   : true,
			gdel  : false
		},function(err,gms){
			if(err || gms.length == 0) {
				console.log("No members found. Exiting...");
				process.exit();
			}

			var pids = [];
			var aids = [];
			gms.forEach(function(gm){
				pids.push(gm.pid);
				aids.push(gm.aid + "");
			});

			aids = _.uniq(aids);
var d = new Date(momentTZ("00:00:00 20:05:2016","HH:mm:ss DD:MM:YYYY").tz("Asia/Kolkata").valueOf());
console.log(d);
			Account.find({
				_id : {$in : aids}
				//$or : [{lsnm: { $gt:d }},{lsni: {$gt:d }}]
			},function(err,accs){
						var mobiles = [];
						accs.forEach(function(acc){
							mobiles.push({
								c : acc["ccod"],
								m : acc["m"],
								e : acc["e"],
								v : (acc["vrsn"] != undefined ? acc["vrsn"] : (acc["iosv"] != undefined ? acc["iosv"] : (acc["webv"] != undefined ? acc["webv"] : undefined))),
								s : (acc["lsnm"] != undefined ? acc["lsnm"] : (acc["lsni"] != undefined ? acc["lsni"] : "-"))
							});
						});

						mobiles.forEach(function(obj){
							console.log(obj.c + obj.m + "|" + obj.v + "|" + obj.e.join(",") + "|" + obj.s);
						});

						process.exit();
			});
		});
	});

	rl.close();
});
