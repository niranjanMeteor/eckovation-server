var mongoose            = require('mongoose');
var configs             = require('./../utility/configs.js');
var readline 			      = require('readline');
var moment				      = require('moment-timezone');

require('../models/groupmembers.js');
require('../models/groups.js');
require('../models/profiles.js');
require('../models/accounts.js');
require('../models/messages.js');
require('../models/statuses.js');

var Group 				      = mongoose.model('Group');
var GroupMember 		    = mongoose.model('GroupMember');
var Profile 				    = mongoose.model('Profile');
var Account 				    = mongoose.model('Account');
var Message 				    = mongoose.model('Message');
var Status 				      = mongoose.model('Status');

var rL = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});
var group_id;
connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});
mongoose.connection.on('connected', function(){
  console.log('Successfully Opened MOngoDb Connection');
  console.log('------------------------------------GROUP AVEG DATA SCRIPT STARTED------------------------------------')
	startGroupAvgData();
});

function startGroupAvgData(){
	rL.question('Enter the Group Code for Average Data  : ', function(answer){
		if(!answer){
			console.log('Oops , you entered a wront group code format , Please enter a right group code format');
			return startGroupAvgData();
		}
		var group_code = answer.trim();
		if(!isNaN(group_code) && group_code.length == 6) {
			Group.findOne({
				code : group_code,
				act  : true
			},function(err,group){
				if(err){
					console.trace(err);
					process.exit(0);
				}
				if(!group){
					console.log('Oops, the group code that you have given , is not valid , Please enter a right group code');
					return startGroupAvgData();
				}
				group_id = group._id+'';
				return generateAvgGroupData();
			});
		} else {
			console.log('Oops , you entered a wront group code format , Please enter a right group code format');
			return startGroupAvgData();
		}
	});
}

function generateAvgGroupData(){
	rL.question('Enter the Start Date (YYYY-MM-DD) : ', function(start){
		if(isValidDateFormat(start)){
			rL.question('Enter the End Date (YYYY-MM-DD) : ', function(end){
				if(isValidDateFormat(end)){
					var datetime = getStartAndEndDateString(start,end);
					console.log('\n');
					console.log('    START DATETIME  ==   '+datetime.start);
					console.log('    END DATETIME    ==   '+datetime.end);
					console.log('\n');
					generateData(datetime);
				} else {
					console.log('Oops , you entered a wrong option , Please enter a right option');
					return startMonthlyDataReport();
				}
			});
		} else {
			console.log('Oops , you entered a wrong option , Please enter a right option');
			return startMonthlyDataReport();
		}
	});
}

function generateData(datetime){
	var toBeDoneItems = 4;
	var done          = 0;
	var data = {};
	getMsgsExchg(datetime,function(err,msgsExchg){
		if(err){
			console.trace(err);
			endScript();
		}
		data.msgsExchg = msgsExchg;
		done++;
		if(done == toBeDoneItems){
			console.log(data);
			endScript();
		}
	});
	getAvgSeenRate(datetime,function(err,avg_seens){
		if(err){
			console.trace(err);
			endScript();
		}
		data.avg_seens = avg_seens;
		done++;
		if(done == toBeDoneItems){
			console.log(data);
			endScript();
		}
	});
	getGrpMems(datetime,function(err,grp_mems){
		if(err){
			console.trace(err);
			endScript();
		}
		data.grp_mems = grp_mems;
		done++;
		if(done == toBeDoneItems){
			console.log(data);
			endScript();
		}
	});
	getActiveMems(datetime,function(err,act_mems){
		if(err){
			console.trace(err);
			endScript();
		}
		data.act_mems = act_mems;
		done++;
		if(done == toBeDoneItems){
			console.log(data);
			endScript();
		}
	});

}

function getMsgsExchg(datetime,cb){
	Message.count({
		to : group_id,
		cat :{
			$lte : datetime.end
		},
		type : {$in : [Message.MESSAGE_TYPE.image,Message.MESSAGE_TYPE.audio,Message.MESSAGE_TYPE.text,Message.MESSAGE_TYPE.video]}
	},function(err,num_msgs){
		if(err){
			return cb(err,null);
		}
		return cb(null,num_msgs);
	});
}

function getAvgSeenRate(datetime,cb){
	Message.distinct('_id',{
		to : group_id,
		cat :{
			$lte : datetime.end
		},
		type : {$in : [Message.MESSAGE_TYPE.image,Message.MESSAGE_TYPE.audio,Message.MESSAGE_TYPE.text,Message.MESSAGE_TYPE.video]}
	},function(err,msgs){
		if(err){
			return cb(err,null);
		}
		Status.count({
			mid : {$in : msgs},
			stim : {$exists:true}
		},function(err,seens){
			if(err){
				return cb(err,null);
			}
			if(msgs.length == 0){
				return cb(null,0);
			}
			var average_rate = seens / (msgs.length);
			return cb(null,average_rate);
		});
	});
}

function getGrpMems(datetime,cb){
	GroupMember.count({
		gid : group_id,
		act : true,
		cat :{
			$lte : datetime.end
		}
	},function(err,mems){
		if(err){
			return cb(err,null);
		}
		return cb(null,mems);
	});
}

function getActiveMems(datetime,cb){
	GroupMember.distinct('aid',{
		gid : group_id,
		act : true,
		cat :{
			$lte : datetime.end
		}
	},function(err,mems_aids){
		if(err){
			return cb(err,null);
		}
		Account.count({
			_id : {$in : mems_aids},
			vrfy: true,
			$or : [
				{lsnm :{
					$gte : datetime.start,
				}},
				{lsni :{
					$gte : datetime.start,
				}},
				{lsnw :{
					$gte : datetime.start,
				}}
			]
		},function(err,accounts){
			if(err){
				return cb(err,null);
			}
			return cb(null,accounts);
		});
	});
}


function getStartAndEndDateString(start, end){
	// var start_date = 'ISODate("'+start+'T00:00:00:000Z")';
	// var end_date   = 'ISODate("'+end+'T023:59:59:000Z")';

	var start_time 	= moment(start, ["YYYY-MM-DD"]).tz(configs.MOMENT_TIMEZONE).valueOf();
	var end_time 		= moment(end, ["YYYY-MM-DD"]).tz(configs.MOMENT_TIMEZONE).endOf("day").valueOf();

	start_time      = new Date(start_time);
	end_time        = new Date(end_time);

	var start_mili  = start_time.getTime();
	var end_mili    = end_time.getTime();

	var datetime = {
		start : start_time,
		end   : end_time,
		start_mili : start_mili,
		end_mili : end_mili
	};
	return datetime;
}

function isValidDateFormat(input){
	if(!input) return false;
	var parts = input.split('-');
	if(parts.length != 3) return false;
	if(!isValidYear(parts[0])) return false;
	if(!isValidMonth(parts[1])) return false;
	if(!isValidDay(parts[2])) return false;
	return true;
}

function isValidYear(year){
	if(!year) return false;
	var input = parseInt(year);
	if(input == 2015 || input == 2016) return true;
	return false;
}

function isValidMonth(month){
	if(!month) return false;
	var input = parseInt(month);
	if(input<1 || input>12) return false;
	return true;
}

function isValidDay(day){
	if(!day) return false;
	var input = parseInt(day);
	if(input < 1 || input > 31) return false;
	return true;
}

function endScript(){
	console.log('\n');
	console.log('------------------MONTHLY DATA REPORT ENDED---------------------');
	console.log('\n');
	process.exit(0);
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : 120
      }
    }
  };
  if(!configs.MONGO_UNAME || configs.MONGO_UNAME == ""){
    return mongoose.connect("mongodb://"+configs.MONGO_HOST+":"+configs.MONGO_PORT+"/"+configs.DB_NAME,options);
  }
  options.replset = {
    rs_name : configs.MONGO_REPLICA_SET_NAME,
    socketOptions : {
      keepAlive : 120
    }
  };
  var connect_string = "mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@";
  var mongo_hosts    = configs.MONGO_HOST;
  var mongo_port     = configs.MONGO_PORT;
  for(var i=0; i<mongo_hosts.length; i++){
    if(i > 0)
      connect_string += ",";
    connect_string += mongo_hosts[i]+":"+mongo_port;
  }
  connect_string += "/"+configs.DB_NAME,options;
  return mongoose.connect(connect_string);
}

