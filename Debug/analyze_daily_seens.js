var momentTZ				    = require('moment-timezone');
var moment				      = require('moment');
var mongoose            = require('mongoose');
var _                   = require('underscore');
var readline 			      = require('readline');

var configs             = require('./../utility/configs.js');

var connect = function () {
  var options = {server : {socketOptions : {keepAlive : 1}}};
  if(configs.MONGO_UNAME!="") {
    mongoose.connect("mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@"+configs.MONGO_HOST+"/"+configs.DB_NAME,options);
  } else {
    mongoose.connect("mongodb://"+configs.MONGO_HOST+"/"+configs.DB_NAME,options);
  }
}
connect();
mongoose.connection.on('error',console.log);
mongoose.connection.on('disconnected', connect);

require('./../models/accounts.js');
require('./../models/statuses.js');
var Account 					   = mongoose.model('Account');
var Status 						   = mongoose.model('Status');

var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

var allemails = [];

rl.question("Please enter your start-end & analytics end date in IST in (hh:mm:ss mm:dd:yy-hh:mm:ss mm:dd:yy-hh:mm:ss mm:dd:yy)? : ", function(dates) {
	var start = dates.split("-")[0].trim();
	var end = dates.split("-")[1].trim();

	var start_ts = momentTZ(start,"HH:mm:ss DD:MM:YYYY").tz(configs.MOMENT_TIMEZONE).valueOf();
	var end_ts = momentTZ(end,"HH:mm:ss DD:MM:YYYY").tz(configs.MOMENT_TIMEZONE).valueOf();

	var start_rev = momentTZ(start_ts).tz(configs.MOMENT_TIMEZONE).format();
	var end_rev = momentTZ(end_ts).tz(configs.MOMENT_TIMEZONE).format();

	Status.count({
		stim : {$exists : true},
		stim : {
			$gte 	: new Date(start_ts),
			$lt 	: new Date(end_ts),
		}
	},function(err,stts){
		if(err) {
			console.log("There was some error!");
			console.log(err);
			process.exit();
		}

		if(stts <= 0) {
				console.log("There were actually no seens for this period. Exiting...");
				process.exit();
		}

		console.log("Okay there were about " + stts + " seens between " + start_rev + " , " + end_rev );


		Status.distinct('pid',{
			stim : {$exists : true},
			stim : {
				$gte 	: new Date(start_ts),
				$lt 	: new Date(end_ts),
			}
		},function(err,stts){
			console.log("Also, there were about " + stts.length + " unique profiles in that, who saw a message!");
		});
	});

	rl.close();
});