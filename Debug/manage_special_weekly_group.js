var mongoose            = require('mongoose');
var readline 			      = require('readline');
var configs             = require('./../utility/configs.js');
var SpecialCategories   = require('./../utility/special_weekly_report_categories.js');

require('../models/groups.js');
require('../models/groupmembers.js');
require('../models/specialweeklygroups.js');

var Group 				      = mongoose.model('Group');
var GroupMember 				= mongoose.model('GroupMember');
var SpecialWeeklyGroup 	= mongoose.model('SpecialWeeklyGroup');

var rL = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});
mongoose.connection.on('connected', function(){
  console.log('Successfully Opened MOngoDb Connection');
  startSpecialWeeklyGroupUpdation();
});

var ADD_NEW_SPECIAL_WEEKLY_GROUP = 1;
var REMOVE_SPECIAL_WEEKLY_GROUP  = 2;
var AVAILABLE_CATEGORIES         = [SpecialCategories.ORISSA,SpecialCategories.ABHISHEK,SpecialCategories.RITULA];

function startSpecialWeeklyGroupUpdation(){
	rL.question("Enter your option ( 0 to Exit, 1 to add, 2 to remove ) : ", function(answer) {
		if(isNaN(answer)) {
			console.log('Oops , you entered a wrong option , Please enter a right option');
			return startSpecialWeeklyGroupUpdation();
		}
		switch(parseInt(answer)){
			case 1:
				addNewSpecialWeeklyGroup();
			break;

			case 2:
				removeFromSpecialWeeklyGroups();
			break;

			case 0:
				endScript();
			break;

			default:
				console.log('Oops , you entered a wrong option , Please enter a right option');
				startSpecialWeeklyGroupUpdation();
		}
	});
}

function addNewSpecialWeeklyGroup(){
	console.log('\n');
	console.log('           Folowing are Special Group Report Categories            ');
	console.log('     -----------------------------------------------------          ');
	for(var key in SpecialCategories){
		console.log('            '+key+'         :        '+SpecialCategories[key]);
	}
	console.log('\n');
	rL.question("Enter the category code from above list ( Or 0 to Exit ) : ", function(category_code) {
		if(category_code == 0 || category_code == '0')
			endScript();
		if(isNaN(category_code)) {
			console.log('Oops , you entered a wrong option , Please enter a right option');
			return addNewSpecialWeeklyGroup();
		}
		var category = parseInt(category_code);
		if(AVAILABLE_CATEGORIES.indexOf(category) <0){
			console.log('Oops , you entered a wrong category , Please enter a right category');
			return addNewSpecialWeeklyGroup();
		}
		rL.question("Enter the group code to add new special weekly group ( Or 0 to Exit ) : ", function(code) {
			if(code == 0 || code == '0')
				endScript();
			if(isNaN(code)) {
				console.log('Oops , you entered a wrong option , Please enter a right option');
				return addNewSpecialWeeklyGroup();
			}
			Group.findOne({
				code : code
			},function(err,group){
				if(err){ 
					console.trace(err); 
					return; 
				}
				if(!group) { 
					console.log('Unfortunately , the group code you entered is not valid , Please try a different group code');
					return addNewSpecialWeeklyGroup();
				}
				SpecialWeeklyGroup.findOne({
					code : code,
					gid  : group._id
				},function(err,special_weekly_group_existing){
					if(err){ 
						console.trace(err); 
						return; 
					}
					if(special_weekly_group_existing) { 
						console.log('Looks Like , the group code you entered is already a special weekly group, you may try a different group code');
						return addNewSpecialWeeklyGroup();
					}
					takeReceiverEmails(function(err,emailArray){
						if(err){ 
							console.trace(err); 
							return; 
						}
						getConfirmationFromUser(group, ADD_NEW_SPECIAL_WEEKLY_GROUP, function(confirm){
							if(confirm == 1){
								new_special_weekly_group = new SpecialWeeklyGroup({
									gid : group._id,
									code: code,
									recv: emailArray,
									catg: category
								});
								new_special_weekly_group.save(function(err, inserted_special_weekly_group){
									if(err){ 
										console.trace(err); 
										return; 
									}
									if(!inserted_special_weekly_group) {
										console.trace('Privilege group record could not be created'); 
										return endScript(); 
									}
									console.log('Congratulations , you have successfully added a new Special Weekly Group');
									endScript();
								});
							} else {
								console.log('Oops, Looks Like you were unsure of the group , try a different group code');
								return addNewSpecialWeeklyGroup();
							}
						});
					});
				});
			});
		});
	});
}

function takeReceiverEmails(cb){
	rL.question("Enter the reciever emails in comma seperetd values ( Or 0 to Exit ) : ", function(answer) {
		if(answer == 0 || answer == '0')
			endScript();
		var emails = getValidityOfEmails(answer);
		if(emails.valid == false) {
			console.log('Oops , you entered a wrong option , Please enter a right option');
			return takeReceiverEmails(cb);
		}
		return cb(null,emails.values);
	});
}

function getValidityOfEmails(commaSperatedEmails){
	if(!commaSperatedEmails) return {valid:false,values:[]};
	if(typeof(commaSperatedEmails) != 'string') return {valid:false,values:[]};
	var components = commaSperatedEmails.split(',');
	var emails = [];
	for(var i=0; i<components.length; i++){
		var email = components[i].trim();
		if(!isValidEmail(email)){
			return {valid:false,values:emails};
		}
		emails.push(email);
	}
	return {valid:true,values:emails};
}
function isValidEmail(email){
	var email_regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  return email_regex.test(email);
}

function removeFromSpecialWeeklyGroups(){
	rL.question("Enter the group code to remove a special weekly group ( Or 0 to Exit ) : ", function(code) {
		if(code == 0 || code == '0')
			endScript();
		if(isNaN(code)) {
			console.log('Oops , you entered a wrong option , Please enter a right option');
			return removeFromSpecialWeeklyGroups();
		}
		Group.findOne({
			code : code
		},function(err,group){
			if(err){ 
				console.trace(err); 
				return; 
			}
			if(!group) { 
				console.log('Unfortunately , the group code you entered is not valid , Please try a different group code');
				return removeFromSpecialWeeklyGroups();
			}
			SpecialWeeklyGroup.findOne({
				code : code,
				gid  : group._id
			},function(err,special_weekly_group_existing){
				if(err){ 
					console.trace(err); 
					return; 
				}
				if(!special_weekly_group_existing) { 
					console.log('Looks Like , the group code you entered is not a special weekly group, you may enter a different group code');
					return removeFromSpecialWeeklyGroups();
				}
				getConfirmationFromUser(group, REMOVE_SPECIAL_WEEKLY_GROUP, function(confirm){
					if(confirm == 1){
						SpecialWeeklyGroup.remove({
							gid : group._id,
							code: code
						},function(err, removed_special_weekly_group){
							if(err){ 
								console.trace(err); 
								return; 
							}
							if(!removed_special_weekly_group) {
								console.trace('Privilege group record could not be removed'); 
								return endScript(); 
							}
							console.log('Congratulations , you have successfully removed a group from special weekly Groups');
							endScript();
						});
					} else {
						console.log('Oops, Looks Like you were unsure of the group , try a different group code');
						return removeFromSpecialWeeklyGroups();
					}
				});
			});
		});
	});
}

function getConfirmationFromUser(group, action, cb){
	if(action == ADD_NEW_SPECIAL_WEEKLY_GROUP){
		console.log('The Group that you want to add to special weekly Groups is following');
		console.log(group);
		console.log('Are you sure that you wanna add this group into special weekly groups');
	} else if(action == REMOVE_SPECIAL_WEEKLY_GROUP){
		console.log('The Group that you want to remove from special weekly Groups is following');
		console.log(group);
		console.log('Are you sure that you wanna remove this group from special weekly groups');
	}
	rL.question("Press 1 to confirm , 2 if unsure ( Or 0 to Exit ) : ", function(confirmation) {
		if(confirmation == 0 || confirmation == '0')
			endScript();
		if(isNaN(confirmation)) {
			console.log('Fuck off you Loser , looks like you have to come to waste your and our time');
			endScript();
		}
		return cb(parseInt(confirmation));
	});
}

function endScript(){
	console.log('----------------------Special Weekly Group Updation Script ended------------------------');
	process.exit(0);
}


function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : 120
      }
    },
    replset : {
      rs_name : configs.MONGO_REPLICA_SET_NAME,
      socketOptions : {
        keepAlive : 120
      }
    }
  };

  if(configs.MONGO_UNAME != "") {
    var connect_string = "mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@";
    var mongo_hosts = configs.MONGO_HOST;
    var mongo_port = configs.MONGO_PORT;
    for(var i=0; i<mongo_hosts.length; i++){
      if(i > 0)
        connect_string += ",";
      connect_string += mongo_hosts[i]+":"+mongo_port;
    }
    connect_string += "/"+configs.DB_NAME,options;
    mongoose.connect(connect_string);
    // mongoose.connect( "mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@"+"127.0.0.1"+":"+configs.MONGO_PORT+"/"+configs.DB_NAME,options);
  } else {
    mongoose.connect("mongodb://"+configs.MONGO_HOST+"/"+configs.DB_NAME,options);
  }
}
