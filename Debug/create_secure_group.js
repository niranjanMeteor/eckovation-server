var mongoose            = require('mongoose');
var readline 			      = require('readline');
var redis               = require('redis');
var configs             = require('./../utility/configs.js');
var RedisPrefixes       = require('./../utility/redis_prefix.js');
var GroupInfoCache      = require('./../utility/group_info_cache.js');

var redis_client        = redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
    console.log("Error " + err);
});

var rhmset              = require("./../utility/redis_hmset.js")(redis_client);

require('../models/groups.js');
var Group 				      = mongoose.model('Group');


var rL = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});
mongoose.connection.on('connected', function(){
  console.log('Connected to mongodb!');
		console.log('------------------------------------Secure Group SCRIPT STARTED------------------------------------')
		console.log('Program will give you a chance to create a new Secure group or to remove an already existing group from the Secured groups');
  startSecureGroupUpdation();
});

var ADD_NEW_SECURE_GROUP 				= 1;
var REMOVE_SECURE_GROUP  				= 2;
var SECURED_GROUPS_DB    				= [];
var SECURED_GROUPS_REDIS 				= [];
var SECURED_GROUPS_DB_CNT 			= 0;
var SECURED_GROUPS_REDIS_CNT 		= 0;
var SECURED_GROUPS_DIFF         = [];
var SECURED_GROUPS_DIFF_CNT     = 0;

function startSecureGroupUpdation(){
	SECURED_GROUPS_DB    				= [];
	SECURED_GROUPS_REDIS 				= [];
	SECURED_GROUPS_DB_CNT 			= 0;
	SECURED_GROUPS_REDIS_CNT 		= 0;
	SECURED_GROUPS_DIFF         = [];
	SECURED_GROUPS_DIFF_CNT     = 0;
	console.log('\n');
	console.log('****************************** MAIN MENU OPTIONS *******************************');
	console.log('\n');
	rL.question("Enter your option (1 to add, 2 to remove, 3 to View, 0 to Exit) : ", function(answer) {
		if(isNaN(answer)) {
			console.log('Oops , you entered a wrong option , Please enter a right option');
			return startSecureGroupUpdation();
		}
		switch(parseInt(answer)){
			case 1:
				return addNewSecureGroup();
			break;

			case 2:
				return removeFromSecureGroups();
			break;

			case 3:
				return viewSecureGroups();
			break;

			case 0:
				endScript();
			break;

			default:
				console.log('Oops , you entered a wrong option , Please enter a right option');
				return startSecureGroupUpdation();
		}
	});
}

function addNewSecureGroup(){
	rL.question("Enter the group code to add new Secure group ( Or 0 to Exit ) : ", function(code) {
		if(code == 0 || code == '0')
			endScript();
		if(isNaN(code)) {
			console.log('Oops , you entered a wrong option , Please enter a right option');
			return addNewSecureGroup();
		}
		Group.findOne({
			code : code,
		},function(err,group){
			if(err) { console.trace(err); return; }
			if(!group) { 
				console.log('Unfortunately, the group code you entered is not valid , Please try a different group code');
				return addNewSecureGroup();
			}
			if(group.act === false) { 
				console.log('Unfortunately, the group you want to make secure is not active anymore , Please try a different group code');
				return addNewSecureGroup();
			}
			if(group.gpic == null || typeof(group.gpic) === 'undefined') { 
				console.log('Unfortunately, the group does not have any group pic , Please try a different group code');
				return addNewSecureGroup();
			}
			if(group.secr){
				console.log('Oops, This Group is already a Secured group. Please try a different group');
				return addNewSecureGroup();
			}
			getConfirmationFromUser(group, ADD_NEW_SECURE_GROUP, function(confirm){
				if(confirm == 1){
					var key = RedisPrefixes.GROUPS;
					rhmset.removefield(key, group._id+'', function(err,secure_cache_rem){
						if(err){
							console.trace(err);
							process.exit(0);
						}
						Group.update({
							_id : group._id
						},{
							secr : true
						},function(err,secured_group){
							if(err){
								console.trace(err);
								process.exit(0);
							}
							rhmset.removefield(key, group._id+'', function(err,secure_cache_rem){
								if(err){
									console.trace(err);
									process.exit(0);
								}
								console.log('Congratulations , you have successfully added a new Secured Group');
								return startSecureGroupUpdation();
							});
						});
					});
				} else {
					console.log('Oops, Looks Like you were unsure of the group , try a different group code');
					return addNewSecureGroup();
				}
			});
		});
	});
}

function removeFromSecureGroups(){
	rL.question("Enter the group code to remove from Secure group List ( Or 0 to Exit ) : ", function(code) {
		if(code == 0 || code == '0')
			endScript();
		if(isNaN(code)) {
			console.log('Oops , you entered a wrong option , Please enter a right option');
			return removeFromSecureGroups();
		}
		Group.findOne({
			code : code,
		},function(err,group){
			if(err) { console.trace(err); return; }
			if(!group) { 
				console.log('Unfortunately, the group code you entered is not valid , Please try a different group code');
				return removeFromSecureGroups();
			}
			if(group.act === false) { 
				console.log('Unfortunately, the group you want to remove from Secure is not active anymore , Please try a different group code');
				return removeFromSecureGroups();
			}
			if(!group.secr){
				console.log('Oops, This Group is already not a Secured group. Please try a different group');
				return removeFromSecureGroups();
			}
			getConfirmationFromUser(group, REMOVE_SECURE_GROUP, function(confirm){
				if(confirm == 1){
					var key = RedisPrefixes.GROUPS;
					rhmset.removefield(key, group._id+'', function(err,secure_cache_rem){
						if(err){
							console.trace(err);
							process.exit(0);
						}
						Group.update({
							_id : group._id
						},{
							secr : false
						},function(err,secured_group){
							if(err){
								console.trace(err);
								process.exit(0);
							}
							rhmset.removefield(key, group._id+'', function(err,secure_cache_rem){
								if(err){
									console.trace(err);
									process.exit(0);
								}
								console.log('Congratulations , you have successfully removed the group from Secured Group List');
								return startSecureGroupUpdation();
							});
						});
					});
				} else {
					console.log('Oops, Looks Like you were unsure of the group , try a different group code');
					return removeFromSecureGroups();
				}
			});
		});
	});
}

function viewSecureGroups(){
	SECURED_GROUPS_DIFF_CNT = 0;
	SECURED_GROUPS_DIFF = [];
	Group.find({
		secr : true,
		act  : true
	},{
		name : 1,
		secr : 1,
		act  : 1,
		code : 1,
		gtyp : 1
	},function(err,groups){
		if(err){
			console.trace(err);
			process.exit(0);
		}
		// SECURED_GROUPS_DB     = groups;
		SECURED_GROUPS_DB_CNT = groups.length;
		if(SECURED_GROUPS_DB_CNT == 0){
			console.log('***************CURRENTLY, NO SECURED GROUPS FOUND******************');
			return startSecureGroupUpdation();
		}
		var done = 0;
		var redis_group_secr = [];
		for(var i=0; i<SECURED_GROUPS_DB_CNT; i++){
			GroupInfoCache.getMongoGroup(groups[i]._id+'', function(err,group){
				if(err){
					console.trace(err);
					process.exit(0);
				}
				done++;
				if(!group){
					console.trace('A group not found ');
					process.exit(0);
				}
				var redis_grp_obj = {
					_id : group._id,
					name: group.name,
					code: group.code,
					gtyp: group.gtyp,
					secr: group.secr
				};
				if(group.secr && group.secr == true){
					SECURED_GROUPS_REDIS_CNT++;
					// SECURED_GROUPS_REDIS.push(redis_grp_obj);
				} else {
					SECURED_GROUPS_DIFF_CNT++;
					SECURED_GROUPS_DIFF.push(redis_grp_obj);
				}
				if(done	== SECURED_GROUPS_DB_CNT){
					printSecureGroupList(groups);
				}
			});
		}
	});
}

function getConfirmationFromUser(group, action, cb){
	if(action == ADD_NEW_SECURE_GROUP){
		console.log('The Group that you want to add to Secure Groups is following');
		console.log(group);
		console.log('Are you sure that you wanna add this group into Secure groups');
	} else if(action == REMOVE_SECURE_GROUP){
		console.log('The Group that you want to remove from Secure Groups is following');
		console.log(group);
		console.log('Are you sure that you wanna remove this group from Secure groups');
	}
	rL.question("Press 1 to confirm , 2 if unsure ( Or 0 to Exit ) : ", function(confirmation) {
		if(confirmation == 0 || confirmation == '0')
			endScript();
		if(isNaN(confirmation)) {
			console.log('Fuck off you Loser , looks like you have to come to waste your and our time');
			endScript();
		}
		return cb(parseInt(confirmation));
	});
}

function printSecureGroupList(groups){
	console.log("\n");
	console.log('                  Existing Secured Groups are following')
	console.log('-----------------------------------------------------------------------');
	for(var m=0; m<groups.length; m++){
		var group = groups[m];
		console.log('{ name : '+group.name+',  code : '+group.code+',  Gtype : '+group.gtyp+' }');
	}
	console.log('-----------------------------------------------------------------------');
	console.log("\n");
	return displayConsistencyData();
}

function displayConsistencyData(){
  console.log('\n');
  console.log('------------------ CONSISTENCY RESULTS --------------------');
  console.log('Total Secure Groups in DB     :  '+SECURED_GROUPS_DB_CNT);
  console.log('Total Secure Groups in REDIS  :  '+SECURED_GROUPS_REDIS_CNT);
  console.log('\n');
  if(SECURED_GROUPS_DB_CNT != SECURED_GROUPS_REDIS_CNT){
    console.log('Seems That there is some InConsistency regarding Secure Groups between DB and Redis');
    printSecureGroupDiff();
    return consistencyOptions();
  } else {
  	console.log('\n');
    return startSecureGroupUpdation();
  }
}

function printSecureGroupDiff(){
	console.log('\n');
	console.log('----------------------Following are Secure groups inconsistent with Redis---------------------');
	for(var i=0; i<SECURED_GROUPS_DIFF_CNT; i++){
		var group = SECURED_GROUPS_DIFF[i];
		console.log('{ name : '+group.name+',  code : '+group.code+',  secr : '+group.secr+',  Gtype : '+group.gtyp+' }');
	}
	console.log('\n');
}

function consistencyOptions(){
  rL.question('Do you wanna create Consistency, ( 1 : MakeConsistent, 2 : Go to Main Menu, 0 : Exit ) : ', function(answer) {
    if(!isNaN(answer)) {
      var limit = parseInt(answer);
      switch(parseInt(answer)){
        case 1:
          makeSecureListConsistentForDbAndRedis();
        break;

        case 2:
          startSecureGroupUpdation();
        break;

        case 0:
          endScript();
        break;

        default:
          console.log('Oops , you entered a wrong option , Please enter a right option');
          consistencyOptions();
      }
    }
  });
}

function makeSecureListConsistentForDbAndRedis(){
	var done = 0;
	var key = RedisPrefixes.GROUPS;
	for(var i=0; i<SECURED_GROUPS_DIFF_CNT; i++){
		var group = SECURED_GROUPS_DIFF[i];
		rhmset.removefield(key, group._id+'', function(err,secure_cache_rem){
			if(err){
				console.trace(err);
				process.exit(0);
			}
			done++;
			if(done == SECURED_GROUPS_DIFF_CNT){
				SECURED_GROUPS_DIFF_CNT = 0;
				SECURED_GROUPS_DIFF = [];
				console.log('Congratulations , you have successfully re-established the secure group consistency');
				console.log('\n');
				return startSecureGroupUpdation();
			}
		});
	}
}

function endScript(){
	console.log('---------------------- Secured Group Updation Script ended ------------------------');
	process.exit(0);
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : 120
      }
    }
  };
  if(!configs.MONGO_UNAME || configs.MONGO_UNAME == ""){
    return mongoose.connect("mongodb://"+configs.MONGO_HOST+":"+configs.MONGO_PORT+"/"+configs.DB_NAME,options);
  }
  options.replset = {
    rs_name : configs.MONGO_REPLICA_SET_NAME,
    socketOptions : {
      keepAlive : 120
    }
  };
  var connect_string = "mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@";
  var mongo_hosts    = configs.MONGO_HOST;
  var mongo_port     = configs.MONGO_PORT;
  for(var i=0; i<mongo_hosts.length; i++){
    if(i > 0)
      connect_string += ",";
    connect_string += mongo_hosts[i]+":"+mongo_port;
  }
  connect_string += "/"+configs.DB_NAME,options;
  return mongoose.connect(connect_string);
}

