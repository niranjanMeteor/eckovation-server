var socketio            = require('socket.io');
var mongoose            = require('mongoose');
var validator           = require('validator');
var kue                 = require('kue');
var redis               = require('redis');
var _                   = require('underscore');
var readline 			      = require('readline');

var tokenValidator      = require('./../utility/token_validator.js');
var configs             = require('./../utility/configs.js');

var connect = function () {
  var options = {server : {socketOptions : {keepAlive : 1}}};
  if(configs.MONGO_UNAME!="") {
    mongoose.connect("mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@"+configs.MONGO_HOST+"/"+configs.DB_NAME,options);
  } else {
    mongoose.connect("mongodb://"+configs.MONGO_HOST+"/"+configs.DB_NAME,options);
  }
}
connect();
mongoose.connection.on('error',console.log);
mongoose.connection.on('disconnected', connect);

require('../models/profiles.js');
var Profile             = mongoose.model('Profile');

var redis_client        = redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
    console.log("Error " + err);
});

var SOCKET_PREFIX       = "s";
var S_COUNT_PREFIX      = "seen_count";

var rmc                 = require("./../utility/redis_mongo_cache.js")(redis_client);
var rcq                 = require("./../utility/redis_chat_queue_1.js")(redis_client);
var rsk                 = require("./../utility/redis_socket_key.js")(redis_client,SOCKET_PREFIX);
var rseencount          = require("./../utility/redis_key_store.js")(redis_client,S_COUNT_PREFIX);
var rcustomq            = require("./../utility/redis_custom_queue.js")(redis_client);

var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.question("This script consumes more than 99% of CPU, Do you wish to proceed (y/n): ", function(answer) {
	if(answer === "y") {
		Profile.find({act:true},function(err, profiles) {
			get_profile_chat_queues(profiles,function(total_rcq_msgs) {
				console.log("Total messages still in memory!: " + total_rcq_msgs);
				console.log("Average in memory msgs per profile!: " + total_rcq_msgs/profiles.length);
			});
        });
	}
	//scan_profile_queue(profile_id);
	rl.close();
});


var get_profile_chat_queues = function(profiles,cb) {
	var total = 0;

	var done = 0;

	for(var i = 0; i < profiles.length; i++) {
		var profile_id = profiles[i]["_id"];

		rcq.getall(profile_id,function(err,profile_msgs){

			total += profile_msgs.length;

			console.log(total+"\n");

			if(++done == profiles.length) {
				cb(total);
			}
		});
	}
};