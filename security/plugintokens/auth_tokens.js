var jwt    									= require('jsonwebtoken');
var configs     				    = require('../../utility/configs.js');

exports.getUserPluginToken = function(object_for_token, private_key){
	if(!private_key){
		private_key 				 = configs.USER_IDENTIFIER_TOKEN_PRIVATE_KEY;
	}
	var expiryTime         = configs.USER_IDENTIFIER_TOKEN_VALIDITY_TIME;
	object_for_token.hcode = randomNumberOfLength(configs.USER_IDENTIFIER_HCODE_LENGTH);
	return jwt.sign(
		object_for_token, 
		private_key, 
		{
	  	expiresIn: expiryTime
		}
	);
};

function randomNumberOfLength(length) {
  return Math.floor(Math.pow(10, length-1) + Math.random() * (Math.pow(10, length) - Math.pow(10, length-1) - 1));
}