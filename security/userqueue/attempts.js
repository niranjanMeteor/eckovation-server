var redis               = require('redis');

var configs					    = require('./../../utility/configs.js');
var RedisPrefixes 			= require('./../../utility/redis_prefix.js');

var redis_client        = redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
  console.log("Error " + err);
});

var rhmset              = require("./../../utility/redis_hmset.js")(redis_client);

exports.check = function(aid, tim, cb){
	var key = RedisPrefixes.BRUTE_FORCE_USERQUEUE+aid+'';
	rhmset.getvals(key,function(err, bruteForceData){
		if(err){
			return cb(err,null);
		}
		if(!bruteForceData){
			return cb(null,false);
		}
		var numberOfAttempts   = bruteForceData[0];
		var lastAttemptTime    = bruteForceData[1];
		if((numberOfAttempts >= configs.USERRCQ_DROP_MAX_ATTEMPTS_ALLOWED) && 
			  (tim - lastAttemptTime) < configs.TIME_TO_PAUSE_USERQUEUE_TOKEN_ATTEMPTS){
			return cb(null,{lmt:numberOfAttempts,ltm:lastAttemptTime});
		}
		return cb(null,false);
	});
};

exports.update = function(aid, tim, cb){
	var key = RedisPrefixes.BRUTE_FORCE_USERQUEUE+aid+'';
	rhmset.get(key, "lmt", function(err, attemptsTillNow){
		if(err){
			return cb(err,null);
		}
		if(!attemptsTillNow){
			rhmset.updtUsrQAtmpt(key, "lmt", 1, "ltm", tim, function(err,resp){
				if(err){
					return cb(err,null);
				}
				if(resp){
					return cb(null,true);
				} else {
					return cb("first_userqueue_attempt_and_tim_not_updated",null);
				}
			});
		} else {
			var lmt = parseInt(attemptsTillNow)+1;
			rhmset.updtUsrQAtmpt(key, "lmt", lmt, function(err,resp){
				if(err){
					return cb(err,null);
				}
				if(resp){
					return cb(null,true);
				} else {
					return cb("userqueue_attempt_and_tim_not_updated",null);
				}
			});
		}
	});
};

exports.remove = function(aid, cb){
	var key = RedisPrefixes.BRUTE_FORCE_USERQUEUE+aid+'';
	rhmset.removekey(key,function(err,resp){
		if(err){
			return cb(err,null);
		}
		if(resp){
			return cb(null,true);
		} else {
			return cb(null,"aid_not_marked_in_userqueue_attempt");
		}
	});
};