var redis               = require('redis');

var configs							= require('../utility/configs.js');
var RedisPrefixes       = require('../utility/redis_prefix.js');
var log4jsLogger  		  = require('../loggers/log4js_module.js');
var log4jsConfigs  		  = require('../loggers/log4js_configs.js');


var redis_client        = redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
  console.log("Error " + err);
});

var rhmset              = require("../utility/redis_hmset.js")(redis_client);
var rcq                 = require("../utility/redis_chat_queue.js")(redis_client);

var logger        			= log4jsLogger.getLogger(log4jsConfigs.CATEGORY_USERQUEUE);

exports.dropPidQ = function(callee, pid, cb){
	rcq.removekey(pid, function(err,resp){
		if(err){
			logger.error({"r":callee,"msg":"dropPidQ_error","p":pid,"er":err});
		}
		logger.info({"r":callee,"msg":"dropPidQ_exection","p":pid});
		return cb(null,true);
	});
};

exports.dropPidsQ = function(callee, pids, cb){
	var total   = pids.length;
	var done    = 0;
	var errors  = 0;
	var success = 0;
	for(var i=0; i<total; i++){
		rcq.removekey(pids[i], function(err,resp){
			if(err){
				logger.error({"r":callee,"msg":"dropPidsQ_error","p":pids,"er":err});
				errors++;
			} else {
				success++;
			}
			done++;
			if(done == total){
				logger.info({"r":callee,"msg":"dropPidsQ_exection","p":pids,"er_cnt":errors,"succ_cnt":success});
				return cb(null,true);
			}
		});
	}
};

exports.dropAidQ = function(callee, aid, cb){
	rcq.removekey(aid, function(err,resp){
		if(err){
			logger.error({"r":callee,"msg":"dropAidQ_error","p":aid,"er":err});
		}
		logger.info({"r":callee,"msg":"dropAidQ_exection","p":aid});
		return cb(null,true);
	});
};

exports.dropPidsSeenCountQ = function(callee, pids, cb){
	var total   = pids.length;
	var done    = 0;
	var errors  = 0;
	var success = 0;
	for(var i=0; i<total; i++){
		rcq.removekeyWithPrefix(RedisPrefixes.SEEN_COUNT, pids[i], function(err,resp){
			if(err){
				logger.error({"r":callee,"msg":"dropPidsSeenCountQ_error","p":pids,"er":err});
				errors++;
			} else {
				success++;
			}
			done++;
			if(done == total){
				logger.info({"r":callee,"msg":"dropPidsSeenCountQ_exection","p":pids,"er_cnt":errors,"succ_cnt":success});
				return cb(null,true);
			}
		});
	}
};

