var http 										= require('http');
var configs 								= require('../utility/configs.js');
var SmsEngine         			= require('../utility/sms/sms_engines.js');
var SmsEngineConfigs  			= require('../utility/sms/sms_engines_config.js');
var mongoose          			= require('mongoose');
var https             			= require('https');

var PROTOCOL 								= "http";
var httpUrl           			= "http";
var httpsUrl          			= "https";

//http://smsalertbox.com/api/sms.php?uid=616b73686174676f656c&pin=52d3b166edf7f&sender=ECKEDU&route=5&tempid=2&mobile=9800125211&message=Your%20OTP%20is%20100.%20Please%20enter%20it%20in%20your%20app.%20Thanks%20for%20downloading%20Eckovation.&pushid=1
//https://control.msg91.com/api/sendhttp.php?authkey=YourAuthKey&mobiles=919999999990,919999999999&message=message&sender=ECKVTN&route=1&country=0

exports.sender = function(engine, country_code, phone, message, callback) {
	// if(process.env.NODE_ENV == "ci" || process.env.NODE_ENV == "debug" || process.env.NODE_ENV == "dapi") {
	// 	return callback({},true);
	// }
	if(engine == SmsEngine.NEXMO){
		return brandNexmoSms(engine, country_code, phone, message, callback);
	} else {
		return brandLocalSms(engine, country_code, phone, message, callback);
	}
}

function brandNexmoSms(engine, ccode, phone, message, callback){
	var smsConfigs = SmsEngineConfigs(engine);
	var credentials  = {
		api_key    : smsConfigs.API_KEY,
		api_secret : smsConfigs.API_SECRET,
		to         : ccode+''+phone
	};
	var options = {
	  host       : smsConfigs.HOST,
	  port       : smsConfigs.PORT,
	  method     : 'POST'
	};
	if(ccode == '1'){
		return callback("NEXMO_SMS_SERVICE_REJECTS_FOR_USA_CLIENTS",false);
	} else {
		credentials.from = smsConfigs.FROM;
		credentials.text = message;
		options.path     = smsConfigs.API_PATH;
	}
	
	var data = JSON.stringify(credentials);
	options.headers = {
    'Content-Type': 'application/json',
    'Content-Length': Buffer.byteLength(data)
  };
	

	var req = https.request(options);
	req.write(data);
	req.end();
	var responseData = '';
	req.on('response', function(res){
	  res.on('data', function(chunk){
	   responseData += chunk;
	  });
	  var decodedResponse;
	  res.on('end', function(){
	  	try{
		    decodedResponse = JSON.parse(responseData);
		    var respData = decodedResponse['messages'][0];
		    if(respData["status"] == "0") {
		      console.log('Success sms for engine :'+engine+' ,phone : '+phone+' ,ccode : '+ccode);
		      return callback(respData,true);
		    } else {
		    	console.log('failure sms for engine :'+engine+' ,phone : '+phone+' ,ccode : '+ccode);
		      console.log(decodedResponse);
		      return callback(respData,false);
		    }
		  } catch(e){
		   	console.log(e);
		   	console.log('Excepetion raised, failure sms for engine :'+engine+' ,phone : '+phone+' ,ccode : '+ccode);
		  	console.log(responseData);
		  	return callback(responseData,false);
		  }
	  });
	});
}

function brandLocalSms(engine, ccode, phone, message, callback){
	var MakeCall, method, was_queued;
	var urlAndProtocol = getSmsSendingUrl(engine, ccode, phone, message);
	if(urlAndProtocol.protocol == httpUrl){
		MakeCall     = http;
		method       = 'http';
	} else if(urlAndProtocol.protocol == httpsUrl){
		MakeCall     = https;
		method       = 'https';
	}
	console.log('http SmsUrl request : '+urlAndProtocol.smsUrl);
	try {
		MakeCall.get(urlAndProtocol.smsUrl,function(response){
			response.setEncoding('utf8');
			response.on("data",function(data) {
				var was_queued = checkResponseStatus(data,engine);
				if (was_queued) {
					console.log(method+' Success sms for engine :'+engine+' ,phone : '+phone+' ,ccode : '+ccode);
				} else {
					console.log(method+' Failure sms for engine :'+engine+' ,phone : '+phone+' ,ccode : '+ccode);
					console.log(data);
				}
				return callback(data,was_queued);
			});
		});
	} catch(excp){
		console.log(excp);
		console.log(method+' Exception and Failure sms for engine :'+engine+' ,phone : '+phone+' ,ccode : '+ccode);
		console.log(data);
		return callback(null,false);
	}
}

function getSmsSendingUrl(engine, country_code, phone, message){
	var smsConfigs      = SmsEngineConfigs(engine);
	var HOST            = smsConfigs.HOST;
	var API_PATH        = smsConfigs.SEND_API_PATH;
	var CALL_PROTOCOL   = smsConfigs.PROTOCOL;
	var params          = {};
	
	params.sender       = smsConfigs.SENDER_ID;
	params.route        = smsConfigs.ROUTE;
	params.message      = message;

	switch(engine){
		case SmsEngine.MSG_91:
			params.mobiles  = country_code+''+phone;
			params.authkey  = smsConfigs.AUTH_KEY;
			params.country  = 0;
			params.response = "json";
		break;
		case SmsEngine.ALERTBOX:
			params.mobile   = phone;
			params.uid      = smsConfigs.USERNAME;
			params.pin      = smsConfigs.PASSWORD;
			params.tempid   = smsConfigs.TEMPID;
			params.pushid   = smsConfigs.PUSHID;
		break;
	}
	params["message"]   = encodeURIComponent(params["message"]);

	var tmp = new Array();
	for(var key in params) {
		tmp.push(key+"="+params[key]);
	}
	var query_string = tmp.join("&");
	var url = CALL_PROTOCOL+"://"+HOST+API_PATH+"?"+query_string;

	return {protocol : CALL_PROTOCOL, smsUrl : url};
}

function checkResponseStatus(response,engine){
	switch(engine){
		case SmsEngine.MSG_91:
			try{
				response = JSON.parse(response);
				if(response.type == "success")
					return true;
				else return false;
			} catch (e) {  
				console.log('exception occurent from MSG_91 SMS SERVICE');
				console.log(e);
				console.log(response);
				return false;
			}

		case SmsEngine.ALERTBOX:
			var regex = /^[0-9]+$/;
			return regex.test(response);
	}
}
