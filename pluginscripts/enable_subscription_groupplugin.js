var mongoose            = require('mongoose');
var readline 			      = require('readline');
var configs             = require('../utility/configs.js');

require('../models/groups.js');
require('../models/groupplugins.js');
require('../models/packages/grouppluginpackages.js');
var Group 				          = mongoose.model('Group');
var GroupPlugin 					  = mongoose.model('GroupPlugin');
var GroupPluginPackage 			= mongoose.model('GroupPluginPackage');


var rL = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});
mongoose.connection.on('connected', function(){
  console.log('Connected to mongodb!');
		console.log('------------------------------------GROUP_PLUGIN_PACKAGE SUBSCRIPTION_MANAGEMENT SCRIPT STARTED------------------------------------')
		console.log('Program will give you a chance to enable or disable subscription in group plugin');
  manageGroupPluginSubscription();
});

function manageGroupPluginSubscription(){
	console.log('\n');
	console.log('****************************** MAIN MENU OPTIONS *******************************');
	console.log('\n');
	rL.question("Enter your option (1 to add, 2 to remove, 3 to View, 0 to Exit) : ", function(answer) {
		if(isNaN(answer)) {
			console.log('Oops , you entered a wrong option , Please enter a right option');
			return manageGroupPluginSubscription();
		}
		switch(parseInt(answer)){
			case 1:
				return enableSubsciptionGroupPlugin();
			break;

			case 2:
				return disableSubsciptionGroupPlugin();
			break;

			case 3:
				return viewGroupPluginSubscriptions();
			break;

			case 0:
				endScript();
			break;

			default:
				console.log('Oops , you entered a wrong option , Please enter a right option');
				return manageGroupPluginSubscription();
		}
	});
}

function enableSubsciptionGroupPlugin(){
	rL.question("Enter the group_code and plugin_id in comma seperated ( Or 0 to Exit ) : ", function(code_plugin) {
    if(code_plugin == 0 || code_plugin == '0')
      endScript();
    if(!code_plugin) {
      console.log('Oops , you entered a wrong option, Please enter a right option');
      return enableSubsciptionGroupPlugin();
    }
    code_plugin = code_plugin.trim();
    var values  = code_plugin.split(",");
    var code    = values[0];
    code = code.trim();
    if(isNaN(code) || code.length < 6) {
      console.log('Oops , you entered a wrong group_code option, Please enter a right group_code');
      return enableSubsciptionGroupPlugin();
    }
    var plugin_id = values[1];
    plugin_id     = plugin_id.trim();
    if(!plugin_id || plugin_id.length < 24) {
      console.log('Oops , you entered a wrong plugin_id, Please enter a right plugin_id');
      return enableSubsciptionGroupPlugin();
    }
    var tim = new Date().getTime();
    Group.findOne({
      code : code
    },function(err,group){
      if(err) { 
        console.trace(err); 
        return process.exit(0); 
      }
      if(!group) { 
        console.log('Unfortunately, the group code you entered is not valid, Please try a different group code');
        return enableSubsciptionGroupPlugin();
      }
      if(group.act === false) { 
        console.log('Unfortunately, the group you want to remove from Secure is not active anymore, Please try a different group code');
        return enableSubsciptionGroupPlugin();
      }
      GroupPlugin.findOne({
        gid       : group._id,
        plgnid    : plugin_id,
        act       : true
      },function(err,group_plugin){
        if(err) { 
          console.trace(err); 
          return process.exit(0); 
        }
        if(!group_plugin) { 
	        console.log('Unfortunately, plugin is not active in group, Please try a different one');
	        return enableSubsciptionGroupPlugin();
	      }
	      GroupPlugin.update({
          gid       : group._id,
          plgnid    : plugin_id,
          act       : true
	      },{
          req_subscription : true
        },function(err,packages){
	      	if(err){
	      		console.trace(err);
		      	return process.exit(0);
	      	}
          if(!group_plugin.promo_url || group_plugin.promo_url.length == 0){
            GroupPlugin.update({
              gid       : group._id,
              plgnid    : plugin_id,
              act       : true
            },{
              $push :  {promo_url : "https://s3-ap-southeast-1.amazonaws.com/web-public-res/publicgroups/plugins/payment_teaser.png"}
            },function(err,packages){
              if(err){
                console.trace(err);
                return process.exit(0);
              }
    	      	console.log('\n');
    	      	console.log('Congratulations, GroupPlugin Subscription Required Has Been Enabled');
    	      	console.log('---------------------------------------------------------');
              return manageGroupPluginSubscription();
            });
          } else {
            console.log('\n');
            console.log('Congratulations, GroupPlugin Subscription Required Has Been Enabled');
            console.log('---------------------------------------------------------');
            return manageGroupPluginSubscription();
          }
		    });
      });
    });
	});
}

function disableSubsciptionGroupPlugin(){
	console.log('\n');
	console.log('This Feature is Awaited.......');
	return manageGroupPluginSubscription();
}

function viewGroupPluginSubscriptions(){
	console.log('\n');
	console.log('This Feature is Awaited.......');
	return manageGroupPluginSubscription();
}

function endScript(){
  console.log('Thanks For using our service, Hope to serve you soon');
  return process.exit(0);
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : 120
      }
    }
  };
  if(!configs.MONGO_UNAME || configs.MONGO_UNAME == ""){
    return mongoose.connect("mongodb://"+configs.MONGO_HOST+":"+configs.MONGO_PORT+"/"+configs.DB_NAME,options);
  }
  options.replset = {
    rs_name : configs.MONGO_REPLICA_SET_NAME,
    socketOptions : {
      keepAlive : 120
    }
  };
  var connect_string = "mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@";
  var mongo_hosts    = configs.MONGO_HOST;
  var mongo_port     = configs.MONGO_PORT;
  for(var i=0; i<mongo_hosts.length; i++){
    if(i > 0)
      connect_string += ",";
    connect_string += mongo_hosts[i]+":"+mongo_port;
  }
  connect_string += "/"+configs.DB_NAME;
  return mongoose.connect(connect_string,options);
}