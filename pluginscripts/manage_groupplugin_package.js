var mongoose            = require('mongoose');
var readline 			      = require('readline');
var configs             = require('../utility/configs.js');

require('../models/groups.js');
require('../models/groupplugins.js');
require('../models/packages/grouppluginpackages.js');
var Group 				          = mongoose.model('Group');
var GroupPlugin 					  = mongoose.model('GroupPlugin');
var GroupPluginPackage 			= mongoose.model('GroupPluginPackage');


var rL = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});
mongoose.connection.on('connected', function(){
  console.log('Connected to mongodb!');
		console.log('------------------------------------GROUP_PLUGIN_PACKAGE_MANAGEMENT SCRIPT STARTED------------------------------------')
		console.log('Program will give you a chance to create a new package in a plugin of the group');
  manageGroupPluginPackage();
});

function manageGroupPluginPackage(){
	console.log('\n');
	console.log('****************************** MAIN MENU OPTIONS *******************************');
	console.log('\n');
	rL.question("Enter your option (1 to add, 2 to remove, 3 to View, 0 to Exit) : ", function(answer) {
		if(isNaN(answer)) {
			console.log('Oops , you entered a wrong option , Please enter a right option');
			return manageGroupPluginPackage();
		}
		switch(parseInt(answer)){
			case 1:
				return addNewGroupPluginPackage();
			break;

			case 2:
				return removeGroupPluginPackage();
			break;

			case 3:
				return viewGroupPluginPackage();
			break;

			case 0:
				endScript();
			break;

			default:
				console.log('Oops , you entered a wrong option , Please enter a right option');
				return manageGroupPluginPackage();
		}
	});
}

function addNewGroupPluginPackage(){
	rL.question("Enter the group_code and plugin_id in comma seperated ( Or 0 to Exit ) : ", function(code_plugin) {
    if(code_plugin == 0 || code_plugin == '0')
      endScript();
    if(!code_plugin) {
      console.log('Oops , you entered a wrong option, Please enter a right option');
      return addNewGroupPluginPackage();
    }
    code_plugin = code_plugin.trim();
    var values  = code_plugin.split(",");
    var code    = values[0];
    code = code.trim();
    if(isNaN(code) || code.length < 6) {
      console.log('Oops , you entered a wrong group_code option, Please enter a right group_code');
      return addNewGroupPluginPackage();
    }
    var plugin_id = values[1];
    plugin_id     = plugin_id.trim();
    if(!plugin_id || plugin_id.length < 24) {
      console.log('Oops , you entered a wrong plugin_id, Please enter a right plugin_id');
      return addNewGroupPluginPackage();
    }
    var tim = new Date().getTime();
    Group.findOne({
      code : code
    },function(err,group){
      if(err) { 
        console.trace(err); 
        return process.exit(0); 
      }
      if(!group) { 
        console.log('Unfortunately, the group code you entered is not valid, Please try a different group code');
        return addNewGroupPluginPackage();
      }
      if(group.act === false) { 
        console.log('Unfortunately, the group you want to remove from Secure is not active anymore, Please try a different group code');
        return addNewGroupPluginPackage();
      }
      GroupPlugin.findOne({
        gid       : group._id,
        plgnid    : plugin_id,
        act       : true
      },function(err,group_plugin){
        if(err) { 
          console.trace(err); 
          return process.exit(0); 
        }
        if(!group_plugin) { 
	        console.log('Unfortunately, plugin is not active in group, Please try a different one');
	        return addNewGroupPluginPackage();
	      }
	      console.log('\n');
	      console.log('Now Fill Package Basic Info');
	      console.log('\n');
	      GroupPluginPackage.find({
	      	gid       : group._id,
	      	plugin_id : plugin_id,
	      	act       : true
	      },function(err,packages){
	      	if(err){
	      		console.trace(err);
		      	return process.exit(0);
	      	}
	      	console.log('\n');
	      	console.log('Following are Existing packages for this Group Plugin');
	      	console.log('---------------------------------------------------------');
	      	if(packages.length == 0){
	      		console.log('Hymm, No Existing Package, You Can Add a New Package');
	      	} else {
	      		for(var i=0; i<packages.length; i++){
	      			console.log(packages[i]);
	      		}
	      	}
	      	console.log('\n');
	      	console.log('CREATE A NEW GROUP PLUGIN PACKAGE');
	      	console.log('\n');
		      createPackage(plugin_id, group._id, function(err,resp){
		      	if(err){
		      		console.trace(err);
		      		return process.exit(0);
		      	}
		      	console.log('\n');
			      console.log('Congratualtion , your package has been created and added to the plugin in the group');
			      console.log('\n');
			      return addNewGroupPluginPackage();
		      });
		    });
      });
    });
	});
}

function removeGroupPluginPackage(){
	console.log('\n');
	console.log('This Feature is Awaited.......');
	return manageGroupPluginPackage();
}

function viewGroupPluginPackage(){
	console.log('\n');
	console.log('This Feature is Awaited.......');
	return manageGroupPluginPackage();
}

function createPackage(plugin_id, group_id, cb){
	rL.question("PACKAGE NAME : ", function(name) {
		if(!name){
			console.log('Oops, PACKAGE_NAME IS COMPULSORY');
			return createPackage(plugin_id, group_id, cb);
		}
		name = name.trim();
		rL.question("PACKAGE DESC : ", function(desc) {
			if(!desc){
				console.log('Oops, DESC IS COMPULSORY');
				return createPackage(plugin_id, group_id, cb);
			}
			desc = desc.trim();
			rL.question("PACKAGE PRICE : ", function(price) {
				if(!price){
					console.log('Oops, PRICE IS COMPULSORY');
					return createPackage(plugin_id, group_id, cb);
				}
				price = price.trim();
				rL.question("DURATION BASE (e.g Hours, Days, Months, Years) : ", function(base) {
					if(!base){
						console.log('Oops, BASE IS COMPULSORY');
						return createPackage(plugin_id, group_id, cb);
					}
					base = base.trim();
					rL.question("DURATION UNIT : ", function(unit) {
						if(!unit){
							console.log('Oops, BASE IS COMPULSORY');
							return createPackage(plugin_id, group_id, cb);
						}
						unit = unit.trim();
						rL.question("PACKAGE DURATION IN MILISECONDS : ", function(duration) {
							if(!duration){
								console.log('Oops, PACKAGE_DURATION IS COMPULSORY');
								return createPackage(plugin_id, group_id, cb);
							}
							duration = duration.trim();
							var new_plugin_package = new GroupPluginPackage({
								gid        : group_id,
								plugin_id  : plugin_id,
								act        : true,
								package_name : name,
								package_desc : desc,
								package_price: price,
								acc          : "1",
								duration_unit: unit,
								duration_base: base,
								package_duration : duration
							});
							new_plugin_package.save(function(err,created_doc){
								if(err){
									console.trace(err);
									process.exit(0);
								}
								return cb(null,true);
							});
						});
					});
				});
			});
		});
	});
}

function endScript(){
  console.log('Thanks For using our service, Hope to serve you soon');
  return process.exit(0);
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : 120
      }
    }
  };
  if(!configs.MONGO_UNAME || configs.MONGO_UNAME == ""){
    return mongoose.connect("mongodb://"+configs.MONGO_HOST+":"+configs.MONGO_PORT+"/"+configs.DB_NAME,options);
  }
  options.replset = {
    rs_name : configs.MONGO_REPLICA_SET_NAME,
    socketOptions : {
      keepAlive : 120
    }
  };
  var connect_string = "mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@";
  var mongo_hosts    = configs.MONGO_HOST;
  var mongo_port     = configs.MONGO_PORT;
  for(var i=0; i<mongo_hosts.length; i++){
    if(i > 0)
      connect_string += ",";
    connect_string += mongo_hosts[i]+":"+mongo_port;
  }
  connect_string += "/"+configs.DB_NAME;
  return mongoose.connect(connect_string,options);
}