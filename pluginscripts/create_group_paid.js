var mongoose            = require('mongoose');
var readline 			      = require('readline');
var redis               = require('redis');

var configs             = require('../utility/configs.js');
var pluginServerConfig  = require('../pluginserver/configs.js');
var GroupInfoCache      = require('../utility/group_info_cache.js');
var UserInfoCache       = require('../utility/user_info_cache.js');

require('../models/groups.js');
require('../models/plugins.js');
require('../models/groupmembers.js');
require('../models/groupplugins.js');
require('../models/pluginconfigs.js');
var Group                 = mongoose.model('Group');
var AppPlugin 					  = mongoose.model('AppPlugin');
var PluginConfig          = mongoose.model('PluginConfig');
var GroupMember           = mongoose.model('GroupMember');
var GroupPlugin           = mongoose.model('GroupPlugin');

var redis_client          = redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
    console.log("Error " + err);
});


var rL = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});
mongoose.connection.on('connected', function(){
  console.log('Connected to mongodb!');
	console.log('------------------------------------CREATE PAID GROUP STARTED------------------------------------')
	console.log('Program will give you a chance to create a new Paid Group and Specify its Configs Also');
  createPlugin();
});

function createPlugin(){
  console.log('\n');
  console.log('GOING TO CREATE A NEW PLUGIN FOR ECKOVATION')
  console.log('FOLLOWING DETAILS ARE REQUIRED FOR CREATING A NEW PLUGIN');
  console.log('\n');
  rL.question("GROUP CODE: ", function(gcode) {
    if(!gcode || isNaN(gcode) || gcode.length != 6){
      console.log('Oops, GROUP CODE IS INVALID');
      return createPlugin();
    }
    Group.findOne({
      code : gcode,
      act  : true
    },function(err,group){
      if(err){
        console.trace(err);
        return process.exit(0);
      }
      if(!group){
        console.log('Oops, GROUP CODE DOES NOT EXISTS');
        return createPlugin();
      }
      if(group.req_sub && group.req_sub == true){
        console.log('Oops, GROUP IS ALREADY PAID');
        return createPlugin();
      }
      GroupPlugin.update({
        gid  : group._id,
        act  : true,
        actv : GroupPlugin.TYPE.GROUP_JOIN 
      },{
        act : false
      },function(err,previous_groupjoin_deactivated){
        if(err){
          console.trace(err);
          return process.exit(0);
        }
        rL.question("NAME OF THE PLUGIN: ", function(name) {
          if(!name){
            console.log('Oops, PLUGIN_NAME IS COMPULSORY');
            return createPlugin();
          }
          name = name.trim();
          rL.question("PLUGIN DESC : ", function(desc) {
            if(!desc){
              console.log('Oops, DESC IS COMPULSORY');
              return createPlugin();
            }
            desc = desc.trim();
            rL.question("THUMB_URL : ", function(thumb_url) {
              if(!thumb_url){
                console.log('Oops, THUMB_URL IS COMPULSORY');
                return createPlugin();
              }
              thumb_url = thumb_url.trim();
              rL.question("SHOWCASE_URL : ", function(showcase_url) {
                if(!showcase_url){
                  console.log('Oops, SHOWCASE_URL IS COMPULSORY');
                  return createPlugin();
                }
                showcase_url = showcase_url.trim();
                rL.question("HOME_URL : ", function(home_url) {
                  if(!home_url){
                    console.log('Oops, HOME_URL IS COMPULSORY');
                    return createPlugin();
                  }
                  home_url = home_url.trim();
                  rL.question("YOUR_NAME : ", function(creatby) {
                    if(!creatby){
                      console.log('Oops, YOUR_NAME IS COMPULSORY');
                      return createPlugin();
                    }
                    creatby = creatby.trim();
                    rL.question("HOST_SERVER (Ip or Name) : ", function(host) {
                      if(!host){
                        console.log('Oops, HOST_SERVER IS COMPULSORY');
                        return createPlugin();
                      }
                      host = host.trim();
                      rL.question("HOST_SERVER_PORT : ", function(port) {
                        if(!port){
                          console.log('Oops, HOST_SERVER_PORT IS COMPULSORY');
                          return createPlugin();
                        }
                        port = port.trim();
                        var new_plugin = new AppPlugin({
                          name         : name,
                          type         : AppPlugin.TYPE.GROUPJOIN,
                          desc         : desc,
                          thumb_url    : thumb_url,
                          showcase_url : showcase_url,
                          home_url     : home_url,
                          act          : true,
                          creatby      : creatby
                        });
                        new_plugin.save(function(err,created_plugin){
                          if(err){
                            console.trace(err);
                            process.exit(0);
                          }
                          var new_plugin_config = new PluginConfig({
                            plugin_id : created_plugin._id,
                            host     : host,
                            port     : port,
                            api_path : '/eckovation/eckovationcallback',
                            api_key  : pluginServerConfig.ECKOVATION_GROUPJOIN_API_KEY,
                            req_exp  : 500
                          });
                          new_plugin_config.save(function(err,created_plugin_config){
                            if(err){
                              console.trace(err);
                              process.exit(0);
                            }
                            var new_group_plugin = new GroupPlugin({
                              plgnid : created_plugin._id,
                              gid    : group._id,
                              act    : true,
                              req_subscription : true,
                              type   : 2
                            });
                            new_group_plugin.save(function(err,created_group_plugin){
                              if(err){
                                console.trace(err);
                                process.exit(0);
                              }
                              Group.update({
                                _id : group._id
                              },{
                                req_sub : true,
                                plugin_id : created_plugin._id,
                                showcase_url : showcase_url,
                                $push : {
                                  promo_url : "https://s3-ap-southeast-1.amazonaws.com/web-public-res/publicgroups/plugins/payment_teaser.png"
                                }
                              },function(err,updated_group){
                                if(err){
                                  console.trace(err);
                                  process.exit(0);
                                }
                                GroupMember.update({
                                  gid : group._id,
                                  act : true,
                                  gdel: false
                                },{
                                  gpaid : 1
                                },{
                                  multi :true
                                },function(err,updated_group){
                                  if(err){
                                    console.trace(err);
                                    process.exit(0);
                                  }
                                  clearGroupCache(group._id,function(err,resp){
                                    if(err){
                                      console.trace(err);
                                      process.exit(0);
                                    }
                                    console.log('\n');
                                    console.log('Congratulations, Following New Plugin has been created');
                                    showCreatedPlugin(created_plugin._id, function(err,resp){
                                      if(err){
                                        console.trace(err);
                                        process.exit(0);
                                      }
                                      console.log('\n');
                                      process.exit(0);
                                    });
                                  });
                                });
                              });
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });
}

function clearGroupCache(gid, cb){
  var toDo = 2;
  var done = 0;
  GroupInfoCache.removeMongoGroup(gid, function(err,resp){
    if(err){
      return cb(err,null);
    }
    done++;
    if(done == toDo){
      return cb(null,true);
    }
  });
  clearCacheForAllGrpMemsAid(gid, function(err,resp){
    if(err){
      return cb(err,null);
    }
    done++;
    if(done == toDo){
      return cb(null,true);
    }
  });
}

function clearCacheForAllGrpMemsAid(gid, cb){
  GroupMember.find({
    gid : gid,
    act : true,
    gdel: false
  },{
    aid : 1,
    _id : 0
  },function(err,members){
    if(err){
      return cb(err,null);
    }
    var total = members.length;
    if(total == 0){
      console.log("No Members found to do clearCacheForAllGrpMemsAid");
      return cb(null,true);
    }
    var aids = [];
    for(var i=0; i<total; i++){
      if(aids.indexOf(members[i].aid) < 0){
        aids.push(members[i].aid);
      }
    }
    total = aids.length;
    var done = 0;
    for(var j=0; j<total; j++){
      UserInfoCache.rmUsrGrpMemsByAidInCache(aids[j],function(err,resp){
        if(err){
          return cb(err,null);
        }
        done++;
        if(done == total){
          return cb(null,true);
        }
      });
    }
  });
}

function showCreatedPlugin(plugin_id, cb){
  AppPlugin.findOne({
    _id : plugin_id
  },function(err,new_plugin){
    if(err){
      return cb(err,null);
    }
    PluginConfig.findOne({
      plugin_id : plugin_id
    },function(err,plugin_configs){
      if(err){
        return cb(err,null);
      }
      console.log('\n');
      console.log(JSON.stringify(new_plugin));
      console.log('\n');
      console.log(JSON.stringify(plugin_configs));
      return cb(null,true);
    });
  });
}

function endScript(){
  console.log('Thanks For using our service, Hope to serve you soon');
  return process.exit(0);
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : 120
      }
    }
  };
  if(!configs.MONGO_UNAME || configs.MONGO_UNAME == ""){
    return mongoose.connect("mongodb://"+configs.MONGO_HOST+":"+configs.MONGO_PORT+"/"+configs.DB_NAME,options);
  }
  options.replset = {
    rs_name : configs.MONGO_REPLICA_SET_NAME,
    socketOptions : {
      keepAlive : 120
    }
  };
  var connect_string = "mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@";
  var mongo_hosts    = configs.MONGO_HOST;
  var mongo_port     = configs.MONGO_PORT;
  for(var i=0; i<mongo_hosts.length; i++){
    if(i > 0)
      connect_string += ",";
    connect_string += mongo_hosts[i]+":"+mongo_port;
  }
  connect_string += "/"+configs.DB_NAME;
  return mongoose.connect(connect_string,options);
}