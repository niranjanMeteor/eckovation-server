var mongoose            = require('mongoose');
var readline 			      = require('readline');
var configs             = require('../utility/configs.js');
var pluginServerConfig  = require('../pluginserver/configs.js');

require('../models/plugins.js');
require('../models/pluginconfigs.js');
var AppPlugin 					  = mongoose.model('AppPlugin');
var PluginConfig          = mongoose.model('PluginConfig');


var rL = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});
mongoose.connection.on('connected', function(){
  console.log('Connected to mongodb!');
	console.log('------------------------------------CREATE PLUGIN SCRIPT STARTED------------------------------------')
	console.log('Program will give you a chance to create a new Plugin and Specify its Configs Also');
  createPlugin();
});

function createPlugin(){
  console.log('\n');
  console.log('GOING TO CREATE A NEW PLUGIN FOR ECKOVATION')
  console.log('FOLLOWING DETAILS ARE REQUIRED FOR CREATING A NEW PLUGIN');
  console.log('\n');
  rL.question("NAME OF THE PLUGIN: ", function(name) {
    if(!name){
      console.log('Oops, PLUGIN_NAME IS COMPULSORY');
      return createPlugin();
    }
    name = name.trim();
    rL.question("PLUGIN DESC : ", function(desc) {
      if(!desc){
        console.log('Oops, DESC IS COMPULSORY');
        return createPlugin();
      }
      desc = desc.trim();
      rL.question("THUMB_URL : ", function(thumb_url) {
        if(!thumb_url){
          console.log('Oops, THUMB_URL IS COMPULSORY');
          return createPlugin();
        }
        thumb_url = thumb_url.trim();
        rL.question("SHOWCASE_URL : ", function(showcase_url) {
          if(!showcase_url){
            console.log('Oops, SHOWCASE_URL IS COMPULSORY');
            return createPlugin();
          }
          showcase_url = showcase_url.trim();
          rL.question("HOME_URL : ", function(home_url) {
            if(!home_url){
              console.log('Oops, HOME_URL IS COMPULSORY');
              return createPlugin();
            }
            home_url = home_url.trim();
            rL.question("YOUR_NAME : ", function(creatby) {
              if(!creatby){
                console.log('Oops, YOUR_NAME IS COMPULSORY');
                return createPlugin();
              }
              creatby = creatby.trim();
              rL.question("HOST_SERVER (Ip or Name) : ", function(host) {
                if(!host){
                  console.log('Oops, HOST_SERVER IS COMPULSORY');
                  return createPlugin();
                }
                host = host.trim();
                rL.question("HOST_SERVER_PORT : ", function(port) {
                  if(!port){
                    console.log('Oops, HOST_SERVER_PORT IS COMPULSORY');
                    return createPlugin();
                  }
                  port = port.trim();
                  rL.question("TYPE_OF_PLUGIN ( 1 : VIDEO, 2 : TEST, 3 : FEE ) : ", function(type) {
                    if(!type){
                      console.log('Oops, TYPE_OF_PLUGIN IS COMPULSORY');
                      return createPlugin();
                    }
                    type = type.trim();
                    var api_key,catg;
                    if(type == 1){
                      api_key = pluginServerConfig.VIDEO_PLUGIN_API_KEY;
                      catg    = AppPlugin.CATEGORY.VIDEO;
                    } else if(type == 2) {
                      api_key = pluginServerConfig.TEST_PLUGIN_API_KEY;//generateNewApiKey();
                      catg    = AppPlugin.CATEGORY.TEST;
                    } else if(type == 3){
                      catg = AppPlugin.CATEGORY.FEE;
                    } else {
                      console.log('SEEMS THAT YOU HAVE COME HERE TO WASTE OUT TIME, FUCK YOU');
                      return createPlugin();
                    }
                    var new_plugin = new AppPlugin({
                      name         : name,
                      desc         : desc,
                      catg         : catg,
                      thumb_url    : thumb_url,
                      showcase_url : showcase_url,
                      home_url     : home_url,
                      act          : true,
                      creatby      : creatby
                    });
                    new_plugin.save(function(err,created_plugin){
                      if(err){
                        console.trace(err);
                        process.exit(0);
                      }
                      if(type == 3){
                        console.log('Following plugin has been created');
                        console.log(created_plugin);
                        process.exit(0);
                      }
                      var new_plugin_config = new PluginConfig({
                        plugin_id : created_plugin._id,
                        host     : host,
                        port     : port,
                        api_path : '/eckovation/eckovationcallback',
                        api_key  : api_key,
                        req_exp  : 500
                      });
                      new_plugin_config.save(function(err,created_plugin_config){
                        if(err){
                          console.trace(err);
                          process.exit(0);
                        }
                        console.log('\n');
                        console.log('Congratulations, Following New Plugin has been created');
                        showCreatedPlugin(created_plugin._id, function(err,resp){
                          if(err){
                            console.trace(err);
                            process.exit(0);
                          }
                          console.log('\n');
                          process.exit(0);
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });
}

function showCreatedPlugin(plugin_id, cb){
  AppPlugin.findOne({
    _id : plugin_id
  },function(err,new_plugin){
    if(err){
      return cb(err,null);
    }
    PluginConfig.findOne({
      plugin_id : plugin_id
    },function(err,plugin_configs){
      if(err){
        return cb(err,null);
      }
      console.log('\n');
      console.log(JSON.stringify(new_plugin));
      console.log('\n');
      console.log(JSON.stringify(plugin_configs));
      return cb(null,true);
    });
  });
}

function generateNewApiKey(){
  var length = 16;
  var selectionChars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  var result = '';
  for (var i = length; i > 0; --i){
    result += selectionChars[Math.floor(Math.random() * selectionChars.length)];
  }
  return result;
}

function endScript(){
  console.log('Thanks For using our service, Hope to serve you soon');
  return process.exit(0);
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : 120
      }
    }
  };
  if(!configs.MONGO_UNAME || configs.MONGO_UNAME == ""){
    return mongoose.connect("mongodb://"+configs.MONGO_HOST+":"+configs.MONGO_PORT+"/"+configs.DB_NAME,options);
  }
  options.replset = {
    rs_name : configs.MONGO_REPLICA_SET_NAME,
    socketOptions : {
      keepAlive : 120
    }
  };
  var connect_string = "mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@";
  var mongo_hosts    = configs.MONGO_HOST;
  var mongo_port     = configs.MONGO_PORT;
  for(var i=0; i<mongo_hosts.length; i++){
    if(i > 0)
      connect_string += ",";
    connect_string += mongo_hosts[i]+":"+mongo_port;
  }
  connect_string += "/"+configs.DB_NAME;
  return mongoose.connect(connect_string,options);
}