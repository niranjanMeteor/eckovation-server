var mongoose            = require('mongoose');
var readline 			      = require('readline');

var configs             = require('../utility/configs.js');
var RabbitMqPublish     = require('../rabbitmq/publisher/publisher.js');
var RabbitMqQueues      = require('../rabbitmq/queues.js');
var PaytmApi            = require('../payment/paytm_api.js');
var PaymentRoute        = require('../routes/payment.js');

var TRXN_ID = "";

var rL = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});
mongoose.connection.on('connected', function(){
  console.log('Connected to mongodb!');
	console.log('------------------------   REDO USER SUBSCRIPTION    --------------------------')
	console.log('Program will give you a chance to recreate user subscription');
  redoUserSubscription();
});

function redoUserSubscription(){
  PaytmApi.getTrxnStatus(TRXN_ID, function(err,paytm_resp){
    if(err){
      console.trace(err);
      console.log('probably , Paytm gateway getTrxnStatus Api Failed');
      return endScript();
    }
    if(!paytm_resp){
      console.log('Seems Like, Paytm does not have response for this trxn Id');
      return endScript();
    }
    console.log('Paytm reponse is Following');
    console.log(paytm_resp);
    PaymentRoute.verifyTransactionResponse(paytm_resp,function(err,resp){
      if(err){
        console.trace(err);
        console.log('probably , verifyTransactionResponse Failed');
        return endScript();
      }
      console.log('Transaction is verified');
      PaymentRoute.reDoUserSubscriptionWithOrderId(paytm_resp,function(err,resp){
        if(err){
          console.trace(err);
          console.log('probably , this could not be done, sorry dude');
        }
        console.log('Successfully Created the Subscription');
        console.log('Going to Activate the Subscription Now');
        rL.question("PRESS ENTER TO CONTINUE: ", function(name) {
          var payment_notify_object = new Buffer(JSON.stringify({
            trxn_id : TRXN_ID,
            atmpt   : 0
          }));
          RabbitMqPublish.publish("",RabbitMqQueues.PAYMENT_NOTIFY,payment_notify_object);
          console.log('Congratualtions, target User Subscription have been Activated');
          return;
        });
      });
    });
  });
}


function endScript(){
  console.log('Thanks For using our service, Hope to serve you soon');
  return process.exit(0);
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : 120
      }
    }
  };
  if(!configs.MONGO_UNAME || configs.MONGO_UNAME == ""){
    return mongoose.connect("mongodb://"+configs.MONGO_HOST+":"+configs.MONGO_PORT+"/"+configs.DB_NAME,options);
  }
  options.replset = {
    rs_name : configs.MONGO_REPLICA_SET_NAME,
    socketOptions : {
      keepAlive : 120
    }
  };
  var connect_string = "mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@";
  var mongo_hosts    = configs.MONGO_HOST;
  var mongo_port     = configs.MONGO_PORT;
  for(var i=0; i<mongo_hosts.length; i++){
    if(i > 0)
      connect_string += ",";
    connect_string += mongo_hosts[i]+":"+mongo_port;
  }
  connect_string += "/"+configs.DB_NAME;
  return mongoose.connect(connect_string,options);
}