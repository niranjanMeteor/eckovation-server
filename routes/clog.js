var express 						= require('express');
var mongoose 						= require('mongoose');
var jwt    							= require('jsonwebtoken');
var redis               = require('redis');

var configs							= require('../utility/configs.js');
var errorCodes 					= require('../utility/errors.js');
var constants						= require('../utility/constants.js');
var helpers							= require('../utility/helpers.js');
var log4jsLogger  			= require('../loggers/log4js_module.js');
var log4jsConfigs  			= require('../loggers/log4js_configs.js');

var redis_client        = redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
  console.log("Error " + err);
});

require('../models/clientlogs.js');
require('../models/contactlogs.js');
var ClientLog 					= mongoose.model('ClientLog');
var ContactLog 				  = mongoose.model('ContactLog');

var rmc                 = require("../utility/redis_mongo_cache.js")(redis_client);

var sendError 					= helpers.sendError;
var sendSuccess 				= helpers.sendSuccess;
var router 							= express.Router();
var logger        			= log4jsLogger.getLogger(log4jsConfigs.CATEGORY_ROUTES_CLIENTLOG);

//route middleware to verify the jwt token for all users
router.use(function(req, res, next){
	var token = req.headers['x-access-token'];
	if(!token){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"token_not_found","p":token});
	  return sendError(res,"Access without token is not authorised","token_not_found",constants.HTTP_STATUS.FORBIDDEN);
	}
  jwt.verify(token, configs.JWT_ACCESS_TOKEN_PRIVATE_KEY, function(err, decoded) {      
    if(err){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"jwt_verify_error","p":token,"er":err});
      return sendError(res,"Failed to authenticate token.","token_auth_failure",constants.HTTP_STATUS.TOKEN_EXPIRED);  
    } else {
    	req.decoded = decoded;
      next();
    }
  });
});

router.post('/subm', function(req, res, next){
	req.checkBody('a_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('dt',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('l',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()){ 
		logger.error({"r":"subm","msg":"invalid_parameters","p":req.body});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}
	
	var account_id    = req.body.a_id;
	var data          = req.body.dt;
	var remaining     = (isNaN(req.body.l)) ? "NA" : req.body.l;
	var tim           = new Date().getTime();
	delete req.body["dt"];
	
	if(req.decoded.id != account_id){
		logger.error({"r":"subm","msg":"token_id_mismatch_with_accountId","p":req.body});
	  return sendError(res,"Access without token is not authorised","forbidden",constants.HTTP_STATUS.FORBIDDEN);
	}
	
	if(!data || !checkIfArray(data)){
		logger.error({"r":"subm","msg":"data_is_invalid","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}
	if(data.length == 0 || data.length > configs.CLIENTLOG_SUBMIT_QUOTA_LIMIT){
		logger.error({"r":"subm","msg":"data_length_quota_exceeded","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}
	
	rmc.queryById("Account",account_id,function(err, account){
		if(err){
			logger.error({"r":"subm","msg":"rmc_queryById_err","p":req.body,"err":err});
			return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}
		if(!account){
			logger.error({"r":"subm","msg":"account_not_found","p":req.body});
			return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}
		var toDo = data.length;
		var done = 0;
		for(var i=0; i<toDo; i++){
			if(!isValidClientPacket(data[i])){
				done++;
				if(done == toDo){
					return sendSuccess(res,{});
				}
				continue;
			}
			saveLog(account_id, data[i], remaining, tim, function(err,resp){
				if(err){
					logger.error({"r":"subm","msg":"saveLogErr","p":req.body,"err":err});
					return sendError(res,"server_error","server_error");
				}
				done++;
				if(done == toDo){
					return sendSuccess(res,{});
				}
			});
		}
	});
});

function saveLog(aid, data, remaining, tim, cb){
	if(data.e == "c"){
		if(!("e" in data.d) && !("p" in data.d)){
			logger.warn({"r":"subm","msg":"saveLogEmailPhoneBothMisingSkip","p":data});
			return cb(null,true);
		}
		ContactLog.update({
			aid : aid,
			p   : data.d.p,
			e   : data.d.e
		},{
			$setOnInsert : {
				aid : aid,
				ut  : data.t,
				n   : data.d.n,
				p   : data.d.p,
				e   : data.d.e,
				l   : remaining,
				st  : tim
			}
		},{
			upsert : true
		},function(err,updated_record){
			if(err){
				logger.error({"r":"subm","msg":"saveContactLogSkip","p":data,"err":err});
				return cb(null,true);
			}
			// if(("nUpserted" in updated_record)  && updated_record.nUpserted == 0){
			// 	logger.warn({"r":"subm","msg":"saveContactLogSkip","p":data});
			// }
			return cb(null,true);
		});
	} else {
		var new_client_log = new ClientLog({
			aid : aid,
			ut  : data.t,
			e   : data.e,
			d   : data.d,
			l   : remaining,
			st  : tim
		});
		new_client_log.save(function(err,saved_log){
			if(err){
				return cb(err,null);
			}
			return cb(null,true);
		});
	}
}

function isValidClientPacket(packet){
	if(!("e" in packet) || !("d" in packet) || !("t" in packet)){
		logger.warn({"r":"subm","msg":"isValidClientPacketSkip","p":packet});
		return false;
	}
	if(isNaN(packet.t)){
		return false;
	}
	return true;
}

function checkIfArray(inputVar){
	if( Object.prototype.toString.call(inputVar) === '[object Array]' ) {
    return true;
	}
	return false;
}

module.exports = router;