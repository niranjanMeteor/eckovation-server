const express 									= require('express');
const mongoose 									= require('mongoose');
const jwt    							      = require('jsonwebtoken');

const constants									= require('../utility/constants.js');
const configs				  					= require('../utility/configs.js');
const errorCodes 								= require('../utility/errors.js');
const helpers										= require('../utility/helpers.js');
const PluginConfigs				  		= require('../plugins/configs.js');
const PluginServerConfig        = require('../pluginserver/configs.js');
const PluginActivation          = require('../plugins/plugin_activation.js');
const PluginTypesInfo           = require('../plugins/plugin_types.js');
const UserInfoCache   					= require('../utility/user_info_cache.js');
const GroupInfoCache       			= require('../utility/group_info_cache.js');
const log4jsLogger  						= require('../loggers/log4js_module.js');
const log4jsConfigs  			  		= require('../loggers/log4js_configs.js');

require('../models/accounts.js');
require('../models/profiles.js');
require('../models/groups.js');
require('../models/groupmembers.js');
require('../models/plugins.js');
require('../models/pluginconfigs.js');
require('../models/groupplugins.js');
require('../models/packages/grouppluginpackages.js');
const Account 									= mongoose.model('Account');
const Profile 									= mongoose.model('Profile'); 
const Group 				  					= mongoose.model('Group'); 
const GroupMember 							= mongoose.model('GroupMember');
const AppPlugin       					= mongoose.model('AppPlugin');
const PluginConfig       				= mongoose.model('PluginConfig');
const GroupPlugin       				= mongoose.model('GroupPlugin');
const GroupPluginPackage 				= mongoose.model('GroupPluginPackage');

const sendError 								= helpers.sendError;
const sendSuccess 							= helpers.sendSuccess;
const router 										= express.Router();
const logger        						= log4jsLogger.getLogger(log4jsConfigs.CATEGORY_ROUTES_GROUP_PLUGIN);
const VALID_PLUGIN_CATEGORY 		= [AppPlugin.CATEGORY.TEST,AppPlugin.CATEGORY.VIDEO];
const PLUGIN_STATUS_VALUES      = getGroupPluginStatusValues();
const PLUGIN_CATEGORY_VALUES    = getPluginCategoryValues();


router.use(function(req, res, next){
	var token = req.body.tokn;
	if(!token){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"token_not_found","p":req.body});
	  return sendError(res,"Access without token is not authorised","token_not_found",constants.HTTP_STATUS.FORBIDDEN);
	}
  jwt.verify(token, configs.JWT_ACCESS_TOKEN_PRIVATE_KEY, function(err, decoded) {      
    if(err){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"jwt_verify_error","p":req.body,"er":err});
      return sendError(res,"Failed to authenticate token.","token_auth_failure",constants.HTTP_STATUS.TOKEN_EXPIRED);  
    }
  	req.decoded = decoded;
    next();
  });
});

// create a brand new plugin
router.post('/cr',function(req, res, next){
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('nm',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('dsc',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('typ',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('t_url',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('s_url',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('p_url',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('pd',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('pkg',errorCodes.invalid_parameters[1]).optional();

	if(req.validationErrors()){
		logger.error({"r":"cr","msg":"invalid_parameters","p":req.body}); 
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var group_id 		    = req.body.g_id;
	var profile_id 		  = req.body.p_id;
	var plugin_name 		= req.body.nm ? req.body.nm.trim()  : "";
	var plugin_desc 		= req.body.dsc ? req.body.dsc.trim() : "";
	var plugin_type 		= req.body.typ ? parseInt(req.body.typ) : "";
	var thumb_url       = (req.body.t_url) ? req.body.t_url : PluginConfigs.DEFAULT_THUMB_URL;
	var showcase_url    = (req.body.s_url) ? req.body.s_url : PluginConfigs.DEFAULT_SHOWCASE_URL;
	var promo_url       = (req.body.p_url) ? req.body.p_url : PluginConfigs.DEFAULT_PROMO_URL;
	var isPaid      		= req.body.pd;
	var packages;
	
	if(plugin_name.length > PluginConfigs.PLUGIN_NAME_MAX_LENGTH){
		logger.error({"r":"cr","msg":"plugin_name_max_limit","p":req.body});
		return sendError(res,"Plugin name cannot be more than 25 characters","plugin_name_too_long",constants.HTTP_STATUS.SERVER_ERROR_CODE);
	}
	if(plugin_desc.length > PluginConfigs.PLUGIN_DESC_MAX_LENGTH){
		logger.error({"r":"cr","msg":"plugin_desc_max_limit","p":req.body});
		return sendError(res,"Plugin name cannot be more than 200 characters","plugin_desc_too_long",constants.HTTP_STATUS.SERVER_ERROR_CODE);
	}
	if(isNaN(plugin_type) || (VALID_PLUGIN_CATEGORY.indexOf(plugin_type) < 0)){
		logger.error({"r":"cr","msg":"plugin_type_not_valid","p":req.body});
		return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}
	if(isPaid){
		if(isPaid == true || isPaid == 'true'){
			isPaid = true;
		} else if(isPaid == false || isPaid == 'false'){
			isPaid = false;
		} else {
			logger.error({"r":"cr","msg":"invalid_value_for parameter_pd","p":req.body});
		  return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}
	}
	if(isPaid){
		validPackage = isValidPaidPackagesObject(req.body.pkg);
		if(validPackage.status == true){
			packages = validPackage.data;
		} else {
			logger.error({"r":"cr","msg":"invalid_package_object","p":req.body,er:validPackage.error});
	    return sendError(res,"Package details are not valid","invalid_package_fields",constants.HTTP_STATUS.SERVER_ERROR_CODE);
		}
	}
	UserInfoCache.getMongoProfile(profile_id,function(err, profile){
		if(err){ 
			logger.error({"r":"cr","msg":"profiles_server_error","p":req.body,"er":err});
			return sendError(res,"server_error","server_error");
		}
		if(!profile){
			logger.error({"r":"cr","msg":"profile_not_found","p":req.body});
			return sendError(res,"Profile not found.","profile_not_found",constants.HTTP_STATUS.BAD_REQUEST);
		}
		if(profile.aid != req.decoded.id){
			logger.error({"r":"cr","msg":"profile_aid_mismatch_with_token_id","p":req.body});
			return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}
		GroupMember.findOne({
			gid : group_id,
			pid : profile_id,
			act : true,
			gdel: false
		},function(err, groupmember){
			if(err){ 
				logger.error({"r":"cr","msg":"groupmember_server_error","p":req.body,"er":err});
				return sendError(res,"server_error","server_error");
			}
			if(!groupmember){
				logger.error({"r":"previous","msg":"not_a_group_member","p":req.body});
				return sendError(res,"You are not a member of this group.","not_a_member",constants.HTTP_STATUS.BAD_REQUEST);
			}
			if(groupmember.type != GroupMember.TYPE.ADMIN){
				logger.error({"r":"gadm_a","m":"PUT","msg":"not_a_group_admin","p":req.body});
				return sendError(res,"You are not Group Administrator","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
			}
			var api_key, home_url, host, port, api_path;
      if(plugin_type == 1){
        api_key        = PluginServerConfig.VIDEO_PLUGIN_API_KEY;
        home_url       = PluginConfigs.VIDEO_PLUGIN_HOME_URL;
        host           = PluginServerConfig.VIDEO_PLUGIN_SERVER_HOST;
        port           = PluginServerConfig.VIDEO_PLUGIN_SERVER_PORT;
        api_path       = PluginServerConfig.VIDEO_PLUGIN_SERVER_API_PATH;
      } else if(plugin_type == 2) {
        api_key        = PluginServerConfig.TEST_PLUGIN_API_KEY;
        home_url       = PluginConfigs.QUIZ_PLUGIN_HOME_URL;
        host           = PluginServerConfig.QUIZ_PLUGIN_SERVER_HOST;
        port           = PluginServerConfig.QUIZ_PLUGIN_SERVER_PORT;
        api_path       = PluginServerConfig.QUIZ_PLUGIN_SERVER_API_PATH;
      } 
      var new_plugin = new AppPlugin({
        name           : plugin_name,
        desc           : plugin_desc,
        catg           : plugin_type,
        thumb_url      : thumb_url,
        showcase_url   : showcase_url,
        home_url       : home_url,
        act            : true,
        aid            : profile.aid,
        pid            : profile_id,
        creatby        : profile.nam
      });
      new_plugin.save(function(err,created_plugin){
        if(err){
          logger.error({"r":"cr","msg":"New_Plugin_Save_error","p":req.body,"er":err});
				  return sendError(res,"server_error","server_error");
        }
        var new_plugin_config = new PluginConfig({
          plugin_id : created_plugin._id,
          host      : host,
          port      : port,
          api_path  : api_path,
          api_key   : api_key,
          req_exp   : 500
        });
        new_plugin_config.save(function(err,created_plugin_config){
          if(err){
            logger.error({"r":"cr","msg":"New_PluginConfig_Save_error","p":req.body,"er":err});
				    return sendError(res,"server_error","server_error");
          }
          var group_plugin_status = GroupPlugin.STATUS.ACTIVE;
          var req_subscription    = false;
          if(isPaid){
          	group_plugin_status = GroupPlugin.STATUS.PENDING_APPROVAL;
          	req_subscription    = true;
          }
          var new_group_plugin = new GroupPlugin({
          	gid              : group_id,
          	act              : true,
          	plgnid           : created_plugin._id,
          	type             : GroupPlugin.TYPE.PLUGIN_PACKAGE,
          	status           : group_plugin_status,
          	req_subscription : req_subscription,
          	promo_url        : [promo_url]
          });
          new_group_plugin.save(function(err,created_plugin_config){
	          if(err){
	            logger.error({"r":"cr","msg":"New_Group_Plugin_Save_error","p":req.body,"er":err});
					    return sendError(res,"server_error","server_error");
	          }
	          Group.update({
				      _id : group_id
				    },{
				      plgn : true
				    },function(err,updated_group){
				    	if(err){
	          		logger.error({"r":"cr","msg":"Group_Update_error","p":req.body,"er":err});
					      return sendError(res,"server_error","server_error");
	          	}
		          if(!isPaid){
		          	PluginActivation.activate(group_id, created_plugin._id, function(err,resp){
						    	if(err){
						    		logger.error({"r":"ad","msg":"PluginActivation_activate_error","p":req.body,"er":err});
										return sendError(res,"server_error","server_error");
						    	}
			          	getPluginDetails(group_id, created_plugin._id, function(err,data){
			          		if(err){
				          		logger.error({"r":"cr","msg":"getPluginDetails_Unpaid_error","p":req.body,"er":err});
								      return sendError(res,"server_error","server_error");
				          	}
				          	logger.info({"r":"cr","msg":"getPluginDetails_Success_Unpaid","p":req.body,"resp":data});
			          		return sendSuccess(res,data);
			          	});
			          });
		          } else {
			          createPluginPackages(group_id, created_plugin._id, packages, function(err,resp){
			          	if(err){
			          		logger.error({"r":"cr","msg":"createPluginPackages_error","p":req.body,"er":err});
							      return sendError(res,"server_error","server_error");
			          	}
			          	getPluginDetails(group_id, created_plugin._id, function(err,data){
			          		if(err){
				          		logger.error({"r":"cr","msg":"getPluginDetails_Paid_error","p":req.body,"er":err});
								      return sendError(res,"server_error","server_error");
				          	}
				          	logger.info({"r":"cr","msg":"getPluginDetails_Success_Paid","p":req.body,"resp":data});
			          		return sendSuccess(res,data);
			          	});
			          });
			        }
			      });
	        });
        });
      });
		});
	});
});

// fetch the list of all plugins in the group
router.post('/rd',function(req, res, next){
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();

	if(req.validationErrors()){
		logger.error({"r":"rd","msg":"invalid_parameters","p":req.body}); 
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var group_id 		    = req.body.g_id;
	var profile_id 		  = req.body.p_id;

	UserInfoCache.getMongoProfile(profile_id,function(err, profile){
		if(err){ 
			logger.error({"r":"rd","msg":"profiles_server_error","p":req.body,"er":err});
			return sendError(res,"server_error","server_error");
		}
		if(!profile){
			logger.error({"r":"rd","msg":"profile_not_found","p":req.body});
			return sendError(res,"Profile not found.","profile_not_found",constants.HTTP_STATUS.BAD_REQUEST);
		}
		if(profile.aid != req.decoded.id){
			logger.error({"r":"rd","msg":"profile_aid_mismatch_with_token_id","p":req.body});
			return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}
		UserInfoCache.getMongoGrpMem(profile_id, group_id, function(err, groupmember){
			if(err){ 
				logger.error({"r":"rd","msg":"profiles_server_error","p":req.body,"er":err});
				return sendError(res,"server_error","server_error");
			}
			if(!groupmember){
				logger.error({"r":"rd","msg":"not_a_group_member","p":req.body});
				return sendError(res,"You are not a member of this group.","not_a_member",constants.HTTP_STATUS.BAD_REQUEST);
			}
			if(groupmember.type != GroupMember.TYPE.ADMIN){
				logger.error({"r":"rd","msg":"not_a_group_admin","p":req.body});
				return sendError(res,"You are not Group Administrator","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
			}
			Group.findOne({
		    _id : group_id,
		    act : true
		  },function(err,group){
		    if(err){ 
					logger.error({"r":"rd","msg":"group_find_server_error","p":req.body,"er":err});
					return sendError(res,"server_error","server_error");
				}
		    if(!group) { 
		      logger.error({"r":"rd","msg":"group_not_found","p":req.body});
					return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		    }
		    GroupPlugin.find({
		    	gid    : group_id,
		    	act    : true,
		    	type   : GroupPlugin.TYPE.PLUGIN_PACKAGE
		    },{
		    	_id       : 0,
		    	plgnid    : 1,
		    	type      : 1,
		    	status    : 1,
		    	req_subscription : 1
		    },function(err,groupplugins){
		    	if(err){
		    		logger.error({"r":"rd","msg":"GroupPlugin_error","p":req.body,"er":err});
						return sendError(res,"server_error","server_error");
		    	}
		    	if(groupplugins.length == 0){
		    		logger.error({"r":"g_pl","msg":"no_plugin_is_active_in_group","p":req.body});
			      return res.status(constants.HTTP_STATUS.NO_PLUGINS_ACTIVE_IN_GROUP).json({
			      									success : true,
			      									data	  : {
			      										pl_typ : PluginTypesInfo
			      									}
			      			 });
		    	}
		    	var plugin_ids = [];
		    	var grouppluginByPluginId = {};
		    	for(var i=0; i<groupplugins.length; i++){
		    		if(plugin_ids.indexOf(groupplugins[i].plgnid+'') < 0){
		    			plugin_ids.push(groupplugins[i].plgnid+'');
		    		}
		    		grouppluginByPluginId[groupplugins[i].plgnid+''] = groupplugins[i];
		    	}
		    	AppPlugin.find({
						_id : { $in : plugin_ids }
					},{
						name         : 1,
						desc         : 1,
						thumb_url    : 1,
						showcase_url : 1,
						catg         : 1,
						createdAt    : 1
					},{
						sort:{
							updatedAt:'asc'
						}
					},function(err,plugins){
						if(err){
			    		logger.error({"r":"rd","msg":"GroupPlugin_error","p":req.body,"er":err});
							return sendError(res,"server_error","server_error");
			    	}
			    	var temp;
			    	var finalData = [];
			    	for(var i=0; i < plugins.length; i++){
			    		temp                   = plugins[i].toJSON();
			    		if(!temp.catg){
			    			temp.catg            = -1;
			    			temp.catg_v          = "RANDOM";
			    		} else {
			    			temp.catg_v          = PLUGIN_CATEGORY_VALUES[temp.catg];
			    		}
			    		finalData[i]           = temp;
			    		if(!grouppluginByPluginId[temp._id].status){
			    			finalData[i].pstatus = GroupPlugin.STATUS.ACTIVE;
			    		} else {
			    			finalData[i].pstatus   = grouppluginByPluginId[temp._id].status;
			    		}
			    		finalData[i].pstatus_v = PLUGIN_STATUS_VALUES[finalData[i].pstatus];
			    		finalData[i].type      = grouppluginByPluginId[temp._id].type;
			    		finalData[i].isp       = grouppluginByPluginId[temp._id].req_subscription;
			    	}
			    	// logger.info({"r":"rd","msg":"plugins_final_data","data":finalData});
			    	return sendSuccess(res,{plugins:finalData,pl_typ:PluginTypesInfo});
					});
		    });
		  });
		});
	});
});

//middleware captures some parameters that will be required for the router below it
router.use(function(req, res, next){
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('pl_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();

	if(req.validationErrors()){
		logger.error({"r":"actv","msg":"invalid_parameters","p":req.body}); 
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var group_id 		    = req.body.g_id;
	var profile_id 		  = req.body.p_id;
	var plugin_id       = req.body.pl_id;

	UserInfoCache.getMongoProfile(profile_id,function(err, profile){
		if(err){ 
			logger.error({"r":"actv","msg":"profiles_server_error","p":req.body,"er":err});
			return sendError(res,"server_error","server_error");
		}
		if(!profile){
			logger.error({"r":"actv","msg":"profile_not_found","p":req.body});
			return sendError(res,"Profile not found.","profile_not_found",constants.HTTP_STATUS.BAD_REQUEST);
		}
		if(profile.aid != req.decoded.id){
			logger.error({"r":"actv","msg":"profile_aid_mismatch_with_token_id","p":req.body});
			return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}
		Group.findOne({
	    _id : group_id,
	    act : true
	  },function(err,group){
	    if(err){ 
				logger.error({"r":"actv","msg":"group_find_server_error","p":req.body,"er":err});
				return sendError(res,"server_error","server_error");
			}
	    if(!group) { 
	      logger.error({"r":"actv","msg":"group_not_found","p":req.body});
				return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	    }
			AppPlugin.findOne({
		    _id : plugin_id,
		    act : true 
		  },function(err, plugin){
		    if(err){ 
					logger.error({"r":"actv","msg":"plugin_find_server_error","p":req.body,"er":err});
					return sendError(res,"server_error","server_error");
				}
		    if(!plugin) { 
		      logger.error({"r":"actv","msg":"plugin_not_found","p":req.body});
				  return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		    }
				GroupMember.findOne({
					gid : group_id,
					pid : profile_id,
					act : true,
					gdel: false
				},function(err, groupmember){
					if(err){ 
						logger.error({"r":"actv","msg":"groupmember_server_error","p":req.body,"er":err});
						return sendError(res,"server_error","server_error");
					}
					if(!groupmember){
						logger.error({"r":"actv","msg":"not_a_group_member","p":req.body});
						return sendError(res,"You are not a member of this group.","not_a_member",constants.HTTP_STATUS.BAD_REQUEST);
					}
					if(groupmember.type != GroupMember.TYPE.ADMIN){
						logger.error({"r":"actv","msg":"not_a_group_admin","p":req.body});
						return sendError(res,"You are not Group Administrator","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
					}
					req.group   = group;
					next();
				});
			});
		});
	});
});

// fetch details about a particular plugin
router.post('/dtl',function(req, res, next){
	
	var group_id 		    = req.body.g_id;
	var plugin_id       = req.body.pl_id;
	var group           = req.group;

	GroupPlugin.findOne({
  	gid    : group_id,
  	plgnid : plugin_id,
  	act    : true
  },{
  	_id              : 0,
  	plgnid           : 1,
  	type             : 1,
  	status           : 1,
  	req_subscription : 1
  },function(err,groupplugin){
  	if(err){
  		logger.error({"r":"dtl","msg":"GroupPlugin_error","p":req.body,"er":err});
			return sendError(res,"server_error","server_error");
  	}
  	if(!groupplugin){
  		logger.error({"r":"dtl","msg":"GroupPlugin_not_found","p":req.body});
  		return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
  	}
  	AppPlugin.findOne({
			_id : plugin_id
		},{
			name         : 1,
			desc         : 1,
			thumb_url    : 1,
			showcase_url : 1,
			catg         : 1,
			createdAt    : 1
		},function(err,appplugin){
			if(err){
    		logger.error({"r":"dtl","msg":"GroupPlugin_error","p":req.body,"er":err});
				return sendError(res,"server_error","server_error");
    	}
  		appplugin            = appplugin.toJSON();
  		if(!appplugin.catg){
				appplugin.catg            = -1;
				appplugin.catg_v          = "RANDOM";
			} else {
				appplugin.catg_v          = PLUGIN_CATEGORY_VALUES[appplugin.catg];
			}
			if(!groupplugin.status){
				appplugin.pstatus   = GroupPlugin.STATUS.ACTIVE;
			} else {
				appplugin.pstatus   = groupplugin.status;
			}
  		appplugin.pstatus_v    = PLUGIN_STATUS_VALUES[appplugin.pstatus];
  		appplugin.type         = groupplugin.type;
  		appplugin.isp          = groupplugin.req_subscription ? groupplugin.req_subscription : false;
  		if(!appplugin.isp){
    		return sendSuccess(res,{plugin_data:appplugin});
  		}
    	GroupPluginPackage.find({
				gid       : group_id,
				plugin_id : plugin_id,
				act       : true
			},{
				package_name     : 1,
				package_desc     : 1,
				package_price    : 1,
				package_duration : 1,
				duration_unit    : 1,
				duration_base    : 1,
				acc              : 1
			},function(err,plugin_packages){
				if(err){ 
					logger.error({"r":"dtl","p":req.body,"er":err,"msg":"GroupPluginPackage_FindError"});
					return sendError(res,"server_error","server_error");
				}
				// logger.info({"r":"dtl","msg":"plugin detail","data":{pl:appplugin,pkgs:plugin_packages}});
				return sendSuccess(res,{plugin_data:appplugin,pkgs:plugin_packages});
			});
    });
  });
});

//add a single or multiple packages in a paid plugin in a group
router.post('/apkg',function(req, res, next){

	var group_id 		    = req.body.g_id;
	var plugin_id       = req.body.pl_id;
	var group           = req.group;
	var packages;

	if(!req.body.pkg){
		logger.error({"r":"apkg","msg":"Package_Field_Missing","p":req.body});
  	return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	validPackage = isValidPaidPackagesObject(req.body.pkg);
	if(validPackage.status == true){
		packages = validPackage.data;
	} else {
		logger.error({"r":"apkg","msg":"invalid_package_object","p":req.body,er:validPackage.error});
	  return sendError(res,"Package details are not valid","invalid_package_fields",constants.HTTP_STATUS.SERVER_ERROR_CODE);
	}

	GroupPlugin.findOne({
  	gid    : group_id,
  	plgnid : plugin_id,
  	act    : true
  },{
  	_id              : 0,
  	plgnid           : 1,
  	type             : 1,
  	status           : 1,
  	req_subscription : 1
  },function(err,groupplugin){
  	if(err){
  		logger.error({"r":"apkg","msg":"GroupPlugin_error","p":req.body,"er":err});
			return sendError(res,"server_error","server_error");
  	}
  	if(!groupplugin){
  		logger.error({"r":"apkg","msg":"GroupPlugin_not_found","p":req.body});
  		return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
  	}
  	if(!groupplugin.req_subscription || groupplugin.req_subscription == false){
  		logger.error({"r":"apkg","msg":"GroupPlugin_found_is_not_paid","p":req.body});
  		return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
  	}
  	if(!groupplugin.status || groupplugin.status != GroupPlugin.STATUS.PENDING_APPROVAL){
      logger.error({"r":"actv","msg":"cannot_modify_package_if_approved","p":req.body});
  		return sendError(res,"cannot_modify_package_if_approved","cannot_modify_package_if_approved",constants.HTTP_STATUS.SERVER_ERROR_CODE);
    }
  	createPluginPackages(group_id, plugin_id, packages, function(err,resp){
    	if(err){
    		logger.error({"r":"apkg","msg":"createPluginPackages_error","p":req.body,"er":err});
	      return sendError(res,"server_error","server_error");
    	}
    	getPluginDetails(group_id, plugin_id, function(err,data){
    		if(err){
      		logger.error({"r":"apkg","msg":"getPluginDetails_Paid_error","p":req.body,"er":err});
		      return sendError(res,"server_error","server_error");
      	}
    		return sendSuccess(res,data);
    	});
    });
  });
});

// delete a package in a paid plugin in a group
router.post('/dpkg',function(req, res, next){
	var group_id 		    = req.body.g_id;
	var plugin_id       = req.body.pl_id;
	var group           = req.group;
	var pkg_id          = req.body.pkg_id;

	GroupPlugin.findOne({
  	gid    : group_id,
  	plgnid : plugin_id,
  	act    : true
  },{
  	_id              : 0,
  	plgnid           : 1,
  	type             : 1,
  	status           : 1,
  	req_subscription : 1
  },function(err,groupplugin){
  	if(err){
  		logger.error({"r":"dtl","msg":"GroupPlugin_error","p":req.body,"er":err});
			return sendError(res,"server_error","server_error");
  	}
  	if(!groupplugin){
  		logger.error({"r":"dtl","msg":"GroupPlugin_not_found","p":req.body});
  		return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
  	}
  	if(!groupplugin.req_subscription || groupplugin.req_subscription == false){
  		logger.error({"r":"dtl","msg":"GroupPlugin_found_is_not_paid","p":req.body});
  		return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
  	}
  	if(!groupplugin.status || groupplugin.status != GroupPlugin.STATUS.PENDING_APPROVAL){
      logger.error({"r":"actv","msg":"cannot_modify_package_if_approved","p":req.body});
  		return sendError(res,"cannot_modify_package_if_approved","cannot_modify_package_if_approved",constants.HTTP_STATUS.SERVER_ERROR_CODE);
    }
  	GroupPluginPackage.findOne({
  		_id       : pkg_id,
	  	gid       : group_id,
	  	plugin_id : plugin_id
	  },{
	  	act           : 1
	  },function(err,grouppluginpackage){
	  	if(err){
	  		logger.error({"r":"dtl","msg":"GroupPlugin_error","p":req.body,"er":err});
				return sendError(res,"server_error","server_error");
	  	}
	  	if(!grouppluginpackage){
	  		logger.error({"r":"dtl","msg":"GroupPlugin_not_found","p":req.body});
	  		return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	  	}
	  	GroupPluginPackage.update({
	  		_id : pkg_id
	  	},{
	  		act : false
	  	},function(err,updated_package){
	  		if(err){
		  		logger.error({"r":"dtl","msg":"GroupPluginPackage_error","p":req.body,"er":err});
					return sendError(res,"server_error","server_error");
		  	}
		  	getPluginDetails(group_id, plugin_id, function(err,data){
	    		if(err){
	      		logger.error({"r":"dpkg","msg":"getPluginDetails_Paid_error","p":req.body,"er":err});
			      return sendError(res,"server_error","server_error");
	      	}
	    		return sendSuccess(res,data);
	    	});
	  	});
	  });
  });
});

// edit the details about a particular plugin
router.post('/ed',function(req, res, next){
	req.checkBody('nm',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('dsc',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('t_url',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('s_url',errorCodes.invalid_parameters[1]).optional();

	var group_id 		    = req.body.g_id;
	var plugin_id       = req.body.pl_id;
	var toEdit          = {};

	if(req.body.nm){
		if(req.body.nm.length > PluginConfigs.PLUGIN_NAME_MAX_LENGTH){
			logger.error({"r":"ed","msg":"plugin_name_max_limit","p":req.body});
			return sendError(res,"Plugin name cannot be more than 25 characters","plugin_name_too_long",constants.HTTP_STATUS.SERVER_ERROR_CODE);
		}
		toEdit.name = req.body.nm.trim();
	}
	if(req.body.desc){
		if(req.body.desc.length > PluginConfigs.PLUGIN_DESC_MAX_LENGTH){
			logger.error({"r":"ed","msg":"plugin_desc_max_limit","p":req.body});
			return sendError(res,"Plugin name cannot be more than 200 characters","plugin_desc_too_long",constants.HTTP_STATUS.SERVER_ERROR_CODE);
		}
		toEdit.desc = req.body.desc.trim();
	}
	if(req.body.t_url){
		toEdit.thumb_url = req.body.t_url.trim();
	}
	if(req.body.s_url){
		toEdit.showcase_url = req.body.s_url.trim();
	}
	if(!toEdit || Object.keys(toEdit).length == 0){
		logger.error({"r":"ed","msg":"no_fields_to_edit","p":req.body});
		return sendError(res,"No fields found to edit for this plugin","no_data_to_edit_plugin",constants.HTTP_STATUS.SERVER_ERROR_CODE);
	}

	GroupPlugin.findOne({
  	gid    : group_id,
  	plgnid : plugin_id,
  	act    : true
  },function(err,groupplugin){
  	if(err){
  		logger.error({"r":"ed","msg":"GroupPlugin_error","p":req.body,"er":err});
			return sendError(res,"server_error","server_error");
  	}
  	if(!groupplugin){
  		logger.error({"r":"ed","msg":"GroupPlugin_not_found","p":req.body});
  		return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
  	}
  	AppPlugin.update({
  		_id : plugin_id
  	},toEdit,function(err,plugin_updated){
  		if(err){
	  		logger.error({"r":"ed","msg":"AppPlugin_Update_error","p":req.body,"er":err});
				return sendError(res,"server_error","server_error");
	  	}
	  	AppPlugin.findOne({
				_id : plugin_id
			},{
				name         : 1,
				desc         : 1,
				thumb_url    : 1,
				showcase_url : 1,
				catg         : 1,
				createdAt    : 1
			},function(err,appplugin){
				if(err){
	    		logger.error({"r":"dtl","msg":"AppPlugin_Find_error","p":req.body,"er":err});
					return sendError(res,"server_error","server_error");
	    	}
	    	appplugin            = appplugin.toJSON();
	  		if(!appplugin.catg){
					appplugin.catg            = -1;
					appplugin.catg_v          = "RANDOM";
				} else {
					appplugin.catg_v          = PLUGIN_CATEGORY_VALUES[appplugin.catg];
				}
				if(!groupplugin.status){
					appplugin.pstatus   = GroupPlugin.STATUS.ACTIVE;
				} else {
					appplugin.pstatus   = groupplugin.status;
				}
	  		appplugin.pstatus_v    = PLUGIN_STATUS_VALUES[appplugin.pstatus];
	  		appplugin.type         = groupplugin.type;
	  		appplugin.isp          = groupplugin.req_subscription ? groupplugin.req_subscription : false;
	  		return sendSuccess(res,{plugin_data:appplugin});
	  	});
  	});
  });
});

// activate a particular plugin in a group
router.post('/actv',function(req, res, next){
	
	var group_id 		    = req.body.g_id;
	var plugin_id       = req.body.pl_id;

	GroupPlugin.findOne({
    gid    : group_id,
    plgnid : plugin_id,
    act    : true
  },function(err,groupplugin){
    if(err){ 
			logger.error({"r":"actv","msg":"GroupPlugin_FindOne_error","p":req.body,"er":err});
			return sendError(res,"server_error","server_error");
		}
    if(!groupplugin){
      logger.error({"r":"actv","msg":"plugin_not_found","p":req.body});
  		return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
    }
    if(groupplugin.req_subscription || groupplugin.req_subscription == true){
    	if(!groupplugin.status || groupplugin.status != GroupPlugin.STATUS.APPROVED){
	  		logger.error({"r":"actv","msg":"GroupPlugin_found_is_paid_NOT_APPROVED","p":req.body,"gp":groupplugin});
	  		return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
    	}
  	}
    if(groupplugin.status && groupplugin.status == GroupPlugin.STATUS.ACTIVE){
      logger.error({"r":"actv","msg":"plugin_is_already_active_in_group","p":req.body});
  		return sendError(res,"Plugin is already Active in the group","plugin_already_active_in_group",constants.HTTP_STATUS.PLUGIN_ALREADY_ACTIVE);
    }
    PluginActivation.activate(group_id, plugin_id, function(err,resp){
    	if(err){
    		logger.error({"r":"actv","msg":"PluginActivation_activate_error","p":req.body,"er":err});
				return sendError(res,"server_error","server_error");
    	}
    	getPluginDetails(group_id, plugin_id, function(err,data){
    		if(err){
      		logger.error({"r":"actv","msg":"getPluginDetails_Paid_error","p":req.body,"er":err});
		      return sendError(res,"server_error","server_error");
      	}
      	logger.info({"r":"actv","msg":"getPluginDetails_Success","p":req.body,"dt":data});
    		return sendSuccess(res,data);
    	});
    });
  });		
});

// deactivate a particular plugin in a group
router.post('/dactv',function(req, res, next){

	var group_id 		    = req.body.g_id;
	var plugin_id       = req.body.pl_id;
	
	GroupPlugin.findOne({
    gid    : group_id,
    plgnid : plugin_id,
    act    : true
  },function(err,groupplugin){
    if(err){ 
			logger.error({"r":"dactv","msg":"GroupPlugin_Finderror","p":req.body,"er":err});
			return sendError(res,"server_error","server_error");
		}
    if(!groupplugin){
      logger.error({"r":"dactv","msg":"plugin_not_found","p":req.body});
  		return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
    }
    if(groupplugin.req_subscription || groupplugin.req_subscription == true){
  		logger.error({"r":"dactv","msg":"GroupPlugin_found_is_paid","p":req.body});
  		return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
  	}
    if(groupplugin.status && groupplugin.status == GroupPlugin.STATUS.INACTIVE){
      logger.error({"r":"dactv","msg":"plugin_is_already_deactive_in_group","p":req.body});
  		return sendError(res,"Plugin is already Deactive in the group","plugin_already_deactive_in_group",constants.HTTP_STATUS.PLUGIN_ALREADY_DEACTIVE);
    }
    PluginActivation.deactivate(group_id, plugin_id, function(err,resp){
    	if(err){
    		logger.error({"r":"cr","msg":"PluginActivation_deactivate_error","p":req.body,"er":err});
				return sendError(res,"server_error","server_error");
    	}
    	getPluginDetails(group_id, plugin_id, function(err,data){
    		if(err){
      		logger.error({"r":"dpkg","msg":"getPluginDetails_Paid_error","p":req.body,"er":err});
		      return sendError(res,"server_error","server_error");
      	}
      	logger.info({"r":"dactv","msg":"getPluginDetails_Success","p":req.body,"dt":data});
    		return sendSuccess(res,data);
    	});
    });
  });				
});

// delete a plugin in a group
router.post('/dl',function(req, res, next){
	var group_id 		    = req.body.g_id;
	var plugin_id       = req.body.pl_id;

	GroupPlugin.findOne({
  	gid    : group_id,
  	plgnid : plugin_id
  },function(err,groupplugin){
  	if(err){
  		logger.error({"r":"dl","msg":"GroupPlugin_error","p":req.body,"er":err});
			return sendError(res,"server_error","server_error");
  	}
  	if(!groupplugin){
  		logger.error({"r":"dl","msg":"GroupPlugin_not_found","p":req.body});
  		return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
  	}
  	if(!groupplugin.act || groupplugin.act == false){
  		logger.error({"r":"dl","msg":"GroupPlugin_found_is_act_false","p":req.body});
  		return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
  	}
  	if(groupplugin.req_subscription && groupplugin.req_subscription == true){
  		logger.error({"r":"dl","msg":"GroupPlugin_found_is_paid","p":req.body});
  		return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
  	}
  	if(groupplugin.status && groupplugin.status == GroupPlugin.STATUS.ACTIVE){
  		logger.error({"r":"dl","msg":"active_plugin_cannot_be_deleted","p":req.body});
  		return sendError(res,"active_plugin_cannot_be_deleted","active_plugin_cannot_be_deleted",constants.HTTP_STATUS.SERVER_ERROR_CODE);
  	}
  	PluginActivation.deletePlugin(group_id, plugin_id, function(err,resp){
    	if(err){
    		logger.error({"r":"dl","msg":"PluginActivation_deletePlugin_error","p":req.body,"er":err});
				return sendError(res,"server_error","server_error");
    	}
    	logger.info({"r":"dl","msg":"PluginActivation_deletePlugin_Success","p":req.body,"gp":groupplugin});
    	return sendSuccess(res,{});
    });
  });
});

function createPluginPackages(group_id, plugin_id, packages, cb){
	var toDo = packages.length;
	var done = 0;
	for(var i=0; i<toDo; i++){
		savePlugin(group_id, plugin_id, packages[i], function(err,resp){
			if(err){
				return cb(err,null);
			}
			done++;
			if(done == toDo){
				return cb(null,true);
			}
		});
	}
}

function savePlugin(group_id, plugin_id, pl_package, cb){
	var new_plugin_package = new GroupPluginPackage({
		gid             : group_id,
		plugin_id       : plugin_id,
		act             : true,
		package_name    : pl_package.n,
		package_desc    : pl_package.d,
		package_price   : pl_package.p,
		acc             : "1",
		duration_unit   : pl_package.u,
		duration_base   : pl_package.b,
		package_duration: pl_package.d
	});
	new_plugin_package.save(function(err,created_doc){
		if(err){
			return cb(err,null);
		}
		return cb(null,true);
	});
}

function getPluginDetails(group_id, plugin_id, cb){
	GroupPlugin.findOne({
  	gid    : group_id,
  	plgnid : plugin_id,
  	act    : true
  },{
  	_id              : 0,
  	plgnid           : 1,
  	type             : 1,
  	status           : 1,
  	req_subscription : 1
  },function(err,groupplugin){
  	if(err){
  		return cb(err,null);
  	}
  	if(!groupplugin){
  		return cb("getPluginDetails_:_GroupPlugin_not_found",null);
  	}
  	AppPlugin.findOne({
			_id : plugin_id
		},{
			name         : 1,
			desc         : 1,
			thumb_url    : 1,
			showcase_url : 1,
			catg         : 1,
			createdAt    : 1
		},function(err,appplugin){
			if(err){
    		return cb(err,null);
    	}
  		appplugin            = appplugin.toJSON();
  		if(!appplugin.catg){
				appplugin.catg            = -1;
				appplugin.catg_v          = "RANDOM";
			} else {
				appplugin.catg_v          = PLUGIN_CATEGORY_VALUES[appplugin.catg];
			}
			if(!groupplugin.status){
				appplugin.pstatus   = GroupPlugin.STATUS.ACTIVE;
			} else {
				appplugin.pstatus   = groupplugin.status;
			}
  		appplugin.pstatus_v    = PLUGIN_STATUS_VALUES[appplugin.pstatus];
  		appplugin.type         = groupplugin.type;
  		appplugin.isp          = groupplugin.req_subscription ? groupplugin.req_subscription : false;
  		if(!appplugin.isp){
    		return cb(null,{plugin_data:appplugin});
  		}
    	GroupPluginPackage.find({
				gid       : group_id,
				plugin_id : plugin_id,
				act       : true
			},{
				package_name     : 1,
				package_desc     : 1,
				package_price    : 1,
				package_duration : 1,
				duration_unit    : 1,
				duration_base    : 1,
				acc              : 1
			},function(err,plugin_packages){
				if(err){ 
					return cb(err,null);
				}
				return cb(null,{plugin_data:appplugin,pkgs:plugin_packages});
			});
    });
  });
}

function isValidPaidPackagesObject(package_object){
	var data = [];
	if(!package_object || typeof(package_object) != 'object'){
		return {status:false, error:"package_object_is_not_object_:_type_=_"+typeof(package_object)};
	}
	if(package_object.length == 0){
		return {status:false, error:"package_object_array_length_0"};
	}
	var temp, base, duration;
	for(var i=0; i < package_object.length; i++){
		var temp_new = {};
		var unit;
		temp = package_object[i];
		console.log(temp);
		if(!("n" in temp)){
			return {status:false, error:"package_object_"+i+"_name_missing"};
		}
		if(!("p" in temp)){
			return {status:false, error:"package_object_"+i+"_price_missing"};
		}
		if(!("u" in temp) || isNaN(temp["u"])){
			return {status:false, error:"package_object_"+i+"_unit_missing"};
		}
		unit = parseInt(temp["u"]);
		if(!("b" in temp)){
			return {status:false, error:"package_object_"+i+"_base_missing"};
		}
		base = temp["b"].toLowerCase();
		console.log('base is '+base);
		if(base == 'd'){
			if(temp["u"] < 0 && temp["u"] > 31){
				return {status:false, error:"package_object_"+i+"_base_days_invalid"};
			}
			duration = unit*24*3600*1000;
			if(temp["u"] == 1){
				base = "Day"
			} else base = "Days";
		} else if(base == 'w'){
			if(temp["u"] < 0 || temp["u"] > 12){
				return {status:false, error:"package_object_"+i+"_base_week_invalid"};
			}
			duration = unit*7*24*3600*1000;
			if(temp["u"] == 1){
				base = "Week"
			} else base = "Weeks";
		} else if(base == 'm'){
			if(temp["u"] < 0 || temp["u"] > 12){
				return {status:false, error:"package_object_"+i+"_base_month_invalid"};
			}
			duration = unit*30*24*3600*1000;
			if(temp["u"] == 1){
				base = "Month"
			} else base = "Months";
		} else {
			return {status:false, error:"package_object_"+i+"_base_not_matching"};
		}
		data[i] = {
			n : temp["n"].trim(),
			p : (parseInt(temp["p"])).toFixed(2),
			u : unit,
			b : base,
			d : duration
		};
	}
	return {status:true,data:data};
}

function getGroupPluginStatusValues(){
	var values = {};
	values[AppPlugin.STATUS.PENDING_APPROVAL] = AppPlugin.STATUS_VALUES.PENDING_APPROVAL;
	values[AppPlugin.STATUS.APPROVED]         = AppPlugin.STATUS_VALUES.APPROVED;
	values[AppPlugin.STATUS.REJECTED]         = AppPlugin.STATUS_VALUES.REJECTED;
	values[AppPlugin.STATUS.ACTIVE]           = AppPlugin.STATUS_VALUES.ACTIVE;
	values[AppPlugin.STATUS.INACTIVE]         = AppPlugin.STATUS_VALUES.INACTIVE;
	return values;
}

function getPluginCategoryValues(){
	var values = {};
	values[AppPlugin.CATEGORY.VIDEO]        = AppPlugin.CATEGORY_NAME.VIDEO;
	values[AppPlugin.CATEGORY.TEST]         = AppPlugin.CATEGORY_NAME.TEST;
	return values;
}

module.exports = router;