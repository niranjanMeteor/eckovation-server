var express 							= require('express');
var mongoose 							= require('mongoose');

var constants							= require('../utility/constants.js');
var configs				  			= require('../utility/configs.js');
var errorCodes 						= require('../utility/errors.js');
var helpers								= require('../utility/helpers.js');
var GroupNotify 					= require('../utility/grp_notify.js');
var log4jsLogger  				= require('../loggers/log4js_module.js');
var log4jsConfigs  			  = require('../loggers/log4js_configs.js');
// var Security              = require('../plugins/groupfeed/security.js');
var GroupFeedConfigs      = require('../plugins/groupfeed/configs.js');
// var AwsFileValidations    = require('../aws/file_validations.js');
// var AwsS3Module           = require('../aws/aws_s3.js');

require('../models/groups.js');
require('../models/groupmembers.js');
require('../models/messages.js');
require('../models/pluginclients.js');

var Group 				  = mongoose.model('Group'); 
var GroupMember 		= mongoose.model('GroupMember');
var Message 				= mongoose.model('Message');
var PluginClient    = mongoose.model('PluginClient');

var sendError 			= helpers.sendError;
var sendSuccess 		= helpers.sendSuccess;
var router 					= express.Router();
var logger        	= log4jsLogger.getLogger(log4jsConfigs.CATEGORY_ROUTES_GROUP_FEED);

//route middleware to check brute force into this plugin resource
router.use(function(req, res, next){
	if(!("client_id" in req.body) || !("client_secret" in req.body)){
		logger.error({"r":req.path,"msg":"client_id or client_secret missing in params","p":req.body});
		return sendError(res,"authentication_error","authentication_error",constants.HTTP_STATUS.UNAUTHORIZED);
	}
	// BruteForce.check(aid, tim, function(err,bruteForce){
	// 	if(err){
	// 		bruteForceAcclogger.error({"r":"account_brute_force_DbError","p":{aid:aid},"err":err});
	// 		return sendError(res,err,"server_error"); 
	// 	}
	// 	if(bruteForce){
	// 		bruteForceAcclogger.error({"r":"account_brute_force_found","p":{aid:aid,lmt:bruteForce.lmt,tm:bruteForce.tm}});
	// 		return sendError(res,"Too many requests","too_many_requests",constants.HTTP_STATUS.TOO_MANY_REQUESTS);
	// 	}
	// 	next();
	// });
	next();
});

//route middleware to verify credentials
router.use(function(req, res, next){
	var tim           = new Date().getTime();
	var client_id     = req.body.client_id;
	var client_secret = req.body.client_secret;
	PluginClient.findOne({
		// plugin_id : GroupFeedConfigs.PLUGIN_ID,
		client_id : client_id,
		client_sec: client_secret,
	},function(err,plugin_client){
		if(err){
			logger.error({"r":req.path,"msg":"PluginClient_find_server_error","p":req.body,"er":err});
			return sendError(res,"server_error","server_error",constants.HTTP_STATUS.SERVER_ERROR);
		}
		if(!plugin_client){
			logger.error({"r":req.path,"msg":"plugin_client_not_found","p":req.body});
			return sendError(res,"authentication_error","authentication_error",constants.HTTP_STATUS.UNAUTHORIZED);
		}
		if(plugin_client.revk){
			logger.error({"r":req.path,"msg":"plugin_client_not_found","p":req.body});
			return sendError(res,"forbidden","forbidden",constants.HTTP_STATUS.FORBIDDEN);
		}
		req.aid = plugin_client.aid;
		req.tim = tim;
		next();
	});
});

router.post('/text', function(req, res, next) {
	req.checkBody('message',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('group_code',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('profile_name',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"text","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var message    = req.body.message.trim();
	var group_code = req.body.group_code;
	var pname      = req.body.profile_name.trim();
	var account_id = req.aid;
	var tim        = req.tim;

	if(message.length < GroupFeedConfigs.MIN_MESSAGE_LENGTH){
		logger.error({"r":"text","msg":"message_length_too_small","p":req.body});
		return sendError(res,"message_length_small","message_length_small",constants.HTTP_STATUS.BAD_REQUEST);
	}
	if(pname == ""){
		logger.error({"r":"text","msg":"pname_is_null","p":req.body});
		return sendError(res,"profile_name_missing","profile_name_missing",constants.HTTP_STATUS.BAD_REQUEST);
	}

	Group.findOne({
		code : group_code
	},function(err,group){
		if(err){ 
			logger.error({"r":"text","msg":"group_find_server_error","p":req.body,"er":err});
			return sendError(res,"server_error","server_error",constants.HTTP_STATUS.SERVER_ERROR);
		}
		if(!group) {
			logger.error({"r":"text","msg":"no_group_found","p":req.body});
			return sendError(res,"invalid_group_code","invalid_group_code",constants.HTTP_STATUS.BAD_REQUEST);
		}
		if(group.act == false) {
			logger.error({"r":"text","msg":"group_is_inactive","p":req.body});
			return sendError(res,"group_is_inactive","group_is_inactive",constants.HTTP_STATUS.BAD_REQUEST);
		}
		var group_id = group._id+'';
		GroupMember.findOne({
			gid : group._id,
			aid : account_id,
			pnam: pname,
			act : true,
			gdel: false
		},function(err,groupmember){
			if(err){ 
				logger.error({"r":"text","msg":"groupmember_find_server_error","p":req.body,"er":err});
				return sendError(res,"server_error","server_error",constants.HTTP_STATUS.SERVER_ERROR);
			}
			if(!groupmember) {
				logger.error({"r":"text","msg":"groupmember_not_found","p":req.body});
				return sendError(res,"profile_not_groupmember","profile_not_groupmember",constants.HTTP_STATUS.BAD_REQUEST);
			}
			if(groupmember.type != GroupMember.TYPE.ADMIN){
				logger.error({"r":"text","msg":"groupmember_not_admin","p":req.body});
				return sendError(res,"profile_not_group_admin","profile_not_group_admin",constants.HTTP_STATUS.BAD_REQUEST);
			}
			var posting_pid  = groupmember.pid+'';
			var message_type = Message.MESSAGE_TYPE.text;
			var gname        = group.name;
			var gtype        = group.gtyp;
			var memType      = groupmember.type;
			GroupNotify.do_client_text_msg(group_id, posting_pid, message, message_type, gname, gtype, memType, function(err,resp){
				if(err){
					logger.error({"r":"text","msg":"GroupNotify_do_grp_msg_error","p":req.body,"er":err});
					return sendError(res,"server_error","server_error",constants.HTTP_STATUS.SERVER_ERROR);
				}
				logger.info({"r":"text","p":req.body});
				return sendSuccess(res,{});
			});
		});
	});
});

router.post('/image', function(req, res, next) {
	req.checkBody('image_filename',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('group_code',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('profile_name',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"image","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var image_filename  = req.body.image_filename.trim();
	var group_code      = req.body.group_code;
	var pname           = req.body.profile_name.trim();
	var account_id      = req.aid;
	var tim             = req.tim;
	var split           = image_filename.split(".");
	var mid             = split[0];
	var extension       = split[1];

	if(mid < GroupFeedConfigs.MIN_IMAGE_URL_LENGTH){
		logger.error({"r":"image","msg":"image_url_length_too_small","p":req.body});
		return sendError(res,"image_filename_invalid","image_filename_invalid",constants.HTTP_STATUS.BAD_REQUEST);
	}
	if(extension != 'jpg'){
		logger.error({"r":"image","msg":"image_filename_not_jpg","p":req.body});
		return sendError(res,"only_jpg_type_image_allowed","only_jpg_type_image_allowed",constants.HTTP_STATUS.BAD_REQUEST);
	}
	if(pname == ""){
		logger.error({"r":"image","msg":"pname_is_null","p":req.body});
		return sendError(res,"profile_name_missing","profile_name_missing",constants.HTTP_STATUS.BAD_REQUEST);
	}

	Group.findOne({
		code : group_code
	},function(err,group){
		if(err){ 
			logger.error({"r":"image","msg":"group_find_server_error","p":req.body,"er":err});
			return sendError(res,"server_error","server_error",constants.HTTP_STATUS.SERVER_ERROR);
		}
		if(!group) {
			logger.error({"r":"image","msg":"no_group_found","p":req.body});
			return sendError(res,"invalid_group_code","invalid_group_code",constants.HTTP_STATUS.BAD_REQUEST);
		}
		if(group.act == false) {
			logger.error({"r":"image","msg":"group_is_inactive","p":req.body});
			return sendError(res,"group_is_inactive","group_is_inactive",constants.HTTP_STATUS.BAD_REQUEST);
		}
		var group_id = group._id+'';
		GroupMember.findOne({
			gid : group._id,
			aid : account_id,
			pnam: pname,
			act : true,
			gdel: false
		},function(err,groupmember){
			if(err){ 
				logger.error({"r":"image","msg":"groupmember_find_server_error","p":req.body,"er":err});
				return sendError(res,"server_error","server_error",constants.HTTP_STATUS.SERVER_ERROR);
			}
			if(!groupmember) {
				logger.error({"r":"image","msg":"groupmember_not_found","p":req.body});
				return sendError(res,"profile_not_groupmember","profile_not_groupmember",constants.HTTP_STATUS.BAD_REQUEST);
			}
			if(groupmember.type != GroupMember.TYPE.ADMIN){
				logger.error({"r":"image","msg":"groupmember_not_admin","p":req.body});
				return sendError(res,"profile_not_group_admin","profile_not_group_admin",constants.HTTP_STATUS.BAD_REQUEST);
			}

			var posting_pid  = groupmember.pid+'';
			var gname        = group.name;
			var gtype        = group.gtyp;
			var memType      = groupmember.type;
			var message_type = Message.MESSAGE_TYPE.image;
			var message      = image_filename;
			GroupNotify.do_client_image_msg(mid, group_id, posting_pid, message, message_type, gname, gtype, memType, function(err,resp){
				if(err){
					logger.error({"r":"image","msg":"GroupNotify_do_grp_msg_image_error","p":req.body,"er":err});
					return sendError(res,"server_error","server_error",constants.HTTP_STATUS.SERVER_ERROR);
				}
				logger.info({"r":"image","p":req.body});
				return sendSuccess(res,{});
			});
		});
	});
});

router.post('/image_and_text', function(req, res, next) {
	req.checkBody('message',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('image_filename',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('group_code',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('profile_name',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()) { 
		logger.error({"r":"image_and_text","msg":"invalid_parameters","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var message         = req.body.message.trim();
	var image_filename  = req.body.image_filename.trim();
	var group_code 			= req.body.group_code;
	var pname      			= req.body.profile_name.trim();
	var account_id 			= req.aid;
	var tim        			= req.tim;
	var split           = image_filename.split(".");
	var mid             = split[0];
	var extension       = split[1];

	if(mid < GroupFeedConfigs.MIN_IMAGE_URL_LENGTH){
		logger.error({"r":"image_and_text","msg":"image_url_length_too_small","p":req.body});
		return sendError(res,"image_filename_invalid","image_filename_invalid",constants.HTTP_STATUS.BAD_REQUEST);
	}
	if(extension != 'jpg'){
		logger.error({"r":"image_and_text","msg":"image_filename_not_jpg","p":req.body});
		return sendError(res,"only_jpg_type_image_allowed","only_jpg_type_image_allowed",constants.HTTP_STATUS.BAD_REQUEST);
	}
	if(message.length < GroupFeedConfigs.MIN_MESSAGE_LENGTH){
		logger.error({"r":"image_and_text","msg":"message_length_too_small","p":req.body});
		return sendError(res,"message_length_small","message_length_small",constants.HTTP_STATUS.BAD_REQUEST);
	}
	if(pname == ""){
		logger.error({"r":"image_and_text","msg":"pname_is_null","p":req.body});
		return sendError(res,"profile_name_missing","profile_name_missing",constants.HTTP_STATUS.BAD_REQUEST);
	}

	Group.findOne({
		code : group_code
	},function(err,group){
		if(err){ 
			logger.error({"r":"image_and_text","msg":"group_find_server_error","p":req.body,"er":err});
			return sendError(res,"server_error","server_error",constants.HTTP_STATUS.SERVER_ERROR);
		}
		if(!group) {
			logger.error({"r":"image_and_text","msg":"no_group_found","p":req.body});
			return sendError(res,"invalid_group_code","invalid_group_code",constants.HTTP_STATUS.BAD_REQUEST);
		}
		if(group.act == false) {
			logger.error({"r":"image_and_text","msg":"group_is_inactive","p":req.body});
			return sendError(res,"group_is_inactive","group_is_inactive",constants.HTTP_STATUS.BAD_REQUEST);
		}
		var group_id = group._id+'';
		GroupMember.findOne({
			gid : group._id,
			aid : account_id,
			pnam: pname,
			act : true,
			gdel: false
		},function(err,groupmember){
			if(err){ 
				logger.error({"r":"image_and_text","msg":"groupmember_find_server_error","p":req.body,"er":err});
				return sendError(res,"server_error","server_error",constants.HTTP_STATUS.SERVER_ERROR);
			}
			if(!groupmember) {
				logger.error({"r":"image_and_text","msg":"groupmember_not_found","p":req.body});
				return sendError(res,"profile_not_groupmember","profile_not_groupmember",constants.HTTP_STATUS.BAD_REQUEST);
			}
			if(groupmember.type != GroupMember.TYPE.ADMIN){
				logger.error({"r":"image_and_text","msg":"groupmember_not_admin","p":req.body});
				return sendError(res,"profile_not_group_admin","profile_not_group_admin",constants.HTTP_STATUS.BAD_REQUEST);
			}
			var posting_pid  = groupmember.pid+'';
			var message_type = Message.MESSAGE_TYPE.text;
			var gname        = group.name;
			var gtype        = group.gtyp;
			var memType      = groupmember.type;
			GroupNotify.do_client_text_msg(group_id, posting_pid, message, message_type, gname, gtype, memType, function(err,resp){
				if(err){
					logger.error({"r":"image_and_text","msg":"GroupNotify_do_grp_msg_text_error","p":req.body,"er":err});
					return sendError(res,"server_error","server_error",constants.HTTP_STATUS.SERVER_ERROR);
				}
				logger.info({"r":"image_and_text","p":req.body});
				message_type = Message.MESSAGE_TYPE.image;
				message      = image_filename;
				GroupNotify.do_client_image_msg(mid, group_id, posting_pid, message, message_type, gname, gtype, memType, function(err,resp){
					if(err){
						logger.error({"r":"image_and_text","msg":"GroupNotify_do_grp_msg_image_error","p":req.body,"er":err});
						return sendError(res,"server_error","server_error",constants.HTTP_STATUS.SERVER_ERROR);
					}
					logger.info({"r":"image_and_text","p":req.body});
					return sendSuccess(res,{});
				});
			});
		});
	});
});

// router.post('/posting_image_url', function(req, res, next) {
// 	req.checkQuery('mode',errorCodes.invalid_parameters[1]).notEmpty();
//   req.checkQuery('file_type',errorCodes.invalid_parameters[1]).notEmpty();
//   req.checkQuery('file_name',errorCodes.invalid_parameters[1]).notEmpty();

// 	if(req.validationErrors()) { 
// 		logger.error({"r":"posting_image_url","msg":"invalid_parameters","p":req.body});
// 		return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
// 	}

// 	var mode            = req.body.mode;
//   var file_type       = req.body.file_type;
//   var file_name       = req.body.file_name;
// 	var account_id      = req.aid;
// 	var tim             = req.tim;

// 	if(file_name.length < GroupFeedConfigs.MIN_IMAGE_URL_LENGTH){
// 		logger.error({"r":"posting_image_url","msg":"image_filename_length_too_small","p":req.body});
// 		return sendError(res,"image_filename_invalid","image_filename_invalid",constants.HTTP_STATUS.BAD_REQUEST);
// 	}
// 	if(!AwsFileValidations.isValidMode(file_type, mode)){
//     logger.error({"r":"posting_image_url","m":"GET","msg":"isValidMode_error","p":req.query});
//     return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
//   }
//   if(!AwsFileValidations.isValidCpicFile(file_type, file_name)){
//     logger.error({"r":"posting_image_url","m":"GET","msg":"isValidCpicFile_error","p":req.query});
//     return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
//   }
//   var format          = mode+"/"+file_type;
//   var credentials     = AwsS3Module.getCredentials(AwsBuckets.CHAT_PIC, format, file_type);
//   return sendSuccess(res, {upload_credentials : credentials});
// });

module.exports = router;