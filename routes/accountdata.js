var express 							= require('express');
var mongoose 							= require('mongoose');
var jwt    				    		= require('jsonwebtoken');

var configs								= require('../utility/configs.js');
var errorCodes 						= require('../utility/errors.js');
var constants							= require('../utility/constants.js');
var helpers								= require('../utility/helpers.js');
var AppClients						= require('../utility/app_clients.js');
var AwsSnsModule					= require('../utility/aws_sns_module.js');
var ClientType        		= require('../utility/client_type.js');
var log4jsLogger  				= require('../loggers/log4js_module.js');
var log4jsConfigs  			  = require('../loggers/log4js_configs.js');
var RabbitMqPublish   		= require('../rabbitmq/publisher/publisher.js');
var RabbitMqQueues    		= require('../rabbitmq/queues.js');
var UnsecureAccounts 		  = require('../utility/auth/unsecure_accounts.js');
var ValidAuthRequest      = require('../utility/auth/valid_request.js');
var AccountDataInfoCache  = require('../cache/accountdata_info.js');
var UserInfoCache         = require('../utility/user_info_cache.js');

require('../models/accounts.js');
require('../models/accountdata.js');
require('../models/groupmembers.js');
var Account     					= mongoose.model('Account');
var AccountData 					= mongoose.model('AccountData');
var GroupMember 					= mongoose.model('GroupMember');

var sendError 						= helpers.sendError;
var sendSuccess 					= helpers.sendSuccess;

var router 								= express.Router();
var logger        				= log4jsLogger.getLogger(log4jsConfigs.CATEGORY_ROUTES_ACCOUNTDATA);

/*Store Token which are orphans*/
router.post('/o_dtkn', function(req, res, next) {
	req.checkBody('dtkn',errorCodes.invalid_parameters[1]).notEmpty().isValidDeviceToken();
	req.checkBody('dtyp',errorCodes.invalid_parameters[1]).notEmpty().validateDeviceType();
	req.checkBody('clv',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('vrsn',errorCodes.invalid_parameters[1]).notEmpty().isValidVersion();

	if(req.validationErrors()){
		logger.error({"r":"o_dtkn","m":"POST","msg":"invalid_parameters","p":req.body}); 
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var device_token 		= req.body.dtkn;
	var device_type  		= req.body.dtyp;
	var client_vrsn  		= req.body.clv;
	var version      		= req.body.vrsn;
	var tim          		= new Date().getTime();

	AccountData.findOne({
		dtkn : device_token
	},function(err,acn_data){
		if(err) { 
			logger.error({"r":"o_dtkn","m":"POST","msg":"server_error","p":req.body});
			return sendError(res,err,"server_error"); 
		}
		if(acn_data) { 
			logger.info({"r":"o_dtkn","m":"POST","msg":"o_token_already_exists_in_DB","p":req.body});
			return sendSuccess(res,{});;
		}
		var new_account_data = new AccountData({
			dtkn : device_token,
			dtyp : device_type,
			clv  : client_vrsn,
			vrsn : version,
			act  : false
		});
		new_account_data.save(function(err,inserted_account_data){
			if(err || !inserted_account_data) { 
				logger.error({"r":"o_dtkn","m":"POST","msg":"o_tokn could not be stored in DB","p":req.body});
				return sendError(res,err,"server_error"); 
			}
			return sendSuccess(res,{});
		});
	});
});

//route middleware to verify the jwt token for all unsecured accounts as well covering for android users
router.use(function(req, res, next){
	var token = req.body.tokn || req.query.tokn;
	if(!token){
		logger.error({"r":"auth","msg":"token_not_found"});
	  return sendError(res,"Access without token is not authorised","token_not_found",constants.HTTP_STATUS.FORBIDDEN);
	}
	var pid = null;//req.body.p_id || req.query.p_id;
	var aid = req.body.a_id || req.query.a_id || req.body.aid || req.query.aid;
	req.decoded = null;
	if(token == 5 || token == '5' || token == 'null'){
		UnsecureAccounts.check(aid, pid, function(err,isUnsecure){
			if(err){
				console.log(err);
				logger.error({"url":req.originalUrl,"r":"auth","msg":"UnsecureAccounts_check_error","p":{aid:aid,tokn:token}});
				return sendError(res,err,"server_error");
			}
			if(isUnsecure){
				next();
			} else {
				logger.error({"url":req.originalUrl,"r":"auth","msg":"account_not_found_in_unsecured_list","p":{aid:aid,tokn:token}});
				return sendError(res,"Failed to authenticate token.","token_auth_failure",constants.HTTP_STATUS.NOT_FOUND);
			}
		});
	} else {
    jwt.verify(token, configs.JWT_ACCESS_TOKEN_PRIVATE_KEY, function(err, decoded) {      
      if(err){
      	console.log(err);
      	logger.error({"url":req.originalUrl,"r":"auth","msg":"jwt_verify_error","p":{aid:aid,tokn:token}});
        return sendError(res,"Failed to authenticate token.","token_auth_failure",constants.HTTP_STATUS.TOKEN_EXPIRED);  
      } else {
      	req.decoded = decoded;
        next();
      }
    });
	}
});

/* Update Token - If token does not exists , it will create or if exists , it will update.
		End Point Arn genration will take place if device token does not exists
*/
router.post('/u_dtkn', function(req, res, next) {
	req.checkBody('aid',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('dtkn',errorCodes.invalid_parameters[1]).notEmpty().isValidDeviceToken();
	req.checkBody('dtyp',errorCodes.invalid_parameters[1]).notEmpty().validateAccountType();
	req.checkBody('clv',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('vrsn',errorCodes.invalid_parameters[1]).notEmpty().isValidVersion();

	if(req.validationErrors()){
		logger.error({"r":"u_dtkn","m":"POST","msg":"invalid_parameters","p":req.body}); 
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}
	
	var account_id   		= req.body.aid;
	var device_token 		= req.body.dtkn;
	var device_type  		= req.body.dtyp;
	var client_vrsn  		= req.body.clv;
	var version      		= req.body.vrsn;
	var dateTime        = new Date();
	var tim          		= dateTime.getTime();

	if(!ValidAuthRequest.check(account_id,req.decoded)){
		logger.error({"r":"u_dtkn","m":"POST","msg":"ValidAuthRequest_check_error","p":req.body});
		return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}
  
	Account.findOne({
		_id : account_id
	},function(err,account){
		if(err){ 
			errorAndActivityLog(account_id, "u_dtkn", "POST", "server_error", tim, req.body, device_type, version, 500);
			return sendError(res,err,"server_error");
		}
		if(!account){ 
			errorAndActivityLog(account_id, "u_dtkn", "POST", "no_account_found", tim, req.body, device_type, version, 400);
			return sendError(res,"Account does not exist","no_account_found",constants.HTTP_STATUS.BAD_REQUEST);
		}
		AccountData.update({
			dtkn : device_token,
			act : true
		},{
			act : false,
			uat : dateTime
		},{
			multi : true
		},function(err,acn_data){
			if(err){
				errorAndActivityLog(account_id, "u_dtkn", "POST", "accountData_server_error", tim, req.body, device_type, version, 500); 
				return sendError(res,err,"server_error");
			}
			AwsSnsModule.createDeviceEndPointArn(account_id, device_token, device_type, function(err,endPointArn){
				if(err){ 
					errorAndActivityLog(account_id, "u_dtkn", "POST", "createDeviceEndPointArn_server_error", tim, req.body, device_type, version, 500);
					return sendError(res,err,"server_error");
				}
				AccountData.update({
					dtkn: device_token,
					act : false,
					aid : { $exists : false }
				},{
					$setOnInsert : {
						dtkn : device_token,
						dtyp : device_type
					},
					$set : {
						aid  : account_id,
						clv  : client_vrsn,
						vrsn : version,
						earn : endPointArn,
						act  : true,
						uat  : dateTime,
						cat  : dateTime
					}
				},{
					upsert : true
				},function(err,accountData_created){
					if(err){ 
						errorAndActivityLog(account_id, "u_dtkn", "POST", "token_could_not_be_stored_in_DB", tim, req.body, device_type, version, '500');
						return sendError(res,err,"server_error"); 
					}
					removeUserAccountDataCache(account_id,function(err,resp){
						if(err){
							logger.error({"r":"u_dtkn","msg":"removeUserAccountDataCache","p":req.body,"er":err});
						}
						successAndActivityLog(account_id, "u_dtkn", tim, device_type, version);
						return sendSuccess(res,{});
					});
				});
			});
		});
	});
});

function removeUserAccountDataCache(aid, cb){
  UserInfoCache.getUserGroupMemPartial(aid, function(err,groupmembers){
  	if(err){ 
    	return cb(err,null);
    }
    total       = groupmembers.length;
    if(total == 0){
	  	return cb(null,true);
	  }
    pids        = null;
    var done    = 0;
    var errors  = 0;
    var success = 0;
    for(var i=0; i<total; i++){
    	AccountDataInfoCache.removeGroupAccountDatas(groupmembers[i].gid,function(err,resp){
    		if(err){
    			console.trace(err);
    			errors++;
    		} else {
    			success++;
    		}
    		done++;
    		if(done == total){
    			logger.info({"r":"u_dtkn","msg":"removeUserAccountDataCache","p":aid,"err_cnt":errors,"succ_cnt":success});
    			return cb(null,true);
    		}
    	});
    }
  });
}

function errorAndActivityLog(aid, route, method, msg, tim, data, cl, vrsn, ecode){
  logger.error({"r":route,"m":method,"msg":msg,"p":data});
  var actv_obj = new Buffer(JSON.stringify({
    aid   : aid,
    cl    : cl,
    vrsn  : vrsn,
    nm    : route,
    stts  : ecode,
    msg   : msg,
    tim   : tim
  }));
  RabbitMqPublish.publish("",RabbitMqQueues.USER_ACTIVITY, actv_obj);
}

function successAndActivityLog(aid, route, tim, cl, vrsn){
  var actv_obj = new Buffer(JSON.stringify({
    aid   : aid,
    cl    : cl,
    vrsn  : vrsn,
    nm    : route,
    stts  : 200,
    msg   : 'success',
    tim   : tim
  }));
  RabbitMqPublish.publish("",RabbitMqQueues.USER_ACTIVITY, actv_obj);
}

module.exports = router;
