var express 						= require('express');
var mongoose 						= require('mongoose');
var jwt    				      = require('jsonwebtoken');
var redis               = require('redis');

var constants						= require('../utility/constants.js');
var configs							= require('../utility/configs.js');
var errorCodes 					= require('../utility/errors.js');
var helpers							= require('../utility/helpers.js');
var GroupNotify 				= require('../utility/grp_notify.js');
var Actions 						= require('../utility/action_names.js');
var AppClients					= require('../utility/app_clients.js');
var AccountInfo   			= require('../utility/account_info.js');
var EncryptData         = require('../utility/encrypt_decrypt.js');
var UserRedisKeys       = require('../utility/user_redis_keys.js');
var GroupRedisKeys      = require('../utility/group_redis_keys.js');
var RedisPrefixes       = require('../utility/redis_prefix.js');
var UserInfoCache       = require('../utility/user_info_cache.js');
var GroupInfoCache      = require('../utility/group_info_cache.js');
var log4jsLogger  		  = require('../loggers/log4js_module.js');
var log4jsConfigs  			= require('../loggers/log4js_configs.js');
var ValidAuthRequest    = require('../utility/auth/valid_request.js');
var UnsecureAccounts    = require('../utility/auth/unsecure_accounts.js');
var GroupJoinLimits     = require('../utility/auth/group_join.js');
var AccountDataInfoCache= require('../cache/accountdata_info.js');

var redis_client        = redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
    console.log("Error " + err);
});

require('../models/groups.js');
require('../models/groupmembers.js');
require('../models/profiles.js');
require('../models/accounts.js');
require('../models/privilegegroups.js');
require('../models/recommendedgroups.js');
require('../models/groupcategories.js');
require('../models/packages/grouppluginpackages.js');

var Group 							= mongoose.model('Group');
var GroupMember 				= mongoose.model('GroupMember'); 
var Profile 						= mongoose.model('Profile');
var Account 						= mongoose.model('Account');
var PrivilegeGroup 			= mongoose.model('PrivilegeGroup');
var RecommendedGroup 		= mongoose.model('RecommendedGroup');
var GroupCategory 	    = mongoose.model('GroupCategory');
var GroupPluginPackage 	= mongoose.model('GroupPluginPackage');

var rhmset              = require("../utility/redis_hmset.js")(redis_client);

var sendError 					= helpers.sendError;
var sendSuccess 				= helpers.sendSuccess;
var router 							= express.Router();
var logger        			= log4jsLogger.getLogger(log4jsConfigs.CATEGORY_ROUTES_GROUPMEMBER);
var bruteForceGrpJnlogger = log4jsLogger.getLogger(log4jsConfigs.CATEGORY_BRUTE_FORCE_GROUP_JOIN);
var secureGroupLogger   = log4jsLogger.getLogger(log4jsConfigs.CATEGORY_SECURE_GROUP);


// Get Member List for a Group for Older version apps
router.get('/list', function(req, res, next) {
	req.checkQuery('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkQuery('tokn',errorCodes.invalid_parameters[1]).notEmpty().validateToken();
	req.checkQuery('start',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkQuery('ipp',errorCodes.invalid_parameters[1]).notEmpty().isValidIPP();

	if(req.validationErrors()){ 
		logger.error({"r":"list","m":"GET","msg":"invalid_parameters","p":req.query});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var group_id 		= req.query.g_id;
	var start 			= parseInt(req.query.start);
	var ipp 				= parseInt(req.query.ipp);

	GroupInfoCache.getMongoGroup(group_id,function(err,group){
		if(err) { 
			logger.error({"r":"list","m":"GET","msg":"getMongoGroup_error","p":req.query});
			return sendError(res,err,"server_error");
		}
		if(!group){
			logger.error({"r":"list","m":"GET","msg":"no_group_found","p":req.query});
			return sendError(res,"Group does not exists.","no_group_found",constants.HTTP_STATUS.NOT_FOUND);
		}
		GroupMember.count({
			gid : group_id,
			act : true,
			gdel: false,
			type: {$ne:GroupMember.TYPE.BANNED}
		},function(err,count) {
			if(err) { 
				logger.error({"r":"list","m":"GET","msg":"groupMember_count_server_error","p":req.query});
				return sendError(res,err,"server_error");
			}
			GroupMember.find({
				gid : group_id,
				act : true
			},{
				_id     : 0,
				cat     : 0,
				__v     : 0,
				gdel    : 0,
				act     : 0,
				gpaid   : 0,
				sub_st  : 0,
				sub_ed  : 0,
				rcmd    : 0,
				ordr_id : 0,
				plugin_id:0,
				pamt    : 0,
				muttm   : 0,
				muttp   : 0
			},function(err, members){
				if(err){ 
					logger.error({"r":"list","m":"GET","msg":"groupMember_server_error","p":req.query});
					return sendError(res,err,"server_error");
				}
				var profile_ids = [];
				var members_by_pid = {};
				for(var i = 0; i < members.length; i++) {
					profile_ids.push(members[i].pid);

					members_by_pid[members[i].pid] = members[i];
				}
				Profile.find({
					_id : {$in : profile_ids},
					act : true
				},{},{
					sort:{nam:'asc'},
					skip:start,
					limit:ipp
				},function(err,profiles){
					if(err){ 
						logger.error({"r":"list","m":"GET","msg":"profiles_server_error","p":req.query});
						return sendError(res,err,"server_error");
					}
					var members_subset = [];
					for(var i = 0; i < profiles.length; i++) {
						members_subset.push(members_by_pid[profiles[i]._id]);
					}
					return sendSuccess(res, { total : count, members : members_subset, profiles : profiles });
				})
			});
		});
	});
});

//route middleware to verify the jwt token for all unsecured accounts as well covering for android users
router.use(function(req, res, next){
	var token = req.body.tokn || req.query.tokn;
	if(!token){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"token_not_found"});
	  return sendError(res,"Access without token is not authorised","token_not_found",constants.HTTP_STATUS.FORBIDDEN);
	}
	var pid;
	if(req.body.admin_id){
		pid = req.body.admin_id;
	} else {
		pid = req.body.p_id || req.query.p_id;
	}
	var aid = null;//req.body.a_id || req.query.a_id;
	req.decoded = null;
	if(token == 5 || token == '5' || token == 'null'){
		UnsecureAccounts.check(aid, pid, function(err,isUnsecure){
			if(err){
				console.log(err);
				logger.error({"url":req.originalUrl,"r":"auth","msg":"UnsecureAccounts_check_error","p":{pid:pid,tokn:token}});
				return sendError(res,err,"server_error");
			}
			if(isUnsecure){
				next();
			} else {
				logger.error({"url":req.originalUrl,"r":"auth","msg":"account_not_found_in_unsecured_list","p":{pid:pid,tokn:token}});
				return sendError(res,"Failed to authenticate token.","token_auth_failure",constants.HTTP_STATUS.NOT_FOUND);
			}
		});
	} else {
    jwt.verify(token, configs.JWT_ACCESS_TOKEN_PRIVATE_KEY, function(err, decoded) {      
      if(err){
      	console.log(err);
      	logger.error({"url":req.originalUrl,"r":"auth","msg":"jwt_verify_error","p":{pid:pid,tokn:token}});
        return sendError(res,"Failed to authenticate token.","token_auth_failure",constants.HTTP_STATUS.TOKEN_EXPIRED);  
      } else {
      	req.decoded = decoded;
        next();
      }
    });
	}
});

// Get Member List for a Group for Newer version apps
router.get('/list_v14', function(req, res, next) {
	req.checkQuery('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkQuery('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkQuery('tokn',errorCodes.invalid_parameters[1]).notEmpty().validateToken();
	req.checkQuery('start',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkQuery('ipp',errorCodes.invalid_parameters[1]).notEmpty().isValidIPP();

	if(req.validationErrors()){ 
		logger.error({"r":"list_v14","m":"GET","msg":"invalid_parameters","p":req.query});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var group_id 		= req.query.g_id;
	var profile_id  = req.query.p_id;
	var start 			= parseInt(req.query.start);
	var ipp 				= parseInt(req.query.ipp);
	var tim         = new Date().getTime();

	GroupInfoCache.getMongoGroup(group_id,function(err,group){
		if(err) { 
			logger.error({"r":"list_v14","m":"GET","msg":"getMongoGroup_error","p":req.query});
			return sendError(res,err,"server_error");
		}
		if(!group){
			logger.error({"r":"list_v14","m":"GET","msg":"no_group_found","p":req.query});
			return sendError(res,"Group does not exists.","no_group_found",constants.HTTP_STATUS.NOT_FOUND);
		}
		UserInfoCache.getMongoGrpMem(profile_id, group_id, function(err, groupmember){
			if(err) { 
				logger.error({"r":"list_v14","m":"GET","msg":"getMongoGrpMem_DbError","p":req.query});
				return sendError(res,err,"server_error"); 
			}
			if(!groupmember){
				logger.error({"r":"list_v14","m":"GET","msg":"not_group_member","p":req.query});
				return sendError(res,"Profile is not member of the group.","not_a_member",constants.HTTP_STATUS.BAD_REQUEST);
			}
			if(!ValidAuthRequest.check(groupmember.aid,req.decoded)){
				logger.error({"r":"list_v14","m":"GET","msg":"ValidAuthRequest_check_error","p":req.query});
				return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
			}
			if(group.req_sub && group.req_sub ==  true){ 
				if(!groupmember.sub_ed && !groupmember.sub_st){
					logger.error({"r":"list_v14","msg":"user_subscription_required","p":req.query});
					return sendError(res,"You need to subscribe to enable your access.","user_subscription_required",constants.HTTP_STATUS.USER_SUBSCRIPTION_REQUIRED);
				}
				if(groupmember.sub_ed < tim){
					logger.error({"r":"list_v14","msg":"user_subscription_ended","p":req.query});
					return sendError(res,"Oops, your subscription has expired. Kindly renew your subscription.","user_subscription_ended",constants.HTTP_STATUS.USER_SUBSCRIPTION_EXPIRED);
				}
			}
			if(groupmember.type == GroupMember.TYPE.BANNED){
				logger.error({"r":"list_v14","m":"GET","msg":"banned_group_member","p":req.query});
				return sendError(res,"Banned group member cannot see group members","banned_group_member",constants.HTTP_STATUS.NOT_FOUND);
			} 
			var valid_types = [GroupMember.TYPE.ADMIN,GroupMember.TYPE.MEMBER];
			GroupMember.count({
				gid : group_id,
				act : true,
				gdel: false,
				type: { $in : valid_types}
			},function(err,total_group_members){
				if(err) { 
					logger.error({"r":"list_v14","m":"GET","msg":"groupMember_count_server_error","p":req.query});
					return sendError(res,err,"server_error"); 
				}
				GroupMember.find({
					gid : group_id,
					act : true,
					gdel: false
				},{
					_id     : 0,
					cat     : 0,
					__v     : 0,
					gdel    : 0,
					act     : 0,
					gpaid   : 0,
					sub_st  : 0,
					sub_ed  : 0,
					rcmd    : 0,
					ordr_id : 0,
					plugin_id:0,
					pamt    : 0,
					muttm   : 0,
					muttp   : 0
				},{
			    sort : {
			      pnam : 'asc' 
			    },
			    skip : start,
			    limit: ipp
				},function(err, members){
					if(err) { 
						logger.error({"r":"list_v14","m":"GET","msg":"groupMember_find_server_error","p":req.query});
						return sendError(res,err,"server_error"); 
					}
					if(members.length == 0) { 
						logger.error({"r":"list_v14","m":"GET","msg":"groupMember_find_server_error","p":req.query});
						return sendError(res,"Group does not exists.","no_group_found",constants.HTTP_STATUS.NOT_FOUND);
					}
					var profile_ids = [];
					var members_length = members.length;
					for(var i = 0; i < members_length; i++) {
						profile_ids.push(members[i].pid);
					}
					Profile.find({
						_id : {$in : profile_ids},
						act : true
					},{
						cat : 0,
						clss: 0,
						sid : 0,
						__v : 0
					},function(err,profiles){
						if(err) { 
							logger.error({"r":"list_v14","m":"GET","msg":"profiles_server_error","p":req.query});
							return sendError(res,err,"server_error"); 
						}
						return sendSuccess(res, { total : total_group_members, members : members, profiles : profiles, tim:tim});
					});
				});
			});
		});
	});
});

// Get Member List for a Group for Newer version apps
router.get('/list_v16', function(req, res, next) {
	req.checkQuery('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkQuery('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkQuery('tokn',errorCodes.invalid_parameters[1]).notEmpty().validateToken();
	req.checkQuery('start',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkQuery('ipp',errorCodes.invalid_parameters[1]).notEmpty().isValidIPP();

	if(req.validationErrors()){ 
		logger.error({"r":"list_v16","m":"GET","msg":"invalid_parameters","p":req.query});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST); 
	}

	var group_id 		= req.query.g_id;
	var profile_id  = req.query.p_id;
	var start 			= parseInt(req.query.start);
	var ipp 				= parseInt(req.query.ipp);
	var tim         = new Date().getTime();

	GroupInfoCache.getMongoGroup(group_id,function(err,group){
		if(err) { 
			logger.error({"r":"list_v16","m":"GET","msg":"getMongoGroup_error","p":req.query});
			return sendError(res,err,"server_error");
		}
		if(!group){
			logger.error({"r":"list_v16","m":"GET","msg":"no_group_found","p":req.query});
			return sendError(res,"Group does not exists.","no_group_found",constants.HTTP_STATUS.NOT_FOUND);
		}
		UserInfoCache.getMongoGrpMem(profile_id, group_id, function(err, groupmember){
			if(err) { 
				logger.error({"r":"list_v16","m":"GET","msg":"getMongoGrpMem_DbError","p":req.query});
				return sendError(res,err,"server_error"); 
			}
			if(!groupmember){
				logger.error({"r":"list_v16","m":"GET","msg":"not_a_group_member","p":req.query});
				return sendError(res,"Profile is not member of the group.","not_a_member",constants.HTTP_STATUS.BAD_REQUEST);
			}
			if(!ValidAuthRequest.check(groupmember.aid,req.decoded)){
				logger.error({"r":"list_v16","m":"GET","msg":"ValidAuthRequest_check_error","p":req.query});
				return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
			}
			if(group.req_sub && group.req_sub ==  true){ 
				if(!groupmember.sub_ed && !groupmember.sub_st){
					logger.error({"r":"list_v16","msg":"user_subscription_required","p":req.query});
					return sendError(res,"You need to subscribe to enable your access.","user_subscription_required",constants.HTTP_STATUS.USER_SUBSCRIPTION_REQUIRED);
				}
				if(groupmember.sub_ed < tim){
					logger.error({"r":"list_v16","msg":"user_subscription_ended","p":req.query});
					return sendError(res,"Oops, your subscription has expired. Kindly renew your subscription.","user_subscription_ended",constants.HTTP_STATUS.USER_SUBSCRIPTION_EXPIRED);
				}
			}
			if(groupmember.type == GroupMember.TYPE.BANNED){
				logger.error({"r":"list_v16","m":"GET","msg":"banned_group_member","p":req.query});
				return sendError(res,"Banned group member cannot see group members","banned_group_member",constants.HTTP_STATUS.NOT_FOUND);
			} 
			var valid_types = [GroupMember.TYPE.ADMIN,GroupMember.TYPE.MEMBER];
			GroupMember.count({
				gid : group_id,
				act : true,
				gdel: false,
				type: { $in : valid_types}
			},function(err,total_group_members){
				if(err) { 
					logger.error({"r":"list_v16","m":"GET","msg":"groupMember_count_server_error","p":req.query});
					return sendError(res,err,"server_error"); 
				}
				GroupMember.find({
					gid : group_id,
					act : true,
					gdel: false
				},{
					_id     : 0,
					cat     : 0,
					__v     : 0,
					gdel    : 0,
					act     : 0,
					gpaid   : 0,
					sub_st  : 0,
					sub_ed  : 0,
					rcmd    : 0,
					ordr_id : 0,
					plugin_id: 0,
					pamt    : 0,
					muttm   : 0,
					muttp   : 0
				},{
			    sort : {
			      pnam : 'asc' 
			    },
			    skip : start,
			    limit: ipp
				},function(err, members){
					if(err) { 
						logger.error({"r":"list_v16","m":"GET","msg":"groupMember_find_server_error","p":req.query});
						return sendError(res,err,"server_error"); 
					}
					if(members.length == 0){
						logger.error({"r":"list_v16","m":"GET","msg":"no_group_found","p":req.query}); 
						return sendError(res,"Group does not exists.","no_group_found",constants.HTTP_STATUS.NOT_FOUND);
					}
					var profile_ids = [];
					var members_length = members.length;
					for(var i = 0; i < members_length; i++) {
						profile_ids.push(members[i].pid);
					}
					Profile.find({
						_id : {$in : profile_ids},
						act : true
					},{
						cat : 0,
						clss: 0,
						sid : 0,
						__v : 0
					},function(err,profiles){
						if(err) { 
							logger.error({"r":"list_v16","m":"GET","msg":"profiles_server_error","p":req.query});
							return sendError(res,err,"server_error"); 
						}
						group = modifyReturningGroupObject(group);
						var num_of_profiles = profiles.length;
						if(groupmember.type == GroupMember.TYPE.ADMIN){
							PrivilegeGroup.findOne({
								gid : group_id
							},function(err, privGrp){
								if(err) { 
									logger.error({"r":"list_v16","m":"GET","msg":"privilege_groups_server_error","p":req.query});
									return sendError(res,err,"server_error"); 
								}
								if(privGrp){
									fetchMobileNumbers(profiles, num_of_profiles, function(err,mobile_data){
										if(err) { 
											logger.error({"r":"list_v16","m":"GET","msg":"fetch_mobiles_server_error","p":req.query});
											return sendError(res,err,"server_error"); 
										}
										if(mobile_data) {
											var profile_data = [];
											for(var n=0; n<num_of_profiles; n++){
												profile_data[n] = profiles[n].toJSON();
												profile_data[n].m = mobile_data[profiles[n]._id];
											}
											return sendSuccess(res, { total : total_group_members, members : members, profiles : profile_data, group : group});
										} else {
											logger.error({"r":"list_v16","m":"GET","msg":"mobile_data_server_error","p":req.query});
											return sendError(res,err,"server_error"); 
										}
									});
								} else {
									return sendSuccess(res, { total : total_group_members, members : members, profiles : profiles, group : group, tim:tim});
								}
							});
						} else {
							return sendSuccess(res, { total : total_group_members, members : members, profiles : profiles, group : group, tim:tim});
						}
					});
				});
			});
		});
	});
});

// Get Member List for a Group from paid onwards
router.get('/list_v18', function(req, res, next) {
	req.checkQuery('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkQuery('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkQuery('tokn',errorCodes.invalid_parameters[1]).notEmpty().validateToken();
	req.checkQuery('start',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkQuery('ipp',errorCodes.invalid_parameters[1]).notEmpty().isValidIPP();

	if(req.validationErrors()){ 
		logger.error({"r":"list_v16","m":"GET","msg":"invalid_parameters","p":req.query});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST); 
	}

	var group_id 		= req.query.g_id;
	var profile_id  = req.query.p_id;
	var start 			= parseInt(req.query.start);
	var ipp 				= parseInt(req.query.ipp);
	var tim         = new Date().getTime();

	GroupInfoCache.getMongoGroup(group_id,function(err,group){
		if(err) { 
			logger.error({"r":"list_v16","m":"GET","msg":"getMongoGroup_error","p":req.query});
			return sendError(res,err,"server_error");
		}
		if(!group){
			logger.error({"r":"list_v16","m":"GET","msg":"no_group_found","p":req.query});
			return sendError(res,"Group does not exists.","no_group_found",constants.HTTP_STATUS.NOT_FOUND);
		}
		UserInfoCache.getMongoGrpMem(profile_id, group_id, function(err, groupmember){
			if(err) { 
				logger.error({"r":"list_v16","m":"GET","msg":"getMongoGrpMem_DbError","p":req.query});
				return sendError(res,err,"server_error"); 
			}
			if(!groupmember){
				logger.error({"r":"list_v16","m":"GET","msg":"not_a_group_member","p":req.query});
				return sendError(res,"Profile is not member of the group.","not_a_member",constants.HTTP_STATUS.BAD_REQUEST);
			}
			if(groupmember.aid != req.decoded.id){
				logger.error({"r":"list_v16","m":"GET","msg":"ValidAuthRequest_check_error","p":req.query});
				return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
			}
			var v_subr = false;
			if(group.req_sub && group.req_sub ==  true){ 
				if(!groupmember.sub_ed && !groupmember.sub_st){
					logger.error({"r":"list_v16","msg":"user_subscription_required","p":req.query});
					return sendError(res,"You need to subscribe to enable your access.","user_subscription_required",constants.HTTP_STATUS.USER_SUBSCRIPTION_REQUIRED);
				}
				if(groupmember.sub_ed < tim){
					logger.error({"r":"list_v16","msg":"user_subscription_ended","p":req.query});
					return sendError(res,"Oops, your subscription has expired. Kindly renew your subscription.","user_subscription_ended",constants.HTTP_STATUS.USER_SUBSCRIPTION_EXPIRED);
				}
				v_subr = true;
			}
			var mtyp = groupmember.type;
			if(mtyp == GroupMember.TYPE.BANNED){
				logger.error({"r":"list_v16","m":"GET","msg":"banned_group_member","p":req.query});
				return sendError(res,"Banned group member cannot see group members","banned_group_member",constants.HTTP_STATUS.NOT_FOUND);
			} 
			var valid_types = [GroupMember.TYPE.ADMIN,GroupMember.TYPE.MEMBER];
			GroupMember.count({
				gid : group_id,
				act : true,
				gdel: false,
				type: { $in : valid_types}
			},function(err,total_group_members){
				if(err) { 
					logger.error({"r":"list_v16","m":"GET","msg":"groupMember_count_server_error","p":req.query});
					return sendError(res,err,"server_error"); 
				}
				GroupMember.find({
					gid : group_id,
					act : true,
					gdel: false
				},{
					_id     : 0,
					cat     : 0,
					__v     : 0,
					gdel    : 0,
					act     : 0,
					gpaid   : 0,
					sub_st  : 0,
					sub_ed  : 0,
					rcmd    : 0,
					ordr_id : 0,
					plugin_id:0,
					pamt    : 0,
					muttm   : 0,
					muttp   : 0
				},{
			    sort : {
			      pnam : 'asc' 
			    },
			    skip : start,
			    limit: ipp
				},function(err, members){
					if(err) { 
						logger.error({"r":"list_v16","m":"GET","msg":"groupMember_find_server_error","p":req.query});
						return sendError(res,err,"server_error"); 
					}
					if(members.length == 0){
						logger.error({"r":"list_v16","m":"GET","msg":"no_group_found","p":req.query}); 
						return sendError(res,"Group does not exists.","no_group_found",constants.HTTP_STATUS.NOT_FOUND);
					}
					var profile_ids = [];
					var members_length = members.length;
					for(var i = 0; i < members_length; i++) {
						profile_ids.push(members[i].pid);
					}
					Profile.find({
						_id : {$in : profile_ids},
						act : true
					},{
						cat : 0,
						clss: 0,
						sid : 0,
						__v : 0
					},function(err,profiles){
						if(err) { 
							logger.error({"r":"list_v16","m":"GET","msg":"profiles_server_error","p":req.query});
							return sendError(res,err,"server_error"); 
						}
						var responseData = {
							total   : total_group_members,
							members : members,
							profiles: profiles,
							tim     : tim
						};
						if(start == 0){
							responseData.group = modifyReturningGroupObject(group);
							responseData.mtyp  = mtyp;
							responseData.v_subr= v_subr;
						}
						if(groupmember.type == GroupMember.TYPE.ADMIN){
							PrivilegeGroup.findOne({
								gid : group_id
							},function(err, privGrp){
								if(err) { 
									logger.error({"r":"list_v16","m":"GET","msg":"privilege_groups_server_error","p":req.query});
									return sendError(res,err,"server_error"); 
								}
								var num_of_profiles = profiles.length;
								if(privGrp){
									fetchMobileNumbers(profiles, num_of_profiles, function(err,mobile_data){
										if(err) { 
											logger.error({"r":"list_v16","m":"GET","msg":"fetch_mobiles_server_error","p":req.query});
											return sendError(res,err,"server_error"); 
										}
										if(mobile_data) {
											var profile_data = [];
											for(var n=0; n<num_of_profiles; n++){
												profile_data[n] = profiles[n].toJSON();
												profile_data[n].m = mobile_data[profiles[n]._id];
											}
											responseData.profiles = profile_data;
											return sendSuccess(res, responseData);
										} else {
											logger.error({"r":"list_v16","m":"GET","msg":"mobile_data_server_error","p":req.query});
											return sendError(res,err,"server_error"); 
										}
									});
								} else {
									return sendSuccess(res, responseData);
								}
							});
						} else {
							return sendSuccess(res, responseData);
						}
					});
				});
			});
		});
	});
});

/*
 * CHECK IF USER IS MEMBER OF THE GROUP
 */
router.get('/check', function(req, res, next) {
	req.checkQuery('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkQuery('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkQuery('tokn',errorCodes.invalid_parameters[1]).notEmpty().validateToken();

	if(req.validationErrors()){
		logger.error({"r":"check","m":"GET","msg":"invalid_parameters","p":req.query}); 
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var group_id 			= req.query.g_id;
	var profile_id 		= req.query.p_id;

	GroupInfoCache.getMongoGroup(group_id,function(err,group){
		if(err) { 
			logger.error({"r":"check","m":"GET","msg":"getMongoGroup_error","p":req.query});
			return sendError(res,err,"server_error");
		}
		if(!group){
			logger.error({"r":"check","m":"GET","msg":"no_group_found","p":req.query});
			return sendError(res,"Group does not exists.","no_group_found",constants.HTTP_STATUS.NOT_FOUND);
		}
		if(group.secr && !req.decoded){
			secureGroupLogger.error({"r":"check","m":"GET","msg":"secr_group_not_authorized","p":req.query});
			logger.error({"r":"check","m":"GET","msg":"secr_group_not_authorized","p":req.query});
			return sendError(res,"not_authorized","not_authorized",constants.HTTP_STATUS.UNAUTHORIZED);
		}
		UserInfoCache.getMongoGrpMem(profile_id, group_id, function(err, group_member){
			if(err) { 
				logger.error({"r":"check","m":"GET","msg":"getMongoGrpMem_DbError","p":req.query});
				return sendError(res,err,"server_error"); 
			}
			if(!group_member){
				logger.error({"r":"check","m":"GET","msg":"not_a_group_member","p":req.query});
				return sendError(res,"Profile is not member of the group.","not_a_member",constants.HTTP_STATUS.BAD_REQUEST);
			}
			if(!ValidAuthRequest.check(group_member.aid,req.decoded)){
				logger.error({"r":"check","m":"GET","msg":"ValidAuthRequest_check_error","p":req.query});
				return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
			}
			return sendSuccess(res, { member : group_member });
		});
	});
});

/*
 * CHECK IF USER IS MEMBER OF THE GROUP
 */
router.post('/check_g', function(req, res, next) {
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('adm_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();

	if(req.validationErrors()){
		logger.error({"r":"check_g","msg":"invalid_parameters","p":req.body}); 
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var group_id 			   = req.body.g_id;
	var admin_profile_id = req.body.adm_id;
	var profile_id 		   = req.body.p_id;

	UserInfoCache.getMongoGrpMem(admin_profile_id, group_id, function(err, admin_group_member){
		if(err) { 
			logger.error({"r":"check_g","msg":"getMongoGrpMem_DbError","p":req.body});
			return sendError(res,err,"server_error"); 
		}
		if(!admin_group_member){
			logger.error({"r":"check_g","msg":"not_a_group_member","p":req.body});
			return sendError(res,"Profile is not member of the group.","not_a_member",constants.HTTP_STATUS.BAD_REQUEST);
		}
		if(req.decoded.id != admin_group_member.aid){
			logger.error({"r":"check_g","msg":"token_id_not_matching_with_admin_profile_aid","p":req.body});
			return sendError(res,"not_authorized","not_authorized",constants.HTTP_STATUS.UNAUTHORIZED);
		}
		if(admin_group_member.type != GroupMember.TYPE.ADMIN){
			logger.error({"r":"check_g","msg":"admin_id_not_a_group_admin","p":req.body});
			return sendError(res,"You are not Group Administrator","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
		}
		UserInfoCache.getMongoGrpMem(profile_id, group_id, function(err, group_member){
			if(err) { 
				logger.error({"r":"check_g","msg":"getMongoGrpMem_DbError","p":req.body});
				return sendError(res,err,"server_error"); 
			}
			if(!group_member){
				logger.error({"r":"check_g","msg":"profile_id_not_a_group_member","p":req.body});
				return sendError(res,"Profile is not member of the group.","not_a_member",constants.HTTP_STATUS.BAD_REQUEST);
			}
			return sendSuccess(res, { member : group_member });
		});
	});
});

/*
 * ADD MEMBER
 */
router.post('/addmember', function(req, res, next) {
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('admin_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('tokn',errorCodes.invalid_parameters[1]).notEmpty().validateToken();

	if(req.validationErrors()){
		logger.error({"r":"addmember","m":"POST","msg":"invalid_parameters","p":req.body}); 
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var group_id 		  = req.body.g_id;
	var admin_id	 	  = req.body.admin_id;
	var profile_id 		= req.body.p_id;

	AccountInfo.validateVersionForAdminActions(profile_id, function(err,resp){
		if(err){ 
			logger.error({"r":"addmember","m":"POST","msg":"validateVersion_DbError","p":req.body}); 
			return sendError(res,err,"server_error");
		}
		if(!resp){ 
			logger.error({"r":"addmember","m":"POST","msg":"lower_mobile_app_version","p":req.body});
			return sendError(res,"Target User needs to upgrade the app version","lower_mobile_app_version");
		} 
		GroupInfoCache.getMongoGroup(group_id,function(err,group_check){
			if(err){ 
				logger.error({"r":"addmember","m":"POST","msg":"validateVersion_DbError","p":req.body}); 
				return sendError(res,err,"server_error");
			}
			if(!group_check){
				logger.error({"r":"addmember","m":"POST","msg":"no_group_found","p":req.body}); 
				return sendError(res,"Group does not exists.","no_group_found",constants.HTTP_STATUS.NOT_FOUND);
			}
			GroupMember.findOne({
				gid : group_id,
				pid : admin_id,
				act : true
			},function(err, admin_check){
				if(err){ 
					logger.error({"r":"addmember","m":"POST","msg":"groupMember_server_error","p":req.body}); 
					return sendError(res,err,"server_error");
				}
				if(!admin_check) {
					logger.error({"r":"addmember","m":"POST","msg":"not_a_group_member","p":req.body});
					return sendError(res,"Profile is not member of the group.","not_a_member",constants.HTTP_STATUS.BAD_REQUEST);
				}
				if(!ValidAuthRequest.check(admin_check.aid,req.decoded)){
					logger.error({"r":"addmember","m":"GET","msg":"ValidAuthRequest_check_error","p":req.query});
					return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
				}
				if(admin_check.type != GroupMember.TYPE.ADMIN){
					logger.error({"r":"addmember","m":"POST","msg":"not_a_group_admin","p":req.body});
					return sendError(res,"User not an Administrator of the Group","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
				}
				Profile.findOne({
					_id : profile_id,
					act : true
				},function(err,member) {
					if(err){ 
						logger.error({"r":"addmember","m":"POST","msg":"profiles_server_error","p":req.body}); 
						return sendError(res,err,"server_error");
					}
					if(!member){
						logger.error({"r":"addmember","m":"POST","msg":"profile_not_found","p":req.body});
						return sendError(res,"Profile not found.","profile_not_found",constants.HTTP_STATUS.NOT_FOUND);
					}
					GroupMember.findOne({
						gid : group_id,
						pid : profile_id,
						act : true
					},function(err, member_check){
						if(err){ 
							logger.error({"r":"addmember","m":"POST","msg":"grpmember_profile_server_error","p":req.body}); 
							return sendError(res,err,"server_error");
						}
						if(member_check){
							logger.error({"r":"addmember","m":"POST","msg":"already_member","p":req.body}); 
							return sendError(res,"Profile is already a member","already_member",constants.HTTP_STATUS.BAD_REQUEST);
						}
						else {
							var new_member = new GroupMember({
								gid : group_id,
								pid : profile_id,
								aid : member.aid,
								type : GroupMember.TYPE.MEMBER,
								act : true
							});
							new_member.save(function(err, new_member){
								if(err){ 
									logger.error({"r":"addmember","m":"POST","msg":"newMember_add_server_error","p":req.body}); 
									return sendError(res,err,"server_error");
								}
								return sendSuccess(res, {data : new_member});
							});
						}
					});
				});
			});
		});
	});
});


/*
 * JOIN A GROUP ON POSSESSION OF A GROUP CODE
 */
router.post('/joingroup', function(req, res, next) {
	req.checkBody('code',errorCodes.invalid_parameters[1]).notEmpty().isValidGroupCode();
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('tokn',errorCodes.invalid_parameters[1]).notEmpty().validateToken();

	if(req.validationErrors()){
		logger.error({"r":"joingroup","m":"POST","msg":"invalid_parameters","p":req.body});  
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var group_code = req.body.code;
	var profile_id = req.body.p_id;
	var tim        =  new Date().getTime();

	UserInfoCache.getMongoProfile(profile_id,function(err, profile){
		if(err){ 
			logger.error({"r":"joingroup","m":"POST","msg":"profiles_server_error","p":req.body});
			return sendError(res,err,"server_error");
		}
		if(!profile){
			logger.error({"r":"joingroup","m":"POST","msg":"profile_not_found","p":req.body});
			return sendError(res,"Profile not found.","profile_not_found",constants.HTTP_STATUS.NOT_FOUND);
		}
		if(!ValidAuthRequest.check(profile.aid,req.decoded)){
			logger.error({"r":"joingroup","m":"POST","msg":"ValidAuthRequest_check_error","p":req.body});
			return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}
		GroupJoinLimits.check(profile.aid, tim, function(err,bruteForceGrpJn){
			if(err){
				logger.error({"r":"joingroup","m":"POST","msg":"ValidAuthRequest_check_error","p":req.body});
				return sendError(res,"Profile not found.","profile_not_found",constants.HTTP_STATUS.NOT_FOUND); 
			}
			if(bruteForceGrpJn){
				bruteForceGrpJnlogger.error({"r":"joingroup_brute_force_found","p":{aid:profile.aid,lmt:bruteForceGrpJn.lmt,tm:bruteForceGrpJn.tm}});
				return sendError(res,"Too many requests","too_many_requests",constants.HTTP_STATUS.TOO_MANY_REQUESTS);
			}
			GroupJoinLimits.mark(profile.aid, tim, function(err,marked){
				if(err){
					logger.error({"r":"joingroup","m":"POST","msg":"GroupJoinLimits_mark_error","p":req.body});
					return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST); 
				}
				Group.findOne({
					code : group_code,
					act  : true
				},{
					__v : 0,
					cat : 0
				},function(err, group){
					if(err){ 
						logger.error({"r":"joingroup","m":"POST","msg":"groups_server_error","p":req.body});
						return sendError(res,err,"server_error");
					}
					if(!group){
						GroupJoinLimits.markError(profile.aid, tim, function(err,marked){
							if(err){
								logger.error({"r":"joingroup","m":"POST","msg":"GroupJoinLimits_markError_error","p":req.body});
							}
						});
						logger.error({"r":"joingroup","m":"POST","msg":"no_group_found","p":req.body});
						return sendError(res,"Group does not exists.","no_group_found",constants.HTTP_STATUS.NOT_FOUND);
					}
					if(group.secr && !req.decoded){
						GroupJoinLimits.markError(profile.aid, tim, function(err,marked){
							if(err){
								logger.error({"r":"joingroup","m":"POST","msg":"GroupJoinLimits_markError_error","p":req.body});
							}
						});
						secureGroupLogger.error({"r":"joingroup","m":"POST","msg":"secure_group_not_authorised","p":req.body});
						logger.error({"r":"joingroup","m":"POST","msg":"secure_group_not_authorised","p":req.body});
						return sendError(res,err,"server_error");
					}
					Account.findOne({
						_id : profile.aid,
						vrfy: true
					},{
						vrsn : 1,
						iosv : 1
					},function(err,account){
						if(err){
							logger.error({"r":"joingroup","m":"POST","msg":"GroupJoinLimits_mark_error","p":req.body});
							return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST); 
						}
						if(!account){
							logger.error({"r":"joingroup","msg":"no_account_found","p":req.body});
							return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST); 
						}
						var client = (!req.decoded || !req.decoded.cl) ? null : req.decoded.cl;
						if(!isValidVersionForPaidGroup("joingroup" ,group.req_sub, account, client)){
							logger.error({"r":"joingroup","msg":"isValidVersionForPaidGroup_Failed","p":req.body});
							return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
						}
						var is_already_joined = false;
						var index = 0;
						GroupMember.find({
							gid : group._id,
							aid : profile.aid,
							act : true,
							gdel: false
						},function(err, members){
							if(err){ 
								logger.error({"r":"joingroup","m":"POST","msg":"groupmember_server_error","p":req.body});
								return sendError(res,err,"server_error");
							}
							var total_mems = members.length;
							for(var i=0; i<total_mems; i++){
								if(members[i].type == GroupMember.TYPE.BANNED){
									logger.error({"r":"joingroup","m":"POST","msg":"banned_group_member","p":req.body});
									return sendError(res,"Banned group member cannot join the group","banned_group_member",constants.HTTP_STATUS.BANNED_GROUP_MEMBER);
								}
								if(members[i].pid+'' == profile_id){
									is_already_joined = true;
									index = i;
								}
							}
							if(is_already_joined){
								logger.warn({"r":"joingroup","m":"POST","msg":"already_member","p":req.body});
								return alreadyJoinedGroupResponse(res, "joingroup", group, members[index], tim);
							}
							var gpaid = 0;
							if(group.req_sub && group.req_sub == true){
								gpaid = 1;
							}
							var new_member = new GroupMember({
								gid : group._id,
								pid : profile_id,
								type : GroupMember.TYPE.MEMBER,
								pnam : profile.nam.toLowerCase(),
								act : true
							});
							GroupMember.update({
								gid : group._id,
								pid : profile_id,
								act : true
							},{
								$setOnInsert : {
									gid : group._id,
									pid : profile_id,
									aid : profile.aid,
									type : GroupMember.TYPE.MEMBER,
									pnam : profile.nam.toLowerCase(),
									gpaid: gpaid,
									act : true,
									cat : new Date
								}
							},{
								upsert : true,
								setDefaultsOnInsert: true
							},function(err,gMem){
								if(err){ 
									logger.error({"r":"joingroup","m":"POST","msg":"groupmember_save_server_error","p":req.body});
									return sendError(res,err,"server_error");
								}
								removeCacheForGroupMemJoin("joingroup", profile_id, group._id, req.body, function(err,isRemoved){
									if(err){
										logger.error({"r":"joingroup","msg":"removeCacheForGroupMemJoin_error","p":profile_id,"er":err});
									}
									if(group.req_sub && group.req_sub == true && group.plugin_id){
										GroupPluginPackage.find({
											gid       : group._id,
											plugin_id : group.plugin_id,
											act       : true
										},{
											plugin_id        : 1,
											package_name     : 1,
											package_desc     : 1,
											package_price    : 1,
											package_duration : 1,
											duration_unit    : 1,
											duration_base    : 1,
											acc              : 1
										},function(err,plugin_packages){
											if(err){
												logger.error({"r":"joingroup","msg":"GroupPluginPackageFind_DbError","p":req.body,"err":err,"grp":group});
												return sendError(res,"server_error","server_error");
											}
											logger.info({"r":"joingroup","msg":"joingroup_Success","ispaid":group.req_sub,"packs":plugin_packages});
											sendSuccess(res, {"Group":group,"GroupMember":new_member,ispaid:group.req_sub,packs:plugin_packages,CC_DC:1});
										});
									} else {
										sendSuccess(res, {"Group":group,"GroupMember":new_member});
									}
									return GroupNotify.abt_grp_j(group._id, profile_id, group.code, group.name, group.gpic, group.act, group.gtyp, new_member.type, tim);
								});
							});
						});
					});
				});
			});
		});
	});
});


/*
 * JOIN A GROUP ON WEB WITH GROUP ID
 */
router.post('/jg_w', function(req, res, next) {
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('tokn',errorCodes.invalid_parameters[1]).notEmpty().validateToken();

	if(req.validationErrors()){
		logger.error({"r":"jg_w","m":"POST","msg":"invalid_parameters","p":req.body});  
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var group_id   = req.body.g_id;
	var profile_id = req.body.p_id;
	var tim        =  new Date().getTime();

	UserInfoCache.getMongoProfile(profile_id,function(err, profile){
		if(err){ 
			logger.error({"r":"jg_w","m":"POST","msg":"profiles_server_error","p":req.body});
			return sendError(res,err,"server_error");
		}
		if(!profile){
			logger.error({"r":"jg_w","m":"POST","msg":"profile_not_found","p":req.body});
			return sendError(res,"Profile not found.","profile_not_found",constants.HTTP_STATUS.NOT_FOUND);
		}
		if(profile.aid != req.decoded.id){
			logger.error({"r":"jg_w","m":"GET","msg":"ValidAuthRequest_check_error","p":req.body});
			return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}
		GroupJoinLimits.check(profile.aid, tim, function(err,bruteForceGrpJn){
			if(err){
				logger.error({"r":"jg_w","m":"GET","msg":"ValidAuthRequest_check_error","p":req.body});
				return sendError(res,err,"server_error"); 
			}
			if(bruteForceGrpJn){
				bruteForceGrpJnlogger.error({"r":"jg_w_brute_force_found","p":{aid:profile.aid,lmt:bruteForceGrpJn.lmt,tm:bruteForceGrpJn.tm}});
				return sendError(res,"Too many requests","too_many_requests",constants.HTTP_STATUS.TOO_MANY_REQUESTS);
			}
			GroupJoinLimits.mark(profile.aid, tim, function(err,marked){
				if(err){
					logger.error({"r":"jg_w","m":"POST","msg":"GroupJoinLimits_mark_error","p":req.body});
					return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST); 
				}
				Group.findOne({
					_id : group_id,
					act  : true
				},{
					__v : 0,
					cat : 0
				},function(err, group){
					if(err){ 
						logger.error({"r":"jg_w","m":"POST","msg":"groups_server_error","p":req.body});
						return sendError(res,err,"server_error");
					}
					if(!group){
						logger.error({"r":"jg_w","m":"POST","msg":"no_group_found","p":req.body});
						return sendError(res,"Group does not exists.","no_group_found",constants.HTTP_STATUS.NOT_FOUND);
					}
					if(group.req_sub && group.req_sub == true){
						logger.error({"r":"jg_w","m":"POST","msg":"Paid_GroupJoin_Not_Allowed_From_WebClients","p":req.body});
						return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST); 
					}
					if(group.secr && !req.decoded){
						secureGroupLogger.error({"r":"jg_w","m":"POST","msg":"secure_group_not_authorised","p":req.body});
						logger.error({"r":"jg_w","m":"POST","msg":"group_secure_token_req","p":req.body});
						return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
					}
					var is_already_joined = false;
					var index = 0;
					GroupMember.find({
						gid : group._id,
						aid : profile.aid,
						act : true,
						gdel: false
					},function(err, members){
						if(err){ 
							logger.error({"r":"jg_w","m":"POST","msg":"groupmember_server_error","p":req.body});
							return sendError(res,err,"server_error");
						}
						var total_mems = members.length;
						for(var i=0; i<total_mems; i++){
							if(members[i].type == GroupMember.TYPE.BANNED){
								logger.error({"r":"jg_w","m":"POST","msg":"banned_group_member","p":req.body});
								return sendError(res,"Banned group member cannot join the group","banned_group_member",constants.HTTP_STATUS.BANNED_GROUP_MEMBER);
							}
							if(members[i].pid+'' == profile_id){
								is_already_joined = true;
								index = i;
							}
						}
						if(is_already_joined){
							logger.warn({"r":"jg_w","m":"POST","msg":"already_member","p":req.body});
							return alreadyJoinedGroupResponse(res, "jg_w", group, members[index], tim);
						}
						var gpaid = 0;
						if(group.req_sub && group.req_sub == true){
							gpaid = 1;
						}
						var new_member = new GroupMember({
							gid : group._id,
							pid : profile_id,
							type : GroupMember.TYPE.MEMBER,
							pnam : profile.nam.toLowerCase(),
							act : true
						});
						GroupMember.update({
							gid : group._id,
							pid : profile_id,
							act : true
						},{
							$setOnInsert : {
								gid : group._id,
								pid : profile_id,
								aid : profile.aid,
								type : GroupMember.TYPE.MEMBER,
								pnam : profile.nam.toLowerCase(),
								gpaid: gpaid,
								act : true,
								cat : new Date
							}
						},{
							upsert : true,
							setDefaultsOnInsert: true
						},function(err,gMem){
							if(err){ 
								logger.error({"r":"jg_w","m":"POST","msg":"groupmember_save_server_error","p":req.body});
								return sendError(res,err,"server_error");
							}
							removeCacheForGroupMemJoin("jg_w", profile_id, group._id, req.body, function(err,isRemoved){
								if(err){
									logger.error({"r":"jg_w","msg":"removeCacheForGroupMemJoin_error","p":profile_id,"er":err});
								}
								if(group.req_sub && group.req_sub == true && group.plugin_id){
									GroupPluginPackage.find({
										gid       : group._id,
										plugin_id : group.plugin_id,
										act       : true
									},{
										plugin_id        : 1,
										package_name     : 1,
										package_desc     : 1,
										package_price    : 1,
										package_duration : 1,
										duration_unit    : 1,
										duration_base    : 1,
										acc              : 1
									},function(err,plugin_packages){
										if(err){
											logger.error({"r":"jg_w","msg":"GroupPluginPackageFind_DbError","p":req.body,"err":err,"grp":group});
											return sendError(res,"server_error","server_error");
										}
										logger.info({"r":"jg_w","msg":"joingroup_Success","ispaid":group.req_sub,"packs":plugin_packages});
										sendSuccess(res, {"Group":group,"GroupMember":new_member,ispaid:group.req_sub,packs:plugin_packages,CC_DC:1});
									});
								} else {
									sendSuccess(res, {"Group":group,"GroupMember":new_member});
								}
								AccountInfo.validateVersionForAdminActions(profile_id, function(err, resp){
									if(resp) {
										return GroupNotify.abt_grp_j(group._id, profile_id, group.code, group.name, group.gpic, group.act, group.gtyp, new_member.type, tim);
									}
								});
							});
						});
					});
				});
			});
		});
	});
});

/*
 * JOIN a Group Which is in the recommended list
 */
router.post('/jg_rcmd', function(req, res, next) {
	req.checkBody('gid',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();

	if(req.validationErrors()){
		logger.error({"r":"jg_rcmd","m":"POST","msg":"invalid_parameters","p":req.body});  
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var group_id   = req.body.gid;
	var profile_id = req.body.p_id;
	var tim        =  new Date().getTime();

	UserInfoCache.getMongoProfile(profile_id,function(err, profile){
		if(err){ 
			logger.error({"r":"jg_rcmd","m":"POST","msg":"profiles_server_error","p":req.body});
			return sendError(res,err,"server_error");
		}
		if(!profile){
			logger.error({"r":"jg_rcmd","m":"POST","msg":"profile_not_found","p":req.body});
			return sendError(res,"Profile not found.","profile_not_found",constants.HTTP_STATUS.NOT_FOUND);
		}
		if(!ValidAuthRequest.check(profile.aid,req.decoded)){
			logger.error({"r":"jg_rcmd","m":"GET","msg":"ValidAuthRequest_check_error","p":req.body});
			return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}
		GroupJoinLimits.check(profile.aid, tim, function(err,bruteForceGrpJn){
			if(err){
				logger.error({"r":"jg_rcmd","m":"GET","msg":"ValidAuthRequest_check_error","p":req.body});
				return sendError(res,"Profile not found.","profile_not_found",constants.HTTP_STATUS.NOT_FOUND); 
			}
			if(bruteForceGrpJn){
				bruteForceGrpJnlogger.error({"r":"jg_rcmd_brute_force_found","p":{aid:profile.aid,lmt:bruteForceGrpJn.lmt,tm:bruteForceGrpJn.tm}});
				return sendError(res,"Too many requests","too_many_requests",constants.HTTP_STATUS.TOO_MANY_REQUESTS);
			}
			GroupJoinLimits.mark(profile.aid, tim, function(err,marked){
				if(err){
					logger.error({"r":"jg_rcmd","m":"GET","msg":"GroupJoinLimits_mark_error","p":req.body});
					return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST); 
				}
				GroupCategory.findOne({
					gid : group_id
				},function(err,rcmd_grp){
					if(err){
						logger.error({"r":"jg_rcmd","m":"POST","msg":"GroupCategory_server_error","p":req.body});
						return sendError(res,err,"server_error");
					}
					if(!rcmd_grp){
						logger.error({"r":"jg_rcmd","m":"POST","msg":"no_GroupCategory_found","p":req.body});
						return sendError(res,"Group does not exists.","no_group_found",constants.HTTP_STATUS.NOT_FOUND);
					}
					Group.findOne({
						_id : group_id,
						act : true
					},{
						__v : 0,
						cat : 0
					},function(err, group){
						if(err){ 
							logger.error({"r":"jg_rcmd","m":"POST","msg":"groups_server_error","p":req.body});
							return sendError(res,err,"server_error");
						}
						if(!group){
							logger.error({"r":"jg_rcmd","m":"POST","msg":"no_group_found","p":req.body});
							return sendError(res,"Group does not exists.","no_group_found",constants.HTTP_STATUS.NOT_FOUND);
						}
						if(group.secr && !req.decoded){
							secureGroupLogger.error({"r":"jg_rcmd","m":"POST","msg":"secr_group_not_authorized","p":req.body});
							logger.error({"r":"jg_rcmd","m":"POST","msg":"group_secure_token_req","p":req.body});
							return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
						}
						Account.findOne({
							_id : profile.aid,
							vrfy: true
						},{
							vrsn : 1,
							iosv : 1
						},function(err,account){
							if(err){
								logger.error({"r":"jg_rcmd","m":"POST","msg":"GroupJoinLimits_mark_error","p":req.body});
								return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST); 
							}
							if(!account){
								logger.error({"r":"jg_rcmd","msg":"no_account_found","p":req.body});
								return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST); 
							}
							var client = (!req.decoded || !req.decoded.cl) ? null : req.decoded.cl;
							if(!isValidVersionForPaidGroup("jg_rcmd" ,group.req_sub, account, client)){
								logger.error({"r":"jg_rcmd","msg":"isValidVersionForPaidGroup_Failed","p":req.body});
								return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
							}
							var is_already_joined = false;
							var index = 0;
							GroupMember.find({
								gid : group._id,
								aid : profile.aid,
								act : true,
								gdel: false
							},function(err, members){
								if(err){ 
									logger.error({"r":"jg_rcmd","m":"POST","msg":"groupmember_server_error","p":req.body});
									return sendError(res,err,"server_error");
								}
								var total_mems = members.length;
								for(var i=0; i<total_mems; i++){
									if(members[i].type == GroupMember.TYPE.BANNED){
										logger.error({"r":"jg_rcmd","m":"POST","msg":"banned_group_member","p":req.body});
										return sendError(res,"Banned group member cannot join the group","banned_group_member",constants.HTTP_STATUS.BANNED_GROUP_MEMBER);
									}
									if(members[i].pid+'' == profile_id){
										is_already_joined = true;
										index = i;
									}
								}
								if(is_already_joined){
									logger.warn({"r":"jg_rcmd","m":"POST","msg":"already_member","p":req.body});
									return alreadyJoinedGroupResponse(res, "jg_rcmd", group, members[index], tim);
								}
								var gpaid = 0;
								if(group.req_sub && group.req_sub == true){
									gpaid = 1;
								}
								var new_member = new GroupMember({
									gid  : group._id,
									pid  : profile_id,
									type : GroupMember.TYPE.MEMBER,
									pnam : profile.nam.toLowerCase(),
									act  : true
								});
								GroupMember.update({
									gid : group._id,
									pid : profile_id,
									act : true
								},{
									$setOnInsert : {
										gid  : group._id,
										pid  : profile_id,
										aid  : profile.aid,
										type : GroupMember.TYPE.MEMBER,
										pnam : profile.nam.toLowerCase(),
										gpaid: gpaid,
										act  : true,
										rcmd : true,
										cat  : new Date
									}
								},{
									upsert : true,
									setDefaultsOnInsert: true
								},function(err,gMem){
									if(err){ 
										logger.error({"r":"jg_rcmd","m":"POST","msg":"groupmember_save_server_error","p":req.body});
										return sendError(res,err,"server_error");
									}
									removeCacheForGroupMemJoin("jg_rcmd", profile_id, group._id, req.body, function(err,isRemoved){
										if(err){
											logger.error({"r":"jg_rcmd","msg":"removeCacheForGroupMemJoin_error","p":profile_id});
										}
										if(group.req_sub && group.req_sub == true && group.plugin_id){
											GroupPluginPackage.find({
												gid       : group._id,
												plugin_id : group.plugin_id,
												act       : true
											},{
												plugin_id        : 1,
												package_name     : 1,
												package_desc     : 1,
												package_price    : 1,
												package_duration : 1,
												duration_unit    : 1,
												duration_base    : 1,
												acc              : 1
											},function(err,plugin_packages){
												if(err){
													logger.error({"r":"jg_rcmd","msg":"GroupPluginPackageFind_DbError","p":req.body,"err":err,"grp":group});
													return sendError(res,"server_error","server_error");
												}
												logger.info({"r":"jg_rcmd","msg":"joingroup_Success","ispaid":group.req_sub,"packs":plugin_packages});
												sendSuccess(res, {"Group":group,"GroupMember":new_member,ispaid:group.req_sub,packs:plugin_packages,CC_DC:1});
											});
										} else {
											sendSuccess(res, {"Group":group,"GroupMember":new_member});
										}
										AccountInfo.validateVersionForAdminActions(profile_id, function(err, resp){
											if(resp) {
												return GroupNotify.abt_grp_j(group._id, profile_id, group.code, group.name, group.gpic, group.act, group.gtyp, new_member.type, tim);
											}
										});
									});
								});
							});
						});
					});
				});
			});
		});
	});
});

/*
 *Add New Admins in Group
 */
router.put('/gadm_a', function(req, res, next) {
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();  // User to be given the admin rights
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('admin_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId(); // User who is performing the action
	req.checkBody('tokn',errorCodes.invalid_parameters[1]).notEmpty().validateToken();

	if(req.validationErrors()){ 
		logger.error({"r":"gadm_a","m":"PUT","msg":"invalid_parameters","p":req.body});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var admin_id 	    = req.body.admin_id;
	var group_id 	    = req.body.g_id;
	var profile_id    = req.body.p_id;

	AccountInfo.validateVersionForAdminActions(profile_id, function(err,resp){
		if(err){ 
			logger.error({"r":"gadm_a","m":"PUT","msg":"validateVersion_DbError","p":req.body});
			return sendError(res,err,"server_error");
		}
		if(!resp){ 
			logger.error({"r":"gadm_a","m":"PUT","msg":"lower_mobile_app_version","p":req.body});
			return sendError(res,"Target User needs to upgrade the app version","lower_mobile_app_version");
		}
		GroupInfoCache.getMongoGroup(group_id,function(err,group){
			if(err){ 
				logger.error({"r":"gadm_a","m":"PUT","msg":"group_found_server_error","p":req.body});
				return sendError(res,err,"server_error");
			}
			if(!group){
				logger.error({"r":"gadm_a","m":"PUT","msg":"no_group_found","p":req.body})
				return sendError(res,"No Group Found","no_group_found",constants.HTTP_STATUS.BAD_REQUEST);
			}
			if(group.secr && !req.decoded){
				secureGroupLogger.error({"r":"gadm_a","m":"PUT","msg":"secr_group_not_authorized","p":req.body});
				logger.error({"r":"gadm_a","m":"PUT","msg":"secr_group_not_authorized","p":req.body});
				return sendError(res,"not_authorized","not_authorized",constants.HTTP_STATUS.UNAUTHORIZED);
			}
			GroupMember.find({
				gid : group_id,
				pid : { $in : [admin_id,profile_id] },
				act : true,
				gdel: false
			},function(err, members) {
				if(err){ 
					logger.error({"r":"gadm_a","m":"PUT","msg":"groupMember_found_server_error","p":req.body});
					return sendError(res,err,"server_error");
				}
				if(members.length !=2){
					logger.error({"r":"gadm_a","m":"PUT","msg":"groupmembers_not_found","p":req.body});
					return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
				}
				var admin , new_admin;
				for (var i = 0; i < 2 ; i++) {
					if(members[i].pid == admin_id) 
						admin = members[i];
					else if (members[i].pid == profile_id){
						new_admin = members[i];
					}
				}
				if(!admin){
					logger.error({"r":"gadm_a","m":"PUT","msg":"not_a_group_member","p":req.body});
					return sendError(res,"You are not a member of the Group","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
				}
				if(!ValidAuthRequest.check(admin.aid,req.decoded)){
					logger.error({"r":"gadm_a","m":"PUT","msg":"ValidAuthRequest_check_error","p":req.body});
					return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
				}
				if(admin.type != GroupMember.TYPE.ADMIN){
					logger.error({"r":"gadm_a","m":"PUT","msg":"not_a_group_admin","p":req.body});
					return sendError(res,"You are not Group Administrator","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
				}
				if(!new_admin){
					logger.error({"r":"gadm_a","m":"PUT","msg":"user_not_group_member","p":req.body});
					return sendError(res,"User being added as an admin is not a member of the Group","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
				}
				if(new_admin.type == GroupMember.TYPE.ADMIN){
					logger.error({"r":"gadm_a","m":"PUT","msg":"user_already_group_admin","p":req.body});
					return sendError(res,"User being added as an admin is already an admin of the Group","already_administrator",constants.HTTP_STATUS.BAD_REQUEST);
				}
				if(new_admin.type == GroupMember.TYPE.BANNED){
					logger.error({"r":"gadm_a","m":"PUT","msg":"banned_group_member","p":req.body});
					return sendError(res,"Banned group member cannot become group admin","banned_group_member",constants.HTTP_STATUS.NOT_FOUND);
				}
				GroupMember.update({
					_id : new_admin._id,
				},{
					type : GroupMember.TYPE.ADMIN
				},function(err,admin_updated) {
					if(err){ 
						logger.error({"r":"gadm_a","m":"PUT","msg":"groupMember_update_server_error","p":req.body});
						return sendError(res,err,"server_error");
					}
					rmGrpMemsFromUsrCache(profile_id,function(err,isRemoved){
						if(err){
							logger.warn({"r":"gadm_a","m":"PUT","msg":"rmGrpMemsFromUsrCache_server_error","p":req.body});
						}
						rmGrpAdmsFromGrpCache(group_id,function(err,isRemoved){
							if(err){
								logger.warn({"r":"gadm_a","m":"PUT","msg":"rmGrpAdmsFromGrpCache_server_error","p":req.body});
							}
							UserInfoCache.removeMongoGrpMem(profile_id, group_id,function(err, rRemoved){
								if(err){
									logger.warn({"r":"gadm_a","m":"PUT","msg":"removeMongoGrpMem_server_error","p":req.body});
								}
								var time = new Date().getTime();
								sendSuccess(res,{"GroupMember" : admin_updated});
								notify_admin_ad_rm(group_id, admin_id, profile_id, Actions.GROUP_ADMIN_ADD, group.name, group.gtyp, time);
							});
						});
					});
				});
			});
		});
	});
});


/*
 *Remove an Admin From the Group
 */
router.put('/gadm_r', function(req, res, next) {
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();  // User whose admin rights ae being removed
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('admin_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId(); // User who is performing this action
	req.checkBody('tokn',errorCodes.invalid_parameters[1]).notEmpty().validateToken();

	if(req.validationErrors()){ 
		logger.error({"r":"gadm_r","m":"PUT","msg":"invalid_parameters","p":req.body});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var admin_id 	  = req.body.admin_id;
	var group_id 	  = req.body.g_id;
	var profile_id  = req.body.p_id;
	
	AccountInfo.validateVersionForAdminActions(profile_id, function(err, resp){
		if(err){
		 	logger.error({"r":"gadm_r","m":"PUT","msg":"validateVersion_DbError","p":req.body});
			return sendError(res,err,"server_error");
		}
		if(!resp){ 
			logger.error({"r":"gadm_r","m":"PUT","msg":"lower_mobile_app_version","p":req.body});
			return sendError(res,"Target User needs to upgrade the app version","lower_mobile_app_version");
		}
		GroupInfoCache.getMongoGroup(group_id,function(err,group){
			if(err){
			 	logger.error({"r":"gadm_r","m":"PUT","msg":"groupMember_find_server_error","p":req.body});
				return sendError(res,err,"server_error");
			}
			if(!group){
				logger.error({"r":"gadm_r","m":"PUT","msg":"no_group_found","p":req.body});
				return sendError(res,"No Group Found","no_group_found",constants.HTTP_STATUS.BAD_REQUEST);
			}
			if(group.secr && !req.decoded){
				secureGroupLogger.error({"r":"gadm_r","m":"PUT","msg":"secr_group_not_authorized","p":req.body});
				logger.error({"r":"gadm_r","m":"PUT","msg":"secr_group_not_authorized","p":req.body});
				return sendError(res,"not_authorized","not_authorized",constants.HTTP_STATUS.UNAUTHORIZED);
			}
			GroupMember.find({
				gid : group_id,
				pid : { $in : [admin_id,profile_id]},
				act : true,
				gdel: false
			},function(err, members) {
				if(err){
				 	logger.error({"r":"gadm_r","m":"PUT","msg":"validateVersion_DbError","p":req.body});
					return sendError(res,err,"server_error");
				}
				if(members.length !=2){
					logger.error({"r":"gadm_r","m":"PUT","msg":"groupmembers_not_found","p":req.body});
					return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
				}
				var admin , admin_to_remove;
				for (var i = 0; i < 2 ; i++) {
					if(members[i].pid == admin_id) 
						admin = members[i];
					else if (members[i].pid == profile_id){
						admin_to_remove 		= members[i];
					}
				}
				if(!admin){
					logger.error({"r":"gadm_r","m":"PUT","msg":"not_group_member","p":req.body});
					return sendError(res,"You are not a member of the Group","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
				}
				if(!ValidAuthRequest.check(admin.aid,req.decoded)){
					logger.error({"r":"gadm_r","m":"PUT","msg":"ValidAuthRequest_check_error","p":req.body});
					return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
				}
				if(admin.type != GroupMember.TYPE.ADMIN){
					logger.error({"r":"gadm_r","m":"PUT","msg":"not_a_group_admin","p":req.body});
					return sendError(res,"You are not Group Administrator","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
				}
				if(!admin_to_remove){
					logger.error({"r":"gadm_r","m":"PUT","msg":"user_not_group_member","p":req.body});
					return sendError(res,"User being removed from admin is not a member of the Group","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
				}

				if(admin_to_remove.type != GroupMember.TYPE.ADMIN){
					logger.error({"r":"gadm_r","m":"PUT","msg":"user_not_group_admin","p":req.body});
					return sendError(res,"User Being removed from admin is not an admin","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
				}
				GroupMember.count({
					gid : group_id,
					act : true
				},function(err,total_admins_count){
					if(err){
					 	logger.error({"r":"gadm_r","m":"PUT","msg":"groupMember_count_server_error","p":req.body});
						return sendError(res,err,"server_error");
					}
					if(total_admins_count < 2){
						logger.error({"r":"gadm_r","m":"PUT","msg":"group_single_admin","p":req.body});
						return sendError(res,"Group has only one admin active at the moment","group_single_admin",constants.HTTP_STATUS.BAD_REQUEST);
					}
					GroupMember.update({
						_id : admin_to_remove._id,
						act : true
					},{
						type : GroupMember.TYPE.MEMBER
					},function(err,removed_group_admin) {
						if(err){
						 	logger.error({"r":"gadm_r","m":"PUT","msg":"groupMember_update_server_error","p":req.body});
							return sendError(res,err,"server_error");
						}
						rmGrpMemsFromUsrCache(profile_id,function(err,isRemoved){
							if(err){
								logger.warn({"r":"gadm_r","m":"PUT","msg":"rmGrpMemsFromUsrCache_server_error","p":req.body});
							}
							rmGrpAdmsFromGrpCache(group_id,function(err,isRemoved){
								if(err){
									logger.warn({"r":"gadm_r","m":"PUT","msg":"rmGrpAdmsFromGrpCache_server_error","p":req.body});
								}
								UserInfoCache.removeMongoGrpMem(profile_id, group_id,function(err, rRemoved){
									if(err){
										logger.warn({"r":"gadm_r","m":"PUT","msg":"removeMongoGrpMem_server_error","p":req.body});
									}
									var time = new Date().getTime();
									sendSuccess(res, {data : removed_group_admin});
									notify_admin_ad_rm(group_id, admin_id, profile_id, Actions.GROUP_ADMIN_REM, group.name, group.gtyp, time);
								});
							});
						});
					});
				});
			});
		});
	});
});


/*
 * BAN A MEMBER : 1=>ADMIN 2=>MEMBER 3=>BANNED
 */
router.put('/gmem_b', function(req, res, next) {
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('admin_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('tokn',errorCodes.invalid_parameters[1]).notEmpty().validateToken();

	if(req.validationErrors()){
		logger.error({"r":"gmem_b","m":"PUT","msg":"invalid_parameters","p":req.body}); 
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var time          = new Date().getTime();
	var group_id 		  = req.body.g_id;
	var admin_id 		  = req.body.admin_id;
	var profile_id 		= req.body.p_id;

	if(admin_id == profile_id){
		logger.error({"r":"gmem_b","msg":"admin_id and profile_id are same","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	AccountInfo.validateVersionForAdminActions(profile_id, function(err,resp){
		if(err){ 
			logger.error({"r":"gmem_b","m":"PUT","msg":"validateVersion_DbError","p":req.body});
			return sendError(res,err,"server_error"); 
		}
		if(!resp){ 
			logger.error({"r":"gmem_b","m":"PUT","msg":"lower_mobile_app_version","p":req.body});
			return sendError(res,"Target User needs to upgrade the app version","lower_mobile_app_version"); 
		} 
		GroupInfoCache.getMongoGroup(group_id,function(err,group){
			if(err) { 
				logger.error({"r":"gmem_b","m":"PUT","msg":"getMongoGroup_error","p":req.body});
				return sendError(res,err,"server_error");
			}
			if(!group){
				logger.error({"r":"gmem_b","m":"PUT","msg":"no_group_found","p":req.body});
				return sendError(res,"Group does not exists.","no_group_found",constants.HTTP_STATUS.NOT_FOUND);
			}
			if(group.secr && !req.decoded){
				secureGroupLogger.error({"r":"gmem_b","m":"PUT","msg":"secr_group_not_authorized","p":req.body});
				logger.error({"r":"gmem_b","m":"PUT","msg":"secr_group_not_authorized","p":req.body});
				return sendError(res,"not_authorized","not_authorized",constants.HTTP_STATUS.UNAUTHORIZED);
			}
			UserInfoCache.getMongoProfile(profile_id,function(err,profile){
				if(err){ 
					logger.error({"r":"gmem_b","m":"PUT","msg":"getMongoProfile_server_error","p":req.body});
					return sendError(res,err,"server_error");  
				}
				if(!profile){ 
					logger.error({"r":"gmem_b","m":"PUT","msg":"getMongoProfile_not_found","p":req.body});
					return sendError(res,err,"server_error");  
				}
				GroupMember.find({
					gid : group_id,
					$or : [
					  {pid : admin_id},
						{aid : profile.aid}
					],
					act : true,
					gdel: false
				},function(err,members){
					if(err) { 
						logger.error({"r":"gmem_b","m":"PUT","msg":"groupMember_find_server_error","p":req.body});
						return sendError(res,err,"server_error");  
					}
					if(members.length < 2){
						logger.error({"r":"gmem_b","m":"PUT","msg":"groupmembers_not_found","p":req.body});
						return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
					}
					var members_by_pid        = {};
					var targetProfilePids     = [];
					var targetProfilePidsName = {};
					var total_mems            = members.length;
					var tempPid;
					for(var i=0; i<total_mems; i++){
						tempPid = members[i].pid+'';
						if(tempPid == profile_id || tempPid == admin_id)
							members_by_pid[tempPid] = members[i];
						if(members[i].aid+'' == profile.aid+'' && members[i].type != GroupMember.TYPE.BANNED){
							targetProfilePids.push(tempPid);
							targetProfilePidsName[tempPid] = members[i].pnam;	
						}
					}
					if(!members_by_pid[admin_id]){
						logger.error({"r":"gmem_b","m":"PUT","msg":"not_a_group_member","p":req.body});
						return sendError(res,"User not a member of the Group","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
					}
					if(!ValidAuthRequest.check(members_by_pid[admin_id].aid,req.decoded)){
						logger.error({"r":"gmem_b","m":"PUT","msg":"ValidAuthRequest_check_error","p":req.body});
						return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
					}
					if(!members_by_pid[profile_id]){
						logger.error({"r":"gmem_b","m":"PUT","msg":"profile_not_a_group_member","p":req.body});
						return sendError(res,"Profile to be banned is not member of the group.","not_a_member",constants.HTTP_STATUS.BAD_REQUEST);
					}
					if(members_by_pid[profile_id].aid+'' == members_by_pid[admin_id].aid+''){
						logger.error({"r":"gmem_b","m":"PUT","msg":"cannot_ban_your_own_profile","p":req.body});
						return sendError(res,"Cannot ban your own profile","cannot_ban_your_own_profile",constants.HTTP_STATUS.BAD_REQUEST);
					}
					if(members_by_pid[admin_id].type != GroupMember.TYPE.ADMIN){
						logger.error({"r":"gmem_b","m":"PUT","msg":"not_a_group_admin","p":req.body});
						return sendError(res,"User not an Administrator of the Group","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
					}
					if(members_by_pid[profile_id].type == GroupMember.TYPE.ADMIN){
						logger.error({"r":"gmem_b","m":"PUT","msg":"cannot_ban_administrator","p":req.body});
						return sendError(res,"Cannot ban an Administrator","cannot_ban_administrator",constants.HTTP_STATUS.BAD_REQUEST);
					}
					if(members_by_pid[profile_id].type == GroupMember.TYPE.BANNED){
						logger.error({"r":"gmem_b","m":"PUT","msg":"already_banned","p":req.body});
						return sendError(res,"Profile is already banned","already_banned",constants.HTTP_STATUS.BAD_REQUEST);
					}
					GroupMember.update({
						gid : group_id,
						aid : profile.aid,
						act : true,
					  gdel: false
					},{
						type : GroupMember.TYPE.BANNED
					},{
						multi: true
					},function(err,banned_member){
						if(err){  
							logger.error({"r":"gmem_b","m":"PUT","msg":"groupMember_update_server_error","p":req.body});
							return sendError(res,err,"server_error",constants.HTTP_STATUS.SERVER_ERROR);
						}
						removeCacheForGroupMemBan(profile.aid, targetProfilePids, group_id, req.body, function(err,isRemoved){
							if(err){
								logger.warn({"r":"gmem_b","msg":"removeCacheForGroupMemBan_error","p":req.body,"er":err});
							}
							sendSuccess(res, {data:banned_member});
							return notify_grp_ban_member(group_id, group, admin_id, profile_id, profile.nam, targetProfilePids, targetProfilePidsName, time);
						});
					});
				});
			});
		});
  });
});

/*
 * UNBAN A MEMBER : 1=>ADMIN 2=>MEMBER 3=>BANNED
 */
router.put('/gmem_ub', function(req, res, next) {
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('admin_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('tokn',errorCodes.invalid_parameters[1]).notEmpty().validateToken();

	if(req.validationErrors()){ 
		logger.error({"r":"gmem_ub","m":"PUT","msg":"invalid_parameters","p":req.body});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var group_id 		  = req.body.g_id;
	var admin_id 		  = req.body.admin_id;
	var profile_id 		= req.body.p_id;

	AccountInfo.validateVersionForAdminActions(profile_id, function(err,resp){
		if(err){ 
			logger.error({"r":"gmem_ub","m":"PUT","msg":"validateVersion_DbError","p":req.body});
			return sendError(res,err,"server_error"); 
		}
		if(!resp){
			logger.error({"r":"gmem_ub","m":"PUT","msg":"lower_mobile_app_version","p":req.body}); 
			return sendError(res,"Target User needs to upgrade the app version","lower_mobile_app_version"); 
		} 
		GroupInfoCache.getMongoGroup(group_id,function(err,group){
			if(err) { 
				logger.error({"r":"gmem_ub","m":"PUT","msg":"getMongoGroup_error","p":req.body});
				return sendError(res,err,"server_error");
			}
			if(!group){
				logger.error({"r":"gmem_ub","m":"PUT","msg":"no_group_found","p":req.body});
				return sendError(res,"Group does not exists.","no_group_found",constants.HTTP_STATUS.NOT_FOUND);
			}
			if(group.secr && !req.decoded){
				secureGroupLogger.error({"r":"gmem_ub","m":"PUT","msg":"secr_group_not_authorized","p":req.body});
				logger.error({"r":"gmem_ub","m":"PUT","msg":"secr_group_not_authorized","p":req.body});
				return sendError(res,"not_authorized","not_authorized",constants.HTTP_STATUS.UNAUTHORIZED);
			}
			GroupMember.find({
				gid : group_id,
				pid : { $in : [admin_id,profile_id] },
				act : true,
				gdel: false
			},function(err,members){
				if(err){ 
					logger.error({"r":"gmem_ub","m":"PUT","msg":"groupMember_find_server_error","p":req.body});
					return sendError(res,err,"server_error"); 
				}
				if(members.length !=2){
					logger.error({"r":"gmem_ub","m":"PUT","msg":"groupmembers_not_found","p":req.body});
					return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
				}
				var members_by_pid = {};
				for(var key in members) {
					members_by_pid[members[key]["pid"]] = members[key];
				}
				if(!members_by_pid[admin_id]){
					logger.error({"r":"gmem_ub","m":"PUT","msg":"user_not_group_member","p":req.body});
					return sendError(res,"User not a member of the Group","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
				}
				if(!ValidAuthRequest.check(members_by_pid[admin_id].aid,req.decoded)){
					logger.error({"r":"gmem_ub","m":"PUT","msg":"ValidAuthRequest_check_error","p":req.body});
					return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
				}
				if(!members_by_pid[profile_id]){
					logger.error({"r":"gmem_ub","m":"PUT","msg":"profile_not_a_group_member","p":req.body});
					return sendError(res,"Profile to be promoted is not member of the group.","not_a_member",constants.HTTP_STATUS.BAD_REQUEST);
				}
				if(members_by_pid[admin_id].type != GroupMember.TYPE.ADMIN){
					logger.error({"r":"gmem_ub","m":"PUT","msg":"not_a_group_admin","p":req.body});
					return sendError(res,"User not an Administrator of the Group","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
				}
				if(members_by_pid[profile_id].type == GroupMember.TYPE.MEMBER){
					logger.error({"r":"gmem_ub","m":"PUT","msg":"not_banned","p":req.body});
					return sendError(res,"Profile is not banned","not_banned",constants.HTTP_STATUS.BAD_REQUEST);
				}
				GroupMember.update({
					gid : group_id,
					pid : profile_id,
					act : true,
					gdel: false
				},{
					type : GroupMember.TYPE.MEMBER
				},function(err,unbanned_member){
					if(err){
						logger.error({"r":"gmem_ub","m":"PUT","msg":"groupMember_update_server_error","p":req.body});  
						return sendError(res,err,"server_error",constants.HTTP_STATUS.SERVER_ERROR);
					}
					removeCacheForGroupMemUnBan(profile_id, group_id, req.body, function(err,isRemoved){
						if(err){
							logger.warn({"r":"gmem_ub","msg":"rmGrpMemsFromUsrCache_error","p":req.body,"er":err});
						}
						var time = new Date().getTime();
						sendSuccess(res, {data:unbanned_member});
						return notify_grp_unban_member(group_id, group, profile_id, admin_id, time);
					});
				});
			});
		});
  });
});

/*
 * Delete a Group Member
 */
router.put('/gmem_d', function(req, res, next) {
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('admin_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('tokn',errorCodes.invalid_parameters[1]).notEmpty().validateToken();

	if(req.validationErrors()) { 
		logger.error({"r":"gmem_d","m":"PUT","msg":"invalid_parameters","p":req.body});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var group_id 		  = req.body.g_id;
	var admin_id 		  = req.body.admin_id;
	var profile_id 		= req.body.p_id;
	
	AccountInfo.validateVersionForAdminActions(profile_id, function(err,resp){
		if(err){ 
			logger.error({"r":"gmem_d","m":"PUT","msg":"validateVersion_DbError","p":req.body});
			return sendError(res,err,"server_error"); 
		}
		if(!resp){
			logger.error({"r":"gmem_d","m":"PUT","msg":"lower_mobile_app_version","p":req.body}); 
			return sendError(res,"Target User needs to upgrade the app version","lower_mobile_app_version"); 
		} 
		GroupInfoCache.getMongoGroup(group_id,function(err,group){
			if(err) { 
				logger.error({"r":"gmem_d","m":"PUT","msg":"getMongoGroup_error","p":req.body});
				return sendError(res,err,"server_error");
			}
			if(!group){
				logger.error({"r":"gmem_d","m":"PUT","msg":"no_group_found","p":req.body});
				return sendError(res,"Group does not exists.","no_group_found",constants.HTTP_STATUS.NOT_FOUND);
			}
			if(group.secr && !req.decoded){
				secureGroupLogger.error({"r":"gmem_d","m":"PUT","msg":"secr_group_not_authorized","p":req.body});
				logger.error({"r":"gmem_d","m":"PUT","msg":"secr_group_not_authorized","p":req.body});
				return sendError(res,"not_authorized","not_authorized",constants.HTTP_STATUS.UNAUTHORIZED);
			}
			GroupMember.find({
				gid : group_id,
				pid : { $in : [admin_id,profile_id]},
				act : true,
				gdel: false
			},function(err,members){
				if(err){ 
					logger.error({"r":"gmem_d","m":"PUT","msg":"groupMember_find_server_error","p":req.body});
					return sendError(res,err,"server_error"); 
				}
				if(members.length < 2){
					logger.error({"r":"gmem_d","m":"PUT","msg":"user_and_admin_members_not_found","p":req.body});
					return sendError(res,"User and Admin not found in groupmembers","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
				}
				var admin , member_to_delete;
				for (var i = 0; i < 2 ; i++) {
					if(members[i].pid == admin_id) 
						admin = members[i];
					else if (members[i].pid == profile_id){
						member_to_delete 		= members[i];
					}
				}
				if(!admin){
					logger.error({"r":"gmem_d","m":"PUT","msg":"user_not_group_member","p":req.body});
					return sendError(res,"User not a member of the Group","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
				}
				if(!ValidAuthRequest.check(admin.aid,req.decoded)){
					logger.error({"r":"gmem_d","m":"PUT","msg":"ValidAuthRequest_check_error","p":req.body});
					return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
				}
				if(admin.type != GroupMember.TYPE.ADMIN){
					logger.error({"r":"gmem_d","m":"PUT","msg":"user_not_group_admin","p":req.body});
					return sendError(res,"User not an Administrator of the Group","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
				}
				if(!member_to_delete){
					logger.error({"r":"gmem_d","m":"PUT","msg":"profile_not_a_group_member","p":req.body});
					return sendError(res,"Profile to be deleted is not a member of the group.","not_a_member",constants.HTTP_STATUS.BAD_REQUEST);
				}
				if(member_to_delete.type == GroupMember.TYPE.ADMIN){
					logger.error({"r":"gmem_d","m":"PUT","msg":"group_admin_cannot_be_deleted","p":req.body});
					return sendError(res,"Group admin cannot be deleted directly","group_admin",constants.HTTP_STATUS.BAD_REQUEST);
				}
				if(member_to_delete.type == GroupMember.TYPE.BANNED){
					logger.error({"r":"gmem_d","m":"PUT","msg":"banned_group_member","p":req.body});
					return sendError(res,"Banned member cannot be deleted directly","banned_group_member",constants.HTTP_STATUS.BAD_REQUEST);
				}
				GroupMember.update({
					_id : member_to_delete._id,
					act : true
				},{
					act : false
				},function(err,deleted_group_member) {
					if(err){ 
						logger.error({"r":"gmem_d","m":"PUT","msg":"groupMember_update_server_error","p":req.body});
						return sendError(res,err,"server_error"); 
					}
					removeCacheForGroupMemDel(profile_id, group_id, req.body, function(err,isRemoved){
						if(err){
							logger.warn({"r":"gmem_d","msg":"removeCacheForGroupMemDel_error","p":req.body,"er":err});
						}					
						var time = new Date().getTime();
						sendSuccess(res, {data : deleted_group_member});
						return notify_grp_del_member(group_id, group, profile_id, admin_id, time);
					});
				});
			});
		});
  });
});

/*
 *Leave A GROUP
 */
router.put('/g_l', function(req, res, next) {
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('tokn',errorCodes.invalid_parameters[1]).notEmpty().validateToken();

	if(req.validationErrors()){ 
		logger.error({"r":"g_l","m":"PUT","msg":"invalid_parameters","p":req.body});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST); 
	}

	var group_id 			= req.body.g_id;
	var profile_id 		= req.body.p_id;

	GroupInfoCache.getMongoGroup(group_id,function(err,group){
		if(err){
			logger.error({"r":"g_l","m":"PUT","msg":"group_find_server_error","p":req.body}); 
			return sendError(res,err,"server_error");
		}
		if(!group){
			logger.error({"r":"g_l","m":"PUT","msg":"no_group_found","p":req.body}); 
			return sendError(res,"Group not found","no_group_found",constants.HTTP_STATUS.NOT_FOUND);
		}
		if(group.secr && !req.decoded){
			secureGroupLogger.error({"r":"g_l","m":"PUT","msg":"secr_group_not_authorized","p":req.body});
			logger.error({"r":"g_l","m":"PUT","msg":"secr_group_not_authorized","p":req.body});
			return sendError(res,"not_authorized","not_authorized",constants.HTTP_STATUS.UNAUTHORIZED);
		}
		GroupMember.findOne({
			gid : group_id,
			pid : profile_id,
			act : true,
			gdel: false
		},function(err, leave_group){
			if(err){
				logger.error({"r":"g_l","m":"PUT","msg":"groupmember_find_server_error","p":req.body}); 
				return sendError(res,err,"server_error");
			}
			if(!leave_group){
				logger.error({"r":"g_l","m":"PUT","msg":"not_group_member","p":req.body}); 
				return sendError(res,"You are not a member of the group.","not_a_member",constants.HTTP_STATUS.NOT_FOUND);
			}
			if(!ValidAuthRequest.check(leave_group.aid,req.decoded)){
				logger.error({"r":"g_l","m":"PUT","msg":"ValidAuthRequest_check_error","p":req.body});
				return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
			}
			if(leave_group.type == GroupMember.TYPE.ADMIN){
				logger.error({"r":"g_l","m":"PUT","msg":"group_admin_cannot_leave","p":req.body}); 
				return sendError(res,"Group Admin cannot leave the group","group_admin_cannot_leave",constants.HTTP_STATUS.NOT_FOUND);
			}
			if(leave_group.type == GroupMember.TYPE.BANNED){
				logger.error({"r":"g_l","m":"PUT","msg":"banned_group_member","p":req.body}); 
				return sendError(res,"Banned member cannot leave the group","banned_group_member",constants.HTTP_STATUS.NOT_FOUND);
			}
			GroupMember.update({
				_id : leave_group._id
			},{
				act : false
			},function(err,left_group_member){
				if(err){ 
					logger.error({"r":"g_l","m":"PUT","msg":"groupMember_update_server_error","p":req.body}); 
					sendError(res,err,"server_error");
				}
				removeCacheForGroupMemLeave("g_l", profile_id, group_id, req.body, function(err,isRemoved){
					if(err){
						logger.warn({"r":"g_l","msg":"removeCacheForGroupMemLeave_error","p":req.body}); 
					}
					var time = new Date().getTime();
					sendSuccess(res, {data : left_group_member});
					return notify_grp_lv(group_id, profile_id, group.name, time);
				});
			});
		});
	});
});

/*
 *Leave A GROUP
 */
router.put('/g_l_i', function(req, res, next) {
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();

	if(req.validationErrors()){ 
		logger.error({"r":"g_l_i","m":"PUT","msg":"invalid_parameters","p":req.body});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.INVALID_PARAMETER);
	}

	var group_id 		= req.body.g_id;
	var profile_id 		= req.body.p_id;

	GroupInfoCache.getMongoGroup(group_id,function(err,group){
		if(err){
			logger.error({"r":"g_l_i","m":"PUT","msg":"group_find_server_error","p":req.body}); 
			return sendError(res,err,"server_error");
		}
		if(!group){
			logger.error({"r":"g_l_i","m":"PUT","msg":"no_group_found","p":req.body}); 
			return sendError(res,"Group not found","no_group_found",constants.HTTP_STATUS.NOT_FOUND);
		}
		if(group.secr && !req.decoded){
			secureGroupLogger.error({"r":"g_l_i","m":"PUT","msg":"secr_group_not_authorized","p":req.body});
			logger.error({"r":"g_l_i","m":"PUT","msg":"secr_group_not_authorized","p":req.body});
			return sendError(res,"not_authorized","not_authorized",constants.HTTP_STATUS.UNAUTHORIZED);
		}
		GroupMember.findOne({
			gid : group_id,
			pid : profile_id,
			act : true,
			gdel: false
		},function(err, leave_group){
			if(err){
				logger.error({"r":"g_l_i","m":"PUT","msg":"groupmember_find_server_error","p":req.body}); 
				return sendError(res,err,"server_error");
			}
			if(!leave_group){
				logger.error({"r":"g_l_i","m":"PUT","msg":"not_group_member","p":req.body});
				return sendError(res,"You are not a member of the group.","not_a_member",constants.HTTP_STATUS.NOT_A_GROUP_MEMBER);
			}
			if(!ValidAuthRequest.check(leave_group.aid,req.decoded)){
				logger.error({"r":"g_l_i","m":"PUT","msg":"ValidAuthRequest_check_error","p":req.body});
				return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
			}
			if(leave_group.type == GroupMember.TYPE.ADMIN){
				logger.error({"r":"g_l_i","m":"PUT","msg":"group_admin_cannot_leave","p":req.body});
				return sendError(res,"Group Admin cannot leave the group","group_admin_cannot_leave",constants.HTTP_STATUS.NOT_A_GROUP_ADMIN);
			}
			if(leave_group.type == GroupMember.TYPE.BANNED){
				logger.error({"r":"g_l_i","m":"PUT","msg":"banned_group_member","p":req.body}); 
				return sendError(res,"Group Admin cannot leave the group","banned_group_member",constants.HTTP_STATUS.NOT_FOUND);
			}
			GroupMember.update({
				_id : leave_group._id
			},{
				act : false
			},function(err,left_group_member){
				if(err){ 
					logger.error({"r":"g_l_i","m":"PUT","msg":"groupMember_update_server_error","p":req.body});
					return sendError(res,err,"server_error");
				}
				removeCacheForGroupMemLeave("g_l_i", profile_id, group_id, req.body, function(err,isRemoved){
					if(err){
						logger.warn({"r":"g_l_i","msg":"removeCacheForGroupMemLeave_error","p":req.body,"er":err});
					}
					var time = new Date().getTime();
					sendSuccess(res, {data : left_group_member});
					return notify_grp_lv(group_id, profile_id, group.name, time);
				});
			});
		});
	});
});

/*
 * get mobile number from the encrypted key
 */
router.post('/d_mb', function(req, res, next) {
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('k_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('mb_e',errorCodes.invalid_parameters[1]).optional();

	if(req.validationErrors()){
		logger.error({"r":"d_mb","m":"POST","msg":"invalid_parameters","p":req.body}); 
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var group_id 			= req.body.g_id;
	var profile_id 		= req.body.p_id;
	var t_profile_id  = req.body.k_id;
	var t_mobile_e     = req.body.mb_e ? req.body.mb_e : null;

	GroupInfoCache.getMongoGroup(group_id,function(err,group){
		if(err) { 
			logger.error({"r":"d_mb","m":"POST","msg":"getMongoGroup_error","p":req.body});
			return sendError(res,err,"server_error");
		}
		if(!group){
			logger.error({"r":"d_mb","m":"POST","msg":"no_group_found","p":req.body});
			return sendError(res,"Group does not exists.","no_group_found",constants.HTTP_STATUS.NOT_FOUND);
		}
		if(group.secr && !req.decoded){
			secureGroupLogger.error({"r":"d_mb","m":"POST","msg":"secr_group_not_authorized","p":req.body});
			logger.error({"r":"d_mb","m":"POST","msg":"secr_group_not_authorized","p":req.body});
			return sendError(res,"not_authorized","not_authorized",constants.HTTP_STATUS.UNAUTHORIZED);
		}
		UserInfoCache.getMongoGrpMem(profile_id, group_id, function(err, group_member){
			if(err) { 
				logger.error({"r":"d_mb","m":"POST","msg":"getMongoGrpMem_DbError","p":req.body});
				return sendError(res,err,"server_error"); 
			}
			if(!group_member){
				logger.error({"r":"d_mb","m":"POST","msg":"not_a_group_member","p":req.body});
				return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
			}
			if(!ValidAuthRequest.isIos(group_member.aid,req.decoded)){
				logger.error({"r":"d_mb","m":"POST","msg":"ValidAuthRequest_isIos-error","p":req.body});
				return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
			}
			if(group_member.type != GroupMember.TYPE.ADMIN){
				logger.error({"r":"d_mb","m":"POST","msg":"not_a_group_admin","p":req.body});
				return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
			}
			UserInfoCache.getMongoGrpMem(t_profile_id, group_id, function(err, t_group_member){
				if(err) { 
					logger.error({"r":"d_mb","m":"POST","msg":"getMongoGrpMem_DbError","p":req.body});
					return sendError(res,err,"server_error"); 
				}
				if(!t_group_member){
					logger.error({"r":"d_mb","m":"POST","msg":"key_for_decryption_not_a_group_member","p":req.body});
					return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
				}
				UserInfoCache.getMongoProfile(t_profile_id,function(err,t_profile){
					if(err) { 
						logger.error({"r":"d_mb","m":"POST","msg":"getMongoProfile_Error","p":req.body});
						return sendError(res,err,"server_error"); 
					}
					if(!t_mobile_e){
						return sendSuccess(res,{p : t_profile});
					}
					PrivilegeGroup.findOne({
						gid : group_id
					},function(err, privGrp){
						if(err) { 
							logger.error({"r":"d_mb","m":"POST","msg":"PrivilegeGroup_find_Error","p":req.body});
							return sendError(res,err,"server_error"); 
						}
						if(!privGrp) { 
							logger.error({"r":"d_mb","m":"POST","msg":"PrivilegeGroup_not_found","p":req.body});
							return sendSuccess(res,{p : t_profile}); 
						}
						var mobile_num = EncryptData.decrypt(t_mobile_e, t_profile_id);
						if(mobile_num == null){
							return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
						}
						return sendSuccess(res,{p : t_profile, m : mobile_num});
					});
				});
			});
		});
	});
});

function modifyReturningGroupObject(group){
	delete group["secr"];
	delete group["plugin_id"];
	delete group["cpid"];
	delete group["caid"];
	delete group["crole"];
	delete group["act"];
	delete group["showcase_url"];
	delete group["promo_url"];
	return group;
}

function alreadyJoinedGroupResponse(res, route, group, groupmember, tim){
	var valid_subscription = false;
	if(group.req_sub && group.req_sub == true && group.plugin_id){
		if(groupmember.sub_ed && groupmember.sub_ed > tim){
			valid_subscription = true;
		}
		GroupPluginPackage.find({
			gid       : group._id,
			plugin_id : group.plugin_id,
			act       : true
		},{
			plugin_id        : 1,
			package_name     : 1,
			package_desc     : 1,
			package_price    : 1,
			package_duration : 1,
			duration_unit    : 1,
			duration_base    : 1,
			acc              : 1
		},function(err,plugin_packages){
			if(err){
				logger.error({"r":route,"msg":"GroupPluginPackageFind_DbError","p":req.body,"err":err,"grp":group});
				return sendError(res,"server_error","server_error");
			}
			logger.info({"r":route,"msg":"joingroup_Success","ispaid":group.req_sub,"packs":plugin_packages});
			return sendSuccess(res, {"Group":group,"GroupMember":groupmember,ispaid:group.req_sub,packs:plugin_packages,CC_DC:1,v_subr:valid_subscription});
		});
	} else {
		return sendSuccess(res, {"Group":group,"GroupMember":groupmember});
	}
}

function isValidVersionForPaidGroup(route, group_paid, account, client){
	if(!group_paid || group_paid == false){
		return true;
	}
	if(!client || client != AppClients.ANDROID){
		logger.error({"r":route,"msg" :"JoinGroup, Group is Paid , But Client is not Android","p":{gpaid:group_paid,acc:account,cl:client}});
		return false;
	}
	if(!account.vrsn || account.vrsn < configs.PAID_GROUPJOIN_ANDROID_VERSION){
		logger.error({"r":route,"msg" :"JoinGroup, Group is Paid , But Android Version is lessar","p":{gpaid:group_paid,acc:account,cl:client}});
		return false;
	}
	return true;
}

function fetchMobileNumbers(profiles, num_of_profiles, cb){
	var result = [];
	var accountByPid = [];
	var account_ids = [];
	for(var i=0; i<num_of_profiles; i++){
		accountByPid[profiles[i]._id] = profiles[i].aid;
		if(account_ids.indexOf(profiles[i].aid) < 0)
			account_ids.push(profiles[i].aid);
	}
	Account.find({
		_id : { $in : account_ids },
	},{
		_id : 1,
		m   : 1,
		ccod: 1
	},function(err, mobiles){
		if(err) { console.trace(err); return cb(true,null); }
		if(mobiles.length == 0){
			console.trace('No Mobiles found in fetching of Mobile Number Data for Groupmember list request'); 
			return cb(null,null);
		}
		var num_of_mobiles = mobiles.length;
		var mobileByAid = []
		for(var j=0; j<num_of_mobiles; j++){
			mobileByAid[mobiles[j]._id] = '+'+mobiles[j].ccod+'-'+mobiles[j].m;
		}
		for(m=0; m<num_of_profiles; m++){
			result[profiles[m]._id] = EncryptData.encrypt(mobileByAid[accountByPid[profiles[m]._id]],profiles[m]._id);
		}
		return cb(null, result);
	});
}

function notify_admin_ad_rm(group_id, admin_id, profile_id, action, gname, gtyp, time) {
	Profile.find({
		_id : { $in : [profile_id,admin_id]},
		act : true
	},function(err,profiles){
		if(err) {
			console.log('In Remove Group Admin : MongoDb Error - Profiles do not exist');
			return;
		}
		if(profiles.length < 2) {
			console.log('In notify_target : Profiles do not exist');
			return;
		}
		var admin_name,profile_name,admin_role;
		for (var i = 0; i < 2 ; i++) {
			if(profiles[i]._id == admin_id){ 
				admin_name = profiles[i].nam;
				admin_role = profiles[i].role;
			}	else {
				profile_name = profiles[i].nam;
			}
		}
		if (action == Actions.GROUP_ADMIN_ADD) {
			var notify_msg = admin_name+' has made '+profile_name+' an admin';
			GroupNotify.abt_gadm_a(group_id, admin_id, profile_id, notify_msg, gname, gtyp, time);
		} else if (action == Actions.GROUP_ADMIN_REM) {
			var notify_msg = admin_name+' has removed '+profile_name+'\'s admin rights';
			GroupNotify.abt_gadm_r(group_id, profile_id, admin_id, admin_name, admin_role, notify_msg, gname, time);
		}
	});
}

function notify_grp_lv(group_id, profile_id, gname, time) {
	UserInfoCache.getMongoProfile(profile_id,function(err,profile){
		if(err) { 
			console.log('In Leave Group : MongoDb Error - Profiles do not exist');
			return;
		}
		if(!profile) {
			console.log('In notify_target : Profile does not exist');
			return;
		}
		var notify_msg = profile.nam+' has left the group';
		GroupNotify.abt_g_l(group_id, profile_id, profile.nam, profile.role, notify_msg, gname, time);
	});
}

function notify_grp_ban_member(group_id, group, admin_id, profile_id, pname, otherAffectedPids, otherAffectedPidsName, time){
	UserInfoCache.getMongoProfile(admin_id,function(err,admin){
		if(err){
			return logger.error({"FUNCTION":"notify_grp_ban_member","admin_id":admin_id,"er":err});
		}
		if(!admin){
			return logger.error({"FUNCTION":"notify_grp_ban_member","admin_id":admin_id,"msg":"profile_not_found"});
		}
		var notify_msg = pname+' has been banned by '+admin.nam;
		GroupNotify.abt_gmem_ban(group_id, profile_id, admin_id, admin.nam, admin.role, notify_msg, group.name, group.gtyp, time);
		notify_grp_ban_affeted_pids(group_id, otherAffectedPids, profile_id, otherAffectedPidsName, admin_id, admin.nam, admin.role, group.name, group.gtyp, time);
	});
}

function notify_grp_ban_affeted_pids(group_id, affected_pids, excludePid, affected_pids_names, admin_id, admin_name, admin_role, group_name, group_type, time){
	var toDo = affected_pids.length;
	var notify_msg, pid;
	if(toDo == 0){
		return logger.error({"FUNCTION":"notify_grp_ban_affeted_pids","pids":affected_pids,"msg":"only_one_pid_found"});
	}
	for(var i=0; i<toDo; i++){
		pid = affected_pids[i];
		if(pid != excludePid){
			notify_msg = affected_pids_names[pid]+' has been banned by '+admin_name;
			GroupNotify.abt_gmem_ban_other_pids(group_id, pid, admin_id, admin_name, admin_role, notify_msg, group_name, group_type, time);
		}
	}
}

function notify_grp_unban_member(group_id, group, profile_id, admin_id, time){
	Profile.find({
		_id : { $in : [profile_id,admin_id]},
		act : true
	},function(err,profiles){
		if(err) {
			console.log('In Banning Group Member : MongoDb Error - Profiles do not exist');
			return;
		}
		if(profiles.length < 2) {
			console.log('In notify_target : Profiles do not exist');
			return;
		}
		var admin_name,profile_name,admin_role;
		for (var i = 0; i < 2 ; i++) {
			if(profiles[i]._id == admin_id){ 
				admin_name = profiles[i].nam;
				admin_role = profiles[i].role;
			}	else profile_name = profiles[i].nam;
		}
		var notify_msg = profile_name+' has been unbanned by '+admin_name;
		GroupNotify.abt_gmem_unban(group_id, profile_id, admin_id, admin_name, admin_role, notify_msg, group.name, time);
	});
}

function notify_grp_del_member(group_id, group, profile_id, admin_id, time){
	Profile.find({
		_id : { $in : [profile_id,admin_id]},
		act : true
	},function(err,profiles){
		if(err) {
			console.log('In Group Member Delete: MongoDb Error - Profiles do not exist');
			return;
		}
		if(profiles.length < 2) {
			console.log('In notify_target : Profiles do not exist');
			return;
		}
		var admin_name,profile_name,admin_role;
		for (var i = 0; i < 2 ; i++) {
			if(profiles[i]._id == admin_id){ 
				admin_name = profiles[i].nam;
				admin_role = profiles[i].role;
			}	else profile_name = profiles[i].nam;
		}
		var notify_msg = profile_name+' has been removed by '+admin_name;
		GroupNotify.abt_gmem_del(group_id, profile_id, admin_id, admin_name, admin_role ,notify_msg, group.name, time);
	});
}

function removeCacheForGroupMemJoin(route, pid, gid, params, cb){
	rmGrpMemsFromUsrCache(pid,function(err,isRemoved){
		if(err){
			logger.warn({"r":route,"msg":"rmGrpMemsFromUsrCache_error","p":params,"er":err});
		}
		AccountDataInfoCache.removeGroupAccountDatas(gid, function(err, resp){
			if(err){
				logger.warn({"r":route,"msg":"removeGroupAccountDatas_error","p":params,"er":err});
			}
			return cb(null,true);
		});
	});
}

function removeCacheForGroupMemBan(aid, targetProfilePids, gid, params, cb){
	UserInfoCache.rmUsrGrpMemsByAidInCache(aid,function(err,isRemoved){
		if(err){
			logger.warn({"r":"gmem_b","msg":"rmGrpMemsFromUsrCache_error","p":params,"er":err});
		}
		UserInfoCache.removeMultiMongoGrpMem(targetProfilePids, gid,function(err, rRemoved){
			if(err){
				logger.warn({"r":"gmem_b","msg":"removeMultiMongoGrpMem","p":params,"er":err});
			}
			AccountDataInfoCache.removeGroupAccountDatas(gid, function(err, resp){
				if(err){
					logger.warn({"r":"gmem_b","msg":"removeGroupAccountDatas_error","p":params,"er":err});
				}
				return cb(null,true);
			});
		});
	});
}

function removeCacheForGroupMemUnBan(pid, gid, params, cb){
	rmGrpMemsFromUsrCache(pid, function(err,isRemoved){
		if(err){
			logger.warn({"r":"gmem_ub","msg":"rmGrpMemsFromUsrCache_error","p":req.body});
		}
		UserInfoCache.removeMongoGrpMem(pid, gid, function(err, rRemoved){
			if(err){
				logger.warn({"r":"gmem_ub","msg":"removeMongoGrpMem_error","p":req.body});
			}
			AccountDataInfoCache.removeGroupAccountDatas(gid, function(err, resp){
				if(err){
					logger.warn({"r":"gmem_ub","msg":"removeGroupAccountDatas_error","p":params,"er":err});
				}
				return cb(null,true);
			});
		});
	});
}

function removeCacheForGroupMemDel(pid, gid, params, cb){
	rmGrpMemsFromUsrCache(pid,function(err,isRemoved){
		if(err){
			logger.warn({"r":"gmem_d","msg":"rmGrpMemsFromUsrCache_error","p":params,"er":err});
		}					
		UserInfoCache.removeMongoGrpMem(pid, gid,function(err, rRemoved){
			if(err){
				logger.warn({"r":"gmem_d","msg":"removeMongoGrpMem_error","p":params,"er":err});
			}
			AccountDataInfoCache.removeGroupAccountDatas(gid, function(err, resp){
				if(err){
					logger.warn({"r":"gmem_d","msg":"removeGroupAccountDatas_error","p":params,"er":err});
				}
				return cb(null,true);
			});
		});
	});
}

function removeCacheForGroupMemLeave(route, pid, gid, params, cb){
	rmGrpMemsFromUsrCache(pid,function(err,isRemoved){
		if(err){
			logger.warn({"r":route,"msg":"rmGrpMemsFromUsrCache_error","p":params,"er":err}); 
		}
		UserInfoCache.removeMongoGrpMem(pid, gid,function(err, rRemoved){
			if(err){
				logger.warn({"r":route,"msg":"removeMongoGrpMem_error","p":params,"er":err}); 
			}
			AccountDataInfoCache.removeGroupAccountDatas(gid, function(err, resp){
				if(err){
					logger.warn({"r":route,"msg":"removeGroupAccountDatas_error","p":params,"er":err});
				}
				return cb(null,true);
			});
		});
	});
}

function rmGrpMemsFromUsrCache(pid, cb){
	UserInfoCache.getMongoProfile(pid,function(err, profile){
		if(err){
			return cb(err,null);
		}
		var key = RedisPrefixes.USER_PROF_GMS+profile.aid+'';
		rhmset.keyexists(key,UserRedisKeys.GROUPS,function(err,isKey){
			if(err){
				return cb(err,null);
			}
			if(isKey){
				rhmset.removefield(key,UserRedisKeys.GROUPS,function(err,resp){
					if(err){
						return cb(err,null);
					} 
					if(resp){
						return cb(null,true);
					} else {
						console.log("Oops,this is unfortunate that Account Profiles key could not be deleted for Account : "+profile.aid);
						return cb(null,false);
					}
				});
			} else {
				console.log("Oops,this is unfortunate that Account Profiles key does not exists for Account : "+profile.aid);
				return cb(null,false);
			}
		});
	});
}

function rmGrpAdmsFromGrpCache(gid, cb){
	var key = RedisPrefixes.GROUP_INFO+gid+'';
	rhmset.keyexists(key,GroupRedisKeys.ADMINS,function(err,isKey){
		if(err){
			return cb(err,null);
		}
		if(isKey){
			rhmset.removefield(key,GroupRedisKeys.ADMINS,function(err,resp){
				if(err){
					return cb(err,null);
				} 
				if(resp){
					return cb(null,true);
				} else {
					console.log("Oops,this is unfortunate that Group Admins HMSET key could not be deleted for gid : "+gid);
					return cb(null,false);
				}
			});
		} else {
			console.log("Oops,this is unfortunate that Group Admins HMSET key does not exists for Account : "+gid);
			return cb(null,false);
		}
	});
}

module.exports = router;