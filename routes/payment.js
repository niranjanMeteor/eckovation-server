const express 								= require('express');
const mongoose 								= require('mongoose');
const fs                      = require('fs');
const path 					          = require('path');
const _						            = require('underscore');

const configs									= require('../utility/configs.js');
const constants								= require('../utility/constants.js');
const PaymentConfigs					= require('../payment/configs.js');
const PaytmConfigs					  = require('../payment/paytm_configs.js');
const PaytmApi                = require('../payment/paytm_api.js');
const log4jsLogger  					= require('../loggers/log4js_module.js');
const log4jsConfigs  					= require('../loggers/log4js_configs.js');
const MailClient          		= require('../utility/mail/mail_client.js');
const BruteForce              = require('../utility/auth/brute_force.js');
const PaytmChecksum					  = require('../payment/paytm_checksum.js');
const RabbitMqPublish         = require('../rabbitmq/publisher/publisher.js');
const RabbitMqQueues          = require('../rabbitmq/queues.js');
const FeeModule               = require('../fee/fee_module.js');

require('../models/groups.js');
require('../models/fee/grouppathshalaserver.js');
require('../models/payment/transactions.js');
require('../models/payment/transactionlogs.js');
require('../models/payment/gatewayresponse.js');
require('../models/packages/grouppluginpackages.js');
require('../models/subscriptions/usersubscriptions.js');
require('../models/fee/feetransactions.js');
const Group 								  = mongoose.model('Group');
const Transaction 					  = mongoose.model('Transaction');
const TransactionLog 					= mongoose.model('TransactionLog');
const GatewayResponse 				= mongoose.model('GatewayResponse');
const GroupPathshalaServer    = mongoose.model('GroupPathshalaServer');
const GroupPluginPackage 			= mongoose.model('GroupPluginPackage');
const UserSubscription 			  = mongoose.model('UserSubscription');
const FeeTransaction          = mongoose.model('FeeTransaction');

const router 									= express.Router();
const logger         			    = log4jsLogger.getLogger(log4jsConfigs.CATEGORY_ROUTES_PAYMENT);

router.post('/paytmchecksum',function(req, res, next){
	var params = req.body;
	if(!params){
		logger.error({"r":"paytmchecksum","msg":"invalid_parameters","p":req.body,"er":err});
		return res.status(constants.HTTP_STATUS.OK).json({CHECKSUMHASH:"",ORDER_ID:params.ORDER_ID,payt_STATUS:2});
	}
	PaytmChecksum.genchecksum(params, PaytmConfigs.KEY, function(err,checksum){
		if(err){
			logger.error({"r":"paytmchecksum","msg":"PaytmChecksum_genchecksum_failed","p":req.body,"er":err});
			return res.status(constants.HTTP_STATUS.OK).json({CHECKSUMHASH:"",ORDER_ID:params.ORDER_ID,payt_STATUS:2});
		}
		logger.info({"r":"paytmchecksum","msg":"PaytmChecksum_genchecksum_success","p":req.body,"chksm":checksum});
		return res.status(constants.HTTP_STATUS.OK).json({CHECKSUMHASH:checksum,ORDER_ID:params.ORDER_ID,payt_STATUS:1});;
	});
});

router.post('/paytmchecksumv',function(req, res, next){
	var params = req.body;
	var error_file = path.join(PaytmConfigs.RESPONSE_FILE_DIR,PaytmConfigs.RESPONSE_ERROR_TEMPLATE_FILE);
	if(!params || !params.CHECKSUMHASH || !params.ORDERID){
		logger.error({"r":"paytmchecksumv","msg":"invalid_parameters","p":req.body});
		res.set('Content-Type', 'text/html');
		return res.sendFile(error_file);
	}
	GatewayResponse.findOne({
		trxn_id : params.ORDERID
	},function(err,alreadyResponded){
		if(err){
			logger.error({"r":"paytmchecksumv","msg":"GatewayResponse_find_Db_error","p":req.body,"er":err});
			res.set('Content-Type', 'text/html');
			return res.sendFile(error_file);
		}
		if(alreadyResponded){
			logger.error({"r":"paytmchecksumv","msg":"GatewayResponse_AlreadyFound_For_This_ORDERID","p":req.body});
			res.set('Content-Type', 'text/html');
			return res.sendFile(error_file);
		}
		PaytmChecksum.verifychecksum(params, PaytmConfigs.KEY, function(err,verified){
			if(err){
				logger.error({"r":"paytmchecksumv","msg":"PaytmChecksum_genchecksum_failed","p":req.body,"er":err});
				res.set('Content-Type', 'text/html');
				return res.sendFile(error_file);
			}
			if(!verified){
				logger.info({"r":"paytmchecksumv","msg":"PaytmChecksum_genchecksum_verify_failed","p":req.body});
				res.set('Content-Type', 'text/html');
				return res.sendFile(error_file);
			}
			verifyTransactionResponse(params, function(err,transaction){
				if(err){
					logger.info({"r":"paytmchecksumv","msg":"verifyTransactionResponse_failed","p":req.body,"er":err});
					res.set('Content-Type', 'text/html');
					return res.sendFile(error_file);
				}
				switch(transaction.module){

					case GatewayResponse.MODULE.VIDEO:
						return processVideoOrTestPayment(req, res, params);
						break;

					case GatewayResponse.MODULE.TEST:
						return processVideoOrTestPayment(req, res, params);
						break;

					case GatewayResponse.MODULE.FEE:
						return processFeePayment(req, res, params, transaction, "paytmchecksumv");
						break;

					default :
						return processVideoOrTestPayment(req, res, params);
				}
			});
		});
	});
});

function processVideoOrTestPayment(req, res, params){
	saveVideoOrTestPaymentResponse(params, function(err,resp){
		if(err){
			logger.info({"r":"paytmchecksum","msg":"saveVideoOrTestPaymentResponse_error","p":req.body,"er":err});
			res.set('Content-Type', 'text/html');
			return res.sendFile(error_file);
		}
		params.IS_CHECKSUM_VALID = "Y";
		delete params['CHECKSUMHASH'];
		var payment_notify_object = new Buffer(JSON.stringify({
	    trxn_id : params.ORDERID,
	    atmpt   : 0,
	    gtwy    : Transaction.GATEWAY.PAYTM
	  }));
		RabbitMqPublish.publish("",RabbitMqQueues.PAYMENT_NOTIFY,payment_notify_object);
		paytmSuccessResponseHtml(params, function(err,html_string){
			if(err){
				logger.info({"r":"paytmchecksum","msg":"processVideoOrTestPayment_paytmSuccessResponseHtml_error","p":req.body,"er":err});
				res.set('Content-Type', 'text/html');
				return res.sendFile(error_file);
			}
			res.set('Content-Type', 'text/html');
			var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
			logger.info({"r":"paytmchecksum","msg":"processVideoOrTestPayment_paytmSuccessResponseHtml_Successfull","p":req.body,"resp":html_string,"src":fullUrl});
      return res.send(html_string);
		});
	});
}

function processFeePayment(req, res, params, transaction, route_name){
	FeeModule.saveFeePayRespPaytm(params, function(err,resp){
		if(err){
			logger.error({"r":route_name,"msg":"paytm_FeeModule_saveFeePayRespPaytm_error","p":req.body,"er":err});
			res.set('Content-Type', 'text/html');
			return res.sendFile(error_file);
		}
		params.IS_CHECKSUM_VALID = "Y";
		delete params['CHECKSUMHASH'];
		paytmSuccessResponseHtml(params, function(err,html_string){
			if(err){
				logger.error({"r":route_name,"msg":"processFeePayment_paytmSuccessResponseHtml_error","p":req.body,"er":err});
				res.set('Content-Type', 'text/html');
				return res.sendFile(error_file);
			}
			res.set('Content-Type', 'text/html');
			var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
			logger.info({"r":route_name,"msg":"processFeePayment_paytmSuccessResponseHtml_Successfull","p":req.body,"src":fullUrl});
      res.send(html_string);
      var toDo = 2;
      var done = 0;
      if(params.STATUS == Transaction.GATEWAY_STATUS.TXN_SUCCESS){
      	return logger.info({"r":route_name,"msg":"paytm_Fee_Transaction_was_not_successfull","p":req.body,});
      }
      FeeModule.postCallback(transaction._id, transaction.gid, function(err,pathshala_resp){
      	if(err){
      		return logger.error({"r":route_name,"msg":"paytm_FeeModule_postCallback_Error","p":req.body,"er":err});
      	}
	    	logger.info({"r":route_name,"msg":"paytm_FeeModule_postCallback_HIT_Success","p":req.body});
	    	var receipt_url = null;
	    	if(pathshala_resp && ("receipt" in pathshala_resp) && (pathshala_resp.receipt == true || pathshala_resp.receipt == 'true')){
	    		if("receipt_url" in pathshala_resp){
	    			receipt_url = pathshala_resp.receipt_url;
	    		}
	    	}
	      FeeModule.smsAndMailFeePay(transaction._id, receipt_url, function(err,fee_sms_resp){
	      	if(err){
	      		return logger.error({"r":route_name,"msg":"paytm_FeeModule_smsAndMailFeePay_Error","p":req.body,"er":err});
	      	}
		    	return logger.info({"r":route_name,"msg":"paytm_FeeModule_postCallback_HIT_smsAndMailFeePay_Success","p":req.body,});
	      });
	    });
		});
	});
}

function saveVideoOrTestPaymentResponse(params, cb){
	var toDo  = 3;
	var done  = 0;
	var status   = Transaction.STATUS.FAILED;

	if(params.STATUS == Transaction.GATEWAY_STATUS.TXN_SUCCESS){
		status = Transaction.STATUS.SUCCESS;
	}
	GatewayResponse.update({
		trxn_id : params.ORDERID
	},{
		$setOnInsert : {
			trxn_id : params.ORDERID,
			resp    : params
		}
	},{
		upsert : true,
		setDefaultsOnInsert: true
	},function(err,gatewayresponse){
		if(err){
			return cb(err,null);
		}
		done++;
		if(done == toDo){
			return cb(null,true);
		}
	});
	Transaction.update({
		_id : params.ORDERID
	},{
		status           : status,
		gateway_status   : params.STATUS,
		gateway_respcode : params.RESPCODE
	},function(err,transaction_updated){
		if(err){
			return cb(err,null);
		}
		if(params.STATUS == Transaction.STATUS.TXN_SUCCESS){
			createUserSubsription(params.ORDERID, function(err, subs_resp){
				if(err){
					return cb(err,null);
				}
				done++;
				if(done == toDo){
					return cb(null,true);
				}
			});
		} else {
			done++;
			if(done == toDo){
				return cb(null,true);
			}
		}
	});
	var new_trxn_log = new TransactionLog({
		trxn_id          : params.ORDERID,
		gateway_status   : params.STATUS,
		gateway_respcode : params.RESPCODE,
		gateway_log      : params.RESPMSG,
		log              : params.RESPMSG
	});
	new_trxn_log.save(function(err,new_log){
		if(err){
			return cb(err,null);
		}
		done++;
		if(done == toDo){
			return cb(null,true);
		}
	});
}

function createUserSubsription(trxn_id, cb){
	var subscription = {};
	var start_time   = new Date().getTime();
	Transaction.findOne({
		_id : trxn_id
	},function(err,transaction){
		if(err){
			return cb(err,null);
		}
		subscription.trxn_id     = trxn_id;
		subscription.customer_id = transaction.customer_id;
		subscription.aid         = transaction.aid;
		subscription.gid         = transaction.gid;
		subscription.pid         = transaction.pid;
		subscription.stim        = start_time;
		subscription.plugin_id   = transaction.plugin_id;
		subscription.identifier  = transaction.identifier;
		if(transaction.package_id){
			subscription.package_id = transaction.package_id;
		}
		UserSubscription.update({
			gid : transaction.gid,
			pid : transaction.pid,
			plugin_id : transaction.plugin_id,
			identifier : transaction.identifier,
			act : true
		},{
			act : false
		},{
			multi : true
		},function(err,resp){
			if(err){
				return cb(err,null);
			}
			findExpiryTimeForSubscription(transaction, function(err,resp){
				if(err){
					return cb(err,null);
				}
				subscription.etim  = start_time + resp;
				subscription.act   = true;
				var user_subscription = new UserSubscription(subscription);
				user_subscription.save(function(err,new_subscription_created){
					if(err){
						return cb(err,null);
					}
					return cb(null,true);
				});
			});
		});
	});
}

function findExpiryTimeForSubscription(transaction, cb){
	GroupPluginPackage.findOne({
		_id       : transaction.package_id,
		gid       : transaction.gid,
		plugin_id : transaction.plugin_id,
		act       : true
	},function(err, plugin_package){
		if(err){
			return cb(err,null);
		}
		if(!plugin_package){
			return cb("no_plugin_packages_found_for_trxnID_"+transaction._id,null); 
		}
		if(!plugin_package.package_duration){
			return cb("no_package_duration_found_for_plugin_packages_for_trxnID_"+transaction._id,null);
		}
		return cb(null,plugin_package.package_duration);
	});
}

function verifyTransactionResponse(params, cb){
	if(!params.MID || params.MID != PaytmConfigs.MID){
		return cb("MID_invalid",false);
	}
	if(!params.TXNID){
		return cb("TXNID_key_missing_in_params",false);
	}
	if(!params.TXNAMOUNT){
		return cb("TXNAMOUNT_key_missing_in_params",false);
	}
	if(!params.STATUS){
		return cb("STATUS_key_missing_in_params",false);
	}
	if(!params.RESPCODE){
		return cb("RESPCODE_key_missing_in_params",false);
	}
	Transaction.findOne({
		_id : params.ORDERID
	},function(err,trxn){
		if(err){
			return cb(err,null);
		}
		if(!trxn){
			return cb("ORDER_ID_not_found_in_transaction",false);
		}
		if(trxn.final_amount != params.TXNAMOUNT){
			return cb("TRXN_AMOUNT_FROM_PAYTM_RESPONSE_MISMATCH",false);
		}
		if(trxn.status != Transaction.STATUS.INITIATED && trxn.status != Transaction.STATUS.IN_PROGRESS){
			return cb("TRANSACTION_STATUS_not valid_with DB",false);
		}
		return cb(null,trxn);
	});
}

function paytmSuccessResponseHtml(params, cb){
	var template_file = path.join(PaytmConfigs.RESPONSE_FILE_DIR,PaytmConfigs.RESPONSE_SUCCESS_TEMPLATE_FILE);
	fs.readFile(template_file, function(err, html){
		if(err){
			return cb(err,null);
		}
		if(!html){
			return cb("no_html_found_in_paytm_response_template",null);
		}
		var html_string     = html.toString('utf8');
		var compiled        = _.template(html_string);
		var html_string     = compiled({
			paytm_response_json : JSON.stringify(params)
		});
		return cb(null,html_string);
	});
}

//exports.reDoUserSubscriptionWithOrderId = saveResponse;
//exports.verifyTransactionResponse       = verifyTransactionResponse;

module.exports = router;




