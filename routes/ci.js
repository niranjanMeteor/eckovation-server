var express 			          = require('express');
var mongoose 								= require('mongoose');
var redis       						= require('redis');
var moment      						= require('moment');
var os 			                = require('os');

var RedisPrefixes           = require('../utility/redis_prefix.js');
var configs									= require('../utility/configs.js');
var errorCodes 							= require('../utility/errors.js');
var constants								= require('../utility/constants.js');
var helpers									= require('../utility/helpers.js');
var GroupNotify 						= require('../utility/grp_notify.js');
var MsgIdGen                = require('../utility/msg_id_generator.js');
var Actions 								= require('../utility/action_names.js');
var Events 									= require('../utility/event_names.js');
var AppClients							= require('../utility/app_clients.js');
var AccountInfo   					= require('../utility/account_info.js');
var ProcessGroupMessage			= require('../chat/process_gm.js');
var Events 									= require('../utility/event_names.js');
var RedisPrefixes						= require('../utility/redis_prefix.js');
var UserRedisKeys           = require('../utility/user_redis_keys.js');
var GroupRedisKeys          = require('../utility/group_redis_keys.js');
var GroupJoinLimits         = require('../utility/auth/group_join.js');
var UserInfoCache           = require('../utility/user_info_cache.js');
var GroupInfoCache          = require('../utility/group_info_cache.js');
var log4jsLogger  		      = require('../loggers/log4js_module.js');
var log4jsConfigs  			    = require('../loggers/log4js_configs.js');

require('../models/accounts.js');
require('../models/otps.js');
require('../models/profiles.js');
require('../models/groupmembers.js');
require('../models/groups.js');
require('../models/accountdata.js');
require('../models/messages.js');
require('../models/accounttokens.js');
require('../models/otpstatuses.js');
require('../models/otpcalls.js');
require('../models/otpcallstatuses.js');
require('../models/privilegegroups.js');
require('../models/recommendedgroups.js');

var Account 			          = mongoose.model('Account');
var Otp 										= mongoose.model('Otp');
var Profile 								= mongoose.model('Profile');
var GroupMember 						= mongoose.model('GroupMember');
var Group 									= mongoose.model('Group');
var AccountData   					= mongoose.model('AccountData');
var Message 								= mongoose.model('Message');
var AccountToken   					= mongoose.model('AccountToken');
var OtpCall   					    = mongoose.model('OtpCall');
var OtpStatus   					  = mongoose.model('OtpStatus');
var OtpCallStatus   				= mongoose.model('AccountData');
var PrivilegeGroup 			    = mongoose.model('PrivilegeGroup');
var RecommendedGroup 		    = mongoose.model('RecommendedGroup');

var sendError 							= helpers.sendError;
var sendSuccess 						= helpers.sendSuccess;
var logger        			    = log4jsLogger.getLogger(log4jsConfigs.CATEGORY_ROUTES_GROUPMEMBER);
var bruteForceGrpJnlogger   = log4jsLogger.getLogger(log4jsConfigs.CATEGORY_BRUTE_FORCE_GROUP_JOIN);
var secureGroupLogger       = log4jsLogger.getLogger(log4jsConfigs.CATEGORY_SECURE_GROUP);

var redis_client   					= redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
    console.log("Error " + err);
});

var io 											= require('socket.io-emitter')(redis_client);
var rhmset                  = require('../utility/redis_hmset.js')(redis_client);
var rcq                     = require("../utility/redis_chat_queue.js")(redis_client);

var router 	= express.Router();

var SEEN_KEY_FIRST_FIELD    = "created";
var hostname    					  = os.hostname();
var AllowedPhoneNumbers     = ['9591997098','9591996098','9591995098','82211721720','87784577079'];
// 87784577079 - 62 , 82211721720 - 62

router.get('/p_and_g', function(req, res, next){
	var p_no = req.query.p_no;
	var toDo = 2;
	var done = 0;
	if(!checkValidationForCI(p_no)) { 
		return sendError(res,"server_error","server_error"); 
	}
	var user_profiles = [];
	var user_groups   = [];
	Account.findOne({
		m    : p_no,
		vrfy : true
	},function(err,account){
		if(err) { 
			return sendError(res,err,"server_error"); 
		}
		if(!account){
			return sendSuccess(res,{});
		}
		var aid = account._id+'';
		GroupMember.distinct('gid',{
			aid : aid,
			act : true,
			gdel: false
		},function(err,groupmembers){
			if(err){
				console.trace(err);
				return sendError(res,err,"server_error");
			}
			if(groupmembers.length == 0){
				done++;
			}
			if(done == toDo){
				return sendSuccess(res,{profiles : user_profiles, groups : user_groups});
			}
			Group.find({
				_id : { $in : groupmembers },
				act : true
			},function(err,groups){
				if(err){
					console.trace(err);
					return sendError(res,err,"server_error");
				}
				done++;
				user_groups = groups;
				if(done == toDo){
					return sendSuccess(res,{profiles : user_profiles, groups : user_groups});
				}
			});
		});
		Profile.find({
			aid : aid,
			act : true
		},function(err,profiles){
			if(err){
				console.trace(err);
				return sendError(res,err,"server_error");
			}
			done++;
			user_profiles = profiles;
			if(done == toDo){
				return sendSuccess(res,{profiles : user_profiles, groups : user_groups});
			}
		});
	});
});

router.get('/rph', function(req, res, next) {
	var p_no = req.query.p_no;
	var toDo = 12;
	var done = 0;
	if(!checkValidationForCI(p_no)) { 
		return sendError(res,"server_error","server_error"); 
	}
	Account.findOne({
		m : p_no,
	},function(err,account){
		if(err) { 
			return sendError(res,err,"server_error"); 
		}
		if(!account){
			return sendSuccess(res,{});
		}
		var aid = account._id+'';
		GroupMember.remove({
			aid : aid
		},function(err,resp){
			if(err){
				console.trace(err);
				return sendError(res,err,"server_error");
			}
			done++;
			if(done == toDo){
				return sendSuccess(res,{});
			}
		});
		Otp.remove({
			aid : aid
		},function(err,resp){
			if(err){
				console.trace(err);
				return sendError(res,err,"server_error");
			}
			done++;
			if(done == toDo){
				return sendSuccess(res,{});
			}
		});
		AccountToken.remove({
			aid : aid
		},function(err,resp){
			if(err){
				console.trace(err);
				return sendError(res,err,"server_error");
			}
			done++;
			if(done == toDo){
				return sendSuccess(res,{});
			}
		});
		AccountData.remove({
			aid : aid
		},function(err,resp){
			if(err){
				console.trace(err);
				return sendError(res,err,"server_error");
			}
			done++;
			if(done == toDo){
				return sendSuccess(res,{});
			}
		});
		OtpCall.remove({
			aid : aid
		},function(err,resp){
			if(err){
				console.trace(err);
				return sendError(res,err,"server_error");
			}
			done++;
			if(done == toDo){
				return sendSuccess(res,{});
			}
		});
		OtpCallStatus.remove({
			aid : aid
		},function(err,resp){
			if(err){
				console.trace(err);
				return sendError(res,err,"server_error");
			}
			done++;
			if(done == toDo){
				return sendSuccess(res,{});
			}
		});
		OtpStatus.remove({
			aid : aid
		},function(err,resp){
			if(err){
				console.trace(err);
				return sendError(res,err,"server_error");
			}
			done++;
			if(done == toDo){
				return sendSuccess(res,{});
			}
		});
		Profile.remove({
			aid : aid
		},function(err,resp){
			if(err){
				console.trace(err);
				return sendError(res,err,"server_error");
			}
			done++;
			if(done == toDo){
				return sendSuccess(res,{});
			}
		});
		var keybruteForceAccount = RedisPrefixes.BRUTE_FORCE_ACCOUNT+aid+'';
		rhmset.removekey(keybruteForceAccount,function(err,resp){
			if(err){
				console.trace(err);
				return sendError(res,err,"server_error");
			}
			done++;
			if(done == toDo){
				return sendSuccess(res,{});
			}
		});
		var keyBruteForceGrpJn = RedisPrefixes.BRUTE_FORCE_GROUP_JOIN+aid+'';
		rhmset.removekey(keyBruteForceGrpJn,function(err,resp){
			if(err){
				console.trace(err);
				return sendError(res,err,"server_error");
			}
			done++;
			if(done == toDo){
				return sendSuccess(res,{});
			}
		});
		var keyBruteForceOtp = RedisPrefixes.BRUTE_FORCE_PHONE_OTP+p_no+'';
		rhmset.removekey(keyBruteForceOtp,function(err,resp){
			if(err){
				console.trace(err);
				return sendError(res,err,"server_error");
			}
			done++;
			if(done == toDo){
				return sendSuccess(res,{});
			}
		});
		Account.remove({
			_id : aid
		},function(err,resp){
			if(err){
				console.trace(err);
				return sendError(res,err,"server_error");
			}
			done++;
			if(done == toDo){
				return sendSuccess(res,{});
			}
		});
	});
});

/*
 * JOIN A GROUP ON POSSESSION OF A GROUP CODE
 */
router.post('/joingroup', function(req, res, next) {
	req.checkBody('code',errorCodes.invalid_parameters[1]).notEmpty().isValidGroupCode();
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();

	if(!checkValidationForCI(req.body.p_no)) { 
		return sendError(res,"server_error","server_error"); 
	}

	if(req.validationErrors()){
		logger.error({"r":"joingroup","m":"POST","msg":"invalid_parameters","p":req.body});  
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var group_code = req.body.code;
	var profile_id = req.body.p_id;
	var tim        =  new Date().getTime();

	UserInfoCache.getMongoProfile(profile_id,function(err, profile){
		if(err){ 
			logger.error({"r":"joingroup","m":"POST","msg":"profiles_server_error","p":req.body});
			return sendError(res,err,"server_error");
		}
		if(!profile){
			logger.error({"r":"joingroup","m":"POST","msg":"profile_not_found","p":req.body});
			return sendError(res,"Profile not found.","profile_not_found",constants.HTTP_STATUS.NOT_FOUND);
		}
		GroupJoinLimits.check(profile.aid, tim, function(err,bruteForceGrpJn){
			if(err){
				logger.error({"r":"joingroup","m":"POST","msg":"ValidAuthRequest_check_error","p":req.query});
				return sendError(res,"Profile not found.","profile_not_found",constants.HTTP_STATUS.NOT_FOUND); 
			}
			if(bruteForceGrpJn){
				bruteForceGrpJnlogger.error({"r":"joingroup_brute_force_found","p":{aid:profile.aid,lmt:bruteForceGrpJn.lmt,tm:bruteForceGrpJn.tm}});
				return sendError(res,"Too many requests","too_many_requests",constants.HTTP_STATUS.TOO_MANY_REQUESTS);
			}
			GroupJoinLimits.mark(profile.aid, tim, function(err,marked){
				if(err){
					logger.error({"r":"joingroup","m":"POST","msg":"GroupJoinLimits_mark_error","p":req.query});
					return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST); 
				}
				Group.findOne({
					code : group_code,
					act  : true
				},{
					__v : 0,
					cat : 0
				},function(err, group){
					if(err){ 
						logger.error({"r":"joingroup","m":"POST","msg":"groups_server_error","p":req.body});
						return sendError(res,err,"server_error");
					}
					if(!group){
						GroupJoinLimits.markError(profile.aid, tim, function(err,marked){
							if(err){
								logger.error({"r":"joingroup","m":"POST","msg":"GroupJoinLimits_markError_error","p":req.query});
							}
						});
						logger.error({"r":"joingroup","m":"POST","msg":"no_group_found","p":req.body});
						return sendError(res,"Group does not exists.","no_group_found",constants.HTTP_STATUS.NOT_FOUND);
					}
					GroupMember.find({
						gid : group._id,
						aid : profile.aid,
						act : true,
						gdel: false
					},function(err, members){
						if(err){ 
							logger.error({"r":"joingroup","m":"POST","msg":"groupmember_server_error","p":req.body});
							return sendError(res,err,"server_error");
						}
						var total_mems = members.length;
						for(var i=0; i<total_mems; i++){
							if(members[i].type == GroupMember.TYPE.BANNED){
								logger.error({"r":"joingroup","m":"POST","msg":"banned_group_member","p":req.body});
								return sendError(res,"Banned group member cannot join the group","banned_group_member",constants.HTTP_STATUS.BANNED_GROUP_MEMBER);
							}
							if(members[i].pid+'' == profile_id){
								logger.error({"r":"joingroup","m":"POST","msg":"already_member","p":req.body});
								return sendError(res,"You are already a member of the group","already_member",constants.HTTP_STATUS.ALREADY_GROUP_MEMBER);
							}
						}
						var new_member = new GroupMember({
							gid : group._id,
							pid : profile_id,
							type : GroupMember.TYPE.MEMBER,
							pnam : profile.nam.toLowerCase(),
							act : true
						});
						GroupMember.update({
							gid : group._id,
							pid : profile_id,
							act : true
						},{
							$setOnInsert : {
								gid : group._id,
								pid : profile_id,
								aid : profile.aid,
								type : GroupMember.TYPE.MEMBER,
								pnam : profile.nam.toLowerCase(),
								act : true,
								cat : new Date
							}
						},{
							upsert : true,
							setDefaultsOnInsert: true
						},function(err,gMem){
							if(err){ 
								logger.error({"r":"joingroup","m":"POST","msg":"groupmember_save_server_error","p":req.body});
								return sendError(res,err,"server_error");
							}
							rmGrpMemsFromUsrCache(profile_id,function(err,isRemoved){
								if(err){
									logger.error({"r":"joingroup","m":"POST","msg":"rmGrpMemsFromUsrCache_error","p":profile_id});
								}
								var time = new Date().getTime();
								sendSuccess(res, {"Group" : group, "GroupMember" : new_member});
								AccountInfo.validateVersionForAdminActions(profile_id, function(err, resp){
									if(resp) {
										GroupNotify.abt_grp_j(group._id, profile_id, group.code, group.name, group.gpic, group.act, group.gtyp, new_member.type, time);
									}
								});
							});
						});
					});
				});
			});
		});
	});
});

/*
 *Add New Admins in Group
 */
router.put('/gadm_a', function(req, res, next) {
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();  // User to be given the admin rights
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('admin_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId(); // User who is performing the action

	if(!checkValidationForCI(req.body.p_no)) { 
		return sendError(res,"server_error","server_error"); 
	}

	if(req.validationErrors()){ 
		logger.error({"r":"gadm_a","m":"PUT","msg":"invalid_parameters","p":req.body});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var admin_id 	    = req.body.admin_id;
	var group_id 	    = req.body.g_id;
	var profile_id    = req.body.p_id;

	AccountInfo.validateVersionForAdminActions(profile_id, function(err,resp){
		if(err){ 
			logger.error({"r":"gadm_a","m":"PUT","msg":"validateVersion_DbError","p":req.body});
			return sendError(res,err,"server_error");
		}
		if(!resp){ 
			logger.error({"r":"gadm_a","m":"PUT","msg":"lower_mobile_app_version","p":req.body});
			return sendError(res,"Target User needs to upgrade the app version","lower_mobile_app_version");
		}
		GroupInfoCache.getMongoGroup(group_id,function(err,group){
			if(err){ 
				logger.error({"r":"gadm_a","m":"PUT","msg":"group_found_server_error","p":req.body});
				return sendError(res,err,"server_error");
			}
			if(!group){
				logger.error({"r":"gadm_a","m":"PUT","msg":"no_group_found","p":req.body})
				return sendError(res,"No Group Found","no_group_found",constants.HTTP_STATUS.BAD_REQUEST);
			}
			GroupMember.find({
				gid : group_id,
				pid : { $in : [admin_id,profile_id] },
				act : true,
				gdel: false
			},function(err, members) {
				if(err){ 
					logger.error({"r":"gadm_a","m":"PUT","msg":"groupMember_found_server_error","p":req.body});
					return sendError(res,err,"server_error");
				}
				if(members.length !=2){
					logger.error({"r":"gadm_a","m":"PUT","msg":"groupmembers_not_found","p":req.body});
					return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
				}
				var admin , new_admin;
				for (var i = 0; i < 2 ; i++) {
					if(members[i].pid == admin_id) 
						admin = members[i];
					else if (members[i].pid == profile_id){
						new_admin = members[i];
					}
				}
				if(!admin){
					logger.error({"r":"gadm_a","m":"PUT","msg":"not_a_group_member","p":req.body});
					return sendError(res,"You are not a member of the Group","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
				}
				if(admin.type != GroupMember.TYPE.ADMIN){
					logger.error({"r":"gadm_a","m":"PUT","msg":"not_a_group_admin","p":req.body});
					return sendError(res,"You are not Group Administrator","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
				}
				if(!new_admin){
					logger.error({"r":"gadm_a","m":"PUT","msg":"user_not_group_member","p":req.body});
					return sendError(res,"User being added as an admin is not a member of the Group","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
				}
				if(new_admin.type == GroupMember.TYPE.ADMIN){
					logger.error({"r":"gadm_a","m":"PUT","msg":"user_already_group_admin","p":req.body});
					return sendError(res,"User being added as an admin is already an admin of the Group","already_administrator",constants.HTTP_STATUS.BAD_REQUEST);
				}
				if(new_admin.type == GroupMember.TYPE.BANNED){
					logger.error({"r":"gadm_a","m":"PUT","msg":"banned_group_member","p":req.body});
					return sendError(res,"Banned group member cannot become group admin","banned_group_member",constants.HTTP_STATUS.NOT_FOUND);
				}
				GroupMember.update({
					_id : new_admin._id,
				},{
					type : GroupMember.TYPE.ADMIN
				},function(err,admin_updated) {
					if(err){ 
						logger.error({"r":"gadm_a","m":"PUT","msg":"groupMember_update_server_error","p":req.body});
						return sendError(res,err,"server_error");
					}
					rmGrpMemsFromUsrCache(profile_id,function(err,isRemoved){
						if(err){
							logger.warn({"r":"gadm_a","m":"PUT","msg":"rmGrpMemsFromUsrCache_server_error","p":req.body});
						}
						rmGrpAdmsFromGrpCache(group_id,function(err,isRemoved){
							if(err){
								logger.warn({"r":"gadm_a","m":"PUT","msg":"rmGrpAdmsFromGrpCache_server_error","p":req.body});
							}
							UserInfoCache.removeMongoGrpMem(profile_id, group_id,function(err, rRemoved){
								if(err){
									logger.warn({"r":"gadm_a","m":"PUT","msg":"removeMongoGrpMem_server_error","p":req.body});
								}
								var time = new Date().getTime();
								sendSuccess(res,{"GroupMember" : admin_updated});
								notify_admin_ad_rm(group_id, admin_id, profile_id, Actions.GROUP_ADMIN_ADD, group.name, group.gtyp, time);
							});
						});
					});
				});
			});
		});
	});
});


/*
 *Remove an Admin From the Group
 */
router.put('/gadm_r', function(req, res, next) {
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();  // User whose admin rights ae being removed
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('admin_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId(); // User who is performing this action

	if(!checkValidationForCI(req.body.p_no)) { 
		return sendError(res,"server_error","server_error"); 
	}

	if(req.validationErrors()){ 
		logger.error({"r":"gadm_r","m":"PUT","msg":"invalid_parameters","p":req.body});
		sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var admin_id 	  = req.body.admin_id;
	var group_id 	  = req.body.g_id;
	var profile_id  = req.body.p_id;
	
	AccountInfo.validateVersionForAdminActions(profile_id, function(err, resp){
		if(err){
		 	logger.error({"r":"gadm_r","m":"PUT","msg":"validateVersion_DbError","p":req.body});
			return sendError(res,err,"server_error");
		}
		if(!resp){ 
			logger.error({"r":"gadm_r","m":"PUT","msg":"lower_mobile_app_version","p":req.body});
			return sendError(res,"Target User needs to upgrade the app version","lower_mobile_app_version");
		}
		GroupInfoCache.getMongoGroup(group_id,function(err,group){
			if(err){
			 	logger.error({"r":"gadm_r","m":"PUT","msg":"groupMember_find_server_error","p":req.body});
				return sendError(res,err,"server_error");
			}
			if(!group){
				logger.error({"r":"gadm_r","m":"PUT","msg":"no_group_found","p":req.body});
				return sendError(res,"No Group Found","no_group_found",constants.HTTP_STATUS.BAD_REQUEST);
			}
			GroupMember.find({
				gid : group_id,
				pid : { $in : [admin_id,profile_id]},
				act : true,
				gdel: false
			},function(err, members) {
				if(err){
				 	logger.error({"r":"gadm_r","m":"PUT","msg":"validateVersion_DbError","p":req.body});
					return sendError(res,err,"server_error");
				}
				if(members.length !=2){
					logger.error({"r":"gadm_r","m":"PUT","msg":"groupmembers_not_found","p":req.body});
					return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
				}
				var admin , admin_to_remove;
				for (var i = 0; i < 2 ; i++) {
					if(members[i].pid == admin_id) 
						admin = members[i];
					else if (members[i].pid == profile_id){
						admin_to_remove 		= members[i];
					}
				}
				if(!admin){
					logger.error({"r":"gadm_r","m":"PUT","msg":"not_group_member","p":req.body});
					return sendError(res,"You are not a member of the Group","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
				}
				if(admin.type != GroupMember.TYPE.ADMIN){
					logger.error({"r":"gadm_r","m":"PUT","msg":"not_a_group_admin","p":req.body});
					return sendError(res,"You are not Group Administrator","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
				}
				if(!admin_to_remove){
					logger.error({"r":"gadm_r","m":"PUT","msg":"user_not_group_member","p":req.body});
					return sendError(res,"User being removed from admin is not a member of the Group","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
				}

				if(admin_to_remove.type != GroupMember.TYPE.ADMIN){
					logger.error({"r":"gadm_r","m":"PUT","msg":"user_not_group_admin","p":req.body});
					return sendError(res,"User Being removed from admin is not an admin","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
				}
				GroupMember.count({
					gid : group_id,
					act : true
				},function(err,total_admins_count){
					if(err){
					 	logger.error({"r":"gadm_r","m":"PUT","msg":"groupMember_count_server_error","p":req.body});
						return sendError(res,err,"server_error");
					}
					if(total_admins_count < 2){
						logger.error({"r":"gadm_r","m":"PUT","msg":"group_single_admin","p":req.body});
						return sendError(res,"Group has only one admin active at the moment","group_single_admin",constants.HTTP_STATUS.BAD_REQUEST);
					}
					GroupMember.update({
						_id : admin_to_remove._id,
						act : true
					},{
						type : GroupMember.TYPE.MEMBER
					},function(err,removed_group_admin) {
						if(err){
						 	logger.error({"r":"gadm_r","m":"PUT","msg":"groupMember_update_server_error","p":req.body});
							return sendError(res,err,"server_error");
						}
						rmGrpMemsFromUsrCache(profile_id,function(err,isRemoved){
							if(err){
								logger.warn({"r":"gadm_r","m":"PUT","msg":"rmGrpMemsFromUsrCache_server_error","p":req.body});
							}
							rmGrpAdmsFromGrpCache(group_id,function(err,isRemoved){
								if(err){
									logger.warn({"r":"gadm_r","m":"PUT","msg":"rmGrpAdmsFromGrpCache_server_error","p":req.body});
								}
								UserInfoCache.removeMongoGrpMem(profile_id, group_id,function(err, rRemoved){
									if(err){
										logger.warn({"r":"gadm_r","m":"PUT","msg":"removeMongoGrpMem_server_error","p":req.body});
									}
									var time = new Date().getTime();
									sendSuccess(res, {data : removed_group_admin});
									notify_admin_ad_rm(group_id, admin_id, profile_id, Actions.GROUP_ADMIN_REM, group.name, group.gtyp, time);
								});
							});
						});
					});
				});
			});
		});
	});
});


/*
 * BAN A MEMBER : 1=>ADMIN 2=>MEMBER 3=>BANNED
 */
router.put('/gmem_b', function(req, res, next) {
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('admin_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();

	if(!checkValidationForCI(req.body.p_no)) { 
		return sendError(res,"server_error","server_error"); 
	}

	if(req.validationErrors()){
		logger.error({"r":"gmem_b","m":"PUT","msg":"invalid_parameters","p":req.body}); 
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var group_id 		  = req.body.g_id;
	var admin_id 		  = req.body.admin_id;
	var profile_id 		= req.body.p_id;

	AccountInfo.validateVersionForAdminActions(profile_id, function(err,resp){
		if(err){ 
			logger.error({"r":"gmem_b","m":"PUT","msg":"validateVersion_DbError","p":req.body});
			return sendError(res,err,"server_error"); 
		}
		if(!resp){ 
			logger.error({"r":"gmem_b","m":"PUT","msg":"lower_mobile_app_version","p":req.body});
			return sendError(res,"Target User needs to upgrade the app version","lower_mobile_app_version"); 
		} 
		GroupInfoCache.getMongoGroup(group_id,function(err,group){
			if(err) { 
				logger.error({"r":"gmem_b","m":"PUT","msg":"getMongoGroup_error","p":req.body});
				return sendError(res,err,"server_error");
			}
			if(!group){
				logger.error({"r":"gmem_b","m":"PUT","msg":"no_group_found","p":req.body});
				return sendError(res,"Group does not exists.","no_group_found",constants.HTTP_STATUS.NOT_FOUND);
			}
			UserInfoCache.getMongoProfile(profile_id,function(err,profile){
				if(err){ 
					logger.error({"r":"gmem_b","m":"PUT","msg":"getMongoProfile_server_error","p":req.body});
					return sendError(res,err,"server_error");  
				}
				if(!profile){ 
					logger.error({"r":"gmem_b","m":"PUT","msg":"getMongoProfile_not_found","p":req.body});
					return sendError(res,err,"server_error");  
				}
				GroupMember.find({
					gid : group_id,
					pid : { $in : [admin_id,profile_id] },
					act : true,
					gdel: false
				},function(err,members){
					if(err) { 
						logger.error({"r":"gmem_b","m":"PUT","msg":"groupMember_find_server_error","p":req.body});
						return sendError(res,err,"server_error");  
					}
					if(members.length !=2){
						logger.error({"r":"gmem_b","m":"PUT","msg":"groupmembers_not_found","p":req.body});
						return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
					}
					var members_by_pid    = {};
					var gmIdsForUpdate    = [];
					var targetProfilePids = [];
					var total_mems        = members.length;
					var tempPid;
					for(var i=0; i<total_mems; i++){
						tempPid = members[i].pid+'';
						if(tempPid == profile_id || tempPid == admin_id)
							members_by_pid[tempPid] = members[i];
						if(members[i].aid+'' == profile.aid+''){
							gmIdsForUpdate.push(members[i]._id);
							targetProfilePids.push(tempPid);
						}
					}
					if(!members_by_pid[admin_id]){
						logger.error({"r":"gmem_b","m":"PUT","msg":"not_a_group_member","p":req.body});
						return sendError(res,"User not a member of the Group","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
					}
					if(!members_by_pid[profile_id]){
						logger.error({"r":"gmem_b","m":"PUT","msg":"profile_not_a_group_member","p":req.body});
						return sendError(res,"Profile to be banned is not member of the group.","not_a_member",constants.HTTP_STATUS.BAD_REQUEST);
					}
					if(members_by_pid[profile_id].aid+'' == members_by_pid[admin_id].aid+''){
						logger.error({"r":"gmem_b","m":"PUT","msg":"cannot_ban_your_own_profile","p":req.body});
						return sendError(res,"Cannot ban your own profile","cannot_ban_your_own_profile",constants.HTTP_STATUS.BAD_REQUEST);
					}
					if(members_by_pid[admin_id].type != GroupMember.TYPE.ADMIN){
						logger.error({"r":"gmem_b","m":"PUT","msg":"not_a_group_admin","p":req.body});
						return sendError(res,"User not an Administrator of the Group","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
					}
					if(members_by_pid[profile_id].type == GroupMember.TYPE.ADMIN){
						logger.error({"r":"gmem_b","m":"PUT","msg":"cannot_ban_administrator","p":req.body});
						return sendError(res,"Cannot ban an Administrator","cannot_ban_administrator",constants.HTTP_STATUS.BAD_REQUEST);
					}
					if(members_by_pid[profile_id].type == GroupMember.TYPE.BANNED){
						logger.error({"r":"gmem_b","m":"PUT","msg":"already_banned","p":req.body});
						return sendError(res,"Profile is already banned","already_banned",constants.HTTP_STATUS.BAD_REQUEST);
					}
					GroupMember.update({
						_id : { $in : gmIdsForUpdate }
					},{
						type : GroupMember.TYPE.BANNED
					},{
						multi: true
					},function(err,banned_member){
						if(err){  
							logger.error({"r":"gmem_b","m":"PUT","msg":"groupMember_update_server_error","p":req.body});
							return sendError(res,err,"server_error",constants.HTTP_STATUS.SERVER_ERROR);
						}
						UserInfoCache.rmUsrGrpMemsByAidInCache(profile.aid,function(err,isRemoved){
							if(err){
								logger.warn({"r":"gmem_b","m":"PUT","msg":"rmGrpMemsFromUsrCache_error","p":req.body});
							}
							UserInfoCache.removeMultiMongoGrpMem(targetProfilePids, group_id,function(err, rRemoved){
								if(err){
									logger.warn({"r":"gmem_b","m":"PUT","msg":"removeMultiMongoGrpMem","p":req.body});
								}
								var time = new Date().getTime();
								sendSuccess(res, {data:banned_member});
								return notify_grp_ban_member(group_id, group, profile_id, admin_id, time);
							});
						});
					});
				});
			});
		});
  });
});

/*
 * UNBAN A MEMBER : 1=>ADMIN 2=>MEMBER 3=>BANNED
 */
router.put('/gmem_ub', function(req, res, next) {
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('admin_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();

	if(!checkValidationForCI(req.body.p_no)) { 
		return sendError(res,"server_error","server_error"); 
	}

	if(req.validationErrors()){ 
		logger.error({"r":"gmem_ub","m":"PUT","msg":"invalid_parameters","p":req.body});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var group_id 		  = req.body.g_id;
	var admin_id 		  = req.body.admin_id;
	var profile_id 		= req.body.p_id;

	AccountInfo.validateVersionForAdminActions(profile_id, function(err,resp){
		if(err){ 
			logger.error({"r":"gmem_ub","m":"PUT","msg":"validateVersion_DbError","p":req.body});
			return sendError(res,err,"server_error"); 
		}
		if(!resp){
			logger.error({"r":"gmem_ub","m":"PUT","msg":"lower_mobile_app_version","p":req.body}); 
			return sendError(res,"Target User needs to upgrade the app version","lower_mobile_app_version"); 
		} 
		GroupInfoCache.getMongoGroup(group_id,function(err,group){
			if(err) { 
				logger.error({"r":"gmem_ub","m":"PUT","msg":"getMongoGroup_error","p":req.body});
				return sendError(res,err,"server_error");
			}
			if(!group){
				logger.error({"r":"gmem_ub","m":"PUT","msg":"no_group_found","p":req.body});
				return sendError(res,"Group does not exists.","no_group_found",constants.HTTP_STATUS.NOT_FOUND);
			}
			GroupMember.find({
				gid : group_id,
				pid : { $in : [admin_id,profile_id] },
				act : true,
				gdel: false
			},function(err,members){
				if(err){ 
					logger.error({"r":"gmem_ub","m":"PUT","msg":"groupMember_find_server_error","p":req.body});
					return sendError(res,err,"server_error"); 
				}
				if(members.length !=2){
					logger.error({"r":"gmem_ub","m":"PUT","msg":"groupmembers_not_found","p":req.body});
					return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
				}
				var members_by_pid = {};
				for(var key in members) {
					members_by_pid[members[key]["pid"]] = members[key];
				}
				if(!members_by_pid[admin_id]){
					logger.error({"r":"gmem_ub","m":"PUT","msg":"user_not_group_member","p":req.body});
					return sendError(res,"User not a member of the Group","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
				}
				if(!members_by_pid[profile_id]){
					logger.error({"r":"gmem_ub","m":"PUT","msg":"profile_not_a_group_member","p":req.body});
					return sendError(res,"Profile to be promoted is not member of the group.","not_a_member",constants.HTTP_STATUS.BAD_REQUEST);
				}
				if(members_by_pid[admin_id].type != GroupMember.TYPE.ADMIN){
					logger.error({"r":"gmem_ub","m":"PUT","msg":"not_a_group_admin","p":req.body});
					return sendError(res,"User not an Administrator of the Group","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
				}
				if(members_by_pid[profile_id].type == GroupMember.TYPE.MEMBER){
					logger.error({"r":"gmem_ub","m":"PUT","msg":"not_banned","p":req.body});
					return sendError(res,"Profile is not banned","not_banned",constants.HTTP_STATUS.BAD_REQUEST);
				}
				GroupMember.update({
					gid : group_id,
					pid : profile_id,
					act : true,
					gdel: false
				},{
					type : GroupMember.TYPE.MEMBER
				},function(err,unbanned_member){
					if(err){
						logger.error({"r":"gmem_ub","m":"PUT","msg":"groupMember_update_server_error","p":req.body});  
						return sendError(res,err,"server_error",constants.HTTP_STATUS.SERVER_ERROR);
					}
					rmGrpMemsFromUsrCache(profile_id,function(err,isRemoved){
						if(err){
							logger.warn({"r":"gmem_ub","m":"PUT","msg":"rmGrpMemsFromUsrCache_error","p":req.body});
						}
						UserInfoCache.removeMongoGrpMem(profile_id, group_id,function(err, rRemoved){
							if(err){
								logger.warn({"r":"gmem_ub","m":"PUT","msg":"removeMongoGrpMem_error","p":req.body});
							}
							var time = new Date().getTime();
							sendSuccess(res, {data:unbanned_member});
							return notify_grp_unban_member(group_id, group, profile_id, admin_id, time);
						});
					});
				});
			});
		});
  });
});

/*
 * Delete a Group Member
 */
router.put('/gmem_d', function(req, res, next) {
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('admin_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();

	if(!checkValidationForCI(req.body.p_no)) { 
		return sendError(res,"server_error","server_error"); 
	}

	if(req.validationErrors()){ 
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var group_id 		  = req.body.g_id;
	var admin_id 		  = req.body.admin_id;
	var profile_id 		= req.body.p_id;
	
	AccountInfo.validateVersionForAdminActions(profile_id, function(err,resp){
		if(err){ 
			logger.error({"r":"gmem_d","m":"PUT","msg":"validateVersion_DbError","p":req.body});
			return sendError(res,err,"server_error"); 
		}
		if(!resp){
			logger.error({"r":"gmem_d","m":"PUT","msg":"lower_mobile_app_version","p":req.body}); 
			return sendError(res,"Target User needs to upgrade the app version","lower_mobile_app_version"); 
		} 
		GroupInfoCache.getMongoGroup(group_id,function(err,group){
			if(err) { 
				logger.error({"r":"gmem_d","m":"PUT","msg":"getMongoGroup_error","p":req.body});
				return sendError(res,err,"server_error");
			}
			if(!group){
				logger.error({"r":"gmem_d","m":"PUT","msg":"no_group_found","p":req.body});
				return sendError(res,"Group does not exists.","no_group_found",constants.HTTP_STATUS.NOT_FOUND);
			}
			GroupMember.find({
				gid : group_id,
				pid : { $in : [admin_id,profile_id]},
				act : true,
				gdel: false
			},function(err,members){
				if(err){ 
					logger.error({"r":"gmem_d","m":"PUT","msg":"groupMember_find_server_error","p":req.body});
					return sendError(res,err,"server_error"); 
				}
				if(members.length < 2){
					logger.error({"r":"gmem_d","m":"PUT","msg":"user_and_admin_members_not_found","p":req.body});
					return sendError(res,"User and Admin not found in groupmembers","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
				}
				var admin , member_to_delete;
				for (var i = 0; i < 2 ; i++) {
					if(members[i].pid == admin_id) 
						admin = members[i];
					else if (members[i].pid == profile_id){
						member_to_delete 		= members[i];
					}
				}
				if(!admin){
					logger.error({"r":"gmem_d","m":"PUT","msg":"user_not_group_member","p":req.body});
					return sendError(res,"User not a member of the Group","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
				}
				if(admin.type != GroupMember.TYPE.ADMIN){
					logger.error({"r":"gmem_d","m":"PUT","msg":"user_not_group_admin","p":req.body});
					return sendError(res,"User not an Administrator of the Group","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
				}
				if(!member_to_delete){
					logger.error({"r":"gmem_d","m":"PUT","msg":"profile_not_a_group_member","p":req.body});
					return sendError(res,"Profile to be deleted is not a member of the group.","not_a_member",constants.HTTP_STATUS.BAD_REQUEST);
				}
				if(member_to_delete.type == GroupMember.TYPE.ADMIN){
					logger.error({"r":"gmem_d","m":"PUT","msg":"group_admin_cannot_be_deleted","p":req.body});
					return sendError(res,"Group admin cannot be deleted directly","group_admin",constants.HTTP_STATUS.BAD_REQUEST);
				}
				if(member_to_delete.type == GroupMember.TYPE.BANNED){
					logger.error({"r":"gmem_d","m":"PUT","msg":"banned_group_member","p":req.body});
					return sendError(res,"Banned member cannot be deleted directly","banned_group_member",constants.HTTP_STATUS.BAD_REQUEST);
				}
				GroupMember.update({
					_id : member_to_delete._id,
					act : true
				},{
					act : false
				},function(err,deleted_group_member) {
					if(err){ 
						logger.error({"r":"gmem_d","m":"PUT","msg":"groupMember_update_server_error","p":req.body});
						return sendError(res,err,"server_error"); 
					}
					rmGrpMemsFromUsrCache(profile_id,function(err,isRemoved){
						if(err){
							logger.warn({"r":"gmem_d","m":"PUT","msg":"rmGrpMemsFromUsrCache_error","p":req.body});
						}					
						UserInfoCache.removeMongoGrpMem(profile_id, group_id,function(err, rRemoved){
							if(err){
								logger.warn({"r":"gmem_d","m":"PUT","msg":"removeMongoGrpMem_error","p":req.body});
							}
							var time = new Date().getTime();
							sendSuccess(res, {data : deleted_group_member});
							return notify_grp_del_member(group_id, group, profile_id, admin_id, time);
						});					
					});
				});
			});
		});
  });
});

/*
 *Leave A GROUP
 */
router.put('/g_l', function(req, res, next) {
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();

	if(!checkValidationForCI(req.body.p_no)) { 
		return sendError(res,"server_error","server_error"); 
	}

	if(req.validationErrors()){ 
		logger.error({"r":"g_l","m":"PUT","msg":"invalid_parameters","p":req.body});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST); 
	}

	var group_id 			= req.body.g_id;
	var profile_id 		= req.body.p_id;

	GroupInfoCache.getMongoGroup(group_id,function(err,group){
		if(err){
			logger.error({"r":"g_l","m":"PUT","msg":"group_find_server_error","p":req.body}); 
			return sendError(res,err,"server_error");
		}
		if(!group){
			logger.error({"r":"g_l","m":"PUT","msg":"no_group_found","p":req.body}); 
			return sendError(res,"Group not found","no_group_found",constants.HTTP_STATUS.NOT_FOUND);
		}
		GroupMember.findOne({
			gid : group_id,
			pid : profile_id,
			act : true,
			gdel: false
		},function(err, leave_group){
			if(err){
				logger.error({"r":"g_l","m":"PUT","msg":"groupmember_find_server_error","p":req.body}); 
				return sendError(res,err,"server_error");
			}
			if(!leave_group){
				logger.error({"r":"g_l","m":"PUT","msg":"not_group_member","p":req.body}); 
				return sendError(res,"You are not a member of the group.","not_a_member",constants.HTTP_STATUS.NOT_FOUND);
			}
			if(leave_group.type == GroupMember.TYPE.ADMIN){
				logger.error({"r":"g_l","m":"PUT","msg":"group_admin_cannot_leave","p":req.body}); 
				return sendError(res,"Group Admin cannot leave the group","group_admin_cannot_leave",constants.HTTP_STATUS.NOT_FOUND);
			}
			if(leave_group.type == GroupMember.TYPE.BANNED){
				logger.error({"r":"g_l","m":"PUT","msg":"banned_group_member","p":req.body}); 
				return sendError(res,"Banned member cannot leave the group","banned_group_member",constants.HTTP_STATUS.NOT_FOUND);
			}
			GroupMember.update({
				_id : leave_group._id
			},{
				act : false
			},function(err,left_group_member){
				if(err){ 
					logger.error({"r":"g_l","m":"PUT","msg":"groupMember_update_server_error","p":req.body}); 
					sendError(res,err,"server_error");
				}
				rmGrpMemsFromUsrCache(profile_id,function(err,isRemoved){
					if(err){
						logger.warn({"r":"g_l","m":"PUT","msg":"rmGrpMemsFromUsrCache_error","p":req.body}); 
					}
					UserInfoCache.removeMongoGrpMem(profile_id, group_id,function(err, rRemoved){
						if(err){
							logger.warn({"r":"g_l","m":"PUT","msg":"removeMongoGrpMem_error","p":req.body}); 
						}
						var time = new Date().getTime();
						sendSuccess(res, {data : left_group_member});
						return notify_grp_lv(group_id, profile_id, group.name, time);
					});
				});
			});
		});
	});
});

function notify_admin_ad_rm(group_id, admin_id, profile_id, action, gname, gtyp, time) {
	Profile.find({
		_id : { $in : [profile_id,admin_id]},
		act : true
	},function(err,profiles){
		if(err) {
			console.log('In Remove Group Admin : MongoDb Error - Profiles do not exist');
			return;
		}
		if(profiles.length < 2) {
			console.log('In notify_target : Profiles do not exist');
			return;
		}
		var admin_name,profile_name,admin_role;
		for (var i = 0; i < 2 ; i++) {
			if(profiles[i]._id == admin_id){ 
				admin_name = profiles[i].nam;
				admin_role = profiles[i].role;
			}	else {
				profile_name = profiles[i].nam;
			}
		}
		if (action == Actions.GROUP_ADMIN_ADD) {
			var notify_msg = admin_name+' has made '+profile_name+' an admin';
			GroupNotify.abt_gadm_a(group_id, admin_id, profile_id, notify_msg, gname, gtyp, time);
		} else if (action == Actions.GROUP_ADMIN_REM) {
			var notify_msg = admin_name+' has removed '+profile_name+'\'s admin rights';
			GroupNotify.abt_gadm_r(group_id, profile_id, admin_id, admin_name, admin_role, notify_msg, gname, time);
		}
	});
}

function notify_grp_lv(group_id, profile_id, gname, time) {
	UserInfoCache.getMongoProfile(profile_id,function(err,profile){
		if(err) { 
			console.log('In Leave Group : MongoDb Error - Profiles do not exist');
			return;
		}
		if(!profile) {
			console.log('In notify_target : Profile does not exist');
			return;
		}
		var notify_msg = profile.nam+' has left the group';
		GroupNotify.abt_g_l(group_id, profile_id, profile.nam, profile.role, notify_msg, gname, time);
	});
}

function notify_grp_ban_member(group_id, group, profile_id, admin_id, time){
		Profile.find({
			_id : { $in : [profile_id,admin_id]},
			act : true
		},function(err,profiles){
			if(err) {
				console.log('In Banning Group Member : MongoDb Error - Profiles do not exist');
				return;
			}
			if(profiles.length < 2) {
				console.log('In notify_target : Profiles do not exist');
				return;
			}
			var admin_name,profile_name,admin_role;
			for (var i = 0; i < 2 ; i++) {
				if(profiles[i]._id == admin_id){
					admin_name = profiles[i].nam;
					admin_role = profiles[i].role;
				} else {
					profile_name = profiles[i].nam;
				}
			}
			var notify_msg = profile_name+' has been banned by '+admin_name;
			GroupNotify.abt_gmem_ban(group_id, profile_id, admin_id, admin_name, admin_role, notify_msg, group.name, group.gtyp, time);
		});
}

function notify_grp_unban_member(group_id, group, profile_id, admin_id, time){
	Profile.find({
		_id : { $in : [profile_id,admin_id]},
		act : true
	},function(err,profiles){
		if(err) {
			console.log('In Banning Group Member : MongoDb Error - Profiles do not exist');
			return;
		}
		if(profiles.length < 2) {
			console.log('In notify_target : Profiles do not exist');
			return;
		}
		var admin_name,profile_name,admin_role;
		for (var i = 0; i < 2 ; i++) {
			if(profiles[i]._id == admin_id){ 
				admin_name = profiles[i].nam;
				admin_role = profiles[i].role;
			}	else profile_name = profiles[i].nam;
		}
		var notify_msg = profile_name+' has been unbanned by '+admin_name;
		GroupNotify.abt_gmem_unban(group_id, profile_id, admin_id, admin_name, admin_role, notify_msg, group.name, time);
	});
}

function notify_grp_del_member(group_id, group, profile_id, admin_id, time){
	Profile.find({
		_id : { $in : [profile_id,admin_id]},
		act : true
	},function(err,profiles){
		if(err) {
			console.log('In Group Member Delete: MongoDb Error - Profiles do not exist');
			return;
		}
		if(profiles.length < 2) {
			console.log('In notify_target : Profiles do not exist');
			return;
		}
		var admin_name,profile_name,admin_role;
		for (var i = 0; i < 2 ; i++) {
			if(profiles[i]._id == admin_id){ 
				admin_name = profiles[i].nam;
				admin_role = profiles[i].role;
			}	else profile_name = profiles[i].nam;
		}
		var notify_msg = profile_name+' has been removed by '+admin_name;
		GroupNotify.abt_gmem_del(group_id, profile_id, admin_id, admin_name, admin_role ,notify_msg, group.name, time);
	});
}

function rmGrpMemsFromUsrCache(pid, cb){
	UserInfoCache.getMongoProfile(pid,function(err, profile){
		if(err){
			return cb(err,null);
		}
		var key = RedisPrefixes.USER_PROF_GMS+profile.aid+'';
		rhmset.keyexists(key,UserRedisKeys.GROUPS,function(err,isKey){
			if(err){
				return cb(err,null);
			}
			if(isKey){
				rhmset.removefield(key,UserRedisKeys.GROUPS,function(err,resp){
					if(err){
						return cb(err,null);
					} 
					if(resp){
						return cb(null,true);
					} else {
						console.log("Oops,this is unfortunate that Account Profiles key could not be deleted for Account : "+profile.aid);
						return cb(null,false);
					}
				});
			} else {
				console.log("Oops,this is unfortunate that Account Profiles key does not exists for Account : "+profile.aid);
				return cb(null,false);
			}
		});
	});
}

function rmGrpAdmsFromGrpCache(gid, cb){
	var key = RedisPrefixes.GROUP_INFO+gid+'';
	rhmset.keyexists(key,GroupRedisKeys.ADMINS,function(err,isKey){
		if(err){
			return cb(err,null);
		}
		if(isKey){
			rhmset.removefield(key,GroupRedisKeys.ADMINS,function(err,resp){
				if(err){
					return cb(err,null);
				} 
				if(resp){
					return cb(null,true);
				} else {
					console.log("Oops,this is unfortunate that Group Admins HMSET key could not be deleted for gid : "+gid);
					return cb(null,false);
				}
			});
		} else {
			console.log("Oops,this is unfortunate that Group Admins HMSET key does not exists for Account : "+gid);
			return cb(null,false);
		}
	});
}

function checkValidationForCI(p_no){
	console.log('Environment is : '+process.env.NODE_ENV);
	console.log('Hostname is : '+hostname);
	if(hostname == "ci" || hostname == "dapi" || hostname == "Akshats-MacBook-Pro.local" || hostname == "niranjan-pc") {
		if(process.env.NODE_ENV == "ci" ||  process.env.NODE_ENV == "dapi"){
			if(AllowedPhoneNumbers.indexOf(p_no) >=0){
				return true;
			}
			return false;
		}
	} else {
		return false;
	}
}

module.exports = router;