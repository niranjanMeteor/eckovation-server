var express 										= require('express');
var mongoose 										= require('mongoose');
var jwt    											= require('jsonwebtoken');
var redis               				= require('redis');

var configs											= require('../utility/configs.js');
var errorCodes 									= require('../utility/errors.js');
var constants										= require('../utility/constants.js');
var helpers											= require('../utility/helpers.js');
var AppClients									= require('../utility/app_clients.js');
var log4jsLogger  							= require('../loggers/log4js_module.js');
var log4jsConfigs  			    		= require('../loggers/log4js_configs.js');
var RedisPrefixes       				= require('../utility/redis_prefix.js');
var AuthModule          				= require('../utility/auth/auth_tokens.js');
var OtpModule          		  		= require('../utility/otp/otp_module.js');
var UnsecureAccountsModule  		= require('../utility/auth/unsecure_accounts.js');
var UserqueueAttempts         = require('../security/userqueue/attempts.js');
var MailClient              		= require('../utility/mail/mail_client.js');

var redis_client        		= redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
  console.log("Error " + err);
});

require('../models/accounts.js');
require('../models/accounttokens.js');
require('../models/unsecureaccounts.js');

var Account 								= mongoose.model('Account');
var AccountToken  					= mongoose.model('AccountToken');
var UnsecureAccount 				= mongoose.model('UnsecureAccount');

var rmc                 		= require("../utility/redis_mongo_cache.js")(redis_client);
var rhmset              		= require("../utility/redis_hmset.js")(redis_client);

var sendError 							= helpers.sendError;
var sendSuccess 						= helpers.sendSuccess;
var router 									= express.Router();
var logger        					= log4jsLogger.getLogger(log4jsConfigs.CATEGORY_ROUTES_AUTH);

var ValidMobileDevicesType 	= [AccountToken.CLIENT.MOBILE,AccountToken.CLIENT.ANDROID,AccountToken.CLIENT.IOS];

/*
 * get access token request for android
 */
router.post('/at_a', function(req, res, next) {
	req.checkBody('a_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('d_id',errorCodes.invalid_parameters[1]).notEmpty().isValidDeviceId();
	req.checkBody('cl',errorCodes.invalid_parameters[1]).notEmpty().isValidTokenClient();
	req.checkBody('rt',errorCodes.invalid_parameters[1]).notEmpty().isValidRefreshToken();
	req.checkBody('at',errorCodes.invalid_parameters[1]).notEmpty().isValidAccessToken();
	req.checkBody('k_ey',errorCodes.invalid_parameters[1]).notEmpty().isValidTokenSecretKey();

	if(req.validationErrors()){
		logger.error({"r":"at_a","m":"POST","msg":"invalid_parameters","p":req.body}); 
		MailClient.informTechTeam("AT_A_INVALID_PARAMETER",req.body);
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var aid		    				= req.body.a_id;
	var device_id 				= req.body.d_id;
	var client    				= req.body.cl;
	var refresh_token 		= req.body.rt;
	var access_token  		= req.body.at;
	var secret_key    		= req.body.k_ey;
	var tim       				= new Date().getTime();

	if(client != AppClients.ANDROID){
		logger.error({"r":"at_a","m":"POST","msg":"client parameter is not android","p":req.body});
		MailClient.informTechTeam("AT_A_NOT_ANDRD",req.body); 
		return sendError(res,"Access Token Denied","access_token_denied",constants.HTTP_STATUS.BAD_REQUEST);
	}

	jwt.verify(access_token, configs.JWT_ACCESS_TOKEN_PRIVATE_KEY, function(err, at_decoded) { 
		if(err){
			if(err.name == "TokenExpiredError" && jwt.decode(access_token).id == aid){
				jwt.verify(refresh_token, configs.JWT_REFRESH_TOKEN_PRIVATE_KEY, function(err, rt_decoded) {      
			    if(err){
			    	logger.error({"r":"at_a","m":"POST","msg":"refresh_token_expired","p":req.body}); 
			    	MailClient.informTechTeam("AT_A_REFRESH_TOKEN_EXP_ANDROID",req.body); 
			      return sendError(res,"Refresh Token expired","refresh_token_expired",constants.HTTP_STATUS.TOKEN_EXPIRED);  
			    } else {
			    	if(rt_decoded.id !== aid){
			    		logger.error({"r":"at_a","m":"POST","msg":"access_token_denied","p":req.body}); 
			    		MailClient.informTechTeam("AT_A_RT_AID_NOT_MATCHED",req.body);
			    		return sendError(res,"Access Token Denied","access_token_denied",constants.HTTP_STATUS.BAD_REQUEST);
			    	}
						AccountToken.findOne({
							aid : aid,
							// udid: device_id,
							clnt: client,
							act : true,
							tokn: refresh_token,
							revk: false,
							skey: secret_key
						},function(err,account_token){
							if(err){ 
								logger.error({"r":"at_a","m":"POST","msg":"accounttoken_find_server_error","p":req.body}); 
								return sendError(res,err,"server_error"); 
							}
							if(!account_token) { 
								logger.error({"r":"at_a","m":"POST","msg":"invalid_refresh_token","p":req.body}); 
								MailClient.informTechTeam("AT_A_INVALID_RT",req.body);
								return sendError(res,"Refresh Token Invalid!","invalid_refresh_token",constants.HTTP_STATUS.BAD_REQUEST); 
							}
							var new_access_token = AuthModule.getAT({id: aid,cl: client}, client);
							return sendSuccess(res,{ aid : aid, at : new_access_token});
						});
					}
			  });
			} else {
				logger.error({"r":"at_a","m":"POST","msg":"invalid_AT","p":req.body});
				MailClient.informTechTeam("AT_A_INVALID_AT",req.body); 
				return sendError(res,"invalid AT","invalid_AT",constants.HTTP_STATUS.BAD_REQUEST);
			}
		} else if(at_decoded.id == aid){
			return sendSuccess(res,{ aid : aid, at : access_token});
		} else {
			logger.error({"r":"at_a","m":"POST","msg":"not_authorized","p":req.body});
			MailClient.informTechTeam("AT_A_NOT_AUTHORIZED",req.body); 
			return sendError(res,"not authorized","not_authorized",constants.HTTP_STATUS.UNAUTHORIZED);
		}
	});
});

/*
 * get acces token request for web
 */
router.post('/at_w', function(req, res, next) {
	req.checkBody('a_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('d_id',errorCodes.invalid_parameters[1]).notEmpty().isValidDeviceId();
	req.checkBody('cl',errorCodes.invalid_parameters[1]).notEmpty().isValidTokenClient();
	req.checkBody('rt',errorCodes.invalid_parameters[1]).notEmpty().isValidRefreshToken();
	req.checkBody('at',errorCodes.invalid_parameters[1]).notEmpty().isValidAccessToken();
	req.checkBody('k_ey',errorCodes.invalid_parameters[1]).notEmpty().isValidTokenSecretKey();

	if(req.validationErrors()){
		logger.error({"r":"at_w","m":"POST","msg":"invalid_parameters","p":req.body});
		MailClient.informTechTeam("AT_W_INVALID_PARAMETER",req.body); 
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var aid		    		= req.body.a_id;
	var device_id 		= req.body.d_id;
	var client    		= req.body.cl;
	var refresh_token = req.body.rt;
	var access_token  = req.body.at;
	var secret_key    = req.body.k_ey;
	var tim       		= new Date().getTime();

	if(client != AppClients.WEB){
		logger.error({"r":"at_w","m":"POST","msg":"client parameter is not web","p":req.body});
		MailClient.informTechTeam("AT_W_NOT_WEB",req.body);
		return sendError(res,"Access Token Denied","access_token_denied",constants.HTTP_STATUS.BAD_REQUEST);
	}

	jwt.verify(access_token, configs.JWT_ACCESS_TOKEN_PRIVATE_KEY, function(err, at_decoded) { 
		if(err){
			if(err.name == "TokenExpiredError" && jwt.decode(access_token).id == aid){
				jwt.verify(refresh_token, configs.JWT_REFRESH_TOKEN_PRIVATE_KEY, function(err, rt_decoded) {      
			    if(err){
			    	logger.error({"r":"at_w","m":"POST","msg":"refresh_token_expired","p":req.body}); 
			    	MailClient.informTechTeam("AT_W_REFRESH_TOKEN_EXP_WEB",req.body); 
			      return sendError(res,"Refresh Token expired","refresh_token_expired",constants.HTTP_STATUS.TOKEN_EXPIRED);  
			    } else {
			    	if(rt_decoded.id !== aid){
			    		logger.error({"r":"at_w","m":"POST","msg":"access_token_denied","p":req.body}); 
			    		MailClient.informTechTeam("AT_W_RTAID_NOT_MATCHED",req.body);
			    		return sendError(res,"Access Token Denied","access_token_denied",constants.HTTP_STATUS.BAD_REQUEST);
			    	}
						AccountToken.findOne({
							aid : aid,
							// udid: device_id,
							clnt: client,
							tokn: refresh_token,
							skey: secret_key,
							act : true,
							revk: false
						},function(err,account_token){
							if(err){ 
								logger.error({"r":"at_w","m":"POST","msg":"accounttoken_find_server_error","p":req.body}); 
								return sendError(res,err,"server_error"); 
							}
							if(!account_token) { 
								logger.error({"r":"at_w","m":"POST","msg":"invalid_refresh_token","p":req.body});
								MailClient.informTechTeam("AT_W_INVALID_RT",req.body); 
								return sendError(res,"Refresh Token Invalid!","invalid_refresh_token",constants.HTTP_STATUS.BAD_REQUEST); 
							}
							var new_access_token = AuthModule.getAT({id: aid,cl: client}, client);
							return sendSuccess(res,{ aid : aid, at : new_access_token});
						});
					}
			  });
			} else {
				logger.error({"r":"at_w","m":"POST","msg":"invalid_AT","p":req.body}); 
				MailClient.informTechTeam("AT_W_INVALID_AT",req.body);
				return sendError(res,"invalid AT","invalid_AT",constants.HTTP_STATUS.BAD_REQUEST);
			}
		} else if(at_decoded.id == aid){
			return sendSuccess(res,{ aid : aid, at : access_token});
		} else {
			logger.error({"r":"at_w","m":"POST","msg":"not_authorized","p":req.body}); 
			MailClient.informTechTeam("AT_W_NOT_AUTHORISED",req.body);
			return sendError(res,"not authorized","not_authorized",constants.HTTP_STATUS.UNAUTHORIZED);
		}
	});
});

/*
 *  Get Access Token for IOS clients
 */
router.post('/at_i', function(req, res, next) {
	req.checkBody('a_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('d_id',errorCodes.invalid_parameters[1]).notEmpty().isValidDeviceId();
	req.checkBody('cl',errorCodes.invalid_parameters[1]).notEmpty().isValidTokenClient();
	req.checkBody('rt',errorCodes.invalid_parameters[1]).notEmpty().isValidRefreshToken();

	if(req.validationErrors()){ 
		logger.error({"r":"at_i","m":"POST","msg":"invalid_parameters","p":req.body});
		MailClient.informTechTeam("AT_I_INVALID_PARAMETER",req.body);
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST); 
	}

	var aid		    		= req.body.a_id;
	var device_id 		= req.body.d_id;
	var client    		= req.body.cl;
	var refresh_token = req.body.rt;
	var tim       		= new Date().getTime();

	if(client != AppClients.IOS){
		logger.error({"r":"at_i_a","m":"POST","msg":"client parameter is not ios","p":req.body}); 
		MailClient.informTechTeam("AT_I_NOT_IOS",req.body);
		return sendError(res,"Access Token Denied","access_token_denied",constants.HTTP_STATUS.BAD_REQUEST);
	}

	jwt.verify(refresh_token, configs.JWT_REFRESH_TOKEN_PRIVATE_KEY, function(err, decoded) {      
    if(err){
    	logger.error({"r":"at_i","m":"POST","msg":"refresh_token_expired","p":req.body});
			MailClient.informTechTeam("REFRESH_TOKEN_EXP_IOS",req.body);    	
      return sendError(res,"Refresh Token expired","refresh_token_expired",constants.HTTP_STATUS.TOKEN_EXPIRED);  
    } else {
    	if(decoded.id !== aid){
    		logger.error({"r":"at_i","m":"POST","msg":"access_token_denied","p":req.body});
    		MailClient.informTechTeam("AT_I_RTAID_NOT_MATCHED",req.body);
    		return sendError(res,"Access Token Denied","access_token_denied",constants.HTTP_STATUS.BAD_REQUEST);
    	}
			AccountToken.findOne({
				aid : aid,
				// udid: device_id,
				clnt: client,
				tokn: refresh_token,
				act : true,
				revk: false
			},function(err,account_token){
				if(err){ 
					logger.error({"r":"at_i","m":"POST","msg":"accounttoken_find_server_error","p":req.body});
					return sendError(res,err,"server_error"); 
				}
				if(!account_token){
					logger.error({"r":"at_i","m":"POST","msg":"invalid_refresh_token","p":req.body});
					MailClient.informTechTeam("AT_I_INVALID_RT",req.body); 
					return sendError(res,"Refresh Token Invalid!","invalid_refresh_token",constants.HTTP_STATUS.BAD_REQUEST); 
				}
				var new_access_token = AuthModule.getAT({id: aid,cl: client}, client);
				return sendSuccess(res,{ aid : aid, at : new_access_token});
			});
		}
  });
});

// Ios Clients can request rt
router.post('/rt_i', function(req, res, next){
	req.checkBody('a_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('d_id',errorCodes.invalid_parameters[1]).notEmpty().isValidDeviceId();
	req.checkBody('k_ey',errorCodes.invalid_parameters[1]).notEmpty().isValidTokenSecretKey();
	req.checkBody('cl',errorCodes.invalid_parameters[1]).notEmpty().isValidTokenClient();
	req.checkBody('clv',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('vrsn',errorCodes.invalid_parameters[1]).notEmpty().isValidVersion();

	if(req.validationErrors()){ 
		logger.error({"r":"rt_i","m":"POST","msg":"invalid_parameters","p":req.body});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var account_id  = req.body.a_id;
	var device_id   = req.body.d_id;
	var client      = req.body.cl;
	var secret_key  = req.body.k_ey;
	var client_vrsn = req.body.clv;
	var version     = req.body.vrsn;

	if(client != AppClients.IOS){
		logger.error({"r":"rt_i","m":"POST","msg":"client parameter is not ios","p":req.body}); 
		return sendError(res,"Access Token Denied","access_token_denied",constants.HTTP_STATUS.BAD_REQUEST);
	}

	Account.findOne({
		_id : account_id
	},function(err,account){
		if(err){ 
			logger.error({"r":"rt_i","m":"POST","msg":"account_find_server_error","p":req.body});
			return sendError(res,err,"server_error");
		}
		if(!account) { 
			logger.error({"r":"rt_i","m":"POST","msg":"no_account_found","p":req.body});
			return sendError(res,"Account not found!","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}
		AccountToken.findOne({
			aid : account_id,
			udid: device_id,
			clnt: client,
			act : true,
		},function(err, account_token){
			if(err){ 
				logger.error({"r":"rt_i","m":"POST","msg":"accounttoken_find_server_error","p":req.body});
				return sendError(res,err,"server_error");
			}
			var new_token,new_secret_key;
			if(account_token) {
				if(account_token.revk === true){
					logger.error({"r":"rt_i","m":"POST","msg":"refresh_token_denied","p":req.body});
					return sendError(res,"Refresh Token Acess Denied","refresh_token_denied",constants.HTTP_STATUS.BAD_REQUEST); 
				}
				if(account_token.skey !== secret_key){
					logger.error({"r":"rt_i","m":"POST","msg":"invalid_secret_key","p":req.body});
					return sendError(res,"Invalid secret key","invalid_secret_key",constants.HTTP_STATUS.INVALID_SECRET);
				}
				jwt.verify(account_token.tokn, configs.JWT_REFRESH_TOKEN_PRIVATE_KEY, function(err, decoded) {      
			    if(err){
			    	new_token      = AuthModule.getRT({id: account_id,cl: client}, client);
						new_secret_key = AuthModule.getSecret(device_id, client);
						AccountToken.update({
							_id : account_token._id
						},{
							tokn : new_token,
							skey : new_secret_key,
							act : true
						},function(err, updated_token){
							if(err){
								logger.error({"r":"rt_i","m":"POST","msg":"accounttoken_update_server_error","p":req.body});
								return sendError(res,err,"server_error");
							}
							if(updated_token){
								return sendSuccess(res,{ aid : account_id, rt : new_token, secret : new_secret_key});
							} else {
								logger.error({"r":"rt_i","m":"POST","msg":"accounttoken_update_error","p":req.body});
								return sendError(res,err,"server_error");
							}
						});
			    } else {
			    	return sendSuccess(res,{ aid : account_id, rt : account_token.tokn, secret : account_token.skey });
			    }
				});
			} else {
				new_token      = AuthModule.getRT({id: account_id,cl: client}, client);
				new_secret_key = AuthModule.getSecret(device_id, client);
				var new_refresh_token = new AccountToken({
					aid : account_id,
					udid: device_id,
					clnt: client,
					tokn: new_token,
					skey: new_secret_key,
					clv : client_vrsn,
					vrsn: version,
					act : true,
					revk: false
				});
				new_refresh_token.save(function(err, new_account_token){
					if(err){ 
						logger.error({"r":"rt_i","m":"POST","msg":"new_refresh_token_save_error","p":req.body});
						return sendError(res,err,"server_error"); 
					}
					if(!new_account_token) { 
						logger.error({"r":"rt_i","m":"POST","msg":"new_account_token_save_error","p":req.body});
						return sendError(res,err,"server_error"); 
					}
					return sendSuccess(res,{ aid : account_id, rt : new_token, secret : new_secret_key });
				});
			}
		});
	});
});

// For all unsecured accounts like(first 3000 accounts), get provisional tokens can be called once
router.post('/prov_tk', function(req, res, next){
	req.checkBody('a_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('d_id',errorCodes.invalid_parameters[1]).notEmpty().isValidDeviceId();
	req.checkBody('cl',errorCodes.invalid_parameters[1]).notEmpty().isValidTokenClient();
	req.checkBody('clv',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('vrsn',errorCodes.invalid_parameters[1]).notEmpty().isValidVersion();

	if(req.validationErrors()){ 
		logger.error({"r":"prov_tk","m":"POST","msg":"invalid_parameters","p":req.body});
		MailClient.informTechTeam("PROVISIONAL_TOKEN_ERR",req.body);
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var account_id  		= req.body.a_id;
	var device_id   		= req.body.d_id;
	var client      		= req.body.cl;
	var client_vrsn 		= req.body.clv;
	var version     		= req.body.vrsn;

	if(client != AppClients.ANDROID){
		logger.error({"r":"prov_tk","m":"POST","msg":"client parameter is not android","p":req.body}); 
		MailClient.informTechTeam("PROVISIONAL_TOKEN_ERR",req.body);
		return sendError(res,"Access Token Denied","access_token_denied",constants.HTTP_STATUS.BAD_REQUEST);
	}

	UnsecureAccount.findOne({
		aid : account_id
	},function(err,unsecured_account){
		if(err){ 
			logger.error({"r":"prov_tk","m":"POST","msg":"account_find_server_error","p":req.body});
			MailClient.informTechTeam("PROVISIONAL_TOKEN_ERR_500",req.body);
			return sendError(res,err,"server_error");
		}
		if(!unsecured_account) { 
			logger.error({"r":"prov_tk","m":"POST","msg":"account_not_unsecured","p":req.body});
			MailClient.informTechTeam("PROVISIONAL_TOKEN_ERR",req.body);
			return sendError(res,"Account not found!","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}
		if(unsecured_account.ack == true){
			logger.error({"r":"prov_tk","m":"POST","msg":"already_acknowledged","p":req.body});
			MailClient.informTechTeam("PROVISIONAL_TOKEN_ERR",req.body);
			return sendError(res,"Already Acknowledged","already_acknowledged",constants.HTTP_STATUS.ALREADY_ACKNOWLEDGED);
		}
		if(unsecured_account.at && unsecured_account.rt && unsecured_account.skey){
			logger.error({"r":"prov_tk","m":"POST","msg":"at_rt_skey_already_exists","p":req.body});
			return sendSuccess(res,{at:unsecured_account.at,rt:unsecured_account.rt,secret:unsecured_account.skey});
		}

		var at 		= AuthModule.getAT({id: account_id,cl: client}, client);
		var rt 		= AuthModule.getRT({id: account_id,cl: client}, client);
		var skey 	= AuthModule.getSecret(device_id, client);

		AccountToken.update({
			aid : account_id,
			clnt: client,
			act : true,
		},{
			$setOnInsert :{
				aid : account_id,
				clnt: client,
				act : true,
				revk: false
			},
			$set :{
				tokn : rt,
				skey : skey,
				udid : device_id,
				clv  : client_vrsn,
				vrsn : version
			}
		},{
			upsert : true
		},function(err, account_token){
			if(err){ 
				logger.error({"r":"prov_tk","m":"POST","msg":"accounttoken_update_server_error","p":req.body});
				MailClient.informTechTeam("PROVISIONAL_TOKEN_ERR_500",req.body);
				return sendError(res,err,"server_error");
			}
			UnsecureAccount.update({
				_id : unsecured_account._id
			},{
				at  : at,
				rt  : rt,
				skey: skey,
				tgtim : new Date()
			},function(err,updated_unsecured_account){
				if(err){
					logger.error({"r":"prov_tk","m":"POST","msg":"unsecureAccount_update_server_error","p":req.body});
					MailClient.informTechTeam("PROVISIONAL_TOKEN_ERR_500",req.body);
					return sendError(res,err,"server_error");
				}
				return sendSuccess(res,{at:at,rt:rt,secret:skey});
			});
		});
	});
});


//Unsecured accounts will acknowledge the receiving of provisional tokens
router.post('/ack_pvtk', function(req, res, next) {
	req.checkBody('a_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('d_id',errorCodes.invalid_parameters[1]).notEmpty().isValidDeviceId();
	req.checkBody('cl',errorCodes.invalid_parameters[1]).notEmpty().isValidTokenClient();
	req.checkBody('clv',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('vrsn',errorCodes.invalid_parameters[1]).notEmpty().isValidVersion();

	if(req.validationErrors()){ 
		logger.error({"r":"ack_pvtk","m":"POST","msg":"invalid_parameters","p":req.body});
		MailClient.informTechTeam("ACK_PROVISIONAL_TOKEN_ERR",req.body);
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST); 
	}

	var account_id  		= req.body.a_id;
	var client      		= req.body.cl;

	if(client != AppClients.ANDROID){
		logger.error({"r":"ack_pvtk","m":"POST","msg":"client parameter is not android","p":req.body}); 
		return sendError(res,"Access Token Denied","access_token_denied",constants.HTTP_STATUS.BAD_REQUEST);
	}
	UnsecureAccount.findOne({
		aid : account_id,
		ack : false
	},function(err,unsecured_account){
		if(err){
			logger.error({"r":"ack_pvtk","m":"POST","msg":"UnsecureAccount_find_error","p":req.body});
			return sendError(res,err,"server_error");
		}
		if(!unsecured_account){
			logger.error({"r":"ack_pvtk","m":"POST","msg":"already_acknowledged","p":req.body});
			MailClient.informTechTeam("ACK_PROVISIONAL_TOKEN_ERR",req.body);
			return sendError(res,"Already Acknowledged","already_acknowledged",constants.HTTP_STATUS.ALREADY_ACKNOWLEDGED);
		}
		UnsecureAccount.update({
			_id : unsecured_account._id
		},{
			ack : true
		},function(err,ack_unsecured_account){
			if(err){
				logger.error({"r":"ack_pvtk","m":"POST","msg":"UnsecureAccount_update_error","p":req.body});
				return sendError(res,err,"server_error");
			}
			UnsecureAccountsModule.remove(account_id, function(err,resp){
				if(err){
					logger.error({"r":"ack_pvtk","m":"POST","msg":"UnsecureAccountsModule_err","p":req.body});
					return sendError(res,err,"server_error");
				}
				if(resp){
					logger.info({"r":"ack_pvtk","m":"POST","msg":"UnsecureAccountsModule_success","p":req.body});
					return sendSuccess(res,{});
				} else {
					logger.info({"r":"ack_pvtk","m":"POST","msg":"UnsecureAccountsModule_aid_not_found_in_cache","p":req.body});
					return sendError(res,"Already Acknowledged","already_acknowledged",constants.HTTP_STATUS.ALREADY_ACKNOWLEDGED);
				}
			});
		});
	});
});

// For Android or Ios clients can call account unlock otp request
router.post('/acc_unlk', function(req, res, next) {
	req.checkBody('a_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('d_id',errorCodes.invalid_parameters[1]).notEmpty().isValidDeviceId();
	req.checkBody('cl',errorCodes.invalid_parameters[1]).notEmpty().isValidTokenClient();
	req.checkBody('rt',errorCodes.invalid_parameters[1]).notEmpty().isValidRefreshToken();
	req.checkBody('at',errorCodes.invalid_parameters[1]).notEmpty().isValidAccessToken();
	req.checkBody('k_ey',errorCodes.invalid_parameters[1]).notEmpty().isValidTokenSecretKey();

	if(req.validationErrors()){
		logger.error({"r":"acc_unlk","m":"POST","msg":"invalid_parameters","p":req.body}); 
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var aid		    		= req.body.a_id;
	var device_id 		= req.body.d_id;
	var client    		= req.body.cl;
	var refresh_token = req.body.rt;
	var access_token  = req.body.at;
	var secret_key    = req.body.k_ey;
	var tim       		= new Date().getTime();

	if(client != AppClients.ANDROID && client != AppClients.IOS){
		logger.error({"r":"acc_unlk","m":"POST","msg":"client parameter is neither android nor ios","p":req.body});
		MailClient.informTechTeam("ACC_UNLK_INVALID_PARAMETER",req.body); 
		return sendError(res,"Access Token Denied","access_token_denied",constants.HTTP_STATUS.BAD_REQUEST);
	}

	rmc.queryById("Account",aid,function(err,account){
		if(err){ 
			logger.error({"r":"acc_unlk","m":"POST","msg":"account_not_found","p":req.body}); 
			return sendError(res,err,"server_error"); 
		}
		if(!account){
			logger.error({"r":"acc_unlk","m":"POST","msg":"account_not_found","p":req.body}); 
			MailClient.informTechTeam("ACC_UNLK_ACCOUNT_NOT_FOUND",req.body);
			return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}
		if(!account.vrfy){
			logger.error({"r":"acc_unlk","m":"POST","msg":"account_not_verified","p":req.body}); 
			MailClient.informTechTeam("ACC_UNLK_ACCOUNT_VERIFIED",req.body);
			return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}
		AuthModule.ifRtExpForUser(refresh_token, aid, function(err,resp){
			if(err){
				logger.error({"r":"acc_unlk","m":"POST","msg":"ifRtExpForUser_error","p":req.body,"err":err});
				MailClient.informTechTeam("ACC_UNLK_RT_EXPIRED",req.body);
				return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
			}
			AuthModule.ifAtExpForUser(access_token, aid, function(err,resp){
				if(err){
					logger.error({"r":"acc_unlk","m":"POST","msg":"ifAtExpForUser_error","p":req.body,"err":err});
					MailClient.informTechTeam("ACC_UNLK_AT_EXPIRED",req.body);
					return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
				}
				AccountToken.findOne({
					aid : aid,
					clnt: client,
					tokn: refresh_token,
					skey: secret_key,
					act : true,
					revk: false
				},function(err,account_token){
					if(err){ 
						logger.error({"r":"acc_unlk","m":"POST","msg":"accounttoken_find_server_error","p":req.body}); 
						return sendError(res,err,"server_error"); 
					}
					if(!account_token) { 
						logger.error({"r":"acc_unlk","m":"POST","msg":"invalid_refresh_token","p":req.body}); 
						MailClient.informTechTeam("ACC_UNLK_INVALID_RT",req.body);
						return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST); 
					}
					OtpModule.accountUnlockByOtp(account, function(err,resp){
						if(err){
							logger.error({"r":"acc_unlk","m":"POST","msg":"OtpModule_accountUnlockByOtp_error","p":req.body,"err":err});
							return sendError(res,err,"server_error"); 
						}
						return sendSuccess(res,{}); 
					});
				});
			});
		});
	});					
});

// For Web clients , logout functionality added
router.post('/logout', function(req, res, next) {
	req.checkBody('a_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('d_id',errorCodes.invalid_parameters[1]).notEmpty().isValidDeviceId();
	req.checkBody('cl',errorCodes.invalid_parameters[1]).notEmpty().isValidTokenClient();
	req.checkBody('rt',errorCodes.invalid_parameters[1]).notEmpty().isValidRefreshToken();
	req.checkBody('at',errorCodes.invalid_parameters[1]).notEmpty().isValidAccessToken();

	if(req.validationErrors()){
		logger.error({"r":"logout","m":"POST","msg":"invalid_parameters","p":req.body}); 
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var aid		    		= req.body.a_id;
	var device_id 		= req.body.d_id;
	var client    		= req.body.cl;
	var refresh_token = req.body.rt;
	var access_token  = req.body.at;
	var tim       		= new Date().getTime();

	if(client != AppClients.WEB){
		logger.error({"r":"logout","m":"POST","msg":"client parameter is neither android nor ios","p":req.body}); 
		return sendError(res,"Access Token Denied","access_token_denied",constants.HTTP_STATUS.BAD_REQUEST);
	}

	rmc.queryById("Account",aid,function(err,account){
		if(err){ 
			logger.error({"r":"logout","m":"POST","msg":"account_not_found","p":req.body}); 
			return sendError(res,err,"server_error"); 
		}
		if(!account){
			logger.error({"r":"logout","m":"POST","msg":"account_not_found","p":req.body}); 
			return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}
		if(!account.vrfy){
			logger.error({"r":"logout","m":"POST","msg":"account_not_verified","p":req.body}); 
			return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}
		AuthModule.ifRtBelongsToUser(refresh_token, aid, function(err,resp){
			if(err){
				logger.error({"r":"logout","m":"POST","msg":"ifRtExpForUser_error","p":req.body,"err":err});
				return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
			}
			AuthModule.ifAtBelongsToUser(access_token, aid, function(err,resp){
				if(err){
					logger.error({"r":"logout","m":"POST","msg":"ifAtExpForUser_error","p":req.body,"err":err});
					return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
				}
				AccountToken.findOne({
					aid : aid,
					clnt: client,
					tokn: refresh_token,
					act : true,
					revk: false
				},function(err,account_token){
					if(err){ 
						logger.error({"r":"logout","m":"POST","msg":"accounttoken_find_server_error","p":req.body}); 
						return sendError(res,err,"server_error"); 
					}
					if(!account_token) { 
						logger.error({"r":"logout","m":"POST","msg":"invalid_refresh_token","p":req.body}); 
						return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST); 
					}
					AccountToken.update({
						aid : aid,
						clnt: client,
						tokn: refresh_token,
						act : true,
						revk: false
					},{
						act : false
					},function(err,resp){
						if(err){ 
							logger.error({"r":"logout","m":"POST","msg":"accounttoken_update_error","p":req.body}); 
							return sendError(res,err,"server_error"); 
						}
						return sendSuccess(res,{});
					});
				});
			});
		});
	});					
});

router.use(function(req, res, next){
	var aid = req.body.a_id;
	var tim = new Date().getTime();
	UserqueueAttempts.check(aid, tim, function(err,notAllowed){
		if(err){
			logger.error({"url":req.originalUrl,"msg":"UserqueueAttempts_DbError","p":{aid:aid},"err":err});
			return sendError(res,err,"server_error"); 
		}
		if(notAllowed){
			logger.error({"url":req.originalUrl,"msg":"UserqueueAttempts_found","p":{aid:aid,lmt:notAllowed.lmt,ltm:notAllowed.ltm,tim:tim}});
			return sendError(res,"Too many requests","too_many_requests",constants.HTTP_STATUS.TOO_MANY_REQUESTS);
		}
		req.tim = tim;
		req.aid = aid;
		next();
	});
});

//route middleware to verify the jwt token for all users
router.use(function(req, res, next){
	var token = req.body.tokn;
	if(!token){
		logger.error({"url":req.originalUrl,"msg":"token_not_found","p":req.body});
	  return sendError(res,"Access without token is not authorised","token_not_found",constants.HTTP_STATUS.FORBIDDEN);
	}
	req.decoded = null;
  jwt.verify(token, configs.JWT_ACCESS_TOKEN_PRIVATE_KEY, function(err, decoded) {      
    if(err){
    	logger.error({"url":req.originalUrl,"msg":"jwt_verify_error","p":req.body,"er":err});
      return sendError(res,"Failed to authenticate token.","token_auth_failure",constants.HTTP_STATUS.TOKEN_EXPIRED);  
    }
    if(decoded.id != req.aid){
			logger.error({"r":req.originalUrl,"msg":"aid_in_token_not_matching_with_accountid","p":req.body});
			return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}
    next();
  });
});

// For Web clients , logout functionality added
router.post('/g_uq_tkn', function(req, res, next) {
	req.checkBody('a_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('d_id',errorCodes.invalid_parameters[1]).notEmpty().isValidDeviceId();
	req.checkBody('rt',errorCodes.invalid_parameters[1]).notEmpty().isValidRefreshToken();
	req.checkBody('k_ey',errorCodes.invalid_parameters[1]).notEmpty().isValidTokenSecretKey();

	if(req.validationErrors()){
		logger.error({"r":"g_uq_tkn","msg":"invalid_parameters","p":req.body}); 
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var device_id 		= req.body.d_id;
	var refresh_token = req.body.rt;
	var secret_key    = req.body.k_ey;
	var aid		    		= req.aid;
	var tim           = req.tim;

	AccountToken.findOne({
		aid : aid,
		udid: device_id,
		tokn: refresh_token,
		skey: secret_key,
		act : true,
		revk: false
	},{
		clnt : 1,
		_id  : 0
	},function(err,account_token){
		if(err){ 
			logger.error({"r":"g_uq_tkn","msg":"accounttoken_find_server_error","p":req.body,"err":err}); 
			return sendError(res,err,"server_error","server_error"); 
		}
		if(!account_token) { 
			logger.error({"r":"g_uq_tkn","msg":"invalid_refresh_token","p":req.body}); 
			return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST); 
		}
		if(ValidMobileDevicesType.indexOf(account_token.clnt) < 0) { 
			logger.error({"r":"g_uq_tkn","msg":"invalid_client","p":req.body,"cl":account_token.client}); 
			return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST); 
		}
		var uq_at = AuthModule.getUserQueueAT({id:aid,cl:account_token.clnt},account_token.clnt);
		UserqueueAttempts.update(aid, tim, function(err,resp){
			if(err){
				logger.error({"r":"g_uq_tkn","msg":"UserqueueAttempts_update_error","p":req.body,"er":err}); 
				return sendError(res,err,"server_error","server_error");
			}
			return sendSuccess(res,{uqat:uq_at});
		});
	});
});


module.exports = router;

