var express 							= require('express');
var mongoose 							= require('mongoose');
var jwt    				  			= require('jsonwebtoken');
var redis           			= require('redis');

var constants							= require('../utility/constants.js');
var configs				  			= require('../utility/configs.js');
var errorCodes 						= require('../utility/errors.js');
var helpers								= require('../utility/helpers.js');
var ProfileNotify 				= require('../utility/profile_notify.js');
var ClientType      			= require('../utility/client_type.js');
var UserRedisKeys   			= require('../utility/user_redis_keys.js');
var RedisPrefixes   			= require('../utility/redis_prefix.js');
var UserInfoCache   			= require('../utility/user_info_cache.js');
var log4jsLogger  				= require('../loggers/log4js_module.js');
var log4jsConfigs  			  = require('../loggers/log4js_configs.js');
var UnsecureAccounts      = require('../utility/auth/unsecure_accounts.js');
var ValidAuthRequest      = require('../utility/auth/valid_request.js');

var redis_client          = redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
    console.log("Error " + err);
});

require('../models/accounts.js');
require('../models/profiles.js');
require('../models/groupmembers.js');

var Account 				= mongoose.model('Account');
var Profile 				= mongoose.model('Profile'); 
var GroupMember 		= mongoose.model('GroupMember');

var rhmset          = require("../utility/redis_hmset.js")(redis_client);
var rmc             = require("../utility/redis_mongo_cache.js")(redis_client);

var sendError 			= helpers.sendError;
var sendSuccess 		= helpers.sendSuccess;
var router 					= express.Router();
var logger        	= log4jsLogger.getLogger(log4jsConfigs.CATEGORY_ROUTES_PROFILE);

//route middleware to verify the jwt token for all unsecured accounts as well covering for android users
router.use(function(req, res, next){
	var token = req.body.tokn || req.query.tokn;
	if(!token){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"token_not_found"});
	  return sendError(res,"Access without token is not authorised","token_not_found",constants.HTTP_STATUS.FORBIDDEN);
	}
	var pid = req.body.p_id || req.query.p_id;
	var aid = req.body.a_id || req.query.a_id;
	if(token == 5 || token == '5' || token == 'null'){
		UnsecureAccounts.check(aid, pid, function(err,isUnsecure){
			if(err){
				console.log(err);
				logger.error({"url":req.originalUrl,"r":"auth","msg":"UnsecureAccounts_check_error","p":{pid:pid,aid:aid,tokn:token}});
				return sendError(res,err,"server_error");
			}
			if(isUnsecure){
				next();
			} else {
				logger.error({"url":req.originalUrl,"r":"auth","msg":"account_not_found_in_unsecured_list","p":{pid:pid,aid:aid,tokn:token}});
				return sendError(res,"Failed to authenticate token.","token_auth_failure",constants.HTTP_STATUS.NOT_FOUND);
			}
		});
	} else {
    jwt.verify(token, configs.JWT_ACCESS_TOKEN_PRIVATE_KEY, function(err, decoded) {      
      if(err){
      	console.log(err);
      	logger.error({"url":req.originalUrl,"r":"auth","msg":"jwt_verify_error","p":{pid:pid,aid:aid,tokn:token}});
        return sendError(res,"Failed to authenticate token.","token_auth_failure",constants.HTTP_STATUS.TOKEN_EXPIRED);  
      } else {
      	req.decoded = decoded;
        next();
      }
    });
	}
});

/*
 *FIND AND RETURN PROFILES
 */
router.post('/find',function(req,res,next){
	req.checkBody('a_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('tokn',errorCodes.invalid_parameters[1]).notEmpty().validateToken();
	
	if(req.validationErrors()){ 
		logger.error({"r":"find","m":"POST","msg":"invalid_parameters","p":req.body});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}
	
	var account_id = req.body.a_id;

	if(!ValidAuthRequest.check(account_id,req.decoded)){
		logger.error({"r":"find","m":"GET","msg":"ValidAuthRequest_check_error","p":req.body});
		return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}
	
	rmc.queryById("Account",account_id,function(err,account){
		if(err){ 
			logger.error({"r":"find","m":"POST","msg":"Account_DbError","p":req.body,"er":err});
			return sendError(res,err,"server_error"); 
		}
		if(!account){
			logger.error({"r":"find","m":"POST","msg":"account_not_found","p":req.body});
			return sendError(res,"Account Not Found","account_not_found",constants.HTTP_STATUS.NOT_FOUND); 
		}
		UserInfoCache.getUserProfilesPartial(account_id,function(err, profiles) {
			if(err) { 
				logger.error({"r":"find","m":"POST","msg":"getUserProfilesPartial_error","p":req.body,"er":err});
				return sendError(res,err,"server_error"); 
			}
			return sendSuccess(res,{ Profile : profiles });
		});
	});
});

/*
 *VIEW PROFILE
 */
router.get('/index',function(req,res,next){
	req.checkQuery('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkQuery('tokn',errorCodes.invalid_parameters[1]).notEmpty().validateToken();
	
	if(req.validationErrors()){ 
		logger.error({"r":"index","m":"GET","msg":"invalid_parameters","p":req.query});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}
	
	var profile_id = req.query.p_id;
	
	UserInfoCache.getMongoProfile(profile_id,function(err, profile){
		if(err){ 
			logger.error({"r":"index","m":"GET","msg":"getMongoProfile_error","p":req.query,"er":err});
			return sendError(res,err,"server_error"); 
		}
		if(!profile){
			logger.error({"r":"index","m":"GET","msg":"profile_not_found","p":req.query});
			return sendError(res,"Profile not found","profile_not_found",constants.HTTP_STATUS.NOT_FOUND);
		}
		if(!ValidAuthRequest.check(profile.aid,req.decoded)){
			logger.error({"r":"index","m":"GET","msg":"ValidAuthRequest_check_error","p":req.query});
			return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}
		return sendSuccess(res,{ Profile : profile });
	});
});

/*
 * CREATE PROFILE
 */
router.post('/index',function(req,res,next){
	req.checkBody('a_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('s_id',errorCodes.invalid_parameters[1]).optional().notEmpty().isValidMongoId();
	req.checkBody('name',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('childname',errorCodes.invalid_parameters[1]).optional().notEmpty();
	req.checkBody('role',errorCodes.invalid_parameters[1]).isInt().isValidRole();
	req.checkBody('clss',errorCodes.invalid_parameters[1]).optional().isValidClass();		
	req.checkBody('ppic',errorCodes.invalid_parameters[1]).optional().isValidURL();
	req.checkBody('tokn',errorCodes.invalid_parameters[1]).notEmpty().validateToken();
	
	if(req.validationErrors()){
		logger.error({"r":"index","m":"POST","msg":"invalid_parameters","p":req.body}); 
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}
	
	var account_id					= req.body.a_id;
	var profile_name  			= req.body.name;
	var profile_role 				= req.body.role;

	var school_id 					= req.body.s_id ? req.body.s_id : null;
	var child_name					= req.body.childname ? req.body.childname : null;
	var profile_class 			= req.body.clss ? req.body.clss : null; 
	var profile_pic 				= req.body.ppic ? req.body.ppic : null;

	if(!ValidAuthRequest.check(account_id,req.decoded)){
		logger.error({"r":"index","m":"POST","msg":"ValidAuthRequest_check_error","p":req.body});
		return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}
	
	rmc.queryById("Account",account_id,function(err,account){
		if(err){ 
			logger.error({"r":"index","m":"POST","msg":"Account_DbError","p":req.body,"er":err});
			return sendError(res,err,"server_error"); 
		}
		if(!account){
			logger.error({"r":"index","m":"POST","msg":"account_not_found","p":req.body});
			return sendError(res,"Account Not Found","account_not_found",constants.HTTP_STATUS.NOT_FOUND); 
		}
		if(account.vrfy === false){
			logger.error({"r":"index","m":"POST","msg":"account_not_verified","p":req.body});
			return sendError(res,"Invalid Entry, Phone Number not verified","account_not_verified",constants.HTTP_STATUS.NOT_FOUND);
		}

		Profile.findOne({
			aid : account_id,
			sid : school_id,
			nam : profile_name,
			cnam: child_name,
			role : profile_role,
			act : true
		},function(err, true_profile){
			if(err){ 
				logger.error({"r":"index","m":"POST","msg":"profile_server_error","p":req.body,"er":err});
				return sendError(res,err,"server_error"); 
			}
			if(true_profile){
				logger.error({"r":"index","m":"POST","msg":"profile_already_exists","p":req.body});
				return sendError(res,"Profile already exists","profile_already_exists",constants.HTTP_STATUS.BAD_REQUEST);
			}
			var new_profile = new Profile({
				aid : account_id,
				sid : school_id,
				nam : profile_name,
				cnam: child_name,
				role : profile_role,
				clss : profile_class,
				ppic : profile_pic,
				act : true
			});
			new_profile.save(function(err,new_profile){
				if(err){ 
					logger.error({"r":"index","m":"POST","msg":"new_profile_save_server_error","p":req.body,"er":err});
					return sendError(res,err,"server_error"); 
				}
				removeProfileAccountHset(account_id,function(err,isRemoved){
					if(err){
						logger.warn({"r":"index","m":"POST","msg":"removeProfileAccountHset_error","p":req.body,"er":err});
					}
					UnsecureAccounts.addProfile(account_id, new_profile._id+'', function(err,resp){
						if(err){
							logger.error({"r":"index","m":"POST","msg":"UnsecureAccounts_addProfile_error","p":req.body,"er":err});
						}
						ProfileNotify.abt_prof_c(
							new_profile._id,
							new_profile.aid,
							new_profile.nam, 
							new_profile.ppic, 
							new_profile.cname, 
							new_profile.role,
							function(err,resp){
								if(err){
									console.trace(err);
									logger.error({"r":"index","m":"POST","msg":"ProfileNotify.abt_prof_c_error","p":req.body,"er":err});
								}
								return sendSuccess(res, {"Profile" : new_profile});
							}
						);
					});
				});
			});
		});
	});
});

/*
 * CREATE PROFILE ios
 */
router.post('/index_i',function(req,res,next){
	req.checkBody('a_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('name',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('role',errorCodes.invalid_parameters[1]).notEmpty().isValidRole();
	req.checkBody('s_id',errorCodes.invalid_parameters[1]).optional().notEmpty().isValidMongoId();
	req.checkBody('childname',errorCodes.invalid_parameters[1]).optional().notEmpty();
	req.checkBody('clss',errorCodes.invalid_parameters[1]).optional().isValidClass();		
	req.checkBody('ppic',errorCodes.invalid_parameters[1]).optional().isValidURL();
	
	if(req.validationErrors()){ 
		logger.error({"r":"index_i","m":"POST","msg":"invalid_parameters","p":req.body});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.INVALID_PARAMETER);
	}
	
	var account_id				= req.body.a_id;
	var profile_name  		= req.body.name;
	var profile_role 			= req.body.role;

	var school_id 				= (req.body.s_id) ? (req.body.s_id) :null;
	var child_name 				= (req.body.childname) ? (req.body.childname) :null;
	var profile_class 		= (req.body.clss) ? (req.body.clss) :null;
	var profile_pic 			= (req.body.ppic) ? (req.body.ppic) :null;	

	if(!ValidAuthRequest.check(account_id,req.decoded)){
		logger.error({"r":"index_i","m":"POST","msg":"ValidAuthRequest_check_error","p":req.body});
		return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}
	
	rmc.queryById("Account",account_id,function(err,account){
		if(err){ 
			logger.error({"r":"index_i","m":"POST","msg":"Account_DbError","p":req.body,"er":err});
			return sendError(res,err,"server_error"); 
		}
		if(!account){
			logger.error({"r":"index_i","m":"POST","msg":"account_not_found","p":req.body});
			return sendError(res,"Account Not Found","account_not_found",constants.HTTP_STATUS.NOT_FOUND); 
		}
		if(account.vrfy === false){
			logger.error({"r":"index_i","m":"POST","msg":"account_not_verified","p":req.body});
			return sendError(res,"Invalid Entry, Phone Number not verified","account_not_verified",constants.HTTP_STATUS.NOT_FOUND);
		}
		Profile.findOne({
			aid : account_id,
			act : true,
			nam : profile_name,
			role : profile_role
		},function(err, true_profile){
			if(err){ 
				logger.error({"r":"index_i","m":"POST","msg":"profile_server_error","p":req.body,"er":err});
				return sendError(res,err,"server_error"); 
			}
			if(true_profile){
				logger.error({"r":"index_i","m":"POST","msg":"profile_already_exists","p":req.body});
				return sendError(res,"Profile already exists","profile_already_exists",constants.HTTP_STATUS.PROFILE_EXISTS);
			}
			Profile.update({
				aid  : account_id,
				act  : true,
				nam  : profile_name,
				role : profile_role
			},{
				$setOnInsert : {
					aid  : account_id,
					sid  : school_id,
					nam  : profile_name,
					cnam : child_name,
					role : profile_role,
					clss : profile_class,
					ppic : profile_pic,
					act  : true,
					cat  : new Date
				}
			},{
				upsert : true,
			},function(err, profile){
				if(err) { 
					logger.error({"r":"index_i","m":"POST","msg":"profile_update_server_error","p":req.body,"er":err});
					return sendError(res,err,"server_error"); 
				}
				Profile.findOne({
					aid : account_id,
					act : true,
					nam : profile_name,
					role : profile_role
				},function(err, created_profile){
					if(err){ 
						logger.error({"r":"index_i","m":"POST","msg":"created_profile_find_server_error","p":req.body,"er":err});
						return sendError(res,err,"server_error"); 
					}
					removeProfileAccountHset(account_id,function(err,isRemoved){
						if(err){
							logger.warn({"r":"index_i","m":"POST","msg":"removeProfileAccountHset_error","p":req.body,"er":err});
						}
						ProfileNotify.abt_prof_c(
							created_profile._id, 
							account_id, 
							profile_name, 
							profile_pic, 
							child_name, 
							profile_role,
							function(err,resp){
								if(err){
									console.trace(err);
									logger.error({"r":"index_i","m":"POST","msg":"ProfileNotify.abt_prof_c_error","p":req.body,"er":err});
								}
								return sendSuccess(res, {"Profile" : created_profile});
							}
						);
					});
				});
			});
		});
	});
});


/*
 * EDIT PROFILE
 */
router.put('/index',function(req,res,next){
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('a_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('s_id',errorCodes.invalid_parameters[1]).optional().notEmpty().isValidMongoId();
	req.checkBody('name',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('childname',errorCodes.invalid_parameters[1]).optional().notEmpty();
	req.checkBody('role',errorCodes.invalid_parameters[1]).isInt().isValidRole();
	req.checkBody('clss',errorCodes.invalid_parameters[1]).optional().isInt().isValidClass();
	req.checkBody('ppic',errorCodes.invalid_parameters[1]).optional().isValidURL();
	req.checkBody('tokn',errorCodes.invalid_parameters[1]).notEmpty().validateToken();

	if(req.validationErrors()){ 
		logger.error({"r":"index","m":"PUT","msg":"invalid_parameters","p":req.body});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST); 
	}
	
	var profile_id					= req.body.p_id;
	var account_id					= req.body.a_id;
	var profile_name  			= req.body.name;
	var profile_role 				= req.body.role;

	var school_id 					= (req.body.s_id) ? (req.body.s_id) :null;
	var child_name 					= (req.body.childname) ? (req.body.childname) :null;
	var profile_class 			= (req.body.clss) ? (req.body.clss) :null;
	var profile_pic 				= (req.body.ppic) ? (req.body.ppic) :null;

	if(!ValidAuthRequest.check(account_id,req.decoded)){
		logger.error({"r":"index","m":"PUT","msg":"ValidAuthRequest_check_error","p":req.body});
		return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}
	
	Profile.findOne({
		_id : profile_id,
		aid : account_id,
		act : true
	},function(err,edit_profile){
		if(err){ 
			logger.error({"r":"index","m":"PUT","msg":"profile_find_server_error","p":req.body,"er":err});
			return sendError(res,err,"server_error"); 
		}
		if(!edit_profile){
			logger.error({"r":"index","m":"PUT","msg":"profile_not_found","p":req.body});
			return sendError(res,"Profile not found","profile_not_found",constants.HTTP_STATUS.NOT_FOUND);
		}		
		Profile.update({
			_id : profile_id,
			act : true
		},{
			sid : school_id,
			nam : profile_name,
			cnam: child_name,
			role : profile_role,
			clss : profile_class,
			ppic : profile_pic
		},function(err,update_no){
			if(err){ 
				logger.error({"r":"index","m":"PUT","msg":"profile_update_server_error","p":req.body,"er":err});
				return sendError(res,err,"server_error"); 
			}
			removeProfileAccountHset(account_id,function(err,isRemoved){
					if(err){
						logger.warn({"r":"index","m":"PUT","msg":"removeProfileAccountHset_error","p":req.body,"er":err});
					}
				UserInfoCache.removeMongoProfile(profile_id,function(err, pRemoved){
					if(err){
						logger.warn({"r":"index","m":"PUT","msg":"removeMongoProfile_error","p":req.body,"er":err});
					}
					return sendSuccess(res, {});
				});
			});
		});
	});
});


//**
//pname child edit
//**
router.put('/pnam_e_child',function(req,res,next){
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('a_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('childname',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('tokn',errorCodes.invalid_parameters[1]).notEmpty().validateToken();

	if(req.validationErrors()){ 
		logger.error({"r":"pnam_e_child","m":"PUT","msg":"invalid_parameters","p":req.body});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}
	
	var profile_id				= req.body.p_id;
	var account_id				= req.body.a_id;
	var new_name  				= req.body.childname;

	if(!ValidAuthRequest.check(account_id,req.decoded)){
		logger.error({"r":"pnam_e_child","m":"PUT","msg":"ValidAuthRequest_check_error","p":req.body});
		return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}
	
	Profile.findOne({
		_id : profile_id,
		aid : account_id,
		act : true
	},function(err,profile){
		if(err){ 
			logger.error({"r":"pnam_e_child","m":"PUT","msg":"profile_find_server_error","p":req.body,"er":err});
			return sendError(res,err,"server_error"); 
		}
		if(!profile){
			logger.error({"r":"pnam_e_child","m":"PUT","msg":"profile_not_found","p":req.body});
			return sendError(res,"Profile not found","profile_not_found",constants.HTTP_STATUS.NOT_FOUND);
		}
		var old_name = profile.nam;
		Profile.update({
			_id : profile_id,
			act : true
		},{
			cnam : new_name
		},function(err,update_no){
			if(err){ 
				logger.error({"r":"pnam_e_child","m":"PUT","msg":"profile_update_server_error","p":req.body,"er":err});
				return sendError(res,err,"server_error"); 
			}
			removeProfileAccountHset(account_id,function(err,isRemoved){
				if(err){
					logger.warn({"r":"pnam_e_child","m":"PUT","msg":"removeProfileAccountHset_error","p":req.body,"er":err});
				}
				UserInfoCache.removeMongoProfile(profile_id,function(err, pRemoved){
					if(err){
						logger.error({"r":"pnam_e_child","m":"PUT","msg":"removeMongoProfile_error","p":req.body,"er":err});
					}
					return sendSuccess(res, {});
				});
			});
		});
	});
});

/*
 * EDIT PROFILE NAME
 */
router.put('/pnam_e',function(req,res,next){
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('a_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('name',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('tokn',errorCodes.invalid_parameters[1]).notEmpty().validateToken();

	if(req.validationErrors()){ 
		logger.error({"r":"pnam_e","m":"PUT","msg":"invalid_parameters","p":req.body});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}
	
	var profile_id				= req.body.p_id;
	var account_id				= req.body.a_id;
	var new_name  				= req.body.name;

	if(!ValidAuthRequest.check(account_id,req.decoded)){
		logger.error({"r":"pnam_e","m":"PUT","msg":"ValidAuthRequest_check_error","p":req.body});
		return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}
	
	Profile.findOne({
		_id : profile_id,
		aid : account_id,
		act : true
	},function(err,profile){
		if(err){ 
			logger.error({"r":"pnam_e","m":"PUT","msg":"profile_find_server_error","p":req.body,"er":err});
			return sendError(res,err,"server_error"); 
		}
		if(!profile){
			logger.error({"r":"pnam_e","m":"PUT","msg":"profile_not_found","p":req.body});
			return sendError(res,"Profile not found","profile_not_found",constants.HTTP_STATUS.NOT_FOUND);
		}
		var old_name = profile.nam;
		Profile.update({
			_id : profile_id,
			act : true
		},{
			nam : new_name,
		},function(err,update_no){
			if(err){ 
				logger.error({"r":"pnam_e","m":"PUT","msg":"profile_update_server_error","p":req.body,"er":err});
				return sendError(res,err,"server_error"); 
			}
			removeProfileAccountHset(account_id,function(err,isRemoved){
				if(err){
					logger.warn({"r":"pnam_e","m":"PUT","msg":"removeProfileAccountHset_error","p":req.body,"er":err});
				}
				UserInfoCache.removeMongoProfile(profile_id,function(err, pRemoved){
					if(err){
						logger.error({"r":"pnam_e","m":"PUT","msg":"removeMongoProfile_error","p":req.body,"er":err});
					}
					var notify_msg = old_name+' changed his profile name to '+new_name;
					ProfileNotify.abt_pnam_e(profile_id, new_name, notify_msg, function(err,resp){
						if(err){
							console.trace(err);
							logger.error({"r":"pnam_e","m":"PUT","msg":"ProfileNotify.abt_pnam_e_error","p":req.body,"er":err});
						}
					  editProfNameInGrpMems(profile_id, new_name);
						return sendSuccess(res, {});
					});
				});
			});
		});
	});
});

/*
 * EDIT PROFILE PIC
 */
router.put('/ppic_e',function(req,res,next){
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('a_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('ppic',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('tokn',errorCodes.invalid_parameters[1]).notEmpty().validateToken();

	if(req.validationErrors()){
		logger.error({"r":"ppic_e","m":"PUT","msg":"profile_update_server_error","p":req.body}); 
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}
	
	var profile_id				= req.body.p_id;
	var account_id				= req.body.a_id;
	var profile_pic  			= req.body.ppic;

	if(!ValidAuthRequest.check(account_id,req.decoded)){
		logger.error({"r":"ppic_e","m":"PUT","msg":"ValidAuthRequest_check_error","p":req.body});
		return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}
	
	Profile.findOne({
		_id : profile_id,
		aid : account_id,
		act : true
	},function(err,profile){
		if(err){ 
			logger.error({"r":"ppic_e","m":"PUT","msg":"profile_find_server_error","p":req.body,"er":err});
			return sendError(res,err,"server_error"); 
		}
		if(!profile){
			logger.error({"r":"ppic_e","m":"PUT","msg":"profile_not_found","p":req.body});
			return sendError(res,"Profile not found","profile_not_found",constants.HTTP_STATUS.NOT_FOUND);
		}

		var profile_name = profile.nam;
		
		Profile.update({
			_id : profile_id,
			act : true
		},{
			ppic : profile_pic,
		},function(err,update_no){
			if(err){ 
				logger.error({"r":"ppic_e","m":"PUT","msg":"profile_update_server_error","p":req.body,"er":err});
				return sendError(res,err,"server_error"); 
			}
			removeProfileAccountHset(account_id,function(err,isRemoved){
				if(err){
					logger.warn({"r":"ppic_e","m":"PUT","msg":"removeProfileAccountHset_error","p":req.body,"er":err});
				}
				UserInfoCache.removeMongoProfile(profile_id,function(err, pRemoved){
					if(err){
						logger.warn({"r":"ppic_e","m":"PUT","msg":"removeMongoProfile_error","p":req.body,"er":err});
					}
					var notify_msg = profile_name+' changed his profile pic';
					ProfileNotify.abt_ppic_e(profile_id, profile_pic, notify_msg, function(err,resp){
						if(err){
							console.trace(err);
							logger.error({"r":"ppic_e","m":"PUT","msg":"ProfileNotify.abt_ppic_e_error","p":req.body,"er":err});
						}
						return sendSuccess(res, {});
					});
				});
			});
		});
	});
});

/*
 * REM PROFILE PIC
 */
router.put('/ppic_r',function(req,res,next){
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('a_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('tokn',errorCodes.invalid_parameters[1]).notEmpty().validateToken();

	if(req.validationErrors()){
		logger.error({"r":"ppic_r","m":"PUT","msg":"profile_update_server_error","p":req.body}); 
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST); 
	}
	
	var profile_id				= req.body.p_id;
	var account_id				= req.body.a_id;

	if(!ValidAuthRequest.check(account_id,req.decoded)){
		logger.error({"r":"ppic_r","m":"PUT","msg":"ValidAuthRequest_check_error","p":req.body});
		return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}
	
	Profile.findOne({
		_id : profile_id,
		aid : account_id,
		act : true
	},function(err,profile){
		if(err){ 
			logger.error({"r":"ppic_r","m":"PUT","msg":"profile_find_server_error","p":req.body,"er":err}); 
			return sendError(res,err,"server_error"); 
		}
		if(!profile){
			logger.error({"r":"ppic_r","m":"PUT","msg":"profile_not_found","p":req.body});
			return sendError(res,"Profile not found","profile_not_found",constants.HTTP_STATUS.NOT_FOUND);
		}

		var profile_name = profile.nam;
		
		Profile.update({
			_id : profile_id,
			act : true
		},{
			ppic : "",
		},function(err,update_no){
			if(err){ 
				logger.error({"r":"ppic_r","m":"PUT","msg":"profile_update_server_error","p":req.body,"er":err});
				return sendError(res,err,"server_error"); 
			}
			removeProfileAccountHset(account_id,function(err,isRemoved){
				if(err){
					logger.warn({"r":"ppic_r","m":"PUT","msg":"removeProfileAccountHset_error","p":req.body,"er":err});
				}
				UserInfoCache.removeMongoProfile(profile_id,function(err, pRemoved){
					if(err){
						logger.warn({"r":"ppic_r","m":"PUT","msg":"removeMongoProfile_error","p":req.body,"er":err});
					}
					var notify_msg = profile_name+' removed his profile pic';
					ProfileNotify.abt_ppic_r(profile_id, notify_msg, function(err,resp){
						if(err){
							console.trace(err);
							logger.error({"r":"ppic_r","m":"PUT","msg":"profile_update_server_error","p":req.body,"er":err});
						}
						return sendSuccess(res, {});
					});
				});
			});
		});
	});
});

/*
 * DELETE PROFILE
 */
router.post('/profile_r',function(req,res,next){
	req.checkBody('a_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('tokn',errorCodes.invalid_parameters[1]).notEmpty().validateToken();

	if(req.validationErrors()){
		logger.error({"r":"profile_r","m":"POST","msg":"invalid_parameters","p":req.body}); 
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var account_id 		= req.body.a_id;
	var profile_id 		= req.body.p_id;

	if(!ValidAuthRequest.check(account_id,req.decoded)){
		logger.error({"r":"profile_r","m":"POST","msg":"ValidAuthRequest_check_error","p":req.body});
		return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	Profile.findOne({
		_id : profile_id,
		aid : account_id,
		act : true
	},function(err, profile){
		if(err){ 
			logger.error({"r":"profile_r","m":"POST","msg":"profile_find_server_error","p":req.body,"er":err});
			return sendError(res,err,"server_error"); 
		}
		if(!profile){
			logger.error({"r":"profile_r","m":"POST","msg":"profile_not_found","p":req.body});
			return sendError(res,"Profile not found","profile_not_found",constants.HTTP_STATUS.NOT_FOUND);
		}
		Profile.count({
			aid : profile.aid,
			act : true
		},function(err,total_profiles_count){
			if(err){ 
				logger.error({"r":"profile_r","m":"POST","msg":"profile_count_server_error","p":req.body,"er":err});
				return sendError(res,err,"server_error"); 
			}
			if(total_profiles_count < 2){
				logger.error({"r":"profile_r","m":"POST","msg":"only_one_profile","p":req.body});
				return sendError(res,"User currently has only one profile","only_one_profile",constants.HTTP_STATUS.BAD_REQUEST);
			}
			GroupMember.find({
				pid : profile_id,
				act : true,
				gdel: false
			},function(err,groups){
				if(err){ 
					logger.error({"r":"profile_r","m":"POST","msg":"groupmember_find_server_error","p":req.body,"er":err});
					return sendError(res,err,"server_error"); 
				}
				if(groups.length	> 0){
					logger.error({"r":"profile_r","m":"POST","msg":"profile_groups_exist","p":req.body}); 
					return sendError(res,"Leave all joined groups with this profile before deleting profile","profile_groups_exist",constants.HTTP_STATUS.BAD_REQUEST);
				}
				Profile.update({
					_id : profile_id,
					aid : account_id,
					act : true
				},{
					act : false
				},function(err){
					if(err){ 
						logger.error({"r":"profile_r","m":"POST","msg":"profile_update_server_error","p":req.body,"er":err});
						return sendError(res,err,"server_error"); 
					}
					removeProfileAccountHset(account_id,function(err,isRemoved){
						if(err){
							logger.error({"r":"profile_r","m":"POST","msg":"removeProfileAccountHset_error","p":req.body,"er":err});
						}
						UserInfoCache.removeMongoProfile(profile_id,function(err, pRemoved){
							if(err){
								logger.warn({"r":"profile_r","m":"POST","msg":"removeMongoProfile_error","p":req.body,"er":err});
							}
							ProfileNotify.abt_profile_r(profile_id, profile.nam, profile.aid, function(err,resp){
								if(err){
									console.trace(err);
									logger.error({"r":"profile_r","m":"POST","msg":"ProfileNotify.abt_profile_r_error","p":req.body,"er":err});
								}
								return sendSuccess(res, {});
							});
						});
					});
				});
			});
		});
	});
});

/*
 * DELETE PROFILE
 */
router.post('/profile_r_i',function(req,res,next){
	req.checkBody('a_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();

	if(req.validationErrors()){
		logger.error({"r":"profile_r_i","m":"POST","msg":"invalid_parameters","p":req.body}); 
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.INVALID_PARAMETER);
	}

	var account_id 		= req.body.a_id;
	var profile_id 		= req.body.p_id;

	if(!ValidAuthRequest.check(account_id,req.decoded)){
		logger.error({"r":"profile_r_i","m":"POST","msg":"ValidAuthRequest_check_error","p":req.body});
		return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	Profile.findOne({
		_id : profile_id,
		aid : account_id,
		act : true
	},function(err, profile){
		if(err){ 
			logger.error({"r":"profile_r_i","m":"POST","msg":"profile_find_server_error","p":req.body,"er":err});
			return sendError(res,err,"server_error"); 
		}
		if(!profile){
			logger.error({"r":"profile_r_i","m":"POST","msg":"profile_not_found","p":req.body});
			return sendError(res,"Profile not found","profile_not_found",constants.HTTP_STATUS.NOT_FOUND);
		}
		Profile.count({
			aid : profile.aid,
			act : true
		},function(err,total_profiles_count){
			if(err){ 
				logger.error({"r":"profile_r_i","m":"POST","msg":"profile_count_server_error","p":req.body,"er":err});
				return sendError(res,err,"server_error"); 
			}
			if(total_profiles_count < 2){
				logger.error({"r":"profile_r_i","m":"POST","msg":"only_one_profile","p":req.body});
				return sendError(res,"User currently has only one profile","only_one_profile",constants.HTTP_STATUS.ONLY_ONE_PROFILE);
			}
			GroupMember.find({
				pid : profile_id,
				act : true,
				gdel: false
			},function(err,groups){
				if(err){ 
					logger.error({"r":"profile_r_i","m":"POST","msg":"groupmember_find_server_error","p":req.body,"er":err});
					return sendError(res,err,"server_error"); 
				}
				if(groups && groups.length	> 0){ 
					logger.error({"r":"profile_r_i","m":"POST","msg":"profile_groups_exist","p":req.body});
					return sendError(res,"Leave all joined groups with this profile before deleting profile","profile_groups_exist",constants.HTTP_STATUS.PROFILE_HAS_ACTIVE_GROUPS);
				}
				Profile.update({
					_id : profile_id,
					aid : account_id,
					act : true
				},{
					act : false
				},function(err){
					if(err){ 
						logger.error({"r":"profile_r_i","m":"POST","msg":"profile_update_server_error","p":req.body,"er":err});
						return sendError(res,err,"server_error"); 
					}
					removeProfileAccountHset(account_id,function(err,isRemoved){
						if(err){
							logger.error({"r":"profile_r_i","m":"POST","msg":"removeProfileAccountHset_error","p":req.body,"er":err});
						}
						UserInfoCache.removeMongoProfile(profile_id,function(err, pRemoved){
							if(err){
								logger.warn({"r":"profile_r_i","m":"POST","msg":"removeMongoProfile_error","p":req.body,"er":err});
							}
							ProfileNotify.abt_profile_r(profile_id, profile.nam, profile.aid,function(err,resp){
								if(err){
									console.trace(err);
									logger.error({"r":"profile_r_i","m":"POST","msg":"ProfileNotify.abt_profile_r_error","p":req.body,"er":err});
								}
								return sendSuccess(res, {});
							});
						});
					});
				});
			});
		});
	});
});

function removeProfileAccountHset(account_id, cb){
	var key = RedisPrefixes.USER_PROF_GMS+account_id+'';
	rhmset.keyexists(key,UserRedisKeys.PROFILES,function(err,isKey){
		if(err){
			console.trace(err);
			return cb(true,null);
		}
		if(isKey){
			rhmset.removefield(key,UserRedisKeys.PROFILES,function(err,resp){
				if(err){
					console.trace(err);
					return cb(true,false);
				} 
				if(resp){
					return cb(null,true);
				} else {
					console.log("Oops,this is unfortunate that Account Profiles key could not be deleted for Account : "+account_id);
					return cb(null,false);
				}
			});
		} else {
			console.log("Oops,this is unfortunate that Account Profiles key does not exists for Account : "+account_id);
			return cb(null,false);
		}
	});
}

function editProfNameInGrpMems(profile_id, new_name){
	GroupMember.update({
		pid : profile_id,
		act : true
	},{
		pnam : new_name.toLowerCase()
	},{
		multi : true
	},function(err, members){
		if(err) { 
			console.trace(err);
			return sendError(res,err,"server_error"); 
		}
		console.log('groupmember updated with profile name for profile id : '+profile_id);
		return;
	});
}

module.exports = router;