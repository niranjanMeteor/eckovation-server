var express 						= require('express');
var mongoose 						= require('mongoose');
var jwt    							= require('jsonwebtoken');
var redis               = require('redis');

var configs							= require('../utility/configs.js');
var errorCodes 					= require('../utility/errors.js');
var constants						= require('../utility/constants.js');
var otpGenerator				= require('../utility/otp_generator.js');
var helpers							= require('../utility/helpers.js');
var AppClients					= require('../utility/app_clients.js');
var AccountInfo   			= require('../utility/account_info.js');
var ClientType    			= require('../utility/client_type.js');
var RabbitMqPublish     = require('../rabbitmq/publisher/publisher.js');
var RabbitMqQueues      = require('../rabbitmq/queues.js');
var log4jsLogger  			= require('../loggers/log4js_module.js');
var log4jsConfigs  			= require('../loggers/log4js_configs.js');
var RedisPrefixes       = require('../utility/redis_prefix.js');
var UnsecureAccounts    = require('../utility/auth/unsecure_accounts.js');
var AuthModule          = require('../utility/auth/auth_tokens.js');
var BruteForce          = require('../utility/auth/brute_force.js');
var OtpAttempts         = require('../utility/auth/otp_attempts.js');
var MailClient          = require('../utility/mail/mail_client.js');
var MobileValidator     = require('../utility/phone_number/phone_validations.js');
var UserQueue           = require('../userqueues/user_rcq.js');
var AccountDataInfoCache= require('../cache/accountdata_info.js');

var redis_client        = redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
  console.log("Error " + err);
});

require('../models/accounts.js');
require('../models/otps.js');
require('../models/profiles.js');
require('../models/groupmembers.js');
require('../models/groups.js');
require('../models/accounttokens.js');
require('../models/otpcalls.js');

var Account 						= mongoose.model('Account');
var Otp 								= mongoose.model('Otp');
var Profile 						= mongoose.model('Profile');
var GroupMember 				= mongoose.model('GroupMember');
var Group 							= mongoose.model('Group');
var AccountToken  			= mongoose.model('AccountToken');
var OtpCall 		        = mongoose.model('OtpCall');

var rhmset              = require("../utility/redis_hmset.js")(redis_client);
var rmc                 = require("../utility/redis_mongo_cache.js")(redis_client);

var sendError 					= helpers.sendError;
var sendSuccess 				= helpers.sendSuccess;
var router 							= express.Router();
var logger        			= log4jsLogger.getLogger(log4jsConfigs.CATEGORY_ROUTES_ACCOUNT);
var bruteForceAcclogger = log4jsLogger.getLogger(log4jsConfigs.CATEGORY_BRUTE_FORCE_ACCOUNT);
var bruteForceOtplogger = log4jsLogger.getLogger(log4jsConfigs.CATEGORY_BRUTE_FORCE_OTP);


// For all unsecured accounts like(first 3000 accounts), get refresh token can be called once
router.post('/g_rt', function(req, res, next){
	req.checkBody('a_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('d_id',errorCodes.invalid_parameters[1]).notEmpty().isValidDeviceId();
	req.checkBody('k_ey',errorCodes.invalid_parameters[1]).notEmpty().isValidTokenSecretKey();
	req.checkBody('cl',errorCodes.invalid_parameters[1]).notEmpty().isValidTokenClient();
	req.checkBody('clv',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('vrsn',errorCodes.invalid_parameters[1]).notEmpty().isValidVersion();

	if(req.validationErrors()){ 
		logger.error({"r":"g_rt","m":"POST","msg":"invalid_parameters","p":req.body});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var account_id  = req.body.a_id;
	var device_id   = req.body.d_id;
	var client      = req.body.cl;
	var secret_key  = req.body.k_ey;
	var client_vrsn = req.body.clv;
	var version     = req.body.vrsn;

	Account.findOne({
		_id : account_id
	},function(err,account){
		if(err){ 
			logger.error({"r":"g_rt","m":"POST","msg":"account_find_server_error","p":req.body});
			return sendError(res,err,"server_error");
		}
		if(!account) { 
			logger.error({"r":"g_rt","m":"POST","msg":"no_account_found","p":req.body});
			return sendError(res,"Account not found!","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}
		AccountToken.findOne({
			aid : account_id,
			udid: device_id,
			clnt: client,
			act : true,
		},function(err, account_token){
			if(err){ 
				logger.error({"r":"g_rt","m":"POST","msg":"accounttoken_find_server_error","p":req.body});
				return sendError(res,err,"server_error");
			}
			var new_token,new_secret_key;
			if(account_token) {
				if(account_token.revk === true){
					logger.error({"r":"g_rt","m":"POST","msg":"refresh_token_denied","p":req.body});
					return sendError(res,"Refresh Token Acess Denied","refresh_token_denied",constants.HTTP_STATUS.BAD_REQUEST); 
				}
				if(account_token.skey !== secret_key){
					logger.error({"r":"g_rt","m":"POST","msg":"invalid_secret_key","p":req.body});
					return sendError(res,"Invalid secret key","invalid_secret_key",constants.HTTP_STATUS.INVALID_SECRET);
				}
				jwt.verify(account_token.tokn, configs.JWT_REFRESH_TOKEN_PRIVATE_KEY, function(err, decoded) {      
			    if(err){
			    	new_token      = AuthModule.getRT({id: account_id,cl: client}, client);
						new_secret_key = AuthModule.getSecret(device_id, client);
						AccountToken.update({
							_id : account_token._id
						},{
							tokn : new_token,
							skey : new_secret_key,
							act : true
						},function(err, updated_token){
							if(err){
								logger.error({"r":"g_rt","m":"POST","msg":"accounttoken_update_server_error","p":req.body});
								return sendError(res,err,"server_error");
							}
							if(updated_token){
								return sendSuccess(res,{ aid : account_id, rt : new_token, secret : new_secret_key});
							} else {
								logger.error({"r":"g_rt","m":"POST","msg":"accounttoken_update_error","p":req.body});
								return sendError(res,err,"server_error");
							}
						});
			    } else {
			    	return sendSuccess(res,{ aid : account_id, rt : account_token.tokn, secret : account_token.skey });
			    }
				});
			} else {
				new_token      = AuthModule.getRT({id: account_id,cl: client}, client);
				new_secret_key = AuthModule.getSecret(device_id, client);
				var new_refresh_token = new AccountToken({
					aid : account_id,
					udid: device_id,
					clnt: client,
					tokn: new_token,
					skey: new_secret_key,
					clv : client_vrsn,
					vrsn: version,
					act : true,
					revk: false
				});
				new_refresh_token.save(function(err, new_account_token){
					if(err){ 
						logger.error({"r":"g_rt","m":"POST","msg":"new_refresh_token_save_error","p":req.body});
						return sendError(res,err,"server_error"); 
					}
					if(!new_account_token) { 
						logger.error({"r":"g_rt","m":"POST","msg":"new_account_token_save_error","p":req.body});
						return sendError(res,err,"server_error"); 
					}
					return sendSuccess(res,{ aid : account_id, rt : new_token, secret : new_secret_key });
				});
			}
		});
	});
});

/*
 *When users current access token has expired then he needs to send his valid refresh token
  along with his device id for that refresh token . If his refresh token is still not expired,
  then he will be assined a new Access token for making further http requests.
 */
router.post('/g_at', function(req, res, next) {
	req.checkBody('a_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('d_id',errorCodes.invalid_parameters[1]).notEmpty().isValidDeviceId();
	req.checkBody('cl',errorCodes.invalid_parameters[1]).notEmpty().isValidTokenClient();
	req.checkBody('rt',errorCodes.invalid_parameters[1]).notEmpty().isValidRefreshToken();

	if(req.validationErrors()){ 
		logger.error({"r":"g_at","m":"POST","msg":"invalid_parameters","p":req.body});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST); 
	}

	var aid		    		= req.body.a_id;
	var device_id 		= req.body.d_id;
	var client    		= req.body.cl;
	var refresh_token = req.body.rt;
	var tim       		= new Date().getTime();

	jwt.verify(refresh_token, configs.JWT_REFRESH_TOKEN_PRIVATE_KEY, function(err, decoded) {      
    if(err){
    	logger.error({"r":"g_at","m":"POST","msg":"refresh_token_expired","p":req.body});
    	MailClient.informTechTeam("REFRESH_TOKEN_EXP_IOS",req.body);
      return sendError(res,"Refresh Token expired","refresh_token_expired",constants.HTTP_STATUS.TOKEN_EXPIRED);  
    } else {
    	if(decoded.id !== aid){
    		logger.error({"r":"g_at","m":"POST","msg":"access_token_denied","p":req.body});
    		return sendError(res,"Access Token Denied","access_token_denied",constants.HTTP_STATUS.BAD_REQUEST);
    	}
			AccountToken.findOne({
				aid : aid,
				// udid: device_id,
				clnt: client,
				tokn: refresh_token,
				act : true,
				revk: false
			},function(err,account_token){
				if(err){ 
					logger.error({"r":"g_at","m":"POST","msg":"accounttoken_find_server_error","p":req.body});
					return sendError(res,err,"server_error"); 
				}
				if(!account_token){
					logger.error({"r":"g_at","m":"POST","msg":"invalid_refresh_token","p":req.body}); 
					return sendError(res,"Refresh Token Invalid!","invalid_refresh_token",constants.HTTP_STATUS.BAD_REQUEST); 
				}
				var new_access_token = AuthModule.getAT({id: aid,cl: client}, client);
				return sendSuccess(res,{ aid : aid, at : new_access_token});
			});
		}
  });
});

/*
 * get access token request for web
 */
router.post('/g_at_w', function(req, res, next) {
	req.checkBody('a_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('d_id',errorCodes.invalid_parameters[1]).notEmpty().isValidDeviceId();
	req.checkBody('cl',errorCodes.invalid_parameters[1]).notEmpty().isValidTokenClient();
	req.checkBody('rt',errorCodes.invalid_parameters[1]).notEmpty().isValidRefreshToken();
	req.checkBody('at',errorCodes.invalid_parameters[1]).notEmpty().isValidAccessToken();
	req.checkBody('k_ey',errorCodes.invalid_parameters[1]).notEmpty().isValidTokenSecretKey();

	if(req.validationErrors()){
		logger.error({"r":"g_at_w","m":"POST","msg":"invalid_parameters","p":req.body}); 
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var aid		    		= req.body.a_id;
	var device_id 		= req.body.d_id;
	var client    		= req.body.cl;
	var refresh_token = req.body.rt;
	var access_token  = req.body.at;
	var secret_key    = req.body.k_ey;
	var tim       		= new Date().getTime();

	jwt.verify(access_token, configs.JWT_ACCESS_TOKEN_PRIVATE_KEY, function(err, at_decoded) { 
		if(err){
			if(err.name == "TokenExpiredError" && jwt.decode(access_token).id == aid){
				jwt.verify(refresh_token, configs.JWT_REFRESH_TOKEN_PRIVATE_KEY, function(err, rt_decoded) {      
			    if(err){
			    	logger.error({"r":"g_at_w","m":"POST","msg":"refresh_token_expired","p":req.body}); 
			    	MailClient.informTechTeam("REFRESH_TOKEN_EXP_WEB",req.body);
			      return sendError(res,"Refresh Token expired","refresh_token_expired",constants.HTTP_STATUS.TOKEN_EXPIRED);  
			    } else {
			    	if(rt_decoded.id !== aid){
			    		logger.error({"r":"g_at_w","m":"POST","msg":"access_token_denied","p":req.body}); 
			    		return sendError(res,"Access Token Denied","access_token_denied",constants.HTTP_STATUS.BAD_REQUEST);
			    	}
						AccountToken.findOne({
							aid : aid,
							// udid: device_id,
							clnt: client,
							act : true,
							tokn: refresh_token,
							revk: false,
							skey: secret_key
						},function(err,account_token){
							if(err){ 
								logger.error({"r":"g_at_w","m":"POST","msg":"accounttoken_find_server_error","p":req.body}); 
								return sendError(res,err,"server_error"); 
							}
							if(!account_token) { 
								logger.error({"r":"g_at_w","m":"POST","msg":"invalid_refresh_token","p":req.body}); 
								return sendError(res,"Refresh Token Invalid!","invalid_refresh_token",constants.HTTP_STATUS.BAD_REQUEST); 
							}
							var new_access_token = AuthModule.getAT({id: aid,cl: client}, client);
							return sendSuccess(res,{ aid : aid, at : new_access_token});
						});
					}
			  });
			} else {
				logger.error({"r":"g_at_w","m":"POST","msg":"invalid_AT","p":req.body}); 
				return sendError(res,"invalid AT","invalid_AT",constants.HTTP_STATUS.BAD_REQUEST);
			}
		} else if(at_decoded.id == aid){
			return sendSuccess(res,{ aid : aid, at : access_token});
		} else {
			logger.error({"r":"g_at_w","m":"POST","msg":"not_authorized","p":req.body}); 
			return sendError(res,"not authorized","not_authorized",constants.HTTP_STATUS.UNAUTHORIZED);
		}
	});
});

//route middleware to check brute force for invalid otp verification attempts
router.use(function(req, res, next){
	var aid = req.body.a_id || req.query.a_id || req.body.aid || req.query.aid;
	if(aid){
		var tim = new Date().getTime();
		BruteForce.check(aid, tim, function(err,bruteForce){
			if(err){
				bruteForceAcclogger.error({"r":"account_brute_force_DbError","p":{aid:aid},"err":err});
				return sendError(res,err,"server_error"); 
			}
			if(bruteForce){
				bruteForceAcclogger.error({"r":"account_brute_force_found","p":{aid:aid,lmt:bruteForce.lmt,tm:bruteForce.tm}});
				return sendError(res,"Too many requests","too_many_requests",constants.HTTP_STATUS.TOO_MANY_REQUESTS);
			}
			next();
		});
	} else {
		next();
	}
});

/*
 *Verify the OTP received from the client
 */
router.post('/verify_otp', function(req, res, next) {
	req.checkBody('otp',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('aid',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('tokn',errorCodes.invalid_parameters[1]).notEmpty().validateToken();

	if(req.validationErrors()){ 
		logger.error({"r":"verify_otp","m":"POST","msg":"invalid_parameters","p":req.body});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var code 	   = req.body.otp;
	var aid		   = req.body.aid;
	var tim      = new Date().getTime();
	var new_prof = 0;

	Account.findOne({_id : aid},function(err,account){
		if(err){ 
			logger.error({"r":"verify_otp","m":"POST","msg":"account_find_server_error","p":req.body});
			return sendError(res,err,"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}
		if(!account){ 
			BruteForce.mark(aid, tim, function(err,resp){
				if(err){
					logger.error({"r":"verify_otp","m":"POST","msg":"BruteForce_mark_account_error","p":req.body,"err":err});
				}
			});
			logger.error({"r":"verify_otp","m":"POST","msg":"no_account_found","p":req.body});
			return sendError(res,"OTP Object not found","otp_error",constants.HTTP_STATUS.BAD_REQUEST);
		}
		if(!account.vrfy){ 
			new_prof = 1; 
		}
		Otp.findOne({
			aid 	: aid,
			cdst	: Otp.STATUS.NOTUSED,
			code 	: code
		},function(err,otp_obj) {
			if(err){ 
				logger.error({"r":"verify_otp","m":"POST","msg":"otp_find_server_error","p":req.body});
				return sendError(res,err,"server_error",constants.HTTP_STATUS.BAD_REQUEST); 
			}
			if(!otp_obj){ 
				BruteForce.mark(aid, tim, function(err,resp){
					if(err){
						logger.error({"r":"verify_otp","m":"POST","msg":"BruteForce_mark_otp_error","p":req.body,"err":err});
					}
				});
				logger.error({"r":"verify_otp","m":"POST","msg":"otp_error","p":req.body});
				return sendError(res,"OTP Object not found","otp_error",constants.HTTP_STATUS.BAD_REQUEST);
			}
			if(!prevOtpStillValid(tim, otp_obj.gnat)){ 
				logger.error({"r":"verify_otp","m":"POST","msg":"otp_expired","p":req.body});
				return sendError(res,"OTP has expired","otp_expired",constants.HTTP_STATUS.BAD_REQUEST);  
			}
			Otp.update({
				_id : otp_obj._id
			},{
				cdst : Otp.STATUS.USED
			},function(err){
				if(err) { 
					logger.error({"r":"verify_otp","m":"POST","msg":"otp_update_server_error","p":req.body});
					return sendError(res,err,"server_error");
				}
				Account.update({
					_id : account._id
				},{
					vrfy    : true,
					vrfy_at : Date()
				},function(err_account){
					if(err_account){ 
						logger.error({"r":"verify_otp","m":"POST","msg":"account_update_server_error","p":req.body});
						return sendError(res,err_account,"server_error");
					}
					Profile.find({
						aid : account._id,
						act : true
					},function(err,profiles){
						if(err){ 
							logger.error({"r":"verify_otp","m":"POST","msg":"profile_find_server_error","p":req.body});
							return sendError(res,err,"server_error"); 
						}
						var profile_ids = [];
						for(var indx=0;indx < profiles.length; indx++) {
							var profile = profiles[indx];
							profile_ids.push(profile["_id"]);
						}
						GroupMember.find({
							pid : {$in : profile_ids},
							act : true,
							gdel: false
						},function(err,gms){
							if(err){ 
								logger.error({"r":"verify_otp","m":"POST","msg":"groupmember_find_server_error","p":req.body});
								return sendError(res,err,"server_error");
							}
							var pid_grp = {};
							var grp_ids = [];
							for(var indx in gms) {
								var gm = gms[indx];
								grp_ids.push(gm["gid"]);
							}
							Group.find({
								_id : {$in : grp_ids},
								act : true
							},function(err,grps){
								if(err){
									logger.error({"r":"verify_otp","m":"POST","msg":"group_find_server_error","p":req.body}); 
									return sendError(res,err,"server_error");
								}
								UnsecureAccounts.add(aid, null, function(err,resp){
									if(err){
										logger.error({"r":"verify_otp","m":"POST","msg":"UnsecureAccounts_add_error","p":req.body,"err":err});
									}
									BruteForce.unmark(aid, function(err,resp){
										if(err){
											logger.error({"r":"verify_otp","m":"POST","msg":"BruteForce_unmark_error","p":req.body,"err":err});
										}
										resetOtpAttemptForAcc("verify_otp", account.m, aid);
										resetGroupMuteOnVerifyOtp("verify_otp", grp_ids, aid);
										return sendSuccess(res,{ Profile : profiles, GroupMember: gms, Group : grps, if_new_prof : new_prof, tim : tim});
									});
								});
							});
						});
					});
				});
			});
		});
	});
});

/*
 *Verify the OTP received from the client , this api will give referesh token on successfull verification
 */
router.post('/verify_otp_v16', function(req, res, next) {
	req.checkBody('otp',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('aid',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('d_id',errorCodes.invalid_parameters[1]).notEmpty().isValidDeviceId();
	req.checkBody('cl',errorCodes.invalid_parameters[1]).notEmpty().isValidTokenClient();
	req.checkBody('clv',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('vrsn',errorCodes.invalid_parameters[1]).notEmpty().isValidVersion();

	if(req.validationErrors()){
		logger.error({"r":"verify_otp_v16","m":"POST","msg":"invalid_parameters","p":req.body}); 
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var code 	    	= req.body.otp;
	var aid		    	= req.body.aid;
	var device_id 	= req.body.d_id;
	var client    	= req.body.cl;
	var client_vrsn = req.body.clv;
	var version     = req.body.vrsn;
	var tim       	= new Date().getTime();
	var new_prof  	= 0;

	Account.findOne({_id : aid},function(err,account){
		if(err){ 
			logger.error({"r":"verify_otp_v16","m":"POST","msg":"account_find_server_error","p":req.body});
			return sendError(res,err,"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}
		if(!account){ 
			BruteForce.mark(aid, tim, function(err,resp){
				if(err){
					logger.error({"r":"verify_otp_v16","m":"POST","msg":"BruteForce_mark_account_error","p":req.body,"err":err});
				}
			});
			logger.error({"r":"verify_otp_v16","m":"POST","msg":"no_account_found","p":req.body});
			return sendError(res,"Account not found!","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST); 
		}
		if(!account.vrfy){ 
			new_prof = 1; 
		}
		Otp.findOne({
			aid 	: aid,
			cdst	: Otp.STATUS.NOTUSED,
			code 	: code
		},function(err,otp_obj) {
			if(err){ 
				logger.error({"r":"verify_otp_v16","m":"POST","msg":"otp_find_server_error","p":req.body});
				return sendError(res,err,"server_error",constants.HTTP_STATUS.BAD_REQUEST);
			}
			if(!otp_obj){
				BruteForce.mark(aid, tim, function(err,resp){
					if(err){
						logger.error({"r":"verify_otp_v16","m":"POST","msg":"BruteForce_mark_otp_error","p":req.body,"err":err});
					}
				});
				logger.error({"r":"verify_otp_v16","m":"POST","msg":"otp_error","p":req.body}); 
				return sendError(res,"OTP Object not found","otp_error",constants.HTTP_STATUS.BAD_REQUEST);
			}
			if(!prevOtpStillValid(tim, otp_obj.gnat)){ 
				logger.error({"r":"verify_otp_v16","m":"POST","msg":"otp_expired","p":req.body});
				return sendError(res,"OTP has expired","otp_expired",constants.HTTP_STATUS.BAD_REQUEST); 
			}
			manageRefreshToken(aid, device_id, client, client_vrsn, version ,function(err, rt_at_secret){
				if(err){
					logger.error({"r":"verify_otp_v16","m":"POST","msg":"manageRefreshToken_error","p":req.body}); 
					return sendError(res,err,"server_error");
				}
				if(!rt_at_secret){ 
					logger.error({"r":"verify_otp_v16","m":"POST","msg":"rf_token_not_gen","p":req.body});
					return sendError(res,"Refresh token generation error","rf_token_not_gen",constants.HTTP_STATUS.NOT_FOUND);
				}
				Otp.update({
					_id : otp_obj._id
				},{
					cdst : Otp.STATUS.USED
				},function(err){
					if(err){ 
						logger.error({"r":"verify_otp_v16","m":"POST","msg":"otp_update_server_error","p":req.body});
						return sendError(res,err,"server_error");
					}
					Account.update({
						_id : account._id
					},{
						vrfy    : true,
						vrfy_at : Date()
					},function(err_account){
						if(err_account){ 
							logger.error({"r":"verify_otp_v16","m":"POST","msg":"account_update_server_error","p":req.body});
							return sendError(res,err_account,"server_error");
						}
						Profile.find({
							aid : account._id,
							act : true
						},function(err,profiles){
							if(err){ 
								logger.error({"r":"verify_otp_v16","m":"POST","msg":"profile_find_server_error","p":req.body});
								return sendError(res,err,"server_error");
							}
							var profile_ids = [];
							for(var indx=0;indx < profiles.length; indx++) {
								var profile = profiles[indx];
								profile_ids.push(profile["_id"]);
							}
							GroupMember.find({
								pid : {$in : profile_ids},
								act : true,
								gdel: false
							},function(err,gms){
								if(err){ 
									logger.error({"r":"verify_otp_v16","m":"POST","msg":"groupmember_find_server_error","p":req.body});
									return sendError(res,err,"server_error");
								}
								var pid_grp = {};
								var grp_ids = [];
								for(var indx in gms) {
									var gm = gms[indx];
									grp_ids.push(gm["gid"]);
								}
								Group.find({
									_id : {$in : grp_ids},
									act : true
								},function(err,grps){
									if(err){ 
										logger.error({"r":"verify_otp_v16","m":"POST","msg":"group_find_server_error","p":req.body});
										return sendError(res,err,"server_error");
									}
									UnsecureAccounts.add(aid, client, function(err,resp){
										if(err){
											logger.error({"r":"verify_otp_v16","m":"POST","msg":"UnsecureAccounts_add_error","p":req.body,"err":err});
										}
										BruteForce.unmark(aid, function(err,resp){
											if(err){
												logger.error({"r":"verify_otp_v16","m":"POST","msg":"BruteForce_unmark_error","p":req.body,"err":err});
											}
											resetCallMeAttemptForDevice('verify_otp_v16',aid);
											resetOtpAttemptForAcc('verify_otp_v16', account.m, aid);
											dropUserQueues('verify_otp_v16', profile_ids, aid, function(err,resp){
												if(err){
													logger.error({"r":"verify_otp_v16","msg":"dropUserQueues_error","p":req.body,"err":err});
												}
												resetGroupMuteOnVerifyOtp("verify_otp_v16", grp_ids, aid);
								        return sendSuccess(res,{ 
								        	Profile : profiles, 
								        	GroupMember: gms, 
								        	Group : grps, 
								        	rt : rt_at_secret.rt, 
								        	secret : rt_at_secret.secret,
								        	at : rt_at_secret.at, 
								        	if_new_prof : new_prof,
								        	tim : tim}
								        );
								      });
							      });
						      });
								});
							});
						});
					});
				});
			});
		});
	});
});

/*
 *Verify the OTP received from the client , this api will give referesh token on successfull verification
 */
router.post('/v_otp_w', function(req, res, next) {
	req.checkBody('otp',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('aid',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('d_id',errorCodes.invalid_parameters[1]).notEmpty().isValidDeviceId();
	req.checkBody('cl',errorCodes.invalid_parameters[1]).notEmpty().isValidTokenClient();
	req.checkBody('clv',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('vrsn',errorCodes.invalid_parameters[1]).notEmpty().isValidVersion();

	if(req.validationErrors()){ 
		logger.error({"r":"v_otp_w","m":"POST","msg":"invalid_parameters","p":req.body});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var code 	    	= req.body.otp;
	var aid		    	= req.body.aid;
	var device_id 	= req.body.d_id;
	var client    	= req.body.cl;
	var client_vrsn = req.body.clv;
	var version     = req.body.vrsn;
	var tim       	= new Date().getTime();
	var new_prof  	= 0;

	Account.findOne({_id : aid},function(err,account){
		if(err){ 
			logger.error({"r":"v_otp_w","m":"POST","msg":"account_find_server_error","p":req.body});
			return sendError(res,err,"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}
		if(!account){ 
			BruteForce.mark(aid, tim, function(err,resp){
				if(err){
					logger.error({"r":"v_otp_w","m":"POST","msg":"BruteForce_mark_account_error","p":req.body,"err":err});
				}
			});
			logger.error({"r":"v_otp_w","m":"POST","msg":"no_account_found","p":req.body});
			return sendError(res,"Account not found!","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}
		if(!account.vrfy){ 
			new_prof = 1; 
		}
		Otp.findOne({
			aid 	: aid,
			cdst	: Otp.STATUS.NOTUSED,
			code 	: code
		},function(err,otp_obj) {
			if(err){
				logger.error({"r":"v_otp_w","m":"POST","msg":"otp_find_server_error","p":req.body}); 
				return sendError(res,err,"server_error",constants.HTTP_STATUS.BAD_REQUEST); 
			}
			if(!otp_obj){
				BruteForce.mark(aid, tim, function(err,resp){
					if(err){
						logger.error({"r":"v_otp_w","m":"POST","msg":"BruteForce_mark_otp_error","p":req.body,"err":err});
					}
				});
				logger.error({"r":"v_otp_w","m":"POST","msg":"otp_error","p":req.body}); 
				return sendError(res,"OTP Object not found","otp_error",constants.HTTP_STATUS.BAD_REQUEST);
			}
			if(!prevOtpStillValid(tim, otp_obj.gnat)) { 
				logger.error({"r":"v_otp_w","m":"POST","msg":"otp_expired","p":req.body});
				return sendError(res,"OTP has expired","otp_expired",constants.HTTP_STATUS.BAD_REQUEST); 
			}
			manageRefreshToken(aid, device_id, client, client_vrsn, version ,function(err, rt_at_secret){
				if(err){
					logger.error({"r":"v_otp_w","m":"POST","msg":"manageRefreshToken_error","p":req.body}); 
					return sendError(res,err,"server_error");
				}
				if(!rt_at_secret){ 
					logger.error({"r":"v_otp_w","m":"POST","msg":"rf_token_not_gen","p":req.body});
					return sendError(res,"Refresh token generation error","rf_token_not_gen",constants.HTTP_STATUS.NOT_FOUND);
				}
				Otp.update({
					_id : otp_obj._id
				},{
					cdst : Otp.STATUS.USED
				},function(err){
					if(err){ 
						logger.error({"r":"v_otp_w","m":"POST","msg":"otp_update_server_error","p":req.body});
						return sendError(res,err,"server_error");
					}
					Account.update({
						_id : account._id
					},{
						vrfy    : true,
						vrfy_at : Date()
					},function(err_account){
						if(err_account) { 
							logger.error({"r":"v_otp_w","m":"POST","msg":"account_update_server_error","p":req.body});
							return sendError(res,err_account,"server_error");
						}
						Profile.find({
							aid : account._id,
							act : true
						},function(err,profiles){
							if(err) { 
								logger.error({"r":"v_otp_w","m":"POST","msg":"profile_find_server_error","p":req.body});
								return sendError(res,err,"server_error");
							}
							var profile_ids = [];
							for(var indx=0;indx < profiles.length; indx++) {
								var profile = profiles[indx];
								profile_ids.push(profile["_id"]);
							}
							GroupMember.find({
								pid : {$in : profile_ids},
								act : true,
								gdel: false
							},function(err,gms){
								if(err){ 
									logger.error({"r":"v_otp_w","m":"POST","msg":"groupmember_find_server_error","p":req.body});
									return sendError(res,err,"server_error");
								}
								var pid_grp = {};
								var grp_ids = [];
								for(var indx in gms) {
									var gm = gms[indx];
									grp_ids.push(gm["gid"]);
								}
								Group.find({
									_id : {$in : grp_ids},
									act : true
								},function(err,grps){
									if(err){ 
										logger.error({"r":"v_otp_w","m":"POST","msg":"group_find_server_error","p":req.body});
										return sendError(res,err,"server_error");
									}
									BruteForce.unmark(aid, function(err,resp){
										if(err){
											logger.error({"r":"v_otp_w","m":"POST","msg":"BruteForce_unmark_error","p":req.body,"err":err});
										}
										resetCallMeAttemptForDevice('v_otp_w',aid);
										resetOtpAttemptForAcc('v_otp_w', account.m, aid);
						        return sendSuccess(res,{ 
						        	Profile : profiles, 
						        	GroupMember: gms, 
						        	Group : grps, 
						        	rt : rt_at_secret.rt, 
						        	secret : rt_at_secret.secret,
						        	at : rt_at_secret.at, 
						        	if_new_prof : new_prof,
						        	tim : tim}
						        );
						      });
								});
							});
						});
					});
				});
			});
		});
	});
});

/*
 *Verify the OTP received from the Android client , this api will give referesh token on successfull verification
 */
router.post('/v_otp_a', function(req, res, next) {
	req.checkBody('otp',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('aid',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('d_id',errorCodes.invalid_parameters[1]).notEmpty().isValidDeviceId();
	req.checkBody('cl',errorCodes.invalid_parameters[1]).notEmpty().isValidTokenClient();
	req.checkBody('clv',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('vrsn',errorCodes.invalid_parameters[1]).notEmpty().isValidVersion();

	if(req.validationErrors()){
		logger.error({"r":"v_otp_a","m":"POST","msg":"invalid_parameters","p":req.body}); 
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var code 	    	= req.body.otp;
	var aid		    	= req.body.aid;
	var device_id 	= req.body.d_id;
	var client    	= req.body.cl;
	var client_vrsn = req.body.clv;
	var version     = req.body.vrsn;
	var tim       	= new Date().getTime();
	var new_prof  	= 0;

	Account.findOne({_id : aid},function(err,account){
		if(err){ 
			logger.error({"r":"v_otp_a","m":"POST","msg":"account_find_server_error","p":req.body});
			return sendError(res,err,"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}
		if(!account){
			BruteForce.mark(aid, tim, function(err,resp){
				if(err){
					logger.error({"r":"v_otp_a","m":"POST","msg":"BruteForce_mark_account_error","p":req.body,"err":err});
				}
			}); 
			logger.error({"r":"v_otp_a","m":"POST","msg":"no_account_found","p":req.body});
			return sendError(res,"Account not found!","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST); 
		}
		if(!account.vrfy){ 
			new_prof = 1; 
		}
		Otp.findOne({
			aid 	: aid,
			cdst	: Otp.STATUS.NOTUSED,
			code 	: code
		},function(err,otp_obj) {
			if(err){ 
				logger.error({"r":"v_otp_a","m":"POST","msg":"otp_find_server_error","p":req.body});
				return sendError(res,err,"server_error",constants.HTTP_STATUS.BAD_REQUEST);
			}
			if(!otp_obj){
				BruteForce.mark(aid, tim, function(err,resp){
					if(err){
						logger.error({"r":"v_otp_a","m":"POST","msg":"BruteForce_mark_otp_error","p":req.body,"err":err});
					}
				}); 
				logger.error({"r":"v_otp_a","m":"POST","msg":"otp_error","p":req.body}); 
				return sendError(res,"OTP Object not found","otp_error",constants.HTTP_STATUS.BAD_REQUEST);
			}
			if(!prevOtpStillValid(tim, otp_obj.gnat)){ 
				logger.error({"r":"v_otp_a","m":"POST","msg":"otp_expired","p":req.body});
				return sendError(res,"OTP has expired","otp_expired",constants.HTTP_STATUS.BAD_REQUEST); 
			}
			manageRefreshToken(aid, device_id, client, client_vrsn, version ,function(err, rt_at_secret){
				if(err){
					logger.error({"r":"v_otp_a","m":"POST","msg":"manageRefreshToken_error","p":req.body}); 
					return sendError(res,err,"server_error");
				}
				if(!rt_at_secret){ 
					logger.error({"r":"v_otp_a","m":"POST","msg":"rf_token_not_gen","p":req.body});
					return sendError(res,"Refresh token generation error","rf_token_not_gen",constants.HTTP_STATUS.NOT_FOUND);
				}
				Otp.update({
					_id : otp_obj._id
				},{
					cdst : Otp.STATUS.USED
				},function(err){
					if(err){ 
						logger.error({"r":"v_otp_a","m":"POST","msg":"otp_update_server_error","p":req.body});
						return sendError(res,err,"server_error");
					}
					Account.update({
						_id : account._id
					},{
						vrfy    : true,
						vrfy_at : Date()
					},function(err_account){
						if(err_account){ 
							logger.error({"r":"v_otp_a","m":"POST","msg":"account_update_server_error","p":req.body});
							return sendError(res,err_account,"server_error");
						}
						Profile.find({
							aid : account._id,
							act : true
						},function(err,profiles){
							if(err){ 
								logger.error({"r":"v_otp_a","m":"POST","msg":"profile_find_server_error","p":req.body});
								return sendError(res,err,"server_error");
							}
							var profile_ids = [];
							for(var indx=0;indx < profiles.length; indx++) {
								var profile = profiles[indx];
								profile_ids.push(profile["_id"]);
							}
							GroupMember.find({
								pid : {$in : profile_ids},
								act : true,
								gdel: false
							},function(err,gms){
								if(err){ 
									logger.error({"r":"v_otp_a","m":"POST","msg":"groupmember_find_server_error","p":req.body});
									return sendError(res,err,"server_error");
								}
								var pid_grp = {};
								var grp_ids = [];
								for(var indx in gms) {
									var gm = gms[indx];
									grp_ids.push(gm["gid"]);
								}
								Group.find({
									_id : {$in : grp_ids},
									act : true
								},function(err,grps){
									if(err){ 
										logger.error({"r":"v_otp_a","m":"POST","msg":"group_find_server_error","p":req.body});
										return sendError(res,err,"server_error");
									}
									UnsecureAccounts.removeAll(aid, function(err,resp){
										if(err){
											logger.error({"r":"v_otp_a","m":"POST","msg":"UnsecureAccounts_remove_error","p":req.body,"err":err});
										}
										BruteForce.unmark(aid, function(err,resp){
											if(err){
												logger.error({"r":"v_otp_a","m":"POST","msg":"BruteForce_unmark_error","p":req.body,"err":err});
											}
											resetCallMeAttemptForDevice('v_otp_a',aid);
											resetOtpAttemptForAcc('v_otp_a', account.m, aid);
											dropUserQueues('v_otp_a',profile_ids, aid, function(err,resp){
												if(err){
													logger.error({"r":"v_otp_a","msg":"dropUserQueues_error","p":req.body,"err":err});
												}
												resetGroupMuteOnVerifyOtp("v_otp_a", grp_ids, aid);
								        return sendSuccess(res,{ 
								        	Profile : profiles, 
								        	GroupMember: gms, 
								        	Group : grps, 
								        	rt : rt_at_secret.rt, 
								        	secret : rt_at_secret.secret,
								        	at : rt_at_secret.at, 
								        	if_new_prof : new_prof,
								        	tim : tim,
								        	referrer : {}}
								        );
								      });
							      });
							    });
								});
							});
						});
					});
				});
			});
		});
	});
});

/*
 *Verify the OTP received from the client , this api will give referesh token on successfull verification
 */
router.post('/r_vrf_a', function(req, res, next) {
	req.checkBody('otp',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('aid',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('d_id',errorCodes.invalid_parameters[1]).notEmpty().isValidDeviceId();
	req.checkBody('cl',errorCodes.invalid_parameters[1]).notEmpty().isValidTokenClient();
	req.checkBody('clv',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('vrsn',errorCodes.invalid_parameters[1]).notEmpty().isValidVersion();

	if(req.validationErrors()){
		logger.error({"r":"r_vrf_a","m":"POST","msg":"invalid_parameters","p":req.body}); 
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var code 	    	= req.body.otp;
	var aid		    	= req.body.aid;
	var device_id 	= req.body.d_id;
	var client    	= req.body.cl;
	var client_vrsn = req.body.clv;
	var version     = req.body.vrsn;
	var tim       	= new Date().getTime();
	var new_prof  	= 0;

	Account.findOne({
		_id : aid,
		vrfy: true
	},function(err,account){
		if(err){ 
			logger.error({"r":"r_vrf_a","m":"POST","msg":"account_find_server_error","p":req.body});
			return sendError(res,err,"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}
		if(!account){
			BruteForce.mark(aid, tim, function(err,resp){
				if(err){
					logger.error({"r":"r_vrf_a","m":"POST","msg":"BruteForce_mark_account_error","p":req.body,"err":err});
				}
			});  
			logger.error({"r":"r_vrf_a","m":"POST","msg":"no_account_found","p":req.body});
			return sendError(res,"Account not found!","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST); 
		}
		Otp.findOne({
			aid 	: aid,
			cdst	: Otp.STATUS.NOTUSED,
			code 	: code
		},function(err,otp_obj) {
			if(err){ 
				logger.error({"r":"r_vrf_a","m":"POST","msg":"otp_find_server_error","p":req.body});
				return sendError(res,err,"server_error",constants.HTTP_STATUS.BAD_REQUEST);
			}
			if(!otp_obj){
				BruteForce.mark(aid, tim, function(err,resp){
					if(err){
						logger.error({"r":"r_vrf_a","m":"POST","msg":"BruteForce_mark_otp_error","p":req.body,"err":err});
					}
				});
				logger.error({"r":"r_vrf_a","m":"POST","msg":"otp_error","p":req.body}); 
				return sendError(res,"OTP Object not found","otp_error",constants.HTTP_STATUS.BAD_REQUEST);
			}
			if(!prevOtpStillValid(tim, otp_obj.gnat)){ 
				logger.error({"r":"r_vrf_a","m":"POST","msg":"otp_expired","p":req.body});
				return sendError(res,"OTP has expired","otp_expired",constants.HTTP_STATUS.BAD_REQUEST); 
			}
			manageRefreshToken(aid, device_id, client, client_vrsn, version ,function(err, rt_at_secret){
				if(err){
					logger.error({"r":"r_vrf_a","m":"POST","msg":"manageRefreshToken_error","p":req.body}); 
					return sendError(res,err,"server_error");
				}
				if(!rt_at_secret){ 
					logger.error({"r":"r_vrf_a","m":"POST","msg":"rf_token_not_gen","p":req.body});
					return sendError(res,"Refresh token generation error","rf_token_not_gen",constants.HTTP_STATUS.NOT_FOUND);
				}
				Otp.update({
					_id : otp_obj._id
				},{
					cdst : Otp.STATUS.USED
				},function(err){
					if(err){ 
						logger.error({"r":"r_vrf_a","m":"POST","msg":"otp_update_server_error","p":req.body});
						return sendError(res,err,"server_error");
					}
					UnsecureAccounts.removeAll(aid, function(err,resp){
						if(err){
							logger.error({"r":"r_vrf_a","m":"POST","msg":"UnsecureAccounts_remove_error","p":req.body,"err":err});
						}
						BruteForce.unmark(aid, function(err,resp){
							if(err){
								logger.error({"r":"r_vrf_a","m":"POST","msg":"BruteForce_unmark_error","p":req.body,"err":err});
							}
							resetCallMeAttemptForDevice('r_vrf_a',aid);
							resetOtpAttemptForAcc('r_vrf_a', account.m, aid);
			        return sendSuccess(res,{ 
			        	rt : rt_at_secret.rt, 
			        	secret : rt_at_secret.secret,
			        	at : rt_at_secret.at
			        });
			      });
			    });
				});
			});
		});
	});
});

/*
 * Call Me api for OTP
 */
router.post('/koo', function(req, res, next) {
	req.checkBody('a_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('d_id',errorCodes.invalid_parameters[1]).notEmpty().isValidDeviceId();
	req.checkBody('cl',errorCodes.invalid_parameters[1]).notEmpty().isValidTokenClient();
	req.checkBody('vrsn',errorCodes.invalid_parameters[1]).notEmpty().isValidVersion();
	req.checkBody('lng',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()){
		logger.error({"r":"koo","m":"POST","msg":"invalid_parameters","p":req.body});  
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var account_id       = req.body.a_id;
	var device_id        = req.body.d_id;
	var client           = req.body.cl;
	var version          = req.body.vrsn;
	var lang             = req.body.lng;
	var tim              = new Date().getTime();

	Account.findOne({
		_id : account_id
	},{
		m    : 1,
		ccod : 1
	},function(err,account){
		if(err){
			logger.error({"r":"koo","m":"POST","msg":"account_find_err","p":req.body}); 
			return sendError(res,err,"server_error");
		}
		if(!account){
			logger.error({"r":"koo","m":"POST","msg":"no_account_found","p":req.body}); 
			return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}
		OtpCall.findOne({
			aid  : account_id,
			act : false
		},function(err,inactive_signup_call){
			if(err){
				logger.error({"r":"koo","m":"POST","msg":"inacive_signup_call_server_error","p":req.body}); 
				return sendError(res,err,"server_error");
			}
			if(inactive_signup_call){
				logger.error({"r":"koo","m":"POST","msg":"account_device_id_banned_for_call","p":req.body});  
				return sendError(res,"Call Me Rejected","banned_for_call_me",constants.HTTP_STATUS.BAD_REQUEST);
			}
			Otp.findOne({
				aid : account_id,
				cdst: Otp.STATUS.NOTUSED
			},function(err,otp_obj){
				if(err){
					logger.error({"r":"koo","m":"POST","msg":"otp_find_server_error","p":req.body}); 
					return sendError(res,err,"server_error");
				}
				if(!otp_obj){
					logger.error({"r":"koo","m":"POST","msg":"no_otp_obj_exists","p":req.body});  
					return sendError(res,"Call Me Rejected","signup_call_rejected",constants.HTTP_STATUS.BAD_REQUEST);
				}
				var timeDiff = tim - otp_obj.gnat;
				if(timeDiff <= configs.CALL_ME_GENERATION_DELAY){
					logger.error({"r":"koo","m":"POST","msg":"call_me_generation_delay_not_reached","p":req.body});  
					return sendError(res,"Call Me Rejected","signup_call_rejected",constants.HTTP_STATUS.BAD_REQUEST);
				}
				if(timeDiff >= configs.CALL_ME_GENERATION_EXPIRY){
					logger.error({"r":"koo","m":"POST","msg":"call_me_generation_expired","p":req.body});  
					return sendError(res,"Call Me Rejected","call_otp_expired",constants.HTTP_STATUS.BAD_REQUEST);
				}
				var atmp = 0;
				var otp_call_id = '0';
				OtpCall.findOne({
					aid  : account_id,
					act  : true
				},function(err,active_signup_call){
					if(err){
						logger.error({"r":"koo","m":"POST","msg":"active_signup_call_server_error","p":req.body}); 
						return sendError(res,err,"server_error");
					}
					if(active_signup_call){
						if(active_signup_call.atmp >= configs.CALL_ME_MAX_LIMIT_PER_DEVICE){
							logger.error({"r":"koo","m":"POST","msg":"active_signup_call_max_atmpt_reached","p":req.body});  
							return sendError(res,"Call Me Rejected","call_max_atmpt_reached",constants.HTTP_STATUS.BAD_REQUEST);
						}
						var sentTimeDiff = tim - active_signup_call.ctim;
						if(sentTimeDiff <= configs.OTP_CALL_ME_REATTEMPT_DELAY){
							logger.error({"r":"koo","m":"POST","msg":"active_signup_call_reattempt_delay_not_reached","p":req.body});  
							return sendError(res,"Call Me Rejected","signup_call_rejected",constants.HTTP_STATUS.BAD_REQUEST);
						}
						atmp = active_signup_call.atmp+1;
						otp_call_id = active_signup_call._id+'';
						return proceedWithOtpCallMe(res,otp_call_id,account_id,account.m,account.ccod,otp_obj._id,otp_obj.code,atmp,tim);
					} else {
						var new_otp_call_obj = new OtpCall({
							aid  : account_id,
							udid : device_id,
							cl   : client,
							act  : true,
							ctim : tim,
							vrsn : version,
							atmp : 0
						});
						new_otp_call_obj.save(function(err,created_call_obj){
							if(err){
								logger.error({"r":"koo","m":"POST","msg":"new_otp_call_obj_save_server_error","p":req.body}); 
								return sendError(res,err,"server_error");
							}
							if(!created_call_obj){
								logger.error({"r":"koo","m":"POST","msg":"new_otp_call_obj_not_created_server_error","p":req.body}); 
								return sendError(res,err,"server_error");
							}
							otp_call_id = created_call_obj._id+'';
							return proceedWithOtpCallMe(res,otp_call_id,account_id,account.m,account.ccod,otp_obj._id,otp_obj.code,atmp,tim);
						});
					}
				});
			});
		});
	});
});

//route middleware to check brute force for invalid otp requests for a number
router.use(function(req, res, next){
	var phone = req.body.p_no || req.query.p_no;
	if(phone){
		var tim   = new Date().getTime();
		OtpAttempts.checkOtpElgibility(phone, tim, function(err,bruteForceOtp){
			if(err){
				bruteForceOtplogger.error({"r":req.originalUrl,"er":"request_otp_brute_force_DbError","p":{aid:aid},"err":err});
				return sendError(res,err,"server_error"); 
			}
			if(bruteForceOtp){
				bruteForceOtplogger.error({"r":req.originalUrl,"er":"request_otp_brute_force_found","p":{phone:phone,lmt:bruteForceOtp.lmt,tm:bruteForceOtp.tm}});
				return sendError(res,"Too many requests","too_many_requests",constants.HTTP_STATUS.TOO_MANY_REQUESTS);
			}
			next();
		});
	} else {
		next();
	}
});

/*
	login route for web
*/
router.post('/login_w', function(req, res, next) {
	req.checkBody('c_code',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('p_no',errorCodes.invalid_parameters[1]).notEmpty();
	
	if(req.validationErrors()){ 
		logger.error({"r":"login_w","m":"POST","msg":"invalid_parameters","p":req.body});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var checkMobile  = MobileValidator.checkPhoneNumber(req.body.p_no, req.body.c_code);
	if(!checkMobile.status){
		logger.error({"r":"login_w","m":"POST","msg":"invalid_phone_number","p":req.body}); 
		return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	} 

	var country_code = req.body.c_code;
	var phone_number = req.body.p_no;
	var tim          = new Date().getTime();

	Account.findOne({
		m    : phone_number,
		ccod : country_code 
	},{
		_id : 1,
		ccod: 1,
		m   : 1,
		vrfy: 1,
		vrsn: 1,
		cat : 1
	},function(err,old_account){
		if(err) { 
			logger.error({"r":"login_w","m":"POST","msg":"account_find_server_error","p":req.body});
			return sendError(res,err,"server_error"); 
		}
		if(!old_account){
			logger.error({"r":"login_w","m":"POST","msg":"no_account_found","p":req.body});
			// updateOtpAttemptResp("login_w", phone_number, tim, "error");
			return sendError(res,"Account not found!","no_account_found",constants.HTTP_STATUS.BAD_REQUEST);
		}
		BruteForce.check(old_account._id+'', tim, function(err,bruteForce){
			if(err){
				bruteForceAcclogger.error({"r":"login_w_brute_force_DbError","p":{aid:old_account._id+''},"err":err});
				return sendError(res,err,"server_error"); 
			}
			if(bruteForce){
				bruteForceAcclogger.error({"r":"login_w_brute_force_found","p":{aid:old_account._id+'',lmt:bruteForce.lmt,tm:bruteForce.tm}});
				return sendError(res,"Too many requests","too_many_requests",constants.HTTP_STATUS.TOO_MANY_REQUESTS);
			}
			OtpAttempts.updtOtpAtmpt(phone_number, tim, function(err,resp){
				if(err){
					logger.error({"r":"login_w","m":"POST","msg":"OtpAttempts_updtOtpAtmpt","err":err,"p":req.body});
					return sendError(res,"Account not found!","no_account_found",constants.HTTP_STATUS.BAD_REQUEST);
				}
				Otp.findOne({
					aid 	: old_account._id,
					cdst 	: Otp.STATUS.NOTUSED
				},function(err,old_otp){
					if(err) { 
						logger.error({"r":"login_w","m":"POST","msg":"otp_find_server_error","p":req.body});
						return sendError(res,err,"server_error"); 
					}
					if(!old_otp) { 
						return generateAndSendNewOtp(res,old_account); 
					}
					if(prevOtpStillValid(tim, old_otp.gnat)){
						return sendOtp(res, old_otp._id, old_otp.code, old_account);
					}
					Otp.update({
						_id : old_otp._id
					},{
						cdst : Otp.STATUS.INVALID
					},function(err){
						if(err){ 
							logger.error({"r":"login_w","m":"POST","msg":"otp_update_server_error","p":req.body});
							return sendError(res,err,"server_error"); 
						}
						resetCallMeAttemptForDevice('login_w',old_account._id+'');
						// updateOtpAttemptResp("login_w", phone_number, tim, "success");
						return generateAndSendNewOtp(res,old_account);	
					});
				});
			});
		});
	});
});

/*
 *request otp for ios and web clients.
 */
router.post('/request_otp_v18', function(req, res, next) {
	req.checkBody('c_code',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('p_no',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('vrsn',errorCodes.invalid_parameters[1]).notEmpty().isValidVersion();
	req.checkBody('client',errorCodes.invalid_parameters[1]).notEmpty().isValidClient();
	req.checkBody('role',errorCodes.invalid_parameters[1]).isInt().isValidRole();
	req.checkBody('email',errorCodes.invalid_parameters[1]).optional().isValidEmail();
	req.checkBody('name',errorCodes.invalid_parameters[1]).optional().notEmpty();
	req.checkBody('childname',errorCodes.invalid_parameters[1]).optional().notEmpty();
	req.checkBody('rf_link',errorCodes.invalid_parameters[1]).optional().notEmpty();

	if(req.validationErrors()){
		logger.error({"r":"request_otp_v18","m":"POST","msg":"invalid_parameters","p":req.body}); 
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var checkMobile  = MobileValidator.checkPhoneNumber(req.body.p_no, req.body.c_code);
	if(!checkMobile.status){
		logger.error({"r":"request_otp_v18","m":"POST","msg":"invalid_phone_number","p":req.body}); 
		return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	} 

	var country_code = req.body.c_code;
	var phone_number = req.body.p_no;
	var version      = req.body.vrsn;
	var client       = req.body.client;
	var role         = parseInt(req.body.role);
	var email        = req.body.email;
	var unam         = req.body.name;
	var cnam         = req.body.childname;
	var rf_link      = (req.body.rf_link) ? req.body.rf_link : "";
	var tim          = new Date().getTime();

	Account.findOne({
		m : phone_number,
	},{
		_id : 1,
		ccod: 1,
		m   : 1,
		vrfy: 1,
		vrsn: 1,
		cat : 1
	},function(err,old_account){
		if(err){ 
			logger.error({"r":"request_otp_v18","m":"POST","msg":"account_find_server_error","p":req.body});
			return sendError(res,err,"server_error"); 
		}
		if(old_account){
			BruteForce.check(old_account._id+'', tim, function(err,bruteForce){
				if(err){
					bruteForceAcclogger.error({"r":"request_otp_v18_brute_force_DbError","p":{aid:old_account._id+''},"err":err});
					return sendError(res,err,"server_error"); 
				}
				if(bruteForce){
					bruteForceAcclogger.error({"r":"request_otp_v18_brute_force_found","p":{aid:old_account._id+'',lmt:bruteForce.lmt,tm:bruteForce.tm}});
					return sendError(res,"Too many requests","too_many_requests",constants.HTTP_STATUS.TOO_MANY_REQUESTS);
				}
				OtpAttempts.updtOtpAtmpt(phone_number, tim, function(err,resp){
					if(err){
						logger.error({"r":"request_otp_v18","m":"POST","msg":"OtpAttempts_updtOtpAtmpt_old","err":err,"p":req.body});
						return sendError(res,"Account not found!","no_account_found",constants.HTTP_STATUS.BAD_REQUEST);
					}
					AccountInfo.updateVersionAndClientSeen(old_account._id, client, version, tim);
					Otp.findOne({
						aid 	: old_account._id,
						cdst 	: Otp.STATUS.NOTUSED
					},function(err,old_otp){
						if(err){ 
							logger.error({"r":"request_otp_v18","m":"POST","msg":"otp_find_server_error","p":req.body});
							return sendError(res,err,"server_error"); 
						}
						if(!old_otp) { 
							return generateAndSendNewOtp(res,old_account); 
						}
						if(prevOtpStillValid(tim, old_otp.gnat)){
							return sendOtp(res, old_otp._id, old_otp.code, old_account);
						}
						Otp.update({
							_id : old_otp._id
						},{
							cdst : Otp.STATUS.INVALID
						},function(err){
							if(err){ 
								logger.error({"r":"request_otp_v18","m":"POST","msg":"otp_update_server_error","p":req.body});
								return sendError(res,err,"server_error"); 
							}
							resetCallMeAttemptForDevice('request_otp_v18',old_account._id+'');
							return generateAndSendNewOtp(res,old_account);	
						});
					});
					if(typeof(unam) !== 'undefined' && typeof(role) !== 'undefined')
						createProfileIfNameSupplied(old_account._id, unam, role, cnam);
					if(typeof(email) !== 'undefined')
						ifNewEmailThenUpdate(old_account._id, email);
				});
			});
		} else {
			var new_account = getNewAccountCreationObj(country_code, phone_number, email, tim, client, version, rf_link);
			new_account.save(function(err,new_account){
				if(err){ 
					logger.error({"r":"request_otp_v18","m":"POST","msg":"new_account_save_error","p":req.body});
					return sendError(res,err,"server_error"); 
				}
				var new_profile_obj = {
					aid : new_account._id,
					nam : unam,
					role: role,
					act : true,
					ppic: null
				};
				if(role === Profile.TYPE.PARENT)
					new_profile_obj.cnam = cnam;
				var new_profile = new Profile(new_profile_obj);
				new_profile.save(function(err, new_profile_created){
					if(err){ 
						logger.error({"r":"request_otp_v18","m":"POST","msg":"new_profile_server_error","p":req.body});
						return sendError(res,err,"server_error"); 
					}
					return generateAndSendNewOtp(res,prepNewAccountObjResp(new_account));
				});
			});
		}
	});
});

/*
 *Request Otp for android version >= 1.16.
 */
router.post('/request_otp_v16', function(req, res, next) {
	req.checkBody('c_code',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('p_no',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('vrsn',errorCodes.invalid_parameters[1]).notEmpty().isValidVersion();
	req.checkBody('client',errorCodes.invalid_parameters[1]).notEmpty().isValidClient();
	req.checkBody('role',errorCodes.invalid_parameters[1]).isInt().isValidRole();
	req.checkBody('email',errorCodes.invalid_parameters[1]).optional().isValidEmail();
	req.checkBody('name',errorCodes.invalid_parameters[1]).optional().notEmpty();
	req.checkBody('childname',errorCodes.invalid_parameters[1]).optional().notEmpty();

	if(req.validationErrors()){
		logger.error({"r":"request_otp_v16","m":"POST","msg":"invalid_parameters","p":req.body}); 
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var checkMobile  = MobileValidator.checkPhoneNumber(req.body.p_no, req.body.c_code);
	if(!checkMobile.status){
		logger.error({"r":"request_otp_v16","m":"POST","msg":"invalid_phone_number","p":req.body}); 
		return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	} 

	var country_code = req.body.c_code;
	var phone_number = req.body.p_no;
	var version      = req.body.vrsn;
	var client       = req.body.client;
	var role         = parseInt(req.body.role);
	var email        = req.body.email;
	var unam         = req.body.name;
	var cnam         = req.body.childname;
	var tim          = new Date().getTime();

	Account.findOne({
		m : phone_number
	},{
		_id : 1,
		ccod: 1,
		m   : 1,
		vrfy: 1,
		vrsn: 1,
		cat : 1
	},function(err,old_account){
		if(err){ 
			logger.error({"r":"request_otp_v16","m":"POST","msg":"account_find_server_error","p":req.body});
			return sendError(res,err,"server_error"); 
		}
		if(old_account){
			BruteForce.check(old_account._id+'', tim, function(err,bruteForce){
				if(err){
					bruteForceAcclogger.error({"r":"request_otp_v18_brute_force_DbError","p":{aid:old_account._id+''},"err":err});
					return sendError(res,err,"server_error"); 
				}
				if(bruteForce){
					bruteForceAcclogger.error({"r":"request_otp_v18_brute_force_found","p":{aid:old_account._id+'',lmt:bruteForce.lmt,tm:bruteForce.tm}});
					return sendError(res,"Too many requests","too_many_requests",constants.HTTP_STATUS.TOO_MANY_REQUESTS);
				}
				OtpAttempts.updtOtpAtmpt(phone_number, tim, function(err,resp){
					if(err){
						logger.error({"r":"request_otp_v18","m":"POST","msg":"OtpAttempts_updtOtpAtmpt_old","err":err,"p":req.body});
						return sendError(res,"Account not found!","no_account_found",constants.HTTP_STATUS.BAD_REQUEST);
					}
					AccountInfo.updateVersionAndClientSeen(old_account._id, client, version, tim);
					Otp.findOne({
						aid 	: old_account._id,
						cdst 	: Otp.STATUS.NOTUSED
					},function(err,old_otp){
						if(err) { 
							logger.error({"r":"request_otp_v16","m":"POST","msg":"otp_find_server_error","p":req.body});
							return sendError(res,err,"server_error"); 
						}
						if(!old_otp) { 
							return generateAndSendNewOtp(res,old_account); 
						}
						if(prevOtpStillValid(tim, old_otp.gnat)){
							return sendOtp(res, old_otp._id, old_otp.code, old_account);
						}
						Otp.update({
							_id : old_otp._id
						},{
							cdst : Otp.STATUS.INVALID
						},function(err){
							if(err) { 
								logger.error({"r":"request_otp_v16","m":"POST","msg":"otp_update_server_error","p":req.body});
								return sendError(res,err,"server_error"); 
							}
							resetCallMeAttemptForDevice('request_otp_v16',old_account._id+'');
							return generateAndSendNewOtp(res,old_account);	
						});
					});
					if(typeof(unam) !== 'undefined' && typeof(role) !== 'undefined')
						createProfileIfNameSupplied(old_account._id, unam, role, cnam);
					if(typeof(email) !== 'undefined')
						ifNewEmailThenUpdate(old_account._id, email);
				});
			});
		} else {
			var new_account = getNewAccountCreationObj(country_code, phone_number, email, tim, client, version, null);
			new_account.save(function(err,new_account){
				if(err) { 
					logger.error({"r":"request_otp_v16","m":"POST","msg":"new_account_save_error","p":req.body});
					return sendError(res,err,"server_error"); 
				}
				var new_profile_obj = {
					aid : new_account._id,
					nam : unam,
					role: role,
					act : true,
					ppic: null
				};
				if(role === Profile.TYPE.PARENT)
					new_profile_obj.cnam = cnam;
				var new_profile = new Profile(new_profile_obj);
				new_profile.save(function(err, new_profile_created){
					if(err){ 
						logger.error({"r":"request_otp_v16","m":"POST","msg":"new_profile_save_error","p":req.body});
						return sendError(res,err,"server_error"); 
					}
					return generateAndSendNewOtp(res,prepNewAccountObjResp(new_account));
				});
			});
		}
	});
});

/*
 *request otp for version < 1.16.
 */
router.post('/request_otp_v14', function(req, res, next) {
	req.checkBody('c_code',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('p_no',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('vrsn',errorCodes.invalid_parameters[1]).notEmpty().isValidVersion();
	req.checkBody('client',errorCodes.invalid_parameters[1]).notEmpty().isValidClient();

	if(req.validationErrors()){ 
		logger.error({"r":"request_otp_v14","m":"POST","msg":"invalid_parameters","p":req.body});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var checkMobile  = MobileValidator.checkPhoneNumber(req.body.p_no, req.body.c_code);
	if(!checkMobile.status){
		logger.error({"r":"request_otp_v14","m":"POST","msg":"invalid_phone_number","p":req.body}); 
		return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	} 

	var country_code = req.body.c_code;
	var phone_number = req.body.p_no;
	var version      = req.body.vrsn;
	var client       = req.body.client;
	var tim          = new Date().getTime();
	var email;

	Account.findOne({
		m : phone_number
	},{
		_id : 1,
		ccod: 1,
		m   : 1,
		vrfy: 1,
		vrsn: 1,
		cat : 1
	},function(err,old_account){
		if(err){ 
			logger.error({"r":"request_otp_v14","m":"POST","msg":"account_find_server_error","p":req.body});
			return sendError(res,err,"server_error");
		}
		if(old_account){
			BruteForce.check(old_account._id+'', tim, function(err,bruteForce){
				if(err){
					bruteForceAcclogger.error({"r":"request_otp_v14_brute_force_DbError","p":{aid:old_account._id+''},"err":err});
					return sendError(res,err,"server_error"); 
				}
				if(bruteForce){
					bruteForceAcclogger.error({"r":"request_otp_v14_brute_force_found","p":{aid:old_account._id+'',lmt:bruteForce.lmt,tm:bruteForce.tm}});
					return sendError(res,"Too many requests","too_many_requests",constants.HTTP_STATUS.TOO_MANY_REQUESTS);
				}
				OtpAttempts.updtOtpAtmpt(phone_number, tim, function(err,resp){
					if(err){
						logger.error({"r":"request_otp_v14","m":"POST","msg":"OtpAttempts_updtOtpAtmpt_old","err":err,"p":req.body});
						return sendError(res,"Account not found!","no_account_found",constants.HTTP_STATUS.BAD_REQUEST);
					}
					AccountInfo.updateVersionAndClientSeen(old_account._id, client, version, tim);
					Otp.findOne({
						aid 	: old_account._id,
						cdst 	: Otp.STATUS.NOTUSED
					},function(err,old_otp){
						if(err){ 
							logger.error({"r":"request_otp_v14","m":"POST","msg":"otp_find_server_error","p":req.body});
							return sendError(res,err,"server_error"); 
						}
						if(!old_otp) { 
							return generateAndSendNewOtp(res,old_account); 
						}
						if(prevOtpStillValid(tim, old_otp.gnat)){
							return sendOtp(res, old_otp._id, old_otp.code, old_account);
						}
						Otp.update({
							_id : old_otp._id
						},{
							cdst : Otp.STATUS.INVALID
						},function(err){
							if(err){ 
								logger.error({"r":"request_otp_v14","m":"POST","msg":"otp_update_server_error","p":req.body});
								return sendError(res,err,"server_error"); 
							}
							return generateAndSendNewOtp(res,old_account);	
						});
					});
				});
			});
		} else {
			var new_account = getNewAccountCreationObj(country_code, phone_number, email, tim, client, version, null);
			new_account.save(function(err,new_account){
				if(err){ 
					logger.error({"r":"request_otp_v14","m":"POST","msg":"new_account_save_error","p":req.body});
					return sendError(res,err,"server_error"); 
				}
				return generateAndSendNewOtp(res,prepNewAccountObjResp(new_account));
			});
		}
	});
});

/*
	Request Otp for older android clients.
 */
router.post('/request_otp', function(req, res, next) {
	req.checkBody('c_code',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('p_no',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()){ 
		logger.error({"r":"request_otp","m":"POST","msg":"invalid_parameters","p":req.body});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var checkMobile  = MobileValidator.checkPhoneNumber(req.body.p_no, req.body.c_code);
	if(!checkMobile.status){
		logger.error({"r":"request_otp","m":"POST","msg":"invalid_phone_number","p":req.body}); 
		return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	} 

	var country_code = req.body.c_code;
	var phone_number = req.body.p_no;
	var tim          = new Date().getTime();

	Account.findOne({
		m : phone_number
	},{
		_id : 1,
		ccod: 1,
		m   : 1,
		vrfy: 1,
		vrsn: 1,
		cat : 1
	},function(err,old_account){
		if(err){ 
			logger.error({"r":"request_otp","m":"POST","msg":"no_account_found","p":req.body});
			return sendError(res,err,"server_error"); 
		}
		if(old_account){
			BruteForce.check(old_account._id+'', tim, function(err,bruteForce){
				if(err){
					bruteForceAcclogger.error({"r":"request_otp_brute_force_DbError","p":{aid:old_account._id+''},"err":err});
					return sendError(res,err,"server_error"); 
				}
				if(bruteForce){
					bruteForceAcclogger.error({"r":"request_otp_brute_force_found","p":{aid:old_account._id+'',lmt:bruteForce.lmt,tm:bruteForce.tm}});
					return sendError(res,"Too many requests","too_many_requests",constants.HTTP_STATUS.TOO_MANY_REQUESTS);
				}
				OtpAttempts.updtOtpAtmpt(phone_number, tim, function(err,resp){
					if(err){
						logger.error({"r":"request_otp","m":"POST","msg":"OtpAttempts_updtOtpAtmpt_old","err":err,"p":req.body});
						return sendError(res,"Account not found!","no_account_found",constants.HTTP_STATUS.BAD_REQUEST);
					}
					Otp.findOne({
						aid 	: old_account._id,
						cdst 	: Otp.STATUS.NOTUSED
					},function(err,old_otp){
						if(err){ 
							logger.error({"r":"request_otp","m":"POST","msg":"otp_find_server_error","p":req.body});
							return sendError(res,err,"server_error"); 
						}
						if(!old_otp) { 
							return generateAndSendNewOtp(res,old_account); 
						}
						if(prevOtpStillValid(tim, old_otp.gnat)){
							return sendOtp(res, old_otp._id, old_otp.code, old_account);
						}
						Otp.update({
							_id : old_otp._id
						},{
							cdst : Otp.STATUS.INVALID
						},function(err){
							if(err){ 
								logger.error({"r":"request_otp","m":"POST","msg":"otp_update_server_error","p":req.body});
								return sendError(res,err,"server_error"); 
							}
							return generateAndSendNewOtp(res,old_account);	
						});
					});
				});
			});
		} else {
			var new_account = new Account({
				ccod : country_code,
				m 	 : phone_number,
				vrfy : Account.VERIFICATION.NOT_VERIFIED,
			});
			new_account.save(function(err,new_account){
				if(err){ 
					logger.error({"r":"request_otp","m":"POST","msg":"new_account_server_error","p":req.body});
					return sendError(res,err,"server_error"); 
				}
				return generateAndSendNewOtp(res,prepNewAccountObjResp(new_account));
			});
		}
	});
});

/*
 * Resend the OTP for the account id
 */
router.post('/resend_otp', function(req, res, next) {
	req.checkBody('aid',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('tokn',errorCodes.invalid_parameters[1]).notEmpty().validateToken();

	if(req.validationErrors()){
		logger.error({"r":"resend_otp","m":"POST","msg":"invalid_parameters","p":req.body});  
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var aid		   = req.body.aid;
	var tim      = new Date().getTime();

	Account.findOne({
	_id : aid
	},function(err,old_account){
		if(err){ 
			logger.error({"r":"resend_otp","m":"POST","msg":"account_find_server_error","p":req.body}); 
			return sendError(res,err,"server_error"); 
		}
		if(!old_account){
			logger.error({"r":"resend_otp","m":"POST","msg":"no_account_found","p":req.body});  
			return sendError(res,"Account not found","no_account_found",constants.HTTP_STATUS.NOT_FOUND); 
		}
		BruteForce.check(old_account._id+'', tim, function(err,bruteForce){
			if(err){
				bruteForceAcclogger.error({"r":"resend_otp_brute_force_DbError","p":{aid:old_account._id+''},"err":err});
				return sendError(res,err,"server_error"); 
			}
			if(bruteForce){
				bruteForceAcclogger.error({"r":"resend_otp_brute_force_found","p":{aid:old_account._id+'',lmt:bruteForce.lmt,tm:bruteForce.tm}});
				return sendError(res,"Too many requests","too_many_requests",constants.HTTP_STATUS.TOO_MANY_REQUESTS);
			}
			OtpAttempts.checkOtpElgibility(old_account.m, tim, function(err,bruteForceOtp){
				if(err){
					logger.error({"r":"resend_otp","msg":"OtpAttempts_checkOtpElgibility_err","p":{aid:aid},"err":err});
					return sendError(res,"Account not found","no_account_found",constants.HTTP_STATUS.NOT_FOUND); 
				}
				if(bruteForceOtp){
					bruteForceOtplogger.error({"r":"resend_otp","msg":"brute_force_found","p":{phone:old_account.m,lmt:bruteForceOtp.lmt,tm:bruteForceOtp.tm}});
					return sendError(res,"Too many requests","too_many_requests",constants.HTTP_STATUS.TOO_MANY_REQUESTS);
				}
				OtpAttempts.updtOtpAtmpt(old_account.m, tim, function(err,resp){
					if(err){
						logger.error({"r":"resend_otp","m":"POST","msg":"OtpAttempts_updtOtpAtmpt","err":err,"p":req.body});
						return sendError(res,"Account not found!","no_account_found",constants.HTTP_STATUS.BAD_REQUEST);
					}
					Otp.findOne({
						aid 	: old_account._id,
						cdst 	: Otp.STATUS.NOTUSED
					},function(err,old_otp){
						if(err){ 
							logger.error({"r":"resend_otp","m":"POST","msg":"otp_find_server_error","p":req.body}); 
							return sendError(res,err,"server_error"); 
						}
						if(!old_otp){ 
							logger.error({"r":"resend_otp","m":"POST","msg":"no_otp_found","p":req.body}); 
							return generateAndSendNewOtp(res, old_account); 
						}
						if(prevOtpStillValid(tim, old_otp.gnat)){
							return sendOtp(res, old_otp._id, old_otp.code, old_account);
						}
						Otp.update({
							_id : old_otp._id
						},{
							cdst : Otp.STATUS.INVALID
						},function(err){
							if(err) { 
								logger.error({"r":"resend_otp","m":"POST","msg":"otp_update_server_error","p":req.body}); 
								return sendError(res,err,"server_error"); 
							}
							return generateAndSendNewOtp(res,old_account);	
						});
					});
				});
			});
		});
	});
});

/*
 * Resend the OTP for rt expired account unlock window
 */
router.post('/rsd_otp', function(req, res, next) {
	req.checkBody('a_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('d_id',errorCodes.invalid_parameters[1]).notEmpty().isValidDeviceId();
	req.checkBody('cl',errorCodes.invalid_parameters[1]).notEmpty().isValidTokenClient();
	req.checkBody('rt',errorCodes.invalid_parameters[1]).notEmpty().isValidRefreshToken();
	req.checkBody('at',errorCodes.invalid_parameters[1]).notEmpty().isValidAccessToken();
	req.checkBody('k_ey',errorCodes.invalid_parameters[1]).notEmpty().isValidTokenSecretKey();

	if(req.validationErrors()){
		logger.error({"r":"rsd_otp","m":"POST","msg":"invalid_parameters","p":req.body});  
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var aid		   			= req.body.a_id;
	var device_id 		= req.body.d_id;
	var client    		= req.body.cl;
	var refresh_token = req.body.rt;
	var access_token  = req.body.at;
	var secret_key    = req.body.k_ey;
	var tim       		= new Date().getTime();

	if(client != AppClients.ANDROID && client != AppClients.IOS){
		logger.error({"r":"rsd_otp","m":"POST","msg":"client parameter is neither android nor ios","p":req.body}); 
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	Account.findOne({
		_id : aid,
		vrfy: true
	},function(err,old_account){
		if(err){ 
			logger.error({"r":"rsd_otp","m":"POST","msg":"account_rmcquery_error","p":req.body}); 
			return sendError(res,err,"server_error"); 
		}
		if(!old_account){
			logger.error({"r":"rsd_otp","m":"POST","msg":"account_not_verified","p":req.body}); 
			return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}
		BruteForce.check(old_account._id+'', tim, function(err,bruteForce){
			if(err){
				bruteForceAcclogger.error({"r":"rsd_otp_brute_force_DbError","p":{aid:old_account._id+''},"err":err});
				return sendError(res,err,"server_error"); 
			}
			if(bruteForce){
				bruteForceAcclogger.error({"r":"rsd_otp_brute_force_found","p":{aid:old_account._id+'',lmt:bruteForce.lmt,tm:bruteForce.tm}});
				return sendError(res,"Too many requests","too_many_requests",constants.HTTP_STATUS.TOO_MANY_REQUESTS);
			}
			OtpAttempts.checkOtpElgibility(old_account.m, tim, function(err,bruteForceOtp){
				if(err){
					logger.error({"r":"rsd_otp","msg":"OtpAttempts_checkOtpElgibility_err","p":{aid:aid},"err":err});
					return sendError(res,"Account not found","no_account_found",constants.HTTP_STATUS.NOT_FOUND); 
				}
				if(bruteForceOtp){
					bruteForceOtplogger.error({"r":"rsd_otp","msg":"brute_force_found","p":{phone:old_account.m,lmt:bruteForceOtp.lmt,tm:bruteForceOtp.tm}});
					return sendError(res,"Too many requests","too_many_requests",constants.HTTP_STATUS.TOO_MANY_REQUESTS);
				}
				OtpAttempts.updtOtpAtmpt(old_account.m, tim, function(err,resp){
					if(err){
						logger.error({"r":"rsd_otp","m":"POST","msg":"OtpAttempts_updtOtpAtmpt","err":err,"p":req.body});
						return sendError(res,"Account not found!","no_account_found",constants.HTTP_STATUS.BAD_REQUEST);
					}
					if(!old_account.vrfy){
						logger.error({"r":"rsd_otp","m":"POST","msg":"account_not_verified","p":req.body}); 
						return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
					}
					AuthModule.ifRtExpForUser(refresh_token, aid, function(err,resp){
						if(err){
							logger.error({"r":"rsd_otp","m":"POST","msg":"ifRtExpForUser_error","p":req.body,"err":err});
							return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
						}
						AuthModule.ifAtBelongsToUser(access_token, aid, function(err,resp){
							if(err){
								logger.error({"r":"rsd_otp","m":"POST","msg":"ifAtExpForUser_error","p":req.body,"err":err});
								return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
							}
							AccountToken.findOne({
								aid : aid,
								clnt: client,
								act : true,
								revk: false,
								tokn: refresh_token,
								skey: secret_key
							},function(err,account_token){
								if(err){ 
									logger.error({"r":"rsd_otp","m":"POST","msg":"accounttoken_find_server_error","p":req.body}); 
									return sendError(res,err,"server_error"); 
								}
								if(!account_token) { 
									logger.error({"r":"rsd_otp","m":"POST","msg":"invalid_refresh_token","p":req.body}); 
									return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST); 
								}
								Otp.findOne({
									aid 	: old_account._id,
									cdst 	: Otp.STATUS.NOTUSED
								},function(err,old_otp){
									if(err){ 
										logger.error({"r":"rsd_otp","m":"POST","msg":"otp_find_server_error","p":req.body}); 
										return sendError(res,err,"server_error"); 
									}
									if(!old_otp){ 
										logger.error({"r":"rsd_otp","m":"POST","msg":"no_otp_found","p":req.body}); 
										return generateAndSendNewOtp(res, old_account); 
									}
									if(prevOtpStillValid(tim, old_otp.gnat)){
										return sendOtp(res, old_otp._id, old_otp.code, old_account);
									}
									Otp.update({
										_id : old_otp._id
									},{
										cdst : Otp.STATUS.INVALID
									},function(err){
										if(err) { 
											logger.error({"r":"rsd_otp","m":"POST","msg":"otp_update_server_error","p":req.body}); 
											return sendError(res,err,"server_error"); 
										}
										return generateAndSendNewOtp(res,old_account);	
									});
								});
							});
						});
					});
				});
			});
		});
	});
});

//route middleware to verify the jwt token for all unsecured accounts as well covering for android users
router.use(function(req, res, next){
	var token = req.body.tokn || req.query.tokn;
	if(!token){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"token_not_found"});
	  return sendError(res,"Access without token is not authorised","token_not_found",constants.HTTP_STATUS.FORBIDDEN);
	}
	if(token == 5 || token == '5' || token == 'null'){
		var pid = null;//req.body.p_id || req.query.p_id;
		var aid = req.body.a_id || req.query.a_id;
		UnsecureAccounts.check(aid, pid, function(err,isUnsecure){
			if(err){
				console.log(err);
				logger.error({"url":req.originalUrl,"r":"auth","msg":"UnsecureAccounts_check_error","p":{pid:pid,aid:aid,tokn:token}});
				return sendError(res,err,"server_error");
			}
			if(isUnsecure){
				next();
			} else {
				logger.error({"url":req.originalUrl,"r":"auth","msg":"account_not_found_in_unsecured_list","p":{pid:pid,aid:aid,tokn:token}});
				return sendError(res,"Failed to authenticate token.","token_auth_failure",constants.HTTP_STATUS.NOT_FOUND);
			}
		});
	} else {
    jwt.verify(token, configs.JWT_ACCESS_TOKEN_SECRET_KEY, function(err, decoded) {      
      if(err){
      	console.log(err);
      	logger.error({"url":req.originalUrl,"r":"auth","msg":"jwt_verify_error","p":{pid:pid,aid:aid}});
        return sendError(res,"Failed to authenticate token.","token_auth_failure",constants.HTTP_STATUS.TOKEN_EXPIRED);  
      } else {
        next();
      }
    });
	}
});

// Get all profile_id's and group_id's for the given account id
router.get('/all_pids_gids', function(req, res, next) {
	req.checkQuery('a_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkQuery('tokn',errorCodes.invalid_parameters[1]).notEmpty().validateToken();

	if(req.validationErrors()){
		logger.error({"r":"all_pids_gids","m":"POST","msg":"invalid_parameters","p":req.body});  
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var account_id 		= req.query.a_id;

	Profile.find({
		aid : account_id,
		act : true
	},function(err, profiles){
		if(err){ 
			logger.error({"r":"all_pids_gids","m":"POST","msg":"profile_find_server_error","p":req.body}); 
			return sendError(res,err,"server_error"); 
		}
		if(profiles.length == 0){
			logger.error({"r":"all_pids_gids","m":"POST","msg":"profile_not_found","p":req.body}); 
			return sendError(res,"Profile not found","profile_not_found",constants.HTTP_STATUS.NOT_FOUND);
		}
		var user_profiles = [];
		for(var j=0; j < profiles.length; j++) {
			user_profiles[j] = profiles[j]._id;
		}
		GroupMember.find({
			pid : {$in : user_profiles},
			act : true,
			gdel: false
		},function(err,groups){
			if(err){ 
				logger.error({"r":"all_pids_gids","m":"POST","msg":"groupmember_find_server_error","p":req.body}); 
				return sendError(res,err,"server_error"); 
			}
			var user_groups = [];
			for(var i=0; i < groups.length; i++) {
				user_groups[i] = groups[i].gid;
			}
			return sendSuccess(res,{ aid : account_id, pids : user_profiles , gids : user_groups});
		});
	});
});

var generateAndSendNewOtp = function(res,account) {
	var otp = otpGenerator(account.m,constants.OTP_LENGTH);
	var new_otp = new Otp({
		code : otp,
		gnat : (new Date().getTime()),
		smst : Otp.SMS_STATUS.NOT_SENT,
		cdst : Otp.STATUS.NOTUSED,
		aid  : account._id,
	});
	new_otp.save(function(err, new_otp){
		if(err){
			logger.error({"r":"generateAndSendNewOtp","m":"FUNCTION","msg":"new_otp_save_error","p":new_otp}); 
			return sendError(res,err,"server_error");  
		}
		return sendOtp(res, new_otp._id, new_otp.code, account);
	});
};

function sendOtp(res, otp_id, otp_code, account) {
	if(process.env.NODE_ENV == "ci" || process.env.NODE_ENV == "debug" || process.env.NODE_ENV == "dapi") {
		return sendSuccess(res,{
			Account : {
				_id		: account._id,
				m 		: account.m,
				ccod 	: account.ccod,
				vrfy 	: false,
			}
		});
	}
	var otp_obj = new Buffer(JSON.stringify({
    _id : otp_id,
		m 	: account.m,
		aid : account._id+'',
		ccod: account.ccod,
		code: otp_code,
		atmpt : 0
  }));
	RabbitMqPublish.publishOtp("",RabbitMqQueues.OTP_DELIVERY,otp_obj,function(err,resp){
		if(err){
			logger.error({"r":"sendOtp","m":"FUNCTION","msg":"rabbitmq_otp_publish_error","p":otp_obj});
			return sendError(res,err,"server_error");
		}
		if(resp){
			return sendSuccess(res,{
				Account : {
					_id		: account._id,
					m 		: account.m,
					ccod 	: account.ccod,
					vrfy 	: false,
				}
			});
		} else {
			logger.error({"r":"sendOtp","m":"FUNCTION","msg":"rabbitmq_otp_publish_resp_error","p":otp_obj});
			return sendError(res,resp,"server_error");
		}
	});
}

function proceedWithOtpCallMe(res, otp_call_id, account_id, phone, ccode, otp_id, otp_code, atmp, tim){
	var valid_phone_testing = configs.VALID_CALL_TESTING_NUMBERS;
	if(process.env.NODE_ENV == "ci" || process.env.NODE_ENV == "debug" || process.env.NODE_ENV == "dapi"){
		if(valid_phone_testing.indexOf(phone) < 0){
			logger.error({"r":"koo","m":"FUNCTION","msg":"invalid number for Testing","p":{p:phone,c:otp_code,atmp:atmp}});
			return sendError(res,"invalid number","server_error");
		}
	}
	var call_obj = new Buffer(JSON.stringify({
		oclid : otp_call_id,
		m     : phone,
		aid   : account_id,
		cc    : ccode,
		oid   : otp_id,
		code  : otp_code,
		tim   : tim,
		atmp  : atmp,
		atmpt : 0
  }));
	RabbitMqPublish.publishOtp("",RabbitMqQueues.OTP_CALL_ME,call_obj,function(err,resp){
		if(err){
			logger.error({"r":"koo","m":"FUNCTION","msg":"rabbitmq_otp_call_me_publish_error","p":{p:phone,c:otp_code,atmp:atmp}});
			return sendError(res,err,"server_error");
		}
		if(resp){
			return sendSuccess(res,{});
		} else {
			logger.error({"r":"koo","m":"FUNCTION","msg":"rabbitmq_otp__call_me_publish_resp_error","p":{p:phone,c:otp_code,atmp:atmp}});
			return sendError(res,resp,"server_error");
		}
	});
}

function resetGroupMuteOnVerifyOtp(route_name, gids, aid){
	if(gids.length == 0){
		return;
	}
	GroupMember.update({
		aid  : aid,
		act  : true,
		gdel : false
	},{
		muttp: "",
		muttm: 0
	},{
		multi : true
	},function(err,resp){
		if(err){
			console.trace(err);
			logger.error({"r":route_name,"m":"resetGroupMuteOnVerifyOtp","msg":"GroupMember_update","p":{aid:aid,gids:gids},"err":err});
		}
		AccountDataInfoCache.removeMultiGroupAccountDatas(gids, function(err,resp){
			if(err){
				logger.error({"r":route_name,"m":"resetGroupMuteOnVerifyOtp","msg":"AccountDataInfoCache_removeMultiGroupAccountDatas","p":{aid:aid,gids:gids},"err":err});
			}
			return;
		});
	});
}

function resetCallMeAttemptForDevice(route_name, aid){
	OtpCall.update({
		aid : aid,
		act  : true,
		atmp : { $gte : configs.CALL_ME_MAX_LIMIT_PER_DEVICE }
	},{
		atmp : 0
	},function(err,resp){
		if(err){
			console.trace(err);
			logger.error({"r":route_name,"m":"FUNCTION","msg":"resetCallMeAttemptForDevice_error","p":{r:route_name,aid:aid}});
		}
	});
}

function resetOtpAttemptForAcc(route_name, phone, aid){
	OtpAttempts.resetOtpAtmpt(phone,function(err,resp){
		if(err){
			console.trace(err);
			logger.error({"r":route_name,"m":"FUNCTION","msg":"resetCallMeAttemptForDevice_error","p":{r:route_name,aid:aid}});
		}
		// logger.info({"r":route_name,"m":"FUNCTION","msg":"resetOtpAttemptForAcc_succes","p":{r:route_name,aid:aid}});
	});
}

function prevOtpStillValid(request_time, old_otp_time) {
	var expiry_time = configs.OTP_VERIFICATION_EXPIRY_TIME;
	return ((request_time - old_otp_time) < expiry_time ) ? true : false;
}

function dropUserQueues(callee, pids, aid, cb){
	var total = 3;
	var done  = 0;
	UserQueue.dropPidsQ(callee, pids, function(err, resp){
		if(err){
			return cb(err,null);
		}
		done++;
		if(done == total){
			return cb(null,true);
		}
	});
	UserQueue.dropAidQ(callee, aid, function(err, resp){
		if(err){
			return cb(err,null);
		}
		done++;
		if(done == total){
			return cb(null,true);
		}
	});
	UserQueue.dropPidsSeenCountQ(callee, pids, function(err, resp){
		if(err){
			return cb(err,null);
		}
		done++;
		if(done == total){
			return cb(null,true);
		}
	});
}

function manageRefreshToken(aid, device_id, client, client_vrsn, version, cb) {
	AccountToken.update({
		aid : aid,
		udid: device_id,
		clnt: client,
		act : true
	},{
		act : false
	},{
		multi : true
	},function(err, token_update){
		if(err) { 
			console.trace(err); 
			return cb(true, null); 
		}
		var new_token  = AuthModule.getRT({id: aid,cl: client}, client);
		var new_secret = AuthModule.getSecret(device_id, client);
		AccountToken.update({
			aid : aid,
			udid: device_id,
			clnt: client,
			act : true
		},{
			$setOnInsert : {
				aid : aid,
				udid: device_id,
				clnt: client,
				tokn: new_token,
				skey: new_secret,
				clv : client_vrsn,
				vrsn: version,
				act : true,
				revk: false
			}
		},{
			upsert : true,
			setDefaultsOnInsert: true
		},function(err, new_account_token){
			if(err) { 
				console.trace(err); 
				return cb(true, null); 
			}
			if(!new_account_token) { 
				return cb(true, null); 
			}
			var access_token = AuthModule.getAT({id: aid,cl: client}, client);
			return cb(null, {rt : new_token, secret : new_secret, at : access_token});
		});
	});
}

function ifNewEmailThenUpdate(aid, email) {
	Account.update({
		_id : aid,
		e   : { $ne : email }
	},{
		$push : { e : email }
	},function(err,new_email_updated){
		if(err){  
			console.trace(err); 
			return; 
		}
		if(new_email_updated){ 
			console.log('New email : '+email+', updated for Accont ID : '+aid+' , is successfull'); 
		}	else { 
			console.log('New email : '+email+', updation for Accont ID : '+aid+', is Successfull'); 
		}
	})
}

function createProfileIfNameSupplied(aid, name, role, cnam){
	Profile.count({
		aid : aid
	},function(err,profile_counts){
		if(err) { console.trace('Error in fetching profile count for Account Id : '+aid); return; }
		if(profile_counts == 0){
			var new_profile_obj = {
				aid : aid,
				nam : name,
				role: role,
				act : true
			};
			if(role === Profile.TYPE.PARENT)
				new_profile_obj.cnam = cnam;
			var new_profile = new Profile(new_profile_obj);
			new_profile.save(function(err, new_profile_created){
				if(err) { console.trace(); return; }
				if(new_profile_created){
					console.log('For Account Id : '+aid+' , a new profile created , profile id : '+new_profile_created._id);
				} else {
					console.log('For Account Id : '+aid+' , new Profile creation failed');
				}
			});
		} else {
			console.log('Account Id : '+aid+' , has existing : '+profile_counts+' Profiles');
		}
	});
}

function getNewAccountCreationObj(country_code, phone_number, email, lsnm, client, version, rf_link){
	var new_account_obj = {
		ccod : country_code,
		m 	 : phone_number,
		vrfy : Account.VERIFICATION.NOT_VERIFIED
	};
	switch (client) {
		case AppClients.MOBILE:
				new_account_obj.vrsn = version;
				new_account_obj.lsnm = lsnm;
			break;
		case AppClients.ANDROID:
				new_account_obj.vrsn = version;
				new_account_obj.lsnm = lsnm;
			break;
		case AppClients.IOS:
				new_account_obj.iosv = version;
				new_account_obj.lsni = lsnm;
			break;
		case AppClients.WINDOWS:
				new_account_obj.wndv = version;
				new_account_obj.lsnwn = lsnm;
			break;
		case AppClients.WEB:
				new_account_obj.webv = version;
				new_account_obj.lsnw = lsnm;
			break;
	}
	if(typeof(email) !== 'undefined')
		new_account_obj.e = email;
	if(rf_link){
		new_account_obj.rf_link = rf_link;
	}
	return new Account(new_account_obj);
}

function prepNewAccountObjResp(new_account){
	if(typeof(new_account.email) !== 'undefined')	
		delete new_account['email'];
	if(typeof(new_account.lsnm) !== 'undefined')	
		delete new_account['lsnm'];
	if(typeof(new_account.lsnw) !== 'undefined')	
		delete new_account['lsnw'];
	if(typeof(new_account.lsni) !== 'undefined')	
		delete new_account['lsni'];
	if(typeof(new_account.lsnwn) !== 'undefined')	
		delete new_account['lsnwn'];
	if(typeof(new_account.iosv) !== 'undefined')	
		delete new_account['iosv'];
	if(typeof(new_account.webv) !== 'undefined')	
		delete new_account['webv'];
	if(typeof(new_account.wndv) !== 'undefined')	
		delete new_account['wndv'];
	return new_account;
}

// function updateOtpAttemptResp(route_name, phone_number, tim, utype){
// 	switch(utype){

// 		case 'total':
// 			OtpAttempts.updtOtpAtmpt(phone_number, tim, function(err,resp){
// 				if(err){
// 					logger.error({"r":route_name,"m":"POST","msg":"OtpAttempts_updtOtpAtmpt","err":err,"p":req.body});
// 					return sendError(res,"Account not found!","no_account_found",constants.HTTP_STATUS.BAD_REQUEST);
// 				}
// 			});
// 		break;

// 		case 'success':
// 			OtpAttempts.updtSucOtpAtmpt(phone_number, tim, function(err,resp){
// 				if(err){
// 					logger.error({"r":route_name,"m":"POST","msg":"OtpAttempts_updtSucOtpAtmpt","err":err,"p":req.body});
// 					return sendError(res,"Account not found!","no_account_found",constants.HTTP_STATUS.BAD_REQUEST);
// 				}
// 			});
// 		break;

// 		case 'error':
// 			OtpAttempts.updtErOtpAtmpt(phone_number, tim, function(err,resp){
// 				if(err){
// 					logger.error({"r":route_name,"m":"POST","msg":"OtpAttempts_updtErOtpAtmpt","err":err,"p":req.body});
// 					return sendError(res,"Account not found!","no_account_found",constants.HTTP_STATUS.BAD_REQUEST);
// 				}
// 			});
// 		break;
// 	}
	
// }

module.exports = router;
