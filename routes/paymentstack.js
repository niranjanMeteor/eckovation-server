var express 								= require('express');
var mongoose 								= require('mongoose');
var jwt    									= require('jsonwebtoken');

var configs									= require('../utility/configs.js');
var errorCodes 							= require('../utility/errors.js');
var constants								= require('../utility/constants.js');
var helpers									= require('../utility/helpers.js');
var AppClients							= require('../utility/app_clients.js');
var log4jsLogger  					= require('../loggers/log4js_module.js');
var log4jsConfigs  					= require('../loggers/log4js_configs.js');
var MailClient          		= require('../utility/mail/mail_client.js');

require('../models/accounts.js');
require('../models/profiles.js');
require('../models/accounttokens.js');

var Account 								= mongoose.model('Account');
var Profile 								= mongoose.model('Profile');
var AccountToken  					= mongoose.model('AccountToken');

var sendError 							= helpers.sendError;
var sendSuccess 						= helpers.sendSuccess;
var router 									= express.Router();
var logger        					= log4jsLogger.getLogger(log4jsConfigs.CATEGORY_ROUTES_ACCOUNT);

var ValidMobileDevicesType 	= [AccountToken.CLIENT.ANDROID,AccountToken.CLIENT.ANDROID,,AccountToken.CLIENT.IOS];

router.post("i_new_trxn",function(req, res, next){
	
});

router.post("i_trxn_refund",function(req, res, next){
	
});

module.exports = router;