var express 								= require('express');
var mongoose 								= require('mongoose');
var jwt    									= require('jsonwebtoken');
var redis           				= require('redis');

var configs									= require('../utility/configs.js');
var PaytmConfigs					  = require('../payment/paytm_configs.js');
var RazorpayConfigs					= require('../payment/razorpay_configs.js');
var TrxnOrder					      = require('../payment/trxnorder.js');
var errorCodes 							= require('../utility/errors.js');
var constants								= require('../utility/constants.js');
var helpers									= require('../utility/helpers.js');
var AppClients							= require('../utility/app_clients.js');
var log4jsLogger  					= require('../loggers/log4js_module.js');
var log4jsConfigs  					= require('../loggers/log4js_configs.js');
var MailClient          		= require('../utility/mail/mail_client.js');
var UserInfoCache           = require('../utility/user_info_cache.js');
var GroupInfoCache          = require('../utility/group_info_cache.js');
var BruteForce              = require('../utility/auth/brute_force.js');
var PluginToken             = require('../security/plugintokens/auth_tokens.js');
var RabbitMqPublish         = require('../rabbitmq/publisher/publisher.js');
var RabbitMqQueues          = require('../rabbitmq/queues.js');

var redis_client            = redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
  console.log("Error " + err);
});

require('../models/accounts.js');
require('../models/profiles.js');
require('../models/groups.js');
require('../models/groupmembers.js');
require('../models/plugins.js');
require('../models/groupplugins.js');
require('../models/packages/grouppluginpackages.js');
require('../models/packages/promotioncodes.js');
require('../models/payment/transactions.js');
require('../models/payment/transactionlogs.js');
require('../models/subscriptions/usersubscriptions.js');
require('../models/pluginidentifiers.js');
require('../models/payment/gatewayresponse.js');
var Account 								= mongoose.model('Account');
var Profile 								= mongoose.model('Profile');
var Group 								  = mongoose.model('Group');
var GroupMember 					  = mongoose.model('GroupMember');
var AppPlugin       			  = mongoose.model('AppPlugin');
var GroupPlugin 					  = mongoose.model('GroupPlugin');
var GroupPluginPackage 			= mongoose.model('GroupPluginPackage');
var PromotionCode 					= mongoose.model('PromotionCode');
var Transaction 					  = mongoose.model('Transaction');
var UserSubscription 			  = mongoose.model('UserSubscription');
var PluginIdentifier        = mongoose.model('PluginIdentifier');
var TransactionLog 					= mongoose.model('TransactionLog');
var GatewayResponse 				= mongoose.model('GatewayResponse');

var rmc                     = require("./../utility/redis_mongo_cache.js")(redis_client);

var sendError 							= helpers.sendError;
var sendSuccess 						= helpers.sendSuccess;
var router 									= express.Router();
var logger        			    = log4jsLogger.getLogger(log4jsConfigs.CATEGORY_ROUTES_TRXN);
var RAZORPAY_MODES          = [Transaction.PAYMENT_MODE.CREDIT_CARD,Transaction.PAYMENT_MODE.DEBIT_CARD,Transaction.PAYMENT_MODE.OTHER_WALLET,Transaction.PAYMENT_MODE.NET_BANKING];

//route middleware to verify the jwt token for all users
router.use(function(req, res, next){
	var token = req.headers['x-access-token'];
	if(!token){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"token_not_found","p":req.body});
	  return sendError(res,"Access without token is not authorised","token_not_found",constants.HTTP_STATUS.FORBIDDEN);
	}
  jwt.verify(token, configs.JWT_ACCESS_TOKEN_PRIVATE_KEY, function(err, decoded) {      
    if(err){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"jwt_verify_error","p":req.body,"er":err});
      return sendError(res,"Failed to authenticate token.","token_auth_failure",constants.HTTP_STATUS.TOKEN_EXPIRED);  
    }
  	req.decoded = decoded;
    next();
  });
});

router.post("/initiate_trxn",function(req, res, next){
	var tim          = new Date().getTime();
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('pl_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('pkg_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('p_mode',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('tamt',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('famt',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('actv',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('cpn_code',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('email',errorCodes.invalid_parameters[1]).optional().isValidEmail();
 
 	if(req.validationErrors()){ 
		logger.error({"r":"initiate_trxn","msg":"invalid_parameters","p":req.body,"er":req.validationErrors()});
		return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var profile_id     = req.body.p_id;
	var group_id       = req.body.g_id;
	var plugin_id      = req.body.pl_id;
	var package_id     = req.body.pkg_id;
	var payment_mode   = req.body.p_mode.trim();
	var pkg_amount     = req.body.tamt.trim();
	var quoted_amount  = req.body.famt.trim();
	var coupon_code    = (req.body.cpn_code) ? req.body.cpn_code.trim() : null;
	var email          = (req.body.email) ? req.body.email.trim() : null;
	var activity       = (req.body.actv) ? Transaction.ACTIVITY.GROUP_JOIN : Transaction.ACTIVITY.PLUGIN_PACKAGE;
	var isAdmin        = false;
	var client         = (!req.decoded.cl) ? AppClients.ANDROID : (req.decoded.cl);

	if(!isValidPaymentMode(payment_mode)){
		logger.error({"r":"initiate_trxn","msg":"invalid_payment_mode","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	GroupMember.findOne({
    gid : group_id,
    pid : profile_id,
    act : true,
    gdel: false
  },function(err, groupmember){
		if(err) { 
			logger.error({"r":"initiate_trxn","msg":"getMongoGrpMem_DbError","p":req.body,"er":err});
			return sendError(res,"server_error","server_error"); 
		}
		if(!groupmember){
			logger.error({"r":"initiate_trxn","msg":"not_group_member","p":req.body,"pid":profile_id,"gid":group_id});
			return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}
		if(groupmember.aid != req.decoded.id){
			logger.error({"r":"initiate_trxn","msg":"aid_not_matching_with_token_id","p":req.body});
			return sendError(res,"not_authorised","not_authorised",constants.HTTP_STATUS.UNAUTHORIZED);
		}
		if(groupmember.type == GroupMember.TYPE.ADMIN){
			isAdmin = true;
			quoted_amount = '1.00';
		}
		GroupPlugin.findOne({
			gid       : group_id,
			act       : true,
			plgnid    : plugin_id,
			status    : GroupPlugin.STATUS.ACTIVE
		},function(err,groupplugin){
			if(err){
				logger.error({"r":"initiate_trxn","msg":"GroupPlugin_DbError","p":req.body,"er":err});
				return sendError(res,"server_error","server_error");
			}
			if(!groupplugin){
				logger.error({"r":"initiate_trxn","msg":"group_plugin_not_found","p":req.body});
				return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
			}
			GroupPluginPackage.findOne({
				_id       : package_id,
				gid       : group_id,
				plugin_id : plugin_id,
				act       : true
			},function(err,packageFound){
				if(err) { 
					logger.error({"r":"initiate_trxn","msg":"GroupPluginPackage_find_DbError","p":req.body,"er":err});
					return sendError(res,"server_error","server_error"); 
				}
				if(!packageFound){
					logger.error({"r":"initiate_trxn","msg":"package_not_found","p":req.body});
					return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
				}
				if(packageFound.package_price != pkg_amount){
					logger.error({"r":"initiate_trxn","msg":"package_amount_incorrect_from_client","p":req.body});
					return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
				}
				UserSubscription.findOne({
					gid       : group_id,
					pid       : profile_id,
					plugin_id : plugin_id,
					aid       : groupmember.aid,
					pkg_id    : package_id,
					act       : true
				},function(err,user_subscription){
					if(err) { 
						logger.error({"r":"initiate_trxn","msg":"UserSubscription_findOne_Error","p":req.body,"er":err});
						return sendError(res,"server_error","server_error"); 
					}
					if(user_subscription && (tim > user_subscription.stim && tim < user_subscription.etim)){
						logger.info({"r":"initiate_trxn","msg":"user_already_subscribed","p":req.body});
						return sendError(res,"Already Subscribed For This Package","user_already_subscribed",constants.HTTP_STATUS.USER_ALREADY_SUBSCRIBED);
					}
					Account.findOne({
						_id : groupmember.aid,
						vrfy: true
					},function(err,account){
						if(err){
							logger.error({"r":"initiate_trxn","msg":"rmc_queryById_account_error","p":req.body,"er":err});
							return sendError(res,"server_error","server_error");
						}
						if(!account){
							logger.error({"r":"initiate_trxn","msg":"account_not_found","p":req.body});
							return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
						}
						if(!email){
							email = (account.e[0]) ? account.e[0] : "noemailecko@gmail.com";
						}
						checkUserPluginIdentifier(group_id, plugin_id, profile_id, function(err, identifier){
							if(err){
								logger.error({"r":"initiate_trxn","msg":"checkUserPluginIdentifier_Error","p":req.body,"er":err});
								return sendError(res,"server_error","server_error");
							}
							createBrandNewOrder(plugin_id,
							 										account._id,
							 										profile_id,
							 										group_id,
							 										groupmember._id,
							 										identifier,
							 										packageFound,
							 										coupon_code,
							 										pkg_amount,
							 										quoted_amount,
							 										payment_mode,
							 										tim,
							 										isAdmin,
							 										activity,
							 										Transaction.GATEWAY.PAYTM,
							 										client, function(err,orderAndAmount){
							 	if(err){
							 		logger.error({"r":"initiate_trxn","msg":"createBrandNewOrder_error","p":req.body,"er":err});
									return sendError(res,"server_error","server_error");
							 	}
							 	email = orderAndAmount.order_id+"@eckovation.com";
							 	if(orderAndAmount.amount == 0 || orderAndAmount.amount == '0.00'){
							 		logger.warn({"r":"initiate_trxn","msg":"trxn amount has come out to be 0","p":req.body});
							 		savePaymentSkipResponse(orderAndAmount.order_id, function(err,resp){
							 			if(err){
							 				logger.error({"r":"initiate_trxn","msg":"savePaymentSkipResponse_error","p":req.body,"er":err,"orderAndAmount":orderAndAmount});
											return sendError(res,"server_error","server_error");
							 			}
								 		var payment_notify_object = new Buffer(JSON.stringify({
									    trxn_id : orderAndAmount.order_id,
									    atmpt   : 1,
									    gtwy    : Transaction.GATEWAY.PAYTM,
									    cl      : client
									  }));
										RabbitMqPublish.publish("",RabbitMqQueues.PAYMENT_NOTIFY,payment_notify_object);
										logger.info({"r":"initiate_trxn","msg":"trxn success with payment skipped","p":req.body,"orderAndAmount":orderAndAmount});
										return sendError(res,"payment_trxn_skip","payment_trxn_skip",constants.HTTP_STATUS.PAYMENT_TRXN_SKIP);
							 		});
							 	} else {
							 		email = orderAndAmount.order_id+"@eckovation.com";
								 	var result = {
										CHANNEL_ID 				: PaytmConfigs.CHANNEL_ID,
										CUST_ID 					: groupmember._id,
										EMAIL 						: email,
										INDUSTRY_TYPE_ID 	: PaytmConfigs.INDUSTRY_TYPE_ID,
										MID 							: PaytmConfigs.MID,
										MOBILE_NO 				: parseInt(account.m),
										ORDER_ID 					: orderAndAmount.order_id,
										THEME 					  : PaytmConfigs.THEME,
										TXN_AMOUNT 				: orderAndAmount.amount.toFixed(2),
										WEBSITE 					: PaytmConfigs.WEBSITE
									};
									logger.info({"r":"initiate_trxn","msg":"trxn success","p":req.body,"orderAndAmount":orderAndAmount});
									var payment_status_notify_object = new Buffer(JSON.stringify({
								    trxn_id : orderAndAmount.order_id,
								    cron    : 50,
								    gtwy    : Transaction.GATEWAY.PAYTM,
								    cl      : client
								  }));
									RabbitMqPublish.publish("",RabbitMqQueues.PAYMENT_NOTIFY,payment_status_notify_object);
									return sendSuccess(res,{params:result});
								}
							});
						});
					});
				});
			});
		});
	});
});

router.post("/i_trxn_v2",function(req, res, next){
	var tim          = new Date().getTime();
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('pl_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('pkg_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('p_mode',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('tamt',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('famt',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('actv',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('cpn_code',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('email',errorCodes.invalid_parameters[1]).optional().isValidEmail();
 
 	if(req.validationErrors()){ 
		logger.error({"r":"initiate_trxn","msg":"invalid_parameters","p":req.body,"er":req.validationErrors()});
		return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var profile_id     = req.body.p_id;
	var group_id       = req.body.g_id;
	var plugin_id      = req.body.pl_id;
	var package_id     = req.body.pkg_id;
	var payment_mode   = req.body.p_mode.trim();
	var pkg_amount     = req.body.tamt.trim();
	var quoted_amount  = req.body.famt.trim();
	var coupon_code    = (req.body.cpn_code) ? req.body.cpn_code.trim() : null;
	var email          = (req.body.email) ? req.body.email.trim() : null;
	var activity       = (req.body.actv) ? Transaction.ACTIVITY.GROUP_JOIN : Transaction.ACTIVITY.PLUGIN_PACKAGE;
	var isAdmin        = false;
	var client         = (!req.decoded.cl) ? AppClients.ANDROID : (req.decoded.cl);

	if(!isValidPaymentMode(payment_mode)){
		logger.error({"r":"i_trxn_v2","msg":"invalid_payment_mode","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	GroupMember.findOne({
    gid : group_id,
    pid : profile_id,
    act : true,
    gdel: false
  },function(err, groupmember){
		if(err) { 
			logger.error({"r":"i_trxn_v2","msg":"getMongoGrpMem_DbError","p":req.body,"er":err});
			return sendError(res,"server_error","server_error"); 
		}
		if(!groupmember){
			logger.error({"r":"i_trxn_v2","msg":"not_group_member","p":req.body,"pid":profile_id,"gid":group_id});
			return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}
		if(groupmember.aid != req.decoded.id){
			logger.error({"r":"i_trxn_v2","msg":"aid_not_matching_with_token_id","p":req.body});
			return sendError(res,"not_authorised","not_authorised",constants.HTTP_STATUS.UNAUTHORIZED);
		}
		if(groupmember.type == GroupMember.TYPE.ADMIN){
			isAdmin = true;
			quoted_amount = '0.00';
		}
		GroupPlugin.findOne({
			gid       : group_id,
			act       : true,
			plgnid    : plugin_id,
			status    : GroupPlugin.STATUS.ACTIVE
		},function(err,groupplugin){
			if(err){
				logger.error({"r":"i_trxn_v2","msg":"GroupPlugin_DbError","p":req.body,"er":err});
				return sendError(res,"server_error","server_error");
			}
			if(!groupplugin){
				logger.error({"r":"i_trxn_v2","msg":"group_plugin_not_found","p":req.body});
				return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
			}
			GroupPluginPackage.findOne({
				_id       : package_id,
				gid       : group_id,
				plugin_id : plugin_id,
				act       : true
			},function(err,packageFound){
				if(err) { 
					logger.error({"r":"i_trxn_v2","msg":"GroupPluginPackage_find_DbError","p":req.body,"er":err});
					return sendError(res,"server_error","server_error"); 
				}
				if(!packageFound){
					logger.error({"r":"i_trxn_v2","msg":"package_not_found","p":req.body});
					return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
				}
				if(packageFound.package_price != pkg_amount){
					logger.error({"r":"i_trxn_v2","msg":"package_amount_incorrect_from_client","p":req.body});
					return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
				}
				UserSubscription.findOne({
					gid       : group_id,
					pid       : profile_id,
					plugin_id : plugin_id,
					aid       : groupmember.aid,
					pkg_id    : package_id,
					act       : true
				},function(err,user_subscription){
					if(err) { 
						logger.error({"r":"i_trxn_v2","msg":"UserSubscription_findOne_Error","p":req.body,"er":err});
						return sendError(res,"server_error","server_error"); 
					}
					if(user_subscription && (tim > user_subscription.stim && tim < user_subscription.etim)){
						logger.info({"r":"i_trxn_v2","msg":"user_already_subscribed","p":req.body});
						return sendError(res,"Already Subscribed For This Package","user_already_subscribed",constants.HTTP_STATUS.USER_ALREADY_SUBSCRIBED);
					}
					Account.findOne({
						_id : groupmember.aid,
						vrfy: true
					},function(err,account){
						if(err){
							logger.error({"r":"i_trxn_v2","msg":"rmc_queryById_account_error","p":req.body,"er":err});
							return sendError(res,"server_error","server_error");
						}
						if(!account){
							logger.error({"r":"i_trxn_v2","msg":"account_not_found","p":req.body});
							return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
						}
						if(!email){
							email = (account.e[0]) ? account.e[0] : "noemailecko@gmail.com";
						}
						checkUserPluginIdentifier(group_id, plugin_id, profile_id, function(err, identifier){
							if(err){
								logger.error({"r":"i_trxn_v2","msg":"checkUserPluginIdentifier_Error","p":req.body,"er":err});
								return sendError(res,"server_error","server_error");
							}
							createBrandNewOrder(plugin_id,
							 										account._id,
							 										profile_id,
							 										group_id,
							 										groupmember._id,
							 										identifier,
							 										packageFound,
							 										coupon_code,
							 										pkg_amount,
							 										quoted_amount,
							 										payment_mode,
							 										tim,
							 										isAdmin,
							 										activity,
							 										Transaction.GATEWAY.PAYTM,
							 										client, function(err,orderAndAmount){
							 	if(err){
							 		logger.error({"r":"i_trxn_v2","msg":"createBrandNewOrder_error","p":req.body,"er":err});
									return sendError(res,"server_error","server_error");
							 	}
							 	email = orderAndAmount.order_id+"@eckovation.com";
							 	if(orderAndAmount.amount == 0 || orderAndAmount.amount == '0.00'){
							 		logger.warn({"r":"i_trxn_v2","msg":"trxn amount has come out to be 0","p":req.body});
							 		savePaymentSkipResponse(orderAndAmount.order_id, function(err,resp){
							 			if(err){
							 				logger.error({"r":"i_trxn_v2","msg":"savePaymentSkipResponse_error","p":req.body,"er":err,"orderAndAmount":orderAndAmount});
											return sendError(res,"server_error","server_error");
							 			}
								 		var payment_notify_object = new Buffer(JSON.stringify({
									    trxn_id : orderAndAmount.order_id,
									    atmpt   : 1,
									    gtwy    : Transaction.GATEWAY.PAYTM,
									    cl      : client
									  }));
										RabbitMqPublish.publish("",RabbitMqQueues.PAYMENT_NOTIFY,payment_notify_object);
										logger.info({"r":"i_trxn_v2","msg":"trxn success with payment skipped","p":req.body,"orderAndAmount":orderAndAmount});
										return sendError(res,"payment_trxn_skip","payment_trxn_skip",constants.HTTP_STATUS.PAYMENT_TRXN_SKIP);
							 		});
							 	} else {
							 		email = orderAndAmount.order_id+"@eckovation.com";
								 	var result = {
										CHANNEL_ID 				: PaytmConfigs.CHANNEL_ID,
										CUST_ID 					: groupmember._id,
										EMAIL 						: email,
										INDUSTRY_TYPE_ID 	: PaytmConfigs.INDUSTRY_TYPE_ID,
										MID 							: PaytmConfigs.MID,
										MOBILE_NO 				: parseInt(account.m),
										ORDER_ID 					: orderAndAmount.order_id,
										THEME 					  : PaytmConfigs.THEME,
										TXN_AMOUNT 				: orderAndAmount.amount.toFixed(2),
										WEBSITE 					: PaytmConfigs.WEBSITE
									};
									logger.info({"r":"i_trxn_v2","msg":"trxn success","p":req.body,"orderAndAmount":orderAndAmount});
									var payment_status_notify_object = new Buffer(JSON.stringify({
								    trxn_id : orderAndAmount.order_id,
								    cron    : 50,
								    gtwy    : Transaction.GATEWAY.PAYTM,
								    cl      : client
								  }));
									RabbitMqPublish.publish("",RabbitMqQueues.PAYMENT_NOTIFY,payment_status_notify_object);
									return sendSuccess(res,{params:result});
								}
							});
						});
					});
				});
			});
		});
	});
});

router.post("/i_trxn_v3",function(req, res, next){
	var tim          = new Date().getTime();
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('pl_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('pkg_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('p_mode',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('tamt',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('famt',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('actv',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('cpn_code',errorCodes.invalid_parameters[1]).optional();
	req.checkBody('email',errorCodes.invalid_parameters[1]).optional().isValidEmail();
 
 	if(req.validationErrors()){ 
		logger.error({"r":"i_trxn_v3","msg":"invalid_parameters","p":req.body,"er":req.validationErrors()});
		return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var profile_id     = req.body.p_id;
	var group_id       = req.body.g_id;
	var plugin_id      = req.body.pl_id;
	var package_id     = req.body.pkg_id;
	var payment_mode   = req.body.p_mode.trim();
	var pkg_amount     = req.body.tamt.trim();
	var quoted_amount  = req.body.famt.trim();
	var coupon_code    = (req.body.cpn_code) ? req.body.cpn_code.trim() : null;
	var email          = (req.body.email) ? req.body.email.trim() : null;
	var activity       = (req.body.actv) ? Transaction.ACTIVITY.GROUP_JOIN : Transaction.ACTIVITY.PLUGIN_PACKAGE;
	var isAdmin        = false;
	var gtwy           = Transaction.GATEWAY.PAYTM;
	var client         = (!req.decoded.cl) ? AppClients.ANDROID : (req.decoded.cl);

	if(!isValidPaymentMode(payment_mode)){
		logger.error({"r":"i_trxn_v3","msg":"invalid_payment_mode","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}
	if(RAZORPAY_MODES.indexOf(payment_mode) >= 0){
		gtwy = Transaction.GATEWAY.RAZORPAY;
	}
	GroupMember.findOne({
    gid : group_id,
    pid : profile_id,
    act : true,
    gdel: false
  },function(err, groupmember){
		if(err) { 
			logger.error({"r":"i_trxn_v3","msg":"getMongoGrpMem_DbError","p":req.body,"er":err});
			return sendError(res,"server_error","server_error"); 
		}
		if(!groupmember){
			logger.error({"r":"i_trxn_v3","msg":"not_group_member","p":req.body,"pid":profile_id,"gid":group_id});
			return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}
		if(groupmember.aid != req.decoded.id){
			logger.error({"r":"i_trxn_v3","msg":"aid_not_matching_with_token_id","p":req.body});
			return sendError(res,"not_authorised","not_authorised",constants.HTTP_STATUS.UNAUTHORIZED);
		}
		if(groupmember.type == GroupMember.TYPE.ADMIN){
			isAdmin = true;
			quoted_amount = '0.00';
		}
		GroupPlugin.findOne({
			gid       : group_id,
			act       : true,
			plgnid    : plugin_id,
			status    : GroupPlugin.STATUS.ACTIVE
		},function(err,groupplugin){
			if(err){
				logger.error({"r":"i_trxn_v3","msg":"GroupPlugin_DbError","p":req.body,"er":err});
				return sendError(res,"server_error","server_error");
			}
			if(!groupplugin){
				logger.error({"r":"i_trxn_v3","msg":"group_plugin_not_found","p":req.body});
				return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
			}
			GroupPluginPackage.findOne({
				_id       : package_id,
				gid       : group_id,
				plugin_id : plugin_id,
				act       : true
			},function(err,packageFound){
				if(err) { 
					logger.error({"r":"i_trxn_v3","msg":"GroupPluginPackage_find_DbError","p":req.body,"er":err});
					return sendError(res,"server_error","server_error"); 
				}
				if(!packageFound){
					logger.error({"r":"i_trxn_v3","msg":"package_not_found","p":req.body});
					return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
				}
				if(packageFound.package_price != pkg_amount){
					logger.error({"r":"i_trxn_v3","msg":"package_amount_incorrect_from_client","p":req.body});
					return sendError(res,"Group Package Price Mismatch","Group_Package_Price_Mismatch",constants.HTTP_STATUS.BAD_REQUEST);
				}
				UserSubscription.findOne({
					gid       : group_id,
					pid       : profile_id,
					plugin_id : plugin_id,
					aid       : groupmember.aid,
					pkg_id    : package_id,
					act       : true
				},function(err,user_subscription){
					if(err) { 
						logger.error({"r":"i_trxn_v3","msg":"UserSubscription_findOne_Error","p":req.body,"er":err});
						return sendError(res,"server_error","server_error"); 
					}
					if(user_subscription && (tim > user_subscription.stim && tim < user_subscription.etim)){
						logger.info({"r":"i_trxn_v3","msg":"user_already_subscribed","p":req.body});
						return sendError(res,"Already Subscribed For This Package","user_already_subscribed",constants.HTTP_STATUS.USER_ALREADY_SUBSCRIBED);
					}
					Account.findOne({
						_id : groupmember.aid,
						vrfy: true
					},function(err,account){
						if(err){
							logger.error({"r":"i_trxn_v3","msg":"rmc_queryById_account_error","p":req.body,"er":err});
							return sendError(res,"server_error","server_error");
						}
						if(!account){
							logger.error({"r":"i_trxn_v3","msg":"account_not_found","p":req.body});
							return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
						}
						if(!email){
							email = (account.e[0]) ? account.e[0] : "noemailecko@gmail.com";
						}
						checkUserPluginIdentifier(group_id, plugin_id, profile_id, function(err, identifier){
							if(err){
								logger.error({"r":"i_trxn_v3","msg":"checkUserPluginIdentifier_Error","p":req.body,"er":err});
								return sendError(res,"server_error","server_error");
							}
							createBrandNewOrder(plugin_id,
							 										account._id,
							 										profile_id,
							 										group_id,
							 										groupmember._id,
							 										identifier,
							 										packageFound,
							 										coupon_code,
							 										pkg_amount,
							 										quoted_amount,
							 										payment_mode,
							 										tim,
							 										isAdmin,
							 										activity,
							 										gtwy, 
							 										client, function(err,orderAndAmount){
							 	if(err){
							 		logger.error({"r":"i_trxn_v3","msg":"createBrandNewOrder_error","p":req.body,"er":err});
									return sendError(res,"server_error","server_error");
							 	}
							 	email = orderAndAmount.order_id+"@eckovation.com";
							 	if(orderAndAmount.amount == 0 || orderAndAmount.amount == '0.00'){
							 		logger.warn({"r":"i_trxn_v3","msg":"trxn amount has come out to be 0","p":req.body});
							 		savePaymentSkipResponse(orderAndAmount.order_id, function(err,resp){
							 			if(err){
							 				logger.error({"r":"i_trxn_v3","msg":"savePaymentSkipResponse_error","p":req.body,"er":err,"orderAndAmount":orderAndAmount});
											return sendError(res,"server_error","server_error");
							 			}
								 		var payment_notify_object = new Buffer(JSON.stringify({
									    trxn_id : orderAndAmount.order_id,
									    atmpt   : 1,
									    gtwy    : gtwy,
									    cl      : client
									  }));
										RabbitMqPublish.publish("",RabbitMqQueues.PAYMENT_NOTIFY,payment_notify_object);
										logger.info({"r":"i_trxn_v3","msg":"trxn success with payment skipped","p":req.body,"orderAndAmount":orderAndAmount});
										return sendError(res,"payment_trxn_skip","payment_trxn_skip",constants.HTTP_STATUS.PAYMENT_TRXN_SKIP);
							 		});
							 	} else {
							 		var result = prepareGatewayInputParams(gtwy, groupmember._id, account.m, email, orderAndAmount);
									result.email = orderAndAmount.order_id+"@eckovation.com";
									logger.info({"r":"i_trxn_v3","msg":"trxn success","p":req.body,"orderAndAmount":orderAndAmount,"gtwy":gtwy,"result":result});
									var payment_status_notify_object = new Buffer(JSON.stringify({
								    trxn_id : orderAndAmount.order_id,
								    cron    : 50,
								    gtwy    : gtwy,
								    cl      : client
								  }));
									RabbitMqPublish.publish("",RabbitMqQueues.PAYMENT_NOTIFY,payment_status_notify_object);
									return sendSuccess(res,{params:result,gtwy:gtwy});
								}
							});
						});
					});
				});
			});
		});
	});
});

router.post("/a_coupon",function(req, res, next){
	var tim          = new Date().getTime();
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('pl_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('pkg_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('amt',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('cpn_code',errorCodes.invalid_parameters[1]).notEmpty();

	var profile_id     = req.body.p_id;
	var group_id       = req.body.g_id;
	var plugin_id      = req.body.pl_id;
	var package_id     = req.body.pkg_id;
	var pkg_amount     = req.body.amt;
	var coupon_code;

	if(req.body.cpn_code){
		coupon_code = req.body.cpn_code.trim();
	} else {
		logger.error({"r":"a_coupon","msg":"coupon_code_invalid_parameter","p":req.body});
		return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	UserInfoCache.getMongoGrpMem(profile_id, group_id, function(err, groupmember){
		if(err) { 
			logger.error({"r":"a_coupon","msg":"getMongoGrpMem_DbError","p":req.body,"er":err});
			return sendError(res,"server_error","server_error"); 
		}
		if(!groupmember){
			logger.error({"r":"a_coupon","msg":"not_group_member","p":req.body,"pid":profile_id,"gid":group_id});
			return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}
		GroupPluginPackage.findOne({
			_id       : package_id,
			gid       : group_id,
			plugin_id : plugin_id,
			act       : true,
		},function(err,package_promo_found){
			if(err){ 
				logger.error({"r":"a_coupon","msg":"GroupPluginPackage_find_DbError","p":req.body,"er":err});
				return sendError(res,"server_error","server_error"); 
			}
			if(!package_promo_found){
				logger.error({"r":"a_coupon","msg":"package_promo_not_found","p":req.body});
				return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
			}
			if(package_promo_found.package_price != pkg_amount){
				logger.error({"r":"a_coupon","msg":"package_amount_incorrect_from_client","p":req.body});
				return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
			}
			if(groupmember.type == GroupMember.TYPE.ADMIN){
				pkg_amount = '1.00';
			}
			PromotionCode.findOne({
				code : coupon_code,
				act  : true,
				$or : [
					{
						type : PromotionCode.TYPE.GENERIC
					},
					{
						type      : PromotionCode.TYPE.GROUP_PLUGIN_SPECIFIC,
						gid       : group_id,
						plugin_id : plugin_id
					},
					{
						type      : PromotionCode.TYPE.PLUGIN_SPECIFIC,
						plugin_id : plugin_id
					},
					{
						type      : PromotionCode.TYPE.GROUP_SPECIFIC,
						gid       : group_id
					}
				]
			},{
				valuetype : 1,
				value     : 1,
				expirydate: 1
			},function(err,promotion){
				if(err){
					logger.error({"r":"a_coupon","msg":"PromotionCode_find_DbError","p":req.body,"er":err});
					return sendError(res,"server_error","server_error");
				}
				if(!promotion){
					logger.error({"r":"a_coupon","msg":"promotion_not_found","p":req.body});
					return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
				}
				if(promotion.expirydate < tim){
					logger.error({"r":"a_coupon","msg":"coupon_code_expired","p":req.body});
					return sendError(res,"coupon_code_expired","coupon_code_expired",constants.HTTP_STATUS.COUPON_CODE_EXPIRED);
				}
				// console.log('package promo expiry details');
				// console.log(promotion.expirydate);
				// console.log(tim);
				var famount = calculateFinalAmountAfterDiscount(pkg_amount, promotion);
				logger.info({"r":"a_coupon","msg":"coupon_code_successfull","p":req.body,"famount":famount,"pkg_amount":pkg_amount});
				return sendSuccess(res,{iamt : pkg_amount, famt : famount});
			});
		});
	});
});

function checkUserPluginIdentifier(group_id, plugin_id, profile_id, cb){
	AppPlugin.findOne({
		_id : plugin_id,
		act : true
	},function(err,app_plugin){
		if(err){ 
			return cb(err,null);
		}
		if(!app_plugin){
			return cb("app_plugin not found for plugin_id : "+plugin_id,null);
		}
		PluginIdentifier.findOne({
			plugin_id : plugin_id,
			gid       : group_id,
			pid       : profile_id,
			act       : true
		},function(err,prev_identifer){
			if(err) { 
				return cb(err,null);
			}
			if(prev_identifer){
				return cb(null,prev_identifer.identifier);
			}
			var data_object = {
				name : 1,
				ppic : 1,
				role : 1,
				m    : 0,
				email: 0,
				grps : 0,
				pfls : 0,
				id1  : profile_id,
				id2  : plugin_id,
				id3  : group_id
			};
			var user_identifier = PluginToken.getUserPluginToken(data_object, app_plugin.private_key);
			var new_identifier = new PluginIdentifier({
				plugin_id : plugin_id,
				gid       : group_id,
				pid       : profile_id,
				act       : true,
				identifier: user_identifier
			});
			new_identifier.save(function(err,new_identifier_created){
				if(err) { 
					return cb(err,null);
				}
				return cb(null,user_identifier);
			});
		});
	});
}

function createBrandNewOrder(
															plugin_id,
															aid, 
															pid, 
															gid, 
															grpMemId, 
															identifier, 
															package_found, 
															coupon_code, 
															iamount, 
															quoted_amount, 
															mode, 
															tim, 
															isAdmin, 
															activity, 
															gtwy, 
															client,
															cb
														) {
	managePromotions(package_found.promo_code, coupon_code, iamount, quoted_amount, isAdmin,  gid, plugin_id, tim, function(err,famount){
		if(err){
			return cb(err,null);
		}
		if(!isAdmin && famount != quoted_amount){
			return cb("quoted_amount_not_equal_to_final_amount, famount : "+famount+", quoted_amount : "+quoted_amount,null);
		}
		var order_id = TrxnOrder.generateNewOrderId();
		var status, gateway_status;
		if(famount == 0){
			status         = Transaction.STATUS.TXN_SUCCESS;
			gateway_status = Transaction.GATEWAY_STATUS.NO_NEED;
		} else {
			status = Transaction.STATUS.INITIATED;
			gateway_status = Transaction.GATEWAY_STATUS.SUBMITTED;
		}
		famount = parseInt(famount);
		var new_transaction = new Transaction({
			_id            : order_id,
			aid            : aid,
			customer_id    : grpMemId,
			pid            : pid,
			gid            : gid,
			module         : Transaction.MODULE.TEST,
			identifier     : identifier,
			activity       : activity,
			cl             : client,
			payment_mode   : mode,
			plugin_id      : plugin_id,
			package_id     : package_found._id,
			actual_amount  : iamount,
			actual_amount_p: (iamount*100),
			coupon_code    : coupon_code,
			final_amount   : famount.toFixed(2),
			final_amount_p : (famount.toFixed(2)*100),
			status         : status,
			gateway        : gtwy,
			gateway_status : gateway_status,
			tim            : tim
		});
		var new_trxn_log = new TransactionLog({
			trxn_id          : order_id,
			status           : Transaction.STATUS.INITIATED,
			log              : "USER HAS INITIATED THE TRXN"
		});
		var done = 0;
		var toDo = 2;
		new_transaction.save(function(err,resp){
			if(err){
				return cb(err,null);
			}
			done++;
			if(done == toDo){
				return cb(null,{order_id:order_id,amount:parseInt(famount)});
			}
		});
		new_trxn_log.save(function(err,new_log){
			if(err){
				return cb(err,null);
			}
			done++;
			if(done == toDo){
				return cb(null,{order_id:order_id,amount:parseInt(famount)});
			}
		});
	});
}

function managePromotions(package_promo, coupon_code, iamount, quoted_amount, isAdmin, group_id, plugin_id, tim, cb){
	if(!coupon_code){
		if(isAdmin){
			return cb(null,quoted_amount);
		}
		return cb(null,iamount);
	}
	PromotionCode.findOne({
		code : coupon_code,
		act  : true,
		$or : [
			{
				type : PromotionCode.TYPE.GENERIC
			},
			{
				type      : PromotionCode.TYPE.GROUP_PLUGIN_SPECIFIC,
				gid       : group_id,
				plugin_id : plugin_id
			},
			{
				type      : PromotionCode.TYPE.PLUGIN_SPECIFIC,
				plugin_id : plugin_id
			},
			{
				type      : PromotionCode.TYPE.GROUP_SPECIFIC,
				gid       : group_id
			}
		],
		expirydate : {$gte:tim}
	},function(err,promo){
		if(err){
			return cb(err,null);
		}
		if(!promo){
			return cb("PROMO_NOT_FOUND, CODE : "+coupon_code+", group_id : "+group_id+", plugin_id :"+plugin_id,iamount+", quoted_amount :"+quoted_amount,null);
		}
		if(isAdmin){
			return cb(null,calculateFinalAmountAfterDiscount(quoted_amount, promo));
		}
		return cb(null,calculateFinalAmountAfterDiscount(iamount, promo));
	});
}

function prepareGatewayInputParams(gtwy, groupmember_id, mobile, email, orderAndAmount){
	mobile   = parseInt(mobile);
	var order_id = orderAndAmount.order_id;
	var amount   = orderAndAmount.amount.toFixed(2);
	if(gtwy == Transaction.GATEWAY.PAYTM){
		return {
			CHANNEL_ID 				: PaytmConfigs.CHANNEL_ID,
			CUST_ID 					: groupmember_id,
			EMAIL 						: email,
			INDUSTRY_TYPE_ID 	: PaytmConfigs.INDUSTRY_TYPE_ID,
			MID 							: PaytmConfigs.MID,
			MOBILE_NO 				: mobile,
			ORDER_ID 					: orderAndAmount.order_id,
			THEME 					  : PaytmConfigs.THEME,
			TXN_AMOUNT 				: amount,
			WEBSITE 					: PaytmConfigs.WEBSITE
		};
	}
	return {
		NAME        : RazorpayConfigs.NAME,
		DESCRIPTION : "Payment Tried From Razorpay",
		CURRENCY    : RazorpayConfigs.CURRENCY,
		TXN_AMOUNT  : (amount*100),
		ORDER_ID    : order_id,
		EMAIL       : email,
		MOBILE_NO   : mobile      
	};
}

function calculateFinalAmountAfterDiscount(iamount, promo){
	var famount = 0;
	if(promo.valuetype == PromotionCode.VALUE_TYPE.NET_DISCOUNT){
		famount = iamount - promo.value;
		if(famount < 0){
			famount = 0;
		}
	} else if(promo.valuetype == PromotionCode.VALUE_TYPE.DISCOUNT_PERCENTAGE){
		famount = iamount - ((iamount * promo.value) / 100);
	}
	return famount.toFixed(2);
}

function isValidPaymentMode(mode){
	var possiblePaymentModes = [
															Transaction.PAYMENT_MODE.CREDIT_CARD,
															Transaction.PAYMENT_MODE.DEBIT_CARD,
															Transaction.PAYMENT_MODE.NET_BANKING,
															Transaction.PAYMENT_MODE.PAYTM_WALLET,
															Transaction.PAYMENT_MODE.OTHER_WALLET
	];
	if(possiblePaymentModes.indexOf(mode) >=0)
		return true;
	return false;
}

function savePaymentSkipResponse(trxn_id, cb){
	var toDo  = 3;
	var done  = 0;
	GatewayResponse.update({
		trxn_id : trxn_id
	},{
		$setOnInsert : {
			trxn_id : trxn_id,
			resp    : {"GATEWAYNAME":"WALLET","STATUS":"TXN_SUCCESS"}
		}
	},{
		upsert : true,
	},function(err,gatewayresponse){
		if(err){
			return cb(err,null);
		}
		done++;
		if(done == toDo){
			return cb(null,true);
		}
	});
	Transaction.update({
		_id : trxn_id
	},{
		status           : Transaction.STATUS.SUCCESS,
		gateway_status   : Transaction.GATEWAY_STATUS.NO_NEED,
		gateway_respcode : "PAYMENT_GATEWAY_SKIPPED"
	},function(err,transaction_updated){
		if(err){
			return cb(err,null);
		}
		createUserSubsription(trxn_id, function(err, subs_resp){
			if(err){
				return cb(err,null);
			}
			done++;
			if(done == toDo){
				return cb(null,true);
			}
		});
	});
	var new_trxn_log = new TransactionLog({
		trxn_id          : trxn_id,
		gateway_status   : Transaction.GATEWAY_STATUS.NO_NEED,
		gateway_log      : "PAYMENT_GATEWAY_SKIPPED",
		log              : "PAYMENT_GATEWAY_SKIPPED"
	});
	new_trxn_log.save(function(err,new_log){
		if(err){
			return cb(err,null);
		}
		done++;
		if(done == toDo){
			return cb(null,true);
		}
	});
}

function createUserSubsription(trxn_id, cb){
	var subscription = {};
	var start_time   = new Date().getTime();
	Transaction.findOne({
		_id : trxn_id
	},function(err,transaction){
		if(err){
			return cb(err,null);
		}
		subscription.trxn_id     = trxn_id;
		subscription.customer_id = transaction.customer_id;
		subscription.aid         = transaction.aid;
		subscription.gid         = transaction.gid;
		subscription.pid         = transaction.pid;
		subscription.stim        = start_time;
		subscription.plugin_id   = transaction.plugin_id;
		subscription.identifier  = transaction.identifier;
		if(transaction.package_id){
			subscription.package_id = transaction.package_id;
		}
		UserSubscription.update({
			gid : transaction.gid,
			pid : transaction.pid,
			plugin_id : transaction.plugin_id,
			identifier : transaction.identifier,
			act : true
		},{
			act : false
		},{
			multi : true
		},function(err,resp){
			if(err){
				return cb(err,null);
			}
			findExpiryTimeForSubscription(transaction, function(err,resp){
				if(err){
					return cb(err,null);
				}
				subscription.etim  = start_time + resp;
				subscription.act   = true;
				var user_subscription = new UserSubscription(subscription);
				user_subscription.save(function(err,new_subscription_created){
					if(err){
						return cb(err,null);
					}
					return cb(null,true);
				});
			});
		});
	});
}

function findExpiryTimeForSubscription(transaction, cb){
	GroupPluginPackage.findOne({
		_id       : transaction.package_id,
		gid       : transaction.gid,
		plugin_id : transaction.plugin_id,
		act       : true
	},function(err, plugin_package){
		if(err){
			return cb(err,null);
		}
		if(!plugin_package){
			return cb("no_plugin_packages_found_for_trxnID_"+transaction._id,null); 
		}
		if(!plugin_package.package_duration){
			return cb("no_package_duration_found_for_plugin_packages_for_trxnID_"+transaction._id,null);
		}
		return cb(null,plugin_package.package_duration);
	});
}

function ifNewEmailThenUpdate(aid, email) {
	Account.update({
		_id : aid,
		e   : { $ne : email }
	},{
		$push : { e : email }
	},function(err,new_email_updated){
		if(err){  
			return logger.error({"r":"ifNewEmailThenUpdate","msg":"updateEmail_error","p":{aid:aid,em:email},"er":err}); 
		}
		return logger.info({"r":"ifNewEmailThenUpdate","p":{aid:aid,em:email},"resp":new_email_updated}); 
	});
}

module.exports = router;