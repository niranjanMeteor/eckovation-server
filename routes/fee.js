const express 								= require('express');
const mongoose 								= require('mongoose');
const jwt    									= require('jsonwebtoken');

const configs									= require('../utility/configs.js');
const PaytmConfigs					  = require('../payment/paytm_configs.js');
const FeeConfigs              = require('../fee/configs.js');
const RazorpayConfigs					= require('../payment/razorpay_configs.js');
const TrxnOrder					      = require('../payment/trxnorder.js');
const errorCodes 							= require('../utility/errors.js');
const constants								= require('../utility/constants.js');
const helpers									= require('../utility/helpers.js');
const AppClients							= require('../utility/app_clients.js');
const log4jsLogger  					= require('../loggers/log4js_module.js');
const log4jsConfigs  					= require('../loggers/log4js_configs.js');
const MailClient          		= require('../utility/mail/mail_client.js');
const UserInfoCache           = require('../utility/user_info_cache.js');
const GroupInfoCache          = require('../utility/group_info_cache.js');
const FeeApi                  = require('../fee/apicalls.js');

require('../models/accounts.js');
require('../models/profiles.js');
require('../models/groups.js');
require('../models/groupmembers.js');
require('../models/plugins.js');
require('../models/groupplugins.js');
require('../models/fee/grouppathshalaserver.js');
require('../models/payment/transactions.js');
require('../models/payment/transactionlogs.js');
require('../models/fee/feetransactions.js');
require('../models/payment/gatewayresponse.js');
const Account 								= mongoose.model('Account');
const Profile 								= mongoose.model('Profile');
const Group 								  = mongoose.model('Group');
const GroupMember 					  = mongoose.model('GroupMember');
const AppPlugin       			  = mongoose.model('AppPlugin');
const GroupPlugin 					  = mongoose.model('GroupPlugin');
const GroupPathshalaServer    = mongoose.model('GroupPathshalaServer');
const Transaction 					  = mongoose.model('Transaction');
const TransactionLog 					= mongoose.model('TransactionLog');
const FeeTransaction          = mongoose.model('FeeTransaction');
const GatewayResponse 				= mongoose.model('GatewayResponse');

const sendError 							= helpers.sendError;
const sendSuccess 						= helpers.sendSuccess;
const router 									= express.Router();
const logger        			    = log4jsLogger.getLogger(log4jsConfigs.CATEGORY_ROUTES_FEE);
const RAZORPAY_MODES          = [Transaction.PAYMENT_MODE.CREDIT_CARD,Transaction.PAYMENT_MODE.DEBIT_CARD,Transaction.PAYMENT_MODE.OTHER_WALLET,Transaction.PAYMENT_MODE.NET_BANKING];
const COMMISSION_DETAILS      = getCommisionCalculationDetails();

//route middleware to verify the jwt token for all users
router.use(function(req, res, next){
	var user_identifier = req.headers['x-access-idnt'];
	if(!user_identifier){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"user_identifier_not_found","p":req.body});
	  return sendError(res,"Access without token is not authorised","token_not_found",constants.HTTP_STATUS.FORBIDDEN);
	}
  jwt.verify(user_identifier, configs.USER_IDENTIFIER_TOKEN_PRIVATE_KEY, function(err, decoded) {      
    if(err){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"jwt_verify_error","p":req.body,"er":err});
      return sendError(res,"Failed to authenticate token.","token_auth_failure",constants.HTTP_STATUS.TOKEN_EXPIRED);  
    }
  	req.decoded = decoded;
    next();
  });
});

router.use(function(req, res, next){
  req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
  req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
  req.checkBody('pl_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();

  if(req.validationErrors()){ 
    logger.error({"r":req.originalUrl,"msg":"invalid_parameters","p":req.body,"er":req.validationErrors()});
    return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
  }

  var profile_id          = req.body.p_id;
  var group_id            = req.body.g_id;
  var plugin_id           = req.body.pl_id;
  var decoded             = req.decoded;

  if(decoded.id1 != profile_id){
    logger.error({"r":req.originalUrl,"msg":"token_decoded_profile_id_mismatch","p":req.body});
    return sendError(res,"token_auth_failure","token_auth_failure",constants.HTTP_STATUS.UNAUTHORIZED);
  }
  if(decoded.id2 != plugin_id){
    logger.error({"r":req.originalUrl,"msg":"token_decoded_plugin_id_mismatch","p":req.body});
    return sendError(res,"token_auth_failure","token_auth_failure",constants.HTTP_STATUS.UNAUTHORIZED);
  }
  if(decoded.id3 != group_id){
    logger.error({"r":req.originalUrl,"msg":"token_decoded_group_id_mismatch","p":req.body});
    return sendError(res,"token_auth_failure","token_auth_failure",constants.HTTP_STATUS.UNAUTHORIZED);
  }
  GroupMember.findOne({
    gid : group_id,
    pid : profile_id,
    act : true,
    gdel: false
  },function(err, groupmember){
    if(err) { 
      logger.error({"r":req.originalUrl,"msg":"getMongoGrpMem_DbError","p":req.body,"er":err});
      return sendError(res,"server_error","server_error"); 
    }
    if(!groupmember){
      logger.error({"r":req.originalUrl,"msg":"not_group_member","p":req.body,"pid":profile_id,"gid":group_id});
      return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
    }
    GroupPlugin.findOne({
      gid       : group_id,
      act       : true,
      plgnid    : plugin_id,
      status    : GroupPlugin.STATUS.ACTIVE
    },function(err,groupplugin){
      if(err){
        logger.error({"r":req.originalUrl,"msg":"GroupPlugin_DbError","p":req.body,"er":err});
        return sendError(res,"server_error","server_error");
      }
      if(!groupplugin){
        logger.error({"r":req.originalUrl,"msg":"group_plugin_not_found","p":req.body});
        return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
      }
      req.aid       = groupmember.aid;
      req.prf_name  = groupmember.pnam;
      req.grpMemId  = groupmember._id;
      next();
    });
  });

});

router.post('/fee_dtl',function(req, res, next){
  req.checkBody('month',errorCodes.invalid_parameters[1]).notEmpty();
  req.checkBody('year',errorCodes.invalid_parameters[1]).notEmpty();
  console.log('1');
  if(req.validationErrors()){ 
    logger.error({"r":"fee_dtl","msg":"invalid_parameters","p":req.body,"er":req.validationErrors()});
    return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
  }
  console.log('2');
  var profile_id          = req.body.p_id;
  var group_id            = req.body.g_id;
  var plugin_id           = req.body.pl_id;
  var month               = req.body.month;
  var year                = req.body.year;
  var aid                 = req.aid;
  var profile_name        = req.prf_name;
  var client              = (!req.decoded.cl) ? AppClients.ANDROID : (req.decoded.cl);
  var tim                 = new Date().getTime();
  console.log('3');
  Group.findOne({
    _id : group_id
  },function(err,group){
    if(err) { 
      logger.error({"r":"fee_dtl","msg":"GroupFind_DbError","p":req.body,"er":err});
      return sendError(res,"server_error","server_error"); 
    }
    if(!group){
      logger.error({"r":"fee_dtl","msg":"group_not_found","p":req.body});
      return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
    }console.log('4');
    Account.findOne({
      _id : aid,
      vrfy: true
    },function(err,account){
      if(err) { 
        logger.error({"r":"fee_dtl","msg":"AccountFind_DbError","p":req.body,"er":err});
        return sendError(res,"server_error","server_error"); 
      }
      if(!account){
        logger.error({"r":"fee_dtl","msg":"account_not_found","p":req.body});
        return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
      }console.log('5');
      if(!group.i_brd){
        getUnboardedFeeDetails(group, 
                      account.m, 
                      profile_id, 
                      account._id,
                      plugin_id,
                      client,
                      profile_name,
                      month,
                      year,
                      tim,
                      function(err,details){
          if(err) { 
            logger.error({"r":"fee_dtl","msg":"getFeeDetails_Error","p":req.body,"er":err});
            return sendError(res,"server_error","server_error"); 
          }console.log('6');
          details.commission = COMMISSION_DETAILS;
          return sendSuccess(res,details);
        });
      } else {
        getBoardedFeeDetails(group, 
                      account.m, 
                      profile_id, 
                      account._id,
                      plugin_id,
                      client,
                      profile_name,
                      month,
                      year,
                      tim,
                      function(err,details){
          if(err) { 
            logger.error({"r":"fee_dtl","msg":"getFeeDetails_Error","p":req.body,"er":err});
            return sendError(res,"server_error","server_error"); 
          }console.log('7');
          if(details == 449){
            logger.warn({"r":"fee_dtl","msg":"no_fee_record_found_for_boarded","p":req.body});
            return sendError(res,"no_fee_record_found","no_fee_record_found",constants.HTTP_STATUS.SERVER_ERROR_CODE);
          }
          details.commission = COMMISSION_DETAILS;
          return sendSuccess(res,details);
        });
      }
    });
  });
});

router.post('/i_fee',function(req, res, next){
  req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
  req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
  req.checkBody('pl_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
  req.checkBody('p_mode',errorCodes.invalid_parameters[1]).notEmpty();
  req.checkBody('a_amt',errorCodes.invalid_parameters[1]).notEmpty();
  req.checkBody('f_amt',errorCodes.invalid_parameters[1]).notEmpty();
  req.checkBody('month',errorCodes.invalid_parameters[1]).notEmpty();
  req.checkBody('year',errorCodes.invalid_parameters[1]).notEmpty();
  req.checkBody('fee_id',errorCodes.invalid_parameters[1]).notEmpty();
  req.checkBody('payee_id',errorCodes.invalid_parameters[1]).notEmpty();
  req.checkBody('payee_n',errorCodes.invalid_parameters[1]).notEmpty();
  req.checkBody('payee_e',errorCodes.invalid_parameters[1]).optional();
  req.checkBody('srv_id',errorCodes.invalid_parameters[1]).optional();
  console.log('1');
  if(req.validationErrors()){ 
    logger.error({"r":"i_fee","msg":"invalid_parameters","p":req.body,"er":req.validationErrors()});
    return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
  }
  console.log('2');
  var profile_id          = req.body.p_id;
  var group_id            = req.body.g_id;
  var plugin_id           = req.body.pl_id;
  var payment_mode        = req.body.p_mode.trim();
  var actual_amount       = req.body.a_amt.trim();
  var final_amount        = req.body.f_amt.trim();
  var month               = req.body.month;
  var year                = req.body.year;
  var fee_id              = req.body.fee_id;
  var payee_id            = (req.body.payee_id) ? req.body.payee_id.trim() : null;
  var payee_name          = (req.body.payee_n) ? req.body.payee_n.trim() : null;
  var payee_email         = (req.body.payee_e) ? req.body.payee_e.trim() : null;
  var server_id           = (req.body.srv_id) ? req.body.srv_id.trim() : null;
  var gtwy                = Transaction.GATEWAY.PAYTM;
  var client              = (!req.decoded.cl) ? AppClients.ANDROID : (req.decoded.cl);
  var tim                 = new Date().getTime();
  console.log('3');
  if(!isValidPaymentMode(payment_mode)){
    logger.error({"r":"i_fee","msg":"invalid_payment_mode","p":req.body});
    return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
  }
  if(RAZORPAY_MODES.indexOf(payment_mode) >= 0){
    gtwy = Transaction.GATEWAY.RAZORPAY;
  }
  if(!actual_amount || isNaN(actual_amount) || actual_amount <= 0 ){
    logger.error({"r":"i_fee","msg":"actual_fee_amount_invalid","p":req.body});
    return sendError(res,"fee_amount_invalid","fee_amount_invalid",constants.HTTP_STATUS.SERVER_ERROR_CODE);
  }console.log('4');
  if(!final_amount || isNaN(final_amount) || final_amount <= 0 ){
    logger.error({"r":"i_fee","msg":"final_fee_amount_invalid","p":req.body});
    return sendError(res,"fee_amount_invalid","fee_amount_invalid",constants.HTTP_STATUS.SERVER_ERROR_CODE);
  }console.log('5');
  // var cal_final_amount = calculateFinalFeeAmount(actual_amount);
  // if(cal_final_amount != final_amount){
  //   logger.error({"r":"i_fee","msg":"client_final_fee_did_not_match_with_server","p":req.body,"f_fmt":cal_final_amount});
  //   return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
  // }
  console.log('6');
  actual_amount = parseInt(actual_amount);
  final_amount  = parseInt(final_amount);
  FeeTransaction.findOne({
    _id       : fee_id,
    gid       : group_id,
    plugin_id : plugin_id,
    pid       : profile_id,
    act       : true
  },function(err,feetransaction){
    if(err) { 
      logger.error({"r":"i_fee","msg":"FeeTransactionFind_DbError","p":req.body,"er":err});
      return sendError(res,"server_error","server_error"); 
    }
    if(!feetransaction){
      logger.error({"r":"i_fee","msg":"feetransaction_not_found","p":req.body});
      return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
    }
    if(feetransaction.month != month){
      logger.error({"r":"i_fee","msg":"feetransaction_month_mismatch_with_params","p":req.body});
      return sendError(res,"feetransaction_month_mismatch_with_params","feetransaction_month_mismatch_with_params",constants.HTTP_STATUS.SERVER_ERROR_CODE);
    }
    if(feetransaction.year != year){
      logger.error({"r":"i_fee","msg":"feetransaction_year_mismatch_with_params","p":req.body});
      return sendError(res,"feetransaction_year_mismatch_with_params","feetransaction_year_mismatch_with_params",constants.HTTP_STATUS.SERVER_ERROR_CODE);
    }
    if(feetransaction.i_brd && !server_id){
      logger.error({"r":"i_fee","msg":"feetransaction_boarded_but_server_id_missing","p":req.body});
      return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
    }console.log('7');
    server_id = profile_id;
    var email;
    Account.findOne({
      _id : req.aid,
      vrfy: true
    },function(err,account){
      if(err){
        logger.error({"r":"i_fee","msg":"AccountFind_error","p":req.body,"er":err});
        return sendError(res,"server_error","server_error");
      }
      if(!account){
        logger.error({"r":"i_fee","msg":"account_not_found","p":req.body});
        return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
      }
      if(!email){
        email = (account.e[0]) ? account.e[0] : "noemailecko@gmail.com";
      }console.log('8');
      var final_amount_p  = (final_amount * 100);
      var actual_amount_p = (actual_amount * 100);
      var order_id        = TrxnOrder.generateFeeTrxnOrderId();
      email = order_id+"@eckovation.com";
      var new_transaction = new Transaction({
        _id            : order_id,
        aid            : req.aid,
        customer_id    : req.grpMemId,
        pid            : profile_id,
        gid            : group_id,
        cl             : client,
        payment_mode   : payment_mode,
        plugin_id      : plugin_id,
        module         : Transaction.MODULE.FEE,
        actual_amount  : actual_amount,
        actual_amount_p: actual_amount_p,
        final_amount   : final_amount.toFixed(2),
        final_amount_p : final_amount_p,
        status         : Transaction.STATUS.INITIATED,
        gateway        : gtwy,
        gateway_status : Transaction.GATEWAY_STATUS.SUBMITTED,
        tim            : tim
      });
      var new_trxn_log = new TransactionLog({
        trxn_id          : order_id,
        status           : Transaction.STATUS.INITIATED,
        log              : "USER HAS INITIATED THE TRXN"
      });
      var toEditFee = {
        order_id    : order_id,
        server_id   : server_id,
        payee_id    : payee_id,
        payee_name  : payee_name,
        a_amount    : actual_amount,
        f_amount    : final_amount,
        a_amount_p  : actual_amount_p,
        f_amount_p  : final_amount_p,
        commission  : (final_amount - actual_amount)
      };console.log('9');
      createFeeOrder(new_transaction, new_trxn_log, toEditFee, fee_id, function(err,resp){
        if(err){
          logger.error({"r":"i_fee","msg":"createFeeOrder_error","p":req.body,"er":err});
          return sendError(res,"server_error","server_error");
        }console.log('10');
        var payment_params = prepareGatewayInputParams(gtwy, req.grpMemId, account.m, email, order_id, final_amount, final_amount_p);
        logger.info({"r":"i_fee","msg":"initiate_fee_trx","p":req.body,"pm_prm":payment_params});
        return sendSuccess(res,{params:payment_params,gtwy:gtwy});
      });
    });
  });
});

function createFeeOrder(new_transaction, new_trxn_log, toEditFee, fee_id, cb){
  var done = 0;
  var toDo = 3;
  new_transaction.save(function(err,new_transaction){
    if(err){
      return cb(err,null);
    }
    done++;
    if(done == toDo){
      return cb(null,true);
    }
  });
  new_trxn_log.save(function(err,new_log){
    if(err){
      return cb(err,null);
    }
    done++;
    if(done == toDo){
      return cb(null,true);
    }
  });
  FeeTransaction.update({
    _id : fee_id
  },toEditFee,function(err,fee_update){
    if(err){
      return cb(err,null);
    }
    done++;
    if(done == toDo){
      return cb(null,true);
    }
  });
}

function getUnboardedFeeDetails(group, mobile, pid, aid, plugin_id, cl, prf_name, month, year, tim, cb){
  var details = {};
  var new_fee_transaction_obj = {
    aid           : aid,
    pid           : pid,
    gid           : group._id,
    gcode         : group.code,
    plugin_id     : plugin_id,
    cl            : cl,
    payee_name    : prf_name,
    payee_id      : pid,
    month         : month,
    year          : year,
    i_brd         : false,
    act           : true,
    status        : FeeTransaction.STATUS.INITIATED,
    tim           : tim
  };
  console.log('new_fee_transaction_obj');
  console.log(new_fee_transaction_obj);
  var new_fee_transaction = new FeeTransaction(new_fee_transaction_obj);
  new_fee_transaction.save(function(err,created_fee){
    if(err){
      return cb(err,null);
    }
    details.month      = month;
    details.year       = year;
    details.fee_id     = created_fee._id;
    details.payee      = [{n:prf_name,id:pid}];
    details.is_boarded = false;
    return cb(null,details);
  });
}

function getBoardedFeeDetails(group, mobile, pid, aid, plugin_id, cl, prf_name, month, year, tim, cb){
  var details = {};
  GroupPathshalaServer.findOne({
    gid  : group._id
  },function(err,pathshalaconfigs){
    if(err){
      return cb(err,null);
    }
    if(!pathshalaconfigs){
      return cb("GroupPathshalaServer_Configs_not_found_:_"+group._id,null);
    }
    var params = {
      group_code : group.code,
      mobile     : mobile,
      month      : month,
      year       : year
    };
    FeeApi.pathshalaApi(params, pathshalaconfigs, 1, function(err,fee_details){
      if(err){
        return cb(err,null);
      }
      if(fee_details == 449){
        return cb(null,449);
      }
      var isFeeValid = isValidFeeDetails(fee_details);
      if(isFeeValid.err){
        return cb(isFeeValid.err,null);
      }
      var new_fee_transaction_obj = {
        aid           : aid,
        pid           : pid,
        gid           : group._id,
        gcode         : group.code,
        plugin_id     : plugin_id,
        cl            : cl,
        month         : month,
        year          : year,
        inst_name     : pathshalaconfigs.inst_name,
        p_payee       : fee_details.payee,
        p_fee         : fee_details.fee,
        i_brd         : true,
        act           : true,
        status        : FeeTransaction.STATUS.INITIATED,
        tim           : tim
      };
      var new_fee_transaction = new FeeTransaction(new_fee_transaction_obj);
      new_fee_transaction.save(function(err,created_fee){
        if(err){
          return cb(err,null);
        }
        details.month      = month;
        details.year       = year;
        details.fee_id     = created_fee._id;
        details.payee      = fee_details.payee;
        details.fee        = fee_details.fee;
        details.is_boarded = true;
        return cb(null,details);
      });
    });
  });
}

function prepareGatewayInputParams(gtwy, groupmember_id, mobile, email, order_id, amount, amount_p){
  mobile   = parseInt(mobile);
  var amount   = amount.toFixed(2);
  if(gtwy == Transaction.GATEWAY.PAYTM){
    return {
      CHANNEL_ID        : PaytmConfigs.CHANNEL_ID,
      CUST_ID           : groupmember_id,
      EMAIL             : email,
      INDUSTRY_TYPE_ID  : PaytmConfigs.INDUSTRY_TYPE_ID,
      MID               : PaytmConfigs.MID,
      MOBILE_NO         : mobile,
      ORDER_ID          : order_id,
      THEME             : PaytmConfigs.THEME,
      TXN_AMOUNT        : amount,
      WEBSITE           : PaytmConfigs.WEBSITE
    };
  }
  return {
    NAME        : RazorpayConfigs.NAME,
    DESCRIPTION : "Payment Tried From Razorpay",
    CURRENCY    : RazorpayConfigs.CURRENCY,
    TXN_AMOUNT  : amount_p,
    ORDER_ID    : order_id,
    EMAIL       : email,
    MOBILE_NO   : mobile      
  };
}

function isValidPaymentMode(mode){
  var possiblePaymentModes = [
                              Transaction.PAYMENT_MODE.CREDIT_CARD,
                              Transaction.PAYMENT_MODE.DEBIT_CARD,
                              Transaction.PAYMENT_MODE.NET_BANKING,
                              Transaction.PAYMENT_MODE.PAYTM_WALLET,
                              Transaction.PAYMENT_MODE.OTHER_WALLET
  ];
  if(possiblePaymentModes.indexOf(mode) >=0)
    return true;
  return false;
}

function getCommisionCalculationDetails(){
  var commission = {
    "NB"         : { "type" : 1, "r" : 0.023 },
    "CC"         : { "type" : 1, "r" : 0.023 },
    "DC"         : { "type" : 3, "r" : [ { "rtype" : 1 , "amount" : 1995 , "ckey" : "lte" , "r" : 0.023 }, { "rtype" : 1 , "amount" : 1995 , "ckey" : "gt" , "r" : 0.023 }]},
    "OW"         : { "type" : 1, "r" : 0.023 },
    "PTMW"     : { "type" : 1, "r" : 0.023 }
  };
  // if(FeeConfigs.FEE_ECKOVATION_CHARGE_TYPE == FeeTransaction.FEE_EXTRA_CHARGE_TYPE.PERCENTAGE){
  //   commission.type  = FeeTransaction.FEE_EXTRA_CHARGE_TYPE.PERCENTAGE;
  //   commission.value = FeeConfigs.FEE_TRANSACTION_CHARGE_PERCENT;
  // } else if(FeeConfigs.FEE_ECKOVATION_CHARGE_TYPE == FeeTransaction.FEE_EXTRA_CHARGE_TYPE.NET_AMOUNT){
  //   commission.type  = FeeTransaction.FEE_EXTRA_CHARGE_TYPE.NET_AMOUNT;
  //   commission.value = FeeConfigs.FEE_TRANSACTION_CHARGE_NET;
  // }
  return commission;
}

function calculateFinalFeeAmount(amount){
  var commission = 0;
  if(FeeConfigs.FEE_ECKOVATION_CHARGE_TYPE == FeeTransaction.FEE_EXTRA_CHARGE_TYPE.PERCENTAGE){
    commission = ((amount * FeeConfigs.FEE_TRANSACTION_CHARGE_PERCENT) / 100);
  } else if(FeeConfigs.FEE_ECKOVATION_CHARGE_TYPE == FeeTransaction.FEE_EXTRA_CHARGE_TYPE.NET_AMOUNT){
    commission = FeeConfigs.FEE_TRANSACTION_CHARGE_NET;
  }
  commission = Math.ceil(commission);
  console.log('amount');
  console.log(amount);
  console.log('commission');
  console.log(commission);
  console.log(parseInt(amount) + parseInt(commission));
  return (parseInt(amount) + parseInt(commission));
}

function isValidFeeDetails(fee_details){
  if(!fee_details){
    return {err:"fee_details_empty",suc:false};
  }
  if(!("payee" in fee_details)){
    return {err:"fee_details_payee_not_found",suc:false};
  }
  if(!("fee" in fee_details)){
    return {err:"fee_details_fee_not_found",suc:false};
  }
  if(fee_details.payee.length == 0){
    return {err:"fee_details_payee_array_length_0",suc:false};
  }
  if(fee_details.fee.length == 0){
    return {err:"fee_details_fee_array_length_0",suc:false};
  }
  for(var i=0; i<fee_details.payee.length; i++){
    var temp = fee_details.payee[i];
    if(!("name" in temp) || !("id" in temp) || !("sid" in temp)){
      return {err:"fee_details_payee_array_"+i+"keys_missing",suc:false};
    }
    if(!fee_details.fee[temp.sid]){
      return {err:"fee_details_fee_array_fee_not_found_sid_"+temp.sid,suc:false};
    }
  }
  return {err:null,suc:true};
}

module.exports = router;
