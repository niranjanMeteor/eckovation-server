var express 						= require('express');
var mongoose 						= require('mongoose');
var jwt    				      = require('jsonwebtoken');

var constants						= require('../utility/constants.js');
var configs							= require('../utility/configs.js');
var errorCodes 					= require('../utility/errors.js');
var helpers							= require('../utility/helpers.js');
var UserInfoCache       = require('../utility/user_info_cache.js');
var log4jsLogger  		  = require('../loggers/log4js_module.js');
var log4jsConfigs  			= require('../loggers/log4js_configs.js');
var pluginServerConfig  = require('../pluginserver/configs.js');

require('../models/groupmembers.js');
var GroupMember 				= mongoose.model('GroupMember'); 

var sendError 					= helpers.sendError;
var sendSuccess 				= helpers.sendSuccess;
var router 							= express.Router();
var logger        			= log4jsLogger.getLogger(log4jsConfigs.CATEGORY_ROUTES_ECKOVATION);

//route middleware to verify the jwt token for all users
router.use(function(req, res, next){
	var tim           = new Date().getTime();
	var parameters    = req.body.parameters;
	if(!parameters){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"parameter_not_found","p":req.body});
	  return sendError(res,"Access without Parameters is not authorised","parameter_not_found",constants.HTTP_STATUS.FORBIDDEN);
	}
	parameters = parameters.trim();
	if(!parameters){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"parameter_not_found_after_trimming","p":req.body});
	  return sendError(res,"Access without Parameters is not authorised","parameter_not_found",constants.HTTP_STATUS.FORBIDDEN);
	}
  jwt.verify(parameters, pluginServerConfig.ECKOVATION_GROUPJOIN_API_KEY, function(err, decoded){      
    if(err){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"jwt_verify_error","p":req.body,"er":err});
      return sendError(res,"Failed to authenticate token.","token_auth_failure",constants.HTTP_STATUS.TOKEN_EXPIRED);  
    } 
    req.tim     = tim;
    req.decoded = decoded;
    next();
  });
});

router.post('/eckovationcallback', function(req, res, next){

	if(!isValidParams(req.decoded)){
		logger.error({"r":"eckovationcallback","msg":"invalid_parameters","p":req.body});
		return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}
	
	var plugin_id    						= req.decoded.PLUGIN_ID;
	var group_id     						= req.decoded.GROUP_ID;
	var groupmember_id     			= req.decoded.CUSTOMER_ID;
	var account_id              = req.decoded.AID;
	var final_amount_paid       = req.decoded.ORDER_AMOUNT;
	var subscription_start_time = req.decoded.START_TIME;
	var subscription_end_time   = req.decoded.END_TIME; 
	var order_id                = req.decoded.ORDER_ID;
	var tim                     = req.decoded.tim;

	GroupMember.findOne({
		_id : groupmember_id,
		act : true,
		gdel: false
	},function(err,groupmember){
		if(err) { 
			logger.error({"r":"eckovationcallback","msg":"GroupMemberFind_DbError","p":req.body,"er":err});
			return sendError(res,"server_error","server_error"); 
		}
		if(!groupmember){
			logger.error({"r":"eckovationcallback","msg":"groupmember_not_found","p":req.body});
			return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}
		GroupMember.update({
			_id : groupmember_id
		},{
			plugin_id : plugin_id,
			ordr_id   : order_id,
			pamt      : final_amount_paid,
			sub_st    : subscription_start_time,
			sub_ed    : subscription_end_time
		},function(err,subscription_created){
			if(err){ 
				logger.error({"r":"eckovationcallback","msg":"GroupMemberUpdate_DbError","p":req.body,"er":err});
				return sendError(res,"server_error","server_error"); 
			}
			clearAllGroupMemberCache(account_id, groupmember.gid, groupmember.pid, function(err,resp){
				if(err){ 
					logger.error({"r":"eckovationcallback","msg":"clearAllGroupMemberCache_Error","p":req.body,"er":err});
					return sendError(res,"server_error","server_error"); 
				}
				logger.error({"r":"eckovationcallback","msg":"GroupMember_Subscription_Created","p":req.decoded});
				return sendSuccess(res,{});
			});
		});
	});
});

function clearAllGroupMemberCache(aid, gid, pid, cb){
	var toDo = 2;
	var done = 0;
	console.log('going to clear the cache for aid : '+aid+', gid : '+gid+', pid :'+pid);
	UserInfoCache.removeMongoGrpMem(pid, gid, function(err,memdel_resp){
		if(err){
			return cb(err,null);
		}
		console.log('removeMongoGrpMem response is :'+memdel_resp);
		done++;
		if(done == toDo){
			return cb(null,true);
		}
	});
	UserInfoCache.rmUsrGrpMemsByAidInCache(aid, function(err,aid_memdel_resp){
		if(err){
			return cb(err,null);
		}
		console.log('rmUsrGrpMemsByAidInCache response is :'+aid_memdel_resp);
		done++;
		if(done == toDo){
			return cb(null,true);
		}
	});
}

function isValidParams(parameters){
	if(!parameters.PLUGIN_ID){
		logger.error({"r":"eckovationcallback","msg":"invalid_parameters_missing_PLUGIN_ID","p":parameters});
		return false;
	}
	if(!parameters.GROUP_ID){
		logger.error({"r":"eckovationcallback","msg":"invalid_parameters_missing_GROUP_ID","p":parameters});
		return false;
	}
	if(!parameters.CUSTOMER_ID){
		logger.error({"r":"eckovationcallback","msg":"invalid_parameters_missing_CUSTOMER_ID","p":parameters});
		return false;
	}
	if(!parameters.AID){
		logger.error({"r":"eckovationcallback","msg":"invalid_parameters_missing_AID","p":parameters});
		return false;
	}
	if(!parameters.ORDER_AMOUNT){
		logger.error({"r":"eckovationcallback","msg":"invalid_parameters_missing_ORDER_AMOUNT","p":parameters});
		return false;
	}
	if(!parameters.START_TIME){
		logger.error({"r":"eckovationcallback","msg":"invalid_parameters_missing_START_TIME","p":parameters});
		return false;
	}
	if(!parameters.END_TIME){
		logger.error({"r":"eckovationcallback","msg":"invalid_parameters_missing_END_TIME","p":parameters});
		return false;
	}
	if(!parameters.ORDER_ID){
		logger.error({"r":"eckovationcallback","msg":"invalid_parameters_missing_ORDER_ID","p":parameters});
		return false;
	}
	return true;
}



module.exports = router;