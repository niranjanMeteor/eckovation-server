var express 							= require('express');
var mongoose 							= require('mongoose');
var jwt    								= require('jsonwebtoken');
var redis         				= require('redis');

var constants							= require('../utility/constants.js');
var configs								= require('../utility/configs.js');
var errorCodes 						= require('../utility/errors.js');
var GroupCode 						= require('../utility/group_code_generator.js');
var helpers								= require('../utility/helpers.js');
var GroupNotify 					= require('../utility/grp_notify.js');
var Actions 							= require('../utility/action_names.js');
var AccountInfo   				= require('../utility/account_info.js');
var ClientType    				= require('../utility/client_type.js');
var AppClients						= require('../utility/app_clients.js');
var UserRedisKeys 				= require('../utility/user_redis_keys.js');
var RedisPrefixes 				= require('../utility/redis_prefix.js');
var log4jsLogger  				= require('../loggers/log4js_module.js');
var log4jsConfigs  			  = require('../loggers/log4js_configs.js');
var UserInfoCache       	= require('../utility/user_info_cache.js');
var GroupInfoCache       	= require('../utility/group_info_cache.js');
var MobileNumberRegions   = require('../utility/check_region_by_mobile.js');
var UnsecureAccounts      = require('../utility/auth/unsecure_accounts.js');
var ValidAuthRequest      = require('../utility/auth/valid_request.js');
var TargeVersionActions   = require('../utility/target_version_actions.js');
var AccountDataInfoCache  = require('../cache/accountdata_info.js');

var redis_client        	= redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
    console.log("Error " + err);
});

require('../models/accounts.js');
require('../models/groups.js');
require('../models/groupmembers.js');
require('../models/profiles.js');
require('../models/oldprofiles.js');
require('../models/accountdata.js');
require('../models/recommendedgroups.js');
require('../models/categories.js');
require('../models/groupcategories.js');
require('../models/groupplugins.js')
require('../models/packages/grouppluginpackages.js');

var Account 			    = mongoose.model('Account');
var Group 						= mongoose.model('Group');
var GroupMember 			= mongoose.model('GroupMember');
var Profile 					= mongoose.model('Profile');
var OldProfile 				= mongoose.model('OldProfile');
var AccountData 			= mongoose.model('AccountData');
var RecommendedGroup 	= mongoose.model('RecommendedGroup');
var Category 	        = mongoose.model('Category');
var GroupCategory 	  = mongoose.model('GroupCategory');
var GroupPlugin       = mongoose.model('GroupPlugin');
var GroupPluginPackage= mongoose.model('GroupPluginPackage');

var rhmset            = require("../utility/redis_hmset.js")(redis_client);
var rmc               = require("./../utility/redis_mongo_cache.js")(redis_client);

var sendError 				= helpers.sendError;
var sendSuccess 			= helpers.sendSuccess;

var router 						= express.Router();
var logger        		= log4jsLogger.getLogger(log4jsConfigs.CATEGORY_ROUTES_GROUPS);
var secureGroupLogger = log4jsLogger.getLogger(log4jsConfigs.CATEGORY_SECURE_GROUP);

/*
 * VIEW GROUP DETAILS
 */
router.get('/index', function(req, res, next) {
	req.checkQuery('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkQuery('fetch_inactive',errorCodes.invalid_parameters[1]).notEmpty().isValidBooleanString();
	req.checkQuery('tokn',errorCodes.invalid_parameters[1]).notEmpty().validateToken();

	if(req.validationErrors()) { 
		logger.error({"r":"index","m":"GET","msg":"invalid_parameters","p":req.query});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var group_id 				= req.query.g_id;
	var fetch_inactive 	= req.query.fetch_inactive;

	GroupInfoCache.getMongoGroup(group_id,function(err,view_group){
		if(err) { 
			logger.error({"r":"index","m":"GET","msg":"group_find_server_error","p":req.query});
			return sendError(res,err,"server_error"); 
		}
		if(!view_group) {
			logger.error({"r":"index","m":"GET","msg":"GroupNotFound","p":req.query});
			return sendError(res,"Sorry, The group code did not match to any group.","invalid_group_code",constants.HTTP_STATUS.NOT_FOUND);
		}
		if(fetch_inactive == "false" && view_group.act == false) {
			logger.error({"r":"index","m":"GET","msg":"no_group_found","p":req.query});
			return sendError(res,"No Group Found","no_group_found",constants.HTTP_STATUS.NOT_FOUND);
		} else {
			return sendSuccess(res, { Group : view_group });
		}
	});
});

//route middleware to verify the jwt token for all unsecured accounts as well covering for android users
router.use(function(req, res, next){
	var token = req.body.tokn || req.query.tokn;
	if(!token){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"token_not_found"});
	  return sendError(res,"Access without token is not authorised","token_not_found",constants.HTTP_STATUS.FORBIDDEN);
	}
	var pid = req.body.p_id || req.query.p_id;
	var aid = req.body.a_id || req.query.a_id;
	req.decoded = null;
	if(token == 5 || token == '5'|| token == 'null'){
		UnsecureAccounts.check(aid, pid, function(err,isUnsecure){
			if(err){
				console.log(err);
				logger.error({"url":req.originalUrl,"r":"auth","msg":"UnsecureAccounts_check_error","p":{pid:pid,aid:aid}});
				return sendError(res,err,"server_error");
			}
			if(isUnsecure){
				next();
			} else {
				logger.error({"url":req.originalUrl,"r":"auth","msg":"account_not_found_in_unsecured_list","p":{pid:pid,aid:aid}});
				return sendError(res,"Failed to authenticate token.","token_auth_failure",constants.HTTP_STATUS.NOT_FOUND);
			}
		});
	} else {
    jwt.verify(token, configs.JWT_ACCESS_TOKEN_PRIVATE_KEY, function(err, decoded) {      
      if(err){
      	console.log(err);
      	logger.error({"url":req.originalUrl,"r":"auth","msg":"jwt_verify_error","p":{pid:pid,aid:aid,tokn:token}});
        return sendError(res,"Failed to authenticate token.","token_auth_failure",constants.HTTP_STATUS.TOKEN_EXPIRED);  
      } else {
      	req.decoded = decoded;
        next();
      }
    });
	}
});

/*
 *PROVIDED A PROFILE ID RETURNS ALL THE GROUPS OF THE USER
 */
router.get('/fetch', function(req, res, next){
	req.checkQuery('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkQuery('tokn',errorCodes.invalid_parameters[1]).notEmpty().validateToken();

	if(req.validationErrors()) { 
		logger.error({"r":"fetch","m":"GET","msg":"invalid_parameters","p":req.query});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var profile_id = req.query.p_id;

	GroupMember.find({
		pid : profile_id,
		act : true
	},function(err,gms){
		if(err) {
			logger.error({"r":"fetch","m":"GET","msg":"groupmember_find_server_error","p":req.query}); 
			return sendError(res,err,"server_error");
		}
		if(gms.length<=0) {
			logger.error({"r":"fetch","m":"GET","msg":"no_group_found","p":req.query});
			return sendError(res,"No Group Found","no_group_found",constants.HTTP_STATUS.NOT_FOUND);
		}
		if(!ValidAuthRequest.check(gms[0].aid,req.decoded)){
			logger.error({"r":"fetch","m":"GET","msg":"ValidAuthRequest_check_error","p":req.query});
			return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}

		var gids = [];
		gms.forEach(function(gm){
			gids.push(gm.gid);
		});

		Group.find({
			_id  : {$in : gids},
			act  : true
		},function(err,groups){
			if(err){ 
				logger.error({"r":"fetch","m":"GET","msg":"group_find_server_error","p":req.query});
				return sendError(res,err,"server_error"); 
			}
			if(groups.length == 0) { 
				logger.error({"r":"fetch","m":"GET","msg":"no_group_found","p":req.query});
				return sendError(res,"No Group Found","no_group_found",constants.HTTP_STATUS.NOT_FOUND); 
			}
			return sendSuccess(res,{ groupmembers : gms, groups : groups});
		});
	});
});

/*
 * CREATE GROUP
 */
router.post('/index', function(req, res, next) {
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('name',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('gpic',errorCodes.invalid_parameters[1]).optional().isValidURL();
	req.checkBody('s_id',errorCodes.invalid_parameters[1]).optional().notEmpty().isValidMongoId();
	req.checkBody('clss',errorCodes.invalid_parameters[1]).optional().isValidClass();
	req.checkBody('tokn',errorCodes.invalid_parameters[1]).notEmpty().validateToken();

	if(req.validationErrors()) { 
		logger.error({"r":"index","m":"POST","msg":"invalid_parameters","p":req.body});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var time                  = new Date().getTime();
	var group_initial_admin 	= req.body.p_id;

	var group_name 						= req.body.name ? req.body.name.trim() : null;
	if(!group_name){
		logger.error({"r":"index","msg":"Group Name Invalid","p":req.body});
		return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}		

	var school_id							= req.body.s_id ? req.body.s_id : null;
	var group_class 					= req.body.clss ? req.body.clss : null;
	var group_pic 						= req.body.gpic ? req.body.gpic : null;	


	UserInfoCache.getMongoProfile(group_initial_admin,function(err,admin_profile){
		if(err) { 
			logger.error({"r":"index","m":"POST","msg":"profile_find_server_error","p":req.body});
			return sendError(res,err,"server_error");
		}
		if(!admin_profile) {
			logger.error({"r":"index","m":"POST","msg":"profile_not_found","p":req.body});
			return sendError(res,"User Profile not found","profile_not_found",constants.HTTP_STATUS.NOT_FOUND);
		}
		if(!ValidAuthRequest.check(admin_profile.aid,req.decoded)){
			logger.error({"r":"index","m":"POST","msg":"ValidAuthRequest_check_error","p":req.body});
			return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}
		var gcode_gen_retry_initial_seed = 0;
		GroupCode.getGroupCode(gcode_gen_retry_initial_seed, function(err, group_code) {
			if(err) { 
				logger.error({"r":"index","m":"POST","msg":"group_code_error","p":req.body});
				return sendError(res, err, "server_error");
			}
			var new_group = new Group({
				code   : group_code,
				name   : group_name,
				gpic   : group_pic,
				act    : true,
				sid    : school_id,
				clss   : group_class,
				cpid   : group_initial_admin,
				caid   : admin_profile.aid,
				crole  : admin_profile.role,
				secr   : true
			});
			new_group.save(function(err, new_group){
				if(err) { 
					logger.error({"r":"index","m":"POST","msg":"new_group_could_not_save","p":req.body});
					return sendError(res,err,"server_error");
				}
				var new_group_admin  = new GroupMember({
					gid  : new_group._id,
					pid  : group_initial_admin,
					aid  : admin_profile.aid,
					type : GroupMember.TYPE.ADMIN,
					act  : true,
					pnam : admin_profile.nam.toLowerCase()
				});
				new_group_admin.save(function(err,new_group_admin) {
					if(err) { 
						logger.error({"r":"index","m":"POST","msg":"new_group_admin_could_not_save","p":req.body});
						return sendError(res,err,"server_error");
					}
					removeGroupsAccountHset(group_initial_admin,function(err,isRemoved){
						if(err){
							logger.error({"r":"index","m":"POST","msg":"removeGroupsAccountHset_error","p":req.body});
						}
						sendSuccess(res,{"Group" : new_group, "GroupMember" : new_group_admin});
						AccountInfo.validateVersionForAdminActions(group_initial_admin, function(err, resp){
							if(resp) {
								return GroupNotify.abt_grp_c(new_group._id, group_initial_admin, group_name, group_code, group_pic, new_group.gtyp, new_group_admin.type, time);
							}
						});
					});
				});
			});
		});
	});
});

/*
 * CREATE GROUP with admin only post For ANDROID and WEB
 */
router.post('/index_v16', function(req, res, next) {
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('name',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('gtyp',errorCodes.invalid_parameters[1]).notEmpty().isValidGroupType();
	req.checkBody('gpic',errorCodes.invalid_parameters[1]).optional().isValidURL();
	req.checkBody('s_id',errorCodes.invalid_parameters[1]).optional().notEmpty().isValidMongoId();
	req.checkBody('clss',errorCodes.invalid_parameters[1]).optional().isValidClass();
	req.checkBody('tokn',errorCodes.invalid_parameters[1]).notEmpty().validateToken();

	if(req.validationErrors()) { 
		logger.error({"r":"index_v16","m":"POST","msg":"invalid_parameters","p":req.body});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.INVALID_PARAMETER);
	}

  var time                  = new Date().getTime();
	var group_initial_admin 	= req.body.p_id;

	var group_name 						= req.body.name ? req.body.name.trim() : null;
	if(!group_name){
		logger.error({"r":"index","msg":"Group Name Invalid","p":req.body});
		return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var group_type            = (parseInt(req.body.gtyp) == 1) ? true : false;
	var school_id							= (req.body.s_id) ? req.body.s_id : null;
	var group_class						= (req.body.clss) ? req.body.clss : null;
	var group_pic							= (req.body.gpic) ? req.body.gpic : null;
	
	UserInfoCache.getMongoProfile(group_initial_admin,function(err,admin_profile){
		if(err) { 
			logger.error({"r":"index_v16","m":"POST","msg":"profile_find_server_error","p":req.body});
			return sendError(res,err,"server_error");
		}
		if(!admin_profile) {
			logger.error({"r":"index_v16","m":"POST","msg":"profile_not_found","p":req.body});
			return sendError(res,"User Profile not found","profile_not_found",constants.HTTP_STATUS.NOT_FOUND);
		}
		if(!ValidAuthRequest.check(admin_profile.aid,req.decoded)){
			logger.error({"r":"index_v16","m":"POST","msg":"ValidAuthRequest_check_error","p":req.body});
			return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}
		var gcode_gen_retry_initial_seed = 0;
		GroupCode.getGroupCode(gcode_gen_retry_initial_seed, function(err, group_code) {
			if(err) { 
				logger.error({"r":"index_v16","m":"POST","msg":"group_code_error","p":req.body});
				return sendError(res, err, "server_error");
			}
			var new_group = new Group({
				code   : group_code,
				name   : group_name,
				gpic   : group_pic,
				gtyp   : group_type, 
				act    : true,
				sid    : school_id,
				clss   : group_class,
				cpid   : group_initial_admin,
				caid   : admin_profile.aid,
				crole  : admin_profile.role,
				secr   : true
			});
			new_group.save(function(err, new_group){
				if(err) { 
					logger.error({"r":"index_v16","m":"POST","msg":"new_group_could_not_save","p":req.body});
					return sendError(res,err,"server_error");
				}
				var new_group_admin  = new GroupMember({
					gid : new_group._id,
					pid : group_initial_admin,
					aid : admin_profile.aid,
					type : GroupMember.TYPE.ADMIN,
					act  : true,
					pnam : admin_profile.nam.toLowerCase()
				});
				new_group_admin.save(function(err,new_group_admin) {
					if(err) { 
						logger.error({"r":"index_v16","m":"POST","msg":"new_group_admin_could_not_save","p":req.body});
						return sendError(res,err,"server_error");
					}
					removeGroupsAccountHset(group_initial_admin,function(err,isRemoved){
						if(err){
							logger.warn({"r":"index_v16","m":"POST","msg":"removeGroupsAccountHset_error","p":req.body});
						}
						sendSuccess(res,{"Group" : new_group, "GroupMember" : new_group_admin});
						AccountInfo.validateVersionForAdminActions(group_initial_admin, function(err, resp){
							if(resp) {
								return GroupNotify.abt_grp_c(new_group._id, group_initial_admin, group_name, group_code, group_pic, new_group.gtyp, new_group_admin.type, time);
							}
						});
					});
				});
			});
		});
	});
});

/*
 * CREATE GROUP with admin only post for IOS
 */
router.post('/index_i_v16', function(req, res, next) {
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('name',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('gtyp',errorCodes.invalid_parameters[1]).notEmpty().isValidGroupType();
	req.checkBody('gpic',errorCodes.invalid_parameters[1]).optional().isValidURL();
	req.checkBody('s_id',errorCodes.invalid_parameters[1]).optional().notEmpty().isValidMongoId();
	req.checkBody('clss',errorCodes.invalid_parameters[1]).optional().isValidClass();

	if(req.validationErrors()) { 
		logger.error({"r":"index_i_v16","m":"POST","msg":"invalid_parameters","p":req.body});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.INVALID_PARAMETER);
	}

  var time                  = new Date().getTime();
	var group_initial_admin 	= req.body.p_id;

	var group_name 						= req.body.name ? req.body.name.trim() : null;
	if(!group_name){
		logger.error({"r":"index","msg":"Group Name Invalid","p":req.body});
		return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var group_type            = (parseInt(req.body.gtyp) == 1) ? true : false;
	var school_id							= (req.body.s_id) ? req.body.s_id : null;
	var group_class						= (req.body.clss) ? req.body.clss : null;
	var group_pic							= (req.body.gpic) ? req.body.gpic : null;
	
	UserInfoCache.getMongoProfile(group_initial_admin,function(err,admin_profile){
		if(err) { 
			logger.error({"r":"index_i_v16","m":"POST","msg":"profile_find_server_error","p":req.body});
			return sendError(res,err,"server_error");
		}
		if(!admin_profile) {
			logger.error({"r":"index_i_v16","m":"POST","msg":"profile_not_found","p":req.body});
			return sendError(res,"User Profile not found","profile_not_found",constants.HTTP_STATUS.NOT_FOUND);
		}
		if(!ValidAuthRequest.check(admin_profile.aid,req.decoded)){
			logger.error({"r":"index_i_v16","m":"POST","msg":"ValidAuthRequest_check_error","p":req.body});
			return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}
		var gcode_gen_retry_initial_seed = 0;
		GroupCode.getGroupCode(gcode_gen_retry_initial_seed, function(err, group_code) {
			if(err) { 
				logger.error({"r":"index_i_v16","m":"POST","msg":"group_code_error","p":req.body});
				return sendError(res, err, "server_error");
			}
			var new_group = new Group({
				code   : group_code,
				name   : group_name,
				gpic   : group_pic,
				gtyp   : group_type, 
				act    : true,
				sid    : school_id,
				clss   : group_class,
				cpid   : group_initial_admin,
				caid   : admin_profile.aid,
				crole  : admin_profile.role,
				secr   : true
			});
			new_group.save(function(err, new_group){
				if(err) { 
					logger.error({"r":"index_i_v16","m":"POST","msg":"new_group_could_not_save","p":req.body});
					return sendError(res,err,"server_error");
				}
				var new_group_admin  = new GroupMember({
					gid   : new_group._id,
					pid   : group_initial_admin,
					aid   : admin_profile.aid,
					type  : GroupMember.TYPE.ADMIN,
					act   : true,
					pnam  : admin_profile.nam.toLowerCase()
				});
				new_group_admin.save(function(err,new_group_admin) {
					if(err) { 
						logger.error({"r":"index_i_v16","m":"POST","msg":"new_group_admin_could_not_save","p":req.body});
						return sendError(res,err,"server_error");
					}
					removeGroupsAccountHset(group_initial_admin,function(err,isRemoved){
						if(err){
							logger.warn({"r":"index_i_v16","m":"POST","msg":"removeGroupsAccountHset_error","p":req.body});
						}
						sendSuccess(res,{"Group" : new_group, "GroupMember" : new_group_admin});
						AccountInfo.validateVersionForAdminActions(group_initial_admin, function(err, resp){
							if(resp) {
								return GroupNotify.abt_grp_c(new_group._id, group_initial_admin, group_name, group_code, group_pic, new_group.gtyp, new_group_admin.type, time);
							}
						});
					});
				});
			});
		});
	});
});

/*
 * EDIT GROUP NAME
 */
router.put('/index', function(req, res, next) {
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('name',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('tokn',errorCodes.invalid_parameters[1]).notEmpty().validateToken();

	if(req.validationErrors()) {
		logger.error({"r":"index","m":"PUT","msg":"invalid_parameters","p":req.body}); 
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var profile_id 			= req.body.p_id;
	var group_id 				= req.body.g_id;
	var group_name 			= req.body.name;

	GroupInfoCache.getMongoGroup(group_id,function(err,group){
		if(err) { 
			logger.error({"r":"index","m":"PUT","msg":"group_find_server_error","p":req.body});
			return sendError(res,err,"server_error");
		}
		if(!group) {
			logger.error({"r":"index","m":"PUT","msg":"no_group_found","p":req.body});
			return sendError(res,"Group does not exists.","no_group_found",constants.HTTP_STATUS.NOT_FOUND);
		}
		if(group.secr && !req.decoded){
			secureGroupLogger.error({"r":"index","m":"PUT","msg":"secr_group_not_authorized","p":req.body});
			logger.error({"r":"index","m":"PUT","msg":"secr_group_not_authorized","p":req.body});
			return sendError(res,"not_authorized","not_authorized",constants.HTTP_STATUS.UNAUTHORIZED);
		}
		var old_grp_name = group.name;

		UserInfoCache.getMongoGrpMem(profile_id, group_id, function(err, group_admin){
			if(err) { 
				logger.error({"r":"index","m":"PUT","msg":"groupmember_find_server_error","p":req.body});
				return sendError(res,err,"server_error");
			}
			if(!group_admin) {
				logger.error({"r":"index","m":"PUT","msg":"not a member of the Group","p":req.body});
				return sendError(res,"You are not a member of the Group","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
			}
			if(!ValidAuthRequest.check(group_admin.aid,req.decoded)){
				logger.error({"r":"index","m":"PUT","msg":"ValidAuthRequest_check_error","p":req.body});
				return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
			}
			if(group_admin.type !=GroupMember.TYPE.ADMIN) {
				logger.error({"r":"index","m":"PUT","msg":"not Group Administrator","p":req.body});
				return sendError(res,"You are not Group Administrator","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
			}
			Group.update({
				_id : group_id,
				act : true
			},{
				name : group_name
			},function(err, edit_group) {
				if(err) { 
					logger.error({"r":"index","m":"PUT","msg":"group_update_server_error","p":req.body});
					sendError(res,err,"server_error");
				}
				GroupInfoCache.removeMongoGroup(group_id,function(err, rRemoved){
					if(err){
						logger.warn({"r":"index","m":"PUT","msg":"removeMongoGroup_server_error","p":req.body});
					}
					var time = new Date().getTime();
					sendSuccess(res, {});
					var notify_msg = old_grp_name+' has been renamed to '+group_name;
					GroupNotify.abt_gnam_e(group_id, group_name, group.gtyp, profile_id, notify_msg, Actions.GROUP_NAME_EDIT, time);
				});
			});
		});
	});
});

/*
 * EDIT GROUP PIC
 */
router.put('/gpic_e', function(req, res, next) {
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('gpic',errorCodes.invalid_parameters[1]).optional().isValidURL();
	req.checkBody('tokn',errorCodes.invalid_parameters[1]).notEmpty().validateToken();

	if(req.validationErrors()) { 
		logger.error({"r":"gpic_e","m":"PUT","msg":"invalid_parameters","p":req.body});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var profile_id 		= req.body.p_id;
	var group_id 			= req.body.g_id;
	var group_pic 		= req.body.gpic;

	GroupInfoCache.getMongoGroup(group_id,function(err,group){
		if(err) { 
			logger.error({"r":"gpic_e","m":"PUT","msg":"group_find_server_error","p":req.body});
			return sendError(res,err,"server_error");
		}
		if(!group) {
			logger.error({"r":"gpic_e","m":"PUT","msg":"no_group_found","p":req.body});
			return sendError(res,"Group does not exists.","no_group_found",constants.HTTP_STATUS.NOT_FOUND);
		}
		if(group.secr && !req.decoded){
			secureGroupLogger.error({"r":"gpic_e","m":"PUT","msg":"secr_group_not_authorized","p":req.body});
			logger.error({"r":"gpic_e","m":"PUT","msg":"secr_group_not_authorized","p":req.body});
			return sendError(res,"not_authorized","not_authorized",constants.HTTP_STATUS.UNAUTHORIZED);
		}
		var group_name = group.name;

		UserInfoCache.getMongoGrpMem(profile_id, group_id, function(err, group_admin){
			if(err) { 
				logger.error({"r":"gpic_e","m":"PUT","msg":"groupmember_find_server_error","p":req.body});
				return sendError(res,err,"server_error");
			}
			if(!group_admin) {
				logger.error({"r":"gpic_e","m":"PUT","msg":"not a member of the Group","p":req.body});
				return sendError(res,"You are not a member of the Group","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
			}
			if(!ValidAuthRequest.check(group_admin.aid,req.decoded)){
				logger.error({"r":"gpic_e","m":"PUT","msg":"ValidAuthRequest_check_error","p":req.body});
				return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
			}
			if(group_admin.type != GroupMember.TYPE.ADMIN) {
				logger.error({"r":"gpic_e","m":"PUT","msg":"not Group Administrator","p":req.body});
				return sendError(res,"You are not Group Administrator","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
			}
			Group.update({
				_id : group_id,
				act : true
			},{
				gpic : group_pic,
			},function(err, edit_group) {
				if(err) { 
					logger.error({"r":"gpic_e","m":"PUT","msg":"group_update_server_error","p":req.body});
					return sendError(res,err,"server_error");
				}
				GroupInfoCache.removeMongoGroup(group_id,function(err, rRemoved){
					if(err){
						logger.warn({"r":"gpic_e","m":"PUT","msg":"removeMongoGroup_server_error","p":req.body});
					}
					var time = new Date().getTime();
					sendSuccess(res, {});
					GroupNotify.abt_gpic_e(group_id, group_pic, group_name, group.gtyp, profile_id, Actions.GROUP_PIC_EDIT, time);
				});
			});
		});
	});
});

/*
 * REMOVE GROUP PIC
 */
router.put('/gpic_r', function(req, res, next) {
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('tokn',errorCodes.invalid_parameters[1]).notEmpty().validateToken();

	if(req.validationErrors()) { 
		logger.error({"r":"gpic_r","m":"PUT","msg":"invalid_parameters","p":req.body});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var profile_id 		= req.body.p_id;
	var group_id 		  = req.body.g_id;

	GroupInfoCache.getMongoGroup(group_id,function(err,group){
		if(err) { 
			logger.error({"r":"gpic_r","m":"PUT","msg":"group_find_server_error","p":req.body});
			return sendError(res,err,"server_error");
		}
		if(!group) {
			logger.error({"r":"gpic_r","m":"PUT","msg":"no_group_found","p":req.body});
			return sendError(res,"Group does not exists.","no_group_found",constants.HTTP_STATUS.NOT_FOUND);
		}
		if(group.secr && !req.decoded){
			secureGroupLogger.error({"r":"gpic_r","m":"PUT","msg":"secr_group_not_authorized","p":req.body});
			logger.error({"r":"gpic_r","m":"PUT","msg":"secr_group_not_authorized","p":req.body});
			return sendError(res,"not_authorized","not_authorized",constants.HTTP_STATUS.UNAUTHORIZED);
		}

		var group_name = group.name;

		UserInfoCache.getMongoGrpMem(profile_id, group_id, function(err, group_admin){
			if(err) {
				logger.error({"r":"gpic_r","m":"PUT","msg":"groupmember_find_server_error","p":req.body}); 
				return sendError(res,err,"server_error");
			}
			if(!group_admin) {
				logger.error({"r":"gpic_r","m":"PUT","msg":"not a member of the Group","p":req.body});
				return sendError(res,"You are not a member of the Group","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
			}
			if(!ValidAuthRequest.check(group_admin.aid,req.decoded)){
				logger.error({"r":"gpic_r","m":"PUT","msg":"ValidAuthRequest_check_error","p":req.body});
				return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
			}
			if(group_admin.type != GroupMember.TYPE.ADMIN) {
				logger.error({"r":"gpic_r","m":"PUT","msg":"not Group Administrator","p":req.body});
				return sendError(res,"You are not Group Administrator","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
			}
			Group.update({
				_id : group_id,
				act : true
			},{
				gpic : "",
			},function(err, edit_group) {
				if(err) { 
					logger.error({"r":"gpic_r","m":"PUT","msg":"group_update_server_error","p":req.body});
					return sendError(res,err,"server_error");
				}
				if(!edit_group) {
					logger.error({"r":"gpic_r","m":"PUT","msg":"server_error","p":req.body});
					return sendError(res,"Update Failed","server_error",constants.HTTP_STATUS.SERVER_ERROR);
				}
				GroupInfoCache.removeMongoGroup(group_id,function(err, rRemoved){
					if(err){
						logger.warn({"r":"gpic_r","m":"PUT","msg":"removeMongoGroup_server_error","p":req.body});
					}
					var time = new Date().getTime();
					sendSuccess(res, {});
					GroupNotify.abt_gpic_r(group_id, group_name, group.gtyp, profile_id, Actions.GROUP_PIC_REM, time);
				});
			});
		});
	});
});

/*
 *CHANGE TYPE OF THE GROUP
 */
router.put('/changetype', function(req, res, next) {
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('gtyp',errorCodes.invalid_parameters[1]).notEmpty().isValidBooleanString();
	req.checkBody('tokn',errorCodes.invalid_parameters[1]).notEmpty().validateToken();

	if(req.validationErrors()) { 
		logger.error({"r":"changetype","m":"PUT","msg":"invalid_parameters","p":req.body});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var admin_id 			= req.body.p_id;
	var group_id 			= req.body.g_id;
	var group_type 		= req.body.gtyp;
	if(group_type == 'true') 	group_type = true;
	if(group_type == 'false') 	group_type = false;

	Group.findOne({
		_id : group_id,
		act : true
	}, function(err, group) {
		if(err) { 
			logger.error({"r":"changetype","m":"PUT","msg":"group_find_server_error","p":req.body});
			return sendError(res,err,"server_error");
		}
		if(!group) {
			logger.error({"r":"changetype","m":"PUT","msg":"no_group_found","p":req.body});
			return sendError(res,"No Group Found","no_group_found",constants.HTTP_STATUS.BAD_REQUEST);
		}
		if(group.secr && !req.decoded){
			secureGroupLogger.error({"r":"changetype","m":"PUT","msg":"secr_group_not_authorized","p":req.body});
			logger.error({"r":"changetype","m":"PUT","msg":"secr_group_not_authorized","p":req.body});
			return sendError(res,"not_authorized","not_authorized",constants.HTTP_STATUS.UNAUTHORIZED);
		}
		UserInfoCache.getMongoGrpMem(admin_id, group_id, function(err, admin){
			if(err) { 
				logger.error({"r":"changetype","m":"PUT","msg":"groupmember_find_server_error","p":req.body});
				return sendError(res,err,"server_error");
			}
			if(!admin){
				logger.error({"r":"changetype","m":"PUT","msg":"not a member of the Group","p":req.body});
				return sendError(res,"You are not a member of the Group","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
			}
			if(!ValidAuthRequest.check(admin.aid,req.decoded)){
				logger.error({"r":"changetype","m":"PUT","msg":"ValidAuthRequest_check_error","p":req.body});
				return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
			}
			if(admin.type != GroupMember.TYPE.ADMIN) {
				logger.error({"r":"changetype","m":"PUT","msg":"not Group Administrator","p":req.body});
				return sendError(res,"You are not Group Administrator","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
			}
			if(group.gtyp === group_type) {
				if(group.gtyp === Group.GROUP_TYPE.TWO_WAY) {
					logger.error({"r":"changetype","m":"PUT","msg":"group_already_two_way","p":req.body});
					return sendError(res,"Group is already Two Way.","group_already_two_way",constants.HTTP_STATUS.GROUP_TYPE_ALREADY_EXISTS);
				} else {
					logger.error({"r":"changetype","m":"PUT","msg":"group_already_one_way","p":req.body});
					return sendError(res,"Group is already One Way.","group_already_one_way",constants.HTTP_STATUS.GROUP_TYPE_ALREADY_EXISTS);
				} 
			} else {
				Group.update({
					_id : group_id,
					act : true
				},{
					gtyp : group_type
				},function(err,updated_group) {
					if(err) {
					 	logger.error({"r":"changetype","m":"PUT","msg":"group_update_server_error","p":req.body});
						return sendError(res,err,"server_error");
					}
					GroupInfoCache.removeMongoGroup(group_id,function(err, rRemoved){
						if(err){
							logger.warn({"r":"changetype","m":"PUT","msg":"removeMongoGroup_server_error","p":req.body});
						}
						var time = new Date().getTime();
						sendSuccess(res, {});
						AccountInfo.validateVersionForAdminActions(admin_id, function(err,resp){
							if (err) { return; }
							if (!resp) { console.trace('Admin is having mobile version lesser than CI Version'); return; }
							GroupNotify.abt_gtyp_e(group_id, group_type, admin_id, admin.type, group.name, time);
						});
					});
				});					
			}
		});
	});
});


/*
 * DELETE GROUP
 */
router.post('/grp_d', function(req, res, next){
	if(!req.decoded || !req.decoded.id){
    logger.error({"r":"grp_d","m":"POST","msg":"not_authorised_GrpDel_without_valid_req_decoded","p":req.body,"dcd":req.decoded});
    return sendError(res,"not_authorised","not_authorised",constants.HTTP_STATUS.UNAUTHORIZED);
  }
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();

	if(req.validationErrors()) { 
		logger.error({"r":"grp_d","m":"PUT","msg":"invalid_parameters","p":req.body});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var profile_id 			= req.body.p_id;
	var group_id 				= req.body.g_id;

	Group.findOne({
		_id : group_id
	},{
		name: 1,
		act : 1
	},function(err,group){
		if(err){ 
			logger.error({"r":"grp_d","m":"POST","msg":"group_find_server_error","p":req.body});
			return sendError(res,err,"server_error");
		}
		if(!group) {
			logger.error({"r":"grp_d","m":"POST","msg":"no_group_found","p":req.body});
			return sendError(res,"No Group Found","no_group_found",constants.HTTP_STATUS.BAD_REQUEST);
		}
		GroupMember.findOne({
			gid : group_id,
			pid : profile_id,
			act : true
		},function(err,to_delete_groupmember){
			if(err) { 
				logger.error({"r":"grp_d","m":"POST","msg":"groupmember_find_if_deleted_group_check","p":req.body});
				return sendError(res,err,"server_error");
			}
			if(!to_delete_groupmember){
				logger.error({"r":"grp_d","m":"POST","msg":"were_not_groupmember","p":req.body});
				return sendError(res,"You Were Not a Groupmember","were_not_groupmember",constants.HTTP_STATUS.BAD_REQUEST);
			}
			if(req.decoded.id != to_delete_groupmember.aid){
				logger.error({"r":"grp_d","m":"POST","msg":"Aid_Mismatch_from_Token_and_groupmemberAID","p":req.body});
				return sendError(res,"authentication_error","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
			}
			if(to_delete_groupmember.type != GroupMember.TYPE.ADMIN) {
				logger.error({"r":"grp_d","m":"POST","msg":"not Group Administrator","p":req.body});
				return sendError(res,"You are not Group Administrator","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
			}
			if(group.act == false){
				logger.warn({"r":"grp_d","m":"POST","msg":"group_already_deleted","p":req.body});
				return sendSuccess(res, {});
			}
			GroupMember.findOne({
				gid : group_id,
				pid : {$ne : profile_id},
				act : true,
				gdel: false
			},function(err,other_member){
				if(err) { 
					logger.error({"r":"grp_d","m":"POST","msg":"groupmember_count_server_error","p":req.body});
					return sendError(res,err,"server_error");
				}
				if(other_member){
					logger.error({"r":"grp_d","m":"POST","msg":"Group has Alive Members in it","p":req.body});
					return sendError(res,"Group Still has Alive Members.","alive_groupmembers_present",constants.HTTP_STATUS.BAD_REQUEST);
				}
				Group.update({
					_id : group_id,
				},{
					act : false
				},function(err, delete_group) {
					if(err){ 
						logger.error({"r":"grp_d","m":"POST","msg":"group_update_server_error","p":req.body,"er":err});
						return sendError(res,"There was some issue with Network","server_error",constants.HTTP_STATUS.BAD_REQUEST);
					}
					GroupMember.update({
						gid : group_id,
					},{
						gdel : true
					},{
						multi : true
					},function(err,gms_deleted){
						if(err){
							logger.error({"r":"grp_d","m":"POST","msg":"groupmember_update_server_error","p":req.body,"er":err}); 
							return sendError(res,"There was some issue with Network","server_error",constants.HTTP_STATUS.BAD_REQUEST);
						}
						removeGroupDeletionCache(group_id, profile_id, to_delete_groupmember.aid, function(err, msg){
							if(err){
								logger.warn({"r":"grp_d","m":"POST","msg":"removeGroupDeletionCache_error","p":req.body,"er":err});
							}
							var time       = new Date().getTime();
							GroupNotify.abt_grp_d(profile_id, group_id, group.name, time, function(err,resp){
								if(err){
									logger.warn({"r":"grp_d","m":"POST","msg":"GroupNotify_abt_grp_d_error","p":req.body,"er":err});
								}
								return sendSuccess(res, {});
							});
						});
					});
				});
			});
		});
	});
});

// Mute my group for any push notification
router.post('/mutemygrp', function(req, res, next) {
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('typ',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()){ 
		logger.error({"r":"mutemygrp","msg":"invalid_parameters","p":req.body});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var group_id 		= req.body.g_id;
	var profile_id  = req.body.p_id;
	var duration 		= req.body.typ;
	var time        = new Date().getTime();

	var POSSIBLE_MUTE_TYPES = [GroupMember.MUTE_TYPE.SHORT,GroupMember.MUTE_TYPE.MEDIUM,GroupMember.MUTE_TYPE.LONG];

	if(POSSIBLE_MUTE_TYPES.indexOf(duration) < 0){
		logger.error({"r":"mutemygrp","msg":"POSSIBLE_MUTE_TYPES_check_error","p":req.body});
		return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var mute_time = time + (GroupMember.MUTE_TIMING[duration]);

	UserInfoCache.getMongoGrpMem(profile_id, group_id, function(err, groupmember){
		if(err) { 
			logger.error({"r":"mutemygrp","msg":"getMongoGrpMem_DbError","p":req.body});
			return sendError(res,err,"server_error"); 
		}
		if(!groupmember){
			logger.error({"r":"mutemygrp","msg":"not_group_member","p":req.body});
			return sendError(res,"Profile is not member of the group.","not_a_member",constants.HTTP_STATUS.BAD_REQUEST);
		}
		if(groupmember.aid != req.decoded.id){
			logger.error({"r":"mutemygrp","msg":"token_id_mismatch","p":req.body});
			return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}
		if(groupmember.type == GroupMember.TYPE.BANNED){
			logger.error({"r":"mutemygrp","msg":"banned_group_member","p":req.body});
			return sendError(res,"Banned group member cannot see group members","banned_group_member",constants.HTTP_STATUS.BAD_REQUEST);
		}
		GroupMember.update({
			gid : group_id,
			aid : groupmember.aid,
			act : true,
			gdel: false
		},{
			muttp: duration,
			muttm: mute_time
		},{
			multi : true
		},function(err,updated_mute){
			if(err) { 
				logger.error({"r":"mutemygrp","msg":"Groupmember_DbError","p":req.body});
				return sendError(res,err,"server_error"); 
			}
			AccountDataInfoCache.removeGroupAccountDatas(group_id, function(err, resp){
				if(err){
					logger.warn({"r":"mutemygrp","msg":"AccountDataInfoCache_removeGroupAccountDatas_error","p":req.body,"er":err});
				}
				return sendSuccess(res,{});
			});
		});
	});
});

// Un Mute my group for any push notification
router.post('/unmutemygrp', function(req, res, next) {
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();

	if(req.validationErrors()){ 
		logger.error({"r":"unmutemygrp","msg":"invalid_parameters","p":req.body});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var group_id 		= req.body.g_id;
	var profile_id  = req.body.p_id;

	UserInfoCache.getMongoGrpMem(profile_id, group_id, function(err, groupmember){
		if(err) { 
			logger.error({"r":"unmutemygrp","msg":"getMongoGrpMem_DbError","p":req.body});
			return sendError(res,err,"server_error"); 
		}
		if(!groupmember){
			logger.error({"r":"unmutemygrp","msg":"not_group_member","p":req.body});
			return sendError(res,"Profile is not member of the group.","not_a_member",constants.HTTP_STATUS.BAD_REQUEST);
		}
		if(groupmember.aid != req.decoded.id){
			logger.error({"r":"unmutemygrp","msg":"token_id_mismatch","p":req.body});
			return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}
		if(groupmember.type == GroupMember.TYPE.BANNED){
			logger.error({"r":"unmutemygrp","msg":"banned_group_member","p":req.body});
			return sendError(res,"Banned group member cannot see group members","banned_group_member",constants.HTTP_STATUS.BAD_REQUEST);
		}
		GroupMember.update({
			gid : group_id,
			aid : groupmember.aid,
			act : true,
			gdel: false
		},{
			muttp: "",
			muttm: 0
		},{
			multi : true
		},function(err,updated_mute){
			if(err) { 
				logger.error({"r":"unmutemygrp","msg":"Groupmember_DbError","p":req.body});
				return sendError(res,err,"server_error"); 
			}
			AccountDataInfoCache.removeGroupAccountDatas(group_id, function(err, resp){
				if(err){
					logger.warn({"r":"unmutemygrp","msg":"AccountDataInfoCache_removeGroupAccountDatas_error","p":req.body,"er":err});
				}
				return sendSuccess(res,{});
			});
		});
	});
});


/*
 * Groups to recomment to the User
 */
// router.get('/g_rcmd', function(req, res, next){
// 	req.checkQuery('a_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
// 	req.checkQuery('cl',errorCodes.invalid_parameters[1]).notEmpty().isValidClient();
// 	req.checkQuery('vrsn',errorCodes.invalid_parameters[1]).notEmpty().isValidVersion();
// 	req.checkQuery('start',errorCodes.invalid_parameters[1]).notEmpty();
// 	req.checkQuery('ipp',errorCodes.invalid_parameters[1]).notEmpty().isValidRcmdIPP();
// 	req.checkQuery('tokn',errorCodes.invalid_parameters[1]).notEmpty().validateToken();

// 	if(req.validationErrors()) { sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST); return; }

// 	var account_id   = req.query.a_id;
// 	var client       = req.query.cl;
// 	var app_version  = req.query.vrsn;
// 	var start 			 = parseInt(req.query.start);
// 	var ipp 				 = configs.RECOMMENDATION_GROUPS_LENGTH_LIMIT;

// 	var condition = {
// 		_id : account_id
// 	};
// 	Account.findOne(condition,function(err,acc_data){
// 		if(err) { 
// 			console.trace(err);
// 			return sendError(res,err,"server_error");
// 		}
// 		if(!acc_data){
// 			console.log('Account Data for RCMD Groups fetching is invalid , aid : '+account_id,+', client :'+client+', app_version : '+app_version);
// 			return sendError(res,"Invalid Account Data","invalid_account_data",constants.HTTP_STATUS.BAD_REQUEST); 
// 		}
// 		if(!ValidAuthRequest.check(account_id,req.decoded)){
// 			logger.error({"r":"g_rcmd","m":"PUT","msg":"ValidAuthRequest_check_error","p":req.query});
// 			return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
// 		}
// 		var user_mobile_region = getUserMobileRegion(acc_data.m);
		
// 		RecommendedGroup.distinct('gid',{
// 			catg : { $in : user_mobile_region}
// 		},function(err,rgids){
// 			if(err) { 
// 				console.trace(err);
// 				return sendError(res,err,"server_error");
// 			}
// 			if(rgids.length == 0){
// 				console.log('Oops, No Recommended Groups found for Account id : '+account_id+', client :'+client);
// 				return sendSuccess(res,{total : 0, rcmd_grps : []});
// 			}
// 			return prepResultForRcmdGrps(res, rgids, account_id, start, ipp);
// 		});
// 	});
// });

/*
 * Groups to recommend to the User
 */
router.get('/g_rcmd', function(req, res, next){
	req.checkQuery('a_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkQuery('cl',errorCodes.invalid_parameters[1]).notEmpty().isValidClient();
	req.checkQuery('vrsn',errorCodes.invalid_parameters[1]).notEmpty().isValidVersion();
	req.checkQuery('start',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkQuery('ipp',errorCodes.invalid_parameters[1]).notEmpty().isValidRcmdIPP();
	req.checkQuery('tokn',errorCodes.invalid_parameters[1]).notEmpty().validateToken();

	if(req.validationErrors()) { sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST); return; }

	var account_id   = req.query.a_id;
	var client       = req.query.cl;
	var app_version  = req.query.vrsn;
	var start 			 = parseInt(req.query.start);
	var ipp 				 = configs.RECOMMENDATION_GROUPS_LENGTH_LIMIT;

	rmc.queryById("Account",account_id,function(err, account){
		if(err){
			logger.error({"r":"g_rcmd","msg":"rmc_queryById_Account_error","p":req.query,"err":err});
			return sendError(res,"server_error","server_error");
		}
		if(!account){
			logger.error({"r":"g_rcmd","msg":"account_not_found","p":req.query});
			return sendError(res,"Invalid Account Data","invalid_account_data",constants.HTTP_STATUS.BAD_REQUEST);
		}
		GroupMember.find({
			aid : account_id,
			act : true,
			gdel: false
		},{
			gid : 1,
			cat : 1,
			_id : 0
		},{
			sort : {
				cat : -1
			},
			limit : configs.RECOMMENDATION_DEPTH
		},function(err,groupmembers){
			if(err){
				logger.error({"r":"g_rcmd","msg":"GroupMemberFind_error","p":req.query,"err":err});
				return sendError(res,"server_error","server_error");
			}
			if(groupmembers.length == 0){
				return recommedationFornewJoinee(res, req.query, start, ipp);
			}
			return recommendationForExistingUsers(res, req.query, account_id, groupmembers, start, ipp);
		});
	});
});

// get group join padi packages if group is paid for join
router.post('/g_packs', function(req, res, next) {
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();

	if(req.validationErrors()){ 
		logger.error({"r":"g_packs","msg":"invalid_parameters","p":req.body});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var group_id 		= req.body.g_id;
	var profile_id  = req.body.p_id;
	var tim         = new Date().getTime();

	Group.findOne({
		_id : group_id,
		act : true
	},function(err,group){
		if(err){
			logger.error({"r":"g_packs","msg":"GroupMemberFind_error","p":req.body,"err":err});
			return sendError(res,"server_error","server_error");
		}
		if(!group){
			logger.error({"r":"g_packs","m":"POST","msg":"no_group_found","p":req.body});
			return sendError(res,"No Group Found","no_group_found",constants.HTTP_STATUS.BAD_REQUEST);
		}
		GroupMember.findOne({
			gid : group_id,
			pid : profile_id,
			act : true,
			gdel: false
		},function(err,groupmember){
			if(err){
				logger.error({"r":"g_packs","msg":"GroupMemberFind_error","p":req.body,"err":err});
				return sendError(res,"server_error","server_error");
			}
			if(!groupmember){
				logger.error({"r":"g_packs","msg":"not_a_group_member","p":req.body});
				return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
			}
			if(!group.req_sub || group.req_sub == false || !group.plugin_id){
				logger.error({"r":"g_packs","msg":"group_not_paid","p":req.body});
				return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
			}
			GroupPlugin.findOne({
				gid    : group_id,
				act    : true,
				plgnid : group.plugin_id,
				type   : GroupPlugin.TYPE.GROUP_JOIN,
				status : GroupPlugin.STATUS.ACTIVE
			},function(err,group_plugin){
				if(err){
					logger.error({"r":"g_packs","msg":"GroupPluginFind_error","p":req.body,"err":err});
					return sendError(res,"server_error","server_error");
				}
				if(!group_plugin){
					logger.error({"r":"g_packs","msg":"group_plugin_type_not_group_join","p":req.body});
					return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
				}
				var valid_subscription = false;
				if(groupmember.sub_ed && groupmember.sub_ed > tim){
					valid_subscription = true;
				}
				GroupPluginPackage.find({
					gid       : group_id,
					plugin_id : group.plugin_id,
					act       : true
				},{
					plugin_id        : 1,
					package_name     : 1,
					package_desc     : 1,
					package_price    : 1,
					package_duration : 1,
					duration_unit    : 1,
					duration_base    : 1,
					acc              : 1
				},function(err,plugin_packages){
					if(err){
						logger.error({"r":"g_packs","msg":"GroupPluginPackageFind_error","p":req.body,"err":err});
						return sendError(res,"server_error","server_error");
					}
					if(!plugin_packages){
						logger.error({"r":"g_packs","msg":"group_packages_not_found","p":req.body});
						return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
					}
					logger.info({"r":"g_packs","msg":"packges_Success","ispaid":group.req_sub,"packs":plugin_packages,v_subr:valid_subscription});
					return sendSuccess(res, {"Group":group,"GroupMember":groupmember,ispaid:group.req_sub,packs:plugin_packages,CC_DC:1,v_subr:valid_subscription});
				});
			});
		});
	});
});

function recommendationForExistingUsers(res, params, aid, groupmembers, start, ipp){
	var gids = [];
	for(var i=0; i<groupmembers.length; i++){
		gids.push(groupmembers[i].gid+'');
	}
	GroupCategory.find({
		gid : { $in : gids },
		act : true
	},{
		catgid : 1,
		_id    : 0
	},function(err,usercategories){
		if(err){
			logger.error({"r":"g_rcmd","msg":"recommendationForExistingUsers_GroupCategoryFindFirst","p":params,"err":err});
			return sendError(res,"server_error","server_error");
		}
		var total = usercategories.length;
		if(total == 0){
			return recommedationFornewJoinee(res, params, start, ipp);
		}
		var comparision = {};
		var maxId = '',maxVal = 0;
		for(var i=0; i<total; i++){
			comparision[usercategories[i].catgid] = (comparision[usercategories[i].catgid]) ? comparision[usercategories[i].catgid]+1 : 1;
			if(comparision[usercategories[i].catgid] > maxVal){
				maxVal = comparision[usercategories[i].catgid];
				maxId  = usercategories[i].catgid;
			}
		}
		GroupCategory.count({
			catgid : maxId,
			act : true
		},function(err,count){
			if(err){
				logger.error({"r":"g_rcmd","msg":"recommendationForExistingUsers_GroupCategoryCount","p":params,"err":err,"maxVal":maxVal,"maxId":maxId});
				return sendError(res,"server_error","server_error");
			}
			if(count <= start){
				logger.info({"r":"g_rcmd","msg":"recommendationForExistingUsers_start_more_than_count","p":params,"maxVal":maxVal,"maxId":maxId});
				return sendSuccess(res,{total : count, rcmd_grps : []});
			}
			GroupCategory.find({
				catgid : maxId,
				act    : true
			},{
				gid : 1,
				_id : 0
			},{
				skip : start,
				limit: ipp
			},function(err,targetgroups){
				if(err){
					logger.error({"r":"g_rcmd","msg":"recommendationForExistingUsers_GroupCategoryFindSecond","p":params,"err":err,"maxVal":maxVal,"maxId":maxId});
					return sendError(res,"server_error","server_error");
				}
				var ftotal = targetgroups.length;
				var fgids = [];
				if(ftotal == 0){
					logger.info({"r":"g_rcmd","msg":"recommendationForExistingUsers_no_target_groups","p":params,"maxVal":maxVal,"maxId":maxId});
					return sendSuccess(res,{total : 0, rcmd_grps : []});
				}
				for(var i=0; i<ftotal; i++){
					fgids.push(targetgroups[i].gid+'');
				}
				Group.find({
					_id : { $in : fgids },
					act : true
				},function(err,groups){
					if(err){ 
						logger.error({"r":"g_rcmd","msg":"recommendationForExistingUsers_GroupFind_Error","p":params,"err":err,"maxVal":maxVal,"maxId":maxId});
						return sendError(res,"server_error","server_error");
					}
					logger.info({"r":"g_rcmd","msg":"recommendationForExistingUsers","p":params,"resp_gids":fgids,"maxVal":maxVal,"maxId":maxId});
					return sendSuccess(res,{total : count, rcmd_grps : groups});
				});
			});
		});
	});
}

function recommedationFornewJoinee(res, params, start, ipp){
	GroupCategory.count({
		catgnm : Category.MAJOR_TYPE.NEW_JOINEE,
		act    : true
	},function(err,count){
		if(err){
			logger.error({"r":"g_rcmd","msg":"recommedationFornewJoinee_GroupCategoryCount","p":params,"err":err});
			return sendError(res,"server_error","server_error");
		}
		if(count <= start){
			return sendSuccess(res,{total : count, rcmd_grps : []});
		}
		GroupCategory.find({
			catgnm : Category.MAJOR_TYPE.NEW_JOINEE,
			act    : true
		},{
			gid : 1,
			_id : 0
		},{
			skip : start,
			limit: ipp
		},function(err,groupcategories){
			if(err){
				logger.error({"r":"g_rcmd","msg":"recommedationFornewJoinee","p":params,"err":err});
				return sendError(res,"server_error","server_error");
			}
			var gids = [];
			for(var i=0; i<groupcategories.length; i++){
				gids.push(groupcategories[i].gid);
			}
			Group.find({
				_id : { $in : gids },
				act : true
			},function(err,groups){
				if(err){ 
					logger.error({"r":"g_rcmd","msg":"recommedationFornewJoinee_GroupFind_Error","p":params,"err":err});
					return sendError(res,"server_error","server_error");
				}
				logger.info({"r":"g_rcmd","msg":"recommedationFornewJoinee","p":params,"resp_gids":gids});
				return sendSuccess(res,{total : count, rcmd_grps : groups});
			});
		});
	});
}


function removeGroupDeletionCache(group_id, profile_id, account_id, cb){
	var toDo = 4;
	var done = 0;
	GroupInfoCache.removeMongoGroup(group_id, function(err, rRemoved){
		if(err){
			return cb(err,false);
		}
		done++;
		if(done == toDo){
			return cb(null,true);
		}
	});
	GroupInfoCache.removeGroupAdminsCache(group_id, function(err, rRemoved){
		if(err){
			return cb(err,false);
		}
		done++;
		if(done == toDo){
			return cb(null,true);
		}
	});
	UserInfoCache.removeMongoGrpMem(profile_id, group_id,function(err, rRemoved){
		if(err){
			return cb(err,false);
		}
		done++;
		if(done == toDo){
			return cb(null,true);
		}
	});
	UserInfoCache.rmUsrGrpMemsByAidInCache(account_id, function(err, rRemoved){
		if(err){
			return cb(err,false);
		}
		done++;
		if(done == toDo){
			return cb(null,true);
		}
	});
}

function prepResultForRcmdGrps(res, rgids, account_id, start, ipp){
	var rcmd_grps_count = 0;
	var done 						= 0;
	var total 					= rgids.length;
	var filterGrps 			= [];
	Profile.distinct('_id',{
		aid : account_id,
		act : true
	},function(err,pids){
		if(err) { 
			console.trace(err);
			return sendError(res,err,"server_error");
		}
		for(var j=0; j<total; j++){
			checkIfUserIsGrpMem(rgids[j],pids,function(err,resp){
				if(err){
					return sendError(res,err,"server_error");
				}
				done++;
				if(!resp.isMem){
					filterGrps.push(resp.gid);
				}
				if(done == total){
					rcmd_grps_count = filterGrps.length;
					if(rcmd_grps_count == 0 || start > rcmd_grps_count){
						return sendSuccess(res,{total : rcmd_grps_count, rcmd_grps : []});
					}
					var finalgids = filterGrps.splice(start,ipp);
					Group.find({
						_id : { $in : finalgids },
						act : true
					},function(err,groups){
						if(err) { 
							console.trace(err);
							return sendError(res,err,"server_error");
						}
						logger.info({"_FUNCTION_":"prepResultForRcmdGrps","aid":account_id,"resp":finalgids});
						return sendSuccess(res,{total : rcmd_grps_count, rcmd_grps : groups});
					});
				}
			});
		}
	});		
}

function checkIfUserIsGrpMem(gid, pids, cb){
	var isMem = false;
	GroupMember.findOne({
		gid : gid,
		pid : { $in : pids },
		act : true,
		gdel: false
	},function(err,grpmem){
		if(err){
			console.trace(err);
			return cb(true,null);
		}
		if(grpmem){
			isMem = true;
		}
		return cb(null,{gid : gid, isMem : isMem});
	});
}

function getUserMobileRegion(mobile){
	var first_five_digits = mobile.substring(0,4);
	var region = MobileNumberRegions[first_five_digits];
	if(region == 'PB' || region == 'CH'){
		return [RecommendedGroup.CATEGORY.PB,RecommendedGroup.CATEGORY.CH,RecommendedGroup.CATEGORY.PUBLIC];
	} else if(region == 'DL'){
		return [RecommendedGroup.CATEGORY.DL,RecommendedGroup.CATEGORY.PUBLIC];
	} else {
		return [RecommendedGroup.CATEGORY.OTHERS,RecommendedGroup.CATEGORY.PUBLIC];
	}
}

function removeGroupsAccountHset(pid, cb){
	UserInfoCache.getMongoProfile(pid, function(err,profile){
		if(err){
			return cb(err,null);
		}
		var key = RedisPrefixes.USER_PROF_GMS+profile.aid+'';
		rhmset.keyexists(key,UserRedisKeys.GROUPS,function(err,isKey){
			if(err){
				return cb(err,null);
			}
			if(isKey){
				rhmset.removefield(key,UserRedisKeys.GROUPS,function(err,resp){
					if(err){
						return cb(err,null);
					} 
					if(resp){
						return cb(null,true);
					} else {
						console.log("Oops,this is unfortunate that Account Profiles key could not be deleted for Account : "+profile.aid);
						return cb(null,false);
					}
				});
			} else {
				console.log("Oops,this is unfortunate that Account Profiles key does not exists for Account : "+profile.aid);
				return cb(null,false);
			}
		});
	});
}

module.exports = router;