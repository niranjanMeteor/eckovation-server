const express 									= require('express');
const mongoose 									= require('mongoose');
const redis           					= require('redis');
const jwt    							      = require('jsonwebtoken');

const constants									= require('../utility/constants.js');
const configs				  					= require('../utility/configs.js');
const errorCodes 								= require('../utility/errors.js');
const helpers										= require('../utility/helpers.js');
const GroupNotify 							= require('../utility/grp_notify.js');
const UserInfoCache   					= require('../utility/user_info_cache.js');
const GroupInfoCache       			= require('../utility/group_info_cache.js');
const log4jsLogger  						= require('../loggers/log4js_module.js');
const log4jsConfigs  			  		= require('../loggers/log4js_configs.js');
const PluginToken               = require('../security/plugintokens/auth_tokens.js');

const redis_client              = redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
  console.log("Error " + err);
});

require('../models/accounts.js');
require('../models/profiles.js');
require('../models/groups.js');
require('../models/groupmembers.js');
require('../models/plugins.js');
require('../models/pluginclients.js');
require('../models/groupplugins.js');
require('../models/pluginidentifiers.js');
require('../models/subscriptions/usersubscriptions.js');
require('../models/packages/grouppluginpackages.js');

const Account 									= mongoose.model('Account');
const Profile 									= mongoose.model('Profile'); 
const Group 				  					= mongoose.model('Group'); 
const GroupMember 							= mongoose.model('GroupMember');
const AppPlugin       					= mongoose.model('AppPlugin');
const GroupPlugin       				= mongoose.model('GroupPlugin');
const PluginClient       				= mongoose.model('PluginClient');
const PluginIdentifier      		= mongoose.model('PluginIdentifier');
const UserSubscription 					= mongoose.model('UserSubscription');
const GroupPluginPackage 				= mongoose.model('GroupPluginPackage');

const rmc                   		= require("../utility/redis_mongo_cache.js")(redis_client);

const sendError 								= helpers.sendError;
const sendSuccess 							= helpers.sendSuccess;
const router 										= express.Router();
const logger        						= log4jsLogger.getLogger(log4jsConfigs.CATEGORY_ROUTES_PLUGIN);
const PLUGIN_CATEGORY_NAMES 	  = getPluginCategoryNames();

router.post('/verify_usr_idnt', function(req, res, next) {
	req.checkBody('client_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('client_sec',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('identifier',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()){ 
		logger.error({"r":"v_usr_idnt","msg":"invalid_parameters","p":req.body});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var client_id       = req.body.client_id;
	var client_secret   = req.body.client_sec;
	var user_identifier = req.body.identifier;

	PluginClient.findOne({
		client_id : client_id,
		client_sec: client_secret,
	},function(err,plugin_client){
		if(err){
			logger.error({"r":"v_usr_idnt","msg":"PluginClient_find_server_error","p":req.body,"er":err});
			return sendError(res,"server_error","server_error",constants.HTTP_STATUS.SERVER_ERROR);
		}
		if(!plugin_client){
			logger.error({"r":"v_usr_idnt","msg":"plugin_client_not_found","p":req.body});
			return sendError(res,"authentication_error","authentication_error",constants.HTTP_STATUS.UNAUTHORIZED);
		}
		if(plugin_client.revk){
			logger.error({"r":"v_usr_idnt","msg":"plugin_client_not_found","p":req.body});
			return sendError(res,"forbidden","forbidden",constants.HTTP_STATUS.FORBIDDEN);
		}
		PluginIdentifier.findOne({
			identifier: user_identifier,
			act       : true
		},function(err,pluginidentifier){
			if(err) { 
				logger.error({"r":"v_usr_idnt","p":req.body,"er":err,"msg":"PluginIdentifier_find_error"});
				return sendError(res,"server_error","server_error");
			}
			if(!pluginidentifier){
				logger.error({"r":"v_usr_idnt","msg":"PluginIdentifier_not_found","p":req.body});
	      return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
			}
			UserInfoCache.getMongoProfile(pluginidentifier.pid,function(err, profile){
				if(err){ 
					logger.error({"r":"v_usr_idnt","msg":"getMongoProfile_error","p":req.body,"er":err});
					return sendError(res,err,"server_error"); 
				}
				if(!profile){
					logger.error({"r":"v_usr_idnt","msg":"profile_not_found_from_identifier","p":req.body});
					return sendError(res,"server_error","server_error",constants.HTTP_STATUS.SERVER_ERROR);
				}
				GroupInfoCache.getMongoGroup(pluginidentifier.gid,function(err,group){
					if(err) { 
						logger.error({"r":"v_usr_idnt","p":req.body,"er":err});
						return sendError(res,"server_error","server_error");
					}
					if(!group){
						logger.error({"r":"v_usr_idnt","msg":"no_group_found","p":req.body});
						return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
					}					
					UserInfoCache.getMongoGrpMem(pluginidentifier.pid, pluginidentifier.gid, function(err, groupmember){
						if(err) { 
							logger.error({"r":"v_usr_idnt","p":req.body,"er":err});
							return sendError(res,"server_error","server_error");
						}
						if(!groupmember){
							logger.error({"r":"v_usr_idnt","msg":"no_group_member_found","p":req.body});
							return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
						}	
						var is_admin = (groupmember.type == GroupMember.TYPE.ADMIN) ? true : false;
						return sendSuccess(res,{
							name     : profile.nam,
							ppic     : profile.ppic,
							role     : profile.role,
							srv_id   : pluginidentifier.pid,
							gid      : group.code,
							is_admin : is_admin,
							plugin_id: pluginidentifier.plugin_id
						});
					});
				});
			});
		});
	});
});

router.post('/check_adm', function(req, res, next) {
	req.checkBody('client_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('client_sec',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkBody('identifier',errorCodes.invalid_parameters[1]).notEmpty();

	if(req.validationErrors()){ 
		logger.error({"r":"check_adm","msg":"invalid_parameters","p":req.body});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var client_id       = req.body.client_id;
	var client_secret   = req.body.client_sec;
	var user_identifier = req.body.identifier;

	PluginClient.findOne({
		client_id : client_id,
		client_sec: client_secret
	},function(err,plugin_client){
		if(err){
			logger.error({"r":"check_adm","msg":"PluginClient_find_server_error","p":req.body,"er":err});
			return sendError(res,"server_error","server_error",constants.HTTP_STATUS.SERVER_ERROR);
		}
		if(!plugin_client){
			logger.error({"r":"check_adm","msg":"plugin_client_not_found","p":req.body});
			return sendError(res,"authentication_error","authentication_error",constants.HTTP_STATUS.UNAUTHORIZED);
		}
		if(plugin_client.revk){
			logger.error({"r":"check_adm","msg":"plugin_client_not_found","p":req.body});
			return sendError(res,"forbidden","forbidden",constants.HTTP_STATUS.FORBIDDEN);
		}
		PluginIdentifier.findOne({
			identifier: user_identifier,
			act       : true
		},function(err,pluginidentifier){
			if(err) { 
				logger.error({"r":"check_adm","p":req.body,"er":err,"msg":"PluginIdentifier_find_error"});
				return sendError(res,"server_error","server_error");
			}
			if(!pluginidentifier){
				logger.error({"r":"check_adm","msg":"PluginIdentifier_not_found","p":req.body});
	      return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
			}
			UserInfoCache.getMongoGrpMem(pluginidentifier.pid, pluginidentifier.gid, function(err, groupmember){
				if(err) { 
					logger.error({"r":"check_adm","p":req.body,"er":err});
					return sendError(res,"server_error","server_error");
				}
				if(!groupmember){
					logger.error({"r":"check_adm","msg":"no_group_member_found","p":req.body});
					return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
				}	
				var is_admin = (groupmember.type == GroupMember.TYPE.ADMIN) ? true : false;
				return sendSuccess(res,{is_admin:is_admin, plugin_id:pluginidentifier.plugin_id});
			});
		});
	});
});

//route middleware to verify the jwt token for all users
router.use(function(req, res, next){
	var token = req.body.tokn;
	if(!token){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"token_not_found","p":req.body});
	  return sendError(res,"Access without token is not authorised","token_not_found",constants.HTTP_STATUS.FORBIDDEN);
	}
  jwt.verify(token, configs.JWT_ACCESS_TOKEN_PRIVATE_KEY, function(err, decoded) {      
    if(err){
    	logger.error({"url":req.originalUrl,"r":"auth","msg":"jwt_verify_error","p":req.body,"er":err});
      return sendError(res,"Failed to authenticate token.","token_auth_failure",constants.HTTP_STATUS.TOKEN_EXPIRED);  
    }
  	req.decoded = decoded;
    next();
  });
});

router.post('/g_pl', function(req, res, next) {
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();

	if(req.validationErrors()){ 
		logger.error({"r":"g_pl","msg":"invalid_parameters","p":req.body});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var group_id 				= req.body.g_id;
	var profile_id  		= req.body.p_id;
	var tim             = new Date().getTime();

	UserInfoCache.getMongoGrpMem(profile_id, group_id, function(err, groupmember){
		if(err) { 
			logger.error({"r":"g_pl","msg":"getMongoGrpMem_DbError","p":req.body,"er":err});
			return sendError(res,"server_error","server_error"); 
		}
		if(!groupmember){
			logger.error({"r":"g_pl","msg":"not_group_member","p":req.body});
			return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}
		if(req.decoded.id != groupmember.aid){
			logger.error({"r":"g_pl","msg":"aid_in_token_not_matching_with_pid_account","p":req.body});
			return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}
		GroupInfoCache.getMongoGroup(group_id,function(err,group){
			if(err) { 
				logger.error({"r":"g_pl","p":req.body,"er":err});
				return sendError(res,"server_error","server_error");
			}
			if(!group){
				logger.error({"r":"g_pl","msg":"no_group_found","p":req.body});
				return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
			}
			if(group.req_sub && group.req_sub ==  true){ 
				if(!groupmember.sub_ed && !groupmember.sub_st){
					logger.error({"r":"g_pl","msg":"user_subscription_required","p":req.body});
					return sendError(res,"You need to subscribe to enable your access.","user_subscription_required",constants.HTTP_STATUS.USER_SUBSCRIPTION_REQUIRED);
				}
				if(groupmember.sub_ed < tim){
					logger.error({"r":"g_pl","msg":"user_subscription_ended","p":req.body});
					return sendError(res,"Oops, your subscription has expired. Kindly renew your subscription.","user_subscription_ended",constants.HTTP_STATUS.USER_SUBSCRIPTION_EXPIRED);
				}
			}
			if(!group.plgn){
				logger.error({"r":"g_pl","msg":"no_plugin_is_active_in_group","p":req.body});
				return sendError(res,"Group does not have any active plugins","no_plugin_is_active_in_group",constants.HTTP_STATUS.NO_PLUGINS_ACTIVE_IN_GROUP);
			}
			GroupPlugin.find({
				gid   : group_id,
				act   : true,
				type  : GroupPlugin.TYPE.PLUGIN_PACKAGE,
				status: GroupPlugin.STATUS.ACTIVE
			},{
				_id              : 0,
				plgnid           : 1,
				act              : 1,
				req_subscription : 1,
				promo_url        : 1 
			},function(err,groupplugins){
				if(err) { 
					logger.error({"r":"g_pl","p":req.body,"er":err});
					return sendError(res,"server_error","server_error");
				}
				if(groupplugins.length == 0){
					logger.info({"r":"g_pl","msg":"no_plugin_found_in_group","p":req.body});
					return sendSuccess(res, { plugins : [] });
				}
				var plugin_ids = [];
				var paid_plugin_ids = [];
				var plugin_promos   = {};
				var total = groupplugins.length;
				for(var i=0; i<total; i++){
					plugin_ids.push(groupplugins[i].plgnid+'');
					plugin_promos[groupplugins[i].plgnid+''] = groupplugins[i].promo_url;
					if(groupplugins[i].req_subscription){
						paid_plugin_ids.push(groupplugins[i].plgnid+'');
					}
				}
				AppPlugin.find({
					_id : { $in : plugin_ids }
				},{
					name         : 1,
					desc         : 1,
					thumb_url    : 1,
					showcase_url : 1,
					home_url     : 1,
					catg         : 1
				},{sort:{updatedAt:'asc'}},function(err,plugins){
					if(err){ 
						logger.error({"r":"g_pl","p":req.body,"er":err});
						return sendError(res,"server_error","server_error");
					}
					GroupPluginPackage.find({
						gid       : group_id,
						plugin_id : {$in : paid_plugin_ids},
						act       : true
					},{
						plugin_id        : 1,
						package_name     : 1,
						package_desc     : 1,
						package_price    : 1,
						package_duration : 1,
						duration_unit    : 1,
						duration_base    : 1,
						acc              : 1
					},function(err,plugin_packages){
						if(err){ 
							logger.error({"r":"g_pl","p":req.body,"er":err,"msg":"GroupPluginPackage_DbFindError"});
							return sendError(res,"server_error","server_error");
						}
						var packages = {};
						for(var j=0; j<plugin_packages.length; j++){
							if(!packages[plugin_packages[j].plugin_id])
								packages[plugin_packages[j].plugin_id] = [];
							packages[plugin_packages[j].plugin_id].push(plugin_packages[j]);
						}
						UserSubscription.find({
							gid       : group_id,
							pid       : profile_id,
							plugin_id : {$in : paid_plugin_ids},
							act       : true
						},{
							_id       : 0,
							plugin_id : 1,
							stim      : 1,
							etim      : 1
						},function(err,subscriptions){
							if(err){ 
								logger.error({"r":"g_pl","p":req.body,"er":err,"msg":"UserSubscription_DbFindError"});
								return sendError(res,"server_error","server_error");
							}
							var user_subscription = [];
							for(m=0; m<subscriptions.length; m++){
								if((subscriptions[m].etim - tim) > 0 && (tim - subscriptions[m].stim > 0)){
									user_subscription.push(subscriptions[m].plugin_id+'');
								}
							}
					  	return sendSuccess(res, { 
					  		plugins       : plugins, 
					  		plugin_promos : plugin_promos, 
					  		paid_plugins  : paid_plugin_ids, 
					  		packages      : packages, 
					  		subscriptions : user_subscription, 
					  		CC_DC         : false,
					  		catg_nm       : PLUGIN_CATEGORY_NAMES
					  	});
						});
					});
				});
			});
		});
	});	
});

router.post('/upgrd_pl', function(req, res, next) {
	req.checkBody('a_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();

	if(req.validationErrors()){ 
		logger.error({"r":"upgrd_pl","msg":"invalid_parameters","p":req.body});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}
	if(req.decoded.id != req.body.a_id){
		logger.error({"r":"upgrd_pl","msg":"aid_in_token_not_matching","p":req.body});
		return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var account_id 				= req.body.a_id;

	rmc.queryById("Account",account_id,function(err,account){
    if(err){ 
			logger.error({"r":"upgrd_pl","p":req.body,"er":err});
			return sendError(res,"server_error","server_error");
		}
    if(!account){
			logger.error({"r":"g_pl","msg":"no_account_found","p":req.body});
			return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}
    if(account.vrfy == false){
      logger.error({"r":"g_pl","msg":"account_unverified","p":req.body});
			return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
    }
		UserInfoCache.getUserGroupMemPartial(account_id, function(err,groupmems){
			if(err){ 
				logger.error({"r":"upgrd_pl","p":req.body,"er":err});
				return sendError(res,"server_error","server_error");
			}
			if(groupmems.length == 0){
				logger.info({"r":"upgrd_pl","msg":"account_has_no_groups_joined","p":req.body});
				return sendSuccess(res, { groups : [] });
			}
			var gids = [];
			var total = groupmems.length;
			for(var i=0; i<total; i++){
				gids.push(groupmems[i].gid+'');
			}
			Group.find({
				_id : { $in : gids },
				act : true,
				plgn: true
			},{
				_id : 1
			},function(err,groups){
				if(err){ 
					logger.error({"r":"upgrd_pl","p":req.body,"er":err});
					return sendError(res,"server_error","server_error");
				}
				return sendSuccess(res, { groups : groups });
			});
		});
	});
});

router.post('/usr_plg_idnt', function(req, res, next) {
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('pl_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();

	if(req.validationErrors()){ 
		logger.error({"r":"usr_plg_idnt","msg":"invalid_parameters","p":req.body});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var group_id 				= req.body.g_id;
	var profile_id      = req.body.p_id;
	var plugin_id       = req.body.pl_id;

	UserInfoCache.getMongoProfile(profile_id,function(err, profile){
		if(err){ 
			logger.error({"r":"usr_plg_idnt","msg":"getMongoProfile_error","p":req.body,"er":err});
			return sendError(res,err,"server_error"); 
		}
		if(!profile){
			logger.error({"r":"usr_plg_idnt","msg":"profile_not_found","p":req.body});
			return sendError(res,"Profile not found","profile_not_found",constants.HTTP_STATUS.NOT_FOUND);
		}
		if(req.decoded.id != profile.aid){
			logger.error({"r":"usr_plg_idnt","msg":"aid_in_token_not_matching_with_pid_account","p":req.body});
			return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}
		var name = profile.nam;
		var aid  = profile.aid;
		var ppic = profile.ppic;

		UserInfoCache.getMongoGrpMem(profile_id, group_id, function(err, groupmember){
			if(err) { 
				logger.error({"r":"usr_plg_idnt","msg":"getMongoGrpMem_DbError","p":req.body,"er":err});
				return sendError(res,"server_error","server_error"); 
			}
			if(!groupmember){
				logger.error({"r":"usr_plg_idnt","msg":"not_group_member","p":req.body});
				return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
			}
			GroupPlugin.findOne({
				gid    : group_id,
				act    : true,
				plgnid : plugin_id,
				status : GroupPlugin.STATUS.ACTIVE
			},function(err,groupplugin){
				if(err) { 
					logger.error({"r":"usr_plg_idnt","p":req.body,"er":err,"msg":"group_plugin_find_error"});
					return sendError(res,"server_error","server_error");
				}
				if(!groupplugin){
					logger.error({"r":"usr_plg_idnt","msg":"group_plugin_not_found","p":req.body});
					return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
				}
				AppPlugin.findOne({
					_id : plugin_id,
					act : true
				},function(err,app_plugin){
					if(err) { 
						logger.error({"r":"usr_plg_idnt","p":req.body,"er":err,"msg":"app_plugin_find_error"});
						return sendError(res,"server_error","server_error");
					}
					if(!app_plugin){
						logger.error({"r":"usr_plg_idnt","msg":"app_plugin_not_found","p":req.body});
						return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
					}
					PluginIdentifier.findOne({
						plugin_id : plugin_id,
						gid       : group_id,
						pid       : profile_id,
						act       : true
					},function(err,prev_identifer){
						if(err) { 
							logger.error({"r":"usr_plg_idnt","p":req.body,"er":err,"msg":"prev_identifier_find_error"});
							return sendError(res,"server_error","server_error");
						}
						if(prev_identifer){
							return sendSuccess(res, { u_idnt : prev_identifer.identifier });
						}
						var data_object = {
							name : 1,
							ppic : 1,
							role : 1,
							m    : 0,
							email: 0,
							grps : 0,
							pfls : 0,
							id1  : profile_id,
							id2  : plugin_id,
							id3  : group_id
						};
						var user_identifier = PluginToken.getUserPluginToken(data_object, app_plugin.private_key);
						var new_identifier = new PluginIdentifier({
							plugin_id : plugin_id,
							gid       : group_id,
							pid       : profile_id,
							act       : true,
							identifier: user_identifier
						});
						new_identifier.save(function(err,new_identifier_created){
							if(err) { 
								logger.error({"r":"usr_plg_idnt","p":req.body,"er":err,"msg":"new_identifier_save_error"});
								return sendError(res,"server_error","server_error");
							}
							return sendSuccess(res, { u_idnt : user_identifier });
						});
					});
				});
			});
		});
	});
});

function getPluginCategoryNames(){
	var catg_names = {};
	catg_names[AppPlugin.CATEGORY.VIDEO] = AppPlugin.CATEGORY_NAME.VIDEO;
	catg_names[AppPlugin.CATEGORY.TEST]  = AppPlugin.CATEGORY_NAME.TEST;
	catg_names[AppPlugin.CATEGORY.FEE]   = AppPlugin.CATEGORY_NAME.FEE;
	return catg_names;
}

module.exports = router;

