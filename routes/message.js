var express 							= require('express');
var mongoose 							= require('mongoose');
var jwt    								= require('jsonwebtoken');
var redis                 = require('redis');

var constants							= require('../utility/constants.js');
var configs								= require('../utility/configs.js');
var errorCodes 						= require('../utility/errors.js');
var helpers								= require('../utility/helpers.js');
var ClientType    				= require('../utility/client_type.js');
var UserInfoCache     		= require('../utility/user_info_cache');
var log4jsLogger  				= require('../loggers/log4js_module.js');
var log4jsConfigs  			  = require('../loggers/log4js_configs.js');
var UnsecureAccounts      = require('../utility/auth/unsecure_accounts.js');
var GroupInfoCache        = require('../utility/group_info_cache');
var ValidAuthRequest      = require('../utility/auth/valid_request.js');
var Actions 						  = require('../utility/action_names.js');
var GroupNotify 				  = require('../utility/grp_notify.js');

var redis_client        = redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
  console.log("Error " + err);
});

require('../models/groups.js');
require('../models/groupmembers.js');
require('../models/messages.js');
require('../models/profiles.js');
require('../models/statuses.js');

var Group 						= mongoose.model('Group');
var GroupMember 			= mongoose.model('GroupMember');
var Message 					= mongoose.model('Message');
var Profile 					= mongoose.model('Profile');
var Status 						= mongoose.model('Status');

var rmc               = require("./../utility/redis_mongo_cache.js")(redis_client);

var sendError 				= helpers.sendError;
var sendSuccess 			= helpers.sendSuccess;
var router 						= express.Router();
var logger        		= log4jsLogger.getLogger(log4jsConfigs.CATEGORY_ROUTES_MESSAGE);
var secureGroupLogger = log4jsLogger.getLogger(log4jsConfigs.CATEGORY_SECURE_GROUP);

//route middleware to verify the jwt token for all unsecured accounts as well covering for android users
router.use(function(req, res, next){
	var token = req.body.tokn || req.query.tokn;
	if(!token){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"token_not_found"});
	  return sendError(res,"Access without token is not authorised","token_not_found",constants.HTTP_STATUS.FORBIDDEN);
	}
	var pid = req.body.p_id || req.query.p_id;
	var aid = null;//req.body.a_id || req.query.a_id;
	req.decoded = null;
	if(token == 5 || token == '5' || token == 'null'){
		UnsecureAccounts.check(aid, pid, function(err,isUnsecure){
			if(err){
				console.log(err);
				logger.error({"url":req.originalUrl,"r":"auth","msg":"UnsecureAccounts_check_error","p":{pid:pid,aid:aid,tokn:token}});
				return sendError(res,err,"server_error");
			}
			if(isUnsecure){
				next();
			} else {
				logger.error({"url":req.originalUrl,"r":"auth","msg":"account_not_found_in_unsecured_list","p":{pid:pid,aid:aid,tokn:token}});
				return sendError(res,"Failed to authenticate token.","token_auth_failure",constants.HTTP_STATUS.NOT_FOUND);
			}
		});
	} else {
    jwt.verify(token, configs.JWT_ACCESS_TOKEN_PRIVATE_KEY, function(err, decoded) {      
      if(err){
      	console.log(err);
      	logger.error({"url":req.originalUrl,"r":"auth","msg":"jwt_verify_error","p":{pid:pid,aid:aid,tokn:token}});
        return sendError(res,"Failed to authenticate token.","token_auth_failure",constants.HTTP_STATUS.TOKEN_EXPIRED);  
      } else {
      	req.decoded = decoded;
        next();
      }
    });
	}
});

router.get('/previous', function(req, res, next){
	req.checkQuery('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkQuery('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkQuery('prev',errorCodes.invalid_parameters[1]).notEmpty().isInt().isValidPrev();
	req.checkQuery('skip',errorCodes.invalid_parameters[1]).notEmpty().isInt();
	req.checkQuery('tokn',errorCodes.invalid_parameters[1]).notEmpty().validateToken();

	if(req.validationErrors()){
		logger.error({"r":"previous","m":"GET","msg":"invalid_parameters","p":req.query});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var profile_id 				    = req.query.p_id;
	var group_id 				      = req.query.g_id;
	var number_of_messages 		= parseInt(req.query.prev);
	var skip_value				    = parseInt(req.query.skip);
	var tim                   = new Date().getTime();

	GroupInfoCache.getMongoGroup(group_id,function(err,group){
		if(err) { 
			logger.error({"r":"previous","m":"GET","msg":"getMongoGroup_error","p":req.query});
			return sendError(res,err,"server_error");
		}
		if(!group){
			logger.error({"r":"previous","m":"GET","msg":"no_group_found","p":req.query});
			return sendError(res,"Group does not exists.","no_group_found",constants.HTTP_STATUS.NOT_FOUND);
		}
		if(group.secr){
			secureGroupLogger.error({"r":"previous","m":"GET","msg":"secr_group_not_authorized","p":req.query});
			logger.error({"r":"previous","m":"GET","msg":"secr_group_not_authorized","p":req.query});
			return sendError(res,"not_authorized","not_authorized",constants.HTTP_STATUS.UNAUTHORIZED);
		}
		UserInfoCache.getMongoGrpMem(profile_id, group_id, function(err, member){
			if(err){ 
				logger.error({"r":"previous","m":"GET","msg":"getMongoGrpMem_DbError","p":req.query});
				return sendError(res,err,"server_error");
			}
			if(!member){
				logger.error({"r":"previous","m":"GET","msg":"not_a_group_member","p":req.query});
				return sendError(res,"You are not a member of this group.","not_a_member",constants.HTTP_STATUS.BAD_REQUEST);
			}
			if(!ValidAuthRequest.check(member.aid,req.decoded)){
				logger.error({"r":"previous","m":"GET","msg":"token_aid_mismatch_member_aid","p":req.query});
				return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
			}
			if(group.req_sub && group.req_sub ==  true){ 
				if(!member.sub_ed && !member.sub_st){
					logger.error({"r":"previous","msg":"user_subscription_required","p":req.query});
					return sendError(res,"You need to subscribe to enable your access.","user_subscription_required",constants.HTTP_STATUS.USER_SUBSCRIPTION_REQUIRED);
				}
				if(member.sub_ed < tim){
					logger.error({"r":"previous","msg":"user_subscription_ended","p":req.query});
					return sendError(res,"Oops, your subscription has expired. Kindly renew your subscription.","user_subscription_ended",constants.HTTP_STATUS.USER_SUBSCRIPTION_EXPIRED);
				}
			}
			if(member.type == GroupMember.TYPE.BANNED){ 
				logger.error({"r":"previous","m":"GET","msg":"banned_group_member","p":req.query});
				return sendError(res,"Banned group member cannot fetch messages","banned_group_member",constants.HTTP_STATUS.BAD_REQUEST); 
			}
			GroupMember.count({
				gid : group_id,
		  	act : true,
		  	gdel: false
		  },function(err,members_count){
		  	if(err){
		  		logger.error({"r":"previous","msg":"GroupMember_Count_DbError","p":req.query});
					return sendError(res,"server_error","server_error");
		  	}
		  	var non_admin_msg_types = [Message.MESSAGE_TYPE.audio,Message.MESSAGE_TYPE.text,Message.MESSAGE_TYPE.video,Message.MESSAGE_TYPE.image];
		  	var condition_or_sub;
		  	if(members_count > configs.GROUP_JOIN_NOTIFY_CONTROL_COUNT){
		  		condition_or_sub = {
		  			type : Message.MESSAGE_TYPE.notification,
		  			$or : [
		  				{ 
		  					actn : {$exists:false} 
		  				},
		  				{
		  					actn : {$exists:true},
								'actn.t' : {$ne : Actions.GROUP_JOIN}
		  				}
		  			]
		  		};
		  	} else {
		  		condition_or_sub = {
		  			type : Message.MESSAGE_TYPE.notification
		  		};
		  	}
				var condition = {};
				var is_admin = false;
				if(member.type !== GroupMember.TYPE.ADMIN){
					condition = {
						to  : group_id,
						$or : [
							{
								type : {$in : non_admin_msg_types}
							},
							condition_or_sub,
							{
								type : Message.MESSAGE_TYPE.admin,
								'actn.d.pid' : profile_id
							}
						],
						ignr : {$exists:false}
					};
				} else {
					is_admin = true;
					condition = {
						to  : group_id,
						ignr : {$exists:false}
					};
				}
				Message.find(condition,{
					cat : 0,
					spcf: 0,
					__v : 0
				},{
			    skip : skip_value, 
			    limit: number_of_messages,
			    sort : {
			      tim : -1
			    }
				},function(err, group_messages){
					if(err) { 
						logger.error({"r":"previous","m":"GET","msg":"message_find_server_error","p":req.query});
						return sendError(res,err,"server_error"); 
					}
					if(group_messages.length < 1){
						logger.error({"r":"previous","m":"GET","msg":"no_messages_found","p":req.query});
						return sendError(res,"Messages Not Found","no_messages_found",constants.HTTP_STATUS.NOT_FOUND);
					}
					return prepResultForPrevMsgs(group_messages, profile_id, is_admin, group_id, res);
				});
			});
		});
	});
});

router.get('/previous_v14', function(req, res, next){
	req.checkQuery('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkQuery('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkQuery('m_id',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkQuery('tim',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkQuery('ipp',errorCodes.invalid_parameters[1]).notEmpty().isInt().isValidIPP();
	req.checkQuery('tokn',errorCodes.invalid_parameters[1]).notEmpty().validateToken();

	if(req.validationErrors()){
		logger.error({"r":"previous_v14","m":"GET","msg":"invalid_parameters","p":req.query}); 
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var profile_id 				    = req.query.p_id;
	var group_id 				      = req.query.g_id;
	var number_of_messages 		= parseInt(req.query.ipp);
	var mid                   = req.query.m_id;
	var tim                   = req.query.tim;
	var srv_tim               = new Date().getTime();

	GroupInfoCache.getMongoGroup(group_id,function(err,group){
		if(err) { 
			logger.error({"r":"previous_v14","m":"GET","msg":"getMongoGroup_error","p":req.query});
			return sendError(res,err,"server_error");
		}
		if(!group){
			logger.error({"r":"previous_v14","m":"GET","msg":"no_group_found","p":req.query});
			return sendError(res,"Group does not exists.","no_group_found",constants.HTTP_STATUS.NOT_FOUND);
		}
		if(group.secr && !req.decoded){
			secureGroupLogger.error({"r":"previous_v14","m":"GET","msg":"secr_group_not_authorized","p":req.query});
			logger.error({"r":"previous_v14","m":"GET","msg":"secr_group_not_authorized","p":req.query});
			return sendError(res,"not_authorized","not_authorized",constants.HTTP_STATUS.UNAUTHORIZED);
		}
		UserInfoCache.getMongoGrpMem(profile_id, group_id, function(err, member) {
			if(err) { 
				logger.error({"r":"previous_v14","m":"GET","msg":"getMongoGrpMem_DbError","p":req.query});
				return sendError(res,err,"server_error"); 
			}
			if(!member) { 
				logger.error({"r":"previous_v14","m":"GET","msg":"not_a_group_member","p":req.query});
				return sendError(res,"You are not a member of this group.","not_a_member",constants.HTTP_STATUS.BAD_REQUEST); 
			}
			if(!ValidAuthRequest.check(member.aid,req.decoded)){
				logger.error({"r":"previous_v14","m":"GET","msg":"token_aid_mismatch_member_aid","p":req.query});
				return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
			}
			if(group.req_sub && group.req_sub ==  true){ 
				if(!member.sub_ed && !member.sub_st){
					logger.error({"r":"previous_v14","msg":"user_subscription_required","p":req.query});
					return sendError(res,"You need to subscribe to enable your access.","user_subscription_required",constants.HTTP_STATUS.USER_SUBSCRIPTION_REQUIRED);
				}
				if(member.sub_ed < srv_tim){
					logger.error({"r":"previous_v14","msg":"user_subscription_ended","p":req.query});
					return sendError(res,"Oops, your subscription has expired. Kindly renew your subscription.","user_subscription_ended",constants.HTTP_STATUS.USER_SUBSCRIPTION_EXPIRED);
				}
			}
			if(member.type == GroupMember.TYPE.BANNED){ 
				logger.error({"r":"previous_v14","m":"GET","msg":"banned_group_member","p":req.query});
				return sendError(res,"Banned group member cannot fetch messages","banned_group_member",constants.HTTP_STATUS.BAD_REQUEST); 
			}
			GroupMember.count({
				gid : group_id,
		  	act : true,
		  	gdel: false
		  },function(err,members_count){
		  	if(err){
		  		logger.error({"r":"previous_v14","msg":"GroupMember_Count_DbError","p":req.query});
					return sendError(res,"server_error","server_error");
		  	}
				var non_admin_msg_types = [Message.MESSAGE_TYPE.audio,Message.MESSAGE_TYPE.text,Message.MESSAGE_TYPE.video,Message.MESSAGE_TYPE.image];
		  	var condition_or_sub;
		  	if(members_count > configs.GROUP_JOIN_NOTIFY_CONTROL_COUNT){
		  		condition_or_sub = {
		  			type : Message.MESSAGE_TYPE.notification,
		  			$or : [
		  				{ 
		  					actn : {$exists:false} 
		  				},
		  				{
		  					actn : {$exists:true},
								'actn.t' : {$ne : Actions.GROUP_JOIN}
		  				}
		  			]
		  		};
		  	} else {
		  		condition_or_sub = {
		  			type : Message.MESSAGE_TYPE.notification
		  		};
		  	}
				var condition = {};
				var is_admin = false;
				if(member.type != GroupMember.TYPE.ADMIN){
					condition = {
						to  : group_id,
						$or : [
							{
								type : {$in : non_admin_msg_types}
							},
							condition_or_sub,
							{
								type : Message.MESSAGE_TYPE.admin,
								'actn.d.pid' : profile_id
							}
						],
						ignr : {$exists:false}
					};
				} else {
					is_admin = true;
					condition = {
						to  : group_id,
						ignr : {$exists:false}
					};
				}
				if(mid == '-'){
					Message.find(condition,{
						cat : 0,
						spcf: 0,
						__v : 0
					},{
				    limit: number_of_messages,
				    sort : {
				      tim : -1 
				    }
					},function(err, group_messages){
						if(err) { 
							logger.error({"r":"previous_v14","m":"GET","msg":"message_find_server_error_1","p":req.query});
							return sendError(res,err,"server_error"); 
						}
						if(group_messages.length == 0){
							logger.error({"r":"previous_v14","m":"GET","msg":"no_messages_found","p":req.query});
							return sendError(res,"Messages Not Found","no_messages_found",constants.HTTP_STATUS.NOT_FOUND);
						}
						return prepResultForPrevMsgs(group_messages, profile_id, is_admin, group_id, res);
					});
				} else {
					Message.findOne({
						_id : mid,
						tim : tim
					},{
						_id : 1
					},function(err,resp){
						if(err){ 
							logger.error({"r":"previous_v14","m":"GET","msg":"message_find_server_error_2","p":req.query});
							return sendError(res,err,"server_error"); 
						}
						if(!resp){ 
							logger.error({"r":"previous_v14","m":"GET","msg":"mid_not_found","p":req.query});
							return sendError(res,"Message id not found","mid_not_found",constants.HTTP_STATUS.NOT_FOUND);
						}
						condition.tim = { $lte : tim};
						Message.find(condition,{
							cat : 0,
							spcf: 0,
							__v : 0
						},{
					    limit: number_of_messages,
					    sort : {
					      tim : -1
					    }
						},function(err, group_messages){
							if(err) { 
								logger.error({"r":"previous_v14","m":"GET","msg":"message_find_server_error_3","p":req.query});
								return sendError(res,err,"server_error"); 
							}
							if(group_messages.length == 0){
								logger.error({"r":"previous_v14","m":"GET","msg":"no_messages_found","p":req.query});
								return sendError(res,"Messages Not Found","no_messages_found",constants.HTTP_STATUS.NOT_FOUND);
							}
							for(var i=0; i<group_messages.length; i++){
								if(group_messages[i]._id == mid){
									group_messages.splice(i,1);
									i--;
								}
							}
							return prepResultForPrevMsgs(group_messages, profile_id, is_admin, group_id, res);
						});
					});
				}
			});
		});
	});
});

router.get('/p_bulk', function(req, res, next){
	req.checkQuery('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();

	if(req.validationErrors()){ 
		logger.error({"r":"p_bulk","m":"GET","msg":"invalid_parameters","p":req.query}); 
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var profile_id 				    = req.query.p_id;
	var number_of_messages 		= 1;//parseInt(req.query.prev);
	var skip_value				    = 0;//parseInt(req.query.skip);

	UserInfoCache.getMongoProfile(profile_id,function(err, profile){
		if(err){ 
			logger.error({"r":"p_bulk","m":"POST","msg":"profiles_server_error","p":req.query});
			return sendError(res,err,"server_error");
		}
		if(!profile){
			logger.error({"r":"p_bulk","m":"POST","msg":"profile_not_found","p":req.query});
			return sendError(res,"Profile not found.","profile_not_found",constants.HTTP_STATUS.NOT_FOUND);
		}
		if(profile.aid != req.decoded.id){
			logger.error({"r":"p_bulk","msg":"token_aid_mismatch_member_aid","p":req.query});
			return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}
		GroupMember.find({
			pid : profile_id,
			act : true,
			gdel: false
		},{
			gid : 1,
			type: 1,
			_id : 0
		},function(err, members){
			if(err) { 
				logger.error({"r":"p_bulk","m":"GET","msg":"groupmember_find_server_error","p":req.query});
				return sendError(res,err,"server_error"); 
			}
			if(members.length == 0){
				logger.error({"r":"p_bulk","m":"GET","msg":"not_a_group_member","p":req.query});
				return sendError(res,"You are not a member of these groups","not_a_member",constants.HTTP_STATUS.BAD_REQUEST);
			}
			var fltrd_gids = [];
			var conditions = [];
			var non_admin_msg_types = [Message.MESSAGE_TYPE.audio,Message.MESSAGE_TYPE.text,Message.MESSAGE_TYPE.video,Message.MESSAGE_TYPE.image,Message.MESSAGE_TYPE.notification];
			members.forEach(function(member){
				if(member.type !== GroupMember.TYPE.BANNED){
					fltrd_gids.push(member.gid);
					var condition;
					if(member.type !== GroupMember.TYPE.ADMIN){
						condition = {
							to  : member.gid,
							$or : [
								{
									type : {$in : non_admin_msg_types}
								},
								{
									type : Message.MESSAGE_TYPE.admin,
									'actn.d.pid' : profile_id
								}
							],
							ignr : {$exists:false}
						};
						conditions[member.gid] = condition;
					} else {
						condition = {
							to  : member.gid,
							ignr : {$exists:false}
						};
						conditions[member.gid] = condition;
					}
				}
			});
			if(fltrd_gids.length == 0){
				return sendSuccess(res, { messages : [] });
			}
			fetch_messages_bulk(fltrd_gids, conditions, skip_value, number_of_messages, function(msgs){
				if(msgs.length < 1){
					logger.error({"r":"p_bulk","m":"GET","msg":"no_messages_found","p":req.query});
					return sendError(res,"Messages Not Found","no_messages_found",constants.HTTP_STATUS.NOT_FOUND);
				}		
				return sendSuccess(res, { messages : msgs });
			});
		});
	});
});

router.get('/seen_list', function(req, res, next){
	req.checkQuery('m_id',errorCodes.invalid_parameters[1]).notEmpty().notEmpty();
	req.checkQuery('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkQuery('ipp',errorCodes.invalid_parameters[1]).notEmpty().isInt();
	req.checkQuery('skip',errorCodes.invalid_parameters[1]).notEmpty().isInt();
	req.checkQuery('tokn',errorCodes.invalid_parameters[1]).notEmpty().validateToken();

	if(req.validationErrors()){
		logger.error({"r":"seen_list","m":"GET","msg":"invalid_parameters","p":req.query}); 
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var profile_id 				    = req.query.p_id;
	var message_id 				    = req.query.m_id;
	var number_of_seens 		  = parseInt(req.query.ipp);
	var skip_value				    = parseInt(req.query.skip);
	var tim                   = new Date().getTime();
	
	if(skip_value != 0 && (message_id == -1 || message_id == '-1')){
		logger.error({"r":"seen_list","m":"GET","msg":"invalid_parameters_skip","p":req.query});  
		return sendError(res,"m_id is invalid","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}
	
	UserInfoCache.getMongoProfile(profile_id, function(err, profile){
		if(err) { 
			logger.error({"r":"seen_list","m":"GET","msg":"getMongoProfile_DbError","p":req.query});  
			return sendError(res,err,"server_error");
		}
		if(!profile) {
			logger.error({"r":"seen_list","m":"GET","msg":"profile_not_found","p":req.query}); 	 
			return sendError(res,"No Profile Found.","profile_not_found",constants.HTTP_STATUS.NOT_FOUND); 
		}
		if(!ValidAuthRequest.check(profile.aid,req.decoded)){
			logger.error({"r":"previous","m":"GET","msg":"ValidAuthRequest_check_error","p":req.query});
			return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}
		Message.findOne({
			_id : message_id
		},{
			_id : 1,
			to  : 1,
			type: 1,
			ttyp: 1
		},function(err, message){
			if(err) { 
				logger.error({"r":"seen_list","m":"GET","msg":"message_find_server_error","p":req.query});  
				return sendError(res,err,"server_error");
			}
			if(!message){ 
				logger.error({"r":"seen_list","m":"GET","msg":"no_msg_found","p":req.query}); 
				return sendError(res,"No Message Found.","no_msg_found",constants.HTTP_STATUS.NOT_FOUND);
			}
			if(message.ttyp !== Message.MESSAGE_RECIPIENT_TYPE.GROUP){
				logger.error({"r":"seen_list","m":"GET","msg":"msg_ttyp_not_group","p":req.query}); 
				return sendError(res,"Message recipient type not group.","msg_ttyp_not_group",constants.HTTP_STATUS.NOT_FOUND); 
			}
			if(!(message.type === Message.MESSAGE_TYPE.text || message.type === Message.MESSAGE_TYPE.audio || message.type === Message.MESSAGE_TYPE.video || message.type === Message.MESSAGE_TYPE.image)) {
				logger.error({"r":"seen_list","m":"GET","msg":"msg_type_not_valid","p":req.query}); 
				return sendError(res,"Message type is not group message","msg_type_not_valid",constants.HTTP_STATUS.NOT_FOUND); 
			}
			GroupInfoCache.getMongoGroup(message.to,function(err,group){
				if(err) { 
					logger.error({"r":"seen_list","m":"GET","msg":"getMongoGroup_error","p":req.query});
					return sendError(res,err,"server_error");
				}
				if(!group){
					logger.error({"r":"seen_list","m":"GET","msg":"no_group_found","p":req.query});
					return sendError(res,"Group does not exists.","no_group_found",constants.HTTP_STATUS.NOT_FOUND);
				}
				if(group.secr && !req.decoded){
					secureGroupLogger.error({"r":"seen_list","m":"GET","msg":"secr_group_not_authorized","p":req.query});
					logger.error({"r":"seen_list","m":"GET","msg":"secr_group_not_authorized","p":req.query});
					return sendError(res,"not_authorized","not_authorized",constants.HTTP_STATUS.UNAUTHORIZED);
				}
				UserInfoCache.getMongoGrpMem(profile_id, message.to, function(err, groupmember){
					if(err){ 
						logger.error({"r":"seen_list","m":"GET","msg":"getMongoGrpMem_DbError","p":req.query});  
						return sendError(res,err,"server_error"); 
					}
					if(!groupmember){
						logger.error({"r":"seen_list","m":"GET","msg":"not_a_group_member","p":req.query});  
						return sendError(res,"User is not a group member.","not_a_member",constants.HTTP_STATUS.NOT_FOUND);
					}
					if(group.req_sub && group.req_sub ==  true){ 
						if(!groupmember.sub_ed && !groupmember.sub_st){
							logger.error({"r":"seen_list","msg":"user_subscription_required","p":req.query});
							return sendError(res,"You need to subscribe to enable your access.","user_subscription_required",constants.HTTP_STATUS.USER_SUBSCRIPTION_REQUIRED);
						}
						if(groupmember.sub_ed < tim){
							logger.error({"r":"seen_list","msg":"user_subscription_ended","p":req.query});
							return sendError(res,"Oops, your subscription has expired. Kindly renew your subscription.","user_subscription_ended",constants.HTTP_STATUS.USER_SUBSCRIPTION_EXPIRED);
						}
					}
					if(groupmember.type !== GroupMember.TYPE.ADMIN){
						logger.error({"r":"seen_list","m":"GET","msg":"not_a_group_admin","p":req.query}); 
						return sendError(res,"User is not group admin.","not_a_group_admin",constants.HTTP_STATUS.NOT_FOUND); 
					}
					Status.count({
						mid : message_id,
						stim: { $exists : true}
					},function(err,total_seen_count){
						if(err){ 
							logger.error({"r":"seen_list","m":"GET","msg":"status_count_server_error","p":req.query});   
							return sendError(res,err,"server_error");
						}
						if(total_seen_count == 0){ 
							return sendSuccess(res, { total_seens:total_seen_count, list:[]}); 
						}
						Status.find({
							mid : message_id,
							stim: { $exists : true}
						},{
							pid : 1,
							stim: 1
						},{
							skip : skip_value,
							limit : number_of_seens,
							sort : {
								stim : -1
							}
						},function(err,seen_list){
							if(err) { 
								logger.error({"r":"seen_list","m":"GET","msg":"status_find_server_error","p":req.query});  
								return sendError(res,err,"server_error");
							}
							if(seen_list.length == 0) { 
								return sendSuccess(res, { total_seens:total_seen_count, list:seen_list}); 
							}
							prepareSeenListData(seen_list, message.to, function(err, data){
								if(err){ 
									logger.error({"r":"seen_list","m":"GET","msg":"preapreSeenListData_error","p":req.query}); 
									return sendError(res,err,"server_error");
								}
								if(data){ 
									return sendSuccess(res, { total_seens:total_seen_count, list:data});
								}	else {
									logger.error({"r":"seen_list","m":"GET","msg":"preapreSeenListData_data_error","p":req.query});  
									return sendError(res,err,"server_error");
								}
							});
						});
					});
				});
			});
		});
	});
});

router.post('/del', function(req, res, next) {
	req.checkBody('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();  // User to be given the admin rights
	req.checkBody('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkBody('m_id',errorCodes.invalid_parameters[1]).notEmpty().validateToken();

	if(req.validationErrors()){ 
		logger.error({"r":"del","msg":"invalid_parameters","p":req.body});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var profile_id 	  = req.body.p_id;
	var group_id 	    = req.body.g_id;
	var message_id    = req.body.m_id;

	UserInfoCache.getMongoGrpMem(profile_id, group_id, function(err, groupmember){
		if(err){ 
			logger.error({"r":"del","msg":"getMongoGrpMem_DbError","p":req.body});
			return sendError(res,"server_error","server_error");
		}
		if(!groupmember){
			logger.error({"r":"del","msg":"not_a_group_member","p":req.body});
			return sendError(res,"You are not a member of this group.","not_a_member",constants.HTTP_STATUS.BAD_REQUEST);
		}
		if(groupmember.aid != req.decoded.id){
			logger.error({"r":"del","msg":"ValidAuthRequest_check_error","p":req.body});
			return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}
		if(groupmember.type != GroupMember.TYPE.ADMIN){ 
			logger.error({"r":"del","msg":"not_a_group_admin","p":req.body});
			return sendError(res,"User not an Administrator of the Group","authentication_error",constants.HTTP_STATUS.BAD_REQUEST); 
		}
		Message.findOne({
			_id : message_id
		},function(err,message){
			if(err){ 
				logger.error({"r":"del","msg":"MessageFind_DbError","p":req.body});
				return sendError(res,"server_error","server_error");
			}
			if(!message){
				logger.error({"r":"del","msg":"mid_not_found","p":req.body});
				return sendError(res,"Message Id Not Found","mid_not_found",constants.HTTP_STATUS.BAD_REQUEST);
			}
			if(message.idel && message.idel == true){
				logger.warn({"r":"del","msg":"MessageAlreadyDeleted","p":req.body});
				return sendSuccess(res, { mid:message_id, body:message.body, ntyp : 1});
			}
			var required_types = [Message.MESSAGE_TYPE.audio,Message.MESSAGE_TYPE.text,Message.MESSAGE_TYPE.video,Message.MESSAGE_TYPE.image];
			if(required_types.indexOf(parseInt(message.type)) < 0){
				logger.error({"r":"del","msg":"Message Type is Not of Required Types","p":req.body,"mtyp":message.type});
				return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
			}
			if(message.to != group_id){
				logger.error({"r":"del","msg":"GroupID_Mismatch_from_MessageData","p":req.body});
				return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
			}
			var new_body = "**Message Removed**";
			Message.update({
				_id : message_id
			},{
				obody : message.body,
				type  : Message.MESSAGE_TYPE.text,
				body  : new_body,
				idel  : true
			},function(err,resp){
				if(err){
					logger.error({"r":"del","msg":"MessageUpdateError","p":req.body,"err":err});
					return sendError(res,"server_error","server_error");
				}
				rmc.remove("Message",message_id,function(err,resp){
					if(err){
						logger.warn({"r":"del","msg":"rmc_removeMessageCache","p":req.body,"err":err});
						return sendError(res,"server_error","server_error");
					}
					logger.info({"r":"del","msg":"Message_Deleted_Success","p":{pid:profile_id,gid:group_id,mid:message_id}});
					sendSuccess(res, { mid:message_id, body:new_body, ntyp : 1});
					return GroupNotify.abt_msg_d(group_id, profile_id, groupmember.pnam, message_id, new_body);
				});
			});
		});
	});
});

function prepResultForPrevMsgs(group_messages, pid, is_admin, group_id, res){
	var unique_profile_ids = [];
	var num_gms = group_messages.length;
	var mids = [];
	var seen_count_msgs = [];
	for(var j=0; j<num_gms; j++){
		if(unique_profile_ids.indexOf(group_messages[j].from) < 0)
			unique_profile_ids.push(group_messages[j].from);
		mids.push(group_messages[j]._id);
		seen_count_msgs[group_messages[j]._id+''] = 0;
	}
	Profile.find({
		_id : { $in : unique_profile_ids }
		// act : true
	},{
		aid:1, 
		nam:1, 
		cnam:1, 
		role:1, 
		ppic:1
	},function(err, profiles){
		if(err){ 
			logger.error({"r":"prepResultForPrevMsgs","m":"FUNCTION","msg":"profile_find_server_error","p":{pid:pid,is_admin:is_admin,gid:group_id}});
			return sendError(res,err,"server_error");
		}
		GroupMember.find({
			gid : group_id,
			pid : { $in : unique_profile_ids },
			act : true,
			gdel: false
		},{
			pid : 1,
			type: 1,
			_id : 0
		},function(err,members){
			if(err){
				logger.error({"r":"prepResultForPrevMsgs","m":"FUNCTION","msg":"groupmember_find_server_error","p":{pid:pid,is_admin:is_admin,gid:group_id}}); 
				return sendError(res,err,"server_error");
			}
			var mem_length = members.length;
			grpMemByPid = [];
			for(var k=0; k<mem_length; k++){
				grpMemByPid[members[k].pid] = members[k]; 
			}
			getSeenstatusOnMsgsForUser(mids, pid, function(err,seen_status){
				if(err) {
					logger.error({"r":"prepResultForPrevMsgs","m":"FUNCTION","msg":"getSeenstatusOnMsgsForUser_error","p":{pid:pid,is_admin:is_admin,gid:group_id}});
					return sendError(res,err,"server_error");
				}
				getSeenCountForMessages(mids, is_admin, seen_count_msgs, function(err, msg_seen_counts){
					if(err) {
						logger.error({"r":"prepResultForPrevMsgs","m":"FUNCTION","msg":"getSeenCountForMessages_error","p":{pid:pid,is_admin:is_admin,gid:group_id}});
						return sendError(res,err,"server_error");
					}
					for(var n=0; n<num_gms; n++){
						group_messages[n] 		    = group_messages[n].toJSON();
						group_messages[n].isn     = seen_status.indexOf(group_messages[n]._id) >=0 ? 1 : 0;
						group_messages[n].mt 	    = typeof(grpMemByPid[group_messages[n].from]) !== 'undefined' ? grpMemByPid[group_messages[n].from].type : 0;
						group_messages[n].seens 	= msg_seen_counts[group_messages[n]._id];
					}
					return sendSuccess(res, { messages : group_messages, profiles : profiles });
				});
			});
		});
	});
}

function getSeenCountForMessages(mids, is_admin, seen_count_msgs, cb){
	if(!is_admin){
		return cb(null,seen_count_msgs);
	}
	Status.aggregate([
    {
      $match :{
        mid  : { $in : mids },
        stim : {$exists:true},
      }
    },
    {
      $group :{
        _id : "$mid",
        count : {
          $sum : 1
        }
      }
    }
  ],function(err,seen_counts){
    if(err){
      return cb(err,null);
    }
    var sc_len = seen_counts.length;
    for(var i=0; i<sc_len; i++)
    	seen_count_msgs[seen_counts[i]._id] = seen_counts[i].count;
    return cb(null,seen_count_msgs);
  });
}

function getSeenstatusOnMsgsForUser(mids, pid, cb){
	Status.find({
		mid : { $in : mids },
		pid : pid,
		stim: { $exists : true }
	},{
		mid : 1,
		_id : 0
	},function(err,sRows){
		if(err) {
			return cb(err,null);
		}
		
		var sLen = sRows.length;
		var result =[];
		for(c=0; c<sLen; c++){
			result.push(sRows[c].mid);
		}
		return cb(null,result);
	});
}

function prepareSeenListData(list, gid, cb){
	var profile_ids = [];
	var data = [];
	var list_length = list.length;
	var seenTimeByPid = [];
	for(var i=0; i<list_length; i++){
		profile_ids.push(list[i].pid);
		seenTimeByPid[list[i].pid] = list[i].stim;
	}
	Profile.find({
		_id : { $in : profile_ids },
		act : true
	},{
		_id : 1,
		nam : 1,
		cnam: 1,
		ppic: 1,
		role: 1
	},function(err, profiles){
		if(err) { console.trace(err); return cb(true,null); }
		GroupMember.find({
			gid : gid,
			pid : { $in : profile_ids},
			act : true,
			gdel: false
		},{
			_id : 1,
			type: 1
		},function(err,members){
			if(err) { console.trace(err); return cb(true,null); }
			if(members.length == 0) { console.trace('Group members data fetching unsuccessfull'); return cb(true,null); }
			var members_count = members.length;
			var membersByPid = [];
			for(var j=0; j<members_count; j++)
				membersByPid[members[j].pid] = members[i];
			var num_of_profiles = profiles.length;
			for(var m=0; m<num_of_profiles; m++){
				data[m] = {
					pid : profiles[m]._id,
					nam : profiles[m].nam,
					role: profiles[m].role,
					cnam: profiles[m].cnam,
					ppic: profiles[m].ppic,
					stim: seenTimeByPid[profiles[m]._id]
				};
				if(typeof(membersByPid[profiles[m]._id]) !== 'undefined')
					data[m].mtyp = membersByPid[profiles[m]._id][type];
				else data[m].mtyp = 0;
			}
			data.sort(function(a, b){
			 return b.stim-a.stim;
			});
			return cb(null,data);
		});
	});
}

var fetch_messages_bulk = function(gids,conditions,skip_value,number_of_messages,cb) {
	var done_gids = [];
	var all_messages = [];
	gids.forEach(function(gid){
		Message.find(conditions[gid],{},{
			skip : skip_value,
			limit : number_of_messages,
			sort : {
				tim : -1
			}
		},function(err,msgs){
			all_messages = all_messages.concat(msgs);
			done_gids.push(gid);
			if(done_gids.length == gids.length) {
				return cb(all_messages);
			}
		});
	});
};

function checkIfMessageIdEqualsMid(message_id, mid){
	var len1 = message_id.length;
	var len2 = mid.length;
	if(len1 == 0 || len2 == 0) return false;
	if(len1 != len2) return false;
	if(message_id[0] != mid[0]) return false;
	if(message_id[len1-1] != mid[len2-1]) return false;
	if(message_id[parseInt(len1/2)] != mid[parseInt(len2/2)]) return false;
	return (message_id==mid);
}

module.exports = router;