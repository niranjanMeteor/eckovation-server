var express 								= require('express');
var mongoose 								= require('mongoose');
var jwt    									= require('jsonwebtoken');

var configs									= require('../utility/configs.js');
var errorCodes 							= require('../utility/errors.js');
var constants								= require('../utility/constants.js');
var helpers									= require('../utility/helpers.js');
var AppClients							= require('../utility/app_clients.js');
var log4jsLogger  					= require('../loggers/log4js_module.js');
var log4jsConfigs  					= require('../loggers/log4js_configs.js');
var MailClient          		= require('../utility/mail/mail_client.js');
var UserQueue           		= require('../userqueues/user_rcq.js');

require('../models/accounts.js');
require('../models/profiles.js');
require('../models/accounttokens.js');

var Account 								= mongoose.model('Account');
var Profile 								= mongoose.model('Profile');
var AccountToken  					= mongoose.model('AccountToken');

var sendError 							= helpers.sendError;
var sendSuccess 						= helpers.sendSuccess;
var router 									= express.Router();
var logger        					= log4jsLogger.getLogger(log4jsConfigs.CATEGORY_ROUTES_ACCOUNT);

var ValidMobileDevicesType 	= [AccountToken.CLIENT.ANDROID,AccountToken.CLIENT.ANDROID,,AccountToken.CLIENT.IOS];

router.use(function(req, res, next){
	var token = req.body.uq_tokn;
	var aid   = req.body.a_id;
	if(!token){
		logger.error({"url":req.originalUrl,"msg":"token_not_found","p":req.body});
	  return sendError(res,"Access without token is not authorised","token_not_found",constants.HTTP_STATUS.FORBIDDEN);
	}
	req.decoded = null;
  jwt.verify(token, configs.JWT_USERQUEUE_TOKEN_PRIVATE_KEY, function(err, decoded) {      
    if(err){
    	logger.error({"url":req.originalUrl,"msg":"jwt_verify_error","p":req.body,"er":err});
      return sendError(res,"Failed to authenticate token.","token_auth_failure",constants.HTTP_STATUS.TOKEN_EXPIRED);  
    }
    if(decoded.id != aid){
			logger.error({"r":req.originalUrl,"msg":"aid_in_token_not_matching_with_accountid","p":req.body});
			return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}
		if(ValidMobileDevicesType.indexOf(decoded.cl) < 0){
			logger.error({"r":req.originalUrl,"msg":"client_in_token_not_valid_mobile_device_client","p":req.body});
			return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}
    next();
  });
});

router.post('/d_uq_all', function(req, res, next) {
	req.checkBody('a_id',errorCodes.invalid_parameters[1]).notEmpty();
	
	if(req.validationErrors()){ 
		logger.error({"r":"d_uq_all","msg":"invalid_parameters","p":req.body});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var aid   = req.body.a_id;
	var tim   = new Date().getTime();

	Account.findOne({
		_id  : aid,
		vrfy : true
	},{
		_id : 1 
	},function(err,account){
		if(err){ 
			logger.error({"r":"d_uq_all","msg":"account_find_server_error","p":req.body,"err":err});
			return sendError(res,"server_error","server_error");
		}
		if(!account) { 
			logger.error({"r":"d_uq_all","msg":"no_account_found","p":req.body});
			return sendError(res,"Account not found!","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}
		Profile.find({
			aid : aid,
			act : true
		},{
			_id : 1
		},function(err,profiles){
			if(err){ 
				logger.error({"r":"d_uq_all","msg":"profile_find_server_error","p":req.body,"err":err});
				return sendError(res,"server_error","server_error");
			}
			if(profiles.length == 0) { 
				logger.error({"r":"d_uq_all","msg":"no_profile_found","p":req.body});
				return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
			}
			var pids  = [];
			var total = profiles.length;
			for(var i=0; i<total; i++){
				pids.push(profiles[i]._id+'');
			}
			var toDo  = 3;
			var done  = 0;
			UserQueue.dropPidsQ("d_uq_all", pids, function(){
				if(err){
					return cb(err,null);
				}
				done++;
				if(done == toDo){
					return sendSuccess(res,{});
				}
			});
			UserQueue.dropAidQ("d_uq_all", aid, function(){
				if(err){
					return cb(err,null);
				}
				done++;
				if(done == toDo){
					return sendSuccess(res,{});
				}
			});
			UserQueue.dropPidsSeenCountQ("d_uq_all", pids, function(){
				if(err){
					return cb(err,null);
				}
				done++;
				if(done == toDo){
					return sendSuccess(res,{});
				}
			});
		});
	});
});

module.exports = router;
