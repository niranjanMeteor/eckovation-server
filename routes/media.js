var express 					   = require('express');
var mongoose 					   = require('mongoose');
var crypto 						   = require('crypto');
var AWS 				  		   = require('aws-sdk');
var jwt    						   = require('jsonwebtoken');
var moment      			   = require('moment');
var redis                = require('redis');

var constants					   = require('../utility/constants.js');
var configs							 = require('../utility/configs.js');
var errorCodes 				   = require('../utility/errors.js');
var helpers						   = require('../utility/helpers.js');
var fileTypeValidator	   = require('../utility/file_type_validator.js');
var ClientType    		   = require('../utility/client_type.js');
var RedisPrefixes        = require('../utility/redis_prefix');
var UserInfoCache        = require('../utility/user_info_cache');
var GroupInfoCache       = require('../utility/group_info_cache');
var log4jsLogger  		   = require('../loggers/log4js_module.js');
var log4jsConfigs        = require('../loggers/log4js_configs.js');
var UnsecureAccounts     = require('../utility/auth/unsecure_accounts.js');
var ValidAuthRequest     = require('../utility/auth/valid_request.js');
var AwsBuckets           = require('../aws/buckets.js');
var AwsFileValidations   = require('../aws/file_validations.js');
var AwsS3Module          = require('../aws/aws_s3.js');


var redis_client      = redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
    console.log("Error " + err);
});

require('../models/groups.js');
require('../models/groupmembers.js');
require('../models/profiles.js');
require('../models/messages.js');

var Group 			  		= mongoose.model('Group');
var GroupMember 			= mongoose.model('GroupMember');
var Profile 		  		= mongoose.model('Profile');
var Message 		  		= mongoose.model('Message');

var rhmset        		= require("../utility/redis_hmset.js")(redis_client);

var sendError 				= helpers.sendError;
var sendSuccess 			= helpers.sendSuccess;
var router 						= express.Router();
var logger        		= log4jsLogger.getLogger(log4jsConfigs.CATEGORY_ROUTES_MEDIA);
var secureGroupLogger = log4jsLogger.getLogger(log4jsConfigs.CATEGORY_SECURE_GROUP);

AWS.config.update({
  accessKeyId: configs.AWS_ACCESS_KEY,
  secretAccessKey: configs.AWS_SECRET_KEY,
  "region": configs.AWS_REGION,
  "output" : configs.AWS_OUTPUT 
});
var s3 						= new AWS.S3();

var getExpiryTime = function () {
    var _date = new Date();
    return '' + (_date.getFullYear()) + '-' + (_date.getMonth() + 1) + '-' +
        (_date.getDate() + 1) + 'T' + (_date.getHours() + 3) + ':' + '00:00.000Z';
};

//route middleware to verify the jwt token for all unsecured accounts as well covering for android users
router.use(function(req, res, next){
	var token = req.body.tokn || req.query.tokn;
	if(!token){
		logger.error({"url":req.originalUrl,"r":"auth","msg":"token_not_found"});
	  return sendError(res,"Access without token is not authorised","token_not_found",constants.HTTP_STATUS.FORBIDDEN);
	}
	var pid = req.body.p_id || req.query.p_id;
	var aid = null;//req.body.a_id || req.query.a_id;
  req.decoded = null;

  if(token == 5 || token == '5' || token == 'null'){
		UnsecureAccounts.check(aid, pid, function(err,isUnsecure){
			if(err){
				console.log(err);
				logger.error({"url":req.originalUrl,"r":"auth","msg":"UnsecureAccounts_check_error","p":{pid:pid,aid:aid,tokn:token}});
				return sendError(res,err,"server_error");
			}
			if(isUnsecure){
				next();
			} else {
				logger.error({"url":req.originalUrl,"r":"auth","msg":"account_not_found_in_unsecured_list","p":{pid:pid,aid:aid,tokn:token}});
				return sendError(res,"Failed to authenticate token.","token_auth_failure",constants.HTTP_STATUS.NOT_FOUND);
			}
		});
	} else {
    jwt.verify(token, configs.JWT_ACCESS_TOKEN_PRIVATE_KEY, function(err, decoded) {      
      if(err){
      	console.log(err);
      	logger.error({"url":req.originalUrl,"r":"auth","msg":"jwt_verify_error","p":{pid:pid,aid:aid,tokn:token}});
        return sendError(res,"Failed to authenticate token.","token_auth_failure",constants.HTTP_STATUS.TOKEN_EXPIRED);  
      } else {
        req.decoded = decoded;
        next();
      }
    });
	}
});

router.get('/ioscpolicy',function(req,res,next){
	var mime = req.query.mimetype;
	var date = new Date();
  var s3Policy = {
    'expiration': getExpiryTime(),
    'conditions': [
    	{'bucket': AwsBuckets.CHAT_PIC},
        ['starts-with', "$key", ""],
        {'acl': 'authenticated-read'},
        ['starts-with', '$Content-Type', mime],
        {'success_action_status' : '201'}
    ]
  };
  var stringPolicy = JSON.stringify(s3Policy);
  var base64Policy = new Buffer(stringPolicy, 'utf-8').toString('base64');
  var signature = crypto.createHmac('sha1', configs.AWS_SECRET_KEY)
                      .update(new Buffer(base64Policy, 'utf-8')).digest('base64');
  var s3Credentials = {
      s3Policy: base64Policy,
      s3Signature: signature,
      AWSAccessKeyId: configs.AWS_ACCESS_KEY
  };
  return sendSuccess(res, s3Credentials);
});

router.get('/ioscpolicy-pluginmedia',function(req,res,next){
  var mime = req.query.mimetype;
  var date = new Date();
  var s3Policy = {
    'expiration': getExpiryTime(),
    'conditions': [
      {'bucket': AwsBuckets.PLUGIN_MEDIA},
        ['starts-with', "$key", ""],
        {'acl': 'public-read'},
        ['starts-with', '$Content-Type', mime],
        {'success_action_status' : '201'}
    ]
  };
  var stringPolicy = JSON.stringify(s3Policy);
  var base64Policy = new Buffer(stringPolicy, 'utf-8').toString('base64');
  var signature = crypto.createHmac('sha1', configs.AWS_SECRET_KEY)
                      .update(new Buffer(base64Policy, 'utf-8')).digest('base64');
  var s3Credentials = {
      s3Policy: base64Policy,
      s3Signature: signature,
      AWSAccessKeyId: configs.AWS_ACCESS_KEY
  };
  return sendSuccess(res, s3Credentials);
});

router.get('/ppicupload', function(req, res, next){
	req.checkQuery('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkQuery('tokn',errorCodes.invalid_parameters[1]).notEmpty().validateToken();

	if(req.validationErrors()) { 
		logger.error({"r":"ppicupload","m":"GET","msg":"invalid_parameters","p":req.query});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var profile_id		= req.query.p_id;
	var filename 			= profile_id+''+moment()+''+'.jpg';
	var format 				= 'image/jpg';

	UserInfoCache.getMongoProfile(profile_id, function(err, profile) {
		if(err) { 
    	logger.error({"r":"ppicupload","m":"GET","msg":"getMongoProfile_DBerror","p":req.query});
    	return sendError(res,err,"server_error"); 
    }
		if(!profile) {
			logger.error({"r":"ppicupload","m":"GET","msg":"profile_not_found","p":req.query});
			return sendError(res,"Profile not found","profile_not_found",constants.HTTP_STATUS.NOT_FOUND);
		}
    var params = { 
      Bucket: AwsBuckets.PROFILE_PIC, 
      Key: filename, 
      Expires: configs.AMAZON_EXPIRY_TIME, 
      ContentType: format, 
      ACL: 'public-read'
    };
    s3.getSignedUrl('putObject', params, function(err, url){ 
      if(err) { 
	    	logger.error({"r":"ppicupload","m":"GET","msg":"S3GetSignedUrlError","p":req.query});
	    	return sendError(res,err,"server_error"); 
	    }
      if(!url) {
      	logger.error({"r":"ppicupload","m":"GET","msg":"S3GetSignedUrlError_NoUrl","p":req.query});
      	return sendError(res,"No response from server","no_server_response",constants.HTTP_STATUS.BAD_REQUEST);
      }
      var return_data = {
        signed_url	: url,
        mask_url	  : 'https://'+AwsBuckets.PROFILE_PIC+'.s3.amazonaws.com/'+filename,
        filename    : filename
      };
      return sendSuccess(res, return_data);
    });
	});
});

router.get('/pp_plcy_i',function(req,res,next){
	var mime = req.query.mimetype;
	var date = new Date();
  var s3Policy = {
    'expiration': getExpiryTime(),
    'conditions': [
    	{'bucket': AwsBuckets.PROFILE_PIC},
        ['starts-with', "$key", ""],
        {'acl': 'public-read'},
        ['starts-with', '$Content-Type', mime],
        {'success_action_status' : '201'}
    ]
  };
  var stringPolicy = JSON.stringify(s3Policy);
  var base64Policy = new Buffer(stringPolicy, 'utf-8').toString('base64');
  var signature = crypto.createHmac('sha1', configs.AWS_SECRET_KEY)
                      .update(new Buffer(base64Policy, 'utf-8')).digest('base64');
  var s3Credentials = {
    s3Policy: base64Policy,
    s3Signature: signature,
    AWSAccessKeyId: configs.AWS_ACCESS_KEY
  };
  return sendSuccess(res, s3Credentials);
});

//ToDo :: Security stuff. So if basically, if a person uploads a
//malicious image, then it straight away gets downloaded
//on various fucking platforms -- and crashes the people's systems!
router.get('/gpicupload', function(req, res, next){
	req.checkQuery('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkQuery('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkQuery('tokn',errorCodes.invalid_parameters[1]).notEmpty().validateToken();

	if(req.validationErrors()) { 
		logger.error({"r":"gpicupload","m":"GET","msg":"invalid_parameters","p":req.query});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var admin_id				= req.query.p_id;
	var group_id				= req.query.g_id;
	var filename  			= group_id+''+moment()+''+'.jpg';
	var file_type 			= 'jpg';
	var file_validity 	= fileTypeValidator("image", file_type);

	if(file_validity === false) { 
		logger.error({"r":"gpicupload","m":"GET","msg":"file_validity_error","p":req.query});
		return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}
  GroupInfoCache.getMongoGroup(group_id,function(err,group){
    if(err) { 
      logger.error({"r":"gpicupload","m":"GET","msg":"getMongoGroup_error","p":req.query});
      return sendError(res,err,"server_error");
    }
    if(!group){
      logger.error({"r":"gpicupload","m":"GET","msg":"no_group_found","p":req.query});
      return sendError(res,"Group does not exists.","no_group_found",constants.HTTP_STATUS.NOT_FOUND);
    }
    if(group.secr && !req.decoded){
      secureGroupLogger.error({"r":"gpicupload","m":"GET","msg":"secr_group_not_authorized","p":req.query});
      logger.error({"r":"gpicupload","m":"GET","msg":"secr_group_not_authorized","p":req.query});
      return sendError(res,"not_authorized","not_authorized",constants.HTTP_STATUS.UNAUTHORIZED);
    }
  	UserInfoCache.getMongoGrpMem(admin_id, group_id, function(err, group_admin) {
  		if(err) { 
      	logger.error({"r":"gpicupload","m":"GET","msg":"getMongoGrpMem_DbError","p":req.query});
      	return sendError(res,err,"server_error"); 
      }
  		if(!group_admin) {
  			logger.error({"r":"gpicupload","m":"GET","msg":"not_a_group_member","p":req.query});
  			return sendError(res,"You are not a member of the Group","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
  		}
  		if(group_admin.type != GroupMember.TYPE.ADMIN) {
  			logger.error({"r":"gpicupload","m":"GET","msg":"not_a_group_admin","p":req.query});
  			return sendError(res,"You are not Group Administrator","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
  		}
      var params = { 
        Bucket: configs.BUCKET_NAME_GROUP_PIC, 
        Key: filename, 
        Expires: configs.AMAZON_EXPIRY_TIME, 
        ContentType: "image/"+file_type, 
        ACL: 'public-read'
      };
      s3.getSignedUrl('putObject', params, function(err, url){ 
        if(err) { 
        	logger.error({"r":"gpicupload","m":"GET","msg":"S3GetSignedUrlError","p":req.query});
        	return sendError(res,err,"server_error"); 
        }
        if(!url) {
        	logger.error({"r":"gpicupload","m":"GET","msg":"S3GetSignedUrlError_NoUrl","p":req.query});
        	return sendError(res,"No response from server","no_server_response",constants.HTTP_STATUS.BAD_REQUEST);
        }
        var return_data = {
          signed_url	: url,
          mask_url	: 'https://'+configs.BUCKET_NAME_GROUP_PIC+'.s3.amazonaws.com/'+filename,
          filename : filename
        };
        return sendSuccess(res, return_data);
      });
  	});
  });
});

router.get('/gp_plcy_i',function(req,res,next){
	var mime = req.query.mimetype;
	var date = new Date();
  var s3Policy = {
    'expiration': getExpiryTime(),
    'conditions': [
    	{'bucket': AwsBuckets.GROUP_PIC},
        ['starts-with', "$key", ""],
        {'acl': 'public-read'},
        ['starts-with', '$Content-Type', mime],
        {'success_action_status' : '201'}
    ]
  };
  var stringPolicy = JSON.stringify(s3Policy);
  var base64Policy = new Buffer(stringPolicy, 'utf-8').toString('base64');
  var signature = crypto.createHmac('sha1', configs.AWS_SECRET_KEY)
                      .update(new Buffer(base64Policy, 'utf-8')).digest('base64');
  var s3Credentials = {
    s3Policy: base64Policy,
    s3Signature: signature,
    AWSAccessKeyId: configs.AWS_ACCESS_KEY
  };
  return sendSuccess(res, s3Credentials);
});

router.get('/cmediaupload', function(req, res, next) {
	req.checkQuery('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkQuery('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkQuery('mode',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkQuery('file_name',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkQuery('tokn',errorCodes.invalid_parameters[1]).notEmpty().validateToken();

	if(req.validationErrors()){ 
		logger.error({"r":"cmediaupload","m":"GET","msg":"invalid_parameters","p":req.query});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var profile_id			= req.query.p_id;
	var group_id	  		= req.query.g_id;
	var name 		    		= req.query.file_name;
	var mode		    		= req.query.mode;
	var file_type   		= name.split(".").pop();
	var format      		= mode+"/"+file_type;
	var file_validity 	= fileTypeValidator(mode, file_type);
  var tim             = new Date().getTime();

	if(file_validity === false){ 
		logger.error({"r":"cmediaupload","m":"GET","msg":"file_validity_error","p":req.query});
		return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST); 
	}
	GroupInfoCache.getMongoGroup(group_id,function(err,group){
		if(err) { 
    	logger.error({"r":"cmediaupload","m":"GET","msg":"group_find_server_error","p":req.query});
    	return sendError(res,err,"server_error"); 
    }
    if(!group){
    	logger.error({"r":"cmediaupload","m":"GET","msg":"no_group_found","p":req.query});
    	return sendError(res,"No Group Found","no_group_found",constants.HTTP_STATUS.NOT_FOUND);
    }
    if(group.secr && !req.decoded){
      secureGroupLogger.error({"r":"cmediaupload","m":"GET","msg":"secr_group_not_authorized","p":req.query});
      logger.error({"r":"cmediaupload","m":"GET","msg":"secr_group_not_authorized","p":req.query});
      return sendError(res,"not_authorized","not_authorized",constants.HTTP_STATUS.UNAUTHORIZED);
    }
		UserInfoCache.getMongoGrpMem(profile_id, group_id, function(err, member) {
			if(err) { 
	    	logger.error({"r":"cmediaupload","m":"GET","msg":"groupmember_find_server_error","p":req.query});
	    	return sendError(res,err,"server_error"); 
	    }
			if(!member) {
				logger.error({"r":"cmediaupload","m":"GET","msg":"not_a_group_member","p":req.query});
				return sendError(res,"You are not a member of the Group","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
			}
      if(group.req_sub && group.req_sub ==  true){ 
        if(!member.sub_ed && !member.sub_st){
          logger.error({"r":"cmediaupload","msg":"user_subscription_required","p":req.query});
          return sendError(res,"You need to subscribe to enable your access.","user_subscription_required",constants.HTTP_STATUS.USER_SUBSCRIPTION_REQUIRED);
        }
        if(member.sub_ed < tim){
          logger.error({"r":"cmediaupload","msg":"user_subscription_ended","p":req.query});
          return sendError(res,"Oops, your subscription has expired. Kindly renew your subscription.","user_subscription_ended",constants.HTTP_STATUS.USER_SUBSCRIPTION_EXPIRED);
        }
      }
			if(member.type == GroupMember.TYPE.BANNED) {
				logger.error({"r":"cmediaupload","m":"GET","msg":"banned_group_member","p":req.query});
				return sendError(res,"Banned Group cannot upload picture","banned_group_member",constants.HTTP_STATUS.BAD_REQUEST);
			}
			if(group.gtyp == Group.GROUP_TYPE.ONE_WAY && member.type != GroupMember.TYPE.ADMIN) {
				logger.error({"r":"cmediaupload","m":"GET","msg":"group_one_way_and_no_admin","p":req.query});
				return sendError(res,"You do not have permission to use this feature.","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
			}
	    var params = { 
	      Bucket: AwsBuckets.CHAT_PIC, 
	      Key: name, 
	      Expires: configs.AMAZON_EXPIRY_TIME, 
	      ContentType: format, 
	      ACL: 'authenticated-read'
	    };
	    s3.getSignedUrl('putObject', params, function(err, url){ 
	      if(err) { 
	      	logger.error({"r":"cmediaupload","m":"GET","msg":"S3GetSignedUrlError","p":req.query});
	      	return sendError(res,err,"server_error"); 
	      }
	      if(!url) {
	      	logger.error({"r":"cmediaupload","m":"GET","msg":"S3GetSignedUrlError_NoUrl","p":req.query});
	      	return sendError(res,"No response from server","no_server_response",constants.HTTP_STATUS.BAD_REQUEST);
	      }
	      var return_data = {
	        signed_url	: url,
	        mask_url	  : 'https://'+AwsBuckets.CHAT_PIC+'.s3.amazonaws.com/'+name 
	      };
	      return sendSuccess(res, return_data);
	    });
		});
	});
});

router.get('/getcmedia', function(req, res, next) {
	req.checkQuery('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkQuery('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
	req.checkQuery('m_url',errorCodes.invalid_parameters[1]).notEmpty();
	req.checkQuery('tokn',errorCodes.invalid_parameters[1]).notEmpty().validateToken();

	if(req.validationErrors()){ 
		logger.error({"r":"getcmedia","m":"GET","msg":"invalid_parameters","p":req.query});
		return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
	}

	var profile_id			= req.query.p_id;
	var group_id	  		= req.query.g_id;
	var mask_url    		= req.query.m_url;
	var splits      		= mask_url.split("/");
	var name        		= splits[splits.length-1];
	var fileSplit   		= name.split(".");
	var mid         		= fileSplit[0];
	var extn        		= fileSplit[1];
  var tim             = new Date().getTime();

  GroupInfoCache.getMongoGroup(group_id,function(err,group){
    if(err) { 
      logger.error({"r":"getcmedia","m":"GET","msg":"group_find_server_error","p":req.query});
      return sendError(res,err,"server_error"); 
    }
    if(!group){
      logger.error({"r":"getcmedia","m":"GET","msg":"no_group_found","p":req.query});
      return sendError(res,"No Group Found","no_group_found",constants.HTTP_STATUS.NOT_FOUND);
    }
  	UserInfoCache.getMongoGrpMem(profile_id, group_id, function(err, member) {
  		if(err) { 
      	logger.error({"r":"getcmedia","m":"GET","msg":"getMongoGrpMem_DbError","p":req.query});
      	return sendError(res,err,"server_error"); 
      }
  		if(!member) {
  			logger.error({"r":"getcmedia","m":"GET","msg":"not_a_group_member","p":req.query});
  			return sendError(res,"You are not a member of the Group","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
  		}
      if(group.req_sub && group.req_sub ==  true){ 
        if(!member.sub_ed && !member.sub_st){
          logger.error({"r":"getcmedia","msg":"user_subscription_required","p":req.query});
          return sendError(res,"You need to subscribe to enable your access.","user_subscription_required",constants.HTTP_STATUS.USER_SUBSCRIPTION_REQUIRED);
        }
        if(member.sub_ed < tim){
          logger.error({"r":"getcmedia","msg":"user_subscription_ended","p":req.query});
          return sendError(res,"Oops, your subscription has expired. Kindly renew your subscription.","user_subscription_ended",constants.HTTP_STATUS.USER_SUBSCRIPTION_EXPIRED);
        }
      }
  		var params = {
  			Bucket: configs.BUCKET_NAME_CHAT_PIC, 
  			Key: name,
  			Expires : configs.AMAZON_EXPIRY_TIME
  		};
  		if(extn == '3gp' || extn == 'mp3'){
  			rhmset.exists(RedisPrefixes.MEDIA_CONVERSION_JOB+mid,function(err,resp){
  				if(err){
  					logger.error({"r":"getcmedia","m":"GET","msg":"media_conversion_job_server_error","p":req.query});
  					return sendError(res,err,"server_error");
  				}
  				if(!resp){
  					s3.getSignedUrl('getObject', params, function (err, url) {
  						if(err) { 
  			      	logger.error({"r":"getcmedia","m":"GET","msg":"media_conversion_job_S3GetSignedUrl_error","p":req.query});
  			      	return sendError(res,err,"server_error"); 
  			      }
  			      if(!url) {
  			      	logger.error({"r":"getcmedia","m":"GET","msg":"media_conversion_job_S3GetSignedUrl_noUrl","p":req.query});
  			      	return sendError(res,"No response from server","no_server_response",constants.HTTP_STATUS.BAD_REQUEST);
  			      }
  			      return sendSuccess(res, {signed_url	: url});
  					});
  				} else {
  					logger.error({"r":"getcmedia","m":"GET","msg":"media_conversion_job_in_progress","p":req.query});
  					return sendError(res,"Media In Processing","media_in_processing",constants.HTTP_STATUS.MEDIA_IN_PROCESSING);
  				}
  			});
  		} else {
  			s3.getSignedUrl('getObject', params, function (err, url) {
  				if(err) { 
  	      	logger.error({"r":"getcmedia","m":"GET","msg":"S3GetSignedUrlError","p":req.query});
  	      	return sendError(res,err,"server_error"); 
  	      }
  	      if(!url) {
  	      	logger.error({"r":"getcmedia","m":"GET","msg":"S3GetSignedUrlError_NoUrl","p":req.query});
  	      	return sendError(res,"No response from server","no_server_response",constants.HTTP_STATUS.BAD_REQUEST);
  	      }
  	      return sendSuccess(res, {signed_url	: url});
  			});
  		}
  	});
  });
});

//route for getting signed url for files of documents types only
router.get('/cp_w', function(req, res, next) {
  if(!req.decoded){
    logger.error({"r":"cp_w","m":"GET","msg":"not_authorised","p":req.query});
    return sendError(res,"not_authorised","not_authorised",constants.HTTP_STATUS.UNAUTHORIZED);
  }
  req.checkQuery('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
  req.checkQuery('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
  req.checkQuery('m_od',errorCodes.invalid_parameters[1]).notEmpty();
  req.checkQuery('f_tp',errorCodes.invalid_parameters[1]).notEmpty();
  req.checkQuery('f_nm',errorCodes.invalid_parameters[1]).notEmpty();

  if(req.validationErrors()){ 
    logger.error({"r":"cp_w","m":"GET","msg":"invalid_parameters","p":req.query});
    return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
  }

  var profile_id      = req.query.p_id;
  var group_id        = req.query.g_id;
  var mode            = req.query.m_od;
  var file_type       = req.query.f_tp;
  var file_name       = req.query.f_nm;
  var tim             = new Date().getTime();

  GroupInfoCache.getMongoGroup(group_id,function(err,group){
    if(err) { 
      logger.error({"r":"cp_w","m":"GET","msg":"group_find_server_error","p":req.query});
      return sendError(res,err,"server_error"); 
    }
    if(!group){
      logger.error({"r":"cp_w","m":"GET","msg":"no_group_found","p":req.query});
      return sendError(res,"No Group Found","no_group_found",constants.HTTP_STATUS.NOT_FOUND);
    }
    UserInfoCache.getMongoGrpMem(profile_id, group_id, function(err, member) {
      if(err) { 
        logger.error({"r":"cp_w","m":"GET","msg":"groupmember_find_server_error","p":req.query});
        return sendError(res,err,"server_error"); 
      }
      if(!member) {
        logger.error({"r":"cp_w","m":"GET","msg":"not_a_group_member","p":req.query});
        return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
      }
      if(member.aid != req.decoded.id){
        logger.error({"r":"cp_w","m":"GET","msg":"ValidAuthRequest_check_error","p":req.query});
        return sendError(res,"not_authorised","not_authorised",constants.HTTP_STATUS.UNAUTHORIZED);
      }
      if(group.req_sub && group.req_sub ==  true){ 
        if(!member.sub_ed && !member.sub_st){
          logger.error({"r":"cp_w","msg":"user_subscription_required","p":req.query});
          return sendError(res,"You need to subscribe to enable your access.","user_subscription_required",constants.HTTP_STATUS.USER_SUBSCRIPTION_REQUIRED);
        }
        if(member.sub_ed < tim){
          logger.error({"r":"cp_w","msg":"user_subscription_ended","p":req.query});
          return sendError(res,"Oops, your subscription has expired. Kindly renew your subscription.","user_subscription_ended",constants.HTTP_STATUS.USER_SUBSCRIPTION_EXPIRED);
        }
      }
      if(member.type == GroupMember.TYPE.BANNED) {
        logger.error({"r":"cp_w","m":"GET","msg":"banned_group_member","p":req.query});
        return sendError(res,"Banned Group cannot upload picture","banned_group_member",constants.HTTP_STATUS.BAD_REQUEST);
      }
      if(group.gtyp == Group.GROUP_TYPE.ONE_WAY && member.type != GroupMember.TYPE.ADMIN) {
        logger.error({"r":"cp_w","m":"GET","msg":"group_one_way_and_no_admin","p":req.query});
        return sendError(res,"You do not have permission to use this feature.","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
      }
      if(!AwsFileValidations.isValidMode(file_type, mode)){
        logger.error({"r":"cp_w","m":"GET","msg":"isValidMode_error","p":req.query});
        return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
      }
      if(!AwsFileValidations.isValidCpicFile(file_type, file_name)){
        logger.error({"r":"cp_w","m":"GET","msg":"isValidCpicFile_error","p":req.query});
        return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
      }
      var format          = mode+"/"+file_type;
      var credentials = AwsS3Module.getCredentials(AwsBuckets.CHAT_PIC, format, file_type);
      return sendSuccess(res, credentials);
    });
  });
});

//route for getting signed url for files of documents types only
router.get('/pp_w', function(req, res, next) {
  if(!req.decoded){
    logger.error({"r":"pp_w","m":"GET","msg":"not_authorised","p":req.query});
    return sendError(res,"not_authorised","not_authorised",constants.HTTP_STATUS.UNAUTHORIZED);
  }
  req.checkQuery('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
  req.checkQuery('m_od',errorCodes.invalid_parameters[1]).notEmpty();
  req.checkQuery('f_tp',errorCodes.invalid_parameters[1]).notEmpty();
  req.checkQuery('f_nm',errorCodes.invalid_parameters[1]).notEmpty();

  if(req.validationErrors()){ 
    logger.error({"r":"pp_w","m":"GET","msg":"invalid_parameters","p":req.query});
    return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
  }

  var profile_id      = req.query.p_id;
  var group_id        = req.query.g_id;
  var mode            = req.query.m_od;
  var file_type       = req.query.f_tp;
  var file_name       = req.query.f_nm;

  UserInfoCache.getMongoProfile(profile_id,function(err,profile){
    if(err) { 
      logger.error({"r":"pp_w","m":"GET","msg":"profile_find_server_error","p":req.query});
      return sendError(res,err,"server_error"); 
    }
    if(!profile){
      logger.error({"r":"pp_w","m":"GET","msg":"no_profile_found","p":req.query});
      return sendError(res,"No profile Found","profile_not_found",constants.HTTP_STATUS.NOT_FOUND);
    }
    if(!ValidAuthRequest.check(profile.aid,req.decoded)){
      logger.error({"r":"vd","m":"GET","msg":"ValidAuthRequest_check_error","p":req.query});
      return sendError(res,"not_authorised","not_authorised",constants.HTTP_STATUS.UNAUTHORIZED);
    }
    if(!AwsFileValidations.isValidMode(file_type, mode)){
      logger.error({"r":"pp_w","m":"GET","msg":"isValidMode_error","p":req.query});
      return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
    }
    if(!AwsFileValidations.isValidPpicFile(file_type, file_name)){
      logger.error({"r":"pp_w","m":"GET","msg":"isValidPpicFile_error","p":req.query});
      return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
    }
    var format          = mode+"/"+file_type;
    var credentials = AwsS3Module.getCredentials(AwsBuckets.PROFILE_PIC, format, file_type);
    return sendSuccess(res, credentials);
  });
});

//route for getting signed url for files of documents types only
router.get('/gp_w', function(req, res, next) {
  if(!req.decoded){
    logger.error({"r":"gp_w","m":"GET","msg":"not_authorised","p":req.query});
    return sendError(res,"not_authorised","not_authorised",constants.HTTP_STATUS.UNAUTHORIZED);
  }
  req.checkQuery('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
  req.checkQuery('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
  req.checkQuery('m_od',errorCodes.invalid_parameters[1]).notEmpty();
  req.checkQuery('f_tp',errorCodes.invalid_parameters[1]).notEmpty();
  req.checkQuery('f_nm',errorCodes.invalid_parameters[1]).notEmpty();

  if(req.validationErrors()){ 
    logger.error({"r":"gp_w","m":"GET","msg":"invalid_parameters","p":req.query});
    return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
  }

  var profile_id      = req.query.p_id;
  var group_id        = req.query.g_id;
  var mode            = req.query.m_od;
  var file_type       = req.query.f_tp;
  var file_name       = req.query.f_nm;

  UserInfoCache.getMongoGrpMem(profile_id, group_id, function(err, member) {
    if(err) { 
      logger.error({"r":"gp_w","m":"GET","msg":"groupmember_find_server_error","p":req.query});
      return sendError(res,err,"server_error"); 
    }
    if(!member) {
      logger.error({"r":"gp_w","m":"GET","msg":"not_a_group_member","p":req.query});
      return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
    }
    if(!ValidAuthRequest.check(member.aid,req.decoded)){
      logger.error({"r":"vd","m":"GET","msg":"ValidAuthRequest_check_error","p":req.query});
      return sendError(res,"not_authorised","not_authorised",constants.HTTP_STATUS.UNAUTHORIZED);
    }
    if(member.type != GroupMember.TYPE.ADMIN) {
      logger.error({"r":"gp_w","m":"GET","msg":"groupmember_not_admin","p":req.query});
      return sendError(res,"invalid_parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
    }
    if(!AwsFileValidations.isValidMode(file_type, mode)){
      logger.error({"r":"gp_w","m":"GET","msg":"isValidMode_error","p":req.query});
      return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
    }
    if(!AwsFileValidations.isValidGpicFile(file_type, file_name)){
      logger.error({"r":"gp_w","m":"GET","msg":"isValidGpicFile_error","p":req.query});
      return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
    }
    var format          = mode+"/"+file_type;
    var credentials = AwsS3Module.getCredentials(AwsBuckets.GROUP_PIC, format, file_type);
    return sendSuccess(res, credentials);
  });
});

//route for getting signed url for files of documents types only
router.get('/doc', function(req, res, next) {
  if(!req.decoded){
    logger.error({"r":"doc","m":"GET","msg":"not_authorised","p":req.query});
    return sendError(res,"not_authorised","not_authorised",constants.HTTP_STATUS.UNAUTHORIZED);
  }
  req.checkQuery('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
  req.checkQuery('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
  req.checkQuery('m_od',errorCodes.invalid_parameters[1]).notEmpty();
  req.checkQuery('f_tp',errorCodes.invalid_parameters[1]).notEmpty();
  req.checkQuery('f_nm',errorCodes.invalid_parameters[1]).notEmpty();

  if(req.validationErrors()){ 
    logger.error({"r":"doc","m":"GET","msg":"invalid_parameters","p":req.query});
    return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
  }

  var profile_id      = req.query.p_id;
  var group_id        = req.query.g_id;
  var mode            = req.query.m_od;
  var file_type       = req.query.f_tp;
  var file_name       = req.query.f_nm;

  GroupInfoCache.getMongoGroup(group_id,function(err,group){
    if(err) { 
      logger.error({"r":"doc","m":"GET","msg":"group_find_server_error","p":req.query});
      return sendError(res,err,"server_error"); 
    }
    if(!group){
      logger.error({"r":"doc","m":"GET","msg":"no_group_found","p":req.query});
      return sendError(res,"No Group Found","no_group_found",constants.HTTP_STATUS.NOT_FOUND);
    }
    UserInfoCache.getMongoGrpMem(profile_id, group_id, function(err, member) {
      if(err) { 
        logger.error({"r":"doc","m":"GET","msg":"groupmember_find_server_error","p":req.query});
        return sendError(res,err,"server_error"); 
      }
      if(!member) {
        logger.error({"r":"doc","m":"GET","msg":"not_a_group_member","p":req.query});
        return sendError(res,"You are not a member of the Group","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
      }
      if(!ValidAuthRequest.check(member.aid,req.decoded)){
        logger.error({"r":"vd","m":"GET","msg":"ValidAuthRequest_check_error","p":req.query});
        return sendError(res,"not_authorised","not_authorised",constants.HTTP_STATUS.UNAUTHORIZED);
      }
      if(member.type == GroupMember.TYPE.BANNED) {
        logger.error({"r":"doc","m":"GET","msg":"banned_group_member","p":req.query});
        return sendError(res,"Banned Group cannot upload picture","banned_group_member",constants.HTTP_STATUS.BAD_REQUEST);
      }
      if(group.gtyp == Group.GROUP_TYPE.ONE_WAY && member.type != GroupMember.TYPE.ADMIN) {
        logger.error({"r":"doc","m":"GET","msg":"group_one_way_and_no_admin","p":req.query});
        return sendError(res,"You do not have permission to use this feature.","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
      }
      if(!AwsFileValidations.isValidMode(file_type, mode)){
        logger.error({"r":"doc","m":"GET","msg":"isValidMode_error","p":req.query});
        return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
      }
      if(!AwsFileValidations.isValidDocFile(file_type, file_name)){
        logger.error({"r":"doc","m":"GET","msg":"isValidDocFile_error","p":req.query});
        return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
      }
      var format          = mode+"/"+file_type;
      var credentials = AwsS3Module.getCredentials(AwsBuckets.DOC, format, file_type);
      return sendSuccess(res, credentials);
    });
  });
});

//routes for getting url for files of video type only
router.get('/vd', function(req, res, next) {
  if(!req.decoded){
    logger.error({"r":"vd","m":"GET","msg":"not_authorised","p":req.query});
    return sendError(res,"not_authorised","not_authorised",constants.HTTP_STATUS.UNAUTHORIZED);
  }
  req.checkQuery('p_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
  req.checkQuery('g_id',errorCodes.invalid_parameters[1]).notEmpty().isValidMongoId();
  req.checkQuery('m_od',errorCodes.invalid_parameters[1]).notEmpty();
  req.checkQuery('f_tp',errorCodes.invalid_parameters[1]).notEmpty();
  req.checkQuery('f_nm',errorCodes.invalid_parameters[1]).notEmpty();

  if(req.validationErrors()){ 
    logger.error({"r":"md_oth","m":"GET","msg":"invalid_parameters","p":req.query});
    return sendError(res,req.validationErrors(),"invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
  }

  var profile_id      = req.query.p_id;
  var group_id        = req.query.g_id;
  var mode            = req.query.m_od;
  var file_type       = req.query.f_tp;
  var file_name       = req.query.f_nm;
  var format          = mode+"/"+file_type;

  GroupInfoCache.getMongoGroup(group_id,function(err,group){
    if(err) { 
      logger.error({"r":"vd","m":"GET","msg":"group_find_server_error","p":req.query});
      return sendError(res,err,"server_error"); 
    }
    if(!group){
      logger.error({"r":"vd","m":"GET","msg":"no_group_found","p":req.query});
      return sendError(res,"No Group Found","no_group_found",constants.HTTP_STATUS.NOT_FOUND);
    }
    UserInfoCache.getMongoGrpMem(profile_id, group_id, function(err, member) {
      if(err) { 
        logger.error({"r":"vd","m":"GET","msg":"groupmember_find_server_error","p":req.query});
        return sendError(res,err,"server_error"); 
      }
      if(!member) {
        logger.error({"r":"vd","m":"GET","msg":"not_a_group_member","p":req.query});
        return sendError(res,"You are not a member of the Group","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
      }
      if(!ValidAuthRequest.check(member.aid,req.decoded)){
        logger.error({"r":"vd","m":"GET","msg":"ValidAuthRequest_check_error","p":req.query});
        return sendError(res,"not_authorised","not_authorised",constants.HTTP_STATUS.UNAUTHORIZED);
      }
      if(member.type == GroupMember.TYPE.BANNED) {
        logger.error({"r":"vd","m":"GET","msg":"banned_group_member","p":req.query});
        return sendError(res,"Banned Group cannot upload picture","banned_group_member",constants.HTTP_STATUS.BAD_REQUEST);
      }
      if(group.gtyp == Group.GROUP_TYPE.ONE_WAY && member.type != GroupMember.TYPE.ADMIN) {
        logger.error({"r":"vd","m":"GET","msg":"group_one_way_and_no_admin","p":req.query});
        return sendError(res,"You do not have permission to use this feature.","authentication_error",constants.HTTP_STATUS.BAD_REQUEST);
      }
      if(!AwsFileValidations.isValidMode(file_type, mode)){
        logger.error({"r":"vd","m":"GET","msg":"isValidMode_error","p":req.query});
        return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
      }
      if(!AwsFileValidations.isValidVideoFile(file_type, file_name)){
        logger.error({"r":"vd","m":"GET","msg":"isValidVideoFile_error","p":req.query});
        return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
      }
      var format          = mode+"/"+file_type;
      var credentials = AwsS3Module.getCredentials(AwsBuckets.VIDEO, format, file_type);
      return sendSuccess(res, credentials);
    });
  });
});

exports.checkMemValidity = function(member, group){
	if(!member) {
		return false;
	}
	if(!group) {
		return false;
	}
	if(member.type == GroupMember.TYPE.BANNED) {
		return false;
	}
	if(group.gtyp == Group.GROUP_TYPE.ONE_WAY && member.type != GroupMember.TYPE.ADMIN) {
		return false;
	}
	return true;
}

module.exports = router;