var express 								= require('express');
var mongoose 								= require('mongoose');
var jwt    									= require('jsonwebtoken');

var configs									= require('../utility/configs.js');
var errorCodes 							= require('../utility/errors.js');
var constants								= require('../utility/constants.js');
var helpers									= require('../utility/helpers.js');
var AppClients							= require('../utility/app_clients.js');
var log4jsLogger  					= require('../loggers/log4js_module.js');
var log4jsConfigs  					= require('../loggers/log4js_configs.js');
var MailClient          		= require('../utility/mail/mail_client.js');

require('../models/accounts.js');
require('../models/profiles.js');
require('../models/accounttokens.js');

var Account 								= mongoose.model('Account');
var Profile 								= mongoose.model('Profile');
var AccountToken  					= mongoose.model('AccountToken');

var sendError 							= helpers.sendError;
var sendSuccess 						= helpers.sendSuccess;
var router 									= express.Router();
var logger        					= log4jsLogger.getLogger(log4jsConfigs.CATEGORY_ROUTES_ACCOUNT);

var ValidMobileDevicesType 	= [AccountToken.CLIENT.ANDROID,AccountToken.CLIENT.ANDROID,,AccountToken.CLIENT.IOS];

router.use(function(req, res, next){
	var token = req.body.uq_tokn;
	var aid   = req.body.a_id;
	if(!token){
		logger.error({"url":req.originalUrl,"msg":"token_not_found","p":req.body});
	  return sendError(res,"Access without token is not authorised","token_not_found",constants.HTTP_STATUS.FORBIDDEN);
	}
	req.decoded = null;
  jwt.verify(token, configs.JWT_USERQUEUE_TOKEN_PRIVATE_KEY, function(err, decoded) {      
    if(err){
    	logger.error({"url":req.originalUrl,"msg":"jwt_verify_error","p":req.body,"er":err});
      return sendError(res,"Failed to authenticate token.","token_auth_failure",constants.HTTP_STATUS.TOKEN_EXPIRED);  
    }
    if(decoded.id != aid){
			logger.error({"r":req.originalUrl,"msg":"aid_in_token_not_matching_with_accountid","p":req.body});
			return sendError(res,"Invalid Parameters","invalid_parameters",constants.HTTP_STATUS.BAD_REQUEST);
		}
    next();
  });
});



/*********************** Single Transaction  *******************************/
router.post("trxn_status",function(req, res, next){
	
});

router.post("refund_status",function(req, res, next){
	
});
/***************************************************************************/




/*********************** List All Transactions *****************************/
router.post("list_trxn",function(req, res, next){
	
});

router.post("list_pend_trxn",function(req, res, next){
	
});

router.post("list_rfnd_trxn",function(req, res, next){
	
});
/***************************************************************************/




/********************** List Account Transactions ***************************/
router.post("list_acnt_trxn",function(req, res, next){
	
});

router.post("list_acnt_trxn_pend",function(req, res, next){
	
});

router.post("list_acnt_trxn_rfnd",function(req, res, next){
	
});
/***************************************************************************/




/*********************** List Group Transaction ****************************/
router.post("list_grp_trxn",function(req, res, next){
	
});

router.post("list_grp_trxn_pend",function(req, res, next){
	
});

router.post("list_grp_trxn_rfnd",function(req, res, next){
	
});
/***************************************************************************/