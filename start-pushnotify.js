require('shelljs/global');
var app 												= require('express')();
var fs 													= require('fs');
var moment											= require('moment');
var configs 										= require('./utility/configs.js');

var dirname 										= __dirname;
var server_conf_filename 				= configs.RABBIT_PUSH_NOTIFY_SERVICE_NAME+'.conf'
var server_conf_full_filename 	= dirname+'/'+server_conf_filename;
var logDir 											= configs.RABBIT_PUSH_NOTIFY_SERVICE_LOG_DIR;
var logFile 										= logDir+'_$N/pushnotify-'+moment().format()+'.log';
var PushNotifyWorkerHeapSize    = configs.PUSH_NOTIFY_WORKER_HEAP_SIZE_MAX;
var NUM_OF_WORKERS              = configs.NUM_PUSH_NOTIFY_WORKERS;
var NodeStackSize               = configs.NODE_SERVER_STACK_SIZE_MAX;

rm('-rf',server_conf_full_filename);

var data = 'limit nofile 150000 150000\n\n';
    data += 'start on runlevel [2345]\n\n';
		data += 'setuid '+configs.RABBIT_PUSH_NOTIFY_SERVICE_USER+'\n\n';
		data += 'respawn\n\n';

data += 'instance $N\n\n'

if(app.get('env') !== 'debug') {
	data += 'pre-start script\n';
	data += ' exec /usr/bin/node '+dirname+'/rabbitmq/notify/pushnotify_restart.js 2>&1\n';
	data += 'end script\n\n';
}

data += 'exec /usr/bin/node --max_old_space_size='+PushNotifyWorkerHeapSize+' --stack-size='+NodeStackSize+' '+dirname+'/rabbitmq/worker/push_notify.js >> '+logFile+' 2>&1\n\n';

if(app.get('env') !== 'debug') {
	data += 'post-stop script\n';
	data += ' exec /usr/bin/node '+dirname+'/rabbitmq/notify/pushnotify_crash.js 2>&1\n';
	data += 'end script\n\n';
}

fs.appendFile(server_conf_full_filename, data, function(err){
	if (err) {
		console.trace(err);
		return;
	}
	mkdir('-p', logDir);

	// TO BE THOUGHT WHETEHR PERMISSONS LESSAR THATN 777 CAN BE GIVEN
	chmod(777, logDir);

	cp('-f',server_conf_full_filename, '/etc/init/'+server_conf_filename);
	var command = 'service '+configs.RABBIT_PUSH_NOTIFY_SERVICE_NAME+' start';
	console.log('Starting All Push Notify Workers');
	console.log('*******************************************************');
	console.log('\n');
	for(var i=1; i<= NUM_OF_WORKERS; i++){
		start_worker(command, i);
	}
});

function start_worker(command, i){
	var fCommand = command+' N='+i;
	exec(fCommand, function(status, output) {
		console.log('Worker : '+i);
		console.log('--------------------------------------');
	  console.log(' Exit status:', status);
	  console.log(' Program output:', output);
	  console.log('\n');
	  return;
	});
}