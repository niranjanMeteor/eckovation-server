var fs                        = require('fs');

var PluginConfigs = {

	CONFIGS_OVERWRITE_FILE                       : "configs_overwrite.js",
	USER_IDENTIFIER_TOKEN_VALIDITY_TIME          : 1*30*24*60*60,
	USER_IDENTIFIER_HCODE_LENGTH                 : 10,
	USER_IDENTIFIER_TOKEN_PRIVATE_KEY            : "",
	PLUGIN_NAME_MAX_LENGTH                       : 50,
	PLUGIN_DESC_MAX_LENGTH                       : 200,
	QUIZ_PLUGIN_HOME_URL                         : "",
	VIDEO_PLUGIN_HOME_URL                        : "",
	DEFAULT_THUMB_URL                            : "https://s3-ap-southeast-1.amazonaws.com/web-public-res/publicgroups/plugins/quiz_promo_thumb.jpg",
	DEFAULT_SHOWCASE_URL                         : "https://s3-ap-southeast-1.amazonaws.com/web-public-res/publicgroups/plugins/quiz_promo.jpg",
	DEFAULT_PROMO_URL                            : "https://s3-ap-southeast-1.amazonaws.com/web-public-res/publicgroups/plugins/payment_teaser.png"

};

var overwriteConfigFulFileName  = __dirname+'/'+PluginConfigs.CONFIGS_OVERWRITE_FILE;
if(fs.existsSync(overwriteConfigFulFileName)){
	var overwriteConfig  =  require(overwriteConfigFulFileName);
	for (var key in overwriteConfig){
		PluginConfigs[key] = overwriteConfig[key];
	}
} else {
	console.log('No Overwrite Configs File Found to overwrite any Plugin configs key');
}

module.exports = PluginConfigs;