const mongoose            = require('mongoose');
const readline 			      = require('readline');
const redis               = require('redis');

const configs             = require('./../utility/configs.js');
const RedisPrefixes       = require('./../utility/redis_prefix.js');
const Events              = require('./../utility/event_names.js');

const redis_client        = redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
    console.log("Error " + err);
});

require('../models/accounts.js');
require('../models/groups.js');
require('../models/groupmembers.js');
require('../models/plugins.js');
require('../models/groupplugins.js');
const Account             = mongoose.model('Account');
const Group 				      = mongoose.model('Group');
const GroupMember         = mongoose.model('GroupMember');
const GroupPlugin         = mongoose.model('GroupPlugin');

const rhmset              = require("./../utility/redis_hmset.js")(redis_client);
const io                  = require('socket.io-emitter')(redis_client);

exports.activate = function(group_id, plugin_id, cb){
  GroupPlugin.update({
    gid    : group_id,
    plgnid : plugin_id,
    act    : true
  },{
    status : GroupPlugin.STATUS.ACTIVE
  },function(err,created_group_plugin){
    if(err){
      return cb(err,null);
    }
    Group.update({
      _id : group_id
    },{
      plgn : true
    },function(err,updated_group){
      if(err){
        return cb(err,null);
      }
      rhmset.removefield(RedisPrefixes.GROUPS, group_id+'', function(err,group_cache_rem){
        if(err){
          return cb(err,null);
        }
        informGroupMembersAboutPlugin(group_id, 1, function(err,resp){
          if(err){
            return cb(err,null);
          }
          console.log('activate : informGroupMembersAboutPlugin :');
          console.log(resp);
          return cb(null,true);
        });
      });
    });
  });
};

exports.deactivate = function(group_id, plugin_id, cb){
  GroupPlugin.update({
    gid    : group_id,
    plgnid : plugin_id,
    act    : true
  },{
    status : GroupPlugin.STATUS.INACTIVE
  },function(err,created_group_plugin){
    if(err){
      return cb(err,null);
    }
    GroupPlugin.count({
      gid    : group_id,
      act    : true,
      status : GroupPlugin.STATUS.ACTIVE
    },function(err,plugins_count){
      if(err){
        return cb(err,null);
      }
      if(plugins_count == 0){
        Group.update({
          _id : group_id
        },{
          plgn : false
        },function(err,updated_group){
          if(err){
            return cb(err,null);
          }
          rhmset.removefield(RedisPrefixes.GROUPS, group_id+'', function(err,group_cache_rem){
            if(err){
              return cb(err,null);
            }
            informGroupMembersAboutPlugin(group_id, 0, function(err,resp){
              if(err){
                return cb(err,null);
              }
              console.log('deactivate : informGroupMembersAboutPlugin  : plugin_activate :');
              console.log(resp);
              return cb(null,true);
            });
          });
        });
      } else {
        Group.update({
          _id : group_id
        },{
          plgn : true
        },function(err,updated_group){
          if(err){
            return cb(err,null);
          }
          rhmset.removefield(RedisPrefixes.GROUPS, group_id+'', function(err,group_cache_rem){
            if(err){
              return cb(err,null);
            }
            informGroupMembersAboutPlugin(group_id, 1, function(err,resp){
              if(err){
                return cb(err,null);
              }
              console.log('deactivate : informGroupMembersAboutPlugin : plugin_activate :');
              console.log(resp);
              return cb(null,true);
            });
          });
        });
      }
    });
  });
};

exports.deletePlugin = function(group_id, plugin_id, cb){
  GroupPlugin.update({
    gid    : group_id,
    plgnid : plugin_id,
    act    : true
  },{
    act    : false,
    status : GroupPlugin.STATUS.DELETED
  },function(err,created_group_plugin){
    if(err){
      return cb(err,null);
    }
    GroupPlugin.count({
      gid    : group_id,
      act    : true,
      status : GroupPlugin.STATUS.ACTIVE
    },function(err,plugins_count){
      if(err){
        return cb(err,null);
      }
      if(plugins_count == 0){
        Group.update({
          _id : group_id
        },{
          plgn : false
        },function(err,updated_group){
          if(err){
            return cb(err,null);
          }
          rhmset.removefield(RedisPrefixes.GROUPS, group_id+'', function(err,group_cache_rem){
            if(err){
              return cb(err,null);
            }
            informGroupMembersAboutPlugin(group_id, 0, function(err,resp){
              if(err){
                return cb(err,null);
              }
              console.log('deletePlugin : informGroupMembersAboutPlugin  : plugin_deactivate :');
              console.log(resp);
              return cb(null,true);
            });
          });
        });
      } else {
        Group.update({
          _id : group_id
        },{
          plgn : true
        },function(err,updated_group){
          if(err){
            return cb(err,null);
          }
          rhmset.removefield(RedisPrefixes.GROUPS, group_id+'', function(err,group_cache_rem){
            if(err){
              return cb(err,null);
            }
            informGroupMembersAboutPlugin(group_id, 1, function(err,resp){
              if(err){
                return cb(err,null);
              }
              console.log('deletePlugin : informGroupMembersAboutPlugin : plugin_activate :');
              console.log(resp);
              return cb(null,true);
            });
          });
        });
      }
    });
  });
};


function informGroupMembersAboutPlugin(gid, state, cb){
	var tim = new Date().getTime();
  var packet = {
    id    : tim,
    gid   : gid,
    state : state
  };
  GroupMember.distinct('aid',{
    gid : gid,
    act : true,
    gdel: false
  },function(err,members){
    if(err){
      return cb(err,null);
    }
    var total = members.length;
    if(total == 0){
      return cb(null,true);
    }
    var done = 0;
    for(var i=0; i<total; i++){
      addInQueueAndEmit(members[i], gid, packet, function(err,resp){
        if(err){
          console.trace(err);
        }
        done++;
        if(done == total){
          console.log('To Emit packets for groupmembers : '+total);
          console.log('Successfully Emitted packets for groupmembers : '+done);
          return cb(null,true);
        }
      });
    }
  });
}

function addInQueueAndEmit(aid, gid, packet, cb){
  Account.findOne({
    _id : aid,
    vrfy: true,
    $or : [
      {
        vrsn: {$gte : configs.TARGET_VERSION_PLUGIN_ANDROID}
      },
      {
        iosv: {$gte : configs.TARGET_VERSION_PLUGIN_IOS}
      }
    ]
  },function(err,account){
    if(err){
      return cb(err,null);
    }
    if(!account){
      return cb(null,false);
    }
    var rPacket = JSON.stringify(packet);
    rhmset.set(RedisPrefixes.PLUGIN_STATE, aid, gid, rPacket, function(err,resp){
      if(err){
        return cb(err,null);
      }
      if(resp){
        io.to(aid).emit(Events.PLUGIN,packet);
        console.log('Following packet emitted for aid : '+aid);
        console.log(packet);
        return cb(null,true);
      }
      return cb(null,false);
    });
  });
}