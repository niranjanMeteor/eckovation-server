var GroupFeedConfigs = {
	NAME                  : "GroupFeed",
	PLUGIN_ID             : "5748a8afd615aad366b64bce",
	MIN_MESSAGE_LENGTH    : 10,
	MIN_IMAGE_URL_LENGTH  : 64
};

module.exports = GroupFeedConfigs;