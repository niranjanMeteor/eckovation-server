var crypto 					= require('crypto');
var log4jsLogger  	= require('../../loggers/log4js_module.js');
var log4jsConfigs   = require('../../loggers/log4js_configs.js');

var logger          = log4jsLogger.getLogger(log4jsConfigs.CATEGORY_UTILITY_EN_DE_CRYPT);

exports.encrypt = function(input, key, salt, algorithm) {
	var modifiedKey = key+salt; 
	try {       
		var hashedKey   = crypto.createHash('sha256').update(modifiedKey).digest('base64');
		var cipher      = crypto.createCipher(algorithm, hashedKey);  
		var encrypted   = cipher.update(input, 'utf8', 'hex') + cipher.final('hex');
		return encrypted;
	} catch(e) {
		logger.error({"FUNC":"plugin_encrypt","err":e.message,"p":{inp:input,k:key}});
		return null;
	} 
};