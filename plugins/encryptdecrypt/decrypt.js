var crypto 					= require('crypto');
var log4jsLogger  	= require('../../loggers/log4js_module.js');
var log4jsConfigs   = require('../../loggers/log4js_configs.js');

var logger          = log4jsLogger.getLogger(log4jsConfigs.CATEGORY_UTILITY_EN_DE_CRYPT);

exports.decrypt = function(input, key, salt, algorithm){
	var modifiedKey = key+salt;        
	try {
		var hashedKey   = crypto.createHash('sha256').update(modifiedKey).digest('base64');
		var decipher    = crypto.createDecipher(algorithm, hashedKey);
		var decrypted   = decipher.update(input, 'hex', 'utf8') + decipher.final('utf8');
		return decrypted;
	} catch(e) {
		logger.error({"FUNC":"plugin_decrypt","err":e.message,"p":{inp:input,k:key}});
		return null;
	}
};