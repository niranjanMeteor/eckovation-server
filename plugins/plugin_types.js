var PLUGIN_TYPES = [

	{
		type         : 1,
		name         : "Video",
		desc         : "Manage all kinds of Video lectures series using this plugin",
		thumb_url    : "https://s3-ap-southeast-1.amazonaws.com/web-public-res/publicgroups/plugins/quiz_promo_thumb.jpg"
	},
	{
		type         : 2,
		name         : "Quiz",
		desc         : "lets you create topic based tests, based on time and negative marking as well",
		thumb_url    : "https://s3-ap-southeast-1.amazonaws.com/web-public-res/publicgroups/plugins/quiz_promo_thumb.jpg"
	}

];

module.exports = PLUGIN_TYPES;