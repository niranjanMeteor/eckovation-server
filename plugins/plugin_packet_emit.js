const mongoose            = require('mongoose');
const redis               = require('redis');

const configs             = require('./../utility/configs.js');
const RedisPrefixes       = require('./../utility/redis_prefix.js');
const Events              = require('./../utility/event_names.js');

const redis_client        = redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
    console.log("Error " + err);
});

require('../models/accounts.js');
require('../models/groupmembers.js');
const Account             = mongoose.model('Account');
const GroupMember         = mongoose.model('GroupMember');

const rhmset              = require("./../utility/redis_hmset.js")(redis_client);
const io                  = require('socket.io-emitter')(redis_client);

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  console.log('Shutting Down the User Trace');
  console.log('User Session Terminated');  
  process.exit(0);
});
mongoose.connection.on('connected', function(){
  console.log('Connected to mongodb!');
  console.log('Will Will Packet about Group Plugin Change to Every GroupMember');
  startPluginPacketEmission();
});

var GROUP_ID     = "";
var PLUGIN_STATE = 1;

function startPluginPacketEmission(){
  if(!GROUP_ID){
    console.log('GROUP_ID IS NOT SET IN SCRIPT');
    process.exit(0);
  }
  if(typeof(PLUGIN_STATE) == undefined){
    console.log('PLUGIN_STATE IS NOT SET IN SCRIPT');
    process.exit(0);
  }
  informGroupMembersAboutPlugin(GROUP_ID, PLUGIN_STATE, function(err,resp){
    if(err){
      console.trace(err);
      process.exit(0);
    }
    console.log('Success Response from informScript : ');
    console.log(resp);
    process.exit(0);
  });
}


function informGroupMembersAboutPlugin(gid, state, cb){
	var tim = new Date().getTime();
  var packet = {
    id    : tim,
    gid   : gid,
    state : state
  };
  GroupMember.distinct('aid',{
    gid : gid,
    act : true,
    gdel: false
  },function(err,members){
    if(err){
      return cb(err,null);
    }
    var total = members.length;
    if(total == 0){
      return cb(null,true);
    }
    var done = 0;
    for(var i=0; i<total; i++){
      addInQueueAndEmit(members[i], gid, packet, function(err,resp){
        if(err){
          console.trace(err);
        }
        done++;
        if(done == total){
          console.log('To Emit packets for groupmembers : '+total);
          console.log('Successfully Emitted packets for groupmembers : '+done);
          return cb(null,true);
        }
      });
    }
  });
}

function addInQueueAndEmit(aid, gid, packet, cb){
  Account.findOne({
    _id : aid,
    vrfy: true,
    $or : [
      {
        vrsn: {$gte : configs.TARGET_VERSION_PLUGIN_ANDROID}
      },
      {
        iosv: {$gte : configs.TARGET_VERSION_PLUGIN_IOS}
      }
    ]
  },function(err,account){
    if(err){
      return cb(err,null);
    }
    if(!account){
      return cb(null,false);
    }
    var rPacket = JSON.stringify(packet);
    rhmset.set(RedisPrefixes.PLUGIN_STATE, aid, gid, rPacket, function(err,resp){
      if(err){
        return cb(err,null);
      }
      if(resp){
        io.to(aid).emit(Events.PLUGIN,packet);
        console.log('Following packet emitted for aid : '+aid);
        console.log(packet);
        return cb(null,true);
      }
      return cb(null,false);
    });
  });
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : configs.MONGO_SERVER_KEEPALIVE
      }
    }
  };
  if(!configs.MONGO_UNAME || configs.MONGO_UNAME == ""){
    options.server.poolSize = configs.MONGO_LOWER_POOLSIZE;
    console.log('[[[[[[[    NO MONGO_UNAME FOUND IN CONFIGS, WILL CONNNECT WITHOUT REPLICA OPTIONS     ]]]]]]]]]');
    console.log('[[[[[[[    MONGODB CONNECTION POOL_SIZE : '+configs.MONGO_LOWER_POOLSIZE+'     ]]]]]]]]]');
    return mongoose.connect("mongodb://"+configs.MONGO_HOST+":"+configs.MONGO_PORT+"/"+configs.DB_NAME,options);
  }
  options.replset = {
    rs_name : configs.MONGO_REPLICA_SET_NAME,
    socketOptions : {
      keepAlive : configs.MONGO_REPLICA_KEEPALIVE
    },
    poolSize : configs.MONGO_LOWER_POOLSIZE
  };
  var connect_string = "mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@";
  var mongo_hosts    = configs.MONGO_HOST;
  var mongo_port     = configs.MONGO_PORT;
  for(var i=0; i<mongo_hosts.length; i++){
    if(i > 0)
      connect_string += ",";
    connect_string += mongo_hosts[i]+":"+mongo_port;
  }
  connect_string += "/"+configs.DB_NAME;
  console.log('[[[[[[[     CONNECTING MONGODB WITH REPLICA_SET OPTIONS     ]]]]]]]]]');
  console.log('[[[[[[[     MONGODB CONNECTION POOL_SIZE : '+configs.MONGO_LOWER_POOLSIZE+'      ]]]]]]]]]');
  return mongoose.connect(connect_string,options);
}