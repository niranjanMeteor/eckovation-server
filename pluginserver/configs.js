var fs                        = require('fs');

var ServerConfigs = {

	SRV_CONFIGS_OVERWRITE_FILE        : "configs_overwrite.js",
	VIDEO_PLUGIN_API_KEY              : "",
	TEST_PLUGIN_API_KEY               : "",
	ECKOVATION_GROUPJOIN_API_KEY      : "",
	REQUEST_API_EXPIRY_TIME           : 5000,
	VIDEO_PLUGIN_SERVER_HOST          : "",
	VIDEO_PLUGIN_SERVER_PORT          : "",
	QUIZ_PLUGIN_SERVER_HOST           : "",
	QUIZ_PLUGIN_SERVER_PORT           : "",
	VIDEO_PLUGIN_SERVER_API_PATH      : "",
	QUIZ_PLUGIN_SERVER_API_PATH       : ""

};

var overwriteConfigFulFileName  = __dirname+'/'+ServerConfigs.SRV_CONFIGS_OVERWRITE_FILE;
if(fs.existsSync(overwriteConfigFulFileName)){
	var overwriteConfig  =  require(overwriteConfigFulFileName);
	for (var key in overwriteConfig){
		ServerConfigs[key] = overwriteConfig[key];
	}
} else {
	console.log('No Overwrite Configs File Found to overwrite any Server configs key');
}

module.exports = ServerConfigs;