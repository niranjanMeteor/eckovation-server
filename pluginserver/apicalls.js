var https             			      		= require('https');
var http             			        		= require('http');

var ServerConfigs                			= require('./configs.js');
var ApiPaths                      		= require('./apipaths.js');

exports.postcallback = function(host, port, api_path, parameter, identifier, activity, cb){
	var params  = {
		parameters : parameter              
	};

	var options = {
	  host       : host,
	  port       : port,
	  path       : api_path,
	  method     : 'POST'
	};

	var data = JSON.stringify(params);
	options.headers = {
    'Content-Type': 'application/json',
    'Content-Length': Buffer.byteLength(data)
  };

	var req = http.request(options);
	req.write(data);

	var responseData = '';
	req.on('response', function(res){
	  res.on('data', function(chunk){
	   responseData += chunk;
	  });
	  res.on('end', function(){
	    var decodedResponse = JSON.parse(responseData);
	    console.log(decodedResponse);
	    if(decodedResponse.success == true){
	    	return cb(null,true);
	    }
	    console.log(' Transaction Activity : '+activity);
    	console.log('Error In Calling postcallback for VideoPlugin Server : '+parameter);
    	return cb(decodedResponse,false);
	  });
	});
	req.end();
	req.on('error', function(err){
		console.log('Error Occurred in Verifying Identifier for identifier : '+identifier+', activity : '+activity);
    console.log(err);
    return cb(null,{success:false});
  });
};