require('shelljs/global');
var configs           = require('./utility/configs.js');

var command = 'service '+configs.RABBIT_MEDIA_SERVICE_NAME+' stop';
exec(command, function(status, output) {
  console.log('Exit status:', status);
  console.log('Program output:', output);
});