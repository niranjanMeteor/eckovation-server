var mongoose            = require('mongoose');
var redis               = require('redis');

var configs             = require('../utility/configs.js');
var RedisPrefixes       = require('../utility/redis_prefix.js');


var redis_client        = redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
    console.log("Error " + err);
});

var rhmset              = require("../utility/redis_hmset.js")(redis_client);

require('../models/accountdata.js');
require('../models/groupmembers.js');

var AccountData         = mongoose.model('AccountData'); 
var GroupMember         = mongoose.model('GroupMember'); 

exports.getUserAccountDatas = function(aid, cb){
    var aid = aid+'';
    var key = RedisPrefixes.ACCOUNTDATA;
    rhmset.get(key,aid,function(err,rAccountDatas){
        if(err) { 
            return cb(err,null); 
        }
        if(rAccountDatas){
            var toReturn = JSON.parse(rAccountDatas);
            return cb(null, toReturn);
        } else {
            AccountData.find({
                aid : aid,
                act : true
            },{
                _id  : 1,
                earn : 1,
                stim : 1,
                dtyp : 1,
                aid  : 1,
                dblc : 1
            },function(err, accountdatas) {
                if(err) { 
                    return cb(err,null); 
                }
                cb(null, accountdatas);
                if(accountdatas.length == 0) return;
                var toStore = JSON.stringify(accountdatas);
                rhmset.setMongoObj(key,aid,toStore,function(err,resp){
                    if(err){
                        console.trace(err);
                    } else if(!resp) {
                        console.log('Could not set user accountdatas in redis hmset for account id : '+aid);
                    }
                    return;
                });
            });
        }
    });
};

exports.getGroupEndpoints = function(gid, cb){
  var tim    = new Date().getTime();
  var key    = gid+'';
  var prefix = RedisPrefixes.GROUP_ACCOUNTDATA;
  rhmset.m4get(prefix+key,"arns","pids","tim","muttm", function(err,rGroupAccountDatas){
    if(err) { 
      return cb(err,null); 
    }
    if(rGroupAccountDatas.length == 4 && rGroupAccountDatas[0] && rGroupAccountDatas[1] && rGroupAccountDatas[2] && rGroupAccountDatas[3]){
      var toReturn = {
        arns : JSON.parse(rGroupAccountDatas[0]),
        pids : JSON.parse(rGroupAccountDatas[1]),
        tim  : rGroupAccountDatas[2],
        muttm: JSON.parse(rGroupAccountDatas[3])
      };
      return cb(null, toReturn);
    }
    GroupMember.find({
      gid  : gid,
      act  : true,
      gdel : false,
      type : {$ne : GroupMember.TYPE.BANNED}
    },{
      aid : 1,
      pid : 1,
      type: 1,
      muttm: 1,
      _id : 0
    },function(err,groupmembers){
      if(err){
        return cb(err,null);
      }
      var total = groupmembers.length;
      if(total == 0){
        return cb(null,{arns:[],pids:[],tim:'',muttm : []});
      }
      var aids = [];
      var pids = {};
      var muttm = {};
      var aid,pid;
      for(var i=0; i<total; i++){
        aid = groupmembers[i].aid+'';
        pid = groupmembers[i].pid+'';
        if(aids.indexOf(aid) < 0){
          aids.push(aid);
        }
        if(!pids[aid]){
          pids[aid] = pid;
        } else if(groupmembers[i].type == GroupMember.TYPE.ADMIN){
          pids[aid] = pid;
        }
        if(!muttm[aid]){
          muttm[aid] = groupmembers[i].muttm;
        }
      }
      groupmembers = null;
      AccountData.find({
        aid : { $in : aids },
        act : true
      },{
        earn : 1,
        dtyp : 1,
        aid  : 1,
        _id  : 0 
      },function(err,accountdatas){
        if(err){
          return cb(err,null);
        }
        aids = null;
        var total = accountdatas.length;
        if(total == 0){
          return cb(null,{arns:[],pids:[],tim:'',muttm:[]});
        }
        rhmset.m4set(
          prefix,
          key,
          "arns",JSON.stringify(accountdatas),
          "pids",JSON.stringify(pids),
          "tim", tim,
          "muttm",JSON.stringify(muttm),
          function(err,resp){
          if(err){
            console.trace(err);
          } else if(!resp) {
            console.log('Could not set group accountdatas in redis hmset for group id : '+gid);
          }
        });
        return cb(null, {
          arns : accountdatas,
          pids : pids,
          tim  : tim,
          muttm: muttm
        });
      });
    });
  });
};

exports.updatePrevTimeForGroupEndpoints = function(gid, tim, cb){
  var gid = gid+'';
  var key = RedisPrefixes.GROUP_ACCOUNTDATA+gid;
  rhmset.exists(key,function(err,isKey){
    if(err){
      return cb(err,null);
    }
    if(isKey){
      rhmset.updateField(key,"tim", tim, function(err,resp){
        if(err){
          return cb(err,null);
        } 
        if(resp){
          console.log("Successfully updated the  prev Time for Group AccountData key for Group ID : "+gid);
          return cb(null,true);
        } else {
          console.log("Oops,this is unfortunate that prev Time for Group AccountData key could not be updated for Group ID : "+gid);
          return cb(null,false);
        }
      });
    } else {
      console.log("Oops,this is unfortunate that group AccountData key does not exists for Group ID : "+gid);
      return cb(null,false);
    }
  });
};

exports.removeUserAccountDatas = function(aid, cb){
    var aid = aid+'';
    var key = RedisPrefixes.ACCOUNTDATA;
    rhmset.keyexists(key,aid,function(err,isKey){
        if(err){
            return cb(err,null);
        }
        if(isKey){
            rhmset.removefield(key,aid,function(err,resp){
                if(err){
                    return cb(err,null);
                } 
                if(resp){
                    return cb(null,true);
                } else {
                    console.log("Oops,this is unfortunate that User AccountData key could not be deleted for Account : "+aid);
                    return cb(null,false);
                }
            });
        } else {
            console.log("Oops,this is unfortunate that AccountData key does not exists for Account : "+aid);
            return cb(null,false);
        }
    });
};

function removeGroupAccountDatas(gid, cb){
  var gid = gid+'';
  var key = RedisPrefixes.GROUP_ACCOUNTDATA+gid;
  rhmset.exists(key,function(err,isKey){
    if(err){
      return cb(err,null);
    }
    if(isKey){
      rhmset.removekey(key,function(err,resp){
        if(err){
          return cb(err,null);
        } 
        if(resp){
          return cb(null,true);
        } else {
          console.log("Oops,this is unfortunate that Group AccountData key could not be deleted for Group ID : "+gid);
          return cb(null,false);
        }
      });
    } else {
      console.log("Oops,this is unfortunate that group AccountData key does not exists for Group ID : "+gid);
      return cb(null,false);
    }
  });
};
exports.removeGroupAccountDatas = removeGroupAccountDatas;

exports.removeMultiGroupAccountDatas = function(gids, cb){
  var toDo = gids.length;
  if(toDo == 0){
    return cb(null,true);
  }
  var done = 0;
  for(var i=0; i<toDo; i++){
    removeGroupAccountDatas(gids[i],function(err,resp){
      if(err){
        return cb(err,null);
      }
      done++;
      if(done == toDo){
        console.log('All Groups AccountData Cleared From Cache');
        return cb(null,true);
      }
    });
  }
};