var mongoose 								= require('mongoose');
var AppClients							= require('./app_clients.js');
var configs			  					= require('./configs.js');
var UserInfoCache       	  = require('./user_info_cache.js');
var TargetVersionInfo        = require('./target_versions.js');
var TargetVersionActions     = require('./target_version_actions.js');

require('../models/accounts.js');
require('../models/profiles.js');
var Account 			= mongoose.model('Account');
var Profile 		  = mongoose.model('Profile');

var APP_BASE_VERSION_MOBILE  = configs.APP_BASE_VERSION_MOBILE;
var APP_BASE_VERSION_ANDROID = configs.APP_BASE_VERSION_ANDROID;
var APP_BASE_VERSION_IOS     = configs.APP_BASE_VERSION_IOS;
var APP_BASE_VERSION_WEB     = configs.APP_BASE_VERSION_WEB; 
var APP_BASE_VERSION_WINDOWS = configs.APP_BASE_VERSION_WINDOWS;

exports.updateVersionAndClientSeen = function(account_id, client, version, tim){
	if(typeof(client) === 'undefined'){
		client = AppClients.ANDROID;
	}
	var update;
	switch (client) {
		case AppClients.MOBILE:
			update = {
				lsnm : tim,
				vrsn : version
			};
			break;
		case AppClients.ANDROID:
			update = {
				lsnm : tim,
				vrsn : version
			};
			break;
		case AppClients.IOS:
			update = {
				lsni : tim,
				iosv : version
			};
			break;
		case AppClients.WINDOWS:
			update = {
				lsnwn : tim,
				wndv  : version
			};
			break;
		case AppClients.WEB:
			update = {
				lsnw  : tim,
				webv  : version
			};
			break;
	}
	if(typeof(update) !== 'undefined'){
		Account.update({
			_id : account_id
		}, update, function(err,updated_account){
			if (err) { 
				console.trace(err); 
				return;
			}
			// console.log('Successfully updated for account id : '+account_id+' , with version : '+version+' , client : '+client);
			return;
		});
 	} else {
 		console.trace('No Update object Created for account id : '+account_id+' , with version : '+version+' , client : '+client);
 		return;
 	}
}

exports.updateAccountVersion = function(aid, client, version) {
	Account.findOne({
		_id : aid
	},function(err,account){
		if (err) { console.trace(err); }
		if (!account) { console.trace('In updateAccountVersion : No Account found'); return; }
		var update = getVersionUpdateObj(client, version);
		Account.update({
			_id : account._id
		}, update, function(err,updated_account){
			if (err) { console.trace(err); }
			if (!updated_account) { console.trace('In updateAccountVersion : Version could not be saved'); return; }
			console.log('account versino update aid : '+aid);
			console.log('account version update version : '+version);
			console.log('In updateAccountVersion : New Version updated succesfully');
		});
	});
};

exports.updateAccountSeenByClient = function(aid , client, tim) {
	Account.findOne({
		_id : aid
	},function(err,account){
		if (err) { console.trace(err); }
		if (!account) { console.trace('In updateAccountSeenByClient : No Account found'); return; }
		var update;
		switch (client) {
			case AppClients.MOBILE:
				if (account.lsnm > tim) { console.trace('Invalid time for last seen mobile client update'); return; }
				update = {
					lsnm : tim
				}
				break;
			case AppClients.ANDROID:
				if (account.lsnm > tim) { console.trace('Invalid time for last seen mobile client update'); return; }
				update = {
					lsnm : tim
				}
				break;
			case AppClients.IOS:
				if (account.lsni > tim) { console.trace('Invalid time for last seen mobile client update'); return; }
				update = {
					lsni : tim
				}
				break;
			case AppClients.WINDOWS:
				if (account.lsnwn > tim) { console.trace('Invalid time for last seen mobile client update'); return; }
				update = {
					lsnwn : tim
				}
				break;
			case AppClients.WEB:
				if (account.lsnw > tim) { console.trace('Invalid time for last seen web client update'); return; }
				update = {
					lsnw : tim
				}
				break;
		}
		Account.update({
			_id : account._id
		}, update, function(err,updated_account){
			if (err) { console.trace(err); }
			if (!updated_account) { console.trace('In updateAccountSeenByClient : account seen info not updated'); return; }
			console.log('In updateAccountSeenByClient : account seen info updated succesfully');
		});
	});
};


exports.isValidAppVersionToProceed =  function(aid, version) {
	Account.findOne({
		_id : aid
	},function(err,account){
		if (err) { console.trace(err); }
		if (!account) { console.trace('In updateAccountVersion : No Account found'); return false; }
		var old_components = account.vrsn.split(".");
		var new_components = version.split(".");
    if ( parseInt(old_components[0]) > parseInt(new_components[0]) ) return false;
    if ( parseInt(old_components[1]) > parseInt(new_components[1]) ) return false;
    return true;
	});
};

exports.validateVersionForAdminActions = function(pid, cb) {
	Profile.findOne({
		_id : pid,
		act : true
	},function(err, profile) {
		if (err) { console.trace(err); return(true, false); }
		if (!profile) { 
			console.trace(' In validateVersionForAdminActions : Profile Not Found for pid : '+pid); 
			return cb(null, false);
		}
		Account.findOne({
			_id : profile.aid
		},function(err,account){
			if (err) { console.trace(err); return cb(true, false);}
			if (!account) { 
				console.trace('In validateVersionForAdminActions : No Account found for aid : '+profile.aid);
				return cb(null, false);
			}
	    if(checkVersionValidityFromAllAccounts(account.vrsn, account.iosv, account.webv, account.wndv)){
				return cb(null, true);
	    } else {
	    	return cb(null, false);
	    }
		});
	});
};

function checkVersionValidityFromAllAccounts(vrsn, iosv, webv, wndv){
	var if_no_vrsns = true;
	if(typeof(vrsn) !== 'undefined'){
		if_no_vrsns = false;
		if(!(checkIfUserVrsnCorrect(APP_BASE_VERSION_MOBILE,vrsn) || checkIfUserVrsnCorrect(APP_BASE_VERSION_ANDROID,vrsn))){
				return false;
		}
	}
	if(typeof(iosv) !== 'undefined'){
		if_no_vrsns = false;
		if(!checkIfUserVrsnCorrect(APP_BASE_VERSION_IOS,iosv)){
			return false;
		}
	}
	if(typeof(webv) !== 'undefined'){
		if_no_vrsns = false;
		if(!checkIfUserVrsnCorrect(APP_BASE_VERSION_WEB,webv)){
			return false;
		}
	}
	if(typeof(wndv) !== 'undefined'){
		if_no_vrsns = false;
		if(!checkIfUserVrsnCorrect(APP_BASE_VERSION_WINDOWS,wndv)){
			return false;
		}
	}
	if(if_no_vrsns)
		return false;
	else return true;
}

exports.validateVersionForRoute = function(pid, activity, cb) {
	UserInfoCache.getMongoProfile(pid,function(err, profile) {
		if(err){ 
			return(err, false); 
		}
		if(!profile){ 
			return cb("profile_not_found", false);
		}
		Account.findOne({
			_id : profile.aid
		},function(err,account){
			if(err){ 
				return cb("account_find_server_error", false);
			}
			if(!account){ 
				return cb("account_not_found", false);
			}
			var targetVersion = getAllTargetForActivity(activity);
			if(!targetVersion){
				return cb("no_target_found_for_activity_"+activity,false);
			}
	    if(checkAllVersionValidity(account.vrsn, account.iosv, account.webv, account.wndv, targetVersion)){
				return cb(null, true);
	    } else {
	    	return cb(null, false);
	    }
		});
	});
};

function checkAllVersionValidity(vrsn, iosv, webv, wndv, TARGET_VERSION){
	var if_no_vrsns = true;
	if(typeof(vrsn) !== 'undefined'){
		if_no_vrsns = false;
		if(!(checkIfUserVrsnCorrect(TARGET_VERSION.m,vrsn) || checkIfUserVrsnCorrect(TARGET_VERSION.a,vrsn))){
				return false;
		}
	}
	if(typeof(iosv) !== 'undefined'){
		if_no_vrsns = false;
		if(!checkIfUserVrsnCorrect(TARGET_VERSION.i,iosv)){
			return false;
		}
	}
	if(typeof(webv) !== 'undefined'){
		if_no_vrsns = false;
		if(!checkIfUserVrsnCorrect(TARGET_VERSION.wb,webv)){
			return false;
		}
	}
	if(typeof(wndv) !== 'undefined'){
		if_no_vrsns = false;
		if(!checkIfUserVrsnCorrect(TARGET_VERSION.wn,wndv)){
			return false;
		}
	}
	if(if_no_vrsns)
		return false;
	else return true;
}

function checkIfUserVrsnCorrect(req_vrsn, user_vrsn){
	var required     = req_vrsn.split(".");
	var components   = user_vrsn.split(".");

	var req_vrsn_p1  = parseInt(required[0]);
	var req_vrsn_p2  = parseInt(required[1]);

	var user_vrsn_p1 = parseInt(components[0]);
	var user_vrsn_p2 = parseInt(components[1]);

	if(user_vrsn_p1 < req_vrsn_p1) {
		return false;
	}
	if ( (user_vrsn_p1 > req_vrsn_p1) || ((user_vrsn_p1 == req_vrsn_p1) && (user_vrsn_p2 >= req_vrsn_p2)) ) {
		return true;
	} 
	return false;
}

function getAllTargetForActivity(activity){
	switch(activity){
		case TargetVersionActions.GROUP_DELETE:
			return TargetVersionInfo.GROUP_DELETE;
		default:
			return TargetVersionInfo.APP_BASE_VERSION;
	}
}

function getVersionUpdateObj(client, version) {
	var update;
	switch (client) {
		case AppClients.MOBILE:
			update = {
				vrsn : version
			}
			break;
		case AppClients.ANDROID:
			update = {
				vrsn : version
			}
			break;
		case AppClients.IOS:
			update = {
				iosv : version
			}
			break;
		case AppClients.WINDOWS:
			update = {
				wndv : version
			}
			break;
		case AppClients.WEB:
			update = {
				webv : version
			}
			break;
	}
	return update;
}
