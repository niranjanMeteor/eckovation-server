var redis               = require('redis');

var configs					    = require('./../configs.js');
var RedisPrefixes 			= require('./../redis_prefix.js');

var redis_client        = redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
  console.log("Error " + err);
});

var rhmset              = require("./../redis_hmset.js")(redis_client);

var ALLOWED_GROUP_JOIN_ATTEMPTS        = configs.ALLOWED_GROUP_JOIN_ATTEMPTS;
var ALLOWED_FAILED_GROUP_JOIN_ATTEMPTS = configs.ALLOWED_FAILED_GROUP_JOIN_ATTEMPTS;
var TIME_TO_PAUSE_GROUP_JOIN_ATTEMPTS  = configs.TIME_TO_PAUSE_GROUP_JOIN_ATTEMPTS;
var TIME_TO_PAUSE_FAILED_GROUP_JOIN_ATTEMPTS  = configs.TIME_TO_PAUSE_FAILED_GROUP_JOIN_ATTEMPTS;

exports.check = function(aid, tim, cb){
	var key = RedisPrefixes.BRUTE_FORCE_GROUP_JOIN+aid+'';
	rhmset.getvals(key,function(err, bruteForceData){
		if(err){
			return cb(err,null);
		}
		if(!bruteForceData){
			return cb(null,false);
		}
		var numberOfGrpJnAttempts         = bruteForceData[0];
		var lastGrpJnTimeWhenLimit        = bruteForceData[1];
		var lastGrpJnTime                 = bruteForceData[2];
		var numberOfFailedGrpJnAttempts   = bruteForceData[3];
		var lastFailedGrpJnTimeWhenLimit  = bruteForceData[4];

		var alpha,factor,alpha_error,factor_error;
		if(numberOfFailedGrpJnAttempts && lastFailedGrpJnTimeWhenLimit){
			alpha_error  = parseInt(numberOfFailedGrpJnAttempts /  ALLOWED_FAILED_GROUP_JOIN_ATTEMPTS)-1;
			factor_error = Math.pow(2,alpha_error);
			if(numberOfFailedGrpJnAttempts >= ALLOWED_FAILED_GROUP_JOIN_ATTEMPTS && 
					((tim - lastFailedGrpJnTimeWhenLimit) < (factor_error*TIME_TO_PAUSE_FAILED_GROUP_JOIN_ATTEMPTS))){
				return cb(null,{lmt:numberOfFailedGrpJnAttempts,tm:lastFailedGrpJnTimeWhenLimit});
			}
		}

		alpha  = parseInt(numberOfGrpJnAttempts /  ALLOWED_GROUP_JOIN_ATTEMPTS)-1;
		factor = Math.pow(2,alpha);
		if(numberOfGrpJnAttempts >= ALLOWED_GROUP_JOIN_ATTEMPTS && 
				((tim - lastGrpJnTimeWhenLimit) < (factor*TIME_TO_PAUSE_GROUP_JOIN_ATTEMPTS))){
			return cb(null,{lmt:numberOfGrpJnAttempts,tm:lastGrpJnTimeWhenLimit});
		}
		return cb(null,false);
	});
};

exports.mark = function(aid, tim, cb){
	var key = RedisPrefixes.BRUTE_FORCE_GROUP_JOIN+aid+'';
	rhmset.get(key, "lmt", function(err, grpJnAttemptsTillNow){
		if(err){
			return cb(err,null);
		}
		if(!grpJnAttemptsTillNow){
			rhmset.updtGrpJnAtmptTim(key, "lmt", 1, "ltm", tim,  "tm", tim, function(err,resp){
				if(err){
					return cb(err,null);
				}
				if(resp){
					return cb(null,true);
				} else {
					return cb("first_groupjoin_attempt_and_tim_not_updated",null);
				}
			});
		} else {
			var lmt = parseInt(grpJnAttemptsTillNow)+1;
			if((lmt % ALLOWED_GROUP_JOIN_ATTEMPTS) == 0){
				rhmset.updtGrpJnAtmptTim(key, "lmt", lmt, "ltm", tim, "tm", tim, function(err,resp){
					if(err){
						return cb(err,null);
					}
					if(resp){
						return cb(null,true);
					} else {
						return cb("last_groupjoin_attempt_and_tim_not_updated",null);
					}
				});
			} else {
				rhmset.updtGrpJnAtmpt(key, "lmt", lmt, "tm", tim, function(err,resp){
					if(err){
						return cb(err,null);
					}
					if(resp){
						return cb(null,true);
					} else {
						return cb("last_groupjoin_attempt_not_updated",null);
					}
				});
			}
		}
	});
};

exports.markError = function(aid, tim, cb){
	var key = RedisPrefixes.BRUTE_FORCE_GROUP_JOIN+aid+'';
	rhmset.get(key, "elmt", function(err, failedGrpJnAttemptsTillNow){
		if(err){
			return cb(err,null);
		}
		if(!failedGrpJnAttemptsTillNow){
			rhmset.updtGrpJnAtmptTim(key, "elmt", 1, "eltm", tim,  "tm", tim, function(err,resp){
				if(err){
					return cb(err,null);
				}
				if(resp){
					return cb(null,true);
				} else {
					return cb("first_failed_groupjoin_attempt_and_tim_not_updated",null);
				}
			});
		} else {
			var elmt = parseInt(failedGrpJnAttemptsTillNow)+1;
			if((elmt % ALLOWED_FAILED_GROUP_JOIN_ATTEMPTS) == 0){
				rhmset.updtGrpJnAtmptTim(key, "elmt", elmt, "eltm", tim, "tm", tim, function(err,resp){
					if(err){
						return cb(err,null);
					}
					if(resp){
						return cb(null,true);
					} else {
						return cb("last_failed_groupjoin_attempt_and_tim_not_updated",null);
					}
				});
			} else {
				rhmset.updtGrpJnAtmpt(key, "elmt", elmt, "tm", tim, function(err,resp){
					if(err){
						return cb(err,null);
					}
					if(resp){
						return cb(null,true);
					} else {
						return cb("last_groupjoin_attempt_not_updated",null);
					}
				});
			}
		}
	});
};

exports.unmark = function(aid, cb){
	var key = RedisPrefixes.BRUTE_FORCE_GROUP_JOIN+aid+'';
	rhmset.removekey(key,function(err,resp){
		if(err){
			return cb(err,null);
		}
		if(resp){
			return cb(null,true);
		} else {
			return cb(null,"aid_not_marked_in_brute_force_grp_join");
		}
	});
};