var AppClients					= require('./../app_clients.js');

exports.check = function(id, decoded){
	if(typeof(decoded) === 'undefined' || decoded == null){
		return true;
	} 
	if(typeof(decoded.id) === 'undefined' || decoded.id == null){
		return true;
	}
	return id == decoded.id;
}

exports.isIos = function(id, decoded){
	if(typeof(decoded) === 'undefined' || decoded == null){
		return false;
	} 
	if(typeof(decoded.id) === 'undefined' || decoded.id == null || typeof(decoded.cl) === 'undefined' || decoded.cl == null){
		return false;
	}
	if(id != decoded.id || decoded.cl != AppClients.IOS){
		return false;
	}
	return true;
}