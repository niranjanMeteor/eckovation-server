var redis               = require('redis');

var configs					    = require('./../configs.js');
var RedisPrefixes 			= require('./../redis_prefix.js');

var redis_client        = redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
  console.log("Error " + err);
});

var rhmset              = require("./../redis_hmset.js")(redis_client);

var ALLOWED_REQUEST_OTP_ATTEMPTS        = configs.ALLOWED_REQUEST_OTP_ATTEMPTS;
var TIME_TO_PAUSE_REQUEST_OTP_ATTEMPTS  = configs.TIME_TO_PAUSE_REQUEST_OTP_ATTEMPTS;


exports.checkOtpElgibility = function(phone, tim, cb){
	var key = RedisPrefixes.BRUTE_FORCE_PHONE_OTP+phone+'';
	rhmset.getOtpAtmpt(key,function(err, phoneOtpData){
		if(err){
			return cb(err,null);
		}
		if(!phoneOtpData){
			return cb(null,false);
		}
		var numberOfTotalOtpAttempts   = phoneOtpData[0];
		var lastOtpTimeWhenLimit       = phoneOtpData[1];
		var lastOtpTime                = phoneOtpData[2];
		var alpha = parseInt(numberOfTotalOtpAttempts /  ALLOWED_REQUEST_OTP_ATTEMPTS)-1;
		var factor  = Math.pow(2,alpha);
		if(numberOfTotalOtpAttempts >= ALLOWED_REQUEST_OTP_ATTEMPTS && 
				((tim - lastOtpTimeWhenLimit) < (factor*TIME_TO_PAUSE_REQUEST_OTP_ATTEMPTS))){
			return cb(null,{lmt:numberOfTotalOtpAttempts,tm:lastOtpTimeWhenLimit});
		}
		return cb(null,false);
	});
};

exports.updtOtpAtmpt = function(phone, tim, cb){
	var key = RedisPrefixes.BRUTE_FORCE_PHONE_OTP+phone+'';
	rhmset.get(key, "tlmt", function(err, otpAttemptsTillNow){
		if(err){
			return cb(err,null);
		}
		if(!otpAttemptsTillNow){
			rhmset.updtOtpAtmptTim(key, "tlmt", 1, "ltm", tim,  "tm", tim, function(err,resp){
				if(err){
					return cb(err,null);
				}
				if(resp){
					return cb(null,true);
				} else {
					return cb("first_otp_attempt_and_tim_not_updated",null);
				}
			});
		} else {
			var tlmt = parseInt(otpAttemptsTillNow)+1;
			if((tlmt % ALLOWED_REQUEST_OTP_ATTEMPTS) == 0){
				rhmset.updtOtpAtmptTim(key, "tlmt", tlmt, "ltm", tim, "tm", tim, function(err,resp){
					if(err){
						return cb(err,null);
					}
					if(resp){
						return cb(null,true);
					} else {
						return cb("last_otp_attempt_and_tim_not_updated",null);
					}
				});
			} else {
				rhmset.updtOtpAtmpt(key, "tlmt", tlmt, "tm", tim, function(err,resp){
					if(err){
						return cb(err,null);
					}
					if(resp){
						return cb(null,true);
					} else {
						return cb("last_otp_attempt_not_updated",null);
					}
				});
			}
		}
	});
};

exports.resetOtpAtmpt = function(phone, cb){ 
	var tim
	var key = RedisPrefixes.BRUTE_FORCE_PHONE_OTP+phone+'';
	rhmset.resetOtpAtmpt(key, "tlmt", 0, function(err,resp){
		if(err){
			return cb(err,null);
		}
		if(resp){
			return cb(null,true);
		} else {
			return cb("resetOtpAtmpt_failure",null);
		}
	});
};

// exports.updtErOtpAtmpt = function(phone, tim, cb){
// 	var key = RedisPrefixes.BRUTE_FORCE_PHONE_OTP+phone+'';
// 	rhmset.incrby(key, "elmt", 1, function(err,resp){
// 		if(err){
// 			return cb(err,null);
// 		}
// 		rhmset.updtOtpAtmpt(key, "etm", tim, function(err,resp){
// 			if(err){
// 				return cb(err,null);
// 			}
// 			if(resp){
// 				return cb(null,true);
// 			} else {
// 				return cb("last_otp_error_attempt_tim_not_updated",null);
// 			}
// 		});
// 	});
// };

// exports.updtSucOtpAtmpt = function(phone, tim, cb){
// 	var key = RedisPrefixes.BRUTE_FORCE_PHONE_OTP+phone+'';
// 	rhmset.incrby(key, "slmt", 1, function(err,resp){
// 		if(err){
// 			return cb(err,null);
// 		}
// 		rhmset.updtOtpAtmpt(key, "stm", tim, function(err,resp){
// 			if(err){
// 				return cb(err,null);
// 			}
// 			if(resp){
// 				return cb(null,true);
// 			} else {
// 				return cb("last_otp_suc_attempt_tim_not_updated",null);
// 			}
// 		});
// 	});
// };


