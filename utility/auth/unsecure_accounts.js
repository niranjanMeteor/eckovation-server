var mongoose 						  = require('mongoose');
var redis         				= require('redis');

var configs								= require('./../configs.js');
var AppClients					  = require('./../app_clients.js');
var RedisPrefixes 				= require('./../redis_prefix.js');
var UserInfoCache         = require('./../user_info_cache.js');

var redis_client        	= redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
    console.log("Error " + err);
});

require('./../../models/unsecureaccounts.js');
var UnsecureAccount 		  = mongoose.model('UnsecureAccount');

var rhmset                = require("./../redis_hmset.js")(redis_client);


exports.check = function(aid, pid, cb){
	var key , field;
	if(pid == null && typeof(aid) !== 'undefined'){
		key = RedisPrefixes.UNSECURE_ACCOUNTS;
		field = aid;
	} else if(aid == null && typeof(pid) !== 'undefined'){
		key = RedisPrefixes.UNSECURE_PROFILES;
		field = pid;
	} else {
		return cb("invalid parmeters for check",null);
	}
	rhmset.keyexists(key,field,function(err,isKey){
		if(err){
			return cb(err,null);
		}
		if(isKey){
			return cb(null,true);
		} else {
			return cb(null,false);
		}
	});
};

exports.add = function(aid, cl, cb){
	if(cl != null && cl != AppClients.ANDROID){
		return cb(null,true);
	}
	var tim = new Date();
	UnsecureAccount.findOne({
		aid : aid
	},function(err,unsecure_account){
		if(err){
			return cb(err,null);
		}
		if(unsecure_account && unsecure_account.ack){
			return cb("aid_already_acknowledged_the_unsecure_list",null);
		} 
		UnsecureAccount.update({
			aid : aid
		},{
			$setOnInsert : {
				aid : aid,
				ack : false
			},
			$set : {
				uat : tim
			}
		},{
			upsert : true
		},function(err,unsecure_account){
			if(err){
				return cb(err,null);
			}
			addAccountInUnsecureList(aid, tim, function(err,resp){
				if(err){
					return cb(err,null);
				}
				return cb(null,resp);
			});
		});
	});
};

exports.addAcc = function(aid, cl, cb){
	if(cl == AppClients.ANDROID || cl == null){
		var tim = new Date();
		UnsecureAccount.update({
			aid : aid
		},{
			$setOnInsert : {
				aid : aid,
				ack : false
			},
			$set : {
				uat : tim
			}
		},{
			upsert : true
		},function(err,unsecure_account){
			if(err){
				return cb(err,null);
			}
			rhmset.keyexists(RedisPrefixes.UNSECURE_ACCOUNTS,aid,function(err,isKey){
				if(err){
					console.trace(err);
					return cb(true,null);
				}
				if(isKey){
					return cb(null,false);
				}
				rhmset.setUnsecuredAccount(RedisPrefixes.UNSECURE_ACCOUNTS, aid, tim.getTime(),	function(err,resp){
					if(err){
						return cb(err,null);
					}
					if(resp){
						return cb(null,true);
					} else {
						return cb("aid_not_added_in_unsecure_list",false);
					}
				});
			});
		});
	} else {
		return cb(null,true);
	}
};

exports.addProfile = function(aid, pid, cb){
	rhmset.keyexists(RedisPrefixes.UNSECURE_ACCOUNTS,aid,function(err,isKey){
		if(err){
			console.trace(err);
			return cb(true,null);
		}
		if(!isKey){
			return cb(null,false);
		}
		rhmset.keyexists(RedisPrefixes.UNSECURE_PROFILES,pid,function(err,isProfileKey){
			if(err){
				return cb(err,null);
			}
			if(isProfileKey){
				return cb(null,false);
			}
			var tim = new Date().getTime();
			rhmset.setUnsecuredAccount(RedisPrefixes.UNSECURE_PROFILES, pid, tim,	function(err,resp){
				if(err){
					console.trace(err);
					return cb(true,false);
				} 
				if(resp){
					return cb(null,true);
				} else {
					return cb(null,false);
				}
			});
		});
	});
};

exports.addAccRedis = function(aid, cb){
	var tim = new Date();
	rhmset.keyexists(RedisPrefixes.UNSECURE_ACCOUNTS,aid,function(err,isKey){
		if(err){
			console.trace(err);
			return cb(true,null);
		}
		if(isKey){
			return cb(null,false);
		}
		rhmset.setUnsecuredAccount(RedisPrefixes.UNSECURE_ACCOUNTS, aid, tim.getTime(),	function(err,resp){
			if(err){
				return cb(err,null);
			}
			if(resp){
				return cb(null,true);
			} else {
				return cb("aid_not_added_in_unsecure_list",false);
			}
		});
	});
};

exports.addProfRedis = function(pid, cb){
	rhmset.keyexists(RedisPrefixes.UNSECURE_PROFILES, pid, function(err,isProfileKey){
		if(err){
			return cb(err,null);
		}
		if(isProfileKey){
			return cb(null,false);
		}
		var tim = new Date().getTime();
		rhmset.setUnsecuredAccount(RedisPrefixes.UNSECURE_PROFILES, pid, tim,	function(err,resp){
			if(err){
				console.trace(err);
				return cb(true,false);
			} 
			if(resp){
				return cb(null,true);
			} else {
				return cb(null,false);
			}
		});
	});
};

exports.remove = function(aid, cb){
	rhmset.keyexists(RedisPrefixes.UNSECURE_ACCOUNTS,aid,function(err,isKey){
		if(err){
			return cb(err,null);
		}
		if(isKey){
			rhmset.removefield(RedisPrefixes.UNSECURE_ACCOUNTS, aid, function(err,resp){
				if(err){
					return cb(err,false);
				} 
				UserInfoCache.getUserProfilesPartial(aid, function(err, profiles){
					if(err){
						return cb(err,null)
					}
					var done  = 0;
					var total = profiles.length;
					for(var i=0; i<total; i++){
						removeProfileFromUnsecureList(profiles[i]._id+'', function(err,resp){
							if(err){
								console.trace(err);
							}
							done++;
							if(total == done){
								return cb(null,true);
							}
						});
          }
				});
			});
		} else {
			return cb(null,false);
		}
	});
};

exports.removeAll = function(aid, cb){
	UnsecureAccount.update({
		aid : aid,
		ack : false
	},{
		ack : true
	},{
		multi : true
	},function(err,ack_unsecured_account){
		if(err){
			return cb(err,null);
		}
		rhmset.keyexists(RedisPrefixes.UNSECURE_ACCOUNTS,aid,function(err,isKey){
			if(err){
				return cb(err,null);
			}
			if(!isKey){
				return cb(null,false);
			}
			rhmset.removefield(RedisPrefixes.UNSECURE_ACCOUNTS, aid, function(err,resp){
				if(err){
					return cb(err,false);
				} 
				UserInfoCache.getUserProfilesPartial(aid, function(err, profiles){
					if(err){
						return cb(err,null)
					}
					var done  = 0;
					var total = profiles.length;
					for(var i=0; i<total; i++){
						removeProfileFromUnsecureList(profiles[i]._id+'', function(err,resp){
							if(err){
								console.trace(err);
							}
							done++;
							if(total == done){
								return cb(null,true);
							}
						});
          }
				});
			});
		});
	});
};

function addAccountInUnsecureList(aid, tim, cb){
	rhmset.keyexists(RedisPrefixes.UNSECURE_ACCOUNTS,aid,function(err,isKey){
		if(err){
			console.trace(err);
			return cb(true,null);
		}
		if(isKey){
			return cb(null,false);
		}
		rhmset.setUnsecuredAccount(RedisPrefixes.UNSECURE_ACCOUNTS, aid, tim.getTime(),	function(err,resp){
			if(err){
				return cb(err,null);
			}
			if(resp){
				UserInfoCache.getUserProfilesPartial(aid, function(err, profiles){
					if(err){
						return cb(err,null)
					}
					var done  = 0;
					var total = profiles.length;
					for(var i=0; i<total; i++){
						addProfileInUnsecureList(profiles[i]._id+'',tim,function(err,resp){
							if(err){
								console.trace(err);
							}
							done++;
							if(total == done){
								return cb(null,true);
							}
						});
					}
				});
			} else {
				return cb("aid_not_added_in_unsecure_list",false);
			}
		});
	});
}

function addProfileInUnsecureList(pid, tim, cb){
	rhmset.keyexists(RedisPrefixes.UNSECURE_PROFILES,pid,function(err,isProfileKey){
		if(err){
			return cb(err,null);
		}
		if(isProfileKey){
			return cb(null,false);
		}
		rhmset.setUnsecuredAccount(RedisPrefixes.UNSECURE_PROFILES, pid, tim,	function(err,resp){
			if(err){
				console.trace(err);
				return cb(true,false);
			} 
			if(resp){
				return cb(null,true);
			} else {
				return cb(null,false);
			}
		});
	});
}

function removeProfileFromUnsecureList(pid, cb){
	rhmset.keyexists(RedisPrefixes.UNSECURE_PROFILES,pid,function(err,isKey){
		if(err){
			return cb(err,null);
		}
		if(!isKey){
			return cb(null,false);
		}
		rhmset.removefield(RedisPrefixes.UNSECURE_PROFILES,pid,function(err,resp){
			if(err){
				return cb(err,null);
			}
			return cb(null,true);
		});
	});
}
