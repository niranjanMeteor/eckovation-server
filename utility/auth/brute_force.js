var redis               = require('redis');

var configs					    = require('./../configs.js');
var RedisPrefixes 			= require('./../redis_prefix.js');

var redis_client        = redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
  console.log("Error " + err);
});

var rhmset              = require("./../redis_hmset.js")(redis_client);

var ALLOWED_FAILED_VERIFY_OTP_ATTEMPTS  = configs.ALLOWED_FAILED_VERIFY_OTP_ATTEMPTS;
var TIME_TO_PAUSE_VERIFY_OTP_ATTEMPTS   = configs.TIME_TO_PAUSE_VERIFY_OTP_ATTEMPTS;


exports.check = function(aid, tim, cb){
	var key = RedisPrefixes.BRUTE_FORCE_ACCOUNT+aid+'';
	rhmset.getvals(key,function(err, bruteForceData){
		if(err){
			return cb(err,null);
		}
		if(!bruteForceData){
			return cb(null,false);
		}
		var numberOfFailedAttempts        = bruteForceData[0];
		var lastFailedLoginTimeWhenLimit  = bruteForceData[1];
		var lastFailedLoginTime           = bruteForceData[2];
		var alpha = parseInt(numberOfFailedAttempts /  ALLOWED_FAILED_VERIFY_OTP_ATTEMPTS)-1;
		var factor  = Math.pow(2,alpha);
		if(numberOfFailedAttempts >= ALLOWED_FAILED_VERIFY_OTP_ATTEMPTS && 
				((tim - lastFailedLoginTimeWhenLimit) < (factor*TIME_TO_PAUSE_VERIFY_OTP_ATTEMPTS))){
			return cb(null,{lmt:numberOfFailedAttempts,tm:lastFailedLoginTimeWhenLimit});
		}
		return cb(null,false);
	});
};

exports.mark = function(aid, tim, cb){
	var key = RedisPrefixes.BRUTE_FORCE_ACCOUNT+aid+'';
	rhmset.get(key, "lmt", function(err, bruteForceAttemptsTillNow){
		if(err){
			return cb(err,null);
		}
		if(!bruteForceAttemptsTillNow){
			rhmset.updtBruteForceAndTim(key, "lmt", 1, "ltm", tim,  "tm", tim, function(err,resp){
				if(err){
					return cb(err,null);
				}
				if(resp){
					return cb(null,true);
				} else {
					return cb("first_bruteforce_attempt_and_tim_not_updated",null);
				}
			});
		} else {
			var lmt = parseInt(bruteForceAttemptsTillNow)+1;
			if((lmt % ALLOWED_FAILED_VERIFY_OTP_ATTEMPTS) == 0){
				rhmset.updtBruteForceAndTim(key, "lmt", lmt, "ltm", tim, "tm", tim, function(err,resp){
					if(err){
						return cb(err,null);
					}
					if(resp){
						return cb(null,true);
					} else {
						return cb("last_bruteforce_attempt_and_tim_not_updated",null);
					}
				});
			} else {
				rhmset.updtBruteForce(key, "lmt", lmt, "tm", tim, function(err,resp){
					if(err){
						return cb(err,null);
					}
					if(resp){
						return cb(null,true);
					} else {
						return cb("last_bruteforce_attempt_not_updated",null);
					}
				});
			}
		}
	});
};

exports.unmark = function(aid, cb){
	var key = RedisPrefixes.BRUTE_FORCE_ACCOUNT+aid+'';
	rhmset.removekey(key,function(err,resp){
		if(err){
			return cb(err,null);
		}
		if(resp){
			return cb(null,true);
		} else {
			return cb(null,"aid_not_marked_in_brute_force");
		}
	});
};