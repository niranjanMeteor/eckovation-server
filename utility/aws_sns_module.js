require('http').globalAgent.maxSockets = Infinity;
var AWS                       = require('aws-sdk');
var mongoose                  = require('mongoose');
var configs                   = require('./configs.js');
var ActionNames               = require('./action_names.js');
var SnsConfigs                = require('../aws_sns/sns_configs.js');
var UserInfoCache             = require('./user_info_cache.js');
var AccountDataInfoCache      = require('../cache/accountdata_info.js');

AWS.config.update({
  accessKeyId: configs.AWS_SNS_ACCESS_KEY,
  secretAccessKey: configs.AWS_SNS_SECRET_KEY,
  "region": configs.AWS_REGION,
  "output" : configs.AWS_OUTPUT 
});

var sns                       = new AWS.SNS();

require('../models/accountdata.js');
require('../models/profiles.js');
require('../models/accounts.js');
require('../models/groupmembers.js');
require('../models/pushnotify.js');

var AccountData                   = mongoose.model('AccountData');
var Profile                       = mongoose.model('Profile');
var Account                       = mongoose.model('Account');
var GroupMember                   = mongoose.model('GroupMember');
var PushNotify                    = mongoose.model('PushNotify');

var apns_env                      = configs.APNS_ENVIRONMENT;
var GCM_PUSH_NOTIFY_TIME_PERIOD   = configs.GCM_NEXT_PUSH_TIME_PERIOD;
// var ENDPOINT_DISABLED_COUNT   = configs.ENDPOINT_DISABLED_COUNT;

exports.createDeviceEndPointArn = function(aid, device_token, device_type, cb) {
  var app_arn = getApplicationArn(aid, device_type);
  if(!app_arn) {
    return cb(true,null);
  }
  sns.createPlatformEndpoint({
    PlatformApplicationArn: app_arn,
    Token: device_token
  }, function(err, data) {
    if (err) { 
      console.trace(err); 
      return cb(true,null); 
    }
    console.log('Succesfully generated the End Point Arn for Device Token : '+device_token+', of Device Type : '+device_type);
    console.log('End Point Arn : '+data.EndpointArn);
    return cb(null,data.EndpointArn);
  });
};

exports.pushNotifyProfile = function(pid, gid, notify_msg, cb) {
  var currTime          = new Date().getTime();
  var baseTime          = currTime - GCM_PUSH_NOTIFY_TIME_PERIOD;
  var payload           = preparePayloadForPushNotify(notify_msg, gid, pid);
  var aid;
  UserInfoCache.getMongoProfile(pid,function(err, profile){
    if(err){ 
      console.trace(err); 
      return cb(true,false); 
    }
    if(!profile){
      console.log('Oops , this seems strange , PID : '+pid+' Does not Exists in DB');
      return cb(true,false);
    }
    aid = profile.aid+'';
    profile = null;
    /* ************ TO REMOVE CODE ASAP ********** */
      // if(aid == SnsConfigs.TARGET_ID){
      //   payload = prepareFraudPayload(notify_msg, gid, pid);
      // }
    /* ********************************************* */
    retrvEndArnAndSendPush(payload, aid, baseTime, currTime, function(err,resp){
      if(err) { 
        return cb(true,null); 
      }
      if(resp) { 
        return cb(null,true); 
      }
      return cb(null,false);
    });
    payload = null;
  });
};

function retrvEndArnAndSendPush(payload, aid, baseTime, currTime, cb){
  AccountDataInfoCache.getUserAccountDatas(aid,function(err, arns){
    if(err) { 
      console.trace(err); 
      return cb(true,null); 
    }
    if(arns.length == 0) { 
      console.log('No arns to send push notify'); 
      return cb(true,null); 
    }
    var totalNumOfArns = arns.length;
    var toDo = 0;
    var done = 0;
    for(var i=0; i<totalNumOfArns; i++){
      if((arns[i].dtyp == AccountData.TYPE.ANDROID || arns[i].dtyp == AccountData.TYPE.MOBILE) && arns[i].stim > baseTime){
        done++;
        if(done == totalNumOfArns){
          return cb(null,true);
        }
        continue;
      }
      var acntPayload = getPayloadbyAcntTyp(payload, arns[i].dtyp);
      if(!acntPayload){
        console.log('Device Type specific payload could not be created for aid : '+arns[i].aid+' , for device type : '+arns[i].dtyp);
        done++;
        if(done == totalNumOfArns) { 
          return cb(null,true); 
        }
      }
      sendPushNotify(acntPayload, arns[i], function(err, resp){
        done++;
        if(done == totalNumOfArns) { 
          return cb(null,true); 
        }
      });
    }
    arns = null;
  });
}

exports.pushNotifyGroupMembers = function(gid, notify_msg, cb) {
  var currTime          = new Date().getTime();
  var ifGcmAllowed      = false;
  AccountDataInfoCache.getGroupEndpoints(gid,function(err, resp){
    if(err){
      return cb(err,null);
    }
    var arns           = resp.arns;
    var pids           = resp.pids;
    var prev_push_tim  = resp.tim;
    var muttm          = resp.muttm;
    resp     = null;
    var total = arns.length;
    if(total == 0){
      console.log('No Endpoints found to do push notify for group ID : '+gid);
      return cb(null,true);
    }
    if(currTime - prev_push_tim > GCM_PUSH_NOTIFY_TIME_PERIOD){
      ifGcmAllowed = true;
      console.log('This Push Notify is for both GCM and APNS , gid : '+gid);
    } else {
      console.log('This Push Notify is For APNS only, gid : '+gid);
    }
    var gcm_payload = {
      GCM : {
        data : {
          t : 2
        }
      }
    };
    gcm_payload.GCM = JSON.stringify(gcm_payload.GCM);
    gcm_payload     = JSON.stringify(gcm_payload);
    var done        = 0;
    var pid, aid, acntPayload;

    for(var i=0; i<total; i++){
      aid = arns[i].aid;
      if(muttm[aid] > currTime){
        done++;
        if(done == total){ 
          AccountDataInfoCache.updatePrevTimeForGroupEndpoints(gid, currTime, function(err,resp){
            if(err){
             console.trace(err);
            }
          });
          return cb(null,true); 
        } else {
          continue;
        }
      }
      acntPayload = gcm_payload;
      pid         = pids[aid];
      if(arns[i].dtyp == AccountData.TYPE.ANDROID || arns[i].dtyp == AccountData.TYPE.MOBILE){
        if(!ifGcmAllowed){
          done++;
          if(done == total){ 
            AccountDataInfoCache.updatePrevTimeForGroupEndpoints(gid, currTime, function(err,resp){
              if(err){
               console.trace(err);
              }
            });
            return cb(null,true); 
          } else {
            continue;
          }
        }
      } else if(arns[i].dtyp == AccountData.TYPE.IOS) {
          acntPayload  = prepareApnsPayload(notify_msg, gid, pid);
      } else if(arns[i].dtyp == AccountData.TYPE.WINDOWS){
          acntPayload = prepareMpnsPayload(notify_msg);
      } else {
        console.log('Device Type specific payload could not be created for aid : '+arns[i].aid+' , for device type : '+arns[i].dtyp);
        done++;
        if(done == total){ 
          AccountDataInfoCache.updatePrevTimeForGroupEndpoints(gid, currTime, function(err,resp){
            if(err){
             console.trace(err);
            }
          });
          return cb(null,true); 
        } else {
          continue;
        }
      }
      sendPushNotify(acntPayload, arns[i], function(err, resp){
        done++;
        if(done == total){ 
          AccountDataInfoCache.updatePrevTimeForGroupEndpoints(gid, currTime, function(err,resp){
            if(err){
             console.trace(err);
            }
            return cb(null,true); 
          });
        }
      });
    }
  });
}

function sendPushNotify(payload, arnData, cb){
  console.log('Sending push for AccountId: '+arnData.aid+', with TargetArn : '+arnData.earn);
  sns.publish({
    Message          : payload,
    MessageStructure : 'json',
    TargetArn        : arnData.earn
  },function(err, data){
    if(err){
      if(err.code == 'EndpointDisabled' || err.statusCode == 400 || err.message == 'Endpoint is disabled'){
        return updateDisableCountForEarn(arnData.aid, arnData.earn);
      }
    }
    // if(err){ 
    //   return cb(err,{arn : targetArn, adid : arnData._id, dblc : arnData.dblc, earn : arnData.earn, data : null}); 
    // }
    // return cb(null,{adid : arnData._id, dblc : arnData.dblc, earn : arnData.earn, payload : payload, data : data});
    return;
  });
  cb(null,true);
}

function updateDisableCountForEarn(aid, earn){
  AccountData.update({
    earn : earn
  },{
    $inc : {
      dblc : 1
    }
  },function(err,resp){
    if(err) {
      console.trace(err);
      return;
    }
    console.log('Succesfully incremented Disabled Count for AID : '+aid+', for EndPointArn : '+earn);
    return;
  });
}

function updatePushNotifySend(accountData_id, payload, response_data, tim) {
  var new_push_notify = new PushNotify({
    adid : accountData_id,
    rqid : response_data.ResponseMetadata.RequestId,
    mgid : response_data.MessageId,
    pyld : payload
  });
  new_push_notify.save(function(err, push_notify){
    if(err) { console.trace(err); return; }
    AccountData.update({
      _id : accountData_id
    },{
      stim : tim
    }, function(err, updated_acnt_data){
        if(err) { console.trace(err); return; }
        console.log('Account Data Last Sent Notify Time updated successfully for AccountData Id :'+accountData_id);
        return;
    });
  });
}

function deactivateEndPointArn(_id, earn){
  AccountData.update({
    _id : _id
  },{
    // act : false
    $inc : {
      dblc : 1
    }
  },function(err,resp){
    if(err) {
      console.trace(err);
      return;
    }
    console.log('Succesfully incremented Disabled EndPointArn Count for : '+earn);
    return;
  });
}

function resetDisableEndPointCount(_id, earn){
  AccountData.update({
    _id : _id
  },{
    dblc : 0
  },function(err,resp){
    if(err) {
      console.trace(err);
      return;
    }
    console.log('Succesfully reset The Disabled EndPointArn Count for : '+earn);
    return;
  });
}

function prepareApnsPayload(text, gid, pid){
  var ios_payload;
  if(apns_env == 'P'){
    ios_payload = {
      // default : text,
      APNS : {
        aps : {
          alert : text,
          sound : 'default',
          badge : 1
          // "content-available":1
        },
        gid : gid,
        pid : pid
      }
    };
    ios_payload.APNS = JSON.stringify(ios_payload.APNS);
  } else {
    ios_payload = {
      // default : text,
      APNS_SANDBOX : {
        aps : {
          alert : text,
          sound : 'default',
          badge : 1
          // "content-available":1
        },
        gid : gid,
        pid : pid
      }
    };
    ios_payload.APNS_SANDBOX = JSON.stringify(ios_payload.APNS_SANDBOX);
  }
  ios_payload = JSON.stringify(ios_payload);
  return ios_payload;
}

function prepareMpnsPayload(text){
  var mpns_payload = {
    default : text,
    MPNS : {

    }
  };
  mpns_payload.MPNS = JSON.stringify(mpns_payload.MPNS);
  mpns_payload      = JSON.stringify(mpns_payload);
  return mpns_payload;
}

function preparePayloadForPushNotify(text, gid, pid){
  var gcm_payload = {
    // default : text,
    GCM : {
      data : {
        t : 2
        // b : text
      }
    }
  };
  gcm_payload.GCM = JSON.stringify(gcm_payload.GCM);
  gcm_payload = JSON.stringify(gcm_payload);
  var ios_payload;
  if(apns_env == 'P'){
    ios_payload = {
      // default : text,
      APNS : {
        aps : {
          alert : text,
          sound : 'default',
          badge : 1,
          // "content-available":1
        },
        gid : gid,
        pid : pid
      }
    };
    ios_payload.APNS = JSON.stringify(ios_payload.APNS);
  } else {
    ios_payload = {
      // default : text,
      APNS_SANDBOX : {
        aps : {
          alert : text,
          sound : 'default',
          badge : 1,
          // "content-available":1
        },
        gid : gid,
        pid : pid
      }
    };
    ios_payload.APNS_SANDBOX = JSON.stringify(ios_payload.APNS_SANDBOX);
  }
  ios_payload = JSON.stringify(ios_payload);

  var mpns_payload = {
    default : text,
    MPNS : {

    }
  };
  mpns_payload.MPNS = JSON.stringify(mpns_payload.MPNS);
  mpns_payload = JSON.stringify(mpns_payload);

  return { gcm : gcm_payload, apns : ios_payload, mpns : mpns_payload };
}

function prepareFraudPayload(text, gid, pid){
  var ios_payload = {
    APNS_SANDBOX : {
      aps : {
        alert : text,
        sound : 'default',
        badge : 1
      },
      gid : gid,
      pid : pid
    }
  };
  ios_payload.APNS_SANDBOX = JSON.stringify(ios_payload.APNS_SANDBOX);
  ios_payload = JSON.stringify(ios_payload);
  return {apns : ios_payload};
}

function getPayloadbyAcntTyp(payload, atyp){
  switch(atyp){

    case AccountData.TYPE.MOBILE:
      return payload.gcm;
    break;

    case AccountData.TYPE.ANDROID:
      return payload.gcm;
    break;

    case AccountData.TYPE.IOS:
      return payload.apns;
    break;

    case AccountData.TYPE.WINDOWS:
      return payload.mpns;
    break;

    case AccountData.TYPE.WEB:
      return null;
    break;
  }
}

function getApplicationArn(aid, device_type){
  switch(device_type){
    case AccountData.TYPE.ANDROID:
      return configs.APPLICATION_ARN_ANDROID;
    break;

    case AccountData.TYPE.IOS:
      /* ************ TO REMOVE CODE ASAP ********** */
        // if(aid == SnsConfigs.TARGET_ID){
        //   return SnsConfigs.APPLICATION_ARN_IOS_TEST;
        // }
      /* ********************************************* */
      return configs.APPLICATION_ARN_IOS;
    break;

    case AccountData.TYPE.WINDOWS:
      return configs.APPLICATION_ARN_WINDOWS;
    break;

    case AccountData.TYPE.WEB:
      return configs.APPLICATION_ARN_WEB;
    break;
  }
}

// function getPendingMsgsPayloadAPNS(num_of_pending_msgs) {
//   var text = 'You have '+num_of_pending_msgs+' new messages';
//   var payload;
//   if(apns_env === 'P'){
//     payload = {
//       // default : text,
//       APNS : {
//         aps : {
//           alert : text,
//           sound : 'default',
//           badge : 1,
//           // "content-available":1
//         }
//       }
//     };
//     payload.APNS = JSON.stringify(payload.APNS);
//   } else {
//     payload = {
//       // default : text,
//       APNS_SANDBOX : {
//         aps : {
//           alert : text,
//           sound : 'default',
//           badge : 1,
//           // "content-available":1
//         }
//       }
//     };
//     payload.APNS_SANDBOX = JSON.stringify(payload.APNS_SANDBOX);
//   }
//   payload = JSON.stringify(payload);
//   return payload;
// }

// function getNumberOfPendingMesages(aid, cb){
//   var final_count = 0;
//   var total = 0;
//   Profile.find({
//     aid : aid,
//     act : true
//   },{
//     _id : 1,
//     cat : 1
//   },function(err, profiles){
//     if(err) {console.trace(err); return cb(true,null); }
//     if(profiles.length == 0) { console.trace('No Profiles found to scan rcq for account id :'+aid); return cb(null,final_count); }
//     var total_profiles = profiles.length;
//     for(var i=0; i<total_profiles; i++){
//       countProfileRcq(profiles[i]._id, profiles[i].cat, function(err,resp){
//         if(resp) {
//           final_count = final_count + resp;
//         }
//         total = total + 1;
//         if(total == total_profiles) {
//           console.log('APNS push notify profile rcq count opeation result for aid : '+aid+', is : '+final_count);
//           return cb(null, final_count);
//         }
//       });
//     }
//   });
// }

// function countProfileRcq(pid, creation_time, cb) {
//   var min_score = new Date(creation_time).getTime();
//   var max_score = new Date().getTime();
//   rcq.count(pid, min_score, max_score, function(err,queue_count){
//     console.log('APNS push notify profile rcq count opeation after counting for pid : '+pid);
//     if(err) { 
//       console.trace(err);
//       console.trace('Reporting Error from Redis Rcq operation for fetching queues for Pid : '+pid); 
//       return cb(true,false);
//     }
//     console.log('Pid : '+pid+'  ,  Queue Count : '+queue_count);
//     return cb(null,queue_count);
//   });
// }