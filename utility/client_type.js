var configs         		= require('./configs.js');
var log4jsLogger        = require('../loggers/log4js_module.js');
var log4jsConfigs  			= require('../loggers/log4js_configs.js');

var httpLogger          = log4jsLogger.getLogger(log4jsConfigs.CATEGORY_CLIENTS_HTTP);
var socketLogger        = log4jsLogger.getLogger(log4jsConfigs.CATEGORY_CLIENTS_SOCKET);

exports.isAndroidDeviceSocket = function(input, aid){
	return false;
	if(!input) return false;
	var match7 = input.match(/okhttp/);
	if(match7 !==  null){
		socketLogger.info({"aid":aid,"ua":input,"ADS": 1});
		return true;
	}
	var match1 = input.match(/WebKit/);
	if(match1 !==  null){
		socketLogger.info({"aid":aid,"ua":input,"ADS": 0});
		return false;
	}
	var match2 = input.match(/IEMobile/);
	if(match2 !==  null){
		socketLogger.info({"aid":aid,"ua":input,"ADS": 0});
		return false;
	}
	var match3 = input.match(/Trident/);
	if(match3 !==  null){
		socketLogger.info({"aid":aid,"ua":input,"ADS": 0});
		return false;
	}
	var match4 = input.match(/KHTML/);
	if(match4 !==  null){
		socketLogger.info({"aid":aid,"ua":input,"ADS": 0});
		return false;
	}
	var match5 = input.match(/Safari/);
	if(match5 !==  null){
		socketLogger.info({"aid":aid,"ua":input,"ADS": 0});
		return false;
	}
	var match6 = input.match(/Gecko/);
	if(match6 !==  null){
		socketLogger.info({"aid":aid,"ua":input,"ADS": 0});
		return false;
	}
	var match8 = input.match(/Dalvik/);
	var match9 = input.match(/Android/);
	if(match8 !== null && match9 !== null){
		socketLogger.info({"aid":aid,"ua":input,"ADS": 1});
		return true;
	} else {
		socketLogger.info({"aid":aid,"ua":input,"ADS": 0});
		return false;
	}
};

exports.isAndroidDevice = function(reqHeaders){
	return false;
	var input = reqHeaders['user-agent'];
	if(!input) return false;
	var match7 = input.match(/okhttp/);
	if(match7 !==  null){
		httpLogger.info({"ua":reqHeaders,"AD": 1});
		return true;
	}
	var match1 = input.match(/WebKit/);
	if(match1 !==  null){
		httpLogger.info({"ua":reqHeaders,"AD": 0});
		return false;
	}
	var match2 = input.match(/IEMobile/);
	if(match2 !==  null){
		httpLogger.info({"ua":reqHeaders,"AD": 0});
		return false;
	}
	var match3 = input.match(/Trident/);
	if(match3 !==  null){
		httpLogger.info({"ua":reqHeaders,"AD": 0});
		return false;
	}
	var match4 = input.match(/KHTML/);
	if(match4 !==  null){
		httpLogger.info({"ua":reqHeaders,"AD": 0});
		return false;
	}
	var match5 = input.match(/Safari/);
	if(match5 !==  null){
		httpLogger.info({"ua":reqHeaders,"AD": 0});
		return false;
	}
	var match6 = input.match(/Gecko/);
	if(match6 !==  null){
		httpLogger.info({"ua":reqHeaders,"AD": 0});
		return false;
	}
	
	var match8 = input.match(/Dalvik/);
	var match9 = input.match(/Android/);
	if(match8 !== null && match9 !== null){
		httpLogger.info({"ua":reqHeaders,"AD": 1});
		return true;
	} else {
		httpLogger.info({"ua":reqHeaders,"AD": 0});
		return false;
	}
};

exports.isIosDevice = function(reqHeaders){
	var input = reqHeaders['user-agent'];
	if(!input) return false;
	var match1 = input.match(/iPhone/);
	var match2 = input.match(/iOS/);
	if((match1 !== null) && (match2 !== null)){
		httpLogger.info({"ua":reqHeaders,"IOSD": 1});
		return true;
	} else {
		httpLogger.info({"ua":reqHeaders,"IOSD": 0});
		return false;
	}
};	

exports.isIosDeviceSocket = function(reqHeaders){
	var input = reqHeaders['user-agent'];
	if(!input) return false;
	var match1 = input.match(/CFNetwork/);
	var match2 = input.match(/Darwin/);
	if((match1 !== null) && (match2 !== null)){
		socketLogger.info({"ua":reqHeaders,"IOSDS": 1});
		return true;
	} else {
		socketLogger.info({"ua":reqHeaders,"IOSDS": 0});
		return false;
	}
};

exports.isWebDevice = function(reqHeaders){
	var input = reqHeaders['user-agent'];
	if(!input) return false;
	var match1_i = input.match(/iPhone/);
	var match2_i = input.match(/iOS/);
	if(match1_i !== null || match2_i !== null)
		return false;
	var match1_a = input.match(/okhttp/);
	if(match1_a !== null)
		return false;
	var match2_a = input.match(/Dalvik/);
	if(match2_a !== null)
		return false;
	var match3_a = input.match(/Android/);
	if(match3_a !== null)
		return false;
	return true;
};

// exports.isIosDevice = function(input){
// 	if(!input) return false;
// 	var match1 = input.match(/iPhone/);
// 	var match2 = input.match(/iOS/);
// 	var match3 = input.match(/CFNetwork/);
// 	var match4 = input.match(/Darwin/);
// 	if(match1 !== null || match2 !== null || match3 !== null || match4 !== null)
// 		return true;
// 	return false;
// };	

// exports.isIosDeviceSocket = function(input){
// 	if(!input) return false;
// 	var match1 = input.match(/CFNetwork/);
// 	var match2 = input.match(/Darwin/);
// 	var match3 = input.match(/iPhone/);
// 	var match4 = input.match(/iOS/);
// 	if(match1 !== null || match2 !== null || match3 !== null || match4 !== null)
// 		return true;
// 	return false;
// };

