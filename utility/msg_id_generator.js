var crypto = require('crypto');

exports.generate = function(input1, input2, input3) {
	var value = ''+input1+input2+input3+Math.floor(Math.random() * 10000);
  var hash = crypto.createHash('sha256').update(value).digest('hex');
  return hash;
};