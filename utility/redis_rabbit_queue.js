var _ = require('underscore');


var rrq = function(client) {
	var redis_client = client;
	var PREFIX = "q";

	return {
		removeLastObj : function(idWithPrefix, cb){
			redis_client.zremrangebyrank(idWithPrefix,0,1,function(err,res){
				if(err) {return cb(new Error(err));}
				return cb(null,res);
			});
		},
		cardinality : function(idWithPrefix, cb){
			redis_client.zcard(idWithPrefix,function(err,res){
				if(err) {return cb(new Error(err));}
				return cb(null,res);
			});
		},
		addWithPrefix : function(prefix,id,score,obj,cb) {
			redis_client.zadd([prefix+id,score,obj],function(err,res){
				if(err) {return cb(new Error(err));}
				return cb(null,res);
			});
		},
		addWithoutPrefix : function(id,score,obj,cb) {
			redis_client.zadd([id,score,obj],function(err,res){
				if(err) {return cb(new Error(err));}
				return cb(null,res);
			});
		},
		removeWithPrefix : function(prefix,id,obj,cb) {
			redis_client.zrem([prefix+id,obj],function(err,res){
				if(err) {return cb(new Error(err));}
				return cb(null,res);
			});
		},
		removeWithoutPrefix : function(id,obj,cb) {
			redis_client.zrem([id,obj],function(err,res){
				if(err) {return cb(new Error(err));}
				return cb(null,res);
			});
		}, 
		scan_wp : function(id,cb) {
			var start = 0;
			redis_client.zscan(id,start,function(err,res){
				if(err) {return cb(new Error(err));}
				return cb(null,res);			
			});
		},
		scan_start_wp : function(id,start,cb) {
			redis_client.zscan(id,start,function(err,res){
				if(err) {return cb(new Error(err));}
				return cb(null,res);			
			});
		},
		scanall_wp : function(id,cb) {
			var start = 0;
			var me = this;
			var total = [];
			var cb1 = function(err,res){
				if(err) {return cb(new Error(err));}
				// total = _.union(res[1],total);
				total = total.concat(res[1]);
				if(res[0]!=0) {
					me.scan_start_wp(id,res[0],cb1);
				} else {
					return cb(null,[0,total]);
				}
			};
			this.scan_start_wp(id,start,cb1);
		},
		getall_wp : function(id, cb){
			redis_client.ZRANGEBYSCORE(id,'-inf','+inf',function(err,res){
				if(err) {return cb(new Error(err));}
				
				return cb(null,res);
			});
		}
	};
};

module.exports = rrq;