var crypto 					= require('crypto');
var configs         = require('./configs.js');
var log4jsLogger  	= require('../loggers/log4js_module.js');
var log4jsConfigs   = require('../loggers/log4js_configs.js');

var logger          = log4jsLogger.getLogger(log4jsConfigs.CATEGORY_UTILITY_EN_DE_CRYPT);

var algorithm 			= configs.MOBILE_NUMBER_ENCRYPT_ALGO;     // or any other algorithm supported by OpenSSL
var salt 						= configs.MOBILE_NUMBER_DECRYPT_KEY_SALT;

exports.encrypt = function(input, key) {
	var modifiedKey = key+salt; 
	try {       
		var hashedKey   = crypto.createHash('sha256').update(modifiedKey).digest('base64');
		var cipher      = crypto.createCipher(algorithm, hashedKey);  
		var encrypted   = cipher.update(input, 'utf8', 'hex') + cipher.final('hex');
		return encrypted;
	} catch(e) {
		logger.error({"FUNC":"encrypt","err":e.message,"p":{inp:input,k:key}});
		return null;
	} 
};

exports.decrypt = function(input, key){
	var modifiedKey = key+salt;        
	try {
		var hashedKey   = crypto.createHash('sha256').update(modifiedKey).digest('base64');
		var decipher    = crypto.createDecipher(algorithm, hashedKey);
		var decrypted   = decipher.update(input, 'hex', 'utf8') + decipher.final('utf8');
		return decrypted;
	} catch(e) {
		logger.error({"FUNC":"decrypt","err":e.message,"p":{inp:input,k:key}});
		return null;
	}
};

// encryptKey();

// function encryptKey(){
// 	var input = "9591999098";
// 	var key = "abcdef12345678"
// 	var iv = crypto.randomBytes(16);
// 	console.log(iv);
// 	var modifiedKey = key+salt;        
// 	var hashedKey   = new Buffer(crypto.createHash('sha256').update(modifiedKey).digest('base64'));
// 	var cipher      = crypto.createCipheriv(algorithm, hashedKey, iv);  
// 	var encrypted   = cipher.update(input, 'utf8', 'hex') + cipher.final('hex');
// 	console.log(encrypted);
// }
// decryptKey();

// function decryptKey(){
// 	var key = 'ZfPPKO47uZlL4hYo3GiIrA42DouqqfKp0PiBDNBcTnY=';
// 	var input   = '61931d02f6909c92ba294d3447c94f5f';

// 	var decipher = crypto.createDecipher(algorithm, key);
// 	console.log(decipher);
// 	var decrypted = decipher.update(input, 'hex', 'hex');// + decipher.final('utf8');
// 	decrypted += decipher.final('hex');
// 	console.log('decryption');
// 	console.log(decrypted);
// }

// function encryptNum(input){
// 	var modifiedKey = input;//key+salt;        
// 	var hash2 = crypto.createHash('sha256').update(modifiedKey);
// 	var hashedKey   = crypto.createHash('sha256').update(modifiedKey).digest('base64');
// 	console.log('hash2');
// 	console.log(hash2);
// 	console.log('hashed key');
// 	console.log(hashedKey);
// 	var cipher      = crypto.createCipher(algorithm, hashedKey);
// 	var encrypted   = cipher.update(input, 'utf8', 'hex') + cipher.final('hex');
// 	console.log('encrypted is');
// 	console.log(encrypted);
// 	return encrypted;
// }