var configs  =  {
	DEFAULT_CACHE_TIME                    : 60*60*24*30,     // 1 Month
	DEFAULT_ACCOUNT_PROF_GMS_CACHE_TIME   : 60*60*24*30*2,   // 2 Months
	DEFAULT_GROUP_ADMINS_CACHE_TIME       : 60*60*24*30*6,   // 6 months
	SOCKET_TOKEN_CACHE_TIME               : 60*60*24*1,      // 1 day
	GROUP_ACCOUNTDATA_CACHE_TIME          : 60*60*24*3,      // 1 day
	USERRCQ_DROP_CACHE_TIME               : 60*60*1,         // 1 Hour 
	BRUTE_FORCE_PHONE_OTP_CACHE_TIME      : 60*60*24*10,     // 10 days
	BRUTE_FORCE_GROUP_JOIN_CACHE_TIME     : 60*60*24*10,     // 10 days
	BRUTE_FORCE_ACCOUNT_CACHE_TIME        : 60*60*24*10      // 10 days
};

module.exports = configs;