var mongoose            = require('mongoose');
var redis               = require('redis');

var configs             = require('./configs.js');
var RedisPrefixes       = require('./redis_prefix.js');
var GroupRedisKeys      = require('./group_redis_keys.js');


var redis_client        = redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
    console.log("Error " + err);
});

var rhmset              = require("./redis_hmset.js")(redis_client);

require('../models/groupmembers.js');
require('../models/groups.js');

var GroupMember         = mongoose.model('GroupMember');
var Group               = mongoose.model('Group');

exports.getGroupAdmins = function(gid, cb){
    var key = RedisPrefixes.GROUP_INFO+gid+'';
    rhmset.get(key,GroupRedisKeys.ADMINS,function(err,rAdmins){
        if(err) { 
            return cb(err,null); 
        }
        if(rAdmins){
            var toReturn = JSON.parse(rAdmins);
            return cb(null, toReturn);
        } else {
            GroupMember.find({
                gid : gid,
                act : true,
                gdel: false,
                type: GroupMember.TYPE.ADMIN
            },function(err,admins){
                if(err) { 
                    return cb(err,null); 
                }
                cb(null, admins);
                var toStore = JSON.stringify(admins);
                rhmset.set(RedisPrefixes.GROUP_INFO,gid,GroupRedisKeys.ADMINS,toStore,function(err,resp){
                    if(err){
                        console.trace(err);
                    } else if(!resp) {
                        console.log('Could not set Group Admins in redis hmset for group id : '+gid);
                    }
                    return;
                });
            });
        }
    });
}

exports.getMongoGroup = function(gid, cb){
    var gid = gid+'';
    var key = RedisPrefixes.GROUPS;
    rhmset.get(key,gid,function(err,group){
        if(err) { 
            return cb(err,null); 
        }
        if(group){
            var toReturn = JSON.parse(group);
            return cb(null, toReturn);
        } else {
            Group.findOne({
                _id : gid,
                act : true
            },{
                cpid : 0,
                caid : 0,
                crole: 0
            },function(err, group) {
                if(err) { 
                    return cb(err,null); 
                }
                cb(null, group);
                if(!group) return;
                var toStore = JSON.stringify(group);
                rhmset.setMongoObj(key,gid,toStore,function(err,resp){
                    if(err){
                        console.trace(err);
                    } else if(!resp) {
                        console.log('Could not set group in redis hmset for group id : '+gid);
                    }
                    return;
                });
            });
        }
    });
};

exports.removeMongoGroup = function(gid, cb){
    var gid = gid+'';
    var key = RedisPrefixes.GROUPS;
    rhmset.keyexists(key,gid,function(err,isKey){
        if(err){
          return cb(err,null);   
        }
        if(isKey){
            rhmset.removefield(key,gid,function(err,resp){
                if(err){
                    return cb(err,null);
                } 
                if(resp){
                    return cb(null,true);
                } else {
                    console.log('Oops,this is unfortunate that Group HMSET key could not be deleted for  gid : '+gid);
                    return cb(null,false);
                }
            });
        } else {
            console.log('Oops,this is unfortunate that Group HMSET key does not exists for gid : '+gid);
            return cb(null,false);
        }
    });
};

exports.removeGroupAdminsCache = function(gid, cb){
    var key = RedisPrefixes.GROUP_INFO+gid+'';
    rhmset.keyexists(key,GroupRedisKeys.ADMINS,function(err,isKey){
        if(err){
            return cb(err,null);
        }
        if(isKey){
            rhmset.removefield(key,GroupRedisKeys.ADMINS,function(err,resp){
                if(err){
                    return cb(err,null);
                } 
                if(resp){
                    return cb(null,true);
                } else {
                    console.log("Oops,this is unfortunate that Group Admins HMSET key could not be deleted for gid : "+gid);
                    return cb(null,false);
                }
            });
        } else {
            console.log("Oops,this is unfortunate that Group Admins HMSET key does not exists for Account : "+gid);
            return cb(null,false);
        }
    });
}