var ConfigsOverwrite = {
        DB_NAME                                          : "eckovation",
        MONGO_HOST                                       : ["10.116.191.13","10.116.191.24"],
        MONGO_PORT                                       : "27017",
        MONGO_UNAME                                      : "eck_user",
        MONGO_PASS                                       : "eck!!--^^(12)3_=_user",
        MONGO_REPLICA_SET_NAME                           : "eckrepl1",
        MONGO_POOLSIZE                                   : 40,
        REDIS_PASS                                       : "eck!!--^^(12)3_=_user######$$$$!@!@!#@@#@",
        REDIS_HOST                                       : "10.116.191.10",
        ANALYTICS_SMTP_HOST                              : "email-smtp.us-east-1.amazonaws.com",
        ANALYTICS_SMTP_USERNAME                          : "AKIAI2KGYEV2NK74MXSA",
        ANALYTICS_SMTP_PASSWORD                          : "AiwgSsgIThITo8ntPc5ULx5OT2RICNN9ZbbAiH6rz1Pm",
        RABBITMQ_HOST                                    : "10.66.233.190",
        RABBITMQ_USER                                    : "eck_rabbit",
        RABBITMQ_PASSWORD                                : "eck!!--^^(12)3_=_user56B23xD(45)_7",
        SEEN_LIST_LIMIT                                  : 20,
        JWT_REFRESH_TOKEN_PRIVATE_KEY                    : "H*1#6Vx+=L@O=$Re#sX3*%@#TION9*2R5*=+pya7g#Gt3$#*ION*D456R#@hr",
        JWT_ACCESS_TOKEN_PRIVATE_KEY                     : "97G#@Gs6A*@O=$Hgd*Qs4%@#TION9*2R5*=+geY#se*S3er*ION*PP98$@#7F",
        JWT_TOKEN_SECRET_KEY_HASH_SALT                   : "@raatJYt$$hung7am7a6cD%#K97#3+H=Af4orAo*B@4bhavNa*6+H@8P%cv$*",
        JWT_ACCESS_TOKEN_EXPIRY_TIME                     : 60*60*24*7,        // 7 days
        JWT_ACCESS_TOKEN_EXPIRY_TIME_WEB                 : 60*60*1,           // 1 hours
        JWT_ACCESS_TOKEN_EXPIRY_TIME_ANDROID             : 60*60*24*5,        // 5 days
        JWT_ACCESS_TOKEN_EXPIRY_TIME_IOS                 : 60*60*24*5,        // 5 days
        JWT_REFRESH_TOKEN_EXPIRY_TIME                    : 60*60*24*365*2,    // 2 years
        JWT_REFRESH_TOKEN_EXPIRY_TIME_WEB                : 60*60*24*60,       // 2 months
        JWT_REFRESH_TOKEN_EXPIRY_TIME_ANDROID            : 60*60*24*365*2,    // 2 years
        JWT_REFRESH_TOKEN_EXPIRY_TIME_IOS                : 60*60*24*365*2,    // 2 years
        LOGGER_APP_ENV                                   : "p",               // production env
        MOBILE_NUMBER_DECRYPT_KEY_SALT                   : "*3$!@k#%$*3P%#Hqm78@Rr$",
        IOS_DEMO_NUMBER                                  : '9000000000',
        APNS_ENVIRONMENT                                 : 'P',
        APPLICATION_ARN_IOS                              : "arn:aws:sns:ap-southeast-1:279388982033:app/APNS/EckovationIosProduction",
        APPLICATION_ARN_ANDROID                          : "arn:aws:sns:ap-southeast-1:279388982033:app/GCM/eckovation-app-production",
        APPLICATION_ARN_WINDOWS                          : "arn:aws:sns:ap-southeast-1:279388982033:app/WNS/EckovationWindowsPhoneProduction",
        CALL_ME_GENERATION_DELAY                         : 25*1000,            // only 25 seconds after a valid otp request
        CALL_ME_GENERATION_EXPIRY                        : 30*60*1000,         // 30 minutes expressed in milliseconds
        CALL_ME_MAX_LIMIT_PER_DEVICE                     : 25,                 // 15 lifetime attempts
        OTP_CALL_ME_DELIVERY_ATTEMPTS_LIMIT              : 20,
        OTP_CALL_ME_DELIVERY_REATTEMPT_DELAY             : 10*1000,            // 10 seconds
        OTP_CALL_ME_REATTEMPT_DELAY                      : 1*1000,
        ALLOWED_FAILED_VERIFY_OTP_ATTEMPTS               : 40,                 // 20 invalid verify otp
        TIME_TO_PAUSE_VERIFY_OTP_ATTEMPTS                : 30*60*1000,         // 30 minutes
        VALID_CALL_TESTING_NUMBERS                       : ['9591999098','9999894738','8341255449','9555670865','9953933422'],
        ALLOWED_REQUEST_OTP_ATTEMPTS                     : 30,                  // 5 allowed request otp limit
        TIME_TO_PAUSE_REQUEST_OTP_ATTEMPTS               : 30*60*1000,         // 30 minutes
        ALLOWED_GROUP_JOIN_ATTEMPTS                      : 40,                 // 40 allowed group join limits
        ALLOWED_FAILED_GROUP_JOIN_ATTEMPTS               : 15,                 // 15 failed group join in 5 minutes 
        TIME_TO_PAUSE_GROUP_JOIN_ATTEMPTS                : 30*60*1000,         // 30 minites,
        TIME_TO_PAUSE_FAILED_GROUP_JOIN_ATTEMPTS         : 5*60*1000,           // 5 minites,
        USER_IDENTIFIER_TOKEN_VALIDITY_TIME              : 6*30*24*60*60,     // 6 month
        USER_IDENTIFIER_HCODE_LENGTH                     : 20,
        USER_IDENTIFIER_TOKEN_PRIVATE_KEY                : "7D3e0T002kL9D1qT3U#9#4*UIRN439idfj7C8De9W3L08D33**6D4ed*#b6P9duerlxc**#DerP87BDe#",
        TIME_TO_PAUSE_USERQUEUE_TOKEN_ATTEMPTS           : 24*60*60*1000,      // 1 day
        JWT_USERQUEUE_TOKEN_PRIVATE_KEY                  : "*Y3by#kL++==@@***R^cQNi#ec74mRa**@@nja45*Nceqs#$$+==##3CdMAH*",
        JWT_USERQUEUE_TOKEN_EXPIRY_TIME                  : 10*60,
        NODE_SERVER_HEAP_SIZE_MAX                        : 3200,
        NODE_SERVER_STACK_SIZE_MAX                       : 300000,
        NODE_SERVER_HEAPDUMP_INTERVAL                    : 15*60*1000         // 15 minutes
};

module.exports = ConfigsOverwrite;