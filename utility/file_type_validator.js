var constants         = require('./constants.js');
var _                 = require('underscore');

//flag : eg. image or audio etc.
//file_type :  jpg, aac, jpeg etc.

var fileTypeValidator = function(mode, file_type){
	var mode   = mode.toUpperCase();
	var format = file_type.toLowerCase();
	if(_.keys(constants.VALID_MEDIA_TYPE).indexOf(mode) == -1) return false;
	if(_.property(mode)(constants.VALID_MEDIA_TYPE).indexOf(format) == -1) return false;
	return true;
};

module.exports = fileTypeValidator;

	  