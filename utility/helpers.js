var constants		= require('./constants.js');
var errorCodes 		= require('./errors.js');


module.exports = {
	sendError : function(res,err,error_index,status_code) {
		console.trace(err);

		if(typeof(status_code) == "undefined" ) {
			status_code = constants.HTTP_STATUS.SERVER_ERROR;
		}

		// res.set("KeepAlive", false);
		res.status(status_code).json({
			code 	: errorCodes[error_index][0],
			message : errorCodes[error_index][1],
			success : false
		});
		return;
	},
	sendSuccess : function(res,data) {
		// res.set("KeepAlive", false);
		res.status(constants.HTTP_STATUS.OK).json({
			success : true,
			data	: data
		});
		return;
	}
};