var rsk = function(client,PREFIX) {
	var redis_client 	= client;

	return {
		//O(n)
		search : function(search_key,cb) {
			redis_client.keys(PREFIX+":"+search_key, function(err, res) {
				if(err) {return cb(new Error(err));}
				
				return cb(null,res);
			});
		},
		//O(1)
		remove : function(delete_key, cb) {
			redis_client.del(PREFIX+":"+delete_key, function(err, res) {
				if(err) {return cb(new Error(err));}
				
				return cb(null,res);
			});
		},
		//O(1)
		set : function(key, value, cb) {
			redis_client.set(PREFIX+":"+key, value,function(err, res) {
				if(err) {return cb(new Error(err));}
				
				return cb(null,res);
			})
		},
		//O(1)
		get : function(key, cb) {
			redis_client.get(PREFIX+":"+key, function(err, res) {
				if(err) {return cb(new Error(err));}
				
				return cb(null,res);
			});
		}
	};
};

module.exports = rsk;