var configs						= require('./configs.js');

var generator = function(phone,digit_count) {
	if( (typeof(configs.IOS_DEMO_NUMBER) !== 'undefined') && (phone == configs.IOS_DEMO_NUMBER) ){
		return configs.IOS_DEMO_OTP;
	}
	var out = "";
	for(var i=0; i< digit_count; i++) {
		if(i == 0) {
			out += (1 + Math.floor(Math.random()*9));
		} else {
			out += Math.floor(Math.random()*10);
		}
	}
	if(process.env.NODE_ENV == "ci" || process.env.NODE_ENV == "dapi") {
		out = "133496";
	}
	return out;
}

module.exports = generator;