var mongoose 						= require('mongoose');

var otpGenerator				= require('./../otp_generator.js');
var RabbitMqPublish     = require('./../../rabbitmq/publisher/publisher.js');
var RabbitMqQueues      = require('./../../rabbitmq/queues.js');
var constants						= require('./../constants.js');

require('./../../models/accounts.js');
require('./../../models/otps.js');

var Account 						= mongoose.model('Account');
var Otp 								= mongoose.model('Otp');

exports.accountUnlockByOtp = function(account, cb){
	Otp.update({
		aid  : account._id,
		cdst : Otp.STATUS.NOTUSED
	},{
		cdst : Otp.STATUS.INVALID
	},{
		multi : true
	},function(err,otp_updated){
		if(err){
			return cb(err,null);  
		}
		var otp = otpGenerator(account.m,constants.OTP_LENGTH);
		var new_otp = new Otp({
			code : otp,
			gnat : (new Date().getTime()),
			smst : Otp.SMS_STATUS.NOT_SENT,
			cdst : Otp.STATUS.NOTUSED,
			aid  : account._id,
		});
		new_otp.save(function(err, new_otp){
			if(err){
				return cb(err,null);  
			}
			if(process.env.NODE_ENV == "ci" || process.env.NODE_ENV == "debug" || process.env.NODE_ENV == "dapi") {
				return cb(null,true);
			}
			var otp_obj = new Buffer(JSON.stringify({
		    _id : otp_id,
				m 	: account.m,
				ccod: account.ccod,
				code: otp_code,
				atmpt : 0
		  }));
			RabbitMqPublish.publishOtp("",RabbitMqQueues.OTP_DELIVERY,otp_obj,function(err,resp){
				if(err){
					return cb(err,null);
				}
				if(resp){
					return cb(null,true);
				} else {
					return cb("otp_could not_be_published",false);
				}
			});
		});
	});
};