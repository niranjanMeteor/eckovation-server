var http 							= require('http');
var passwords 				= require('./../password.js');
var configs 					= require('./../configs.js');
var SmsEngine         = require('./sms_engines.js');
var SmsEngineConfigs  = require('./sms_engines_config.js');
var mongoose          = require('mongoose');
var https             = require('https');

require('../../models/otpstatuses.js');
var OtpStatus 				= mongoose.model('OtpStatus');
var PROTOCOL 					= "http";
var httpUrl           = "http";
var httpsUrl          = "https";

//http://smsalertbox.com/api/sms.php?uid=616b73686174676f656c&pin=52d3b166edf7f&sender=ECKEDU&route=5&tempid=2&mobile=9800125211&message=Your%20OTP%20is%20100.%20Please%20enter%20it%20in%20your%20app.%20Thanks%20for%20downloading%20Eckovation.&pushid=1
//https://control.msg91.com/api/sendhttp.php?authkey=YourAuthKey&mobiles=919999999990,919999999999&message=message&sender=ECKVTN&route=1&country=0

exports.sender = function(otp_id, engine, country_code, phone, otp_code, message, callback) {
	if(process.env.NODE_ENV == "ci" || process.env.NODE_ENV == "debug" || process.env.NODE_ENV == "dapi") {
		return callback({},true);
	}
	if(engine == SmsEngine.NEXMO){
		return brandNexmoSms(otp_id, engine, country_code, phone, otp_code, message, callback);
	} else {
		return brandLocalSms(otp_id, engine, country_code, phone, message, callback);
	}
}

function brandNexmoSms(otp_id, engine, ccode, phone, otp_code, message, callback){
	var smsConfigs = SmsEngineConfigs(engine);
	var credentials  = {
		api_key    : smsConfigs.API_KEY,
		api_secret : smsConfigs.API_SECRET,
		to         : ccode+''+phone
	};
	var options = {
	  host       : smsConfigs.HOST,
	  port       : smsConfigs.PORT,
	  method     : 'POST'
	};
	if(ccode == '1'){
		credentials.pin  = otp_code;
		options.path     = smsConfigs.API_PATH_USA;
	} else {
		credentials.from = smsConfigs.FROM;
		credentials.text = message;
		options.path = smsConfigs.API_PATH;
	}
	
	var data = JSON.stringify(credentials);
	options.headers = {
    'Content-Type': 'application/json',
    'Content-Length': Buffer.byteLength(data)
  };
	

	var req = https.request(options);
	req.write(data);
	req.end();
	var responseData = '';
	req.on('response', function(res){
	  res.on('data', function(chunk){
	   responseData += chunk;
	  });
	  var decodedResponse;
	  res.on('end', function(){
	  	try{
		    decodedResponse = JSON.parse(responseData);
		    var respData = decodedResponse['messages'][0];
		    if(respData["status"] == "0") {
		      console.log('Success sms for engine :'+engine+' and otp_id : '+otp_id+' ,phone : '+phone+' ,ccode : '+ccode);
		      return callback(respData,true);
		    } else {
		    	console.log('failure sms for engine :'+engine+' and otp_id : '+otp_id+' ,phone : '+phone+' ,ccode : '+ccode);
		      console.log(decodedResponse);
		      return callback(respData,false);
		    }
		  } catch(e){
		   	console.log(e);
		   	console.log('Excepetion raised, failure sms for engine :'+engine+' and otp_id : '+otp_id+' ,phone : '+phone+' ,ccode : '+ccode);
		  	console.log(responseData);
		  	return callback(responseData,false);
		  }
	  });
	});
}

function brandLocalSms(otp_id, engine, ccode, phone, message, callback){
	var MakeCall, method, was_queued;
	var urlAndProtocol = getSmsSendingUrl(engine, ccode, phone, message);
	if(urlAndProtocol.protocol == httpUrl){
		MakeCall     = http;
		method       = 'http';
	} else if(urlAndProtocol.protocol == httpsUrl){
		MakeCall     = https;
		method       = 'https';
	}
	console.log('http SmsUrl request : '+urlAndProtocol.smsUrl);
	try {
		MakeCall.get(urlAndProtocol.smsUrl,function(response){
			response.setEncoding('utf8');
			response.on("data",function(data) {
				var was_queued = checkResponseStatus(data,engine);
				if (was_queued) {
					console.log(method+' Success sms for engine :'+engine+' and otp_id : '+otp_id+' ,phone : '+phone+' ,ccode : '+ccode);
				} else {
					console.log(method+' Failure sms for engine :'+engine+' and otp_id : '+otp_id+' ,phone : '+phone+' ,ccode : '+ccode);
					console.log(data);
				}
				return callback(data,was_queued);
			});
		});
	} catch(excp){
		console.log(excp);
		console.log(method+' Exception and Failure sms for engine :'+engine+' and otp_id : '+otp_id+' ,phone : '+phone+' ,ccode : '+ccode);
		console.log(data);
		return callback(null,false);
	}
}

function getSmsSendingUrl(engine, country_code, phone, message){
	var smsConfigs      = SmsEngineConfigs(engine);
	var HOST            = smsConfigs.HOST;
	var API_PATH        = smsConfigs.SEND_API_PATH;
	var CALL_PROTOCOL   = smsConfigs.PROTOCOL;
	var params          = {};
	
	params.sender       = smsConfigs.SENDER_ID;
	params.route        = smsConfigs.ROUTE;
	params.message      = message;

	switch(engine){
		case SmsEngine.MSG_91:
			params.mobiles  = country_code+''+phone;
			params.authkey  = smsConfigs.AUTH_KEY;
			params.country  = 0;
			params.response = "json";
		break;
		case SmsEngine.ALERTBOX:
			params.mobile   = phone;
			params.uid      = smsConfigs.USERNAME;
			params.pin      = smsConfigs.PASSWORD;
			params.tempid   = smsConfigs.TEMPID;
			params.pushid   = smsConfigs.PUSHID;
		break;
	}
	params["message"]   = encodeURIComponent(params["message"]);

	var tmp = new Array();
	for(var key in params) {
		tmp.push(key+"="+params[key]);
	}
	var query_string = tmp.join("&");
	var url = CALL_PROTOCOL+"://"+HOST+API_PATH+"?"+query_string;

	return {protocol : CALL_PROTOCOL, smsUrl : url};
}

function checkResponseStatus(response,engine){
	switch(engine){
		case SmsEngine.MSG_91:
			try{
				response = JSON.parse(response);
				if(response.type == "success")
					return true;
				else return false;
			} catch (e) {  
				console.log('exception occurent from MSG_91 SMS SERVICE');
				console.log(e);
				console.log(response);
				return false;
			}

		case SmsEngine.ALERTBOX:
			var regex = /^[0-9]+$/;
			return regex.test(response);
	}
}

function saveOtpStatus(otp_id, sms_engine, response) {
	OtpStatus.findOne({
		oid : otp_id,
		engn : sms_engine
	},function(err,result){
		if (err) { console.trc(err); }

		if (result) { 
			console.log('In saveOtpStatus : OtpId = '+ otp_id+' has already been tried by Engine = '+sms_engine); 
			console.log('In saveOtpStatus : So , Just updating the Delvery Report only');
			updateSmsDeliveryReport(result._id);
			return;
		}

		newOtpStatus = new OtpStatus({
			oid : otp_id,
			engn : sms_engine,
			resp : response
		});

		newOtpStatus.save(function(err,otp_status) {
			if (err) { console.trc(err); }

			if (!otp_status) {
				console.log('In saveOtpStatus : OtpStatus for OtpID = '+otp_id+' from Engine = '+sms_engine+' could not be saved'); 
				return;
			}
			console.log('In saveOtpStatus : OtpStatus for OtpID = '+otp_id+' from Engine = '+sms_engine+' generated successfully');
			setTimeout(function() {
				updateSmsDeliveryReport(otp_status._id)
			},10000);
		});

	});
}

function updateSmsDeliveryReport(otp_status_id) {
	OtpStatus.findOne({
		_id : otp_status_id
	},function(err, otp_status) {
		if (err) { console.trc(err); }

		if (!otp_status) {
			console.log('In updateSmsDeliveryReport : otp status ID is = '+otp_status_id);
			console.log('In updateSmsDeliveryReport : No Otp Status found for fetching delivery Report');
			return;
		}
		switch (otp_status.engn) {
			case OtpStatus.SMS_ENGINE.ALERTBOX:
				alertBoxDeliverReport(otp_status);
				break;
		}
	});
}

function alertBoxDeliverReport(otp_status_doc) {
	//http://smsalertbox.com/api/dlr.php?uid=616b73686174676f656c&pin=52d3b166edf7f&pushid=88379871&rtype=json
	var host = SmsEngine.ALERTBOX_HOST;
	var api_path = SmsEngine.ALERTBOX_DEL_REP_API_PATH;
	var PARAMS = {
		uid 	  : passwords.SMS_GATEWAY_USERNAME,
		pin 	  : passwords.SMS_GATEWAY_PASSWORD,
		pushid 	: otp_status_doc.resp,
		rtype   : 'json'
	};

	var tmp = new Array();
	for(var key in PARAMS) {
		tmp.push(key+"="+PARAMS[key]);
	}
	var query_string = tmp.join("&");
	http.get(PROTOCOL+"://"+host+api_path+"?"+query_string,function(response){
		response.setEncoding('utf8');
		response.on("data",function(data) {
			if (data == 'null' || data === undefined || data === null || data == 'undefined') {
				console.log('In alertBoxDeliverReport : data is null');
				return;
			} else {
				var report = JSON.parse(data)[0];
				reportStatus = getReportDeliveryStatus(report.status);
				if (reportStatus == '' || reportStatus === null || reportStatus === undefined) {
					console.log('In alertBoxDeliverReport : invalid report status');
					return;
				}
				OtpStatus.update({
					_id : otp_status_doc._id
				},
				{
					svst : reportStatus
				},function(err) {
					if (err) { 
						console.trace(err); 
						console.log('In alertBoxDeliverReport , For OTP_ID = '+otp_status_doc._id+' : updating delivery status report failed');
						return;
					}
					console.log('In alertBoxDeliverReport , For OTP_ID = '+otp_status_doc._id+' : updating delivery status report successfull');
				});
			}
		});
	});
}

function getReportDeliveryStatus(report_status) {
	switch (report_status) {
		case OtpStatus.SERVICE_STATUS_STRING.DELIVERED:
			return OtpStatus.SERVICE_STATUS_NUMBER.DELIVERED;

		case OtpStatus.SERVICE_STATUS_STRING.SENT:
			return OtpStatus.SERVICE_STATUS_NUMBER.SENT;

		case OtpStatus.SERVICE_STATUS_STRING.NOT_SENT:
			return OtpStatus.SERVICE_STATUS_NUMBER.NOT_SENT;

		case OtpStatus.SERVICE_STATUS_STRING.FAILED:
			return OtpStatus.SERVICE_STATUS_NUMBER.FAILED;

		case OtpStatus.SERVICE_STATUS_STRING.DND:
			return OtpStatus.SERVICE_STATUS_NUMBER.DND;

		case OtpStatus.SERVICE_STATUS_STRING.INVALID_MESSAGEID_SENT:
			return OtpStatus.SERVICE_STATUS_NUMBER.INVALID_MESSAGEID_SENT;

		case OtpStatus.SERVICE_STATUS_STRING.INVALID_REQUEST:
			return OtpStatus.SERVICE_STATUS_NUMBER.INVALID_REQUEST;

		case OtpStatus.SERVICE_STATUS_STRING.DND_TEMPLATE_MISMATCH:
			return OtpStatus.SERVICE_STATUS_NUMBER.DND_TEMPLATE_MISMATCH;

		case OtpStatus.SERVICE_STATUS_STRING.REJECTED:
			return OtpStatus.SERVICE_STATUS_NUMBER.REJECTED;
	}
}
