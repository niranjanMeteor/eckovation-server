var SmsEngines        = require('./sms_engines.js');

var SmsEngineConfigs = function(engine){
	var configs = {};
	switch(engine){
		case SmsEngines.ALERTBOX:
		  configs.PROTOCOL              = "http";
			configs.HOST 						      = "smsalertbox.com";
			configs.AUTH_KEY              = "";
			configs.USERNAME							= "616b73686174676f656c";
			configs.PASSWORD							= "52d3b166edf7f";
			configs.SENDER_ID             = "ECKVTN";
			configs.ROUTE                 = 5;
			configs.TEMPID                = 2;
			configs.PUSHID                = 1;
			configs.SEND_API_PATH 		    = "/api/sms.php";
			configs.DEL_REP_API_PATH      = "/api/dlr.php";
			return configs;

		case SmsEngines.MSG_91:
		  configs.PROTOCOL              = "http";
			configs.HOST 						      = "control.msg91.com";
			configs.AUTH_KEY              = "88664ABwiMbdFL563db683";
			configs.SENDER_ID             = "ECKVTN";
			configs.ROUTE                 = 4;
			configs.USERNAME							= "";
			configs.PASSWORD							= "";
			configs.SEND_API_PATH 		    = "/api/sendhttp.php";
			configs.DEL_REP_API_PATH      = "";
			return configs;

		case SmsEngines.NEXMO:
		  configs.PROTOCOL              = "https";
		  configs.PORT							    = "443";
			configs.API_KEY               = "c890d09d";
			configs.API_SECRET					  = "32c5bde81bcb1f07";
			configs.HOST 						      = "rest.nexmo.com";
			configs.API_PATH 		          = "/sms/json";
			configs.API_PATH_USA 		      = "/sc/us/2fa/json";
			configs.SENDER_ID             = "ECKVTN";
			configs.FROM							    = "441632960961";
			return configs;
	}
};

module.exports = SmsEngineConfigs;