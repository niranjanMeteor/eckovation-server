var mail_client             = require('./../mail/mail_client.js');
var configs                 = require('./../configs.js');
var ip                      = require('ip');
var os                      = require("os");
var server_num              = process.argv[2];

var html_string = '<h4>'+ Date()  +': The Application Server <'+server_num+'> has Restarted </h4>';
var myIpAddres  = ip.address(); 
var hostname    = os.hostname();


var mailOptions = {
    from    : configs.TECH_FROM, // sender address
    to      : configs.TECH_TO, // list of receivers
    subject : myIpAddres+' : '+hostname+' : Eckovation Node Server <'+server_num+'> Restarted',
    html    : html_string, // plaintext body
};

mail_client.send(mailOptions, function(err, response){
    if (err) {
    	console.trace(err);
        console.log('Server Restart : Mail not sent successfully',err);
    } else {
        console.log('Server Restart : Mail sent successfully');
    }
});