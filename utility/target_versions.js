var TargetVersions	=	{
	GROUP_DELETE	:	{
		a  : "4.3",
		m  : "4.1",
		i  : "1.9",
		wb : "1.0",
		wn : "1.0"
	},
	APP_BASE_VERSION	:	{
		m  : "1.14",
		a  : "1.16",
		i  : "1.0",
		wb : "1.0",
		wn : "1.0"
	}
};

module.exports = TargetVersions;