var Prefixes = {
	SEEN 				             : 'sn:',
	SEEN_COUNT               : 'sc:',
	SEEN_LIST                : 'sl:',
	STATUS_UPDATE_JOB        : 's_u_job:',
	STATUS_UPDATE_PENDING    : 'st_up_pending',
	MEDIA_CONVERSION_JOB     : 'md_conv_job:',
	MEDIA_CONVERSION_PENDING : 'md_conv_pending',
	USER_PROF_GMS            : 'hms:',
	STATUS_INSERT_JOB        : 'st_in_job:',
	STATUS_INSERT_PENDING    : 'st_in_pending',
	GROUP_INFO               : 'hmsg:',
	PROFILES                 : 'hms_profiles:',
	GROUP_MEMBERS            : 'hms_groupmems:',
	GROUPS                   : 'hms_groups:',
	UNSECURE_ACCOUNTS        : 'unsecured_accounts:',
	UNSECURE_PROFILES        : "unsecured_profiles:",
	BRUTE_FORCE_ACCOUNT      : "brute_force:",
	BRUTE_FORCE_PHONE_OTP    : "brute_force_otp:",
	BRUTE_FORCE_GROUP_JOIN   : "brute_force_group_join:",
	SOCKET_ACCOUNT_TOKEN     : "socket_account_tokens:",
	PLUGIN_STATE             : "plugin_state:",
	ACCOUNTDATA              : "hms_accountdatas:",
	GROUP_ACCOUNTDATA        : "hms_group_acountdatas:",
	BRUTE_FORCE_USERQUEUE    : "brute_force_userqueue:"
};

module.exports = Prefixes;