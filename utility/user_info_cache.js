var mongoose            = require('mongoose');
var redis               = require('redis');

var configs             = require('./configs.js');
var RedisPrefixes       = require('./redis_prefix.js');
var UserRedisKeys       = require('./user_redis_keys.js');


var redis_client        = redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
    console.log("Error " + err);
});

var rhmset              = require("./redis_hmset.js")(redis_client);

require('../models/profiles.js');
require('../models/groupmembers.js');

var Profile             = mongoose.model('Profile'); 
var GroupMember         = mongoose.model('GroupMember');

exports.getProfilesGrpMemspartial = function(account_id, cb){
    var toDo          = 2;
    var done          = 0;
    var key           = RedisPrefixes.USER_PROF_GMS+account_id+'';
    var profilesF     = [];
    var groupmembersF = [];
    rhmset.m2get(key,"profiles","groupmembers",function(err,rProfilesGrpmems){
        if(err){ 
            return cb(err,null); 
        }
        console.log('getProfilesGrpMemspartial  profilesAndGrpmems : ');
        console.log(rProfilesGrpmems);
        if(rProfilesGrpmems && (rProfilesGrpmems.length == 2) && rProfilesGrpmems[0] && rProfilesGrpmems[1]){
            var toReturn = {
                profiles     : JSON.parse(rProfilesGrpmems[0]),
                groupmembers : JSON.parse(rProfilesGrpmems[1]) 
            };
            return cb(null, toReturn);
        }
        Profile.find({
            aid : account_id,
            act : true
        },function(err, profiles) {
            if(err){ 
                return cb(err,null); 
            }
            done++;
            profilesF = profiles;
            if(done == toDo){
                rhmset.m2set(RedisPrefixes.USER_PROF_GMS,account_id,
                    "profiles",
                    JSON.stringify(profilesF),
                    "groupmembers",
                    JSON.stringify(groupmembersF),
                    function(err,resp){
                        if(err){
                            console.trace(err);
                        } else if(!resp) {
                            console.log('Could not set user profiles in redis hmset for account id : '+account_id);
                        }
                        return cb(null, {
                            profiles     : profilesF,
                            groupmembers : groupmembersF
                        });
                    }
                );
            }
        });
        GroupMember.find({
            aid : account_id,
            act : true,
            gdel: false
        },{
            gid    : 1,
            type   : 1,
            sub_ed : 1,
            gpaid  : 1,
            _id    : 0
        },function(err, groupmembers) {
            if(err) { 
                return cb(err,null); 
            }
            done++;
            groupmembersF = groupmembers;
            if(done == toDo){
              rhmset.m2set(RedisPrefixes.USER_PROF_GMS,account_id,
                    "profiles",
                    JSON.stringify(profilesF),
                    "groupmembers",
                    JSON.stringify(groupmembersF),
                    function(err,resp){
                        if(err){
                            console.trace(err);
                        } else if(!resp) {
                            console.log('Could not set user profiles in redis hmset for account id : '+account_id);
                        }
                        return cb(null, {
                            profiles     : profilesF,
                            groupmembers : groupmembersF
                        });
                    }
                );  
            }
        });
    });
};

exports.getUserProfilesPartial = function(account_id, cb){
    var key = RedisPrefixes.USER_PROF_GMS+account_id+'';
    rhmset.get(key,UserRedisKeys.PROFILES,function(err,rProfiles){
        if(err){ 
            return cb(err,null); 
        }
        if(rProfiles){
            var toReturn = JSON.parse(rProfiles);
            return cb(null, toReturn);
        } else {
            Profile.find({
                aid : account_id,
                act : true
            },function(err, profiles) {
                if(err){ 
                    return cb(err,null); 
                }
                if(profiles.length == 0){
                    return cb(null, profiles);
                }
                var toStore = JSON.stringify(profiles);
                rhmset.set(RedisPrefixes.USER_PROF_GMS,account_id,UserRedisKeys.PROFILES,toStore,function(err,resp){
                    if(err){
                        console.trace(err);
                    } else if(!resp) {
                        console.log('Could not set user profiles in redis hmset for account id : '+account_id);
                    }
                    return cb(null, profiles);
                });
            });
        }
    });
};

exports.getUserGroupMemPartial = function(account_id, cb){
    var key = RedisPrefixes.USER_PROF_GMS+account_id+'';
    rhmset.get(key,UserRedisKeys.GROUPS,function(err,rGroups){
        if(err) { 
            return cb(err,null); 
        }
        if(rGroups){
            var toReturn = JSON.parse(rGroups);
            return cb(null, toReturn);
        } else {
            GroupMember.find({
                aid : account_id,
                act : true,
                gdel: false
            },{
                gid    : 1,
                type   : 1,
                sub_ed : 1,
                gpaid  : 1,
                _id    : 0
            },function(err, groups) {
                if(err) { 
                    return cb(err,null); 
                }
                if(groups.length == 0){
                    return cb(null, groups);
                }
                var toStore = JSON.stringify(groups);
                rhmset.set(RedisPrefixes.USER_PROF_GMS,account_id,UserRedisKeys.GROUPS,toStore,function(err,resp){
                    if(err){
                        console.trace(err);
                    } else if(!resp) {
                        console.log('Could not set user groups in redis hmset for account id : '+account_id);
                    }
                    return cb(null, groups);
                });
            });
        }
    });
};

exports.getMongoProfile = function(pid, cb){
    var pid = pid+'';
    var key = RedisPrefixes.PROFILES;
    rhmset.get(key,pid,function(err,profile){
        if(err) { 
            return cb(err,null); 
        }
        if(profile){
            var toReturn = JSON.parse(profile);
            return cb(null, toReturn);
        } else {
            Profile.findOne({
                _id : pid,
                act : true
            },function(err, profile) {
                if(err) { 
                    return cb(err,null); 
                }
                cb(null, profile);
                if(!profile) return;
                var toStore = JSON.stringify(profile);
                rhmset.setMongoObj(key,pid,toStore,function(err,resp){
                    if(err){
                        console.trace(err);
                    } else if(!resp) {
                        console.log('Could not set user profiles in redis hmset for profile id : '+pid);
                    }
                    return;
                });
            });
        }
    });
};

exports.getMongoGrpMem = function(pid, gid, cb){
    var pid = pid+'';
    var gid = gid+'';
    var key = RedisPrefixes.GROUP_MEMBERS;
    var field = gid+'_'+pid;
    rhmset.get(key,field,function(err,groupMem){
        if(err) { 
            return cb(err,null); 
        }
        if(groupMem){
            var toReturn = JSON.parse(groupMem);
            return cb(null, toReturn);
        } else {
            GroupMember.findOne({
                gid : gid,
                pid : pid,
                act : true,
                gdel: false
            },function(err, grpMem) {
                if(err) { 
                    return cb(err,null); 
                }
                cb(null, grpMem);
                if(!grpMem) return;
                var toStore = JSON.stringify(grpMem);
                rhmset.setMongoObj(key,field,toStore,function(err,resp){
                    if(err){
                        console.trace(err);
                    } else if(!resp) {
                        console.log('Could not set user groupmembers in redis hmset for profile id : '+pid+'  , and gid : '+gid);
                    }
                    return;
                });
            });
        }
    });
};

exports.rmUsrGrpMemsByAidInCache = function(aid, cb){
    var key = RedisPrefixes.USER_PROF_GMS+aid+'';
    rhmset.keyexists(key,UserRedisKeys.GROUPS,function(err,isKey){
        if(err){
            return cb(err,null);
        }
        if(isKey){
            rhmset.removefield(key,UserRedisKeys.GROUPS,function(err,resp){
                if(err){
                    return cb(err,null);
                } 
                if(resp){
                    return cb(null,true);
                } else {
                    console.log("Oops,this is unfortunate that Account Profiles key could not be deleted for Account : "+aid);
                    return cb(null,false);
                }
            });
        } else {
            console.log("Oops,this is unfortunate that Account Profiles key does not exists for Account : "+aid);
            return cb(null,false);
        }
    });
}

exports.removeMongoGrpMem = removeMongoGrpMem;

function removeMongoGrpMem(pid, gid, cb){
    var pid = pid+'';
    var gid = gid+'';
    var key = RedisPrefixes.GROUP_MEMBERS;
    var field = gid+'_'+pid;
    rhmset.keyexists(key,field,function(err,isKey){
        if(err){
          return cb(err,null);   
        }
        if(isKey){
            rhmset.removefield(key,field,function(err,resp){
                if(err){
                    return cb(err,null);
                } 
                if(resp){
                    return cb(null,true);
                } else {
                    console.log("Oops,this is unfortunate that Group Members HMSET key could not be deleted for gid : "+gid+' , pid : '+pid);
                    return cb(null,false);
                }
            });
        } else {
            console.log("Oops,this is unfortunate that Group Members HMSET key does not exists for Account : "+gid+' , pid : '+pid);
            return cb(null,false);
        }
    });
};

exports.removeMultiMongoGrpMem = function(pids, gid, cb){
    var toBeDone = pids.length;
    var done     = 0;
    if(toBeDone == 0){
        return cb(null,true); 
    }
    for(var i=0; i< toBeDone; i++){
        removeMongoGrpMem(pids[i],gid,function(err,resp){
            done++
            if(err){
                return cb(err,null);
            } 
            if(done == toBeDone){
                return cb(null,true);
            }
        });
    }
}

exports.removeMongoProfile = function(pid, cb){
    var pid = pid+'';
    var key = RedisPrefixes.PROFILES;
    rhmset.keyexists(key,pid,function(err,isKey){
        if(err){
          return cb(err,null);   
        }
        if(isKey){
            rhmset.removefield(key,pid,function(err,resp){
                if(err){
                    return cb(err,null);
                } 
                if(resp){
                    return cb(null,true);
                } else {
                    console.log('Oops,this is unfortunate that Profiles HMSET key could not be deleted for  pid : '+pid);
                    return cb(null,false);
                }
            });
        } else {
            console.log('Oops,this is unfortunate that Profiles HMSET key does not exists for pid : '+pid);
            return cb(null,false);
        }
    });
};


