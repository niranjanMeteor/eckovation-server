var fs                                = require('fs');

var configs = {
	SERVER_PORT                         : "3000",
	SERVER_PORT_2                       : "3002",
	DB_NAME 														: "eckovation",
	MONGO_HOST                          : "127.0.0.1",
	MONGO_PORT                          : "27017",
	MONGO_REPLICA_SET_NAME              : "rs2",
	MONGO_UNAME													: "eck_user",
	MONGO_PASS													: "eck!!--^^(12)3_=_user",
	MONGO_POOLSIZE                      : 5,
	MONGO_LOWER_POOLSIZE                : 5,
	MONGO_SERVER_KEEPALIVE              : 120,
	MONGO_REPLICA_KEEPALIVE             : 120,
	REDIS_PASS													: "eck!!--^^(12)3_=_user######$$$$!@!@!#@@#@",
	REDIS_HOST													: "127.0.0.1", // Testing server 119.81.222.206
	REDIS_PORT													: 6379,        // Testing Server  26379
	REDIS_CHAT_DB												: 4,
	OTP_VERIFICATION_EXPIRY_TIME        : 30*60*1000, // 30 minutes expressed in milliseconds
	SMS_SENDER_ID 											: "ECKVTN",
	SMS_OTP_TEMPLATE										: "Your OTP is <%= code %>. Please enter it in your app. Thanks for downloading Eckovation.",
	G_CODE_LENGTH 											: 6,
	MAX_TEXT_MESSAGE_LENGTH							: 60000,
	AWS_ACCESS_KEY											: 'AKIAJYLIZFRJAUEK3BGQ',
	AWS_SECRET_KEY											: 'gbjWILxiQn98swIVG57VKvYggFxxW4fe8cANogOW',
	AWS_SNS_ACCESS_KEY									: 'AKIAISZHHV6VV5CHCBUA',
	AWS_SNS_SECRET_KEY									: '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',
	AWS_REGION 													: "ap-southeast-1", 
	AWS_OUTPUT            							: "table",
	BUCKET_NAME_PROFILE_PIC							: "eckppic",
	BUCKET_NAME_GROUP_PIC								: "eckgpic",
	BUCKET_NAME_CHAT_PIC								: "eckcpic",
	VALID_IMAGE_SIZE 										: 2097152,
	AMAZON_EXPIRY_TIME									: 900,
	ANALYTICS_SMTP_HOST									: 'smtp.zoho.com',
	ANALYTICS_SMTP_PORT									: 465,
	ANALYTICS_SMTP_USERNAME							: 'hi@eckovation.com',
	ANALYTICS_SMTP_PASSWORD							: 'eckovationECKOVATION987@123',
	SMTP_HOST_GENERIC				            : 'smtp.zoho.com',
	SMTP_PORT_GENERIC				            : 465,
	SMTP_USERNAME_GENERIC		            : 'hi@eckovation.com',
	SMTP_PASSWORD_GENERIC		            : 'eckovationECKOVATION987@123',
	PAYMENT_MAILING_GROUP		            : 'payments@eckovation.com',
	ANALYTICS_FROM											: 'Eckovation Analytics <hi@eckovation.com>',
	OTP_FROM											      : 'Eckovation Otp <hi@eckovation.com>',
	PUSHNOTIFY_FROM                     : 'Eckovation Push Notify <hi@eckovation.com>',
	RABBITMQ_WORKER_FROM								: 'Eckovation RabbitMq Worker <hi@eckovation.com>',
	SERVER_FROM								          : 'Eckovation Server <hi@eckovation.com>',
	MOMENT_TIMEZONE											: 'Asia/Kolkata',
	TRENDING_GROUPS_TIMEZONE						: 'Asia/Kolkata',
	ANALYTICS_TO 												: 'akshatg@eckovation.com,riteshs@eckovation.com,niranjan@eckovation.com',
	ANALYTICS_NEW 											: 'akshatg@eckovation.com,riteshs@eckovation.com,niranjan@eckovation.com,prashant@eckovation.com',
	TECH_FROM				    								: 'Eckovation Tech <hi@eckovation.com>',
	TECH_TO															: 'akshatg@eckovation.com,niranjan@eckovation.com',
	PAYMENTS_TO                         : 'akshatg@eckovation.com,riteshs@eckovation.com,niranjan@eckovation.com',
	PAYMENTS_FROM											  : 'Eckovation Payments <hi@eckovation.com>',
	SPECIAL_GROUP_REPORT_CC             : 'riteshs@eckovation.com',
	GROUP_ACTIVITY_PARAMETER						: 3,
	PROFILE_ACTIVITY_PARAMETER					: 3,
	REDIS_INIT_LOG_QUEUE_IDEN						: "intl",
	REDIS_INIT_LOG_REATTEMPT_CNT				: 1,
	REDIS_INIT_LOG_REATTEMPT_DELAY 			: 1*1000,
	REDIS_INIT_LOG_QUEUE_CONCURRENCY 		: 100,
	NODE_SERVER_SERVICE_NAME 						: 'eckovation-server',
	NODE_SERVER_LOG_DIR 								: '/var/log/eckovation',
	NODE_SERVER_USER    								: 'ubuntu',
	MORGAN_LOG_DIRECTORY                : '/var/log/eckovation/daily',
	APP_BASE_VERSION_MOBILE         		: "1.14",
	APP_BASE_VERSION_ANDROID            : "1.16",
	APP_CI_VERSION                      : "1.16",
	TRENDING_GROUPS_SIZE                : 150,
	TRENDING_GROUPS_ALPHA_COEFFICIENT   : 1,
	TRENDING_GROUPS_BETA_COEFFICIENT    : 1,
	NGINX_MAIN_WEB_SERVER               : '119.81.222.200',
	NGINX_API_PATHSHALA_SERVER          : '52.74.87.178',
	APPLICATION_ARN_ANDROID             : 'arn:aws:sns:ap-southeast-1:279388982033:app/GCM/eckovation-app-development',
	APPLICATION_ARN_IOS                 : 'arn:aws:sns:ap-southeast-1:279388982033:app/APNS_SANDBOX/EckovationIosDevelopment',
	APPLICATION_ARN_WINDOWS             : 'arn:aws:sns:ap-southeast-1:279388982033:app/WNS/EckovationWindowsPhoneProduction',
	APPLICATION_ARN_WEB                 : '',
	GCM_NEXT_PUSH_TIME_PERIOD           : 10*60*1000,
	GROUP_CODE_GENERATE_RETRY           : 10,
	CONFIGS_OVERWRITE_FILE              : 'configs_overwrite.js',
	MOBILE_NUMBER_ENCRYPT_ALGO          : 'aes-128-ecb',
	MOBILE_NUMBER_DECRYPT_KEY_SALT      : '*3$!@k#%$*3P%#Hqm78@Rr$',
	MOBILE_NUMBER_SHA_HASH_KEY          : 'eckovationECKOVATION654321',
	MESSAGE_LOAD_IPP_LIMIT              : 15,
	JWT_ACCESS_TOKEN_SECRET_KEY         : 'eckovationECKOVATION654321',
	JWT_ACCESS_TOKEN_PRIVATE_KEY        : "eckovationECKOVATION654321123456ECKOVATIONeckovation",
	JWT_ACCESS_TOKEN_EXPIRY_TIME        : 60*4,
	JWT_ACCESS_TOKEN_EXPIRY_TIME_ANDROID: 60*1,
	JWT_ACCESS_TOKEN_EXPIRY_TIME_WEB    : 60*60*60, 
	JWT_ACCESS_TOKEN_EXPIRY_TIME_IOS    : 60*30,   
	JWT_REFRESH_TOKEN_PRIVATE_KEY       : "eckovationECKOVATION654321123456ECKOVATIONeckovation",
	JWT_REFRESH_TOKEN_EXPIRY_TIME       : 60*8,
	JWT_REFRESH_TOKEN_EXPIRY_TIME_WEB   : 60*60*60,
	JWT_REFRESH_TOKEN_EXPIRY_TIME_IOS   : 60*60,
	JWT_REFRESH_TOKEN_EXPIRY_TIME_ANDROID : 60*3,
	JWT_TOKEN_SECRET_KEY_HASH_SALT      : "eckovationECKOVATION654321123456ECKOVATIONeckovation",
	JWT_REFRESH_TOKEN_SECRET_KEY        : "eckovationECKOVATION654321123456ECKOVATIONeckovation",
	APNS_ENVIRONMENT                    : 'P',   // T for APNS_SANDBOX   , P for APNS,
	DEVICE_ID_TESTING_ENV               : '00000000000000000',
	RABBITMQ_HOST                       : 'localhost',
	RABBITMQ_PORT                       : 5672,
	RABBITMQ_USER                       : 'niranjkk',
	RABBITMQ_PASSWORD                   : 'niranjkk1234**',
	SEEN_LIST_LIMIT                     : 30,
	MEDIA_ANDROID_FILE_PENDING_DIR      : '/var/log/eckovation/media/android/pending',
	MEDIA_IOS_FILE_PENDING_DIR          : '/var/log/eckovation/media/ios/pending',
	MEDIA_ANDROID_FILE_DONE_DIR         : '/var/log/eckovation/media/android/done',
	MEDIA_IOS_FILE_DONE_DIR             : '/var/log/eckovation/media/ios/done',
	APP_BASE_VERSION_IOS        				: '1.0',
	APP_BASE_VERSION_WEB                : '1.0',
	APP_BASE_VERSION_WINDOWS            : '1.0',
	RANDOM_NUMBER_LENGTH                : 16,
	MESSAGE_LOAD_PREV_LIMIT             : 20,
	IOS_DEMO_NUMBER                     : '998877665544',
	IOS_DEMO_OTP                        : '123456',
	LOGGER_APP_ENV                      : 'T', // For CI ENV = "ci" , for Shared Server = "dapi"
	OTP_DELIVERY_ATTEMPTS_LIMIT         : 7,
	SMS_OTP_REATTEMPT_DELAY             : 2000, // In Milliseconds
	DB_CONNECTION_RETTEMPT_LIMIT        : 20,
	DB_CONNECTION_RETTEMPT_LIMIT_NODE   : 25,
	RABBIT_EMITRCQ_SERVICE_NAME 		    : 'rabbit-emitrcq-server',
	RABBIT_EMITRCQ_SERVICE_LOG_DIR 			: '/var/log/eckovation/rabbit/emitrcq',
	RABBIT_EMITRCQ_SERVICE_USER    			: 'ubuntu',
	RABBIT_OTP_SERVICE_NAME 		    		: 'rabbit-otp-server',
	RABBIT_OTP_SERVICE_LOG_DIR 					: '/var/log/eckovation/rabbit/otp',
	RABBIT_OTP_SERVICE_USER    					: 'ubuntu',
	RABBIT_PROCESS_GM_SERVICE_NAME 		  : 'rabbit-processgm-server',
	RABBIT_PROCESS_GM_SERVICE_LOG_DIR 	: '/var/log/eckovation/rabbit/processGM',
	RABBIT_PROCESS_GM_SERVICE_USER    	: 'ubuntu',
	RECOMMENDATION_GROUPS_LENGTH_LIMIT  : 50,
	CALL_ME_GENERATION_DELAY            : 25*1000,     // 2 sec
	CALL_ME_GENERATION_EXPIRY           : 30*60*1000, // 30 minutes expressed in milliseconds
	CALL_ME_MAX_LIMIT_PER_DEVICE        : 5,
	OTP_CALL_ME_DELIVERY_ATTEMPTS_LIMIT : 5,
	OTP_CALL_ME_DELIVERY_REATTEMPT_DELAY: 10*1000,
	OTP_CALL_ME_REATTEMPT_DELAY         : 1*1000,
	ALLOWED_FAILED_VERIFY_OTP_ATTEMPTS  : 20,              // 20 invalid verify otp
	TIME_TO_PAUSE_VERIFY_OTP_ATTEMPTS   : 5*60*1000,     // 30 minutes
	VALID_CALL_TESTING_NUMBERS          : ['9591999098','9999894738','8341255449','9555670865','9953933422'],
	ALLOWED_REQUEST_OTP_ATTEMPTS        : 15,              // 5 allowed request otp limit
	TIME_TO_PAUSE_REQUEST_OTP_ATTEMPTS  : 3*60*1000,      // 30 minutes
	ALLOWED_GROUP_JOIN_ATTEMPTS         : 60,             // 30 allowed group join limits
	ALLOWED_FAILED_GROUP_JOIN_ATTEMPTS  : 25,              // 15 failed group join in 5 minutes 
	TIME_TO_PAUSE_GROUP_JOIN_ATTEMPTS   : 3*60*1000,     // 30 minites,
	TIME_TO_PAUSE_FAILED_GROUP_JOIN_ATTEMPTS : 3*60*1000,  // 5 minites,
	NODE_SERVER_HEAP_SIZE_MAX           : 2700,
	PROCESS_GM_WORKER_HEAP_SIZE_MAX     : 1900,
	USER_IDENTIFIER_TOKEN_VALIDITY_TIME : 10*12*30*24*60*60,     // 10 years
	USER_IDENTIFIER_HCODE_LENGTH        : 10,
	USER_IDENTIFIER_TOKEN_PRIVATE_KEY   : "8FJF884IUF0ICMU94UIRN439idfjsduf99erj757THNUYV9SUVNUqqppotr###",
	TARGET_VERSION_PLUGIN_ANDROID       : "4.7",
	TARGET_VERSION_PLUGIN_IOS           : "2.3",
	RABBIT_PUSH_NOTIFY_SERVICE_NAME 		: 'rabbit-pushnotify-server',
	RABBIT_PUSH_NOTIFY_SERVICE_LOG_DIR 	: '/var/log/eckovation/rabbit/pushnotify',
	RABBIT_PUSH_NOTIFY_SERVICE_USER    	: 'ubuntu',
	PUSH_NOTIFY_WORKER_HEAP_SIZE_MAX    : 990,
	NUM_PUSH_NOTIFY_WORKERS             : 3,
	RABBIT_MEDIA_SERVICE_NAME 		    	: 'rabbit-media-server',
	RABBIT_MEDIA_SERVICE_LOG_DIR 				: '/var/log/eckovation/rabbit/media',
	RABBIT_MEDIA_SERVICE_USER    				: 'ubuntu',
	MEDIA_WORKER_HEAP_SIZE_MAX          : 990,
	RABBIT_OTPCALL_SERVICE_NAME 		    : 'rabbit-otpcall-server',
	RABBIT_OTPCALL_SERVICE_LOG_DIR 			: '/var/log/eckovation/rabbit/otpcall',
	RABBIT_OTPCALL_SERVICE_USER    			: 'ubuntu',
	OTPCALL_WORKER_HEAP_SIZE_MAX        : 990,
	RABBIT_PAYMENT_SERVICE_NAME 		    : 'rabbit-payment-server',
	RABBIT_PAYMENT_SERVICE_LOG_DIR 	    : '/var/log/eckovation/rabbit/payment',
	RABBIT_PAYMENT_SERVICE_USER    	    : 'ubuntu',
	PAYMENT_WORKER_HEAP_SIZE_MAX        : 1000,
	USERRCQ_DROP_MAX_ATTEMPTS_ALLOWED   : 5,
	TIME_TO_PAUSE_USERQUEUE_TOKEN_ATTEMPTS : 1*60*1000,      // 1 day
	JWT_USERQUEUE_TOKEN_PRIVATE_KEY     : "eckovationECKOVATION654321123456ECKOVATIONeckovation",
	JWT_USERQUEUE_TOKEN_EXPIRY_TIME     : 1*60,
	NODE_SERVER_STACK_SIZE_MAX          : 200000,
	NODE_SERVER_HEAPDUMP_INTERVAL       : 2*60*60*1000,       // 2 Hours
	NOTIFY_WORKER_HEAPDUMP_INTERVAL     : 10*60*1000,         // 10 MINS
	GROUP_JOIN_NOTIFY_CONTROL_COUNT     : 100,
	RECOMMENDATION_DEPTH                : 20,
	ECKOVATION_GROUPJOIN_API_KEY        : "485H4UNW98W498545048NGU9958",
	DELETE_MESSAGE_ANDROID_VERSION      : '6.0',
	DELETE_MESSAGE_IOS_VERSION          : '3.5',
	PAID_GROUPJOIN_ANDROID_VERSION      : '6.0',
	CLIENTLOG_SUBMIT_QUOTA_LIMIT        : 50,
	FIRST_SERVER     : {
		SERVER_NUM     : 1,
		SERVICE_NAME   : 'eckovation-server-1',
		LOG_DIR        : '/var/log/eckovation',
		HEAP_SIZE_MAX  : 1550,
		STACK_SIZE_MAX : 300000,
		SERVICE_USER   : "ubuntu",
		PORT           : "3000"
	},
	SECOND_SERVER    : {
		SERVER_NUM     : 2,
		SERVICE_NAME   : 'eckovation-server-2',
		LOG_DIR        : '/var/log/eckovation',
		HEAP_SIZE_MAX  : 1552,
		STACK_SIZE_MAX : 300002,
		SERVICE_USER   : "ubuntu",
		PORT           : "3002"
	},
	THIRD_SERVER     : {
		SERVER_NUM     : 3,
		SERVICE_NAME   : 'eckovation-server-3',
		LOG_DIR        : '/var/log/eckovation',
		HEAP_SIZE_MAX  : 1553,
		STACK_SIZE_MAX : 300003,
		SERVICE_USER   : "ubuntu",
		PORT           : "3003"
	},
	FOURTH_SERVER    : {
		SERVER_NUM     : 4,
		SERVICE_NAME   : 'eckovation-server-4',
		LOG_DIR        : '/var/log/eckovation',
		HEAP_SIZE_MAX  : 1554,
		STACK_SIZE_MAX : 300004,
		SERVICE_USER   : "ubuntu",
		PORT           : "3004"
	},
	FIFTH_SERVER    : {
		SERVER_NUM     : 5,
		SERVICE_NAME   : 'eckovation-server-5',
		LOG_DIR        : '/var/log/eckovation',
		HEAP_SIZE_MAX  : 1555,
		STACK_SIZE_MAX : 300005,
		SERVICE_USER   : "ubuntu",
		PORT           : "3005"
	}
};


/* 
	1 . Configs keys can vary from environment to environment.
  2 . There will be a file named configs_overwrite_sample.js in the current directory only .
  		This is kind of sample configs overwrite keys file.
  3 . In your environment , first rename the configs_overwrite_sample.js to configs_overwrite.js.
  4 . Then in configs_overwrite.js file , set the keys value based on the environemnt.
  5 . The environemnt wise configs key values will be overwritten to the values you provided in the 
      configs_overwrite.js
  6 . However , all other keys apart form the the one's specified in the configs_overwrite.js will be 
  		default keys
*/
var overwriteConfigFulFileName  = __dirname+'/'+configs.CONFIGS_OVERWRITE_FILE;
if(fs.existsSync(overwriteConfigFulFileName)){
	var overwriteConfig  =  require(overwriteConfigFulFileName);
	for (var key in overwriteConfig){
		configs[key] = overwriteConfig[key];
	}
} else {
	console.log('[[[[[[[ No Overwrite Configs File Found to overwrite any config key ]]]]]]]]');
}

module.exports = configs;
