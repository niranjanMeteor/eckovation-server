var mongoose                = require('mongoose');
var configs                 = require('./configs.js');
var redis                   = require('redis');
var moment                  = require('moment');
var MsgIdGen                = require('./msg_id_generator.js');
var ProcessProfileMessage   = require('./process_prof_ntfy.js');
var Actions                 = require('./action_names.js');
var Events                  = require('./event_names.js');

var redis_client            = redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
  console.log("Error " + err);
});

var io                      = require('socket.io-emitter')(redis_client);

require('../models/profiles.js');
require('../models/messages.js');

var Message                 = mongoose.model('Message');
var Profile 		            = mongoose.model('Profile');
var GroupMember             = mongoose.model('GroupMember');

exports.abt_prof_c = function (pid, aid, pname, ppic, pchildnam, prole, cb) {
  var tim = moment()+'';
  var msg_id = MsgIdGen.generate(pid, pid, tim);
  var actionData = {
    t : Actions.PROFILE_CREATE,
    d : {
      pid   : pid,
      pnam  : pname,
      ppic  : ppic,
      cnam  : pchildnam,
      role  : prole,
      act   : true
    }
  };
  var notify_msg = pname+' created a new profile';
  var msg_data = {
    mid     : msg_id, 
    body    : notify_msg,
    type    : Message.MESSAGE_TYPE.invisible,
    pid     : pid,
    tim     : tim,
    ttyp    : Message.MESSAGE_RECIPIENT_TYPE.GROUP,
    pnam    : pname,
    actn    : actionData
  };
  ProcessProfileMessage.save_prof_c(msg_data,function(err,resp){
    if(err){ 
      return cb(err,null); 
    }
    cb(null,true);
    io.to(aid).emit(Events.PROFILE_CREATE,msg_data);
    return;
  });
};

exports.abt_pnam_e = function(pid, new_name, notify_msg, cb) {
	var tim = moment()+'';
	var	msg_id = MsgIdGen.generate(pid, pid, tim);
  var actionData = {
    t : Actions.PROFILE_NAME_EDIT,
    d : {
      pid   : pid,
      pnam  : new_name
    }
  };
	var msg_data = {
		mid			: msg_id, 
    body    : notify_msg,
    type    : Message.MESSAGE_TYPE.invisible,
    pid     : pid,
    tim     : tim,
    ttyp    : Message.MESSAGE_RECIPIENT_TYPE.GROUP,
    actn    : actionData
  };
  GroupMember.distinct('gid',{
  	pid : pid,
  	act : true,
    gdel: false
  },function(err,groups){
  	if(err){
      return cb(err,null); 
    }
  	if(!groups || groups.length == 0) {
      return cb("In abt_pnam_e : No Groups Found for pid",null); 
    }
  	GroupMember.distinct('pid',{
  		gid : { $in : groups },
  		act : true,
      gdel: false
  	},function(err,pids){
  		if(err){ 
        return cb(err,null); 
      }
  		if(!pids || pids.length == 0){
        return cb('In abt_pnam_e : No Profiles found for distinct groups',null); 
      }
      ProcessProfileMessage.save_pnam_e(msg_data, pids, function(err,resp){
        if(err){
          return cb(err,null); 
        }
        dataEmit = copyObject(msg_data);
        delete dataEmit['ttyp'];
        cb(null,true); 
        for(var i = 0; i < pids.length; i++) {
          io.to(pids[i]).emit(Events.PROFILE_NAME_EDIT, dataEmit);
        }
        io.to(pid).emit(Events.PROFILE_NAME_EDIT, dataEmit);
        return;
      });
  	}); 	
  });
};

exports.abt_ppic_e = function(pid, ppic, notify_msg, cb) {
	var tim = moment()+'';
	var	msg_id = MsgIdGen.generate(pid, pid, tim);
  var actionData = {
    t : Actions.PROFILE_PIC_EDIT,
    d : {
      pid   : pid,
      ppic  : ppic
    }
  };
	var msg_data = {
		mid			: msg_id, 
    body    : notify_msg,
    type    : Message.MESSAGE_TYPE.invisible,
    pid     : pid,
    tim     : tim,
    ttyp    : Message.MESSAGE_RECIPIENT_TYPE.GROUP,
    actn    : actionData
  };
  GroupMember.distinct('gid',{
  	pid : pid,
  	act : true,
    gdel: false
  },function(err,groups){
  	if(err){ 
      return cb(err,null); 
    }
  	if(!groups || groups.length == 0) {
      return cb("In abt_ppic_e : No Groups Found for pid",null); 
    }
  	GroupMember.distinct('pid',{
  		gid : { $in : groups },
  		act : true,
      gdel: false
  	},function(err,pids){
  		if(err){
        return cb(err,null); 
      }
  		if (!pids || pids.length == 0){
        return cb('In abt_ppic_e : No Profiles found for distinct groups',null); 
      }
  		ProcessProfileMessage.save_ppic_e(msg_data, pids, function(err,resp){
        if(err){
          return cb(err,null); 
        }
        dataEmit = copyObject(msg_data);
        delete dataEmit['ttyp'];
        cb(null,true);
    		for(var i = 0; i < pids.length; i++) {
  	  		io.to(pids[i]).emit(Events.PROFILE_PIC_EDIT, dataEmit);
  	  	}
  	  	io.to(pid).emit(Events.PROFILE_PIC_EDIT, dataEmit);
        return;
      });
  	}); 	
  });
};

exports.abt_ppic_r = function(pid, notify_msg, cb) {
	var tim = moment()+'';
	var	msg_id = MsgIdGen.generate(pid, pid, tim);
  var actionData = {
    t : Actions.PROFILE_PIC_REM,
    d : {
      pid   : pid,
    }
  };
	var msg_data = {
		mid			: msg_id, 
    body    : notify_msg,
    type    : Message.MESSAGE_TYPE.invisible,
    pid     : pid,
    tim     : tim,
    ttyp    : Message.MESSAGE_RECIPIENT_TYPE.GROUP,
    actn    : actionData
  };
  GroupMember.distinct('gid',{
  	pid : pid,
  	act : true,
    gdel: false
  },function(err,groups){
  	if(err){
      return cb(err,null); 
    }
  	if(!groups || groups.length == 0){
      return cb("In abt_ppic_r : No Groups Found for pid",null); 
    }
  	GroupMember.distinct('pid',{
  		gid : { $in : groups },
  		act : true,
      gdel: false
  	},function(err,pids){
  		if(err){
        return cb(err,null); 
      }
  		if(!pids || pids.length == 0){
        return cb('In abt_ppic_r : No Profiles found for distinct groups',null); 
      }
  		ProcessProfileMessage.save_ppic_r(msg_data, pids, function(err,resp){
        if(err){
          return cb(err,null); 
        }
        dataEmit =copyObject(msg_data);
        delete dataEmit['ttyp'];
        cb(null,true);
    		for(var i = 0; i < pids.length; i++) {
  	  		io.to(pids[i]).emit(Events.PROFILE_PIC_REM, dataEmit);
  	  	}
  	  	io.to(pid).emit(Events.PROFILE_PIC_REM, dataEmit);
        return;
      });
  	}); 	
  });
};


exports.abt_profile_r = function(pid, pnam, aid, cb) {
  var tim = moment()+'';
  var msg_id = MsgIdGen.generate(pid, pid, tim);
  var actionData = {
    t : Actions.PROFILE_DELETE,
    d : {
      pid   : pid,
    }
  };
  var notify_msg = 'Profile '+pnam+' deleted';
  var msg_data = {
    mid     : msg_id, 
    body    : notify_msg,
    type    : Message.MESSAGE_TYPE.invisible,
    pid     : pid,
    tim     : tim,
    pnam    : pnam,
    ttyp    : Message.MESSAGE_RECIPIENT_TYPE.GROUP,
    actn    : actionData
  };
  GroupMember.distinct('gid',{
    pid : pid,
    act : true,
    gdel: false
  },function(err,groups){
    if(err){
      return cb(err,null); 
    }
    if(!groups || groups.length == 0){ 
      return cb("In abt_profile_r : No Groups Found for pid",null); 
    }
    GroupMember.distinct('pid',{
      gid : { $in : groups },
      act : true,
      gdel: false
    },function(err,pids){
      if(err){
        return cb(err,null); 
      }
      if(!pids || pids.length == 0){ 
        return cb('In abt_profile_r : No Profiles found for distinct groups',null); 
      }
      ProcessProfileMessage.save_profile_r(msg_data, aid, pids, function(err,resp){
        if(err){
          return cb(err,null); 
        }
        cb(null,true);
        for(var i = 0; i < pids.length; i++) {
          io.to(pids[i]).emit(Events.PROFILE_DELETE, msg_data);
        }
        io.to(aid).emit(Events.PROFILE_DELETE, msg_data);
        return; 
      });
    });   
  });
};


function copyObject(obj) {
  if (null == obj || "object" != typeof obj) return obj;
  var copy = obj.constructor();
  for (var attr in obj) {
      if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
  }
  return copy;
}
