var mongoose            = require('mongoose');
var _                   = require('underscore');
var redis               = require('redis');

var configs             = require('./../utility/configs.js');

var redis_client        = redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
  console.log("Error " + err);
});

require('../models/messages.js');
var Message             = mongoose.model('Message');

var rcq                 = require("./../utility/redis_chat_queue.js")(redis_client);


exports.save_prof_c = function(data, cb) {
  var message = new Message({
    _id     : data.mid,
    body    : data.body,
    type    : data.type,
    from    : data.pid,
    tim     : data.tim,
    ttyp    : data.ttyp,
    actn    : data.actn
  });
  message.save(function(err,r){
    if(err){ 
      return cb(err,false); 
    }
    add_in_rcq(data.pid, data.tim, data.mid);
    return cb(null,true);
  });
};

exports.save_pnam_e = function(data, pids, cb) {
	var message = new Message({
    _id     : data.mid,
    body    : data.body,
    type    : data.type,
    from    : data.pid,
    tim     : data.tim,
    ttyp    : data.ttyp,
    actn    : data.actn
  });
  message.save(function(err,r){
    if(err){ 
      return cb(err,null); 
    }
    for (var j=0; j < pids.length; j++){
    	add_in_rcq(pids[j], data.tim, data.mid);
    }
    return cb(null,true);
  });
};

exports.save_ppic_e = function(data, pids, cb) {
	var message = new Message({
    _id     : data.mid,
    body    : data.body,
    type    : data.type,
    from    : data.pid,
    tim     : data.tim,
    ttyp    : data.ttyp,
    actn    : data.actn
  });
  message.save(function(err,r){
    if(err){ 
      return cb(err,null); 
    }
    for (var j=0; j < pids.length; j++){
    	add_in_rcq(pids[j], data.tim, data.mid);
    }
    return cb(null,true);
  });
};

exports.save_ppic_r = function(data, pids, cb) {
  var message = new Message({
    _id     : data.mid,
    body    : data.body,
    type    : data.type,
    from    : data.pid,
    tim     : data.tim,
    ttyp    : data.ttyp,
    actn    : data.actn
  });
  message.save(function(err,r){
    if(err){ 
      return cb(err,null); 
    }
    for (var j=0; j < pids.length; j++){
      add_in_rcq(pids[j], data.tim, data.mid);
    }
    return cb(null,true);
  });
};

exports.save_profile_r = function(msg_data, aid, pids, cb) {
  var message = new Message({
    _id     : msg_data.mid,
    body    : msg_data.body,
    type    : msg_data.type,
    from    : msg_data.pid,
    tim     : msg_data.tim,
    ttyp    : msg_data.ttyp,
    actn    : msg_data.actn
  });
  message.save(function(err,r){
    if(err){ 
      return cb(err,null); 
    }
    for (var j=0; j < pids.length; j++){
      add_in_rcq(pids[j], msg_data.tim, msg_data.mid);
    }
    add_in_rcq(aid, msg_data.tim, msg_data.mid);
    empty_profile_queues(msg_data.pid, msg_data.mid, function(err,resp){
      if(err){
        console.trace(err);
      }
      return cb(null,true);
    });
  });
};

function empty_profile_queues(profile_id, skipmid, cb) {
  rcq.getall(profile_id,function(err,profile_msgs){
    if(err){ 
      return cb(err,null);
    }
    for(var pm_i=0; pm_i<profile_msgs.length; pm_i++) {
      if(profile_msgs[pm_i] != skipmid)
        remove_in_rcq(profile_id, profile_msgs[pm_i]);
    }
    return cb(null,true);
  });
};

function remove_in_rcq(pid, mid) {
  rcq.remove(pid, mid, function(err,res){
    if(err) { console.log(err); return; }
    if(res == 1) {
        console.log("finally removed "+mid+" from "+pid+"'s message queue");
    } else {
        console.trace("wasn't able to remove "+mid+" from "+pid+"'s message queue");
    }
  });
}

function add_in_rcq(pid, tim, mid) {
  rcq.add(pid, tim, mid, function(err,res){
    if(err) {console.trace(err);}
    if(!res) {console.log("No response recieved from server.");}
    console.log("Queued "+mid+" message for "+pid+" profile at");
  });
}