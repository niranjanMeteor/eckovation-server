var mongoose            = require('mongoose');
var configs             = require('./configs.js');

require('../models/groups.js');
var Group 				      = mongoose.model('Group');

var code_length         = configs.G_CODE_LENGTH;
var gcode_gen_retry     = configs.GROUP_CODE_GENERATE_RETRY;

exports.getGroupCode = function(RetriesExecuted, cb){
	if(RetriesExecuted == gcode_gen_retry) { 
		console.log('Sorry , All Retries Attempts for Gcode generation failed to produce a non existing group code'); 
		return cb(true,null); 
	}
	var gcode;
	//var RetriesExecuted = 0;
	//var code_gen = false;
	// for(var j=0; j<gcode_gen_retry; j++){
	// 	gcode = generateGroupCode();
	// 	checkIfCodeExists(gcode,function(err,resp){
	// 		RetriesExecuted++;
	// 		if(!code_gen){
	// 			if(err) { 
	// 				console.trace(err); 
	// 				if(RetriesExecuted == gcode_gen_retry) { 
	// 					console.log('Sorry , All Retries for Gcode generation failed to produce a non existing group code'); 
	// 					return cb(true,null); 
	// 				}
	// 			} else {
	// 					if(resp.exists) { 
	// 						console.log('Group Code : '+resp.code+' ,already exists in database'); 
	// 						if(RetriesExecuted == gcode_gen_retry) { 
	// 							console.log('Sorry , All Retries for Gcode generation failed to produce a non existing group code'); 
	// 							return cb(true,null); 
	// 						}
	// 					}
	// 					if(!resp.exists) { code_gen = true; console.log('Group Code generation was successfull : '+resp.code); return cb(null,resp.code); }
	// 			}
	// 		}
	// 	});
	// }
	gcode = generateGroupCode();
	checkIfCodeExists(gcode,function(err,resp){
	 	RetriesExecuted++;
	 	if(err) {
	 		console.log('Error in Checking Group Code existence in Application for code : '+resp.code);
	 		exports.getGroupCode(RetriesExecuted,cb);
	 	} else if(resp.exists){
	 		console.log('Group Code : '+resp.code+' ,already exists in database');
	 		exports.getGroupCode(RetriesExecuted,cb);
	 	}	else if(!resp.exists){
	 		console.log('Group Code generation was successfull : '+resp.code);
	 		return cb(null,resp.code);
	 	}
	});
}

function checkIfCodeExists(gcode, cb) {
	Group.findOne({
		code : gcode
	},function(err,resp){
		if(err) { console.trace(err); return cb(true,null); }
		if(resp) {	console.log('group code : '+gcode+' was already exisiting in database'); return cb(null,{code:gcode, exists:true}); }
		console.log('Congratulations, group code : '+gcode+' is a brand new Group Code');
		return cb(null,{code:gcode, exists:false});
	});
}

function generateGroupCode(){
	var digits = [1,2,3,4,5,6,7,8,9];
	var group_code = [];
	for(var i=0; i < code_length; i++){
		group_code[i] = digits[Math.floor(Math.random() * (digits.length-1))];
		digits.splice(digits.indexOf(group_code[i]),1);
		if(i == 0) digits.push(0);
	}
	return group_code.join("");
}

//console.log('code1 is '+groupCode(6));