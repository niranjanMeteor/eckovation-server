var _                   = require('underscore');
var redis               = require('redis');
var mongoose            = require('mongoose');

var configs             = require('./configs.js');
var RabbitMqPublish     = require('../rabbitmq/publisher/publisher.js');
var RabbitMqQueues      = require('../rabbitmq/queues.js');
var RedisPrefixes       = require('./redis_prefix.js');

var redis_client        = redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
  console.log("Error " + err);
});

require('../models/messages.js');
require('../models/statuses.js');
var Message             = mongoose.model('Message');
var Status              = mongoose.model('Status');

var rcq                 = require("./redis_chat_queue.js")(redis_client);
var rhmset              = require("./redis_hmset.js")(redis_client);
var RABBIT_NOTIFY_QUEUE = RabbitMqQueues.PUSH_NOTIFY;


exports.save_grp_join = function(data, admins, cb) {
  var message = prepareMsgData(data);
  message.save(function(err,grp_join_msg){
    if(err){ 
      return cb(err,null); 
    }
    if(!grp_join_msg){ 
      return cb("Message Could not be saved in MongoDB",false);
    }
    var notify_msg = data.sns_msg;
    var sgm_obj;
    for(var j = 0; j < admins.length; j++) {
      sgm_obj = new Buffer(JSON.stringify({
        pid    : admins[j].pid,
        gid    : data.gid,
        msg    : notify_msg
      }));
      RabbitMqPublish.publish("",RABBIT_NOTIFY_QUEUE,sgm_obj);
      add_in_rcq(admins[j].pid, data.tim, data.mid);
      save_status(admins[j].pid, data.gid, data.mid, data.tim);
    }
    sgm_obj = new Buffer(JSON.stringify({
      pid    : data.actn.d.pid,
      gid    : data.gid,
      msg    : notify_msg
    }));
    RabbitMqPublish.publish("",RABBIT_NOTIFY_QUEUE,sgm_obj);
    add_in_rcq(data.actn.d.pid, data.tim, data.mid);
    cb(null,true);
    save_status(data.actn.d.pid, data.gid, data.mid, data.tim);
  });
};

exports.save_gadm_r = function(data, admins, cb) {
  var message = prepareMsgData(data);
  message.save(function(err,grp_adm_rem){
    if(err){ 
      return cb(err,null); 
    }
    if(!grp_adm_rem){ 
      return cb("Message Could not be saved in MongoDB",false);
    }
    var notify_msg = data.sns_msg;
    var sgm_obj;
    for(var j = 0; j < admins.length; j++) {
      sgm_obj = new Buffer(JSON.stringify({
        pid    : admins[j].pid,
        gid    : data.gid,
        msg   : notify_msg
      }));
      RabbitMqPublish.publish("",RABBIT_NOTIFY_QUEUE,sgm_obj);
      add_in_rcq(admins[j].pid, data.tim, data.mid);
      save_status(admins[j].pid, data.gid, data.mid, data.tim);
    }
    sgm_obj = new Buffer(JSON.stringify({
      pid    : data.actn.d.pid,
      gid    : data.gid,
      msg    : notify_msg
    }));
    RabbitMqPublish.publish("",RABBIT_NOTIFY_QUEUE,sgm_obj);
    add_in_rcq(data.actn.d.pid, data.tim, data.mid);
    cb(null,true);
    save_status(data.actn.d.pid, data.gid, data.mid, data.tim);
  });
};

exports.save_g_l = function(data, admins, cb) {
  var message = prepareMsgData(data);
  message.save(function(err,new_message){
    if(err){ 
      return cb(err,null); 
    }
    if(!new_message){ 
      return cb("Message Could not be saved in MongoDB",false);
    }
    var notify_msg = data.sns_msg;
    var sgm_obj;
    for(var j = 0; j < admins.length; j++) {
      sgm_obj = new Buffer(JSON.stringify({
        pid   : admins[j].pid,
        gid   : data.gid,
        msg   : notify_msg
      }));
      RabbitMqPublish.publish("",RABBIT_NOTIFY_QUEUE,sgm_obj);
      add_in_rcq(admins[j].pid, data.tim, data.mid);
      save_status(admins[j].pid, data.gid, data.mid, data.tim);
    }
    sgm_obj = new Buffer(JSON.stringify({
      pid   : data.pid,
      gid   : data.gid,
      msg   : notify_msg
    }));
    RabbitMqPublish.publish("",RABBIT_NOTIFY_QUEUE,sgm_obj);
    dropMidsFromPidQueue(data.pid, data.gid, function(err,resp){
      if(err){
        console.trace(err);
      }
      add_in_rcq(data.pid, data.tim, data.mid);
      save_status(data.pid, data.gid, data.mid, data.tim);
      return cb(null,true);
    });
  });
};

exports.save_gmem_ban = function(data, admins, cb) {
  var message = prepareMsgData(data);
  message.save(function(err,new_message){
    if(err){ 
      return cb(err,null); 
    }
    if(!new_message){ 
      return cb("Message Could not be saved in MongoDB",false);
    }
    var notify_msg = data.sns_msg;
    var sgm_obj;
    for(var j = 0; j < admins.length; j++) {
      sgm_obj = new Buffer(JSON.stringify({
        pid    : admins[j].pid,
        gid    : data.gid,
        msg   : notify_msg
      }));
      RabbitMqPublish.publish("",RABBIT_NOTIFY_QUEUE,sgm_obj);
      add_in_rcq(admins[j].pid, data.tim, data.mid);
      save_status(admins[j].pid, data.gid, data.mid, data.tim);
    }
    sgm_obj = new Buffer(JSON.stringify({
      pid    : data.pid,
      gid    : data.gid,
      msg   : notify_msg
    }));
    RabbitMqPublish.publish("",RABBIT_NOTIFY_QUEUE,sgm_obj);
    add_in_rcq(data.pid, data.tim, data.mid);
    cb(null,true);
    save_status(data.pid, data.gid, data.mid, data.tim);
  });
};

exports.save_gmem_ban_other_pids = function(data, cb) {
  var message = prepareMsgData(data);
  message.save(function(err,saved_msg){
    if(err){ 
      return cb(err,false); 
    }
    if(!saved_msg) { 
      return cb("Msg Could Not Be Saved With MID : "+data.mid,null);
    }
    add_in_rcq(data.actn.d.pid, data.tim, data.mid);
    save_status(data.actn.d.pid, data.gid, data.mid, data.tim);
    cb(null,true);
  });
};

exports.save_gmem_unban = function(data, admins, cb) {
  var message = prepareMsgData(data);
  message.save(function(err,new_message){
    if(err){ 
      return cb(err,null); 
    }
    if(!new_message){ 
      return cb("Message Could not be saved in MongoDB",false);
    }
    var notify_msg = data.sns_msg;
    var sgm_obj;
    for(var j = 0; j < admins.length; j++) {
      sgm_obj = new Buffer(JSON.stringify({
        pid    : admins[j].pid,
        gid    : data.gid,
        msg   : notify_msg
      }));
      RabbitMqPublish.publish("",RABBIT_NOTIFY_QUEUE,sgm_obj);
      add_in_rcq(admins[j].pid, data.tim, data.mid);
      save_status(admins[j].pid, data.gid, data.mid, data.tim);
    }
    sgm_obj = new Buffer(JSON.stringify({
      pid    : data.pid,
      gid    : data.gid,
      msg   : notify_msg
    }));
    RabbitMqPublish.publish("",RABBIT_NOTIFY_QUEUE,sgm_obj);
    add_in_rcq(data.actn.d.pid, data.tim, data.mid);
    cb(null,true);
    save_status(data.pid, data.gid, data.mid, data.tim);
  });
};

exports.save_gmem_del = function(data, admins, cb) {
  var message = prepareMsgData(data);
  message.save(function(err,new_message){
    if(err){ 
      return cb(err,null); 
    }
    if(!new_message){ 
      return cb("Message Could not be saved in MongoDB",false);
    }
    var notify_msg = data.sns_msg;
    var sgm_obj;
    for(var j = 0; j < admins.length; j++) {
      sgm_obj = new Buffer(JSON.stringify({
        pid    : admins[j].pid,
        gid    : data.gid,
        msg    : notify_msg
      }));
      RabbitMqPublish.publish("",RABBIT_NOTIFY_QUEUE,sgm_obj);
      add_in_rcq(admins[j].pid, data.tim, data.mid);
      save_status(admins[j].pid, data.gid, data.mid, data.tim);
    }
    sgm_obj = new Buffer(JSON.stringify({
      pid    : data.pid,
      gid    : data.gid,
      msg   : notify_msg
    }));
    RabbitMqPublish.publish("",RABBIT_NOTIFY_QUEUE,sgm_obj);
    dropMidsFromPidQueue(data.actn.d.pid, data.gid, function(err,resp){
      if(err){
        console.trace(err);
      }
      add_in_rcq(data.actn.d.pid, data.tim, data.mid);
      save_status(data.pid, data.gid, data.mid, data.tim);
      return cb(null,true);
    });
  });
};

exports.save_grp_d = function(data, pid, cb) {
  var message = prepareMsgData(data);
  message.save(function(err,saved_msg){
    if(err){ 
      return cb(err,false); 
    }
    if(!saved_msg){ 
      return cb("Message Saving Failed",false);
    }
    var sgm_obj= new Buffer(JSON.stringify({
      pid  : pid,
      gid  : data.gid,
      msg  : data.sns_msg
    }));
    RabbitMqPublish.publish("",RABBIT_NOTIFY_QUEUE,sgm_obj);
    add_in_rcq(pid, data.tim, data.mid);
    save_status(pid, data.gid, data.mid, data.tim);
    return cb(null,true);
  });
};

exports.save_msg_del = function(pids, total, data, cb){
  var message = prepareMsgData(data);
  message.save(function(err,saved_msg){
    if(err){ 
      return cb(err,false); 
    }
    if(!saved_msg){ 
      return cb("Message Saving Failed",false);
    }
    for(var j = 0; j < total; j++) {
      add_in_rcq(pids[j].pid, data.tim, data.mid);
      save_status(pids[j].pid, data.gid, data.mid, data.tim);
    }
    return cb(null,true);
  });
};

exports.rm_grp_msg_from_prof_rcq = function(pid, gid, cb){
  rcq.getall(pid,function(err,profile_msgs){
    if(err) { console.trace(err); return cb(true,false); }
    if(profile_msgs.length == 0) { 
      console.log('No previous messages found for the group : '+gid+' , in the profile queue of pid : '+pid); 
      return cb(null,true);
    }
    var mids = [];
    for(var pm_i=0; pm_i<profile_msgs.length; pm_i++) {
      mids.push(profile_msgs[pm_i]);
    }
    var target_action_types = [Message.ACTION_TYPE.GROUP_JOIN,Message.ACTION_TYPE.GROUP_LEAVE];
    var target_msg_types = [Message.MESSAGE_TYPE.notification,Message.MESSAGE_TYPE.admin];
    Message.find({
      _id : { $in : mids },
      to  : gid,
      type : { $in : target_msg_types},
      'actn.t' : { $in : target_action_types},
      from : pid
    },{
      _id : 1,
    },function(err,messages){
      if(err) { console.trace(err); return cb(true,false); }
      if(messages.length  == 0){
        console.log('No Message found in the profile queue for pid : '+pid+', for group id : '+gid);
        return cb(null,true);
      }
      cb(null,true);
      var num_msgs = messages.length;
      for(var j=0; j<num_msgs; j++){
        remove_in_rcq(pid, messages[j]._id);
      }
    });
  });
}

function dropMidsFromPidQueue(pid, gid, cb){
  rcq.getall(pid,function(err,grp_msgs){
    if(err){ 
      console.trace(err); 
      return cb(true,false); 
    }
    if(grp_msgs.length == 0) { 
      console.log('No previous messages found for the group : '+gid+' , in the profile queue of pid : '+pid); 
      return cb(null,true);
    }
    var mids = [];
    for(var i=0; i<grp_msgs.length; i++) {
      mids.push(grp_msgs[i]);
    }
    grp_msgs = null;
    Message.find({
      _id : { $in : mids },
      to  : gid
    },{
      _id : 1,
    },function(err,messages){
      if(err){ 
        return cb(err,false); 
      }
      if(messages.length  == 0){
        console.log('No Message found in the profile queue for pid : '+pid+', for group id : '+gid);
        return cb(null,true);
      }
      var done     = 0;
      var num_msgs = messages.length;
      var toDo     = 2*num_msgs;
      for(var j=0; j<num_msgs; j++){
        remove_from_rcq(pid, messages[j]._id, function(err,resp){
          if(err){
            console.trace(err);
          }
          done++;
          if(done == toDo){
            return cb(null,true);
          }
        });
        remove_from_adseenQ(pid, messages[j]._id, function(err,resp){
          if(err){
            console.trace(err);
          }
          done++;
          if(done == toDo){
            return cb(null,true);
          }
        });
      }
      messages = null;
    });
  });
}

function prepareMsgData(data) {
  var message = new Message({
    _id     : data.mid,
    body    : data.body,
    type    : data.type,
    from    : data.pid,
    pnam    : data.pnam,
    to      : data.gid,
    spcf    : data.spcf,
    tim     : data.tim,
    ttyp    : data.ttyp,
    actn    : data.actn,
    ignr    : data.ignr
  });
  return message;
}

function add_in_rcq(pid, tim, mid) {
  rcq.add(pid, tim, mid, function(err,res){
    if(err){
      console.trace(err); return; 
    }
    if(!res){
      console.trace("Could not Queue "+mid+" message for "+pid+" profile at " +tim);
    }
    return;
  });
}

function save_status(pid, gid, mid, tim) {
  var new_status = new Status({
    pid : pid,
    mid : mid,
    gid : gid,
    stts: Status.MESSAGE_STATUS.SERVER_RECEIVED,
    tim : tim
  });
  new_status.save(function(err, new_status) {
    if(err){ 
      console.trace(err); 
      return; 
    }
    if(!new_status) { 
      console.trace("Failed to save Message Status, i.e. SERVER_RECEIVED for "+mid); 
      return; 
    }
  });
};

function remove_in_rcq(pid, mid) {
  rcq.remove(pid, mid, function(err,res){
    if(err) { console.log(err); return; }
    if(res == 1) {
        console.log("finally removed "+mid+" from "+pid+"'s message queue");
    } else {
        console.trace("wasn't able to remove "+mid+" from "+pid+"'s message queue");
    }
    return;
  });
}

function remove_from_rcq(pid, mid, cb) {
  rcq.remove(pid, mid, function(err,res){
    if(err){ 
      return cb(err,null); 
    }
    return cb(null,true);
  });
}

function remove_from_adseenQ(pid, mid, cb) {
  rhmset.removefield(RedisPrefixes.SEEN_COUNT+pid, mid, function(err,res){
    if(err){ 
      return cb(err,null); 
    }
    return cb(null,true);
  });
}