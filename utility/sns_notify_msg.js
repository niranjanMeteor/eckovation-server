var mongoose            = require('mongoose');

require('../models/messages.js');
var Message             = mongoose.model('Message');


exports.prepNotifyMsg = function(data, gname) {
  var msg = '';
  switch(parseInt(data.type)){
    case Message.MESSAGE_TYPE.text:
        msg = data.pnam+' @ '+gname+'\n'+data.body;
    break;

    case Message.MESSAGE_TYPE.audio:
        msg = data.pnam+' @ '+gname+' : sent an Video';
    break;

    case Message.MESSAGE_TYPE.image:
        msg = data.pnam+' @ '+gname+' : sent an Image';
    break;

    case Message.MESSAGE_TYPE.video:
        msg = data.pnam+' @ '+gname+' : sent an Audio';
    break;
  }
  return msg;
}