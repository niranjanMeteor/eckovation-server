var ConfigsOverwrite = {
	DB_NAME 														: "Set the database name to the database server database based on your environment",
	MONGO_HOST 													: "set it to the ip address of the mongo database server",
	MONGO_UNAME													: "set it to database server user name to connect to the database server database",
	MONGO_PASS													: "Set the password for the mongodb username for database server",
	REDIS_PASS													: "Set it to Redis password for authenticating to redis server",
	REDIS_HOST													: "Set the redis server ip address", 
	REDIS_PORT													: "Set the port on which redis is running on redis server", 
	// you can add new keys which you wanna overwrite in the application configuration file 
};

module.exports = ConfigsOverwrite;