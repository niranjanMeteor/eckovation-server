var _               = require('underscore');
var rhmsetConfigs 	= require("./rhmset_configs.js");
var RedisPrefixes   = require('./redis_prefix.js');

var custom_redis = function(client) {
	var redis_client                  = client;
	var default_cache_time 	          = rhmsetConfigs.DEFAULT_CACHE_TIME;
	var account_prof_gms_cache_time 	= rhmsetConfigs.DEFAULT_ACCOUNT_PROF_GMS_CACHE_TIME;
	var group_admins_cache_time       = rhmsetConfigs.DEFAULT_GROUP_ADMINS_CACHE_TIME;
	var socket_token_cache_time 	    = rhmsetConfigs.SOCKET_TOKEN_CACHE_TIME;
  var group_accountdata_cache_time  = rhmsetConfigs.GROUP_ACCOUNTDATA_CACHE_TIME;
  var userrcq_drop_cache_time       = rhmsetConfigs.USERRCQ_DROP_CACHE_TIME;

	return {

		//O(log(1))
		exists : function(key, cb) {
			redis_client.exists(key,function(err,res){
				if(err) {return cb(new Error(err)); }
				return cb(null,res);
			});
		},

		//O(log(1))
		keyexists : function(key, field, cb) {
			redis_client.hexists(key, field, function(err,res){
				if(err) {return cb(new Error(err));}
				return cb(null,res);
			});
		},
		//O(log(1))
		set : function(prefix, key, field, value, cb) {
			redis_client.hmset(prefix+key, field, value, function(err,res){
				if(err) {return cb(new Error(err));}
				if(prefix == RedisPrefixes.SEEN) {
					redis_client.expire(prefix+key, default_cache_time);
				} else if(prefix == RedisPrefixes.GROUP_INFO){
					redis_client.expire(prefix+key, account_prof_gms_cache_time);
				} else if(prefix == RedisPrefixes.USER_PROF_GMS){
					redis_client.expire(prefix+key, group_admins_cache_time);
				}
				return cb(null,res);
			});
		},
		m4set : function(prefix, key, field1, value1, field2, value2, field3, value3, field4, value4, cb){
			redis_client.hmset(prefix+key, field1, value1, field2, value2, field3, value3, field4, value4, function(err,res){
				if(err) {return cb(new Error(err));}
				if(prefix == RedisPrefixes.GROUP_ACCOUNTDATA){
					redis_client.expire(prefix+key, group_accountdata_cache_time);
				}
				return cb(null,res);
			});
		},
		m2set : function(prefix, key, field1, value1, field2, value2, cb){
			redis_client.hmset(prefix+key, field1, value1, field2, value2, function(err,res){
				if(err) {return cb(new Error(err));}
				return cb(null,res);
			});
		},
		updateField : function(key, field, value, cb) {
			redis_client.hmset(key, field, value, function(err,res){
				if(err) {return cb(new Error(err));}
				return cb(null,res);
			});
		},
		setMongoObj : function(key, field, value, cb) {
			redis_client.hmset(key, field, value, function(err,res){
				if(err) {return cb(new Error(err));}
				return cb(null,res);
			});
		},
		setSeenJob : function(prefix, key, mid, gid, pid, stim, tim, cb) {
			redis_client.hmset(prefix+key,"mid", mid, "gid", gid, "pid", pid, "stim", stim, "tim", tim, function(err,res){
				if(err) {return cb(new Error(err));}
				return cb(null,res);
			});
		},
		setStatusInsertJob : function(prefix, key, mid, gid, pid, data_pid, tim, cb) {
			redis_client.hmset(prefix+key, "mid", mid, "gid", gid, "pid", pid, "data_pid", data_pid, "tim", tim, function(err,res){
				if(err) {return cb(new Error(err));}
				return cb(null,res);
			});
		},
		setMediaJob : function(prefix, mid, tim, cb) {
			redis_client.hmset(prefix+mid, "tim", tim, function(err,res){
				if(err) {return cb(new Error(err));}
				return cb(null,res);
			});
		},
		setUnsecuredAccount : function(key, field, value, cb) {
			redis_client.hmset(key, field, value, function(err,res){
				if(err) {return cb(new Error(err));}
				return cb(null,res);
			});
		},
		updtBruteForce : function(key, field1, value1, field2, value2, cb) {
			redis_client.hmset(key, field1, value1, field2, value2, function(err,res){
				if(err) {return cb(new Error(err));}
				redis_client.expire(key, rhmsetConfigs.BRUTE_FORCE_ACCOUNT_CACHE_TIME);
				return cb(null,res);
			});
		},
		updtBruteForceAndTim : function(key, field1, value1, field2, value2, field3, value3, cb) {
			redis_client.hmset(key, field1, value1, field2, value2, field3, value3, function(err,res){
				if(err) {return cb(new Error(err));}
				redis_client.expire(key, rhmsetConfigs.BRUTE_FORCE_ACCOUNT_CACHE_TIME);
				return cb(null,res);
			});
		},
		updtOtpAtmpt : function(key, field1, value1, field2, value2, cb) {
			redis_client.hmset(key, field1, value1, field2, value2, function(err,res){
				if(err) {return cb(new Error(err));}
				redis_client.expire(key, rhmsetConfigs.BRUTE_FORCE_PHONE_OTP_CACHE_TIME);
				return cb(null,res);
			});
		},
		updtOtpAtmptTim : function(key, field1, value1, field2, value2, field3, value3, cb) {
			redis_client.hmset(key, field1, value1, field2, value2, field3, value3, function(err,res){
				if(err) {return cb(new Error(err));}
				redis_client.expire(key, rhmsetConfigs.BRUTE_FORCE_PHONE_OTP_CACHE_TIME);
				return cb(null,res);
			});
		},
		resetOtpAtmpt : function(key, field, value, cb) {
			redis_client.hmset(key, field, value, function(err,res){
				if(err) {return cb(new Error(err));}
				return cb(null,res);
			});
		},
		updtGrpJnAtmpt : function(key, field1, value1, field2, value2, cb) {
			redis_client.hmset(key, field1, value1, field2, value2, function(err,res){
				if(err) {return cb(new Error(err));}
				redis_client.expire(key, rhmsetConfigs.BRUTE_FORCE_GROUP_JOIN_CACHE_TIME);
				return cb(null,res);
			});
		},
		updtGrpJnAtmptTim : function(key, field1, value1, field2, value2, field3, value3, cb) {
			redis_client.hmset(key, field1, value1, field2, value2, field3, value3, function(err,res){
				if(err) {return cb(new Error(err));}
				redis_client.expire(key, rhmsetConfigs.BRUTE_FORCE_GROUP_JOIN_CACHE_TIME);
				return cb(null,res);
			});
		},
		updtUsrQAtmpt : function(key, field1, value1, cb) {
			redis_client.hmset(key, field1, value1, function(err,res){
				if(err) {return cb(new Error(err));}
				redis_client.expire(key, userrcq_drop_cache_time);
				return cb(null,res);
			});
		},
		setSocketToken : function(key, field, value, cb){
			redis_client.hmset(key, field, value, function(err,res){
				if(err) {return cb(new Error(err));}
				redis_client.expire(key, socket_token_cache_time);
				return cb(null,res);
			});
		},
		get : function(key, field, cb) {
			redis_client.hget(key, field, function(err,res){
				if(err) {return cb(new Error(err));}
				return cb(null,res);
			});
		},
		m4get : function(key, field1, field2, field3, field4, cb) {
			redis_client.hmget(key, field1, field2, field3, field4, function(err,res){
				if(err) {return cb(new Error(err));}
				return cb(null,res);
			});
		},
		m2get : function(key, field1, field2, cb) {
			redis_client.hmget(key, field1, field2, function(err,res){
				if(err) {return cb(new Error(err));}
				return cb(null,res);
			});
		},
		mget : function(key, field, cb) {
			redis_client.hmget(key, field, function(err,res){
				if(err) {return cb(new Error(err));}
				return cb(null,res);
			});
		},
		len : function(key, cb) {
			redis_client.hlen(key, function(err,res){
				if(err) {return cb(new Error(err));}
				return cb(null,res);
			});
		},

		//O(log(N))
		getall : function(key, cb) {
			redis_client.hgetall(key, function(err,res){
				if(err) {return cb(new Error(err));}
				return cb(null,res);
			});
		},

		//O(log(N))
		getvals : function(key, cb) {
			redis_client.hvals(key, function(err,res){
				if(err) {return cb(new Error(err));}
				return cb(null,res);
			});
		},

		getOtpAtmpt : function(key, cb) {
			redis_client.hmget(key, "tlmt", "ltm", "tm", function(err,res){
				if(err) {return cb(new Error(err));}
				return cb(null,res);
			});
		},

		//O(log(N))
		incrby : function(key, field, value, cb) {
			redis_client.hincrby(key, field, value, function(err,res){
				if(err) {return cb(new Error(err));}
				return cb(null,res);
			});
		},

		//O(log(N))
		removefield : function(key, field, cb) {
			redis_client.hdel(key, field, function(err,res){
				if(err) {return cb(new Error(err));}
				return cb(null,res);
			});
		},

		//O(log(N))
		removekey : function(key, cb) {
			redis_client.del(key, function(err,res){
				if(err) {return cb(new Error(err));}
				return cb(null,res);
			});
		}

	};
};

module.exports = custom_redis;