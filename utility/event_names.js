var events = {
	GROUP_MESSAGE     : "g_m",
	GROUP_MESSAGE_W   : "g_m_w",
	GROUP_NAME_EDIT   : "gnam_e",
	GROUP_PIC_EDIT    : "gpic_e",
	GROUP_PIC_REM     : "gpic_r",
	GROUP_ADMIN_ADD   : "gadm_a",
	GROUP_ADMIN_REM   : "gadm_r",
	GROUP_LEAVE       : "g_l",
	PROFILE_NAME_EDIT : "pnam_e",
	PROFILE_PIC_EDIT  : "ppic_e",
	PROFILE_PIC_REM   : "ppic_r",
	PROFILE_DELETE		: "prof_r",
	PROFILE_CREATE    : "prof_c",
	PLUGIN            : "plg",
	EXTRA             : "extra",
	EXTRA_W           : "extra_w"
};

module.exports = events;