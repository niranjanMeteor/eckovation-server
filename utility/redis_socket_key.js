var rsk = function(client,PREFIX) {
	var redis_client 	= client;

	return {
		//O(1)
		add : function(add_key, cb){
			redis_client.set(PREFIX+":"+add_key, 1,function(err, res) {
				if(err) {return cb(new Error(err));}
				
				return cb(null,res);
			})
		},
		//O(n)
		search : function(search_key,cb) {
			//ToDo :: replace with scan
			redis_client.keys(PREFIX+":"+search_key, function(err, res) {
				if(err) {return cb(new Error(err));}
				
				return cb(null,res);
			});
		},
		//O(1)
		remove : function(delete_key, cb) {
			redis_client.del(PREFIX+":"+delete_key, function(err, res) {
				if(err) {return cb(new Error(err));}
				
				return cb(null,res);
			});
		}
	};
};

module.exports = rsk;