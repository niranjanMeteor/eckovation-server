var _                 	= require('underscore');
var mongoose            = require('mongoose');
var configs 			      = require("../utility/rmc_configs.js");
var pluralize 			    = require('pluralize');

var rmc = function(client) {
	var redis_client 		    = client;
	var short_names 		    = configs.MODEL_SHORT_NAMES;
	var cache_times 		    = configs.MODEL_CACHE_TIME;
	var default_cache_time 	= configs.DEFAULT_CACHE_TIME;

	for(var model_name in short_names) {
		var plural = pluralize.plural(model_name.toLowerCase());
		require('../models/' + plural + '.js');
	}

	return {
		save : function(model_name,data) {

		},
		queryById : function(model_name,id,cb) {
			if(short_names[model_name]===undefined) {
				return cb(new Error("model's short name not defined"));
			}
			var hash_key = short_names[model_name]+":"+id;
			redis_client.hgetall(hash_key,function(err,obj){
				if(err) { 
					return cb(new Error("error in redis fetching")); 
				}
				var model = mongoose.model(model_name);
				if(obj) {
					obj = model.cast(obj);
					if((model_name == "Message") && ("actn" in obj)) {
						obj.actn = JSON.parse(obj.actn);
					}
					return cb(null,obj);
				} else {
					var condition = {
						_id : id
					};
					if(model_name == "Account"){
						condition.vrfy = true;
					}
					model.findOne(condition,function(err,mongo_obj){
						if(err) {
							return cb(new Error("error in mongo fetching"));
						}
						if(!mongo_obj) { 
							return cb(null,null); 
						}
						mongo_obj = mongo_obj.toObject();
						if(mongo_obj["cat"]) {
							mongo_obj["cat"] = mongo_obj["cat_ms"];
							delete mongo_obj["cat_ms"];
						}
						//TODO : remove model_name checking conditon which is happening at too many places
						modified_mongo_obj = copyObject(mongo_obj);
						if((model_name == "Message") && ("actn" in mongo_obj)) {
							modified_mongo_obj.actn = JSON.stringify(modified_mongo_obj.actn);
						}
						redis_client.hmset(hash_key,modified_mongo_obj,function(err){
							if(err) { 
								return cb(new Error(err)); 
							}
							var ttl = (cache_times[model_name]) ? cache_times[model_name] : default_cache_time;
							redis_client.expire(hash_key,ttl);
							return cb(null,mongo_obj);
						});
					});
				}
			});
		},
		remove : function(model_name, id, cb){
			var hash_key = short_names[model_name]+":"+id;
			redis_client.exists(hash_key,function(err,isKey){
        if(err){
          return cb(err,null);
        }
        if(isKey){
          redis_client.del(hash_key,function(err,resp){
            if(err){
              return cb(err,null);
            } 
            if(resp){
              return cb(null,true);
            } else {
              console.log('Oops,this is unfortunate that '+model_name+' Object Does not exists for ID : '+id);
              return cb(null,false);
            }
          });
        } else {
          console.log('Oops,this is unfortunate that '+model_name+' Object Does not exists for ID : '+id);
          return cb(null,false);
        }
    	});
		}
	};	
};

function copyObject(obj) {
  if (null == obj || "object" != typeof obj) return obj;
  var copy = obj.constructor();
  for (var attr in obj) {
      if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
  }
  return copy;
}

module.exports = rmc;