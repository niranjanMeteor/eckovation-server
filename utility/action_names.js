var actions = {
	GROUP_NAME_EDIT 	: 1,//"gnam_e",
	GROUP_PIC_EDIT  	: 2,//"gpic_e",
	GROUP_PIC_REM   	: 3,//"gpic_r",
	GROUP_ADMIN_ADD 	: 4,//"gadm_a",
	GROUP_ADMIN_REM 	: 5,//"gadm_r",
	GROUP_LEAVE     	: 6,//"g_l",
	PROFILE_NAME_EDIT : 7,//"pnam_e",
	PROFILE_PIC_EDIT  : 8,//"ppic_e",
	PROFILE_PIC_REM   : 9,//"ppic_r"
	PROFILE_DELETE		: 10,//profile_r
	GROUP_JOIN        : 11,//
	GROUP_CREATE      : 12,
	GROUP_MEMBER_BAN  : 13,
	GROUP_MEMBER_UNBAN: 14,
	GROUP_MEMBER_DEL  : 15,
	GROUP_DELETE      : 16,
	PROFILE_CREATE    : 17,
	GROUP_TYPE_EDIT   : 18,
	DELETE_MESSAGE    : 19
};

module.exports = actions;