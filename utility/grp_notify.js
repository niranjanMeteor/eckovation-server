var mongoose            		= require('mongoose');
var configs									= require('./configs.js');
var ProcessGroupMessage			= require('./../chat/process_gm.js');
var redis       						= require('redis');
var moment      						= require('moment');
var MsgIdGen                = require('./msg_id_generator.js');
var ProcessSpcfGroupMessage	= require('./process_grp_ntfy.js');
var Actions 								= require('./action_names.js');
var Events 									= require('./event_names.js');
var RedisPrefixes           = require('./redis_prefix.js');
var RabbitMqPublish     		= require('../rabbitmq/publisher/publisher.js');
var RabbitMqQueues      		= require('../rabbitmq/queues.js');
var log4jsLogger  					= require('../loggers/log4js_module.js');
var log4jsConfigs  			    = require('../loggers/log4js_configs.js')
var UserInfoCache       	  = require('./user_info_cache.js');

var redis_client   					= redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
    console.log("Error " + err);
});
var io 											= require('socket.io-emitter')(redis_client);
var rhmset                  = require("./redis_hmset.js")(redis_client);
var rcq                     = require("./redis_chat_queue.js")(redis_client);

require('../models/profiles.js');
require('../models/messages.js');
require('../models/groupmembers.js');
require('../models/groups.js');

var Message 								= mongoose.model('Message');
var Profile 								= mongoose.model('Profile');
var GroupMember							= mongoose.model('GroupMember');
var Group						        = mongoose.model('Group');
var logger        		      = log4jsLogger.getLogger(log4jsConfigs.CATEGORY_UTILITY_GROUP_NOTIFY);

exports.grp_notify = function (gid, pid, notify_msg) {
	var tim = moment()+'';
	Profile.findOne({
		_id : pid,
		act : true
	},function(err,profile){
		if(err) { console.trace(err); }
		if(!profile) { console.log("Profile does not exists : no_profile_found"); return;	}
		var msg_id = MsgIdGen.generate(pid, gid, tim);
		var msg_data = {
			mid			: msg_id, 
      body    : notify_msg,
      type    : Message.MESSAGE_TYPE.notification,
      pid     : pid,
      gid     : gid,
      tim     : tim,
      ttyp    : Message.MESSAGE_RECIPIENT_TYPE.GROUP,
      pnam		: profile.nam
	  };
	  ProcessGroupMessage.mobile(msg_data,function(error,resp){
	  	if(error){ 
	  		console.trace(error); 
	  		return logger.error({"FUNCTION":"grp_notify","packet":{msg:msg_data}});
	  	}
	  	logger.info({"FUNCTION":"grp_notify","packet":{msg:msg_data}});
		  var room = gid;
		  io.to(room).emit(Events.GROUP_MESSAGE,msg_data);
		  io.to(room).emit(Events.GROUP_MESSAGE_W,msg_data);
		  return ;
	  });
	});
};

exports.abt_msg_d = function (gid, pid, pnam, mid, nbody) {
	var tim    = new Date().getTime();
  var msg_id = MsgIdGen.generate(pid, gid, tim);
  var actionData = {
    t : Actions.DELETE_MESSAGE,
    d : {
      mid   : mid,
      nbody : nbody,
      ntype : Message.MESSAGE_TYPE.text
    }
  };
  var notify_msg = 'Message_Deleter_For_Mid_'+mid;
  var msg_data = {
    mid     : msg_id, 
    body    : notify_msg,
    type    : Message.MESSAGE_TYPE.invisible,
    gid     : gid,
    pid     : pid,
    tim     : tim,
    pnam    : pnam,
    ttyp    : Message.MESSAGE_RECIPIENT_TYPE.GROUP,
    actn    : actionData,
    ignr    : true
  };
  GroupMember.find({
  	gid : gid,
  	act : true,
  	gdel: false,
  	type: {$ne : GroupMember.TYPE.BANNED}
  },{
  	pid : 1,
  	_id : 0
  },function(err,groupmembers){
  	if(err){
  		logger.error({"FUNCTION":"abt_msg_d","err":err,"d":{pid:pid, gid:gid, mid:mid}}); 
			return;
  	}
  	if(groupmembers.length == 0){
  		logger.error({"FUNCTION":"abt_msg_d","msg":"no unique account ids found to send delete message notification","d":{pid:pid, gid:gid, mid:mid}}); 
			return;
  	}
  	var total = groupmembers.length;
  	ProcessSpcfGroupMessage.save_msg_del(groupmembers, total, msg_data, function(err,resp){
  		if(err){
  			logger.error({"FUNCTION":"abt_msg_d","err":err,"d":{pid:pid, gid:gid, mid:mid},"msg":"ProcessSpcfGroupMessage_save_msg_del"}); 
				return;
  		}
	  	for(var i = 0; i < total; i++) {
	  		io.to(groupmembers[i].pid).emit(Events.EXTRA, msg_data);
	  		io.to(groupmembers[i].pid).emit(Events.EXTRA_W,msg_data);
	  	}
  		logger.info({"FUNCTION":"abt_msg_d","packet":{msg:msg_data,actn:JSON.stringify(actionData)}});
	  	return;
  	});
  });
};

exports.abt_grp_c = function (gid, pid, gname, gcode, gpic, gtyp, mtype, tim) {
	UserInfoCache.getMongoProfile(pid,function(err,profile){
		if(err){ 
			console.trace(err); 
			return ;
		}
		if(!profile){ 
			return ;
		}
		var msg_id = MsgIdGen.generate(pid, gid, tim);
		var actionData = {
			t : Actions.GROUP_CREATE,
			d : {
				gid 	: gid,
				pid   : pid,
				gnam  : gname,
				gcod  : gcode,
				gpic  : gpic,
				act   : true,
				gtyp  : gtyp,
				mtyp  : mtype
			}
		};
		var notify_msg = profile.nam+' joined the group';
		var sns_msg = '@'+gname+': '+profile.nam+' created the group';
		var msg_data = {
			mid			: msg_id, 
      body    : notify_msg,
      type    : Message.MESSAGE_TYPE.notification,
      pid     : pid,
      gid     : gid,
      tim     : tim,
      ttyp    : Message.MESSAGE_RECIPIENT_TYPE.GROUP,
      pnam		: profile.nam,
      actn    : actionData,
      sns_msg : sns_msg,
      gtyp    : gtyp,
      mt      : mtype 
	  };
	  ProcessGroupMessage.mobile(msg_data,function(error,resp){
	  	if(error){ 
	  		console.trace(error); 
	  		return logger.error({"FUNCTION":"abt_grp_c","packet":{msg:msg_data,actn:JSON.stringify(actionData)}});
	  	}
	  	dataEmit = copyObject(msg_data);
	  	dataEmit.r = profile.role;
	  	dataEmit.mt = GroupMember.TYPE.ADMIN;
	  	logger.info({"FUNCTION":"abt_grp_c","packet":{msg:dataEmit,actn:JSON.stringify(actionData)}});
      delete dataEmit['sns_msg'];
	  	io.to(pid).emit(Events.GROUP_MESSAGE,dataEmit);
	  	io.to(pid).emit(Events.GROUP_MESSAGE_W,dataEmit);
	  	return ;
	  });
	});
};

exports.abt_grp_d = function (pid, gid, gname, tim, cb) {
	var tim = moment()+'';
	UserInfoCache.getMongoProfile(pid,function(err, profile){
		if(err){ 
			logger.error({"FUNCTION":"abt_grp_d","err":err,"d":{pid:pid, gid:gid}}); 
			return cb(err,null);
		}
		if(!profile){ 
			logger.error({"FUNCTION":"abt_grp_d","err":"profile_not_found","d":{pid:pid, gid:gid}});
			return cb("abt_grp_d_profile_not_found",null);
		}
		var msg_id = MsgIdGen.generate(pid, gid, tim);
		var actionData = {
			t : Actions.GROUP_DELETE,
			d : {
				gid 	: gid,
				pid   : pid
			}
		};
		var notify_msg = 'This group has been removed by '+profile.nam;
		var sns_msg = '@'+gname+': '+profile.nam+' deleted the group';
		var msg_data = {
			mid			: msg_id, 
	    body    : notify_msg,
	    type    : Message.MESSAGE_TYPE.admin,
	    pid     : pid,
	    gid     : gid,
	    tim     : tim,
	    ttyp    : Message.MESSAGE_RECIPIENT_TYPE.GROUP,
	    pnam		: profile.nam,
	    actn    : actionData,
	    sns_msg : sns_msg
	  };
	  ProcessSpcfGroupMessage.save_grp_d(msg_data, pid, function(err,resp){
	  	if(err){ 
	  		logger.error({"FUNCTION":"abt_grp_d","err":err,"packet":{msg:msg_data,actn:JSON.stringify(actionData)}});
	  		return cb(err,null);
	  	}
	    dataEmit = copyObject(msg_data);
		  delete dataEmit['ttyp'];
		  delete dataEmit['sns_msg'];
		  dataEmit.r  = profile.role;
			dataEmit.mt = GroupMember.TYPE.ADMIN;
	  	logger.info({"FUNCTION":"abt_grp_d","packet":{msg:dataEmit,actn:JSON.stringify(actionData)}});
	  	io.to(gid).emit(Events.GROUP_MESSAGE,dataEmit);
	    io.to(gid+"_w").emit(Events.GROUP_MESSAGE_W,dataEmit);
	  	return cb(null,true);
	  });
	});
};

exports.abt_grp_j = function (gid, pid, gcode, gname, gpic, gact, gtyp, mtype, tim) {
	ProcessSpcfGroupMessage.rm_grp_msg_from_prof_rcq(pid, gid, function(err,resp){
		if(err){
			return logger.error({"FUNCTION":"abt_grp_j","msg":"rm_grp_msg_from_prof_rcq_error","p":{pid:pid,gid:gid},"err":err});
		}
		UserInfoCache.getMongoProfile(pid,function(err,profile){
			if(err){ 
				return logger.error({"FUNCTION":"abt_grp_j","msg":"profile_find_server_error","pid":pid,"err":err}); 
			}
			if(!profile){ 
				return logger.error({"FUNCTION":"abt_grp_j","msg":"profile_not_found","pid":pid});
			}
			var msg_id = MsgIdGen.generate(pid, gid, tim);
			var actionData = {
				t : Actions.GROUP_JOIN,
				d : {
					gid 	: gid,
					pid   : pid,
					gcod  : gcode,
					gnam  : gname,
					gpic  : gpic,
					act   : gact,
					gtyp  : gtyp,
					mtyp  : mtype
				}
			};
			var notify_msg = profile.nam+' joined the group';
			var sns_msg = '@'+gname+': '+notify_msg;
			var msg_data = {
				mid			: msg_id, 
	      body    : notify_msg,
	      type    : Message.MESSAGE_TYPE.notification,
	      pid     : pid,
	      gid     : gid,
	      tim     : tim,
	      ttyp    : Message.MESSAGE_RECIPIENT_TYPE.GROUP,
	      pnam		: profile.nam,
	      actn    : actionData,
	      sns_msg : sns_msg,
	      gtyp    : gtyp,
	      mt      : mtype
		  };
		  GroupMember.count({
		  	gid : gid,
		  	act : true,
		  	gdel: false
		  },function(err,members_count){
		  	if(err){ 
		  		return logger.error({"FUNCTION":"abt_grp_j","msg":"GroupMember_Count_DbError","err":err});
		  	}
		  	if(members_count > configs.GROUP_JOIN_NOTIFY_CONTROL_COUNT){
				  GroupMember.find({
				  	gid : gid,
				  	act : true,
				  	gdel: false,
				  	type: GroupMember.TYPE.ADMIN
				  },{
				  	pid : 1,
				  	_id : 0
				  },function(err,admins){
				  	if(err){ 
				  		return logger.error({"FUNCTION":"abt_grp_j","msg":"GroupMember_Find_DbError","gid":gid,"err":err,"mthd":"ProcessSpcfGroupMessage"}); 
				  	}
				  	if(admins.length == 0){
				  		return logger.error({"FUNCTION":"abt_grp_j","msg":"GroupMember_No_Admins_Found","gid":gid,"mthd":"ProcessSpcfGroupMessage"});  
				  	}
				    ProcessSpcfGroupMessage.save_grp_join(msg_data, admins, function(error,resp){
				    	if(error){ 
					  		return logger.error({"FUNCTION":"abt_grp_j","packet":{msg:msg_data,actn:JSON.stringify(actionData)},"err":error,"mem_cnt":members_count,"mthd":"ProcessSpcfGroupMessage"});
					  	}
					    dataEmit = copyObject(msg_data);
						  delete dataEmit['ttyp'];
						  delete dataEmit['sns_msg'];
						  dataEmit.r = profile.role;
			  		  dataEmit.mt = GroupMember.TYPE.MEMBER;
					  	logger.info({"FUNCTION":"abt_grp_j","packet":{msg:dataEmit,actn:JSON.stringify(actionData)},"mem_cnt":members_count,"mthd":"ProcessSpcfGroupMessage"});
					  	for(var i = 0; i < admins.length; i++) {
					  		io.to(admins[i].pid).emit(Events.GROUP_MESSAGE, dataEmit);
					  		io.to(admins[i].pid).emit(Events.GROUP_MESSAGE_W,dataEmit);
					  	}
					  	io.to(pid).emit(Events.GROUP_MESSAGE, dataEmit);
					  	io.to(pid).emit(Events.GROUP_MESSAGE_W,dataEmit);
					  	return;
					  });
				  });
		  	} else {
				  ProcessGroupMessage.mobile(msg_data,function(error,resp){
				  	if(error){ 
				  		return logger.error({"FUNCTION":"abt_grp_j","packet":{msg:msg_data,actn:JSON.stringify(actionData)},"mem_cnt":members_count,"mthd":"ProcessGroupMessage"});
				  	}
				  	dataEmit = copyObject(msg_data);
		      	delete dataEmit['sns_msg'];
		      	dataEmit.r = profile.role;
			  		dataEmit.mt = GroupMember.TYPE.MEMBER;
				  	logger.info({"FUNCTION":"abt_grp_j","packet":{msg:dataEmit,actn:JSON.stringify(actionData)},"mem_cnt":members_count,"mthd":"ProcessGroupMessage"});
					  io.to(pid).emit(Events.GROUP_MESSAGE,dataEmit);
					  io.to(gid).emit(Events.GROUP_MESSAGE,dataEmit);
			      io.to(gid+"_w").emit(Events.GROUP_MESSAGE_W,dataEmit);
					  return ;
				  });
		  	}
		  });
		});
	});
};

exports.abt_gtyp_e = function (gid, gtyp, pid, mem_type, gname, tim) {
	var notify_msg = '';
	var msg_id = MsgIdGen.generate(pid, gid, tim);
	// var actionData = {
	// 	t : Actions.GROUP_TYPE_EDIT,
	// 	d : {
	// 		gid 	: gid,
	// 		gtyp  : gtyp
	// 	}
	// };
	UserInfoCache.getMongoProfile(pid,function(err,profile){
		if(err){ 
			console.trace(err); 
			return; 
		}
		if(!profile){ 
			console.trace("Profile does not exists : no_profile_found"); 
			return;	
		}
		if(gtyp === Group.GROUP_TYPE.ONE_WAY){
			notify_msg = 'Now, only admins can post into this group';
		} else if(gtyp === Group.GROUP_TYPE.TWO_WAY){
			notify_msg = 'Now all the members can post messages in this group';
		}
		var sns_msg = '@'+gname+': '+notify_msg;
		var msg_data = {
			mid			: msg_id, 
	    body    : notify_msg,
	    type    : Message.MESSAGE_TYPE.notification,
	    pid     : pid,
	    gid     : gid,
	    tim     : tim,
	    ttyp    : Message.MESSAGE_RECIPIENT_TYPE.GROUP,
	    pnam		: profile.nam,
	    mt      : mem_type,
	    sns_msg : sns_msg,
	    gtyp    : gtyp
	    //actn    : actionData
	  };
	  ProcessGroupMessage.mobile(msg_data,function(error,resp){
	  	if(error){ 
	  		console.trace(error); 
	  		return logger.error({"FUNCTION":"abt_gtyp_e","packet":{msg:msg_data}});
	  	}
		  dataEmit = copyObject(msg_data);
		  delete dataEmit['ttyp'];
		  delete dataEmit['sns_msg'];
		  dataEmit.r = profile.role;
	  	dataEmit.mt = GroupMember.TYPE.ADMIN;
	  	logger.info({"FUNCTION":"abt_gtyp_e","packet":{msg:dataEmit}});
		  io.to(gid).emit(Events.GROUP_MESSAGE,dataEmit);
      io.to(gid+"_w").emit(Events.GROUP_MESSAGE_W,dataEmit);
		  return ;
	  });
	});
};

exports.abt_gnam_e = function (gid, gnam, gtyp, pid, notify_msg, action, tim) {
	var sns_msg = '@'+gnam+': '+notify_msg;
	UserInfoCache.getMongoProfile(pid,function(err,profile){
		if(err){ 
			console.trace(err); 
			return; 
		}
		if(!profile){ 
			console.trace("Profile does not exists : no_profile_found"); 
			return;	
		}
		var msg_id = MsgIdGen.generate(pid, gid, tim);
		var actionData = {
			t : action,
			d : {
				gid 	: gid,
				gnam  : gnam
			}
		};
		var msg_data = {
			mid			: msg_id, 
      body    : notify_msg,
      type    : Message.MESSAGE_TYPE.notification,
      pid     : pid,
      gid     : gid,
      tim     : tim,
      ttyp    : Message.MESSAGE_RECIPIENT_TYPE.GROUP,
      pnam		: profile.nam,
      actn    : actionData,
      sns_msg : sns_msg,
      gtyp    : gtyp
	  };
	  ProcessGroupMessage.mobile(msg_data,function(error,resp){
	  	if(error){ 
	  		console.trace(error); 
	  		return logger.error({"FUNCTION":"abt_gnam_e","packet":{msg:msg_data,actn:JSON.stringify(actionData)}});
	  	}
		  dataEmit = copyObject(msg_data);
		  delete dataEmit['ttyp'];
		  delete dataEmit['sns_msg'];
		  dataEmit.r = profile.role;
	  	dataEmit.mt = GroupMember.TYPE.ADMIN;
	  	logger.info({"FUNCTION":"abt_gnam_e","packet":{msg:dataEmit,actn:JSON.stringify(actionData)}});
		  io.to(gid).emit(Events.GROUP_MESSAGE,dataEmit);
      io.to(gid+"_w").emit(Events.GROUP_MESSAGE_W,dataEmit);
		  return ;
	  });
	});
};

exports.abt_gpic_e = function (gid, gpic, gnam, gtyp, pid, action, tim) {
	UserInfoCache.getMongoProfile(pid,function(err,profile){
		if(err){ 
			console.trace(err); 
			return; 
		}
		if(!profile){ 
			console.trace("Profile does not exists : no_profile_found"); 
			return;	
		}
		var notify_msg = profile.nam+' changed the pic for '+gnam;
		var sns_msg = '@'+gnam+': '+profile.nam+' changed the group profile pic';
		var msg_id = MsgIdGen.generate(pid, gid, tim);
		var actionData = {
			t : action,
			d : {
				gid 	: gid,
				gpic  : gpic
			}
		};
		var msg_data = {
			mid			: msg_id, 
      body    : notify_msg,
      type    : Message.MESSAGE_TYPE.notification,
      pid     : pid,
      gid     : gid,
      tim     : tim,
      ttyp    : Message.MESSAGE_RECIPIENT_TYPE.GROUP,
      pnam		: profile.nam,
      actn    : actionData,
      sns_msg : sns_msg,
      gtyp    : gtyp
	  };
	  ProcessGroupMessage.mobile(msg_data,function(error,resp){
	  	if(error){ 
	  		console.trace(error); 
	  		return logger.error({"FUNCTION":"abt_gpic_e","packet":{msg:msg_data,actn:JSON.stringify(actionData)}});
	  	}
		  dataEmit = copyObject(msg_data);
		  delete dataEmit['ttyp'];
		  delete dataEmit['sns_msg'];
		  dataEmit.r = profile.role;
	  	dataEmit.mt = GroupMember.TYPE.ADMIN;
	  	logger.info({"FUNCTION":"abt_gpic_e","packet":{msg:dataEmit,actn:JSON.stringify(actionData)}});
		  io.to(gid).emit(Events.GROUP_MESSAGE,dataEmit);
      io.to(gid+"_w").emit(Events.GROUP_MESSAGE_W,dataEmit);
		  return ;
	  });
	});
};

exports.abt_gpic_r = function (gid, gname, gtyp, pid, action, tim) {
	UserInfoCache.getMongoProfile(pid,function(err,profile){
		if(err){ 
			console.trace(err); 
			return; 
		}
		if(!profile){ 
			console.trace("Profile does not exists : no_profile_found"); 
			return;	
		}
		var notify_msg = profile.nam+' removed the pic for '+gname;
		var sns_msg = '@'+gname+': '+profile.nam+' removed the group profile pic';
		var msg_id = MsgIdGen.generate(pid, gid, tim);
		var actionData = {
			t : action,
			d : {
				gid 	: gid
			}
		};
		var msg_data = {
			mid			: msg_id, 
      body    : notify_msg,
      type    : Message.MESSAGE_TYPE.notification,
      pid     : pid,
      gid     : gid,
      tim     : tim,
      ttyp    : Message.MESSAGE_RECIPIENT_TYPE.GROUP,
      pnam		: profile.nam,
      actn    : actionData,
      sns_msg : sns_msg,
      gtyp    : gtyp
	  };
	  ProcessGroupMessage.mobile(msg_data,function(error,resp){
	  	if(error){ 
	  		console.trace(error); 
	  		return logger.error({"FUNCTION":"abt_gpic_r","packet":{msg:msg_data,actn:JSON.stringify(actionData)}});
	  	}
		  dataEmit = copyObject(msg_data);
		  delete dataEmit['ttyp'];
		  delete dataEmit['sns_msg'];
		  dataEmit.r = profile.role;
	  	dataEmit.mt = GroupMember.TYPE.ADMIN;
	  	logger.info({"FUNCTION":"abt_gpic_r","packet":{msg:dataEmit,actn:JSON.stringify(actionData)}});
		  io.to(gid).emit(Events.GROUP_MESSAGE,dataEmit);
      io.to(gid+"_w").emit(Events.GROUP_MESSAGE_W,dataEmit);
		  return ;
	  });
	});
};

exports.abt_gadm_a = function (gid, admin_id, pid, notify_msg, gname, gtyp, tim) {
	var sns_msg = '@'+gname+': '+notify_msg;
	UserInfoCache.getMongoProfile(admin_id,function(err,profile){
		if(err){ 
			console.trace(err); 
			return; 
		}
		if(!profile){ 
			console.trace("Profile does not exists : no_profile_found"); 
			return;	
		}
		var msg_id = MsgIdGen.generate(admin_id, gid, tim);
		var actionData = {
			t : Actions.GROUP_ADMIN_ADD,
			d : {
				gid 	: gid,
				pid   : pid
			}
		};
		var msg_data = {
			mid			: msg_id, 
      body    : notify_msg,
      type    : Message.MESSAGE_TYPE.notification,
      pid     : admin_id,
      gid     : gid,
      tim     : tim,
      ttyp    : Message.MESSAGE_RECIPIENT_TYPE.GROUP,
      pnam		: profile.nam,
      actn    : actionData,
      sns_msg : sns_msg,
      gtyp    : gtyp
	  };
	  ProcessGroupMessage.mobile(msg_data,function(error,resp){
	  	if(error){ 
	  		console.trace(error); 
	  		return logger.error({"FUNCTION":"abt_gadm_a","packet":{msg:msg_data,actn:JSON.stringify(actionData)}});
	  	}
		  dataEmit = copyObject(msg_data);
		  delete dataEmit['ttyp'];
		  delete dataEmit['sns_msg'];
		  dataEmit.r = profile.role;
	  	dataEmit.mt = GroupMember.TYPE.ADMIN;
	  	logger.info({"FUNCTION":"abt_gadm_a","packet":{msg:dataEmit,actn:JSON.stringify(actionData)}});
		  io.to(gid).emit(Events.GROUP_MESSAGE,dataEmit);
      io.to(gid+"_w").emit(Events.GROUP_MESSAGE_W,dataEmit);
		  return ;
	  });
	});
};

exports.abt_gadm_r = function(gid, pid, admin_id, admin_nam, admrole, notify_msg, gname, tim) {
	var sns_msg = '@'+gname+': '+notify_msg;
	var msg_id = MsgIdGen.generate(admin_id, gid, tim);
	var actionData = {
		t : Actions.GROUP_ADMIN_REM,
		d : {
			gid 	: gid,
			pid   : pid
		}
	};
	var msg_data = {
		mid			: msg_id, 
    body    : notify_msg,
    type    : Message.MESSAGE_TYPE.admin,
    pid     : admin_id,
    gid     : gid,
    tim     : tim,
    ttyp    : Message.MESSAGE_RECIPIENT_TYPE.GROUP,
    pnam		: admin_nam,
    spcf    : pid,
    actn    : actionData,
    sns_msg : sns_msg
  };

  dropAdminSeenCountKey(pid);
  
	GroupMember.find({
  	gid : gid,
  	act : true,
  	gdel: false,
  	type: GroupMember.TYPE.ADMIN
  },function(err,admins){
  	if(err) {console.trace(err); return; }
  	if(admins.length == 0) {console.trace("In abt_grp_lev : Admins of Group not found"); return; }
    ProcessSpcfGroupMessage.save_gadm_r(msg_data, admins, function(error,resp){
    	if(error){ 
	  		console.trace(error); 
	  		return logger.error({"FUNCTION":"abt_gadm_r","packet":{msg:msg_data,actn:JSON.stringify(actionData)}});
	  	}
	    dataEmit = copyObject(msg_data);
		  delete dataEmit['ttyp'];
		  delete dataEmit['spcf'];
		  delete dataEmit['sns_msg'];
		  dataEmit.r  = admrole;
	  	dataEmit.mt = GroupMember.TYPE.ADMIN;
	  	logger.info({"FUNCTION":"abt_gadm_r","packet":{msg:dataEmit,actn:JSON.stringify(actionData)}});
	  	for(var i = 0; i < admins.length; i++) {
	  		io.to(admins[i].pid).emit(Events.GROUP_MESSAGE, dataEmit);
	  		io.to(admins[i].pid).emit(Events.GROUP_MESSAGE_W,dataEmit);
	  	}
	  	io.to(pid).emit(Events.GROUP_MESSAGE, dataEmit);
	  	io.to(pid).emit(Events.GROUP_MESSAGE_W,dataEmit);
	  	return;
  	});
  });
};

exports.abt_g_l = function(gid, pid, pnam, prole, notify_msg, gname, tim) {
	var sns_msg = '@'+gname+': '+notify_msg;
	var msg_id = MsgIdGen.generate(pid, gid, tim);
	var actionData = {
		t : Actions.GROUP_LEAVE,
		d : {
			gid 	: gid,
			pid   : pid
		}
	};
	var msg_data = {
		mid			: msg_id, 
    body    : notify_msg,
    type    : Message.MESSAGE_TYPE.admin,
    pid     : pid,
    gid     : gid,
    tim     : tim,
    pnam		: pnam,
    ttyp    : Message.MESSAGE_RECIPIENT_TYPE.GROUP,
    actn    : actionData,
    sns_msg : sns_msg
  };
  GroupMember.find({
  	gid : gid,
  	act : true,
  	gdel: false,
  	type: GroupMember.TYPE.ADMIN
  },function(err,admins){
  	if(err) { console.trace(err); return; }
  	if(admins.length == 0) {console.trace("In abt_grp_lev : Admins of Group not found"); return; }
    ProcessSpcfGroupMessage.save_g_l(msg_data, admins, function(error,resp){
    	if(error){ 
	  		console.trace(error); 
	  		return logger.error({"FUNCTION":"abt_g_l","packet":{msg:msg_data,actn:JSON.stringify(actionData)}});
	  	}
	    dataEmit = copyObject(msg_data);
		  delete dataEmit['ttyp'];
		  delete dataEmit['sns_msg'];
		  dataEmit.r  = prole;
	  	dataEmit.mt = GroupMember.TYPE.MEMBER;
	  	logger.info({"FUNCTION":"abt_g_l","packet":{msg:dataEmit,actn:JSON.stringify(actionData)}});
	  	for(var i = 0; i < admins.length; i++) {
	  		io.to(admins[i].pid).emit(Events.GROUP_MESSAGE, dataEmit);
	  		io.to(admins[i].pid).emit(Events.GROUP_MESSAGE_W,dataEmit);
	  	}
	  	io.to(pid).emit(Events.GROUP_MESSAGE, dataEmit);
	  	io.to(pid).emit(Events.GROUP_MESSAGE_W,dataEmit);
	  	//removeProfileFromGroupRoom(aid, pid, gid);
	  	return;
	  });
  });
};

exports.abt_gmem_ban = function(gid, pid, admin_id, admin_name, admrole, notify_msg, gname, gtyp, tim) {
	var sns_msg = '@'+gname+': '+notify_msg;
	var msg_id = MsgIdGen.generate(admin_id, gid, tim);
	var actionData = {
		t : Actions.GROUP_MEMBER_BAN,
		d : {
			gid 	: gid,
			pid   : pid
		}
	};
	var msg_data = {
		mid			: msg_id, 
    body    : notify_msg,
    type    : Message.MESSAGE_TYPE.notification,
    pid     : admin_id,
    gid     : gid,
    tim     : tim,
    pnam		: admin_name,
    ttyp    : Message.MESSAGE_RECIPIENT_TYPE.GROUP,
    actn    : actionData,
    sns_msg : sns_msg,
    gtyp    : gtyp
  };
	ProcessGroupMessage.mobile(msg_data,function(error,resp){
  	if(error){ 
  		console.trace(error); 
  		return logger.error({"FUNCTION":"abt_gmem_ban","packet":{msg:msg_data,actn:JSON.stringify(actionData)}});
  	}
  	add_in_rcq(pid, tim, msg_id);
	  dataEmit = copyObject(msg_data);
	  delete dataEmit['ttyp'];
	  delete dataEmit['sns_msg'];
	  dataEmit.r = admrole;
	  dataEmit.mt = GroupMember.TYPE.ADMIN;
  	logger.info({"FUNCTION":"abt_gmem_ban","packet":{msg:dataEmit,actn:JSON.stringify(actionData)}});
	  io.to(gid).emit(Events.GROUP_MESSAGE,dataEmit);
    io.to(gid+"_w").emit(Events.GROUP_MESSAGE_W,dataEmit);
	  return ;
  });
};

exports.abt_gmem_ban_other_pids = function(gid, pid, admin_id, admin_name, admrole, notify_msg, gname, gtyp, tim){
	var msg_id = MsgIdGen.generate(pid, gid, tim);
	var actionData = {
		t : Actions.GROUP_MEMBER_BAN,
		d : {
			gid 	: gid,
			pid   : pid
		}
	};
	var msg_data = {
		mid			: msg_id, 
    body    : notify_msg,
    type    : Message.MESSAGE_TYPE.notification,
    pid     : admin_id,
    gid     : gid,
    tim     : tim,
    pnam		: admin_name,
    ttyp    : Message.MESSAGE_RECIPIENT_TYPE.GROUP,
    actn    : actionData
  };
  ProcessSpcfGroupMessage.save_gmem_ban_other_pids(msg_data, function(error,resp){
  	if(error){ 
  		return logger.error({"FUNCTION":"abt_gmem_ban_other_pids","packet":{msg:msg_data,actn:JSON.stringify(actionData)},"er":error});
  	}
    dataEmit = copyObject(msg_data);
	  delete dataEmit['ttyp'];
	  dataEmit.r = admrole;
  	dataEmit.mt = GroupMember.TYPE.ADMIN;
  	logger.info({"FUNCTION":"abt_gmem_ban_other_pids","packet":{msg:dataEmit,actn:JSON.stringify(actionData)}});
  	io.to(pid).emit(Events.GROUP_MESSAGE, dataEmit);
  	return;
  });
};

exports.abt_gmem_unban = function(gid, pid, admin_id, admin_name, admrole, notify_msg, gname, tim) {
	var sns_msg = '@'+gname+': '+notify_msg;
	var msg_id = MsgIdGen.generate(admin_id, gid, tim);
	var actionData = {
		t : Actions.GROUP_MEMBER_UNBAN,
		d : {
			gid 	: gid,
			pid   : pid
		}
	};
	var msg_data = {
		mid			: msg_id, 
    body    : notify_msg,
    type    : Message.MESSAGE_TYPE.admin,
    pid     : admin_id,
    gid     : gid,
    tim     : tim,
    pnam		: admin_name,
    ttyp    : Message.MESSAGE_RECIPIENT_TYPE.GROUP,
    actn    : actionData,
    sns_msg : sns_msg
  };
  GroupMember.find({
  	gid : gid,
  	act : true,
  	gdel: false,
  	type: GroupMember.TYPE.ADMIN
  },function(err,admins){
  	if(err) { console.trace(err); return; }
  	if(admins.length == 0) {console.trace("In abt_gmem_unban : Admins of Group not found"); return; }
    ProcessSpcfGroupMessage.save_gmem_unban(msg_data, admins, function(error,resp){
    	if(error){ 
	  		console.trace(error); 
	  		return logger.error({"FUNCTION":"abt_gmem_unban","packet":{msg:msg_data,actn:JSON.stringify(actionData)}});
	  	}
	    dataEmit = copyObject(msg_data);
		  delete dataEmit['ttyp'];
		  delete dataEmit['sns_msg'];
		  dataEmit.r = admrole;
	  	dataEmit.mt = GroupMember.TYPE.ADMIN;
	  	logger.info({"FUNCTION":"abt_gmem_unban","packet":{msg:dataEmit,actn:JSON.stringify(actionData)}});
	  	for(var i = 0; i < admins.length; i++) {
	  		io.to(admins[i].pid).emit(Events.GROUP_MESSAGE, dataEmit);
	  		io.to(admins[i].pid).emit(Events.GROUP_MESSAGE_W,dataEmit);
	  	}
	  	io.to(pid).emit(Events.GROUP_MESSAGE, dataEmit);
	  	io.to(pid).emit(Events.GROUP_MESSAGE_W,dataEmit);
	  	return;
	  });
  });
};

exports.abt_gmem_del = function(gid, pid, admin_id, admin_name, admrole, notify_msg, gname, tim) {
	var sns_msg = '@'+gname+': '+notify_msg;
	var msg_id = MsgIdGen.generate(admin_id, gid, tim);
	var actionData = {
		t : Actions.GROUP_MEMBER_DEL,
		d : {
			gid 	: gid,
			pid   : pid
		}
	};
	var msg_data = {
		mid			: msg_id, 
    body    : notify_msg,
    type    : Message.MESSAGE_TYPE.admin,
    pid     : admin_id,
    gid     : gid,
    tim     : tim,
    pnam		: admin_name,
    ttyp    : Message.MESSAGE_RECIPIENT_TYPE.GROUP,
    actn    : actionData,
    sns_msg : sns_msg
  };
  GroupMember.find({
  	gid : gid,
  	act : true,
  	gdel: false,
  	type: GroupMember.TYPE.ADMIN
  },function(err,admins){
  	if(err) return; 
  	if(admins.length == 0) {console.trace("In abt_gmem_del : Admins of Group not found"); return; }
    ProcessSpcfGroupMessage.save_gmem_del(msg_data, admins, function(error,resp){
    	if(error){ 
	  		console.trace(error); 
	  		return logger.error({"FUNCTION":"abt_gmem_del","packet":{msg:msg_data,actn:JSON.stringify(actionData)}});
	  	}
	    dataEmit = copyObject(msg_data);
		  delete dataEmit['ttyp'];
		  delete dataEmit['sns_msg'];
		  dataEmit.r = admrole;
	  	dataEmit.mt = GroupMember.TYPE.ADMIN;
	  	logger.info({"FUNCTION":"abt_gmem_del","packet":{msg:dataEmit,actn:JSON.stringify(actionData)}});
	  	for(var i = 0; i < admins.length; i++) {
	  		io.to(admins[i].pid).emit(Events.GROUP_MESSAGE, dataEmit);
	  		io.to(admins[i].pid).emit(Events.GROUP_MESSAGE_W,dataEmit);
	  	}
	  	io.to(pid).emit(Events.GROUP_MESSAGE, dataEmit);
	  	io.to(pid).emit(Events.GROUP_MESSAGE_W,dataEmit);
	  	return;
	  });
  });
};

exports.do_client_text_msg = function(gid, pid, notify_msg, msg_type, gnam, gtyp, mtype, cb) {
	var tim = moment()+'';
	UserInfoCache.getMongoProfile(pid,function(err,profile){
		if(err){ 
			logger.error({"FUNCTION":"do_client_text_msg","packet":{gid:gid,pid:pid,msg_type:msg_type,msg:notify_msg},"msg":"getMongoProfile_error","err":err});
			return cb(err,false); 
		}
		if(!profile){ 
			logger.error({"FUNCTION":"do_client_text_msg","packet":{gid:gid,pid:pid,msg_type:msg_type,msg:notify_msg},"er":"profile_not_found"});
			return ("profile_not_found"+pid,false);	
		}
		var msg_id = MsgIdGen.generate(pid, gid, tim);
		var msg_data = {
			mid			: msg_id, 
      body    : notify_msg,
      type    : msg_type,
      pid     : pid,
      gid     : gid,
      tim     : tim,
      ttyp    : Message.MESSAGE_RECIPIENT_TYPE.GROUP,
      pnam		: profile.nam,
      gnam    : gnam,
      gtyp    : gtyp,
      mt      : mtype
	  };
	  ProcessGroupMessage.mobile(msg_data,function(err,resp){
	  	if(err){ 
	  		logger.error({"FUNCTION":"do_client_text_msg","packet":msg_data,"er":err});
	  		return cb(err,false);
	  	}
		  var room = gid;
		  msg_data.r = profile.role;
		  io.to(room).emit(Events.GROUP_MESSAGE,msg_data);
		  io.to(room).emit(Events.GROUP_MESSAGE_W,msg_data);
		  logger.info({"FUNCTION":"do_client_text_msg","packet":msg_data,"msg":"success"});
		  return cb(null,true);
	  });
	});
};

exports.do_client_image_msg = function(mid, gid, pid, notify_msg, msg_type, gnam, gtyp, mtype, cb) {
	var tim = moment()+'';
	UserInfoCache.getMongoProfile(pid,function(err,profile){
		if(err){ 
			logger.error({"FUNCTION":"do_client_image_msg","packet":{gid:gid,pid:pid,msg_type:msg_type,msg:notify_msg},"msg":"getMongoProfile_error","err":err});
			return cb(err,false); 
		}
		if(!profile){ 
			logger.error({"FUNCTION":"do_client_image_msg","packet":{gid:gid,pid:pid,msg_type:msg_type,msg:notify_msg},"er":"profile_not_found"});
			return ("profile_not_found"+pid,false);	
		}
		var msg_id = mid;
		var msg_data = {
			mid			: msg_id, 
      body    : notify_msg,
      type    : msg_type,
      pid     : pid,
      gid     : gid,
      tim     : tim,
      ttyp    : Message.MESSAGE_RECIPIENT_TYPE.GROUP,
      pnam		: profile.nam,
      gnam    : gnam,
      gtyp    : gtyp,
      mt      : mtype
	  };
	  ProcessGroupMessage.mobile(msg_data,function(err,resp){
	  	if(err){ 
	  		logger.error({"FUNCTION":"do_client_image_msg","packet":msg_data,"er":err});
	  		return cb(err,false);
	  	}
		  var room = gid;
		  msg_data.r = profile.role;
		  io.to(room).emit(Events.GROUP_MESSAGE,msg_data);
		  io.to(room).emit(Events.GROUP_MESSAGE_W,msg_data);
		  logger.info({"FUNCTION":"do_client_image_msg","packet":msg_data,"msg":"success"});
		  return cb(null,true);
	  });
	});
};

function dropAdminSeenCountKey(pid){
	rhmset.removekey(RedisPrefixes.SEEN_COUNT+pid, function(err,resp){
		if(err) {console.trace(err); return; }
		if(resp == 1) {
			console.log('In Admin Removal : Seen Count key for admin pid : '+pid+' dropped succesfully');
		} else {
			console.log('In Admin Removal : Seen Count key for admin pid : '+pid+' could not be dropped');
		}
	});
}

function removeProfileFromGroupRoom(aid, pid, gid) {
	Profile.distinct('_id',{
  	aid : aid,
  	act : true
  },function(err,profiles){
  	if(err) {console.trace(err); return; }
  	if(!profiles || profiles.length == 0) {console.trace("In removeProfileFromGroupRoom : No Profiles Found for pid"); return; }
  	GroupMember.find({
  		pid : { $in : profiles },
  		gid : gid,
  		act : true
  	},function(err,groups){
  		if (err) { console.trace(err) ; }
  		//if (!groups) { console.log('In removeProfileFromGroupRoom : No Groups to remove'); return; }
  		if (groups.length > 0) { console.trace('In removeProfileFromGroupRoom : User has joined the group with other profiles also'); return; }
  		//if ( groups.length == 1 && groups[0].pid == pid ) {
	  		var room_clients = io.sockets.adapter.rooms[gid]; 
			  for (var clientId in room_clients ) {
			    if ( io.sockets.connected[clientId].username == aid ) {
			    	io.sockets.connected[clientId].leave(gid);
			    }
			  }
			//}
  	});
  });
}

function add_in_rcq(pid, tim, mid) {
  rcq.add(pid, tim, mid, function(err,res){
    if(err){
    	console.trace(err); return; 
    }
    if(!res){
    	console.trace("Could not Queue "+mid+" message for "+pid+" profile at " +tim);
    }
  });
}

function copyObject(obj) {
  if (null == obj || "object" != typeof obj) return obj;
  var copy = obj.constructor();
  for (var attr in obj) {
    if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
  }
  return copy;
}