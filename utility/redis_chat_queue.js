var _ = require('underscore');


var rcq = function(client) {
	var redis_client = client;
	var PREFIX = "q";

	return {
		//O(log(N))
		add : function(pid,score,obj,cb) {
			//check the length of the current queue, and store into db if it exceeds
			redis_client.zadd([PREFIX+pid,score,obj],function(err,res){
				if(err) {return cb(new Error(err));}
				
				return cb(null,res);
			});
		},
		//O(N)
		scan : function(pid,cb) {
			var start = 0;
			redis_client.zscan(PREFIX+pid,start,function(err,res){
				if(err) {return cb(new Error(err));}

				return cb(null,res);			
			});
		},
		scan_start : function(pid,start,cb) {
			redis_client.zscan(PREFIX+pid,start,function(err,res){
				if(err) {return cb(new Error(err));}

				return cb(null,res);			
			});
		},
		count : function(pid,min_score,max_score,cb) {
			redis_client.zcount(PREFIX+pid,min_score,max_score,function(err,res){
				if(err) {return cb(new Error(err));}
				
				return cb(null,res);
			});
		},
		scanall : function(pid,cb) {
			var start = 0;
			var me = this;

			var total = [];

			var cb1 = function(err,res){
				if(err) {return cb(new Error(err));}

				// total = _.union(res[1],total);
				total = total.concat(res[1]);
				if(res[0]!=0) {
					me.scan_start(pid,res[0],cb1);
				} else {
					return cb(null,[0,total]);
				}
			};

			this.scan_start(pid,start,cb1);
		},
		//O(log(N))
		remove : function(pid,obj,cb) {
			redis_client.zrem([PREFIX+pid,obj],function(err,res){
				if(err) {return cb(new Error(err));}
				return cb(null,res);
			});
		},
		removeall : function(pids,obj,cb) {
			var me = this;
			var count = 0;
			var total = 0;

			for(var k=0; k<pids.length; k++){
	      me.remove(pids[k],obj,function(err,res){
	      	total = total + 1;
	      	if (err) { console.trace(err); }
	      	if (res == 1) {	count = count + 1;}
	      	if (count == pids.length || total == pids.length) {	cb(null,count);	}
	      });
	    }
		},
		//remove all objs from the queue
		removeAllObjs : function(pid, cb){
			redis_client.zremrangebyrank(PREFIX+pid,0,-1,function(err,res){
				if(err) {return cb(new Error(err));}
				return cb(null,res);
			});
		},
		exists : function(pid, obj, cb){
			redis_client.zscore(PREFIX+pid,obj,function(err,res){
				if(err) {return cb(new Error(err));}
				return cb(null,res);
			});
		},
		getall : function(pid, cb){
			redis_client.ZRANGEBYSCORE(PREFIX+pid,'-inf','+inf',function(err,res){
				if(err) {return cb(new Error(err));}
				
				return cb(null,res);
			});
		},
		cardinality : function(pid,cb) {
			redis_client.zcard(PREFIX+pid,function(err,res){
				if(err) {return cb(new Error(err));}
				return cb(null,res);
			});
		},
		removekey : function(pid, cb) {
			redis_client.del(PREFIX+pid, function(err,res){
				if(err) {return cb(new Error(err));}
				return cb(null,res);
			});
		},
		removekeyWithPrefix : function(prefix, id, cb) {
			redis_client.del(prefix+id, function(err,res){
				if(err) {return cb(new Error(err));}
				return cb(null,res);
			});
		}
	};
};

module.exports = rcq;