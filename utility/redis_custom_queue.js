var _ = require('underscore');

var custom_redis = function(client) {
	var redis_client = client;

	//O(log(N))
	return {
		add : function(add_object,score,obj,cb) {
			console.log('Adding in rCustomQ :');
			console.log(add_object);
			console.log(obj);
			redis_client.zadd(add_object,score,obj,function(err,res){
				if(err) {return cb(new Error(err));}
				return cb(null,res);
			});
		},

		//O(log(N))
		remove : function(remove_object,obj,cb) {
			console.log('Removing in rCustomQ :');
			console.log(remove_object);
			console.log(obj);
			redis_client.zrem(remove_object,obj,function(err,res){
				if(err) {return cb(new Error(err));}
				return cb(null,res);
			});
		},

		//O(N)
		scan : function(search_query,cb) {
			var start = 0;
			redis_client.zscan(search_query,start,function(err,res){
				if(err) {return cb(new Error(err));}
				return cb(null,res);			
			});
		}
	};
};

module.exports = custom_redis;