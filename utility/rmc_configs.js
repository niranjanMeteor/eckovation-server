var configs = {
	MODEL_SHORT_NAMES : {
		"Account" 		: "a",
		"Group"			: "g",
		"GroupMember"	: "G",
		"Otp"			: "o",
		"Profile"		: "p",
		"Message"		: "m"
	},
	//in seconds
	MODEL_CACHE_TIME : {
		"Account"		: 6*30*24*3600, // 6 Months
	},
	//in seconds
	DEFAULT_CACHE_TIME : 2*30*24*3600,   // 2 Months
};

module.exports = configs;
