var http 							= require('http');
var https             = require('https');
var configs 					= require('./../configs.js');
var CallEngines       = require('./call_engines.js');
var CallConfigs       = require('./call_configs.js');

var PROTOCOL 					= "http";
var httpUrl           = "http";
var httpsUrl          = "https";

//http://www.kookoo.in/outbound/outbound.php?phone_no=9333422312&api_key=KKd6a3ec8cd7a2dd76fa2b0b953ab1e6b9&outbound_version=2&extra_data=<response><playtext>ILove KooKoo</playtext><hangup/></response>

exports.caller = function(engine, phone, ccode, otp_code, cb) {
	var urlAndProtocol = getCallingUrl(engine, phone, ccode, otp_code);
	generateOtpCall(urlAndProtocol, engine, phone, ccode, function(err,resp){
		if(err){
			console.trace(err);
		}
		return cb(resp.data,resp.ifQ);
	});
};

function getCallingUrl(engine, phone, ccode, otp_code){
	var callingConfigs  = CallConfigs(engine);
	var HOST            = callingConfigs.HOST;
	var API_PATH        = callingConfigs.CALL_API_PATH;
	var CALL_PROTOCOL   = callingConfigs.PROTOCOL;
	var params          = {};

	switch(engine){
		case CallEngines.KOOKOO:
			params.phone_no         = phone;
			params.api_key          = callingConfigs.API_KEY;
			params.outbound_version = callingConfigs.OUTBOUND_VERSION;
			params.extra_data       = getExtraDataParamForKookoo(otp_code);
			params["extra_data"]    = encodeURIComponent(params["extra_data"]);
		break;
		case CallEngines.NEXMO:
			params.api_key          = callingConfigs.API_KEY;
			params.api_secret       = callingConfigs.API_SECRET;
			params.to               = ccode+''+phone;
			params.repeat           = callingConfigs.REPEAT;
			params.text             = encodeURIComponent(getTextParamForNexmo(otp_code));
		break;
	}
	
	var tmp = new Array();
	for(var key in params) {
		tmp.push(key+"="+params[key]);
	}
	var query_string = tmp.join("&");
	var url = CALL_PROTOCOL+"://"+HOST+API_PATH+"?"+query_string;

	return {protocol : CALL_PROTOCOL, callUrl : url};
}

function generateOtpCall(urlAndProtocol, engine, phone, ccode, callback){
	var MakeCall, method, was_queued, respData;
	if(urlAndProtocol.protocol == httpUrl){
		MakeCall     = http;
		method       = 'http';
	} else if(urlAndProtocol.protocol == httpsUrl){
		MakeCall     = https;
		method       = 'https';
	}
	console.log('url for Call is');
	console.log(urlAndProtocol.callUrl);
	MakeCall.get(urlAndProtocol.callUrl,function(response){
		response.setEncoding('utf8');
		response.on("data",function(data) {
			console.log(data);
			var callResp = checkCallResponse(data,engine);
			was_queued   = callResp.queued;
			respData     = callResp.data;
			if (was_queued) {
				console.log(method+' otp call me was successfull for engine :'+engine+' and phone : '+phone);
			} else {
				console.log(method+' otp call me  was unsuccessfull for engine :'+engine+' and phone : '+phone);
			}
			return callback(null,{engn:engine, data:respData, ifQ:was_queued});
		});
	});
}

function checkCallResponse(response, engine){
	switch(engine){

		case CallEngines.KOOKOO:
			try{
				if(response.match(/queued/))
					return {data:response, queued:true};
				return {data:response, queued:false};
			} catch (e) {  
				console.log('exception occurent from KOOKOO CALL SERVICE');
				console.log(e);
				console.log(response);
				return {data:{}, queued:false};
			}
		break;

		case CallEngines.NEXMO:
			try{
				var respData = JSON.parse(response);
				if(respData.status == 0 || respData.status == '0')
					return {data:respData, queued:true};
				return {data:respData, queued:false};
			} catch (e) {  
				console.log('exception occurent from NEXMO CALL SERVICE');
				console.log(e);
				console.log(response);
				return {data:{}, queued:false};
			}
		break;

	}
}

function getTextParamForNexmo(otp_code){
	var code = otp_code+'';
	var data  = '<break time="1s"/> YOUR, ONE, TIME, PASSWORD, FOR, ECKOVATION, IS ';
	for(var i=0; i<code.length; i++){
		if(i == code.length-1){
			data   += code[i]+' <break time="1s"/>'
		} else {
			data   += code[i]+', '
		}
	}
	return data;
}

function getExtraDataParamForKookoo(otp_code){
	var text_for_otp = createTextForOtpCode(otp_code);
	var data  = '<response><playtext> YOUR ONE TIME PASSWORD FOR ECKOVATION IS   ';
	data     += text_for_otp+' .';
	data     += 'YOUR ONE TIME PASSWORD FOR ECKOVATION IS   ';
	data     += text_for_otp+' .';
	data     += '</playtext><hangup/></response>';
	return data;
}

function createTextForOtpCode(otp_code){
	var digitsInWords = ['zero','one','two','three','four','five','six','seven','eight','nine'];
	var string_digits = otp_code+'';
	var result = '';
	var otp_len = otp_code.length;
	for(var i=0; i<otp_len; i++){
		var num = parseInt(string_digits[i]);
		if(i == otp_len)
			result += digitsInWords[num];
		else result += digitsInWords[num]+' ';
	}
	return result;
}