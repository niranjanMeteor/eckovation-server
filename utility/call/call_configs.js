var CallEngines        = require('./call_engines.js');

var CallEngineConfigs = function(engine){
	var configs = {};
	switch(engine){

		case CallEngines.KOOKOO:
		  configs.PROTOCOL              = "http";
			configs.HOST 						      = "www.kookoo.in";
			configs.API_KEY               = "KKd6a3ec8cd7a2dd76fa2b0b953ab1e6b9";
			configs.CALL_API_PATH 		    = "/outbound/outbound.php";
			configs.OUTBOUND_VERSION      = 2;
			return configs;

		case CallEngines.NEXMO:
		  configs.PROTOCOL              = "https";
			configs.HOST 						      = "api.nexmo.com";
			configs.API_KEY               = "c890d09d";
			configs.API_SECRET            = "32c5bde81bcb1f07";
			configs.CALL_API_PATH 		    = "/tts/json";
			configs.REPEAT                = 2;
			return configs;
	}
};

module.exports = CallEngineConfigs;