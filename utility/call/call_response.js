var CallMeResponse = function(){
	var responses = {
		MAX_LIMIT_REACHED 				: "You have exceeded the max limit. Please request a new otp",
		OTP_EXPIRED               : "Your otp has expired. Please request a new otp",
		TRY_LATER                 : "Please try again later",
		BANNED_FOR_CALL           : "Please try again later"
	};
};

module.exports  = CallMeResponse;