require('shelljs/global');
var app 												= require('express')();
var fs 													= require('fs');
var moment											= require('moment');
var configs;

decideServer();

var dirname 										= __dirname;
var server_conf_filename 				= configs.SERVICE_NAME+'.conf'
var server_conf_full_filename 	= dirname+'/'+server_conf_filename;
var logDir 											= configs.LOG_DIR;
var logFile 										= logDir+'/express_'+configs.SERVER_NUM+'-'+moment().format()+'.log';
var NodeHeapSize                = configs.HEAP_SIZE_MAX;
var NodeStackSize               = configs.STACK_SIZE_MAX;

rm('-rf',server_conf_full_filename);

var data = 'limit nofile 100000 100000\n\n';
	data += 'start on runlevel [2345]\n\n';
	data += 'setuid '+configs.SERVICE_USER+'\n\n';
	data += 'respawn\n\n';

if(app.get('env') !== 'debug'){
	data += 'pre-start script\n';
	data += ' exec /usr/bin/node '+dirname+'/utility/notify_server/restart.js '+configs.SERVER_NUM+' 2>&1\n';
	data += 'end script\n\n';
}

data += 'env PORT='+configs.PORT+'\n\n';
data += 'exec /usr/bin/node --max_old_space_size='+NodeHeapSize+' --stack-size='+NodeStackSize+' '+dirname+'/bin/www >> '+logFile+' 2>&1\n\n';

if(app.get('env') !== 'debug'){
	data += 'post-stop script\n';
	data += ' exec /usr/bin/node '+dirname+'/utility/notify_server/crash.js '+configs.SERVER_NUM+' 2>&1\n';
	data += 'end script\n\n';
}

fs.appendFile(server_conf_full_filename, data, function(err){
	if (err) {
		console.trace(err);
		return;
	}
	mkdir('-p', logDir);

	// TO BE THOUGHT WHETEHR PERMISSONS LESSAR THATN 777 CAN BE GIVEN
	chmod(777, logDir);

	cp('-f',server_conf_full_filename, '/etc/init/'+server_conf_filename);

	exec('service '+configs.SERVICE_NAME+' start', function(status, output) {
	  console.log('Exit status:', status);
	  console.log('Program output:', output);
	});

});

function decideServer(){
	var server_num = process.argv[2];
	if(!server_num || isNaN(server_num)){
		console.log("Server Number required in Start Server Command");
		process.exit(0);
	}
	server_num = parseInt(server_num);
	switch(server_num) {

		case 1:
			configs = require('./utility/configs.js').FIRST_SERVER;
			break;
		case 2:
			configs = require('./utility/configs.js').SECOND_SERVER;
			break;
		case 3:
			configs = require('./utility/configs.js').THIRD_SERVER;
			break;
		case 4:
			configs = require('./utility/configs.js').FOURTH_SERVER;
			break;
		case 5:
			configs = require('./utility/configs.js').FIFTH_SERVER;
			break;

	}
	if(!configs){
		console.log("Server configs Could not be decided, Server Number Seems Invalid");
		process.exit(0);
	}
	return;
}