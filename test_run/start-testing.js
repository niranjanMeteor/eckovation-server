// if(process.env.NODE_ENV != "dapi"){
// 	process.exit(1);
// }

require('shelljs/global');

var fs                	= require('fs');
var ip                	= require('ip');
var testConfigs       	= require('./../test/test_configs.js');

var myIpAddres        	= ip.address();
var serverBin         	= __dirname+'/../bin/www';
var mochaTestModuleDir 	= __dirname+'/../test/routes/';

var server_process_pid, mongodb_process_pid, redis_process_pid;

// if(myIpAddres != '10.116.5.232'){
// 	console.log('Test Module cannot be run on this machine');
// 	process.exit(1);
// }

var testConfigFulFileName  = __dirname+'/../test/test_configs.js';
if(fs.existsSync(testConfigFulFileName)){
	var testConfig  =  require(testConfigFulFileName);
	if(testConfig.MONGO_HOST != '127.0.0.1'){
		console.log('=> Invalid Mongo Host for Test Environement');
		console.log('=> Exiting The Program');
		process.exit(1);
	}
	if(testConfig.REDIS_HOST != '127.0.0.1'){
		console.log('=> Invalid Redis Host for Test Environement');
		console.log('=> Exiting The Program');
		process.exit(1);
	}
	if(testConfig.DB_NAME != 'eckovation_test'){
		console.log('=> Invalid Mongo Database Name for Test Environement');
		console.log('=> Exiting The Program');
		process.exit(1);
	}
	if(testConfig.MONGO_UNAME){
		console.log(testConfig.MONGO_UNAME);
		console.log('=> Invalid Mongo User Name for Test Environement');
		console.log('=> Exiting The Program');
		process.exit(1);
	}
	if(testConfig.MONGO_PORT != '27019'){
		console.log('=> Invalid Mongo Port for Test Environement');
		console.log('=> Exiting The Program');
		process.exit(1);
	}
	if(testConfig.REDIS_PORT != '6378'){
		console.log('=> Invalid Redis Port for Test Environement');
		console.log('=> Exiting The Program');
		process.exit(1);
	}
} else {
	console.log('=> Overwrite Configs File Missing');
	console.log('=> Exiting The Program');
	process.exit(1);
}

console.log('\n');
welcomeScript();
console.log('\n');

startMongoDbProcess(function(err,resp){
	if(err){
		process.exit(resp);
	}
	setTimeout(function(){
		startServerProcess(function(errS,respS){
			if(errS){
				process.exit(respS);
			}
			setTimeout(function(){
				exec('mocha --recursive '+mochaTestModuleDir, function(status_mocha, output3){
					console.log('**************************	  Mocha Test case module completed   **************************');
					console.log('\n');
						exec('kill '+server_process_pid+'', function(status_stop_server, output4){
							console.log('=> Node Server Stopped : '+server_process_pid);
							console.log('\n');
							exec('kill '+mongodb_process_pid+'', function(status_stop_mongodb, output4){
								console.log('=> MongoDB Stopped : '+mongodb_process_pid);
								console.log('\n');
								console.log('=> Exiting The Program');
								process.exit(status_mocha);
							});
							process.exit(status_mocha);
						});
				});
			},30000);
		});
	},35000);
});

function startMongoDbProcess(cb){
	exec('mongod --fork --port '+testConfigs.MONGO_PORT+' --noauth --dbpath ~/dbdata/mongodb2 --logpath ~/dblog/mongodb2/mongodb2.log && echo $!',function(code,stdout,stderr){
		if(stderr){
			console.log('=> MongoDb Could not be started');
			console.log(stderr);
			console.log('=> Exiting The Program');
			return cb(true,code);
		}
		if(code != 0){
			console.log('=> MongoDb Start was unsuccesfull');
			console.log('=> Program Output Error Code : '+code);
			console.log('=> Exiting The Program');
			return cb(true,code);
		}
		if(!stdout && isNaN(stdout)){
			console.log('=> MongoDb Started but Process PID NOT FOUND');
			console.log('=> Program output is');
			console.log(stdout);
			console.log('=> Exiting The Program');
			return cb(true,code);
		}
		
		mongodb_process_pid = stdout.split(":")[1].split("\n")[0].trim();
		console.log('=> Started Testing MongoDB : '+mongodb_process_pid);
		console.log('\n');
		return cb(null,true);
	});
}

function startServerProcess(cb){
	exec('node '+serverBin+' > ~/eckovationlog/express2.log 2>&1 & echo $!',function(code,stdout,stderr){
		if(stderr){
			console.log('=> Node Server Could not be started');
			console.log(stderr);
			console.log('=> Exiting The Program');
			return cb(true,code);
		}
		if(code != 0){
			console.log('=> Node Server Start was unsuccesfull : '+code);
			console.log('=> Exiting The Program');
			return cb(true,code);
		}
		if(!stdout && isNaN(stdout)){
			console.log('=> Node Server Started but Process PID NOT FOUND');
			console.log('=> Program output is');
			console.log(stdout);
			console.log('=> Exiting The Program');
			return cb(true,code);
		}
		server_process_pid = stdout;

		console.log('=> Started Node Server : '+server_process_pid);
		console.log('\n');
		return cb(null,true);
	});
}

function welcomeScript(){
	console.log('\n');
	console.log('######################################################################################################');
	console.log('######################################################################################################');
	console.log('######################################################################################################');
	console.log('##########  ##########  #  ############  #############  ##  #############           ##################');
	console.log('##########  #########  ##  #########  #####  #########  #####  ##########  ###########################');
	console.log('##########  ########  ###  #######  ##########  ######  #######  ########  ###########################');
	console.log('##########  #######  ####  ######  ############  #####  #########  ######  ###########################');
	console.log('##########  ######  #####  #####  #############  #####  ##########  #####           ##################');
	console.log('##########  #####  ######  ######  ###########  ######  #########  ######  ###########################');
	console.log('##########  ####  #######  #######  ########  ########  #######  ########  ###########################');
	console.log('##########  ###  ########  #########  ####  ##########  #####  ##########  ###########################');
	console.log('##########  ##  #########  ############  #############  ###  ############           ##################');
	console.log('######################################################################################################');
	console.log('######################################################################################################');
	console.log('######################################################################################################');
	console.log('\n');
}


// var to_kill_process_count = 0;

// var grepresult 				= exec('ps -aux | grep /var/www/eckovation-server/bin/www');
// var grepresult 				= exec('ps -aux | grep node');
// grepresultOut 				= grepresult.output.split("\n");
// console.log(grepresultOut);


// console.log('CHECKING IF ANY ALREADY RUNNING TESTING SERVER PROCESS ON THIS MACHINE..........')

// for(var i=0; i<grepresultOut.length; i++){
// 	if(grepresultOut[i].match(/node .\/bin\/www/)){
// 		to_kill_process_count++;
// 		var to_kill_pid 		= grepresultOut[i].split(" ")[2];
// 		var killProcess 		= exec('kill '+to_kill_pid+'');

// 		console.log('Found one server process already running with pid : '+to_kill_pid);

// 		if(killProcess.code != 0){
// 			console.log('Could not stop an existing node server process on this machine and hence this script will not proceed further');
// 			console.log(killProcess.code);
// 			process.exit(killProcess.code);
// 		} else {
// 			console.log('Server process with pid : '+to_kill_pid+' , stopped.........');
// 		}
// 	}
// }

// if(to_kill_process_count == 0){
// 	console.log('Seems like, there were no already running server process on this machine.........');
// } else {
// 	console.log('All existing Testing server process stopped successfully.........');
// }