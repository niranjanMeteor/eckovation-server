var fs                                = require('fs');

var configs = {

	CONFIGS_OVERWRITE_FILE                    : "configs_overwrite.js",
	ORDER_ID_RANDOM_NUMBER_LENGTH             : 4,
	GROUP_JOIN_ACTIVITY_PACKAGE_EXPIRY        : 6*30*24*60*60*1000,
	PAYMENT_NOTIFY_ATTEMPTS_LIMIT             : 3,
	SMS_PAYMENT_NOTIFY_REATTEMPT_DELAY        : 30*1000,
	PAYMENT_STATUS_NOTIFY_DELAY               : 5*60*1000,
	SMS_PAYMENT_NOTIFY_TEMPLATE               : "Your order (<%= trxn_id %>) of Rs <%= amt %> for <%= pkg_name %> on <%= group %> <%= status %>.",
	PAYMENT_HELPLINE_NUMBER                   : "9953933422",
	PAYMENT_HELPLINE_EMAIL                    : "billing@eckovation.com",
	PAYMENT_STATUS_TEMPLATE_MAIL              : "/home/ubuntu/eckovation-server/payment/payment_status_template.html"
};

var overwriteConfigFulFileName  = __dirname+'/'+configs.CONFIGS_OVERWRITE_FILE;
if(fs.existsSync(overwriteConfigFulFileName)){
	var overwriteConfig  =  require(overwriteConfigFulFileName);
	for (var key in overwriteConfig){
		configs[key] = overwriteConfig[key];
	}
} else {
	console.log('No Overwrite Configs File Found to overwrite any payment configurations key');
}

module.exports = configs;