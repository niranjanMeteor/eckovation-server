var https             			= require('https');
var PaytmConfigs 						= require('./paytm_configs.js');

exports.getTrxnStatus = function(order_id, callback){
	var trxnStatusUrl = getTransactionStatusUrl(order_id);
	console.log('http trxnStatusUrl request : '+trxnStatusUrl);
	try {
		https.get(trxnStatusUrl,function(response){
			if(!response){
				console.log('responseData is Null');
				return callback("PAYTM_TRXN_STATUS_API_FAILED",null);
			}
			response.setEncoding('utf8');
			console.log('Reponse  statusCode From Paytm');
			console.log(response.statusCode);
			if(!response.statusCode || response.statusCode != 200){
				console.trace("PAYTM_TRXN_STATUS_API_RESPONSE_IS_NOT_200");
				return callback("PAYTM_TRXN_STATUS_API_RESPONSE_IS_NOT_200",null);
			}
			response.on("data",function(responseData) {
				console.log('response from paytm get Transaction status');
				if(!responseData){
					console.trace("PAYTM_TRXN_STATUS_API_RESPONSE_DATA_NOT_FOUND");
					return callback("PAYTM_TRXN_STATUS_API_RESPONSE_DATA_NOT_FOUND",null);
				}
				console.log('Call From Paytm TRXn Status Api Successfull');
				console.log(responseData);
				return callback(null,JSON.parse(responseData));
			});
		});
	} catch(excp){
		console.trace(excp);
		console.log(method+' Exception and Failure in Fetching Transaction Status for OrderId :'+order_id);
		console.log(data);
		return callback("error in trxn status api call",false);
	}
}

function getTransactionStatusUrl(order_id){
	var HOST            = PaytmConfigs.HOST;
	var API_PATH        = PaytmConfigs.TRXN_STATUS_API_PATH;

	var params = encodeURIComponent('"MID"');
	params += ':';
	params += encodeURIComponent('"'+PaytmConfigs.MID+'"');
	params += ',';
	params += encodeURIComponent('"ORDERID"');
	params += ':'
	params += encodeURIComponent('"'+order_id+'"');

	var url = 'https://'+HOST+API_PATH+'?JsonData={'+params+'}';
	return url;
}