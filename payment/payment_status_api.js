var mongoose                    = require('mongoose');

var PaytmApi                    = require('./paytm_api.js');
var RazorpayApi                 = require('./razorpay_api.js');

require('../models/payment/transactions.js');
var Transaction                 = mongoose.model('Transaction');

exports.getTrxnStatus = function(trxn_id, gtwy, razorpay_id, cb){

  switch(gtwy){

    case Transaction.GATEWAY.PAYTM:
      PaytmApi.getTrxnStatus(trxn_id, function(err,resp){
        if(err){
          console.trace(err);
          return cb('PaytmApi getTrxnStatus Status API Failed',null);
        }
        if(!resp){
          console.log(resp);
          return cb('PaytmApi getTrxnStatus API Reponse Not Found',null);
        }
        return cb(null,resp);
      });
    break;

    case Transaction.GATEWAY.RAZORPAY:
      if(!razorpay_id || razorpay_id == null){
        return cb(null,{STATUS:"PENDING",RESPMSG:"Looks like you cancelled the payment, you can try later."})
      }
      RazorpayApi.status(razorpay_id, function(err,resp){
        if(err){
          console.trace(err);
          return cb('RazorpayApi getTrxnStatus Status API Failed',null);
        }
        if(!resp){
          console.log(resp);
          return cb('RazorpayApi getTrxnStatus API Reponse Not Found',null);
        }
        return cb(null,resp);
      });
    break;

    default:
      return cb("Gateway Did not Match",null);

  }
};