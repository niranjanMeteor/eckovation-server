const PaymentConfigs         = require('../payment/configs.js');
const FeeConfigs             = require('../fee/configs.js');

exports.generateNewOrderId = function(){
	var first  = "ECKO";
	var second = new Date().getTime();
	var third  = randomNumberOfLength(PaymentConfigs.ORDER_ID_RANDOM_NUMBER_LENGTH);
	return first+""+second+""+third;
};

exports.generateFeeTrxnOrderId = function(){
	var first  = "ECK";
	var second = new Date().getTime()+'';
	second     = second.slice(8);
	var third  = randomNumberOfLength(FeeConfigs.FEE_ORDER_ID_RANDOM_NUMBER_LENGTH);
	return first+""+second+""+third;
};

function randomNumberOfLength(length) {
  return Math.floor(Math.pow(10, length-1) + Math.random() * (Math.pow(10, length) - Math.pow(10, length-1) - 1));
}