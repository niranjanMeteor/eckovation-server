var PAYMENT_STATUS = {
	
		CREATED     : "created",
		AUTHORIZED  : "authorized",
		CAPTURED    : "captured",
		REFUNDED    : "refunded",
		FAILED      : "failed"

};

module.exports = PAYMENT_STATUS;