var fs                                = require('fs');

var configs = {

	CONFIGS_OVERWRITE_FILE                    : "paytm_configs_overwrite.js",
	ORDER_ID_RANDOM_NUMBER_LENGTH             : 10,
	CHANNEL_ID																: "WAP",
	INDUSTRY_TYPE_ID 													: "Retail",
	MID 																			: "Eckova47889406314817",
	THEME 					  												: "merchant",
	WEBSITE 																	: "EckovationWAP",
	KEY                                       : "#F4GzKLw779fEIqM",
	RESPONSE_FILE_DIR                         : "/home/ubuntu/eckovation-server/payment",
	RESPONSE_SUCCESS_TEMPLATE_FILE            : "paytm_response.html",
	RESPONSE_ERROR_TEMPLATE_FILE              : "paytm_response_error.html",
	HOST                                      : "pguat.paytm.com",
	PROTOCOL                                  : "https",
	TRXN_STATUS_API_PATH                      : "/oltp/HANDLER_INTERNAL/TXNSTATUS"

};

var overwriteConfigFulFileName  = __dirname+'/'+configs.CONFIGS_OVERWRITE_FILE;
if(fs.existsSync(overwriteConfigFulFileName)){
	var overwriteConfig  =  require(overwriteConfigFulFileName);
	for (var key in overwriteConfig){
		configs[key] = overwriteConfig[key];
	}
} else {
	console.log('No Overwrite Configs File Found to overwrite any PAYTM payment configurations key');
}

module.exports = configs;