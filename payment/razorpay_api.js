var request                 = require('request');
var RazorpayConfigs					= require('../payment/razorpay_configs.js');

exports.capture = function(rajorpay_payment_id, amount, callback){
	var captureUrl = getTransactionCaptureUrl(rajorpay_payment_id);
	console.log('http trxnCaptureUrl request : '+captureUrl);
	request({
	  method  : 'POST',
	  url     : captureUrl,
	  form    : {
	    amount  : amount
	  }
	},function(error, response, body) {
	  console.log('Status:', response.statusCode);
	  console.log('Response:', body);
	  if(error){
	  	return callback(error,null);
	  }
	  if(response.statusCode == 200){
	  	body = JSON.parse(body);
	  	return callback(null,body);
	  }
	  return callback(body,false);
	});
};

exports.status = function(rajorpay_payment_id, callback){
	var statusUrl = getTransactionStatusUrl(rajorpay_payment_id);
	console.log('http trxnStatusUrl request : '+statusUrl);
	request(statusUrl, function(error, response, body) {
	  console.log('Status:', response.statusCode);
	  console.log('Response:', body);
	  if(error){
	  	return callback(error,null);
	  }
	  if(response.statusCode == 200){
	  	body = JSON.parse(body);
	  	return callback(null,{STATUS:body.status,RESPMSG:body.status,body:body});
	  }
	  return callback(body,null);
	});
};

function getTransactionStatusUrl(rajorpay_payment_id){
	var HOST            = RazorpayConfigs.HOST;
	var API_PATH        = RazorpayConfigs.API_PATH;
	var API_KEY         = RazorpayConfigs.API_KEY;
	var API_SECRET      = RazorpayConfigs.API_SECRET;

	var url = 'https://'+API_KEY+':'+API_SECRET+'@'+HOST+'/'+API_PATH+'/'+rajorpay_payment_id;
	return url;
}

function getTransactionCaptureUrl(rajorpay_payment_id){
	var HOST            = RazorpayConfigs.HOST;
	var API_PATH        = RazorpayConfigs.API_PATH;
	var API_KEY         = RazorpayConfigs.API_KEY;
	var API_SECRET      = RazorpayConfigs.API_SECRET;

	var url = 'https://'+API_KEY+':'+API_SECRET+'@'+HOST+'/'+API_PATH+'/'+rajorpay_payment_id+'/capture';
	return url;
}