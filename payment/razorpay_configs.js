var fs                                = require('fs');

var configs  = {

	CONFIGS_OVERWRITE_FILE                    : "razorpay_configs_overwrite.js",
	HOST                                      : "",
	API_PATH                                  : "",
	API_KEY                                   : "",
	API_SECRET                                : "",
	NAME                                      : "Eckovation",
	CURRENCY                                  : "INR",

};

var overwriteConfigFulFileName  = __dirname+'/'+configs.CONFIGS_OVERWRITE_FILE;
if(fs.existsSync(overwriteConfigFulFileName)){
	var overwriteConfig  =  require(overwriteConfigFulFileName);
	for (var key in overwriteConfig){
		configs[key] = overwriteConfig[key];
	}
} else {
	console.log('No Overwrite Configs File Found to overwrite any PAYTM payment configurations key');
}

module.exports = configs;


