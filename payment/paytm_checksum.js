var crypt                   = require('./paytm_crypt.js');
var crypto                  = require('crypto');

//mandatory flag: when it set, only mandatory parameters are added to checksum

function paramsToString(params, mandatoryflag) {
  var data = '';
  var flag = params.refund ? true : false;
  delete params.refund;
  var tempKeys = Object.keys(params);
  if(!flag){
    tempKeys.sort();
  }
  tempKeys.forEach(function (key) {
    if(key !== 'CHECKSUMHASH'){
      if(params[key] === 'null'){
        params[key] = '';
      }
      // if(!mandatoryflag || mandatoryParams.indexOf(key) !== -1) {
        data += (params[key] + '|');
      // }
    }
  });
  return data;
}


exports.genchecksum = function(params, key, cb) {
  var flag     = params.refund ? true : false;
  var data     = paramsToString(params, false);
  crypt.gen_salt(4, function (err, salt){
    if(err){
      return cb(err,null);
    }
    if(!salt){
      return cb("salt_generation_failed",null);
    }
    var sha256    = crypto.createHash('sha256').update(data + salt).digest('hex');
    var check_sum = sha256 + salt;
    var encrypted = crypt.encrypt(check_sum, key);
    // return cb(null, encodeURIComponent(encrypted));
    return cb(null, encrypted);
  });
};


exports.verifychecksum = function(params, key, cb) {
  var data = paramsToString(params, false);
  // var temp     = decodeURIComponent(params.CHECKSUMHASH);
  var temp            = params.CHECKSUMHASH;
  var checksum        = crypt.decrypt(temp, key);
  var salt            = checksum.substr(checksum.length - 4);
  var sha256          = checksum.substr(0, checksum.length - 4);
  var hash            = crypto.createHash('sha256').update(data + salt).digest('hex');
  if(hash === sha256) {
    return cb(null,true);
  } else {
    return cb(null,false);
  }
};