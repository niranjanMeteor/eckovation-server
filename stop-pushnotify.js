require('shelljs/global');
var configs           = require('./utility/configs.js');
var NUM_OF_WORKERS    = configs.NUM_PUSH_NOTIFY_WORKERS;

var command = 'service '+configs.RABBIT_PUSH_NOTIFY_SERVICE_NAME+' stop';

console.log('Stopping All Push Notify Workers');
console.log('*******************************************************');
console.log('\n');
for(var i=1; i<= NUM_OF_WORKERS; i++){
	stop_worker(command, i);
}
function stop_worker(command, i){
	var fCommand = command+' N='+i;
	exec(fCommand, function(status, output) {
		console.log('Worker : '+i);
		console.log('--------------------------------------------');
	  console.log(' Exit status:', status);
	  console.log(' Program output:', output);
	  console.log('\n');
	  return;
	});
}