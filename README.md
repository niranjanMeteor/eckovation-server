# eckovation-server

* ensuring security
* stop random stack traces, from displaying
* add authentication to Redis
* add authentication to Mongodb
* to ensure that server is always running
* to ensure that failed messages will be requeued, and re-sent
* to ensure that the delivery reports are fetched for all the OTPs that have been sent already
* to ensure that the errors being written to the log files, are mailed immediately to the admin

## Sys admin stuff
* Write scripts to check the server queues' health


## Queue Storage
1. Seen count storage method is not correct. Improve it.

## Hard Deletes
1. When you leave a group, a row in Group Member, gets removed
2. When you delete your profile, its gets hard removed from the DB

#RabbitMQ
1. install rabbitmq from https://www.rabbitmq.com/install-debian.html
2. on installation , rabbitmq server is started by default 
3. manually start rabbitmq server   ==  
     sudo service rabbitmq-server restart
4. List Users of rabbitmq  ==
      sudo rabbitmqctl list_users
5. create a rabbit user  ==  
      sudo rabbitmqctl add_user [username] [password]
6. set appropritate permisions on that user  == 
      rabbitmqctl set_permissions -p /myhost [username] [queue_name] ".*" ".*"
7. delete the guest user that is created by default on installation of rabbitmq server == 
      sudo rabbitmqctl delete_user guest
8. see status of rabbitmq server ==   
      sudo rabbitmqctl status
9. List queues in rabbitmq  ==  
      sudo rabbitmqctl list_queues

RCQ DROP PROCEDURE
=====================================
1 . For Dry Run  == "
     node Debug/unverified_accounts_rcqdrop.js direct unreal "
     
2 . For actual execution
     a) Interactively  ==  "
               node Debug/unverified_accounts_rcqdrop.js indirect real "
     b) Direct ==  "
               node Debug/unverified_accounts_rcqdrop.js direct real "
