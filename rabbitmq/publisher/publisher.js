var amqp               = require('amqplib/callback_api');
var configs             = require('../../utility/configs.js');

var user               = configs.RABBITMQ_USER;
var passwd             = configs.RABBITMQ_PASSWORD;
var rabbitmq_url       = 'amqp://'+user+':'+passwd+'@'+configs.RABBITMQ_HOST+':'+configs.RABBITMQ_PORT;
var amqpConn           = null; 

startRabbitServer();

function startRabbitServer() {
  amqp.connect(rabbitmq_url + "?heartbeat=60", function(err, conn) {
    if (err) {
      console.error("[AMQP] -> Publisher", err.message);
      return setTimeout(startRabbitServer, 1000);
    }
    conn.on("error", function(err) {
      if (err.message !== "Publisher Connection closing") {
        console.error("[AMQP] Publisher conn error", err.message);
      }
    });
    conn.on("close", function() {
      console.error("[AMQP] Publisher reconnecting");
      return setTimeout(startRabbitServer, 1000);
    });
    console.log("[AMQP] Publisher connected");
    amqpConn = conn;
    whenConnected();
  });
}

function whenConnected() {
  startPublisher();
}

var pubChannel = null;
var offlinePubQueue = [];
function startPublisher() {
  amqpConn.createConfirmChannel(function(err, ch) {
    if (closeOnErr(err)) return;
      ch.on("error", function(err) {
      console.error("[AMQP] Publisher channel error", err.message);
    });
    ch.on("close", function() {
      console.log("[AMQP] publisher channel closed");
    });

    pubChannel = ch;
    while (true) {
      var m = offlinePubQueue.shift();
      if (!m) break;
      publish(m[0], m[1], m[2]);
    }
  });
}

exports.publish = function(exchange, routingKey, content) {
  try {    
    pubChannel.publish(exchange, routingKey, content, { persistent: true },function(err, ok) {
      if(err){
        console.trace("[AMQP] Publisher publish", err);
        return offlinePubQueue.push([exchange, routingKey, content]);
        // pubChannel.connection.close();
      } else {
        // console.log('[AMQP] Publisher succesfully published message in live mode');
        // console.log(content);
        return;
      }
    });
  } catch (e) {                                                                                                                               
    console.error("[AMQP] Publisher published in Offline Mode", e.message);
    console.log(content);
    return offlinePubQueue.push([exchange, routingKey, content]);
  }
}

exports.publishOtp = function(exchange, routingKey, content, cb) {
  try {    
    pubChannel.publish(exchange, routingKey, content, { persistent: true },function(err, ok) {
      if (err) {
        return cb(err,false);
      } else {
        return cb(null,true);
      }
    });
  } catch (e) {                                                                                                                               
    return cb(e.message,false);
  }
}

exports.publishRcq = function(exchange, routingKey, content, cb) {
  try {    
    pubChannel.publish(exchange, routingKey, content, { persistent: true },function(err, ok) {
      if (err) {
        return cb(err,false);
      } else {
        return cb(null,true);
      }
    });
  } catch (e) {                                                                                                                               
    return cb(e.message,false);
  }
}

exports.publishGroupMsg = function(exchange, routingKey, content, cb) {
  try {    
    pubChannel.publish(exchange, routingKey, content, { persistent: true },function(err, ok) {
      if (err) {
        return cb(err,false);
      } else {
        return cb(null,true);
      }
    });
  } catch (e) {                                                                                                                               
    return cb(e.message,false);
  }
}

function closeOnErr(err) {
  if (!err) return false;
  console.error("[AMQP] Publisher error", err);
  amqpConn.close();
  return true;
}
