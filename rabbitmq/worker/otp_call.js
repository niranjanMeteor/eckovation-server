var amqp                  = require('amqplib/callback_api');
var mongoose              = require('mongoose');
var redis                 = require('redis');

var mail_client           = require('../../utility/mail/mail_client.js');
var configs               = require('../../utility/configs.js');
var CallGateway           = require('../../utility/call/calling_gateway.js');
var CallEngines           = require('../../utility/call/call_engines.js');
var RabbitMqPublish       = require('../publisher/publisher.js');
var RabbitMqQueues        = require('../queues.js');
var dbConnectionAttempts  = 0;

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  var subject = 'OTP CALL ME WORKER => MongoDb Connection Failure'
  var mbody = '<h4>'+ Date()  +': MongoDb Connection Failed to connect Due to Follwoing error</h4>';
  mbody += '<h5>'+err+'</h5>';
  if(dbConnectionAttempts == configs.DB_CONNECTION_RETTEMPT_LIMIT){
    informDevTeamAboutConnectionError(subject, mbody, function(err){
      process.exit(0);
    });
  } else {
    dbConnectionAttempts++;
    connectMongoDb();
  }
});
mongoose.connection.on('connected', function(){
  console.log('Successfully Opened MOngoDb Connection');
  dbConnectionAttempts = 0;
});

require('../../models/otpcallstatuses.js');
require('../../models/otpcalls.js');
var OtpCallStatus               = mongoose.model('OtpCallStatus');
var OtpCall                     = mongoose.model('OtpCall');

var user                        = configs.RABBITMQ_USER;
var passwd                      = configs.RABBITMQ_PASSWORD;
var rabbitmq_url                = 'amqp://'+user+':'+passwd+'@'+configs.RABBITMQ_HOST+':'+configs.RABBITMQ_PORT;
var amqpConn                    = null;
var otp_call_me_queue           = RabbitMqQueues.OTP_CALL_ME; 
var otp_call_me_attempts_limit  = configs.OTP_CALL_ME_DELIVERY_ATTEMPTS_LIMIT;
var otp_call_me_reattempt_delay = configs.OTP_CALL_ME_DELIVERY_REATTEMPT_DELAY;

startRabbitServer();

function startRabbitServer() {
  amqp.connect(rabbitmq_url + "?heartbeat=60", function(err, conn) {
    if (err) {
      console.error("[AMQP] Otp Call Me Delivery Worker", err.message);
      return setTimeout(startRabbitServer, 1000);
    }
    conn.on("error", function(err) {
      if (err.message !== "Connection closing") {
        console.error("[AMQP] Otp Call Me Worker conn error", err.message);
      }
    });
    conn.on("close", function() {
      console.error("[AMQP] reconnecting");
      return setTimeout(startRabbitServer, 1000);
    });
    console.log("[AMQP] Otp Call Me Worker connected");
    amqpConn = conn;
    whenConnected();
  });
}

function whenConnected() {
  startWorker();
}

function startWorker() {
  amqpConn.createChannel(function(err, ch) {
    if (closeOnErr(err)) return;
    ch.on("error", function(err) {
      console.error("[AMQP] Otp call_me Worker channel error", err.message);
    });
    ch.on("close", function() {
      console.log("[AMQP] Otp call_me worker  channel closed");
    });
    
    ch.prefetch(10);
    ch.assertQueue(otp_call_me_queue, { durable: true }, function(err, _ok) {
      if (closeOnErr(err)) return;
      ch.consume(otp_call_me_queue, processMsg, { noAck: false });
      console.log("[AMQP] Otp call_me Worker is started");
    });

    function processMsg(msg) {
      work(msg, function(ok) {
        if (ok)
          ch.ack(msg);
        else
          ch.reject(msg, true);
      });
    }
  });
}

function closeOnErr(err) {
  if (!err) return false;
  console.error("[AMQP] Otp Call Me Worker error", err);
  amqpConn.close();
  return true;
}

function work(msg, cb) {
  var call_obj    = JSON.parse(msg.content);
  var otp_call_id = call_obj.oclid;
  var otp_id      = call_obj.otp_id;
  var phone       = call_obj.m;
  var ccode       = call_obj.cc;
  var aid         = call_obj.aid;
  var call_engine;
  var subject;
  if(call_obj.atmpt > otp_call_me_attempts_limit){
    console.log('Otp Call Object has reached its max attempt limit and hence will not be republished');
    console.log(call_obj);
    console.log('Informing Dev Team about Otp Call Not Found Error');
    subject = 'Maximum Otp Call Me delivery attempts reached for : '+phone;
    informDevTeam(call_obj, subject);
    return cb(true);
  }
  if(ccode == '91'){
    call_engine = CallEngines.KOOKOO;
  } else {
    call_engine = CallEngines.NEXMO;
  }
  CallGateway.caller(
    call_engine, 
    phone, ccode, 
    call_obj.code, 
    function(data, was_queued){
      console.log('Caller output from engines are');
      console.log(was_queued);
      console.log(data);
      if(was_queued){
        OtpCall.update({
          _id : otp_call_id
        },{
          atmp : call_obj.atmp,
          ctim : call_obj.tim,
          uat  : new Date(parseInt(call_obj.tim))
        },function(err,raw){
          if(err){ 
            console.log("Failed to update the attempts for OTP call id"); 
            console.log(call_obj); 
            console.trace(err);
            return cb(true);
          }
          updateOtpCallResp(otp_call_id, otp_id, aid, call_engine, data, function(err,resp){
            if(err){
              console.trace(err);
            }
            console.log('Successfully generated otp call for otp call object');
            console.log(call_obj);
            return cb(true);
          });
        });
      } else {
        updateOtpCallResp(otp_call_id, otp_id, aid, call_engine, data, function(err,resp){
          if(err){
            console.trace(err);
          }
          console.trace("was not able to generate call via gateway. response : " + data);
          console.log(call_obj);
          console.log('Informing Dev Team about Otp Call Me Not Found Error');
          console.log('The Otp Call obj will be processed again after some delay again');
          subject = 'CALL Gateway Unable to send otp for : '+phone;
          informDevTeam(call_obj, subject);
          republishOtpWithDelay(call_obj);
          return cb(true);
        });
      }
    }
  );
}

// function updateOtpCallStatus(otp_call_id, otp_id, numOfEngns, engn_resp_obj, cb){
//   var done = 0;
//   for(var key in engn_resp_obj){
//     updateOtpCallResp(otp_call_id, otp_id, key, engn_resp_obj[key],function(err,resp){
//       if(err){
//         return cb(err,null);
//       }
//       done++;
//       if(done == numOfEngns){
//         return cb(null,true);
//       }
//     });
//   }
// }

function updateOtpCallResp(otp_call_id, otp_id, aid, engn, resp, cb){
  var new_otp_call_status_obj = new OtpCallStatus({
    oclid : otp_call_id,
    otpid : otp_id,
    aid   : aid,
    engn  : engn,
    resp  : resp
  });
  new_otp_call_status_obj.save(function(err,created_obj){
    if(err){
      return cb(err,null);
    }
    if(created_obj){
      return  cb(null,true);
    }
    return cb(true,false);
  });
}

function republishOtpWithDelay(call_obj){
  setTimeout(function(){
    call_obj.atmpt = parseInt(call_obj.atmpt)+1;
    var re_call_obj = new Buffer(JSON.stringify(call_obj));
    RabbitMqPublish.publishOtp("",RabbitMqQueues.OTP_CALL_ME,re_call_obj,function(err,resp){
      if(err) {
        console.trace(err);
        console.log('Otp Call Obj could not be RePublished for delivery for mobile number : '+call_obj.m);
        return;
      }
      if(resp){
        console.log('Successfully RePublished Otp Call Obj message for mobile number : '+call_obj.m);
      } else {
        console.trace('Otp Call Obj could not be published for delivery for mobile number : '+call_obj.m+' , response is : '+resp);
      }
      return;
    });
  },
    otp_call_me_reattempt_delay
  );
}

function informDevTeam(call_obj, subject){
  var html_string = '<h4>'+ Date()  +': Otp Call Me Failure for : '+call_obj.m+' </h4>';
  html_string += '<h5>'+JSON.stringify(call_obj)+'</h5>';
  var mailOptions = {
    from    : configs.OTP_FROM,
    to      : configs.TECH_TO,
    subject : subject, 
    html    : html_string
  };
  mail_client.send(mailOptions, function(err, response){
    if (err) {
      console.trace(err);
      console.log('Informing Dev Team: Mail could be not sent');
    } else {
      console.log('Informing Dev Team: Mail sent succesfully');
    }
  });
}

function informDevAndMrktTeam(call_obj, subject){
  var html_string = '<h4>'+ Date()  +': Otp Call Me Failure for : '+call_obj.m+' </h4>';
  html_string += '<h5>'+JSON.stringify(call_obj)+'</h5>';
  var mailOptions = {
    from    : configs.OTP_FROM, 
    to      : configs.ANALYTICS_TO, 
    subject : subject, 
    html    : html_string
  };
  mail_client.send(mailOptions, function(err, response){
    if (err) {
      console.trace(err);
      console.log('Informing Dev and Mrkt Team: Mail could be not sent');
    } else {
      console.log('Informing Dev and Mrkt Team: Mail sent succesfully');
    }
  });
}

function informDevTeamAboutConnectionError(subject, body, cb){
  var mailOptions = {
    from    : configs.RABBITMQ_WORKER_FROM,
    to      : configs.TECH_TO,
    subject : subject, 
    html    : body
  };
  mail_client.send(mailOptions, function(err, response){
    if (err) {
      console.trace(err);
      console.log('Informing Dev Team: Mail could be not sent');
      return cb(err);
    } else {
      console.log('Informing Dev Team: Mail sent succesfully');
      return cb(null);
    }
  });
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : configs.MONGO_SERVER_KEEPALIVE
      }
    }
  };
  if(!configs.MONGO_UNAME || configs.MONGO_UNAME == ""){
    options.server.poolSize = configs.MONGO_LOWER_POOLSIZE;
    console.log('[[[[[[[    NO MONGO_UNAME FOUND IN CONFIGS, WILL CONNNECT WITHOUT REPLICA OPTIONS     ]]]]]]]]]');
    console.log('[[[[[[[    MONGODB CONNECTION POOL_SIZE : '+configs.MONGO_LOWER_POOLSIZE+'     ]]]]]]]]]');
    return mongoose.connect("mongodb://"+configs.MONGO_HOST+":"+configs.MONGO_PORT+"/"+configs.DB_NAME,options);
  }
  options.replset = {
    rs_name : configs.MONGO_REPLICA_SET_NAME,
    socketOptions : {
      keepAlive : configs.MONGO_REPLICA_KEEPALIVE
    },
    poolSize : configs.MONGO_LOWER_POOLSIZE
  };
  var connect_string = "mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@";
  var mongo_hosts    = configs.MONGO_HOST;
  var mongo_port     = configs.MONGO_PORT;
  for(var i=0; i<mongo_hosts.length; i++){
    if(i > 0)
      connect_string += ",";
    connect_string += mongo_hosts[i]+":"+mongo_port;
  }
  connect_string += "/"+configs.DB_NAME;
  console.log('[[[[[[[     CONNECTING MONGODB WITH REPLICA_SET OPTIONS     ]]]]]]]]]');
  console.log('[[[[[[[     MONGODB CONNECTION POOL_SIZE : '+configs.MONGO_LOWER_POOLSIZE+'      ]]]]]]]]]');
  return mongoose.connect(connect_string,options);
}
