var amqp                = require('amqplib/callback_api');
var redis               = require('redis');
var AWS                 = require('aws-sdk');
var fs                  = require('fs');
var https               = require('https');
var ffmpeg              = require('fluent-ffmpeg');

var configs             = require('../../utility/configs.js');
var RedisPrefixes       = require('../../utility/redis_prefix');
var mail_client         = require('../../utility/mail/mail_client.js');

var redis_client        = redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
  console.trace("Redis Connection Error " + err);
  var subject = 'CONVERT MEDIA WORKER => Redis COnnection Failure'
  var rbody = '<h4>'+ Date()  +': Redis Connection Failed to connect Due to Follwoing error</h4>';
  rbody += '<h5>'+err+'</h5>';
  informDevTeam(subject, rbody, function(err){
    process.exit(0);
  });
});

var RabbitMqQueues          = require('../queues.js');
var rrq                     = require("../../utility/redis_rabbit_queue.js")(redis_client);
var rhmset                  = require("../../utility/redis_hmset.js")(redis_client);

var user                    = configs.RABBITMQ_USER;
var passwd                  = configs.RABBITMQ_PASSWORD;
var rabbitmq_url            = 'amqp://'+user+':'+passwd+'@'+configs.RABBITMQ_HOST+':'+configs.RABBITMQ_PORT;
var amqpConn                = null;
var media_queue             = RabbitMqQueues.MEDIA_CONVERSION;

var PENDING_ANDROID_DIR     = configs.MEDIA_ANDROID_FILE_PENDING_DIR;
var PENDING_IOS_DIR         = configs.MEDIA_IOS_FILE_PENDING_DIR;
var DONE_ANDROID_DIR        = configs.MEDIA_ANDROID_FILE_DONE_DIR;
var DONE_IOS_DIR            = configs.MEDIA_IOS_FILE_DONE_DIR;

AWS.config.update({
  accessKeyId: configs.AWS_ACCESS_KEY,
  secretAccessKey: configs.AWS_SECRET_KEY,
  "region": configs.AWS_REGION,
  "output" : configs.AWS_OUTPUT 
});
var s3 = new AWS.S3();

startRabbitMediaServer();

function startRabbitMediaServer() {
  amqp.connect(rabbitmq_url + "?heartbeat=60", function(err, conn) {
    if (err) {
      console.error("[AMQP] Media Worker", err.message);
      return setTimeout(startRabbitMediaServer, 1000);
    }
    conn.on("error", function(err) {
      if (err.message !== "Connection closing") {
        console.error("[AMQP] Media Worker conn error", err.message);
      }
    });
    conn.on("close", function() {
      console.error("[AMQP] Media Worker reconnecting");
      return setTimeout(startRabbitMediaServer, 1000);
    });
    console.log("[AMQP] Media Worker connected");
    amqpConn = conn;
    whenConnected();
  });
}

function whenConnected() {
  startWorker();
}

function startWorker() {
  amqpConn.createChannel(function(err, ch) {
    if (closeOnErr(err)) return;
    ch.on("error", function(err) {
      console.error("[AMQP] Media Worker channel error", err.message);
    });
    ch.on("close", function() {
      console.log("[AMQP] Media worker  channel closed");
    });
    
    ch.prefetch(10);
    ch.assertQueue(media_queue, { durable: true }, function(err, _ok) {
      if (closeOnErr(err)) return;
      ch.consume(media_queue, processMsg, { noAck: false });
      console.log("[AMQP] Media Worker is started");
    });

    function processMsg(msg) {
      work(msg, function(ok) {
        if (ok)
          ch.ack(msg);
        else
          ch.reject(msg, true);
      });
    }
  });
}

function closeOnErr(err) {
  if (!err) return false;
  console.error("[AMQP] Media Worker error", err);
  amqpConn.close();
  return true;
}

function work(msg, cb) {
  var in_file, out_file;
  var msgOb = JSON.parse(msg.content);
  var mask_url = msgOb.body;
  var splits = mask_url.split("/");
  var filename = splits[splits.length-1];
  var nameExtSplits = filename.split(".");
  var nameExt = nameExtSplits[1];
  var params = {
    Bucket: configs.BUCKET_NAME_CHAT_PIC, 
    Key: filename,
    Expires : configs.AMAZON_EXPIRY_TIME
  };
  s3.getSignedUrl('getObject', params, function (err, download_url) {
    if(err || !download_url){
      console.log('RabbitMQ Media Worker : error in media conversion for packet');
      console.log(msgOb);
      console.log(err);
      addIntoMediaConversionPending(msgOb.mid, msgOb.tim);
      removeFromMediaConversionJob(msgOb);
      return cb(true);
    }
    if(nameExt == '3gp'){
      in_file  = PENDING_IOS_DIR+'/'+filename;
      out_file = DONE_IOS_DIR+'/'+nameExtSplits[0]+'.m4a';
    } else if(nameExt == 'm4a'){
      in_file  = PENDING_ANDROID_DIR+'/'+filename;
      out_file = DONE_ANDROID_DIR+'/'+nameExtSplits[0]+'.3gp';
    }
    var file = fs.createWriteStream(in_file);
    https.get(download_url, function(response) {
      console.log('File Fetching from S3 server, filename : '+in_file+', response:  '+ response.statusCode);
      response.pipe(file);
    }).on('error', function(e) {
      console.log('File Fetching from S3 server, filename : '+in_file+',Got error: ' + e.message);
      addIntoMediaConversionPending(msgOb.mid, msgOb.tim);
      removeFromMediaConversionJob(msgOb)
      return cb(true);
    });
    file.on('finish', function(){
      console.log('Succesfully File Fetched from aws S3 server : '+in_file);
      var nameExtSplits = filename.split(".");
      var nameExt = nameExtSplits[1];
      var out_filename;
      if(nameExt == '3gp'){
        out_filename = nameExtSplits[0]+'.m4a';
        andriodToIos(in_file, out_file, out_filename, function(err,resp){
          if(resp){
            console.log('Succesfull converison of media file for android and ios done');
            console.log(msgOb);
          } else {
            console.log('Failure for converison of media file for android and ios');
            console.log(msgOb);
            addIntoMediaConversionPending(msgOb.mid, msgOb.tim);
          }
          removeFromMediaConversionJob(msgOb);
          return cb(true);
        });
      } else if(nameExt == 'm4a'){
        out_filename = nameExtSplits[0]+'.3gp';
        iosToAndroid(in_file, out_file, out_filename, function(err,resp){
          if(resp){
            console.log('Succesfull converison of media file for android and ios done');
            console.log(msgOb);
          } else {
            console.log('Failure for converison of media file for android and ios');
            console.log(msgOb)
            addIntoMediaConversionPending(msgOb.mid, msgOb.tim);
          }
          removeFromMediaConversionJob(msgOb);
          return cb(true);
        });
      }
    });
  });
}

function andriodToIos(input, output, filename, cb){
  var format = 'audio/m4a';
  ffmpeg(input)
    .audioCodec('aac')
    .audioFrequency(22050)
    .on('error', function(err) {
      console.log('convertVideoToAudio : Failed for input filename : '+filename);
      console.log('An error occurred: ' + err.message);
      return cb(null,false);
    })
    .on('end', function() {
      console.log('Processing finished ! for file name : '+filename);
      uploadFileToAws(filename, format, output, input, cb);
    })
    .save(output);
}

function iosToAndroid(input, output, filename, cb){
  var format = 'audio/3gp';
  ffmpeg(input)
    .audioCodec('aac')
    .audioFrequency(22050)
    .on('error', function(err) {
      console.log('convertVideoToAudio : Failed for input filename : '+filename);
      console.log('An error occurred: ' + err.message);
      return cb(null,false);
    })
    .on('end', function() {
      console.log('Processing finished ! for file name : '+filename);
      uploadFileToAws(filename, format, output, input, cb);
    })
    .save(output);
}

// function convertAudioToVideo(input, output, filename, cb){
//   var format = 'audio/3gp';
//   ffmpeg(input)
//     .videoCodec('libx264')
//     .videoBitrate(1000)
//     .preset('divx')
//     .on('error', function(err) {
//       console.log('convertAudioToVideo : Failed for input filename : '+filename);
//       console.log('An error occurred: ' + err.message);
//       return cb(null,false);
//     })
//     .on('end', function() {
//       console.log('Processing finished ! for file name : '+filename);
//       uploadFileToAws(filename, format, output, input, cb);
//     })
//     .save(output);
// }

// function convertVideoToAudio(input, output, filename, cb){
//   var format = 'audio/m4a';
//   ffmpeg(input)
//     .audioCodec('libmp3lame')
//     // .audioCodec('libfaac')
//     .audioFrequency(22050)
//     .on('error', function(err) {
//       console.log('convertVideoToAudio : Failed for input filename : '+filename);
//       console.log('An error occurred: ' + err.message);
//       return cb(null,false);
//     })
//     .on('end', function() {
//       console.log('Processing finished ! for file name : '+filename);
//       uploadFileToAws(filename, format, output, input, cb);
//     })
//     .save(output);
// }

function uploadFileToAws(filename, format, filepath, input_path, cb){
  var params = {
    Bucket: configs.BUCKET_NAME_CHAT_PIC, 
    Key: filename,
    Body : fs.createReadStream(filepath),
    ACL: 'authenticated-read',
    ContentType: format,
  };
  s3.upload(params, function (err, data) {
    if(err){
      console.log('uploadFileToAws : Failed for input filename : '+filename);
      console.log(err);
      return cb(null,false);
    } 
    console.log('Successfully uploaded to S3 the media converted file : '+filename);
    deleteFile(input_path);
    deleteFile(filepath);
    return cb(null,true);
  });
}

function removeFromMediaConversionJob(msgOb){
  rhmset.removekey(RedisPrefixes.MEDIA_CONVERSION_JOB+msgOb.mid,function(err,resp){
    if(resp) {
      console.log('RabbitMQ Media Worker : Successfully removed from rrq ');
      console.log(msgOb);
      console.log(resp);
    } else {
      console.log('RabbitMQ Media Worker : could not remove from rrq ');
      console.log(msgOb);
    }
    return;
  });
}

function addIntoMediaConversionPending(mid, tim){
  rrq.addWithoutPrefix(RedisPrefixes.MEDIA_CONVERSION_PENDING, tim, mid, function(err,added){
    if(err) { console.trace(err); return; }
    if(added) { console.log('addIntoStatusUpdatePending : added msg for status pending update : '+mid); }
    return;
  });
}

function deleteFile(filepath){
  fs.unlink(filepath,function(err,resp){
    if(err) {
      console.log('could not remove file : '+filepath+' , after media conversion');
    } else {
      console.log('successfully deleted file : '+filepath+' , after media conversion');
    }
  });
}

function informDevTeam(subject, body, cb){
  var mailOptions = {
    from    : configs.RABBITMQ_WORKER_FROM,
    to      : configs.TECH_TO,
    subject : subject, 
    html    : body
  };
  mail_client.send(mailOptions, function(err, response){
    if (err) {
      console.trace(err);
      console.log('Informing Dev Team: Mail could be not sent');
      return cb(err);
    } else {
      console.log('Informing Dev Team: Mail sent succesfully');
      return cb(null);
    }
  });
}

// var in_file  = PENDING_ANDROID_DIR+'/'+'test13.m4a';
// var out_file = DONE_ANDROID_DIR+'/'+'test13.3gp';
// var in_file  = PENDING_IOS_DIR+'/'+'test14.3gp';
// var out_file = DONE_IOS_DIR+'/'+'test14.m4a';
// convertAudioToVideoW(in_file, out_file);
// function convertAudioToVideoW(input, output){
//   ffmpeg(input)
//     // .audioCodec('libmp3lame')
//     .audioCodec('aac')
//     .audioFrequency(22050)
//     .on('error', function(err) {
//       console.log('An error occurred: ' + err.message);
//     })
//     .on('end', function() {
//       console.log('Processing finished ! for file name : ');
//     })
//     .save(output);
// }
