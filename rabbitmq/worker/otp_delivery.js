var amqp                = require('amqplib/callback_api');
var mongoose            = require('mongoose');
var redis               = require('redis');
var _                   = require('underscore');

var mail_client         = require('../../utility/mail/mail_client.js');
var configs             = require('../../utility/configs.js');
var SmsGateway          = require('../../utility/sms/sms_gateway.js');
var SmsEngines          = require('../../utility/sms/sms_engines.js');
var RabbitMqPublish     = require('../publisher/publisher.js');
var RabbitMqQueues      = require('../queues.js');
var dbConnectionAttempts= 0;

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  var subject = 'OTP WORKER => MongoDb Connection Failure'
  var mbody = '<h4>'+ Date()  +': MongoDb Connection Failed to connect Due to Follwoing error</h4>';
  mbody += '<h5>'+err+'</h5>';
  if(dbConnectionAttempts == configs.DB_CONNECTION_RETTEMPT_LIMIT){
    informDevTeamAboutConnectionError(subject, mbody, function(err){
      process.exit(0);
    });
  } else {
    dbConnectionAttempts++;
    connectMongoDb();
  }
});
mongoose.connection.on('connected', function(){
  console.log('Successfully Opened MOngoDb Connection');
  dbConnectionAttempts = 0;
});

require('../../models/accounts.js');
require('../../models/otps.js');
require('../../models/otpstatuses.js');

var Account             = mongoose.model('Account');
var Otp                 = mongoose.model('Otp');
var OtpStatus           = mongoose.model('OtpStatus');

var user                = configs.RABBITMQ_USER;
var passwd              = configs.RABBITMQ_PASSWORD;
var rabbitmq_url        = 'amqp://'+user+':'+passwd+'@'+configs.RABBITMQ_HOST+':'+configs.RABBITMQ_PORT;
var amqpConn            = null;
var otp_delivery_queue  = RabbitMqQueues.OTP_DELIVERY; 
var otp_delivery_attempts_limit = configs.OTP_DELIVERY_ATTEMPTS_LIMIT;
var sms_otp_reattempt_delay     = configs.SMS_OTP_REATTEMPT_DELAY;
var sms_otp_template    = configs.SMS_OTP_TEMPLATE;

startRabbitServer();

function startRabbitServer() {
  amqp.connect(rabbitmq_url + "?heartbeat=60", function(err, conn) {
    if (err) {
      console.error("[AMQP] Otp Delivery Worker", err.message);
      return setTimeout(startRabbitServer, 1000);
    }
    conn.on("error", function(err) {
      if (err.message !== "Connection closing") {
        console.error("[AMQP] Otp Delivery Worker conn error", err.message);
      }
    });
    conn.on("close", function() {
      console.error("[AMQP] reconnecting");
      return setTimeout(startRabbitServer, 1000);
    });
    console.log("[AMQP] Otp Delivery Worker connected");
    amqpConn = conn;
    whenConnected();
  });
}

function whenConnected() {
  startWorker();
}

function startWorker() {
  amqpConn.createChannel(function(err, ch) {
    if (closeOnErr(err)) return;
    ch.on("error", function(err) {
      console.error("[AMQP] Otp Delivery Worker channel error", err.message);
    });
    ch.on("close", function() {
      console.log("[AMQP] Otp Delivery worker  channel closed");
    });
    
    ch.prefetch(10);
    ch.assertQueue(otp_delivery_queue, { durable: true }, function(err, _ok) {
      if (closeOnErr(err)) return;
      ch.consume(otp_delivery_queue, processMsg, { noAck: false });
      console.log("[AMQP] Otp Delivery Worker is started");
    });

    function processMsg(msg) {
      work(msg, function(ok) {
        if (ok)
          ch.ack(msg);
        else
          ch.reject(msg, true);
      });
    }
  });
}

function closeOnErr(err) {
  if (!err) return false;
  console.error("[AMQP] Otp Delivery Worker error", err);
  amqpConn.close();
  return true;
}

function work(msg, cb) {
  var otp_obj = JSON.parse(msg.content);
  console.log('sms packet is');
  console.log(otp_obj);
  var subject;
  if(otp_obj.atmpt > otp_delivery_attempts_limit){
    console.log('Otp Object has reached its max attempt limit and hence will not be republished');
    console.log(otp_obj);
    console.log('Informing Dev Team about Otp Not Found Error');
    subject = 'Maximum Otp delivery attempts reached for : '+otp_obj.m;
    informDevAndMrktTeam(otp_obj, subject);
    return cb(true);
  }
  Otp.findOne({
    _id : otp_obj._id
  },function(err,otp){
    if(err) { 
      console.trace("Error occurred in querying Otp from database"); 
      console.log('The otp obj will be processed again after some delay');
      console.log(otp_obj);
      republishOtpWithDelay(otp_obj);
      return cb(true);
    }
    if(!otp) { 
      console.trace("Invalid OTP ID found in the queue");
      console.log('The otp obj will not be processed again after some delay');
      console.log(otp_obj); 
      console.log('Informing Dev Team about Otp Not Found Error');
      subject = 'Otp Object Id not found for : '+otp_obj.m;
      informDevTeam(otp_obj, subject);
      return cb(true);
    }
    var compiled  = _.template(sms_otp_template);
    var message   = compiled({code: otp_obj.code});

    var sms_engine;
    if(otp_obj.ccod == '91' && otp_obj.atmpt == 0){
      sms_engine = SmsEngines.MSG_91;
      otp_obj.ENGINE = "MSG91";
    } else {
      sms_engine = SmsEngines.NEXMO;
      otp_obj.ENGINE = "NEXMO";
    }
    var sent = 0;
    SmsGateway.sender(
      otp_obj._id,
      sms_engine,
      otp_obj.ccod,
      otp_obj.m,
      otp_obj.code,
      message,
      function(data, was_queued){
        console.log('SMS output from engines are');
        console.log(was_queued);
        console.log(data);
        if(was_queued){
          Otp.update({
            _id : otp_obj._id
          },{
            smst : Otp.SMS_STATUS.QUEUED
          },function(err,raw){
            if(err) { 
              console.log("Failed to update the OTP Code status to [NOT_SENT]"); 
              console.log(otp_obj); 
              console.trace(err);
              return cb(true);
            }
            updateOtpResp(otp_obj._id, otp_obj.aid, sms_engine, "success", data, function(err,resp){
              if(err){
                console.trace(err);
              }
              console.log('Successfully sent the sms for otp object');
              console.log(otp_obj);
              return cb(true);
            });
          });
        } else {
          updateOtpResp(otp_obj._id, otp_obj.aid, sms_engine, "failure", data, function(err,resp){
            if(err){
              console.trace(err);
            }
            console.trace("was not able to send sms via gateway. response : " + data);
            console.log(otp_obj);
            console.log('Informing Dev Team about Otp Not Found Error');
            console.log('The Otp obj will be processed again after some delay again');
            subject = 'SMS Gateway Unable to send otp for : '+otp_obj.m;
            informDevTeam(otp_obj, subject);
            republishOtpWithDelay(otp_obj);
            return cb(true);
          });
        }
      }
    );
  });
}

function updateOtpResp(otp_id, aid, engn, status, resp, cb){
  var respData;
  if(typeof(resp) != 'object'){
    respData = {
      data : resp
    };
  } else {
    respData = resp;
  }
  var new_otp_status_obj = new OtpStatus({
    oid  : otp_id,
    aid  : aid,
    engn : engn,
    stts : status,
    resp : respData
  });
  new_otp_status_obj.save(function(err,created_obj){
    if(err){
      return cb(err,null);
    }
    if(created_obj){
      return  cb(null,true);
    }
    return cb(true,false);
  });
}

function republishOtpWithDelay(otp_obj){
  setTimeout(function(){
    otp_obj.atmpt = parseInt(otp_obj.atmpt)+1;
    var re_otp_obj = new Buffer(JSON.stringify(otp_obj));
    RabbitMqPublish.publishOtp("",RabbitMqQueues.OTP_DELIVERY,re_otp_obj,function(err,resp){
      if(err) {
        console.trace(err);
        console.log('Otp could not be RePublished for delivery for mobile number : '+otp_obj.m);
        return;
      }
      if(resp){
        console.log('Successfully RePublished Otp message for mobile number : '+otp_obj.m);
      } else {
        console.trace('Otp could not be published for delivery for mobile number : '+otp_obj.m+' , response is : '+resp);
      }
      return;
    });
  },
    sms_otp_reattempt_delay
  );
}

function informDevTeam(otp_obj, subject){
  if(process.env.NODE_ENV == "ci" || process.env.NODE_ENV == "debug" || process.env.NODE_ENV == "dapi"){
    return;
  }
  var html_string = '<h4>'+ Date()  +': Otp Failure for : '+otp_obj.m+' </h4>';
  html_string += '<h5>'+JSON.stringify(otp_obj)+'</h5>';
  var mailOptions = {
    from    : configs.OTP_FROM,
    to      : configs.TECH_TO,
    subject : subject, 
    html    : html_string
  };
  mail_client.send(mailOptions, function(err, response){
    if (err) {
      console.trace(err);
      console.log('Informing Dev Team: Mail could be not sent');
    } else {
      console.log('Informing Dev Team: Mail sent succesfully');
    }
  });
}

function informDevAndMrktTeam(otp_obj, subject){
  if(process.env.NODE_ENV == "ci" || process.env.NODE_ENV == "debug" || process.env.NODE_ENV == "dapi"){
    return;
  }
  var html_string = '<h4>'+ Date()  +': Otp Failure for : '+otp_obj.m+' </h4>';
  html_string += '<h5>'+JSON.stringify(otp_obj)+'</h5>';
  var mailOptions = {
    from    : configs.OTP_FROM, 
    to      : configs.ANALYTICS_TO, 
    subject : subject, 
    html    : html_string
  };
  mail_client.send(mailOptions, function(err, response){
    if (err) {
      console.trace(err);
      console.log('Informing Dev and Mrkt Team: Mail could be not sent');
    } else {
      console.log('Informing Dev and Mrkt Team: Mail sent succesfully');
    }
  });
}

function informDevTeamAboutConnectionError(subject, body, cb){
  var mailOptions = {
    from    : configs.RABBITMQ_WORKER_FROM,
    to      : configs.TECH_TO,
    subject : subject, 
    html    : body
  };
  mail_client.send(mailOptions, function(err, response){
    if (err) {
      console.trace(err);
      console.log('Informing Dev Team: Mail could be not sent');
      return cb(err);
    } else {
      console.log('Informing Dev Team: Mail sent succesfully');
      return cb(null);
    }
  });
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : configs.MONGO_SERVER_KEEPALIVE
      }
    }
  };
  if(!configs.MONGO_UNAME || configs.MONGO_UNAME == ""){
    options.server.poolSize = configs.MONGO_POOLSIZE;
    console.log('[[[[[[[    NO MONGO_UNAME FOUND IN CONFIGS, WILL CONNNECT WITHOUT REPLICA OPTIONS     ]]]]]]]]]');
    console.log('[[[[[[[    MONGODB CONNECTION POOL_SIZE : '+configs.MONGO_POOLSIZE+'     ]]]]]]]]]');
    return mongoose.connect("mongodb://"+configs.MONGO_HOST+":"+configs.MONGO_PORT+"/"+configs.DB_NAME,options);
  }
  options.replset = {
    rs_name : configs.MONGO_REPLICA_SET_NAME,
    socketOptions : {
      keepAlive : configs.MONGO_REPLICA_KEEPALIVE
    },
    poolSize : configs.MONGO_LOWER_POOLSIZE
  };
  var connect_string = "mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@";
  var mongo_hosts    = configs.MONGO_HOST;
  var mongo_port     = configs.MONGO_PORT;
  for(var i=0; i<mongo_hosts.length; i++){
    if(i > 0)
      connect_string += ",";
    connect_string += mongo_hosts[i]+":"+mongo_port;
  }
  connect_string += "/"+configs.DB_NAME;
  console.log('[[[[[[[     CONNECTING MONGODB WITH REPLICA_SET OPTIONS     ]]]]]]]]]');
  console.log('[[[[[[[     MONGODB CONNECTION POOL_SIZE : '+configs.MONGO_LOWER_POOLSIZE+'      ]]]]]]]]]');
  return mongoose.connect(connect_string,options);
}
