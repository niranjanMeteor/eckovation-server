var amqp                  = require('amqplib/callback_api');
var mongoose              = require('mongoose');
var redis                 = require('redis');

var RabbitMqQueues        = require('../queues.js');
var configs               = require('../../utility/configs.js');
var RedisPrefixes         = require('../../utility/redis_prefix');
var UserInfoCache         = require('../../utility/user_info_cache.js');
var Events                = require('../../utility/event_names.js');
var mail_client           = require('../../utility/mail/mail_client.js');
var dbConnectionAttempts  = 0;

var redis_client        = redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
  console.trace("Redis Connection Error " + err);
  var subject = 'EMIT RCQ WORKER => Redis COnnection Failure'
  var rbody = '<h4>'+ Date()  +': Redis Connection Failed to connect Due to Follwoing error</h4>';
  rbody += '<h5>'+err+'</h5>';
  informDevTeam(subject, rbody, function(err){
    process.exit(0);
  });
});

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  var subject = 'EMIT RCQ WORKER => MongoDb Connection Failure'
  var mbody = '<h4>'+ Date()  +': MongoDb Connection Failed to connect Due to Follwoing error</h4>';
  mbody += '<h5>'+err+'</h5>';
  if(dbConnectionAttempts == configs.DB_CONNECTION_RETTEMPT_LIMIT){
    informDevTeam(subject, mbody, function(err){
      process.exit(0);
    });
  } else {
    dbConnectionAttempts++;
    connectMongoDb();
  }
});
mongoose.connection.on('connected', function(){
  console.log('Successfully Opened MOngoDb Connection');
  dbConnectionAttempts = 0;
});

var rcq                 = require("../../utility/redis_chat_queue.js")(redis_client);
var rmc                 = require("../../utility/redis_mongo_cache.js")(redis_client);
var rhmset              = require("../../utility/redis_hmset.js")(redis_client);
var io                  = require('socket.io-emitter')(redis_client);


require('../../models/groupmembers.js');
require('../../models/accounts.js');
require('../../models/messages.js');
var GroupMember         = mongoose.model('GroupMember');
var Account             = mongoose.model('Account');
var Message             = mongoose.model('Message');


var user                = configs.RABBITMQ_USER;
var passwd              = configs.RABBITMQ_PASSWORD;
var rabbitmq_url        = 'amqp://'+user+':'+passwd+'@'+configs.RABBITMQ_HOST+':'+configs.RABBITMQ_PORT;
var amqpConn            = null;
var emit_rcq_queue      = RabbitMqQueues.EMIT_RCQ;
var ADMIN_SEEN          = "ad_seen";
var NORMAL_MSG_TYPES    = [Message.MESSAGE_TYPE.audio,Message.MESSAGE_TYPE.text,Message.MESSAGE_TYPE.video,Message.MESSAGE_TYPE.image];


startRabbitServer();

function startRabbitServer() {
  amqp.connect(rabbitmq_url + "?heartbeat=60", function(err, conn) {
    if (err) {
      console.error("[AMQP] EMIT_RCQ Worker", err.message);
      return setTimeout(startRabbitServer, 1000);
    }
    conn.on("error", function(err) {
      if (err.message !== "Connection closing") {
        console.error("[AMQP] EMIT_RCQ Worker conn error", err.message);
      }
    });
    conn.on("close", function() {
      console.error("[AMQP] reconnecting");
      return setTimeout(startRabbitServer, 1000);
    });
    console.log("[AMQP] EMIT_RCQ Worker connected");
    amqpConn = conn;
    whenConnected();
  });
}

function whenConnected() {
  startWorker();
}

function startWorker() {
  amqpConn.createChannel(function(err, ch) {
    if (closeOnErr(err)) return;
    ch.on("error", function(err) {
      console.error("[AMQP] EMIT_RCQ Worker channel error", err.message);
    });
    ch.on("close", function() {
      console.log("[AMQP] EMIT_RCQ Worker  channel closed");
    });
    
    ch.prefetch(10);
    ch.assertQueue(emit_rcq_queue, { durable: true }, function(err, _ok) {
      if (closeOnErr(err)) return;
      ch.consume(emit_rcq_queue, processMsg, { noAck: false });
      console.log("[AMQP] EMIT_RCQ Worker is started");
    });

    function processMsg(msg) {
      work(msg, function(ok) {
        if (ok)
          ch.ack(msg);
        else
          ch.reject(msg, true);
      });
    }
  });
}

function closeOnErr(err) {
  if (!err) return false;
  console.error("[AMQP] EMIT_RCQ Worker error", err);
  amqpConn.close();
  return true;
}

function work(msg, cb) {
  var msgOb = JSON.parse(msg.content);
  if(typeof(msgOb.pid) !== 'undefined'){
    processProfileRcq(msgOb, function(err){
      if(err){
        console.log('Error found in Profile Rcq Emission and hence Seen count will not be processed for pid : '+msgOb.pid);
        return cb(true);
      }
      perProfilePerMsgSeenCount(msgOb.pid, msgOb.aid, function(err){
        if(err){
          console.log('Could not process Successfully Seen counts for pid : '+msgOb.pid);
        }
        return cb(true);
      })
    });
  } else {
    processAccountRcq(msgOb, function(err){
      if(err){
        console.trace(err);
        console.log('Error found in Account Rcq Emission for AID : '+msgOb.aid);
      }
      processAccountPluginQueue(msgOb, function(err){
        if(err){
          console.trace(err);
          console.log('Error found in Account Plugin Queue Emission for AID : '+msgOb.aid);
        }
        return cb(true);
      });
    });
  }
}

function processProfileRcq(msgOb, cb){
  var aid = msgOb.aid;
  var pid = msgOb.pid;
  var mids = [];
  rcq.getall(pid,function(err,profile_msgs){
    if(err){
      console.log('\n'); 
      console.log('DateTime : '+Date());
      console.log('Could Not process the Following Profile Rcq Object');
      console.log(msgOb);
      console.trace(err);
      console.log('\n');
      return cb(err);
    }
    var num_prof_msgs = profile_msgs.length;
    for(var i=0; i<num_prof_msgs; i++) {
      mids.push(profile_msgs[i]);
    }
    var num_msgs = mids.length;
    if(num_msgs == 0){
      console.log('\n');
      console.log(msgOb);
      console.log('YOU ROCK BABY , BECOZ THERE ARE NO MESSAGES IN YOUR PROFILE QUEUE');
      console.log('DateTime : '+Date());
      console.log('\n');
      return cb(null);
    }
    var done      = 0;
    console.log('EMITRCQ will try to send messages  : '+num_msgs+' , for PID  : '+pid);
    console.log('DateTime : '+Date());
    for(var i=0; i<num_msgs; i++) {
      sendMessageFromProfRcq(mids[i], pid, aid, function(err,resp){
        done++;
        if(err){
          console.trace(err);
        }
        if(done == num_msgs){
          console.log('\n');
          console.log('For Profile RCQ of pid : '+pid);
          console.log('Total Messages in the RCQ  : '+num_msgs);
          console.log('Total messages successfully delivered  :  '+done);
          console.log('DateTime : '+Date());
          console.log('\n');
          return cb(null);
        }
      });
    }
  });
}

function processAccountRcq(msgOb, cb){
  var aid = msgOb.aid;
  var mids = [];
  rcq.getall(aid,function(err,account_msgs){
    if(err){ 
      console.log('\n');
      console.log('Could Not process The Following Account Rcq Object');
      console.log(msgOb);
      console.trace(err);
      console.log('DateTime : '+Date());
      console.log('\n');
      return cb(err);
    }
    var num_acc_msgs = account_msgs.length; 
    for(var i=0; i<num_acc_msgs; i++) {
      mids.push(account_msgs[i]);
    }
    var num_msgs = mids.length;
    if(num_msgs == 0){
      console.log('\n');
      console.log(msgOb);
      console.log('YOU ROCK BABY , BECOZ THERE ARE NO MESSAGES IN YOUR ACCOUNT QUEUE');
      console.log('DateTime : '+Date());
      console.log('\n');
      return cb(null);
    }
    var done = 0;
    console.log('EMITRCQ will have to send messages  : '+num_msgs+' , for AID  : '+aid);
    console.log('DateTime : '+Date());
    for(var i=0; i<num_msgs; i++) {
      sendMessageFromAccRcq(mids[i], aid, function(err,resp){
        done++;
        if(err){
          console.trace(err);
        }
        if(done == num_msgs){
          console.log('\n');
          console.log('For Account RCQ of Aid : '+aid);
          console.log('Total Messages in the RCQ  : '+num_msgs);
          console.log('Total messages successfully delivered  :  '+done);
          console.log('DateTime : '+Date());
          console.log('\n');
          return cb(null);
        }
      });
    }
  });
}

function processAccountPluginQueue(msgOb, cb){
  var aid = msgOb.aid;
  rhmset.getall(RedisPrefixes.PLUGIN_STATE+aid, function(err,plugin_states){
    if(err){ 
      return cb(err);
    }
    if(!plugin_states){ 
      console.log('******No plugin Messages in Account Plugin Queue***** : '+aid);
      return cb(null); 
    }
    console.log("Plugin States found for Account Plugin for aid : "+aid);
    console.log('DateTime : '+Date());
    var gid, data, id, state;
    for(var key in plugin_states){
      gid  = key;
      data = JSON.parse(plugin_states[key]);
      id   = data.id;
      state= data.state;
      sendPluginPacket(aid, gid, id, state);
    }
    return cb(null);
  });
}

function sendPluginPacket(aid, gid, id, state){
  var data = {
    gid : gid,
    id  : id,
    state : state 
  };
  io.to(aid).emit(Events.PLUGIN,data);
  console.log("Following packet emitted for aid : "+aid);
  console.log(data);
  return;
}

function sendMessageFromProfRcq(mid, pid, aid, cb){
  rmc.queryById("Message",mid,function(err, message){
    if(err){
      return cb(err,null);
    } 
    if(!message){
      dropMessageFromUserRcq(pid, mid);
      return cb('Unfortunately For Profile Rcq ,the Message with id : '+mid+' , NOT FOUND IN DATABASE',null);
    }
    if(("ignr" in message) && (message.ignr == true || message.ignr == 'true')){
      return processMessageDeleterPacket(message, pid, aid, cb);
    }
    message.fp  = pid;
    if(typeof(message.from) !== 'undefined' && typeof(message.to) !== 'undefined'){
      getMessageUserRoleAndMT(message.from,message.to,message.type,function(err,roleAndMT){
        if(err){
          console.log('error from message packet while fetching user role and mem type');
          console.log(message);
          return cb(err,null);
        }
        message.r    = roleAndMT.r;
        message.mt   = roleAndMT.mt;
        if ("actn" in message){
          processMessageWithAction(message, aid);
        } else {
          data = {};
          data.body   = message.body;
          data.type   = message.type;
          data.gid    = message.to;
          data.pid    = message.from;
          data.pnam   = (typeof(message.pnam)!= "undefined") ? message.pnam : "";
          data.mid    = message._id;
          data.tim    = message.tim;
          data.r      = roleAndMT.r;
          data.mt     = roleAndMT.mt;
          data.fp     = message.fp;

          if("pi" in message && 
              (message.type == Message.MESSAGE_TYPE.text || message.type == Message.MESSAGE_TYPE.image ||
                message.type == Message.MESSAGE_TYPE.audio || message.type == Message.MESSAGE_TYPE.video)){
            data.pi = message.pi;
          }
          io.to(aid).emit(Events.GROUP_MESSAGE,data);
        }
        return cb(null,true);
      });
    } else {
      if ("actn" in message){
        processMessageWithAction(message, aid);
      } else {
        console.log('Unfortunately , actn not found in message and this message will not be emitted from Profile RCQ');
        console.log('pid : '+pid+', mid : '+mid+', aid : '+aid);
        console.log(message);
        dropMessageFromUserRcq(pid, mid);
      }
      return cb(null,true);
    }
  });
}

function processMessageDeleterPacket(message, pid, aid, cb){
  Account.findOne({
    _id : aid,
    vrfy: true
  },{
    vrsn : 1,
    iosv : 1,
    _id  : 0
  },function(err,account){
    if(err){
      return cb(err,null);
    }
    if(!account){
      return cb("account_ID_not_found_while_searching_for_message_deleter_packet_"+aid,null);
    }
    if(account.vrsn && (account.vrsn > configs.DELETE_MESSAGE_ANDROID_VERSION)){
      processMessageWithAction(message, aid);
      return cb(null,true);
    }
    if(account.iosv && (account.iosv > configs.DELETE_MESSAGE_IOS_VERSION)){
      processMessageWithAction(message, aid);
      return cb(null,true);
    }
    dropMessageFromUserRcqForMsgDeleter(pid, message._id, account.vrsn, account.iosv);
    return cb(null,true);
  });
}

function sendMessageFromAccRcq(mid, aid, cb){
  rmc.queryById("Message",mid,function(err, message){
    if(err){
      return cb(err,null);
    } 
    if(!message){
      dropMessageFromUserRcq(aid, mid);
      return cb('Unfortunately For Account Rcq : '+aid+', the Message with id : '+mid+' , NOT FOUND IN DATABASE',null);
    }
    if ("actn" in message){
      processMessageWithAction(message, aid);
    } else {
      if(typeof(message.from) !== 'undefined' && typeof(message.to) !== 'undefined'){
        data = {};
        data.body   = message.body;
        data.type   = message.type;
        data.gid    = message.to;
        data.pid    = message.from;
        data.pnam   = (typeof(message.pnam)!= "undefined") ? message.pnam : "";
        data.mid    = message._id;
        data.tim    = message.tim;

        //pi is pic identifier. Currently it is sent from client, 
        //pi is formed the ppic value by splitting it to get the time stamp
        //that was attached the ppic url during its creation.
        if("pi" in message && 
            (message.type == Message.MESSAGE_TYPE.text || message.type == Message.MESSAGE_TYPE.image ||
              message.type == Message.MESSAGE_TYPE.audio || message.type == Message.MESSAGE_TYPE.video)){
          data.pi = message.pi;
        }
        io.to(aid).emit(Events.GROUP_MESSAGE,data);
      } else {
        console.log('Unfortunately , actn not found in message and this message will not be emitted from Account RCQ');
        console.log('mid : '+mid+', aid : '+aid);
        console.log(message);
        dropMessageFromUserRcq(aid, mid);
      }
    }
    return cb(null,true);
  });
}

function perProfilePerMsgSeenCount(pid, aid, cb){
  rhmset.getall(RedisPrefixes.SEEN_COUNT+pid, function(err, perProfilePerMsgCounts){
    if(err) { 
      console.trace(err); 
      return cb(err);
    }
    if(perProfilePerMsgCounts) {
      if(!perProfilePerMsgCounts.length){
        for(var mid in perProfilePerMsgCounts){
          counts = perProfilePerMsgCounts[mid];
          var pkt = { mid : mid, apid : pid, count : counts} ;
          emitIfMidNotInRcq(pkt, aid);
        }
      } else {
        var totalMsgForSeens = perProfilePerMsgCounts.length;
        console.log('perProfilePerMsgSeenCount for aid : '+aid+' , pid : '+pid+' , totalMsgForSeens : '+totalMsgForSeens);
        for(var i=0 ; i< totalMsgForSeens; i+=2) {
          var tempProfilePerMsgCounts = perProfilePerMsgCounts[i];
          for(var mid in tempProfilePerMsgCounts){
            counts = perProfilePerMsgCounts[mid];
            var pkt = { mid : mid, apid : pid, count : counts };
            emitIfMidNotInRcq(pkt, aid);
          }
        }
      }
    }
    return cb(null);
  });
}

function emitIfMidNotInRcq(pkt, aid){
  rcq.exists(pkt.apid, pkt.mid, function(err, midExists){
    if(err) { 
      console.trace(err); 
      console.log('In emitIfMidNotInRcq Seen count : pkt could not be emitted for aid : '+aid+' , and pkt : '+pkt); 
      return;
    }
    if(midExists){
      // console.log('In emitIfMidNotInRcq Seen count : mid : '+pkt.mid+' , is in RCQ for pid :'+pkt.apid+' and hence will not be emitted');
      return;
    }
    pkt.fp = 1;
    io.to(aid).emit(ADMIN_SEEN, pkt);
  });
}

function prepareUserGrpMem(pid, cb){
  GroupMember.find({
    pid : pid,
    act : true
  },{
    gid :1,
    pid :1,
    type:1,
    _id :0
  },function(err,groupmembers){
    if(err){
      console.trace(err);
      return cb(true,null);
    }
    var result = [];
    var total_gms = groupmembers.length;
    for(var i=0; i< total_gms; i++){
      if(groupmembers[i].pid == pid){
        result[groupmembers[i].gid] = groupmembers[i];
      }
    }
    return cb(null,result);
  });
}

function getMessageUserRoleAndMT(pid, gid, msg_type, cb){
  var role = 0;
  var mt   = 0;
  UserInfoCache.getMongoProfile(pid, function(err,profile){
    if(err){
      return cb(err,null);
    }
    if(profile){
      role = profile.role;
    }
    if(NORMAL_MSG_TYPES.indexOf(msg_type) >= 0){
      UserInfoCache.getMongoGrpMem(pid, gid, function(err, grpMem){
        if(err){
          return cb(err,null);
        }
        if(grpMem){
          mt = grpMem.type;
        }
        return cb(null, {r : role, mt : mt});
      });
    } else {
      GroupMember.findOne({
        gid : gid,
        pid : pid,
        act : true
      },{
        type:1,
        _id :0
      },function(err,grpMem){
        if(err){
          return cb(err,null);
        }
        if(grpMem){
          mt = grpMem.type;
        }
        return cb(null, {r : role, mt : mt});
      });
    }
  });
}

function dropMessageFromUserRcq(id, mid){
  rcq.remove(id,mid,function(err,res){
    if(err) {
      console.trace(err); 
    }
    if(res == 1) {
      console.log('Succesfully removed mid : '+mid+' , from RCQ of User Id : '+id+', DateTime : '+Date());
    } else {
      console.log('Could not remove mid : '+mid+' , from RCQ of User Id : '+id+', DateTime : '+Date());
    }
  });
}

function dropMessageFromUserRcqForMsgDeleter(id, mid, vrsn, iosv){
  rcq.remove(id,mid,function(err,res){
    if(err) {
      console.trace(err); 
    }
    if(res == 1) {
      console.log('Succesfully removed mid : '+mid+' , from RCQ of User Id : '+id+', for older account version, vrsn '+vrsn+', iosv :'+iosv+', DateTime : '+Date());
    } else {
      console.log('Could not remove mid : '+mid+' , from RCQ of User Id : '+id+', for older account version, vrsn '+vrsn+', iosv :'+iosv+', DateTime : '+Date());
    }
  });
}

function processMessageWithAction(message, aid) {
  data = {};
  data.body   = message.body;
  data.type   = parseInt(message.type);
  data.pid    = message.from;
  data.mid    = message._id;
  data.tim    = message.tim;
  data.pnam   = (typeof(message.pnam)!= "undefined") ? message.pnam : "";
  data.actn   = message.actn;

  if(typeof(message.r) !== 'undefined') 
    data.r    = message.r;
  if(typeof(message.mt) !== 'undefined') 
    data.mt   = message.mt;
  if("fp" in message){
    data.fp   = message.fp;
  }

  var event_name = Events.GROUP_MESSAGE;

  switch(parseInt(message.actn.t)) {
    case Message.ACTION_TYPE.GROUP_NAME_EDIT:
      data.gid    = message.to;
      break;
    case Message.ACTION_TYPE.GROUP_PIC_EDIT:
      data.gid    = message.to;
      break;
    case Message.ACTION_TYPE.GROUP_PIC_REM:
      data.gid    = message.to;
      break;
    case Message.ACTION_TYPE.GROUP_ADMIN_ADD:
      data.gid    = message.to;
      break;
    case Message.ACTION_TYPE.GROUP_ADMIN_REM:
      data.gid    = message.to;
      break;
    case Message.ACTION_TYPE.GROUP_LEAVE:
      data.gid    = message.to;
      break;
    case Message.ACTION_TYPE.GROUP_JOIN:
      data.gid    = message.to;
      break;
    case Message.ACTION_TYPE.GROUP_CREATE:
      data.gid    = message.to;
      break;
    case Message.ACTION_TYPE.GROUP_TYPE_EDIT:
      data.gid    = message.to;
      break;
    case Message.ACTION_TYPE.GROUP_MEMBER_BAN:
      data.gid    = message.to;
      break;
    case Message.ACTION_TYPE.GROUP_MEMBER_UNBAN:
      data.gid    = message.to;
      break;
    case Message.ACTION_TYPE.GROUP_MEMBER_DEL:
      data.gid    = message.to;
      break;
    case Message.ACTION_TYPE.PROFILE_NAME_EDIT:
      event_name = Events.PROFILE_NAME_EDIT;
      break;
    case Message.ACTION_TYPE.PROFILE_PIC_EDIT:
      event_name = Events.PROFILE_PIC_EDIT;
      break;
    case Message.ACTION_TYPE.PROFILE_PIC_REM:
      event_name = Events.PROFILE_PIC_REM;
      break;
    case Message.ACTION_TYPE.PROFILE_DELETE:
      event_name = Events.PROFILE_DELETE;
      break;
    case Message.ACTION_TYPE.PROFILE_CREATE:
      event_name = Events.PROFILE_CREATE;
      break;
    case Message.ACTION_TYPE.DELETE_MESSAGE:
      data.gid    = message.to;
      event_name  = Events.EXTRA;
      break;
  }
  io.to(aid).emit(event_name,data);
  return;
}

function informDevTeam(subject, body, cb){
  var mailOptions = {
    from    : configs.RABBITMQ_WORKER_FROM,
    to      : configs.TECH_TO,
    subject : subject, 
    html    : body
  };
  mail_client.send(mailOptions, function(err, response){
    if (err) {
      console.trace(err);
      console.log('Informing Dev Team: Mail could be not sent');
      return cb(err);
    } else {
      console.log('Informing Dev Team: Mail sent succesfully');
      return cb(null);
    }
  });
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : configs.MONGO_SERVER_KEEPALIVE
      }
    }
  };
  if(!configs.MONGO_UNAME || configs.MONGO_UNAME == ""){
    options.server.poolSize = configs.MONGO_LOWER_POOLSIZE;
    console.log('[[[[[[[    NO MONGO_UNAME FOUND IN CONFIGS, WILL CONNNECT WITHOUT REPLICA OPTIONS     ]]]]]]]]]');
    console.log('[[[[[[[    MONGODB CONNECTION POOL_SIZE : '+configs.MONGO_LOWER_POOLSIZE+'     ]]]]]]]]]');
    return mongoose.connect("mongodb://"+configs.MONGO_HOST+":"+configs.MONGO_PORT+"/"+configs.DB_NAME,options);
  }
  options.replset = {
    rs_name : configs.MONGO_REPLICA_SET_NAME,
    socketOptions : {
      keepAlive : configs.MONGO_REPLICA_KEEPALIVE
    },
    poolSize : configs.MONGO_LOWER_POOLSIZE
  };
  var connect_string = "mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@";
  var mongo_hosts    = configs.MONGO_HOST;
  var mongo_port     = configs.MONGO_PORT;
  for(var i=0; i<mongo_hosts.length; i++){
    if(i > 0)
      connect_string += ",";
    connect_string += mongo_hosts[i]+":"+mongo_port;
  }
  connect_string += "/"+configs.DB_NAME;
  console.log('[[[[[[[     CONNECTING MONGODB WITH REPLICA_SET OPTIONS     ]]]]]]]]]');
  console.log('[[[[[[[     MONGODB CONNECTION POOL_SIZE : '+configs.MONGO_LOWER_POOLSIZE+'      ]]]]]]]]]');
  return mongoose.connect(connect_string,options);
}