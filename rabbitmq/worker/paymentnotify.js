var amqp                        = require('amqplib/callback_api');
var mongoose                    = require('mongoose');
var redis                       = require('redis');
var jwt                         = require('jsonwebtoken');
var fs                          = require('fs');
var path                        = require('path');
var _                           = require('underscore');

var AppClients                  = require('../../utility/app_clients.js');
var RedisPrefixes               = require('../../utility/redis_prefix.js');
var mail_client                 = require('../../utility/mail/mail_client.js');
var configs                     = require('../../utility/configs.js');
var PaymentConfigs              = require('../../payment/configs.js');
var PaytmApi                    = require('../../payment/paytm_api.js');
var PaymentStatusApi            = require('../../payment/payment_status_api.js');
var GenericSmsGateway           = require('../../sms_module/sms_gateway.js');
var SmsEngines                  = require('../../utility/sms/sms_engines.js');
var GroupInfoCache              = require('../../utility/group_info_cache.js');
var RabbitMqPublish             = require('../publisher/publisher.js');
var RabbitMqQueues              = require('../queues.js');
var PluginServerApi             = require('../../pluginserver/apicalls.js');
var Events                      = require('../../utility/event_names.js');
var dbConnectionAttempts        = 0;

var redis_client          = redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
    console.log("Error " + err);
});

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  var subject = 'PAYMENT WORKER => MongoDb Connection Failure'
  var mbody = '<h4>'+ Date()  +': MongoDb Connection Failed to connect Due to Follwoing error</h4>';
  mbody += '<h5>'+err+'</h5>';
  if(dbConnectionAttempts == configs.DB_CONNECTION_RETTEMPT_LIMIT){
    informDevTeamAboutConnectionError(subject, mbody, function(err){
      process.exit(0);
    });
  } else {
    dbConnectionAttempts++;
    connectMongoDb();
  }
});
mongoose.connection.on('connected', function(){
  console.log('Successfully Opened MOngoDb Connection');
  dbConnectionAttempts = 0;
});

require('../../models/accounts.js');
require('../../models/payment/transactions.js');
require('../../models/payment/transactionlogs.js');
require('../../models/packages/grouppluginpackages.js');
require('../../models/subscriptions/usersubscriptions.js');
require('../../models/pluginconfigs.js');
var Account                 = mongoose.model('Account');
var Transaction             = mongoose.model('Transaction');
var TransactionLog          = mongoose.model('TransactionLog');
var GroupPluginPackage      = mongoose.model('GroupPluginPackage');
var UserSubscription        = mongoose.model('UserSubscription');
var PluginConfig            = mongoose.model('PluginConfig');

var rhmset                  = require("../../utility/redis_hmset.js")(redis_client);
var io                      = require('socket.io-emitter')(redis_client);

var user                            = configs.RABBITMQ_USER;
var passwd                          = configs.RABBITMQ_PASSWORD;
var rabbitmq_url                    = 'amqp://'+user+':'+passwd+'@'+configs.RABBITMQ_HOST+':'+configs.RABBITMQ_PORT;
var amqpConn                        = null;
var payment_notify_queue            = RabbitMqQueues.PAYMENT_NOTIFY; 
var payment_notify_attempts_limit   = PaymentConfigs.PAYMENT_NOTIFY_ATTEMPTS_LIMIT;
var payment_notify_reattempt_delay  = PaymentConfigs.SMS_PAYMENT_NOTIFY_REATTEMPT_DELAY;
var payment_notify_template         = PaymentConfigs.SMS_PAYMENT_NOTIFY_TEMPLATE;

startRabbitServer();

function startRabbitServer() {
  amqp.connect(rabbitmq_url + "?heartbeat=60", function(err, conn) {
    if (err) {
      console.error("[AMQP] PAYMENT Delivery Worker", err.message);
      return setTimeout(startRabbitServer, 1000);
    }
    conn.on("error", function(err) {
      if (err.message !== "Connection closing") {
        console.error("[AMQP] PAYMENT Delivery Worker conn error", err.message);
      }
    });
    conn.on("close", function() {
      console.error("[AMQP] reconnecting");
      return setTimeout(startRabbitServer, 1000);
    });
    console.log("[AMQP] PAYMENT Delivery Worker connected");
    amqpConn = conn;
    whenConnected();
  });
}

function whenConnected() {
  startWorker();
}

function startWorker() {
  amqpConn.createChannel(function(err, ch) {
    if (closeOnErr(err)) return;
    ch.on("error", function(err) {
      console.error("[AMQP] PAYMENT Delivery Worker channel error", err.message);
    });
    ch.on("close", function() {
      console.log("[AMQP] PAYMENT Delivery worker  channel closed");
    });
    
    ch.prefetch(10);
    ch.assertQueue(payment_notify_queue, { durable: true }, function(err, _ok) {
      if (closeOnErr(err)) return;
      ch.consume(payment_notify_queue, processMsg, { noAck: false });
      console.log("[AMQP] PAYMENT Delivery Worker is started");
    });

    function processMsg(msg) {
      work(msg, function(ok) {
        if (ok)
          ch.ack(msg);
        else
          ch.reject(msg, true);
      });
    }
  });
}

function closeOnErr(err) {
  if (!err) return false;
  console.error("[AMQP] PAYMENT Delivery Worker error", err);
  amqpConn.close();
  return true;
}

function work(msg, cb) {
  var trxn_obj = JSON.parse(msg.content);
  console.log('transaction notify packet is');
  console.log(trxn_obj);
  var subject;
  if(trxn_obj.cron && trxn_obj.cron > 0){
    console.log('This Trxn packet has come for Cron Running and checking of Failed Trxns');
    console.log('This packet will not create subscription');
    console.log('Above packet is for informing about failed transactions');
    runDelayedCheckForFailedTrxn(trxn_obj);
    return cb(true);
  }
  if(trxn_obj.atmpt > payment_notify_attempts_limit){
    console.log('transaction notify Object has reached its max attempt limit and hence will not be republished');
    console.log(trxn_obj);
    console.log('Informing Dev Team about transaction notify Not Found Error');
    subject = 'Maximum transaction notify delivery attempts reached for : '+trxn_obj.m;
    informDevAndMrktTeam(trxn_obj, subject);
    return cb(true);
  }
  Transaction.findOne({
    _id : trxn_obj.trxn_id
  },function(err,transaction){
    if(err) { 
      console.trace("Error occurred in querying transaction from database"); 
      console.log(err);
      console.log('The transaction obj will be processed again after some delay');
      console.log(trxn_obj);
      republishPaymentNotifyWithDelay(trxn_obj);
      return cb(true);
    }
    if(!transaction) { 
      console.trace("Invalid transaction ID found in the queue");
      console.log('The transaction obj will not be processed again after some delay');
      console.log(trxn_obj); 
      subject = 'transaction Id not found for : '+trxn_obj.trxn_id;
      informDevTeam(trxn_obj, subject);
      return cb(true);
    }
    GroupInfoCache.getMongoGroup(transaction.gid,function(err,group){
      if(err){ 
        console.trace("Error occurred in querying group from transaction"); 
        console.log(err);
        console.log('The transaction obj will be processed again after some delay');
        console.log(trxn_obj);
        republishPaymentNotifyWithDelay(trxn_obj);
        return cb(true);
      }
      if(!group){
        console.trace("Group Id from transaction not found in the Group");
        console.log('The transaction obj will not be processed again after some delay');
        console.log(trxn_obj); 
        subject = 'transaction Id not found for : '+trxn_obj.trxn_id;
        informDevTeam(trxn_obj, subject);
        return cb(true);
      }
      GroupPluginPackage.findOne({
        _id : transaction.package_id
      },function(err,plugin_package){
        if(err){
          console.trace("Error occurred in querying GroupPluginPackage from transaction"); 
          console.log(err);
          console.log('The transaction obj will be processed again after some delay');
          console.log(trxn_obj);
          republishPaymentNotifyWithDelay(trxn_obj);
          return cb(true);
        }
        if(!plugin_package){
          console.trace("Package ID from Transaction not found in GroupPluginPackage");
          console.log('The transaction obj will not be processed again after some delay');
          console.log(trxn_obj); 
          subject = 'transaction Id not found for : '+trxn_obj.trxn_id;
          informDevTeam(trxn_obj, subject);
          return cb(true);
        }
        checkTransactionStatus(trxn_obj.trxn_id, transaction.gateway_status, transaction.gateway_respcode, trxn_obj.gtwy, function(err,transaction_status){
          if(err){
            console.trace("Error occurred in querying checkTransactionStatus"); 
            console.log(err);
            console.log('The transaction obj will be processed again after some delay');
            console.log(trxn_obj);
            republishPaymentNotifyWithDelay(trxn_obj);
            return cb(true);
          }
          var compiled  = _.template(payment_notify_template);
          var message   = compiled({
            trxn_id  : trxn_obj.trxn_id, 
            amt      : transaction.final_amount,
            group    : group.name,
            pkg_name : plugin_package.package_name,
            status   : transaction_status
          });
          PostResponseToPluginServer(transaction.gateway_status, transaction, plugin_package, group, function(err,resp){
            if(err){
              console.trace("Error occurred in querying PostResponseToPluginServer"); 
              console.log(err);
              console.log('The transaction obj will be processed again after some delay');
              console.log(trxn_obj);
              republishPaymentNotifyWithDelay(trxn_obj);
              return cb(true);
            }
            emitSubscriptionSuccessPacket(transaction, function(err,resp){
              if(err){
                console.log('Error From : emitSubscriptionSuccessPacket');
                console.log(err);
              }
              createSmsForUser(transaction.aid, message, function(err,sms_response){
                if(err){
                  console.trace("Error occurred in querying createSmsForUser"); 
                  console.log(err);
                  console.log('The transaction obj will be processed again after some delay');
                  console.log(trxn_obj);
                  republishPaymentNotifyWithDelay(trxn_obj);
                  return cb(true);
                }
                Account.findOne({
                  _id : transaction.aid
                },{
                  m    : 1,
                  e    : 1
                },function(err,account){
                  if(err){
                    console.trace('Account Find Failed');
                    console.log(err);
                    return;
                  }
                  if(!account){
                    console.trace('Account Not For AID in Trxn ID');
                    console.log(trxn_obj);
                    return;
                  }
                  var mail_object   = {
                    ORDER_ID       : trxn_obj.trxn_id, 
                    TRXN_AMOUNT    : transaction.final_amount,
                    MOBILE_NO      : account.m,
                    EMAIL          : (account.e) ? account.e : "NONE",
                    PLATFORM       : (!transaction.cl) ? AppClients.ANDROID : transaction.cl,
                    GROUP_NAME     : group.name,
                    GROUP_CODE     : group.code,
                    PKG_NAME       : plugin_package.package_name,
                    UNIT           : plugin_package.duration_unit,
                    BASE           : plugin_package.duration_base,
                    PACKAGE_AMOUNT : transaction.actual_amount,
                    MODE           : transaction.payment_mode,
                    TRXN_STATUS    : transaction.status,
                    GTWY_STATUS    : transaction.gateway_status,
                    STATUS_STRING  : transaction_status,
                    TRXN_START_TIME: transaction.createdAt,
                    COUPON         : (!transaction.coupon_code) ? "NONE" : transaction.coupon_code 
                  };
                  informPaymentSuccessToPaymentsTeam(group._id, mail_object, transaction_status);
                  if(!sms_response){
                    console.log(trxn_obj);
                    console.log('The Payment Notify obj will be processed again after some delay again');
                    subject = 'SMS Gateway Unable to send payment notify for trxn_id : '+trxn_obj.trxn_id;
                    informDevTeam(trxn_obj, subject);
                    republishPaymentNotifyWithDelay(trxn_obj);
                    return cb(true);
                  } else {
                    console.log('********************************************');
                    console.log('Payment Notify was Successfull');
                    console.log(trxn_obj);
                    return cb(true);
                  }
                });
              });
            });
          });
        });
      });
    });
  });
}

function PostResponseToPluginServer(trxn_status, transaction, plugin_package, group, cb){
  if(trxn_status != Transaction.GATEWAY_STATUS.TXN_SUCCESS 
    && trxn_status != Transaction.GATEWAY_STATUS.NO_NEED 
    && transaction.status != Transaction.STATUS.SUCCESS){
    return cb(null,true);
  }
  UserSubscription.findOne({
    trxn_id : transaction._id
  },function(err,subscription){
    if(err){
      return cb(err,null);
    }
    if(!subscription){
      return cb("user_subscription_not_found",null);
    }
    var parameters = {
      PLUGIN_ID       : transaction.plugin_id,
      GROUP_ID        : group.code,
      IDENTIFIER      : transaction.identifier,
      PACKAGE_ID      : transaction.package_id,
      PACKAGE_NAME    : plugin_package.package_name,
      PACKAGE_PRICE   : plugin_package.package_price,
      PROMO_CODE      : transaction.coupon_code,
      ORDER_AMOUNT    : transaction.final_amount,
      ORDER_ID        : transaction._id,
      END_TIME        : subscription.etim,
      START_TIME      : subscription.stim
    };
    if(transaction.activity == Transaction.ACTIVITY.GROUP_JOIN){
      delete parameters["IDENTIFIER"];
      delete parameters["PACKAGE_NAME"];
      delete parameters["PACKAGE_PRICE"];
      delete parameters["PROMO_CODE"];
      parameters.CUSTOMER_ID = transaction.customer_id;
      parameters.AID         = transaction.aid;
    }
    PluginConfig.findOne({
      plugin_id : transaction.plugin_id
    },function(err,server_configs){
      if(err){
        return cb(err,null);
      }
      if(!server_configs){
        return cb("server_configs_not_found for plugin_id : "+transaction.plugin_id,null);
      }
      parameters = secureParameterString(parameters, server_configs.api_key, server_configs.req_exp)
      PluginServerApi.postcallback(server_configs.host, server_configs.port, server_configs.api_path, parameters, transaction.identifier, transaction.activity, function(err,res){
        if(err){
          return cb(err,null);
        }
        return cb(null,true);
      });
    });
  });
}

function emitSubscriptionSuccessPacket(transaction, cb){
  if(!transaction.cl || transaction.cl != AppClients.WEB){
    return cb("Client_Is_Not_web_:_NoPacketEmition",true);
  }
  if(transaction.gateway_status != Transaction.GATEWAY_STATUS.TXN_SUCCESS 
    && transaction.gateway_status != Transaction.GATEWAY_STATUS.NO_NEED 
    && transaction.status != Transaction.STATUS.SUCCESS){
    return cb(null,true);
  }
  var gid = transaction.gid;
  var aid = transaction.aid;
  var packet = {
    id    : new Date().getTime(),
    gid   : gid,
    state : 1
  };
  var rPacket = JSON.stringify(packet);
  rhmset.set(RedisPrefixes.PLUGIN_STATE, aid, gid, rPacket, function(err,resp){
    if(err){
      return cb(err,null);
    }
    if(resp){
      io.to(aid).emit(Events.PLUGIN,packet);
      console.log('emitSubscriptionSuccessPacket, Following packet emitted for aid : '+aid);
      console.log(packet);
      return cb(null,true);
    }
    return cb('emitSubscriptionSuccessPacket_redis_resp_false',true);
  });
}

function secureParameterString(param_object, private_key, request_expiry_time){
  return jwt.sign(
    param_object, 
    private_key, 
    {
      expiresIn: request_expiry_time
    }
  );
};

function checkTransactionStatus(trxn_id, gateway_status, gateway_respcode, gtwy, cb){
  if(gateway_status == Transaction.GATEWAY_STATUS.NO_NEED || gtwy == Transaction.GATEWAY.RAZORPAY){
    return cb(null,getTrxnStatusString(Transaction.STATUS.TXN_SUCCESS));
  }
  PaytmApi.getTrxnStatus(trxn_id, function(err,resp){
    if(err){
      return cb(err,null);
    }
    if(!resp){
      return cb("Paytm_Trxn_Status_response_not_found",null);
    }
    var status = getTrxnStatusString(resp.STATUS);
    if(!status || status == ""){
      return cb("Paytm_Trxn_Status_response_status_not_found",null);
    }
    if(resp.STATUS == gateway_status && resp.RESPCODE == gateway_respcode){
      return cb(null,status);
    }
    Transaction.update({
      _id : trxn_id
    },{
      gateway_status   : resp.STATUS,
      gateway_respcode : resp.RESPCODE
    },function(err,transaction_updated){
      if(err){
        return cb(err,null);
      }
      var new_trxn_log = new TransactionLog({
        trxn_id          : trxn_id,
        gateway_status   : resp.STATUS,
        gateway_respcode : resp.RESPCODE,
        gateway_log      : resp.RESPMSG,
        log              : "gateway_response_updated_while_notifying"
      });
      new_trxn_log.save(function(err,new_log){
        if(err){
          console.trace(err);
        }
        return cb(null,status);
      });
    });  
  });
}

function getTrxnStatusString(status){
  var respString = "";
  if(status == "TXN_SUCCESS" || status == "SUCCESS"){
    respString =  "has completed successfully";
  } else if(status == "TXN_FAILURE"){
    respString = "has failed. For queries, "+PaymentConfigs.PAYMENT_HELPLINE_NUMBER+" , "+PaymentConfigs.PAYMENT_HELPLINE_EMAIL;
  } else if(status == "OPEN"){
    respString = "is pending. For queries, "+PaymentConfigs.PAYMENT_HELPLINE_NUMBER+" , "+PaymentConfigs.PAYMENT_HELPLINE_EMAIL;
  }
  return respString;
}

function createSmsForUser(aid, message, cb){
  Account.findOne({
    _id : aid
  },{
    ccod : 1,
    m    : 1
  },function(err,account){
    if(err){
      return cb(err,null);
    }
    if(!account){
      return cb("account does nto exists for AID : "+aid,null);
    }
    var sms_engine;
    if(account.ccod == '91'){
      sms_engine = SmsEngines.MSG_91;
    } else {
      sms_engine = SmsEngines.NEXMO;
    }
    var sent = 0;
    GenericSmsGateway.sender(
      sms_engine,
      account.ccod,
      account.m,
      message,
      function(data, was_queued){
        console.log('SMS output from engines are');
        console.log(was_queued);
        console.log(data);
        if(!was_queued){
          console.trace("was not able to send sms for payment Notify via gateway. response : " + data);
          return cb(null,false);
        }
        return cb(null,account.m);
    });
  });
}

function runDelayedCheckForFailedTrxn(trxn_obj_orig){
  // if(trxn_obj_orig.gtwy && trxn_obj_orig.gtwy == 2){
  //   console.log('No runDelayedCheckForFailedTrxn Executed for GTWY :'+trxn_obj_orig.gtwy);
  //   return;
  // }
  var trxn_obj = trxn_obj_orig;
  setTimeout(function(){
    Transaction.findOne({
      _id : trxn_obj.trxn_id
    },function(err,transaction){
      if(err) { 
        console.trace("Error occurred in querying transaction from database"); 
        console.log(err);
        console.log(trxn_obj);
        return;
      }
      if(!transaction) { 
        console.trace("Invalid transaction ID found in the queue");
        console.log(trxn_obj); 
        return;
      }
      if(transaction.status == Transaction.STATUS.SUCCESS){
        console.log('TRANSACNTION with TRXN_ID : '+transaction._id+', HAS_COMPLETED_SUCCESSFULLY');
        console.log('Hence Ending the Cron for The Status of This Transaction');
        return;
      }
      console.log('######################  FAILED PAYMENTS  STATUS REPORT  ############################');
      console.log('\n');
      console.log('ORDER_ID : '+transaction._id);
      GroupInfoCache.getMongoGroup(transaction.gid,function(err,group){
        if(err){ 
          console.trace("Error occurred in querying group from transaction"); 
          console.log(err);
          console.log(trxn_obj);
          return;
        }
        if(!group){
          console.trace("Group Id from transaction not found in the Group");
          console.log(trxn_obj); 
          informDevTeam(trxn_obj, subject);
          return;
        }
        GroupPluginPackage.findOne({
          _id : transaction.package_id
        },function(err,plugin_package){
          if(err){
            console.trace("Error occurred in querying GroupPluginPackage from transaction"); 
            console.log(err);
            console.log(trxn_obj);
            return;
          }
          if(!plugin_package){
            console.trace("Package ID from Transaction not found in GroupPluginPackage");
            console.log(trxn_obj); 
            return;
          }
          PaymentStatusApi.getTrxnStatus(transaction._id, transaction.gateway, transaction.razorpay_id, function(err,resp){
            if(err){
              console.log(err);
              return;
            }
            if(!resp){
              console.log('PaymentStatusApi getTrxnStatus API Reponse Not Found');
              console.log(resp);
              return;
            }
            Account.findOne({
              _id : transaction.aid
            },{
              ccod : 1,
              m    : 1,
              e    : 1
            },function(err,account){
              if(err){
                console.trace('Account Find Failed');
                console.log(err);
                return;
              }
              if(!account){
                console.trace('Account Not For AID in Trxn ID');
                console.log(trxn_obj);
                return;
              }
              var mail_object   = {
                ORDER_ID       : transaction._id, 
                TRXN_AMOUNT    : transaction.final_amount,
                MOBILE_NO      : account.m,
                EMAIL          : (account.e) ? account.e : "NONE",
                PLATFORM       : (!transaction.cl) ? AppClients.ANDROID : transaction.cl,
                GROUP_NAME     : group.name,
                GROUP_CODE     : group.code,
                PKG_NAME       : plugin_package.package_name,
                UNIT           : plugin_package.duration_unit,
                BASE           : plugin_package.duration_base,
                PACKAGE_AMOUNT : transaction.actual_amount,
                MODE           : transaction.payment_mode,
                TRXN_STATUS    : transaction.status,
                GTWY_STATUS    : (resp.STATUS) ? resp.STATUS : "NONE",
                STATUS_STRING  : (resp.RESPMSG) ? resp.RESPMSG : "NONE",
                TRXN_START_TIME: transaction.createdAt,
                COUPON         : (!transaction.coupon_code) ? "NONE" : transaction.coupon_code 
              };
              informPaymentStatusToPaymentsTeam(group._id, mail_object);
              console.log(mail_object);
              console.log('Successfully Informed Payments Team about This Trxn Failure');
              console.log('##################################################');
              return;
            });
          });
        });
      });
    });
  },
    PaymentConfigs.PAYMENT_STATUS_NOTIFY_DELAY
  );
}


function republishPaymentNotifyWithDelay(trxn_obj){
  setTimeout(function(){
    trxn_obj.atmpt  = ("atmpt"  in trxn_obj) ? parseInt(trxn_obj.atmpt)+1 : 2;
    var re_trxn_obj = new Buffer(JSON.stringify(trxn_obj));
    RabbitMqPublish.publishOtp("",RabbitMqQueues.PAYMENT_NOTIFY,re_trxn_obj,function(err,resp){
      if(err) {
        console.trace(err);
        console.log('Payment Notify could not be RePublished for delivery for trxn_id : '+trxn_obj.trxn_id);
        return;
      }
      if(resp){
        console.log('Successfully RePublished Payment Notify message for trxn_id : '+trxn_obj.trxn_id);
      } else {
        console.trace('Payment Notify could not be published for delivery for trxn_id : '+trxn_obj.trxn_id+' , response is : '+resp);
      }
      return;
    });
  },
    payment_notify_reattempt_delay,
    trxn_obj
  );
}

function informDevTeam(trxn_obj, subject){
  if(process.env.NODE_ENV == "ci" || process.env.NODE_ENV == "debug" || process.env.NODE_ENV == "dapi"){
    return;
  }
  var html_string = '<h4>'+ Date()  +': Payment Notify Failure for : '+trxn_obj.trxn_id+' </h4>';
  html_string    += '<h5>'+JSON.stringify(trxn_obj)+'</h5>';
  var mailOptions = {
    from    : configs.OTP_FROM,
    to      : configs.TECH_TO,
    subject : subject, 
    html    : html_string
  };
  mail_client.send(mailOptions, function(err, response){
    if (err) {
      console.trace(err);
      console.log('Informing Dev Team: Mail could be not sent');
    } else {
      console.log('Informing Dev Team: Mail sent succesfully');
    }
    return;
  });
}

function informDevAndMrktTeam(trxn_obj, subject){
  if(process.env.NODE_ENV == "ci" || process.env.NODE_ENV == "debug" || process.env.NODE_ENV == "dapi"){
    return;
  }
  var html_string = '<h4>'+ Date()  +': Payment Notify Failure for : '+trxn_obj.trxn_id+' </h4>';
  html_string += '<h5>'+JSON.stringify(trxn_obj)+'</h5>';
  var mailOptions = {
    from    : configs.OTP_FROM, 
    to      : configs.ANALYTICS_TO, 
    subject : subject, 
    html    : html_string
  };
  mail_client.send(mailOptions, function(err, response){
    if (err) {
      console.trace(err);
      console.log('Informing Dev and Mrkt Team: Mail could be not sent');
    } else {
      console.log('Informing Dev and Mrkt Team: Mail sent succesfully');
    }
    return;
  });
}

function informPaymentSuccessToPaymentsTeam(gid, message_object, transaction_status){
  if(process.env.NODE_ENV == "ci" || process.env.NODE_ENV == "debug" || process.env.NODE_ENV == "dapi"){
    return;
  }
  var subject = 'PAYMENT : '+message_object.ORDER_ID+', STATUS : '+transaction_status;
  getAdminsEmailsOfGroup(gid, function(err,paymentEmailIds){
    if(err){
      console.log("informPaymentSuccessToPaymentsTeam : Emails Fetcing For Group Admins Error fr GID == "+gid);
      console.trace(err);
      paymentEmailIds = configs.PAYMENTS_TO;
    }
    prepareHtmlStringForpaymentStatus(message_object, function(err,html_string){
      if(err){
        console.log('ErrorFrom prepareHtmlStringForpaymentStatus');
        console.trace(err);
        html_string = '<h4>'+JSON.stringify(message_object)+'</h4>';
      }
      var mailOptions = {
        from    : configs.PAYMENTS_FROM, 
        to      : paymentEmailIds, 
        subject : subject, 
        html    : html_string
      };
      mail_client.send_gen(mailOptions, function(err, response){
        if (err) {
          console.trace(err);
          console.log('Informing Dev and Mrkt Team: Mail could be not sent');
        } else {
          console.log('Informing Payments Team: Mail sent succesfully');
        }
        return;
      });
    });
  });
}

function informPaymentStatusToPaymentsTeam(gid, message_object){
  if(process.env.NODE_ENV == "ci" || process.env.NODE_ENV == "debug" || process.env.NODE_ENV == "dapi"){
    return;
  }
  var subject = 'FAILED PAYMENT ATTEMPT: '+message_object.ORDER_ID+', STATUS : '+message_object.TRXN_STATUS;
  getAdminsEmailsOfGroup(gid, function(err,paymentEmailIds){
    if(err){
      console.log("informPaymentStatusToPaymentsTeam : Emails Fetcing For Group Admins Error fr GID == "+gid);
      console.trace(err);
      paymentEmailIds = configs.PAYMENTS_TO;
    }
    prepareHtmlStringForpaymentStatus(message_object, function(err,html_string){
      if(err){
        console.log('ErrorFrom prepareHtmlStringForpaymentStatus');
        console.trace(err);
        html_string = '<h4>'+JSON.stringify(message_object)+'</h4>';
      }
      var mailOptions = {
        from    : configs.PAYMENTS_FROM, 
        to      : paymentEmailIds, 
        subject : subject, 
        html    : html_string
      };
      mail_client.send_gen(mailOptions, function(err, response){
        if (err) {
          console.trace(err);
          console.log('Informing Dev and Mrkt Team: Mail could be not sent');
        } else {
          console.log('Informing Payments Team: Mail sent succesfully');
        }
        return;
      });
    });
  });
}

function informDevTeamAboutConnectionError(subject, body, cb){
  var mailOptions = {
    from    : configs.RABBITMQ_WORKER_FROM,
    to      : configs.TECH_TO,
    subject : subject, 
    html    : body
  };
  mail_client.send(mailOptions, function(err, response){
    if (err) {
      console.trace(err);
      console.log('Informing Dev Team: Mail could be not sent');
      return cb(err);
    } else {
      console.log('Informing Dev Team: Mail sent succesfully');
      return cb(null);
    }
  });
}

function getAdminsEmailsOfGroup(gid, cb){
  GroupInfoCache.getGroupAdmins(gid, function(err,admins){
    if(err){
      return cb(err,null);
    }
    var adminsL = admins.length;
    if(adminsL == 0){
      return cb(null,configs.PAYMENTS_TO);
    }
    var aids = [];
    var skipArrAids = ["5578675092bfdcd3624a243b","556dee31d5084f113d338b9a","55f7e977b8d035133d0d2679"];
    for(var i=0; i< adminsL; i++){
      if((aids.indexOf(admins[i].aid+'') < 0) && (skipArrAids.indexOf(admins[i].aid+'') < 0)){
        aids.push(admins[i].aid+'');
      }
    }
    var skipArr = ["akshatg@eckovation.com","riteshs@eckovation.com","niranjan@eckovation.com"];
    Account.find({
      _id : {$in : aids},
      vrfy: true
    },function(err,emails){
      if(err){
        return cb(err,null);
      }
      var emailsL = emails.length;
      if(emailsL == 0){
        return cb(null,configs.PAYMENTS_TO);
      }
      var temp;
      var emailsArr = [configs.PAYMENT_MAILING_GROUP];
      for(var j=0; j<emailsL; j++){
        temp = emails[j].e;
        for(m=0; m<temp.length; m++){
          if(!temp[m] || temp[m] == ""  || temp[m] == null || temp[m] == "noemail@gmail.com" || (skipArr.indexOf(temp[m]) >= 0)){
            continue;
          } else {
            if(emailsArr.indexOf(temp[m]) < 0){
              emailsArr.push(temp[m]);
            }
          }
        }
      }
      if(emailsArr.length == 0){
        return cb(null,configs.PAYMENTS_TO);
      }
      return cb(null,emailsArr.join());
    });
  });
}

function prepareHtmlStringForpaymentStatus(data, cb){
  fs.readFile(PaymentConfigs.PAYMENT_STATUS_TEMPLATE_MAIL, function(err, html){
    if(err){ 
      return cb(err,null);
    }
    if(html){
      var html_string     = html.toString('utf8');
      var compiled        = _.template(html_string);
      var html_string = compiled({
        k1 : "ORDER_ID",
        v1 : data.ORDER_ID,
        k2 : "TRXN_AMOUNT",
        v2 : data.TRXN_AMOUNT,
        k3 : "MOBILE_NO",
        v3 : data.MOBILE_NO,
        k4 : "EMAIL",
        v4 : (!data.EMAIL) ? "NO_EMAIL" : data.EMAIL,
        k5 : "GROUP_NAME",
        v5 : data.GROUP_NAME,
        k6 : "GROUP_CODE",
        v6 : data.GROUP_CODE,
        k7 : "PACKAGE_NAME",
        v7 : data.PKG_NAME,
        k8 : "DURATION",
        v8 : data.UNIT +" "+data.BASE,
        k9 : "PACKAGE_PRICE",
        v9 : data.PACKAGE_AMOUNT,
        k10 : "MODE_OF_PAYMENT",
        v10 : data.MODE,
        k11 : "TRXN_STATUS",
        v11 : data.TRXN_STATUS,
        k12 : "GATEWAY_STATUS",
        v12 : data.GTWY_STATUS,
        k13 : "TRXN_RESPONSE(CUST)",
        v13 : data.STATUS_STRING,
        k14 : "TRXN_TIME",
        v14 : data.TRXN_START_TIME,
        k15 : "COUPON",
        v15 : data.COUPON,
        k16 : "PLATFORM",
        v16 : data.PLATFORM
      });
      return cb(null,html_string);
    } else {
      return cb("PaymentStatusTemplatePreparationFailed",null);
    }
  });
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : configs.MONGO_SERVER_KEEPALIVE
      }
    }
  };
  if(!configs.MONGO_UNAME || configs.MONGO_UNAME == ""){
    options.server.poolSize = configs.MONGO_LOWER_POOLSIZE;
    console.log('[[[[[[[    NO MONGO_UNAME FOUND IN CONFIGS, WILL CONNNECT WITHOUT REPLICA OPTIONS     ]]]]]]]]]');
    console.log('[[[[[[[    MONGODB CONNECTION POOL_SIZE : '+configs.MONGO_LOWER_POOLSIZE+'     ]]]]]]]]]');
    return mongoose.connect("mongodb://"+configs.MONGO_HOST+":"+configs.MONGO_PORT+"/"+configs.DB_NAME,options);
  }
  options.replset = {
    rs_name : configs.MONGO_REPLICA_SET_NAME,
    socketOptions : {
      keepAlive : configs.MONGO_REPLICA_KEEPALIVE
    },
    poolSize : configs.MONGO_LOWER_POOLSIZE
  };
  var connect_string = "mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@";
  var mongo_hosts    = configs.MONGO_HOST;
  var mongo_port     = configs.MONGO_PORT;
  for(var i=0; i<mongo_hosts.length; i++){
    if(i > 0)
      connect_string += ",";
    connect_string += mongo_hosts[i]+":"+mongo_port;
  }
  connect_string += "/"+configs.DB_NAME;
  console.log('[[[[[[[     CONNECTING MONGODB WITH REPLICA_SET OPTIONS     ]]]]]]]]]');
  console.log('[[[[[[[     MONGODB CONNECTION POOL_SIZE : '+configs.MONGO_LOWER_POOLSIZE+'      ]]]]]]]]]');
  return mongoose.connect(connect_string,options);
}
