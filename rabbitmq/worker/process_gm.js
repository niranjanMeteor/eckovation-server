var amqp                     = require('amqplib/callback_api');
var mongoose                 = require('mongoose');
var redis                    = require('redis');
var ip                       = require('ip');
var os                       = require("os");
var https                    = require('https');

var configs                  = require('../../utility/configs.js');
var RedisPrefixes            = require('../../utility/redis_prefix');
var mail_client              = require('../../utility/mail/mail_client.js');
var RabbitMqPublish          = require('../publisher/publisher.js');
var RabbitMqQueues           = require('../queues.js');
var dbConnectionAttempts     = 0;

var redis_client        = redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
  console.trace("Redis Connection Error " + err);
  var subject = 'PROCESS_GM_WORKER => Redis COnnection Failure'
  var rbody = '<h4>'+ Date()  +': Redis Connection Failed to connect Due to Follwoing error</h4>';
  rbody += '<h5>'+err+'</h5>';
  informDevTeam(subject, rbody, function(err){
    process.exit(0);
  });
});

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  var subject = 'PROCESS_GM_WORKER => MongoDb Connection Failure'
  var mbody = '<h4>'+ Date()  +': MongoDb Connection Failed to connect Due to Follwoing error</h4>';
  mbody += '<h5>'+err+'</h5>';
  if(dbConnectionAttempts == configs.DB_CONNECTION_RETTEMPT_LIMIT){
    informDevTeam(subject, mbody, function(err){
      process.exit(0);
    });
  } else {
    dbConnectionAttempts++;
    connectMongoDb();
  }
});
mongoose.connection.on('connected', function(){
  console.log('Successfully Opened MOngoDb Connection');
  dbConnectionAttempts = 0;
});

var rcq                         = require("../../utility/redis_chat_queue.js")(redis_client);
var rhmset                      = require("../../utility/redis_hmset.js")(redis_client);

require('../../models/groups.js');
require('../../models/messages.js');
require('../../models/groupmembers.js');
var Group                       = mongoose.model('Group');
var Message                     = mongoose.model('Message');
var GroupMember                 = mongoose.model('GroupMember');

var user                        = configs.RABBITMQ_USER;
var passwd                      = configs.RABBITMQ_PASSWORD;
var rabbitmq_url                = 'amqp://'+user+':'+passwd+'@'+configs.RABBITMQ_HOST+':'+configs.RABBITMQ_PORT;
var amqpConn                    = null;
var RABBIT_GROUP_MESSAGE_QUEUE  = RabbitMqQueues.GROUP_MESSAGE;
var RABBIT_NOTIFY_QUEUE         = RabbitMqQueues.PUSH_NOTIFY;
var RABBIT_STATUS_INSERT_QUEUE  = RabbitMqQueues.STATUS_INSERT;
var myIpAddres                  = ip.address(); 
var hostname                    = os.hostname();
var BASIC_MESSAGE_TYPES         = [Message.MESSAGE_TYPE.audio,Message.MESSAGE_TYPE.text,Message.MESSAGE_TYPE.video,Message.MESSAGE_TYPE.image];

startRabbitServer();

function startRabbitServer() {
  amqp.connect(rabbitmq_url + "?heartbeat=60", function(err, conn) {
    if (err) {
      console.error("[AMQP] GroupMessage Worker", err.message);
      console.trace(err);
      return setTimeout(startRabbitServer, 1000);
    }
    conn.on("error", function(err) {
      if (err.message !== "Connection closing") {
        console.error("[AMQP] GroupMessage Worker conn error", err.message);
        console.trace(err);
      }
    });
    conn.on("close", function() {
      console.error("[AMQP] reconnecting");
      return setTimeout(startRabbitServer, 1000);
    });
    console.log("[AMQP] GroupMessage Worker connected");
    amqpConn = conn;
    whenConnected();
  });
}

function whenConnected() {
  startWorker();
}

function startWorker() {
  amqpConn.createChannel(function(err, ch) {
    if(closeOnErr(err)) return;
    ch.on("error", function(err) {
      console.error("[AMQP] GroupMessage Worker channel error", err.message);
      console.trace(err);
    });
    ch.on("close", function() {
      console.log("[AMQP] GroupMessage Worker  channel closed");
    });
    
    ch.prefetch(10);
    ch.assertQueue(RABBIT_GROUP_MESSAGE_QUEUE, { durable: true }, function(err, _ok) {
      if(closeOnErr(err)) return;
      ch.consume(RABBIT_GROUP_MESSAGE_QUEUE, processMsg, { noAck: false });
      console.log("[AMQP] GroupMessage Worker is started");
    });

    function processMsg(msg) {
      work(msg, function(ok) {
        // try {
          if (ok)
            ch.ack(msg);
          else
            ch.reject(msg, true);
        // } catch (e) {
        //   closeOnErr(e);
        // }
      });
    }
  });
}

function closeOnErr(err){
  if(!err) return false;
  console.error("[AMQP] GroupMessage Worker error", err);
  console.trace(err);
  amqpConn.close();
  return true;
}

function work(msg, cb){
  var stim        = new Date().getTime();
  var msgOb       = JSON.parse(msg.content);
  var gid         = msgOb.gid;
  var mid         = msgOb.mid;
  var tim         = msgOb.tim;
  var notify_msg  = msgOb.msg;
  var pid         = msgOb.pid;
  var mid_eligible_paid = false;
  console.log('new msg object came for processing');
  console.log(mid);
  Message.findOne({
    _id : mid
  },function(err,message){
    if(err){
      console.log('Message Fetching Problem MongoDB');
      console.trace(err);
      console.log(msgOb);
      informDevTeamAbtMsgFail(msgOb, err);
      return cb(true);
    }
    if(!message){
      console.log('Message Not Found In MongoDB');
      console.log(msgOb);
      informDevTeamAbtMsgFail(msgOb, err);
      return cb(true);
    }
    Group.findOne({
      _id : gid,
      act : true
    },function(err,group){
      if(err){
        console.log('Group Fetching Problem MongoDB');
        console.log(msgOb);
        informDevTeamAbtMsgFail(msgOb, err);
        return cb(true);
      }
      if(!group){
        console.log('Group Not Found In MongoDB');
        console.log(msgOb);
        informDevTeamAbtMsgFail(msgOb, err);
        return cb(true);
      }
      if(group.req_sub && group.req_sub == true){
        if(BASIC_MESSAGE_TYPES.indexOf(message.type) >= 0){
          mid_eligible_paid = true;
        }
      }
      GroupMember.find({
        gid : gid,
        act : true,
        gdel: false,
        type: {$ne : GroupMember.TYPE.BANNED}
      },{
        pid    : 1,
        sub_ed : 1
      },function(err,groupmembers){
        if(err){
          console.log('GroupMember Fetching Problem MongoDB');
          console.trace(err);
          console.log(msgOb);
          informDevTeamAbtMsgFail(msgOb, err);
          return cb(true);
        }
        var total     = groupmembers.length;
        var r_done    = 0;
        var r_done_s  = 0;
        var r_done_e  = 0;
        var s_done    = 0;
        var s_done_s  = 0;
        var s_done_e  = 0;
        var unpaid_c  = 0;
        var gm_obj;

        gm_obj = new Buffer(JSON.stringify({
          // pid    : groupmembers[i].pid,
          gid    : gid,
          msg    : notify_msg
        }));
        RabbitMqPublish.publish("",RABBIT_NOTIFY_QUEUE,gm_obj);
        for(var i=0; i<total; i++){
          if(mid_eligible_paid && (!groupmembers[i].sub_ed || groupmembers[i].sub_ed < tim)){
            r_done++;
            s_done++;
            unpaid_c++;
            if(r_done == total && s_done == total){
              console.log('\n');
              console.log(msgOb);
              console.log('            *******************************************');
              console.log('Total GroupMembers to Process : '+total);
              console.log('RCQ Insertion , Attempted : '+r_done+', Success : '+r_done_s+', Error : '+r_done_e);
              console.log('STATUS Insertion , Attempted : '+s_done+', Success : '+s_done_s+', Error : '+s_done_e);
              if(mid_eligible_paid){
                console.log('GroupPaid : '+mid_eligible_paid+', msg_type : '+message.type+', skipped for unpaid count : '+unpaid_c);
              }
              var ntime = new Date().getTime();
              console.log('Total Time Taken For Worker Processing : '+(ntime-stim));
              var overallTime = ntime - (parseInt(tim));
              console.log('Total Time Taken For Overall Message Processing Completion : '+(overallTime));
              console.log('            *******************************************');
              console.log('\n');
              updateMessageEmittion(mid);
              postGroupSmsApi(message.type, group, pid, message.body);
              return cb(true);
            }
            continue;
          }
          save_status(groupmembers[i].pid, gid, pid, mid, tim, function(err,resp_s){
            if(err){
              s_done_e++;
            }
            if(resp_s){
              s_done_s++;
            }
            s_done++;
            if(r_done == total && s_done == total){
              console.log('\n');
              console.log(msgOb);
              console.log('            *******************************************');
              console.log('Total GroupMembers to Process : '+total);
              console.log('RCQ Insertion , Attempted : '+r_done+', Success : '+r_done_s+', Error : '+r_done_e);
              console.log('STATUS Insertion , Attempted : '+s_done+', Success : '+s_done_s+', Error : '+s_done_e);
              if(mid_eligible_paid){
                console.log('GroupPaid : '+mid_eligible_paid+', msg_type : '+message.type+', skipped for unpaid count : '+unpaid_c);
              }
              var ntime = new Date().getTime();
              console.log('Total Time Taken For Worker Processing : '+(ntime-stim));
              var overallTime = ntime - (parseInt(tim));
              console.log('Total Time Taken For Overall Message Processing Completion : '+(overallTime));
              console.log('            *******************************************');
              console.log('\n');
              updateMessageEmittion(mid);
              postGroupSmsApi(message.type, group, pid, message.body);
              return cb(true);
            }
          });
          add_in_rcq(groupmembers[i].pid, tim, mid, function(err,resp_r){
            if(err){
              r_done_e++;
            }
            if(resp_r){
              r_done_s++;
            }
            r_done++;
            if(r_done == total && s_done == total){
              console.log('\n');
              console.log(msgOb);
              console.log('            *******************************************');
              console.log('Total GroupMembers to Process : '+total);
              console.log('RCQ Insertion , Attempted : '+r_done+', Success : '+r_done_s+', Error : '+r_done_e);
              console.log('STATUS Insertion , Attempted : '+s_done+', Success : '+s_done_s+', Error : '+s_done_e);
              if(mid_eligible_paid){
                console.log('GroupPaid : '+mid_eligible_paid+', msg_type : '+message.type+', skipped for unpaid count : '+unpaid_c);
              }
              var ntime = new Date().getTime();
              console.log('Total Time Taken For Worker Processing : '+(ntime-stim));
              var overallTime = ntime - tim;
              console.log('Total Time Taken For Overall Message Processing Completion : '+(overallTime));
              console.log('            *******************************************');
              console.log('\n');
              updateMessageEmittion(mid);
              postGroupSmsApi(message.type, group, pid, message.body);
              return cb(true);
            }
          });
        }
      });
    });
  });
}

function updateMessageEmittion(mid){
  Message.update({
    _id : mid
  },{
    $inc : {
      nemit : 1
    }
  },function(err,updated_emit){
    if(err){
      console.log('Message Emit Update Failed');    
      console.trace(err);
    }
    return;
  });
}

function postGroupSmsApi(msg_type, group, pid, text){
  if(!group.i_gm_sms || group.i_gm_sms != true || !group.gm_sms_api){
    return;
  }
  if(msg_type != Message.MESSAGE_TYPE.text){
    return;
  }
  if(!group.gm_sms_api.h || !group.gm_sms_api.p || !group.gm_sms_api.pt){
    return;
  }
  GroupMember.findOne({
    gid : group._id,
    pid : pid,
    act : true,
    gdel: false
  },{
    pnam    : 1,
    type    : 1
  },function(err,groupmember){
    if(err){
      console.trace(err);
      return;
    }
    if(!groupmember){
      console.log('postGroupSmsApi : groupmember not found for pid : '+pid+', gid : '+group._id);
      return;
    }
    if(groupmember.type != GroupMember.TYPE.ADMIN){
      console.log('postGroupSmsApi : groupmember not ADMIN for pid : '+pid+', gid : '+group._id);
      return;
    }
    var params  = {
      gcode : group.code,
      gname : group.name,
      text  : text,
      pnam  : groupmember.pnam              
    };

    var options = {
      host       : group.gm_sms_api.h,
      port       : group.gm_sms_api.p,
      path       : group.gm_sms_api.pt,
      method     : 'POST'
    };

    try {
      var data = JSON.stringify(params);
      options.headers = {
        'Content-Type': 'application/json',
        'Content-Length': Buffer.byteLength(data)
      };
      if(group.gm_sms_api.id && group.gm_sms_api.sec){
        options.headers.client_id = group.gm_sms_api.id;
        options.headers.secret    = group.gm_sms_api.sec;
      }
      
      var req = https.request(options);
      req.write(data);

      var responseData = '';
      req.on('response', function(res){
        res.on('data', function(chunk){
         responseData += chunk;
        });
        res.on('end', function(){
          console.log(responseData);
        });
      });
      req.end();
      req.on('error', function(err){
        console.log(err);
        return;
      });
    } catch(excp){
      console.log(excp);
      return;
    }
  });
}

function add_in_rcq(pid, tim, mid, cb) {
  rcq.add(pid, tim, mid, function(err,resp){
    if(err){ 
      console.log('add_in_rcq failed for pid : '+pid+', mid : '+mid+', tim : '+tim);
      console.log(err); 
      return cb(true,null); 
    }
    if(!resp){
      console.log('add_in_rcq NOT QUEUED for pid : '+pid+', mid : '+mid+', tim : '+tim); 
      console.log(resp);
      return cb(null,false);
    }
    return cb(null,true);
  });
};

function save_status(pid, gid, data_pid, mid, tim, cb) {
  var pid = pid+'';
  rhmset.setStatusInsertJob(RedisPrefixes.STATUS_INSERT_JOB, mid+pid, mid, gid, pid, data_pid, tim, function(err,status_job){
    if(err){ 
      console.log('save_status_setStatusInsertJob failed for pid : '+pid+', mid : '+mid+', gid : '+gid+', data_pid : '+data_pid);
      console.log(err); 
      return cb(true,null);
    }
    if(!status_job) { 
      console.log('save_status_setStatusInsertJob NOT QUEUED for pid : '+pid+', mid : '+mid+', gid : '+gid+', data_pid : '+data_pid); 
      return cb(null,false); 
    }
    var gms_obj = new Buffer(JSON.stringify({
      pid        : pid,
      gid        : gid,
      data_pid   : data_pid,
      mid        : mid,
      tim        : tim
    }));
    RabbitMqPublish.publish("",RABBIT_STATUS_INSERT_QUEUE,gms_obj);
    return cb(null,true);
  });
};

function informDevTeamAbtMsgFail(msgOb, err){
  if(process.env.NODE_ENV == "ci" || process.env.NODE_ENV == "debug" || process.env.NODE_ENV == "dapi"){
    return;
  }
  if(typeof(err) == 'object'){
    err = JSON.stringify(err);
  }
  var subject     = myIpAddres+' : '+hostname+' : GM Worker could not deliver Msg Obj';
  var html_string = '<h4>'+ Date()  +' : Group Message Failed from GroupMessage Worker </h4>';
  html_string    += '<h5>'+JSON.stringify(msgOb)+'</h5><br><br><h5>'+err+'</h5>';
  var mailOptions = {
    from    : configs.OTP_FROM,
    to      : configs.TECH_TO,
    subject : subject, 
    html    : html_string
  };
  mail_client.send(mailOptions, function(err, response){
    if(err){
      console.trace(err);
      console.log('Informing Dev Team: Mail could be not sent');
      return;
    } else {
      console.log('Informing Dev Team: Mail sent succesfully');
      return;
    }
  });
}

function informDevTeam(subject, body, cb){
  var mailOptions = {
    from    : configs.RABBITMQ_WORKER_FROM,
    to      : configs.TECH_TO,
    subject : subject, 
    html    : body
  };
  mail_client.send(mailOptions, function(err, response){
    if(err){
      console.trace(err);
      console.log('Informing Dev Team: Mail could be not sent');
      return cb(err);
    } else {
      console.log('Informing Dev Team: Mail sent succesfully');
      return cb(null);
    }
  });
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : configs.MONGO_SERVER_KEEPALIVE
      }
    }
  };
  if(!configs.MONGO_UNAME || configs.MONGO_UNAME == ""){
    options.server.poolSize = configs.MONGO_LOWER_POOLSIZE;
    console.log('[[[[[[[    NO MONGO_UNAME FOUND IN CONFIGS, WILL CONNNECT WITHOUT REPLICA OPTIONS     ]]]]]]]]]');
    console.log('[[[[[[[    MONGODB CONNECTION POOL_SIZE : '+configs.MONGO_LOWER_POOLSIZE+'     ]]]]]]]]]');
    return mongoose.connect("mongodb://"+configs.MONGO_HOST+":"+configs.MONGO_PORT+"/"+configs.DB_NAME,options);
  }
  options.replset = {
    rs_name : configs.MONGO_REPLICA_SET_NAME,
    socketOptions : {
      keepAlive : configs.MONGO_REPLICA_KEEPALIVE
    },
    poolSize : configs.MONGO_LOWER_POOLSIZE
  };
  var connect_string = "mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@";
  var mongo_hosts    = configs.MONGO_HOST;
  var mongo_port     = configs.MONGO_PORT;
  for(var i=0; i<mongo_hosts.length; i++){
    if(i > 0)
      connect_string += ",";
    connect_string += mongo_hosts[i]+":"+mongo_port;
  }
  connect_string += "/"+configs.DB_NAME;
  console.log('[[[[[[[     CONNECTING MONGODB WITH REPLICA_SET OPTIONS     ]]]]]]]]]');
  console.log('[[[[[[[     MONGODB CONNECTION POOL_SIZE : '+configs.MONGO_LOWER_POOLSIZE+'      ]]]]]]]]]');
  return mongoose.connect(connect_string,options);
}
