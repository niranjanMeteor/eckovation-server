var amqp                  = require('amqplib/callback_api');
var mongoose              = require('mongoose');

var mail_client           = require('../../utility/mail/mail_client.js');
var configs               = require('../../utility/configs.js');
var RabbitMqQueues        = require('../queues.js');
var dbConnectionAttempts  = 0;

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  var subject = 'USER ACTIVITY WORKER => MongoDb Connection Failure'
  var mbody   = '<h4>'+ Date()  +': MongoDb Connection Failed to connect Due to Follwoing error</h4>';
  mbody      += '<h5>'+err+'</h5>';
  if(dbConnectionAttempts == configs.DB_CONNECTION_RETTEMPT_LIMIT){
    informDevTeamAboutConnectionError(subject, mbody, function(err){
      process.exit(0);
    });
  } else {
    dbConnectionAttempts++;
    connectMongoDb();
  }
});
mongoose.connection.on('connected', function(){
  console.log('Successfully Opened MOngoDb Connection');
  dbConnectionAttempts = 0;
});

require('../../models/accounts.js');
require('../../models/useractivity.js');

var UserActivity          = mongoose.model('UserActivity');

var user                  = configs.RABBITMQ_USER;
var passwd                = configs.RABBITMQ_PASSWORD;
var rabbitmq_url          = 'amqp://'+user+':'+passwd+'@'+configs.RABBITMQ_HOST+':'+configs.RABBITMQ_PORT;
var amqpConn              = null;
var user_activity_queue   = RabbitMqQueues.USER_ACTIVITY; 

startRabbitServer();

function startRabbitServer() {
  amqp.connect(rabbitmq_url + "?heartbeat=60", function(err, conn) {
    if (err) {
      console.error("[AMQP] USER ACTIVITY Worker", err.message);
      return setTimeout(startRabbitServer, 1000);
    }
    conn.on("error", function(err) {
      if (err.message !== "Connection closing") {
        console.error("[AMQP] USER ACTIVITY Worker conn error", err.message);
      }
    });
    conn.on("close", function() {
      console.error("[AMQP] reconnecting");
      return setTimeout(startRabbitServer, 1000);
    });
    console.log("[AMQP] USER ACTIVITY Worker connected");
    amqpConn = conn;
    whenConnected();
  });
}

function whenConnected() {
  startWorker();
}

function startWorker() {
  amqpConn.createChannel(function(err, ch) {
    if (closeOnErr(err)) return;
    ch.on("error", function(err) {
      console.error("[AMQP] USER ACTIVITY Worker channel error", err.message);
    });
    ch.on("close", function() {
      console.log("[AMQP] USER ACTIVITY worker  channel closed");
    });
    
    ch.prefetch(10);
    ch.assertQueue(user_activity_queue, { durable: true }, function(err, _ok) {
      if (closeOnErr(err)) return;
      ch.consume(user_activity_queue, processMsg, { noAck: false });
      console.log("[AMQP] USER ACTIVITY Worker is started");
    });

    function processMsg(msg) {
      work(msg, function(ok) {
        if (ok)
          ch.ack(msg);
        else
          ch.reject(msg, true);
      });
    }
  });
}

function closeOnErr(err) {
  if (!err) return false;
  console.error("[AMQP] USER ACTIVITY Worker error", err);
  amqpConn.close();
  return true;
}

function work(msg, cb) {
  var activity_obj  = JSON.parse(msg.content);
  var aid           = activity_obj.aid;
  var tim           = activity_obj.tim;
  var cl            = activity_obj.cl;
  var vrsn          = activity_obj.vrsn;
  var status        = activity_obj.stts;
  var emsg          = activity_obj.msg;
  var activity_name = activity_obj.nm;

  var new_user_activity = new UserActivity({
    aid   : aid,
    cl    : cl,
    vrsn  : vrsn,
    enam  : activity_name,
    stts  : status,
    emsg  : emsg,
    cat   : new Date(tim),
    tim   : tim
    // avtd  : {a:'abc',b:'bcd',c:'def'}
  });
  new_user_activity.save(function(err,new_activity){
    if(err){
      console.trace(err);
      console.log('User Activity Update failed');
      console.log(activity_obj);
    }
    if(new_activity){
      console.log('user activity sucessfully saved for aid : '+aid);
    } else {
      console.log('user activity could not be saved');
      console.log(activity_obj);
    }
    return cb(true);
  });
}

function informDevTeamAboutConnectionError(subject, body, cb){
  var mailOptions = {
    from    : configs.RABBITMQ_WORKER_FROM,
    to      : configs.TECH_TO,
    subject : subject, 
    html    : body
  };
  mail_client.send(mailOptions, function(err, response){
    if (err) {
      console.trace(err);
      console.log('Informing Dev Team: Mail could be not sent');
      return cb(err);
    } else {
      console.log('Informing Dev Team: Mail sent succesfully');
      return cb(null);
    }
  });
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : configs.MONGO_SERVER_KEEPALIVE
      }
    }
  };
  if(!configs.MONGO_UNAME || configs.MONGO_UNAME == ""){
    options.server.poolSize = configs.MONGO_POOLSIZE;
    console.log('[[[[[[[    NO MONGO_UNAME FOUND IN CONFIGS, WILL CONNNECT WITHOUT REPLICA OPTIONS     ]]]]]]]]]');
    console.log('[[[[[[[    MONGODB CONNECTION POOL_SIZE : '+configs.MONGO_POOLSIZE+'     ]]]]]]]]]');
    return mongoose.connect("mongodb://"+configs.MONGO_HOST+":"+configs.MONGO_PORT+"/"+configs.DB_NAME,options);
  }
  options.replset = {
    rs_name : configs.MONGO_REPLICA_SET_NAME,
    socketOptions : {
      keepAlive : configs.MONGO_REPLICA_KEEPALIVE
    },
    poolSize : configs.MONGO_POOLSIZE
  };
  var connect_string = "mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@";
  var mongo_hosts    = configs.MONGO_HOST;
  var mongo_port     = configs.MONGO_PORT;
  for(var i=0; i<mongo_hosts.length; i++){
    if(i > 0)
      connect_string += ",";
    connect_string += mongo_hosts[i]+":"+mongo_port;
  }
  connect_string += "/"+configs.DB_NAME;
  console.log('[[[[[[[     CONNECTING MONGODB WITH REPLICA_SET OPTIONS     ]]]]]]]]]');
  console.log('[[[[[[[     MONGODB CONNECTION POOL_SIZE : '+configs.MONGO_POOLSIZE+'      ]]]]]]]]]');
  return mongoose.connect(connect_string,options);
}
