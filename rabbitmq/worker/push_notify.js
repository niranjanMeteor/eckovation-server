require('http').globalAgent.maxSockets = Infinity;
var amqp                  = require('amqplib/callback_api');
var mongoose              = require('mongoose');

var RabbitMqQueues        = require('../queues.js');
var configs               = require('../../utility/configs.js');
var AwsSnsModule          = require('../../utility/aws_sns_module.js');
var mail_client           = require('../../utility/mail/mail_client.js');
var dbConnectionAttempts  = 0;

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  var subject = 'PUSH NOTIFY WORKER => MongoDb Connection Failure'
  var mbody   = '<h4>'+ Date()  +': MongoDb Connection Failed to connect Due to Follwoing error</h4>';
  mbody += '<h5>'+err+'</h5>';
  if(dbConnectionAttempts == configs.DB_CONNECTION_RETTEMPT_LIMIT){
    informDevTeam(subject, mbody, function(err){
      process.exit(0);
    });
  } else {
    dbConnectionAttempts++;
    connectMongoDb();
  }
});
mongoose.connection.on('connected', function(){
  console.log('Successfully Opened MOngoDb Connection');
  dbConnectionAttempts = 0;
});


var user                    = configs.RABBITMQ_USER;
var passwd                  = configs.RABBITMQ_PASSWORD;
var rabbitmq_url            = 'amqp://'+user+':'+passwd+'@'+configs.RABBITMQ_HOST+':'+configs.RABBITMQ_PORT;
var amqpConn                = null;
var push_notify_queue       = RabbitMqQueues.PUSH_NOTIFY;

startRabbitPushNotifyServer();

function startRabbitPushNotifyServer() {
  amqp.connect(rabbitmq_url + "?heartbeat=60", function(err, conn) {
    if (err) {
      console.error("[AMQP] Push Notify Worker", err.message);
      return setTimeout(startRabbitPushNotifyServer, 1000);
    }
    conn.on("error", function(err) {
      if (err.message !== "Connection closing") {
        console.error("[AMQP] Push Notify Worker conn error", err.message);
      }
    });
    conn.on("close", function() {
      console.error("[AMQP] Push Notify Worker reconnecting");
      return setTimeout(startRabbitPushNotifyServer, 1000);
    });
    console.log("[AMQP] Push Notify Worker connected");
    amqpConn = conn;
    whenConnected();
  });
}

function whenConnected() {
  startWorker();
}

function startWorker() {
  amqpConn.createChannel(function(err, ch) {
    if (closeOnErr(err)) return;
    ch.on("error", function(err) {
      console.error("[AMQP] Push Notify Worker channel error", err.message);
    });
    ch.on("close", function() {
      console.log("[AMQP] Push Notify worker  channel closed");
    });
    
    ch.prefetch(10);
    ch.assertQueue(push_notify_queue, { durable: true }, function(err, _ok) {
      if (closeOnErr(err)) return;
      ch.consume(push_notify_queue, processMsg, { noAck: false });
      console.log("[AMQP] Push Notify Worker is started");
    });

    function processMsg(msg) {
      work(msg, function(ok) {
        if (ok)
          ch.ack(msg);
        else
          ch.reject(msg, true);
      });
    }
  });
}

function closeOnErr(err) {
  if (!err) return false;
  console.error("[AMQP] Push Notify Worker error", err);
  amqpConn.close();
  return true;
}

function work(msg, cb){
  var msgOb      = JSON.parse(msg.content);
  if(msgOb.pid){
    AwsSnsModule.pushNotifyProfile(msgOb.pid, msgOb.gid, msgOb.msg, function(err,resp){
      if(err) { 
        console.log('Sending Push Notification in Group : '+msgOb.gid+' for pid :'+msgOb.pid+' was Unsuccessfull'); 
      } else {
        console.log('Sending Push Notification in Group : '+msgOb.gid+'  for pid : '+msgOb.pid+' was Successfull');
      }
      msgOb = null;
      return;
    });
  } else {
    AwsSnsModule.pushNotifyGroupMembers(msgOb.gid, msgOb.msg, function(err,resp){
      if(err) { 
        console.log('Sending Push Notification in Group : '+msgOb.gid+'  was Unsuccessfull'); 
      } else {
        console.log('Sending Push Notification in Group : '+msgOb.gid+'  was Successfull');
      }
      msgOb = null;
      return;
    });
  }
  cb(true);
}

// /*********************************   MEMWATCH  ****************************************/
// var log4jsLogger            = require('../../loggers/log4js_module.js');
// var log4jsConfigs           = require('../../loggers/log4js_configs.js');
// var logger                  = log4jsLogger.getLogger(log4jsConfigs.CATEGORY_MEMWATCH);
// var memwatch                = require('memwatch-next');
// memwatch.on('leak', function(info) {
//   logger.info({"leakdata":info});
// });
// var snapshotTaken = false,
//     hd;
// memwatch.on('stats', function(stats) {
//     logger.info({"stats":stats});
//     if(snapshotTaken==false){
//         hd = new memwatch.HeapDiff();
//         snapshotTaken = true;
//     } else {
//         var diff = hd.end();
//         snapshotTaken = false;
//         logger.info({"before":diff.before,"after":diff.after,"size_bytes":diff.change.size_bytes,"size":diff.change.size,"freed_nodes":diff.change.freed_nodes,"allocated_nodes":diff.change.allocated_nodes,"details":JSON.stringify(diff.change.details)});
//     }
// });
// /***************************************************************************************/

// /***********************  HEAPDUMP - Runs Every two hours  ********************************/
// var heapdump = require('heapdump');
// setInterval(function () {
//   heapdump.writeSnapshot();
// }, configs.NODE_SERVER_HEAPDUMP_INTERVAL);
// /***************************************************************************************/

function informDevTeam(subject, body, cb){
  var mailOptions = {
    from    : configs.PUSHNOTIFY_FROM,
    to      : configs.TECH_TO,
    subject : subject, 
    html    : body
  };
  mail_client.send(mailOptions, function(err, response){
    if (err) {
      console.trace(err);
      console.log('Informing Dev Team: Mail could be not sent');
      return cb(err);
    } else {
      console.log('Informing Dev Team: Mail sent succesfully');
      return cb(null);
    }
  });
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : configs.MONGO_SERVER_KEEPALIVE
      }
    }
  };
  if(!configs.MONGO_UNAME || configs.MONGO_UNAME == ""){
    options.server.poolSize = configs.MONGO_POOLSIZE;
    console.log('[[[[[[[    NO MONGO_UNAME FOUND IN CONFIGS, WILL CONNNECT WITHOUT REPLICA OPTIONS     ]]]]]]]]]');
    console.log('[[[[[[[    MONGODB CONNECTION POOL_SIZE : '+configs.MONGO_POOLSIZE+'     ]]]]]]]]]');
    return mongoose.connect("mongodb://"+configs.MONGO_HOST+":"+configs.MONGO_PORT+"/"+configs.DB_NAME,options);
  }
  options.replset = {
    rs_name : configs.MONGO_REPLICA_SET_NAME,
    socketOptions : {
      keepAlive : configs.MONGO_REPLICA_KEEPALIVE
    },
    poolSize : configs.MONGO_POOLSIZE
  };
  var connect_string = "mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@";
  var mongo_hosts    = configs.MONGO_HOST;
  var mongo_port     = configs.MONGO_PORT;
  for(var i=0; i<mongo_hosts.length; i++){
    if(i > 0)
      connect_string += ",";
    connect_string += mongo_hosts[i]+":"+mongo_port;
  }
  connect_string += "/"+configs.DB_NAME;
  console.log('[[[[[[[     CONNECTING MONGODB WITH REPLICA_SET OPTIONS     ]]]]]]]]]');
  console.log('[[[[[[[     MONGODB CONNECTION POOL_SIZE : '+configs.MONGO_POOLSIZE+'      ]]]]]]]]]');
  return mongoose.connect(connect_string,options);
}
