var amqp                = require('amqplib/callback_api');
var mongoose            = require('mongoose');
var redis               = require('redis');

var configs             = require('../../utility/configs.js');
var RedisPrefixes       = require('../../utility/redis_prefix');
var mail_client         = require('../../utility/mail/mail_client.js');
var dbConnectionAttempts  = 0;

var redis_client        = redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
  console.trace("Redis Connection Error " + err);
  var subject = 'STATUS_INSERT => Redis COnnection Failure'
  var rbody = '<h4>'+ Date()  +': Redis Connection Failed to connect Due to Follwoing error</h4>';
  rbody += '<h5>'+err+'</h5>';
  informDevTeam(subject, rbody, function(err){
    process.exit(0);
  });
});

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  var subject = 'STATUS_INSERT WORKER => MongoDb Connection Failure'
  var mbody = '<h4>'+ Date()  +': MongoDb Connection Failed to connect Due to Follwoing error</h4>';
  mbody += '<h5>'+err+'</h5>';
  if(dbConnectionAttempts == configs.DB_CONNECTION_RETTEMPT_LIMIT){
    informDevTeam(subject, mbody, function(err){
      process.exit(0);
    });
  } else {
    dbConnectionAttempts++;
    connectMongoDb();
  }
});
mongoose.connection.on('connected', function(){
  console.log('Successfully Opened MOngoDb Connection');
  dbConnectionAttempts = 0;
});

var RabbitMqQueues      = require('../queues.js');
var rrq                 = require("../../utility/redis_rabbit_queue.js")(redis_client);
var rhmset              = require("../../utility/redis_hmset.js")(redis_client);

require('../../models/groupmembers.js');
require('../../models/accounts.js');
require('../../models/statuses.js');

var GroupMember         = mongoose.model('GroupMember');
var Account             = mongoose.model('Account');
var Status              = mongoose.model('Status');

var user                = configs.RABBITMQ_USER;
var passwd              = configs.RABBITMQ_PASSWORD;
var rabbitmq_url        = 'amqp://'+user+':'+passwd+'@'+configs.RABBITMQ_HOST+':'+configs.RABBITMQ_PORT;
var amqpConn            = null;
var status_insert_queue = RabbitMqQueues.STATUS_INSERT;

startRabbitServer();

function startRabbitServer() {
  amqp.connect(rabbitmq_url + "?heartbeat=60", function(err, conn) {
    if (err) {
      console.error("[AMQP] GmStatus Worker", err.message);
      return setTimeout(startRabbitServer, 1000);
    }
    conn.on("error", function(err) {
      if (err.message !== "Connection closing") {
        console.error("[AMQP] GmStatus Worker conn error", err.message);
      }
    });
    conn.on("close", function() {
      console.error("[AMQP] reconnecting");
      return setTimeout(startRabbitServer, 1000);
    });
    console.log("[AMQP] GmStatus Worker connected");
    amqpConn = conn;
    whenConnected();
  });
}

function whenConnected() {
  startWorker();
}

function startWorker() {
  amqpConn.createChannel(function(err, ch) {
    if (closeOnErr(err)) return;
    ch.on("error", function(err) {
      console.error("[AMQP] GmStatus Worker channel error", err.message);
    });
    ch.on("close", function() {
      console.log("[AMQP] GmStatus Worker  channel closed");
    });
    
    ch.prefetch(10);
    ch.assertQueue(status_insert_queue, { durable: true }, function(err, _ok) {
      if (closeOnErr(err)) return;
      ch.consume(status_insert_queue, processMsg, { noAck: false });
      console.log("[AMQP] GmStatus Worker is started");
    });

    function processMsg(msg) {
      work(msg, function(ok) {
        // try {
          if (ok)
            ch.ack(msg);
          else
            ch.reject(msg, true);
        // } catch (e) {
        //   closeOnErr(e);
        // }
      });
    }
  });
}

function closeOnErr(err) {
  if (!err) return false;
  console.error("[AMQP] GmStatus Worker error", err);
  amqpConn.close();
  return true;
}

function work(msg, cb) {
  var msgOb = JSON.parse(msg.content);
  var new_status;
  var rtim = new Date(msgOb.tim);
  var stim = new Date(msgOb.tim);
  if(msgOb.pid == msgOb.data_pid){
    new_status = {
      pid : msgOb.pid,
      mid : msgOb.mid,
      gid : msgOb.gid,
      stts: Status.MESSAGE_STATUS.USER_SEEN,
      rtim: rtim,
      stim: stim,
      tim : msgOb.tim
    };
  } else {
    new_status = {
      pid : msgOb.pid,
      mid : msgOb.mid,
      gid : msgOb.gid,
      stts: Status.MESSAGE_STATUS.SERVER_RECEIVED,
      tim : msgOb.tim
    };
  }
  Status.update({
    pid : msgOb.pid,
    mid : msgOb.mid
  },{
    // $set :{
    //   rtim: msgOb.tim
    // },
    $setOnInsert : new_status
  },{
    upsert:true
  },function(err, status_inserted) {
    if(err) {
      console.log('RabbitMQ GmStatus Worker : error in inserting the status for packet');
      console.log(err);
      console.log(msgOb);
      console.log('RabbitMQ GmStatus Worker : msg object removed from rabbit queue and will be added to redis pending status insert queue');
      addIntoStatusInsertPending(msgOb.mid, msgOb.pid, msgOb.tim);
      return cb(true);
    }
    if(status_inserted.n == 0){
      console.log('RabbitMQ GmStatus Worker : status msg object is already inserted');
      console.log(msgOb);
    } else {
      console.log('RabbitMQ GmStatus Worker : succesfully inserted the status for msg object');
      console.log(msgOb);
    }
    removeFromStatusInsertJob(msgOb.mid, msgOb.pid, function(err,resp){
      if(!resp) {
        console.log('RabbitMQ GmStatus Worker : could not remove from rrq ');
        console.log(msgOb);
        console.log(resp);
      }
      return cb(true);
    });
  });
}

function removeFromStatusInsertJob(msg_id, pid, cb){
  rhmset.removekey(RedisPrefixes.STATUS_INSERT_JOB+msg_id+pid,function(err,resp){
    if(resp) return cb(null,true);
    return cb(true,resp);
  });
}

function addIntoStatusInsertPending(mid, pid, tim){
  rrq.addWithoutPrefix(RedisPrefixes.STATUS_INSERT_PENDING, tim, mid+pid, function(err,added){
    if(err) { console.trace(err); }
    if(added) { console.log('addIntoStatusInsertPending : added msg for status pending insert : '+mid+' and pid : '+pid); }
    return;
  });
}

function informDevTeam(subject, body, cb){
  var mailOptions = {
    from    : configs.RABBITMQ_WORKER_FROM,
    to      : configs.TECH_TO,
    subject : subject, 
    html    : body
  };
  mail_client.send(mailOptions, function(err, response){
    if (err) {
      console.trace(err);
      console.log('Informing Dev Team: Mail could be not sent');
      return cb(err);
    } else {
      console.log('Informing Dev Team: Mail sent succesfully');
      return cb(null);
    }
  });
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : configs.MONGO_SERVER_KEEPALIVE
      }
    }
  };
  if(!configs.MONGO_UNAME || configs.MONGO_UNAME == ""){
    options.server.poolSize = configs.MONGO_POOLSIZE;
    console.log('[[[[[[[    NO MONGO_UNAME FOUND IN CONFIGS, WILL CONNNECT WITHOUT REPLICA OPTIONS     ]]]]]]]]]');
    console.log('[[[[[[[    MONGODB CONNECTION POOL_SIZE : '+configs.MONGO_POOLSIZE+'     ]]]]]]]]]');
    return mongoose.connect("mongodb://"+configs.MONGO_HOST+":"+configs.MONGO_PORT+"/"+configs.DB_NAME,options);
  }
  options.replset = {
    rs_name : configs.MONGO_REPLICA_SET_NAME,
    socketOptions : {
      keepAlive : configs.MONGO_REPLICA_KEEPALIVE
    },
    poolSize : configs.MONGO_POOLSIZE
  };
  var connect_string = "mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@";
  var mongo_hosts    = configs.MONGO_HOST;
  var mongo_port     = configs.MONGO_PORT;
  for(var i=0; i<mongo_hosts.length; i++){
    if(i > 0)
      connect_string += ",";
    connect_string += mongo_hosts[i]+":"+mongo_port;
  }
  connect_string += "/"+configs.DB_NAME;
  console.log('[[[[[[[     CONNECTING MONGODB WITH REPLICA_SET OPTIONS     ]]]]]]]]]');
  console.log('[[[[[[[     MONGODB CONNECTION POOL_SIZE : '+configs.MONGO_POOLSIZE+'      ]]]]]]]]]');
  return mongoose.connect(connect_string,options);
}
