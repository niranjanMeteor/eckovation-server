var amqp                = require('amqplib/callback_api');
var mongoose            = require('mongoose');
var redis               = require('redis');

var configs             = require('../../utility/configs.js');
var RedisPrefixes       = require('../../utility/redis_prefix');
var mail_client         = require('../../utility/mail/mail_client.js');
var dbConnectionAttempts  = 0;

var redis_client        = redis.createClient(configs.REDIS_PORT,configs.REDIS_HOST);
redis_client.auth(configs.REDIS_PASS);
redis_client.select(configs.REDIS_CHAT_DB, function() { /* ... */ });

redis_client.on("error", function (err) {
  console.trace("Redis Connection Error " + err);
  var subject = 'STATUS_UPDATE WORKER => Redis COnnection Failure'
  var rbody = '<h4>'+ Date()  +': Redis Connection Failed to connect Due to Follwoing error</h4>';
  rbody += '<h5>'+err+'</h5>';
  informDevTeam(subject, rbody, function(err){
    process.exit(0);
  });
});

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  var subject = 'STATUS_UPDATE WORKER => MongoDb Connection Failure'
  var mbody = '<h4>'+ Date()  +': MongoDb Connection Failed to connect Due to Follwoing error</h4>';
  mbody += '<h5>'+err+'</h5>';
  if(dbConnectionAttempts == configs.DB_CONNECTION_RETTEMPT_LIMIT){
    informDevTeam(subject, mbody, function(err){
      process.exit(0);
    });
  } else {
    dbConnectionAttempts++;
    connectMongoDb();
  }
});
mongoose.connection.on('connected', function(){
  console.log('Successfully Opened MOngoDb Connection');
  dbConnectionAttempts = 0;
});


var RabbitMqQueues      = require('../queues.js');
var rrq                 = require("../../utility/redis_rabbit_queue.js")(redis_client);
var rhmset              = require("../../utility/redis_hmset.js")(redis_client);

require('../../models/groupmembers.js');
require('../../models/accounts.js');
require('../../models/statuses.js');
var GroupMember         = mongoose.model('GroupMember');
var Account             = mongoose.model('Account');
var Status              = mongoose.model('Status');


var user                = configs.RABBITMQ_USER;
var passwd              = configs.RABBITMQ_PASSWORD;
var rabbitmq_url        = 'amqp://'+user+':'+passwd+'@'+configs.RABBITMQ_HOST+':'+configs.RABBITMQ_PORT;
var amqpConn            = null;
var status_update_queue = RabbitMqQueues.STATUS_UPDATE; 

startRabbitServer();

function startRabbitServer() {
  amqp.connect(rabbitmq_url + "?heartbeat=60", function(err, conn) {
    if (err) {
      console.error("[AMQP] Status Update Worker", err.message);
      return setTimeout(startRabbitServer, 1000);
    }
    conn.on("error", function(err) {
      if (err.message !== "Connection closing") {
        console.error("[AMQP] Status Update Worker conn error", err.message);
      }
    });
    conn.on("close", function() {
      console.error("[AMQP] reconnecting");
      return setTimeout(startRabbitServer, 1000);
    });
    console.log("[AMQP] Status Update Worker connected");
    amqpConn = conn;
    whenConnected();
  });
}

function whenConnected() {
  startWorker();
}

function startWorker() {
  amqpConn.createChannel(function(err, ch) {
    if (closeOnErr(err)) return;
    ch.on("error", function(err) {
      console.error("[AMQP] Status Update Worker channel error", err.message);
    });
    ch.on("close", function() {
      console.log("[AMQP] Status Update worker  channel closed");
    });
    
    ch.prefetch(10);
    ch.assertQueue(status_update_queue, { durable: true }, function(err, _ok) {
      if (closeOnErr(err)) return;
      ch.consume(status_update_queue, processMsg, { noAck: false });
      console.log("[AMQP] Status Update Worker is started");
    });

    function processMsg(msg) {
      work(msg, function(ok) {
        if (ok)
          ch.ack(msg);
        else
          ch.reject(msg, true);
      });
    }
  });
}

function closeOnErr(err) {
  if (!err) return false;
  console.error("[AMQP] Status Update Worker error", err);
  amqpConn.close();
  return true;
}

function setCharAt(str,index,chr) {
    if(index > str.length-1) return str;
    return str.substr(0,index) + chr + str.substr(index+1);
}

function work(msg, cb) {
  var cmsg          = msg.content.toString();
  var lastStr       = cmsg[cmsg.length-1];
  var secondlastStr = cmsg[cmsg.length-2];
  cmsg              = setCharAt(cmsg,cmsg.length-1,'');
  //console.log(JSON.parse(cmsg));
  var msgOb;
  if(lastStr != '}'){
    msgOb = JSON.parse(cmsg);
  } else if(secondlastStr == '}'){
    sgOb = JSON.parse(cmsg);
  } else {
    msgOb = JSON.parse(msg.content);
  }
  Status.update({
    pid : msgOb.pid,
    mid : msgOb.mid
  },{
    $set :{
      stts: Status.MESSAGE_STATUS.USER_SEEN,
      stim: msgOb.stim
    },
    $setOnInsert : {
      pid : msgOb.pid,
      mid : msgOb.mid,
      gid : msgOb.gid,
      tim : msgOb.tim
    }
  },{
    safe:true,
    multi:false,
    upsert:true
  },function(err, seen_update) {
    if(err) {
      console.log('RabbitMQ Worker : error in updating the status for packet');
      console.log(err);
      console.log(msgOb);
      console.log('RabbitMQ Worker : msg object removed from rabbit queue and will be added to redis pending status update queue');
      addIntoStatusUpdatePending(msgOb.mid, msgOb.pid, msgOb.tim);
      return cb(true);
    }
    if(seen_update.n == 0 && seen_update.nModified == 0){
      console.log('RabbitMQ Worker : seen already upto date for msg object');
      console.log(msgOb);
    } else {
      console.log('RabbitMQ Worker : succesfully updated the status for msg object');
      console.log(msgOb);
    }
    removeFromSeenUpdateJob(msgOb.mid, msgOb.pid, function(err,resp){
      if(!resp) {
        console.log('RabbitMQ Worker : could not remove from rrq ');
        console.log(msgOb);
        console.log(resp);
      }
      return cb(true);
    });
  });
}

function removeFromSeenUpdateJob(msg_id, pid, cb){
  rhmset.removekey(RedisPrefixes.STATUS_UPDATE_JOB+msg_id+pid,function(err,resp){
    if(resp) return cb(null,true);
    return cb(true,resp);
  });
}

function addIntoStatusUpdatePending(mid, pid, tim){
  rrq.addWithoutPrefix(RedisPrefixes.STATUS_UPDATE_PENDING, tim, mid+pid, function(err,added){
    if(err) { console.trace(err); }
    if(added) { console.log('addIntoStatusUpdatePending : added msg for status pending update : '+mid+' and pid : '+pid); }
    return;
  });
}

function informDevTeam(subject, body, cb){
  var mailOptions = {
    from    : configs.RABBITMQ_WORKER_FROM,
    to      : configs.TECH_TO,
    subject : subject, 
    html    : body
  };
  mail_client.send(mailOptions, function(err, response){
    if (err) {
      console.trace(err);
      console.log('Informing Dev Team: Mail could be not sent');
      return cb(err);
    } else {
      console.log('Informing Dev Team: Mail sent succesfully');
      return cb(null);
    }
  });
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : configs.MONGO_SERVER_KEEPALIVE
      }
    }
  };
  if(!configs.MONGO_UNAME || configs.MONGO_UNAME == ""){
    options.server.poolSize = configs.MONGO_POOLSIZE;
    console.log('[[[[[[[    NO MONGO_UNAME FOUND IN CONFIGS, WILL CONNNECT WITHOUT REPLICA OPTIONS     ]]]]]]]]]');
    console.log('[[[[[[[    MONGODB CONNECTION POOL_SIZE : '+configs.MONGO_POOLSIZE+'     ]]]]]]]]]');
    return mongoose.connect("mongodb://"+configs.MONGO_HOST+":"+configs.MONGO_PORT+"/"+configs.DB_NAME,options);
  }
  options.replset = {
    rs_name : configs.MONGO_REPLICA_SET_NAME,
    socketOptions : {
      keepAlive : configs.MONGO_REPLICA_KEEPALIVE
    },
    poolSize : configs.MONGO_POOLSIZE
  };
  var connect_string = "mongodb://"+configs.MONGO_UNAME+":"+configs.MONGO_PASS+"@";
  var mongo_hosts    = configs.MONGO_HOST;
  var mongo_port     = configs.MONGO_PORT;
  for(var i=0; i<mongo_hosts.length; i++){
    if(i > 0)
      connect_string += ",";
    connect_string += mongo_hosts[i]+":"+mongo_port;
  }
  connect_string += "/"+configs.DB_NAME;
  console.log('[[[[[[[     CONNECTING MONGODB WITH REPLICA_SET OPTIONS     ]]]]]]]]]');
  console.log('[[[[[[[     MONGODB CONNECTION POOL_SIZE : '+configs.MONGO_POOLSIZE+'      ]]]]]]]]]');
  return mongoose.connect(connect_string,options);
}
