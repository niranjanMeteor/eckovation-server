var Queues = {
	STATUS_UPDATE 				: 'status_update:',
	MEDIA_CONVERSION      : 'media_conversion:',
	PUSH_NOTIFY           : 'push_notify:',
	STATUS_INSERT         : 'status_insert:',
	OTP_DELIVERY          : 'otp_delivery:',
	EMIT_RCQ              : 'emit_rcq:',
	USER_ACTIVITY         : 'user-activity:',
	OTP_CALL_ME           : 'otp_call:',
	GROUP_MESSAGE         : 'group_message:',
	PAYMENT_NOTIFY        : 'payment_notify:'
};

module.exports = Queues;