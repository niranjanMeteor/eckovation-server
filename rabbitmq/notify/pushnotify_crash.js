var mail_client 			= require('../../utility/mail/mail_client.js');
var configs					= require('../../utility/configs.js');
var ip                      = require('ip');
var os                      = require("os");

var html_string = '<h4>'+  Date() +' RabbitMq PUSH_NOTIFY Service has Crashed </h4>';
var myIpAddres  = ip.address(); 
var hostname    = os.hostname();

var mailOptions = {
    from    : configs.TECH_FROM,
    to      : configs.TECH_TO,
    subject : myIpAddres+' : '+hostname+' : RabbitMq PUSH_NOTIFY Service Crashed',
    html    : html_string
};

mail_client.send(mailOptions, function(err, response){
    if (err) {
    	console.tace(err);
        console.trace('Server Crash : Mail not sent successfully',err);
    } else {
        console.log('Server Crash : Mail sent successfully');
    }
});
