var mail_client             = require('../../utility/mail/mail_client.js');
var configs                 = require('../../utility/configs.js');
var ip                      = require('ip');
var os                      = require("os");

var html_string = '<h4>'+ Date()  +': RabbitMq MEDIA Service has Restarted </h4>';
var myIpAddres  = ip.address(); 
var hostname    = os.hostname();

var mailOptions = {
    from    : configs.TECH_FROM,
    to      : configs.TECH_TO,
    subject : myIpAddres+' : '+hostname+' : RabbitMq MEDIA Service Restarted',
    html    : html_string
};

mail_client.send(mailOptions, function(err, response){
    if (err) {
    	console.trace(err);
        console.log('Server Restart : Mail not sent successfully',err);
    } else {
        console.log('Server Restart : Mail sent successfully');
    }
});