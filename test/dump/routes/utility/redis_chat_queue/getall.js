if(process.env.NODE_ENV != "ci"){
	process.exit(1);
}

var redis_client 	= require("fakeredis").createClient("", "");
var rcq 			= require("./../../../utility/redis_chat_queue.js")(redis_client);
var _ 				= require("underscore");

var assert = require('assert');

describe('redis_chat_queue/getall.js',function(){
	describe('single pid',function() {
		var pid1 = "p1";
		//same score, multiple mid
		describe('getall with multiple mids and same timestamps', function() {
			before(function(done){
				var data = [];
				data.push({ pid : pid1, score: 100, data : "m1" });
				data.push({ pid : pid1, score: 100, data : "m2" });
				data.push({ pid : pid1, score: 100, data : "m3" });

				insert_redis_data(data,done);
			});

			it('returned mids are correct', function(done) {
				rcq.getall(pid1,function(err,data){
					assert(_.isEqual(data,["m1","m2","m3"]));
					done();
				});
			});

			after(function(done){
				rcq.removeAllObjs(pid1,done);
			});
		});

		//same score, same mid
		describe('getall with same mids and same timestamps', function() {
			before(function(done){
				var data = [];
				data.push({ pid : pid1, score: 100, data : "m1" });
				data.push({ pid : pid1, score: 100, data : "m1" });
				data.push({ pid : pid1, score: 100, data : "m1" });

				insert_redis_data(data,done);
			});

			it('returned mids are correct', function(done) {
				rcq.getall(pid1,function(err,data){
					assert(_.isEqual(data,["m1"]));
					done();
				});
			});

			after(function(done){
				rcq.removeAllObjs(pid1,done);
			});
		});

		//diff score, same mid
		describe('getall with same mids and different timestamps', function() {
			before(function(done){
				var data = [];
				data.push({ pid : pid1, score: 110, data : "m1" });
				data.push({ pid : pid1, score: 100, data : "m1" });
				data.push({ pid : pid1, score: 120, data : "m1" });

				insert_redis_data(data,done);
			});

			it('returned mids are correct', function(done) {
				rcq.getall(pid1,function(err,data){
					assert(_.isEqual(data,["m1"]));
					done();
				});
			});

			after(function(done){
				rcq.removeAllObjs(pid1,done);
			});
		});

		describe('different mids and different timestamps', function() {
			before(function(done){
				var data = [];
				data.push({ pid : pid1, score: 1000, data : "m1" });
				data.push({ pid : pid1, score: 1050, data : "m2" });
				data.push({ pid : pid1, score: 900, data : "m3" });

				insert_redis_data(data,done);
			});

			it('returned mids are correct', function(done) {
				rcq.getall(pid1,function(err,data){
					assert(_.isEqual(data,["m3","m1","m2"]));
					done();
				});
			});

			after(function(done){
				rcq.removeAllObjs(pid1,done);
			});
		});
	});
});

var insert_redis_data = function(datas,cb) {
	var inserted = 0;

	datas.forEach(function(data){
		rcq.add(data.pid,data.score,data.data,function(){
			inserted++;
			if(inserted>=datas.length) {
				cb();
			}
		});
	});
}