// if(process.env.NODE_ENV != "dapi"){
// 	process.exit(1);
// }

var mongoose 						= require('mongoose');
var AppClients					= require('../../../utility/app_clients.js');

require('../../../models/accounts.js');
require('../../../models/otps.js');

var Account 						= mongoose.model('Account');
var Otp 								= mongoose.model('Otp');

exports.verify_otp = function(cb){
	var condition = {
		_id  : { $exists : true},
		m    : { $exists : true}
	};
	getRandomAccount(condition,function(err,account){
		if(err){
			console.trace(err);
			return cb(err,null);
		}
		prepareParamsForVerifyOtp(account, function(err,paramObject){
			if(err){
				console.trace(err);
				return cb(err,null);
			}
			return cb(null,paramObject);
		});
	});
};

exports.verify_otp_v16 = function(cb){
	var condition = {
		_id  : { $exists : true},
		m    : { $exists : true}
	};
	getRandomAccount(condition,function(err,account){
		if(err){
			console.trace(err);
			return cb(err,null);
		}
		prepareParamsForVerifyOtp(account, function(err,paramObject){
			if(err){
				console.trace(err);
				return cb(err,null);
			}
			paramObject.cl     = AppClients.IOS;
			paramObject.vrsn   = "3.0.1";
			paramObject.clv    = "9.2";
			paramObject.d_id   = "FF0EB765-FE72-468E-8DD4-303347079D15";
			return cb(null,paramObject);
		});
	});
};

exports.v_otp_w = function(cb){
	var condition = {
		_id  : { $exists : true},
		// webv : { $exists : true},
		m    : { $exists : true}
	};
	getRandomAccount(condition,function(err,account){
		if(err){
			console.trace(err);
			return cb(err,null);
		}
		prepareParamsForVerifyOtp(account, function(err,paramObject){
			if(err){
				console.trace(err);
				return cb(err,null);
			}
			paramObject.cl     = AppClients.WEB;
			paramObject.vrsn   = "1.0.0";
			paramObject.clv    = "Mozilla/5.0 (Linux; Android 5.1.1; ONE A2003 Build/LMY47V) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.95 Mobile Safari/537.36";
			paramObject.d_id   = "476bbaf44cb6fa50161f15fcb7aeeeb5";
			return cb(null,paramObject);
		});
	});
};

exports.v_otp_a = function(cb){
	var condition = {
		_id  : { $exists : true},
		m    : { $exists : true}
	};
	getRandomAccount(condition,function(err,account){
		if(err){
			console.trace(err);
			return cb(err,null);
		}
		prepareParamsForVerifyOtp(account, function(err,paramObject){
			if(err){
				console.trace(err);
				return cb(err,null);
			}
			paramObject.cl     = AppClients.ANDROID;
			paramObject.vrsn   = "3.0.1";
			paramObject.clv    = "6.0";
			paramObject.d_id   = "FF0EB765-FE72-468E-8DD4-303347079D15";
			return cb(null,paramObject);
		});
	});
};

function prepareParamsForVerifyOtp(account, cb){
	var params      = {};
	params.aid 			= account._id+'';
	params.tokn 		= '5';

	Otp.update({},{
		gnat : new Date().getTime()
	},{
		multi : true
	},function(err,resp){
		if(err){
			return cb(err,null);
		}
		Otp.findOne({
			aid : account._id,
			cdst: Otp.STATUS.NOTUSED
		},function(err,otp_obj){
			if(err){
				return cb(err,null);
			}
			if(!otp_obj){
				return cb('Not Otp Object Found',null);
			}
			if(typeof(otp_obj.code) === 'undefined' || otp_obj.code == null){
				return cb('otp Code not found in otp object',null);
			}
			params.otp = otp_obj.code;
			return cb(null, params)
		});
	});
}

function getRandomAccount(condition, cb){
	Account.count(condition,function(err,count){
		if(err){
			return cb(err,null);
		}
		if(count == 0){
			return cb('No Sample Testing Account Found',null);
		}
		var rand = randomNum(count);
		Account.find(condition,{},{
			skip  : rand,
			limit : 1
		},function(err,account){
			if(err){
				return cb(err,null);
			}
			if(account.length == 0){
				return cb('No Random Account Found From Sample Testing Accounts',null);
			}
			return  cb(null,account[0]);
		});
	});
}

function randomNum(count){
	return Math.floor( Math.random() * count );
}