// if(process.env.NODE_ENV != "dapi"){
// 	process.exit(1);
// }

var mongoose 						= require('mongoose');
var AppClients					= require('../../../utility/app_clients.js');
var AuthModule					= require('../../../utility/auth/auth_tokens.js');

require('../../../models/accounttokens.js');

var AccountToken 			= mongoose.model('AccountToken');

exports.g_rt = function(cb){
	AccountToken.findOne({
		clnt : AppClients.IOS
	},function(err,account_token){
		if(err){
			return cb(err,null);
		}
		var params        = {};
		params.a_id 			= account_token.aid+'';
		params.d_id 			= account_token.udid;
		params.k_ey 			= account_token.skey;
		params.cl 			  = account_token.clnt;
		params.clv 			  = account_token.clv;
		params.vrsn 			= account_token.vrsn;
		return cb(null,params);
	});
};

exports.g_at = function(cb){
	AccountToken.findOne({
		clnt : AppClients.IOS
	},function(err,account_token){
		if(err){
			return cb(err,null);
		}
		var params        = {};
		params.a_id 			= account_token.aid+'';
		params.d_id 			= account_token.udid;
		params.cl 			  = account_token.clnt;
		params.rt 			  = AuthModule.getRT({id:account_token.aid,cl:account_token.clnt},AppClients.IOS);
		AccountToken.update({
			aid : account_token.aid,
			udid: account_token.udid,
			tokn: account_token.tokn
		},{
			tokn : params.rt
		},function(err,resp){
			if(err){
				return cb(err,null);
			}
			return cb(null,params);
		});
	});
};

exports.g_at_w = function(cb){
	AccountToken.findOne({
		clnt : AppClients.WEB
	},function(err,account_token){
		if(err){
			return cb(err,null);
		}
		var params        = {};
		params.a_id 			= account_token.aid+'';
		params.d_id 			= account_token.udid;
		params.cl 			  = account_token.clnt;
		params.rt 			  = AuthModule.getRT({id:account_token.aid,cl:account_token.clnt},AppClients.WEB);
		params.k_ey 			= account_token.skey;
		params.at         = AuthModule.getAT({id:account_token.aid,cl:account_token.clnt},AppClients.WEB);
		AccountToken.update({
			aid : account_token.aid,
			udid: account_token.udid,
			tokn: account_token.tokn
		},{
			tokn : params.rt
		},function(err,resp){
			if(err){
				return cb(err,null);
			}
			return cb(null,params);
		});
	});
};

