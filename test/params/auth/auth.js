// if(process.env.NODE_ENV != "dapi"){
// 	process.exit(1);
// }

var mongoose 						= require('mongoose');
var AppClients					= require('../../../utility/app_clients.js');
var AuthModule					= require('../../../utility/auth/auth_tokens.js');
var UnsecureAccountsModule  = require('../../../utility/auth/unsecure_accounts.js');

require('../../../models/accounttokens.js');
require('../../../models/unsecureaccounts.js');

var AccountToken 			= mongoose.model('AccountToken');
var UnsecureAccount 		= mongoose.model('UnsecureAccount');

exports.rt_i = function(cb){
	AccountToken.findOne({
		clnt : AppClients.IOS
	},function(err,account_token){
		if(err){
			return cb(err,null);
		}
		var params        = {};
		params.a_id 			= account_token.aid+'';
		params.d_id 			= account_token.udid;
		params.k_ey 			= account_token.skey;
		params.cl 			  = account_token.clnt;
		params.clv 			  = account_token.clv;
		params.vrsn 			= account_token.vrsn;
		return cb(null,params);
	});
};

exports.at_i = function(cb){
	AccountToken.findOne({
		clnt : AppClients.IOS
	},function(err,account_token){
		if(err){
			return cb(err,null);
		}
		var params        = {};
		params.a_id 			= account_token.aid+'';
		params.d_id 			= account_token.udid;
		params.cl 			  = account_token.clnt;
		params.rt 			  = AuthModule.getRT({id:account_token.aid,cl:account_token.clnt},AppClients.IOS);
		AccountToken.update({
			aid : account_token.aid,
			udid: account_token.udid,
			tokn: account_token.tokn
		},{
			tokn : params.rt
		},function(err,resp){
			if(err){
				return cb(err,null);
			}
			return cb(null,params);
		});
	});
};

exports.at_w = function(cb){
	AccountToken.findOne({
		clnt : AppClients.WEB
	},function(err,account_token){
		if(err){
			return cb(err,null);
		}
		var params        = {};
		params.a_id 			= account_token.aid+'';
		params.d_id 			= account_token.udid;
		params.cl 			  = account_token.clnt;
		params.rt 			  = AuthModule.getRT({id:account_token.aid,cl:account_token.clnt},AppClients.WEB);
		params.k_ey 			= account_token.skey;
		params.at         = AuthModule.getAT({id:account_token.aid,cl:account_token.clnt},AppClients.WEB);
		AccountToken.update({
			aid : account_token.aid,
			udid: account_token.udid,
			tokn: account_token.tokn
		},{
			tokn : params.rt
		},function(err,resp){
			if(err){
				return cb(err,null);
			}
			return cb(null,params);
		});
	});
};

exports.at_a = function(cb){
	AccountToken.findOne({
		clnt : AppClients.ANDROID
	},function(err,account_token){
		if(err){
			return cb(err,null);
		}
		var params        = {};
		params.a_id 			= account_token.aid+'';
		params.d_id 			= account_token.udid;
		params.cl 			  = account_token.clnt;
		params.rt 			  = AuthModule.getRT({id:account_token.aid,cl:account_token.clnt},AppClients.ANDROID);
		params.k_ey 			= account_token.skey;
		params.at         = AuthModule.getAT({id:account_token.aid,cl:account_token.clnt},AppClients.ANDROID);
		AccountToken.update({
			aid : account_token.aid,
			udid: account_token.udid,
			tokn: account_token.tokn
		},{
			tokn : params.rt
		},function(err,resp){
			if(err){
				return cb(err,null);
			}
			return cb(null,params);
		});
	});
};

exports.prov_tk = function(cb){
	UnsecureAccount.findOne({
		ack : false
	},function(err,unsecure_account){
		if(err){
			return cb(err,null);
		}
		AccountToken.findOne({
			aid: unsecure_account.aid,
			clnt : AppClients.ANDROID
		},function(err,account_token){
			if(err){
				return cb(err,null);
			}
			var params        = {};
			params.a_id 			= account_token.aid+'';
			params.d_id 			= account_token.udid;
			params.cl 			  = account_token.clnt;
			params.clv 			  = account_token.clv;
			params.vrsn 			= account_token.vrsn;
			return cb(null,params);
		});
	});
};

exports.ack_pvtk = function(cb){
	UnsecureAccount.findOne({
		ack : false
	},function(err,unsecure_account){
		if(err){
			return cb(err,null);
		}
		AccountToken.findOne({
			aid: unsecure_account.aid,
			clnt : AppClients.ANDROID
		},function(err,account_token){
			if(err){
				return cb(err,null);
			}
			var params        = {};
			params.a_id 			= account_token.aid+'';
			params.d_id 			= account_token.udid;
			params.cl 			  = account_token.clnt;
			params.clv 			  = account_token.clv;
			params.vrsn 			= account_token.vrsn;
			UnsecureAccountsModule.add(account_token.aid+'',AppClients.ANDROID,function(err,resp){
				if(err){
					return cb(err,null);
				}
				return cb(null,params);
			});
		});
	});
};

exports.acc_unlk = function(cb){
	AccountToken.findOne({
		clnt : AppClients.ANDROID
	},function(err,account_token){
		if(err){
			return cb(err,null);
		}
		var params        = {};
		params.a_id 			= account_token.aid+'';
		params.d_id 			= account_token.udid;
		params.cl 			  = account_token.clnt;
		params.rt 			  = account_token.tokn;
		params.k_ey 			= account_token.skey;
		params.at         = AuthModule.getExpAT({id:account_token.aid,cl:account_token.clnt},AppClients.ANDROID);
		setTimeout(function() {cb(null,params)}, 2000);
		// return cb(null,params);
	});
};

