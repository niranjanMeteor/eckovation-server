// if(process.env.NODE_ENV != "dapi"){
//   process.exit(1);
// }

var chai                          = require('chai');
var chaiHttp                      = require('chai-http');
var should                        = chai.should();
var AccountApiParams              = require('../../params/auth/auth.js');
var DataMethods                   = require('../../data_methods.js');
var server                        = 'http://localhost:'+3004;

chai.use(chaiHttp);

console.log('\n');

describe('  **************** routes/auth/auth.js **************\n',function(){
  this.timeout(20000);

  describe('    ------------- AUTH APIS -------------  \n',function(){
    // this.timeout(5000);

    describe('   ______rt_i_____       \n',function(){

      before(function(done){
        DataMethods.load('rt_i',function(err){
          if(err){
            console.trace(err);
            process.exit(1);
          }
          done();
        });
      });
      it('/auth/rt_i ==>> IOS User\n', function(done) {
        AccountApiParams.rt_i(function(err,params){
          if(err){
            console.trace(err);
            process.exit(1);
          }
          chai.request(server)
          .post('/auth/rt_i')
          .send(params)
          .end(function(err,res){
            if(err){
              console.log(err);
              process.exit(1);
            }
            res.should.have.status(200);
            res.should.be.json;
            res.body.should.be.a('object');
            res.body.should.have.property('success');
            res.body.success.should.equal(true);
            res.body.should.have.property('data');
            res.body.data.should.be.a('object');
            res.body.data.should.have.property('aid');
            res.body.data.aid.should.be.a('string');
            res.body.data.should.have.property('rt');
            res.body.data.rt.should.be.a('string');
            res.body.data.should.have.property('secret');
            res.body.data.secret.should.be.a('string');
            done();
          });
        });
      }); 
      after(function(done){
        DataMethods.unload(function(err){
          if(err){
            console.trace(err);
            process.exit(1);
          }
          done();
        });
      });
    });

    describe('   ______at_i_____       \n',function(){

      before(function(done){
        DataMethods.load('at_i',function(err){
          if(err){
            console.trace(err);
            process.exit(1);
          }
          done();
        });
      });
      it('/auth/at_i ==>> IOS User\n', function(done) {
        AccountApiParams.at_i(function(err,params){
          if(err){
            console.trace(err);
            process.exit(1);
          }
          chai.request(server)
          .post('/auth/at_i')
          .send(params)
          .end(function(err,res){
            if(err){
              console.log(err);
              process.exit(1);
            }
            res.should.have.status(200);
            res.should.be.json;
            res.body.should.be.a('object');
            res.body.should.have.property('success');
            res.body.success.should.equal(true);
            res.body.should.have.property('data');
            res.body.data.should.be.a('object');
            res.body.data.should.have.property('aid');
            res.body.data.aid.should.be.a('string');
            res.body.data.should.have.property('at');
            res.body.data.at.should.be.a('string');
            done();
          });
        });
      }); 
      after(function(done){
        DataMethods.unload(function(err){
          if(err){
            console.trace(err);
            process.exit(1);
          }
          done();
        });
      });
    });

    describe('   ______at_w_____       \n',function(){

      before(function(done){
        DataMethods.load('at_w',function(err){
          if(err){
            console.trace(err);
            process.exit(1);
          }
          done();
        });
      });
      it('/auth/at_w ==>> WEB User\n', function(done) {
        AccountApiParams.at_w(function(err,params){
          if(err){
            console.trace(err);
            process.exit(1);
          }
          chai.request(server)
          .post('/auth/at_w')
          .send(params)
          .end(function(err,res){
            if(err){
              console.log(err);
              process.exit(1);
            }
            res.should.have.status(200);
            res.should.be.json;
            res.body.should.be.a('object');
            res.body.should.have.property('success');
            res.body.success.should.equal(true);
            res.body.should.have.property('data');
            res.body.data.should.be.a('object');
            res.body.data.should.have.property('aid');
            res.body.data.aid.should.be.a('string');
            res.body.data.should.have.property('at');
            res.body.data.at.should.be.a('string');
            done();
          });
        });
      }); 
      after(function(done){
        DataMethods.unload(function(err){
          if(err){
            console.trace(err);
            process.exit(1);
          }
          done();
        });
      });
    });

    describe('   ______at_a_____       \n',function(){

      before(function(done){
        DataMethods.load('at_a',function(err){
          if(err){
            console.trace(err);
            process.exit(1);
          }
          done();
        });
      });
      it('/auth/at_a ==>> ANDROID User\n', function(done) {
        AccountApiParams.at_a(function(err,params){
          if(err){
            console.trace(err);
            process.exit(1);
          }
          chai.request(server)
          .post('/auth/at_a')
          .send(params)
          .end(function(err,res){
            if(err){
              console.log(err);
              process.exit(1);
            }
            res.should.have.status(200);
            res.should.be.json;
            res.body.should.be.a('object');
            res.body.should.have.property('success');
            res.body.success.should.equal(true);
            res.body.should.have.property('data');
            res.body.data.should.be.a('object');
            res.body.data.should.have.property('aid');
            res.body.data.aid.should.be.a('string');
            res.body.data.should.have.property('at');
            res.body.data.at.should.be.a('string');
            done();
          });
        });
      }); 
      after(function(done){
        DataMethods.unload(function(err){
          if(err){
            console.trace(err);
            process.exit(1);
          }
          done();
        });
      });
    });

    describe('   ______prov_tk_____       \n',function(){

      before(function(done){
        DataMethods.load('prov_tk',function(err){
          if(err){
            console.trace(err);
            process.exit(1);
          }
          done();
        });
      });
      it('/auth/prov_tk ==>> ANDROID User\n', function(done) {
        AccountApiParams.prov_tk(function(err,params){
          if(err){
            console.trace(err);
            process.exit(1);
          }
          chai.request(server)
          .post('/auth/prov_tk')
          .send(params)
          .end(function(err,res){
            if(err){
              console.log(err);
              process.exit(1);
            }
            res.should.have.status(200);
            res.should.be.json;
            res.body.should.be.a('object');
            res.body.should.have.property('success');
            res.body.success.should.equal(true);
            res.body.should.have.property('data');
            res.body.data.should.be.a('object');
            res.body.data.should.have.property('rt');
            res.body.data.rt.should.be.a('string');
            res.body.data.should.have.property('at');
            res.body.data.rt.should.be.a('string');
            res.body.data.should.have.property('secret');
            res.body.data.secret.should.be.a('string');
            done();
          });
        });
      }); 
      after(function(done){
        DataMethods.unload(function(err){
          if(err){
            console.trace(err);
            process.exit(1);
          }
          done();
        });
      });
    });

    // describe('   ______ack_pvtk_____       \n',function(){

    //   before(function(done){
    //     DataMethods.load('ack_pvtk',function(err){
    //       if(err){
    //         console.trace(err);
    //         process.exit(1);
    //       }
    //       done();
    //     });
    //   });
    //   it('/auth/ack_pvtk ==>> ANDROID User\n', function(done) {
    //     AccountApiParams.ack_pvtk(function(err,params){
    //       if(err){
    //         console.trace(err);
    //         process.exit(1);
    //       }
    //       chai.request(server)
    //       .post('/auth/ack_pvtk')
    //       .send(params)
    //       .end(function(err,res){
    //         if(err){
    //           console.log(err);
    //           process.exit(1);
    //         }
    //         res.should.have.status(200);
    //         res.should.be.json;
    //         res.body.should.be.a('object');
    //         res.body.should.have.property('success');
    //         res.body.success.should.equal(true);
    //         res.body.should.have.property('data');
    //         res.body.data.should.be.a('object');
    //         done();
    //       });
    //     });
    //   }); 
    //   after(function(done){
    //     DataMethods.unload(function(err){
    //       if(err){
    //         console.trace(err);
    //         process.exit(1);
    //       }
    //       done();
    //     });
    //   });
    // });

    describe('   ______acc_unlk_____       \n',function(){

      before(function(done){
        DataMethods.load('acc_unlk',function(err){
          if(err){
            console.trace(err);
            process.exit(1);
          }
          done();
        });
      });
      it('/auth/acc_unlk ==>> ANDROID User\n', function(done) {
        AccountApiParams.acc_unlk(function(err,params){
          if(err){
            console.trace(err);
            process.exit(1);
          }
          chai.request(server)
          .post('/auth/acc_unlk')
          .send(params)
          .end(function(err,res){
            if(err){
              console.log(err);
              process.exit(1);
            }
            res.should.have.status(200);
            res.should.be.json;
            res.body.should.be.a('object');
            res.body.should.have.property('success');
            res.body.success.should.equal(true);
            res.body.should.have.property('data');
            res.body.data.should.be.a('object');
            done();
          });
        });
      }); 
      after(function(done){
        DataMethods.unload(function(err){
          if(err){
            console.trace(err);
            process.exit(1);
          }
          done();
        });
      });
    });

  });

});



