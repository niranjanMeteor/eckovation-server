// var isUserAdminInOneWayOrGrpIsTwoWay = require("../../../chat/process_gm.js").isUserAdminInOneWayOrGrpIsTwoWay;

// var mongoose          = require('mongoose');

// require('../../../models/messages.js');
// require('../../../models/groupmembers.js');
// var Message             = mongoose.model('Message');
// var GroupMember         = mongoose.model('GroupMember');

// var assert = require('assert');

// describe('chat/test.js',function() {
// 	this.timeout(20000);
// 	describe('for two way group and any kind of member', function(){
// 		it('any shit okay', function(){
// 			assert(isUserAdminInOneWayOrGrpIsTwoWay(true,"","") == true);
// 		});
// 	});

// 	describe('for one way group and normal member', function(){
// 	  var member_type = GroupMember.TYPE.MEMBER;

// 	  it('notification - can go', function(){
// 	    assert(isUserAdminInOneWayOrGrpIsTwoWay(false,Message.MESSAGE_TYPE.notification,member_type) == true);
// 	  });

// 	  it('text message - can\'t go', function(){
// 	    assert(isUserAdminInOneWayOrGrpIsTwoWay(false,Message.MESSAGE_TYPE.text,member_type) == false);
// 	  });

// 	  it('image message - can\'t go', function(){
// 	    assert(isUserAdminInOneWayOrGrpIsTwoWay(false,Message.MESSAGE_TYPE.image,member_type) == false);
// 	  });

// 	  it('audio message - can\'t go', function(){
// 	    assert(isUserAdminInOneWayOrGrpIsTwoWay(false,Message.MESSAGE_TYPE.audio,member_type) == false);
// 	  });

// 	  it('video message - can\'t go', function(){
// 	    assert(isUserAdminInOneWayOrGrpIsTwoWay(false,Message.MESSAGE_TYPE.video,member_type) == false);
// 	  });

// 	  it('invisible message - can\'t go', function(){
// 	    assert(isUserAdminInOneWayOrGrpIsTwoWay(false,Message.MESSAGE_TYPE.invisible,member_type) == false);
// 	  });
// 	});

// 	describe('for one way group and banned member', function(){
// 		var member_type = GroupMember.TYPE.BANNED;

// 		it('notification - can go', function(){
// 		assert(isUserAdminInOneWayOrGrpIsTwoWay(false,Message.MESSAGE_TYPE.notification,member_type) == false);
// 		});

// 		it('text message - can\'t go', function(){
// 		assert(isUserAdminInOneWayOrGrpIsTwoWay(false,Message.MESSAGE_TYPE.text,member_type) == false);
// 		});

// 		it('image message - can\'t go', function(){
// 		assert(isUserAdminInOneWayOrGrpIsTwoWay(false,Message.MESSAGE_TYPE.image,member_type) == false);
// 		});

// 		it('audio message - can\'t go', function(){
// 		assert(isUserAdminInOneWayOrGrpIsTwoWay(false,Message.MESSAGE_TYPE.audio,member_type) == false);
// 		});

// 		it('video message - can\'t go', function(){
// 		assert(isUserAdminInOneWayOrGrpIsTwoWay(false,Message.MESSAGE_TYPE.video,member_type) == false);
// 		});

// 		it('invisible message - can\'t go', function(){
// 		assert(isUserAdminInOneWayOrGrpIsTwoWay(false,Message.MESSAGE_TYPE.invisible,member_type) == false);
// 		});
// 	});

// 	describe('for one way group and admin member', function(){
// 		var member_type = GroupMember.TYPE.ADMIN;

// 		it('notification - can go', function(){
// 		assert(isUserAdminInOneWayOrGrpIsTwoWay(false,Message.MESSAGE_TYPE.notification,member_type) == true);
// 		});

// 		it('text message - can go', function(){
// 		assert(isUserAdminInOneWayOrGrpIsTwoWay(false,Message.MESSAGE_TYPE.text,member_type) == true);
// 		});

// 		it('image message - can go', function(){
// 		assert(isUserAdminInOneWayOrGrpIsTwoWay(false,Message.MESSAGE_TYPE.image,member_type) == true);
// 		});

// 		it('audio message - can go', function(){
// 		assert(isUserAdminInOneWayOrGrpIsTwoWay(false,Message.MESSAGE_TYPE.audio,member_type) == true);
// 		});

// 		it('video message - can go', function(){
// 		assert(isUserAdminInOneWayOrGrpIsTwoWay(false,Message.MESSAGE_TYPE.video,member_type) == true);
// 		});

// 		it('invisible message - can go', function(){
// 		assert(isUserAdminInOneWayOrGrpIsTwoWay(false,Message.MESSAGE_TYPE.invisible,member_type) == true);
// 		});
// 	});

// });