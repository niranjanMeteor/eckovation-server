// if(process.env.NODE_ENV != "dapi"){
//   process.exit(1);
// }

var chai                          = require('chai');
var chaiHttp                      = require('chai-http');
var should                        = chai.should();
var VerifyOtpParams               = require('../../params/account/verify_otp.js');
var AccountTokensParams           = require('../../params/account/account_tokens.js');
var DataMethods                   = require('../../data_methods.js');
var server                        = 'http://localhost:'+3004;

chai.use(chaiHttp);

console.log('\n');
console.log('**************************      Starting Mocha Test Module       ***************************');


describe('  **************** routes/account/account.js **************\n',function(){
  this.timeout(20000);

  describe('    ----- WEB LOGIN ------\n',function(){


    before(function(done){
      DataMethods.load('login_w',function(err){
        if(err){
          console.trace(err);
          process.exit(1);
        }
        done();
      });
    });
    it('/account/login_w', function(done) {
      var params = {
        'c_code': '91',
        'p_no'  : '9591999098',
      };
      chai.request(server)
        .post('/account/login_w')
        .send(params)
        .end(function(err,res){
          if(err){
            console.log(err);
          }
          res.should.have.status(200);
          res.should.be.json;
          res.body.should.be.a('object');
          res.body.should.have.property('success');
          res.body.success.should.equal(true);
          res.body.should.have.property('data');
          res.body.data.should.be.a('object');
          res.body.data.should.have.property('Account');
          res.body.data.Account.should.be.a('object');
          res.body.data.Account.should.have.property('_id');
          res.body.data.Account.should.have.property('m');
          res.body.data.Account.should.have.property('ccod');
          res.body.data.Account.should.have.property('vrfy');
          done();
        });
    });
    after(function(done){
      DataMethods.unload(function(err){
        if(err){
          console.trace(err);
          process.exit(1);
        }
        done();
      });
    });
  });

  describe('      ----------- REQUEST OTP ----------\n',function(){

    it('/account/request_otp ==>> ANDROID User\n', function(done) {
      var params = {
        'c_code':'91',
        'p_no'  : '9591999085',
        'tokn'  : '5'
      };
      chai.request(server)
        .post('/account/request_otp')
        .send(params)
        .end(function(err,res){
          res.should.have.status(200);
          res.should.be.json;
          res.body.should.be.a('object');
          res.body.should.have.property('success');
          res.body.success.should.equal(true);
          res.body.should.have.property('data');
          res.body.data.should.be.a('object');
          res.body.data.should.have.property('Account');
          res.body.data.Account.should.be.a('object');
          res.body.data.Account.should.have.property('_id');
          res.body.data.Account.should.have.property('m');
          res.body.data.Account.should.have.property('ccod');
          res.body.data.Account.should.have.property('vrfy');
          done();
        });
    });

    it('/account/request_otp_v14 ==>> ANDROID User\n', function(done) {
      var params = {
        'c_code':'91',
        'p_no'  : '9591999083',
        'vrsn'  : '3.0.0',
        'client': 'A',
        'tokn'  : '5'
      };
      chai.request(server)
        .post('/account/request_otp_v14')
        .send(params)
        .end(function(err,res){
          res.should.have.status(200);
          res.should.be.json;
          res.body.should.be.a('object');
          res.body.should.have.property('success');
          res.body.success.should.equal(true);
          res.body.should.have.property('data');
          res.body.data.should.be.a('object');
          res.body.data.should.have.property('Account');
          res.body.data.Account.should.be.a('object');
          res.body.data.Account.should.have.property('_id');
          res.body.data.Account.should.have.property('m');
          res.body.data.Account.should.have.property('ccod');
          res.body.data.Account.should.have.property('vrfy');
          done();
        });
    });

    it('/account/request_otp_v16 ==>> ANDROID User\n', function(done) {
      var params = {
        'c_code':'91',
        'p_no'  : '9591999081',
        'vrsn'  : '3.0.0',
        'client': 'A',
        'role'  : '1',
        'email' : 'nir.striker@gmail.com',
        'name'  : 'nir_andr',
        'tokn'  : '5'
      };
      chai.request(server)
        .post('/account/request_otp_v16')
        .send(params)
        .end(function(err,res){
          res.should.have.status(200);
          res.should.be.json;
          res.body.should.be.a('object');
          res.body.should.have.property('success');
          res.body.success.should.equal(true);
          res.body.should.have.property('data');
          res.body.data.should.be.a('object');
          res.body.data.should.have.property('Account');
          res.body.data.Account.should.be.a('object');
          res.body.data.Account.should.have.property('_id');
          res.body.data.Account.should.have.property('m');
          res.body.data.Account.should.have.property('ccod');
          res.body.data.Account.should.have.property('vrfy');
          done();
        });
    });
  
  	it('/account/request_otp_v18 ==>> IOS User\n', function(done) {
      var params = {
        'c_code': '91',
        'p_no'  : '9591999080',
        'vrsn'  : '1.1.0',
        'client': 'I',
        'role'  : '1',
        'email' : 'nir.striker@gmail.com',
        'name'  : 'nir_ios'
      };
  	  chai.request(server)
  	    .post('/account/request_otp_v18')
  	    .send(params)
  	    .end(function(err,res){
  	      res.should.have.status(200);
  	      res.should.be.json;
  	      res.body.should.be.a('object');
          res.body.should.have.property('success');
          res.body.success.should.equal(true);
          res.body.should.have.property('data');
          res.body.data.should.be.a('object');
          res.body.data.should.have.property('Account');
          res.body.data.Account.should.be.a('object');
          res.body.data.Account.should.have.property('_id');
          res.body.data.Account.should.have.property('m');
          res.body.data.Account.should.have.property('ccod');
          res.body.data.Account.should.have.property('vrfy');
          done();
  	    });
  	});

    it('/account/request_otp_v18 ==>> WEB User\n', function(done) {
      var params = {
        'c_code': '91',
        'p_no'  : '9591999079',
        'vrsn'  : '1.0.0',
        'client': 'W',
        'role'  : '1',
        'email' : 'nir.striker@gmail.com',
        'name'  : 'nir_web'
      };
      chai.request(server)
        .post('/account/request_otp_v18')
        .send(params)
        .end(function(err,res){
          res.should.have.status(200);
          res.should.be.json;
          res.body.should.be.a('object');
          res.body.should.have.property('success');
          res.body.success.should.equal(true);
          res.body.should.have.property('data');
          res.body.data.should.be.a('object');
          res.body.data.should.have.property('Account');
          res.body.data.Account.should.be.a('object');
          res.body.data.Account.should.have.property('_id');
          res.body.data.Account.should.have.property('m');
          res.body.data.Account.should.have.property('ccod');
          res.body.data.Account.should.have.property('vrfy');
          done();
        });
    });

  });

  describe('    ------------- VERIFY OTP -------------\n',function(){
    // this.timeout(5000);

    describe('           _______verify_otp________\n',function(){

      before(function(done){
        DataMethods.load('verify_otp',function(err){
          if(err){
            console.trace(err);
            process.exit(1);
          }
          done();
        });
      });
      it('/account/verify_otp ==>> ANDROID User\n', function(done) {
        VerifyOtpParams.verify_otp(function(err,params){
          if(err){
            console.trace(err);
            process.exit(1);
          }
          chai.request(server)
          .post('/account/verify_otp')
          .send(params)
          .end(function(err,res){
            if(err){
              console.trace(err);
              process.exit(1);
            }
            res.should.have.status(200);
            res.should.be.json;
            res.body.should.be.a('object');
            res.body.should.have.property('success');
            res.body.success.should.equal(true);
            res.body.should.have.property('data');
            res.body.data.should.be.a('object');
            res.body.data.should.have.property('Profile');
            res.body.data.Profile.should.be.a('array');
            res.body.data.should.have.property('Group');
            res.body.data.Group.should.be.a('array');
            res.body.data.should.have.property('GroupMember');
            res.body.data.GroupMember.should.be.a('array');
            res.body.data.should.have.property('if_new_prof');
            res.body.data.if_new_prof.should.be.a('number');
            done();
          });
        });
      });
      after(function(done){
        DataMethods.unload(function(err){
          if(err){
            console.trace(err);
            process.exit(1);
          }
          done();
        });
      });
    });

    describe('         ______verify_otp_v16_____\n',function(){

      before(function(done){
        DataMethods.load('verify_otp_v16',function(err){
          if(err){
            console.trace(err);
            process.exit(1);
          }
          done();
        });
      });
      it('/account/verify_otp_v16 ==>> IOS User\n', function(done) {
        VerifyOtpParams.verify_otp_v16(function(err,params){
          if(err){
            console.trace(err);
            process.exit(1);
          }
          chai.request(server)
          .post('/account/verify_otp_v16')
          .send(params)
          .end(function(err,res){
            if(err){
              console.log(err);
              process.exit(1);
            }
            res.should.have.status(200);
            res.should.be.json;
            res.body.should.be.a('object');
            res.body.should.have.property('success');
            res.body.success.should.equal(true);
            res.body.should.have.property('data');
            res.body.data.should.be.a('object');
            res.body.data.should.have.property('Profile');
            res.body.data.Profile.should.be.a('array');
            res.body.data.should.have.property('Group');
            res.body.data.Group.should.be.a('array');
            res.body.data.should.have.property('GroupMember');
            res.body.data.GroupMember.should.be.a('array');
            res.body.data.should.have.property('rt');
            res.body.data.rt.should.be.a('string');
            res.body.data.should.have.property('at');
            res.body.data.at.should.be.a('string');
            res.body.data.should.have.property('secret');
            res.body.data.secret.should.be.a('string');
            res.body.data.should.have.property('if_new_prof');
            res.body.data.if_new_prof.should.be.a('number');
            done();
          });
        });
      }); 
      after(function(done){
        DataMethods.unload(function(err){
          if(err){
            console.trace(err);
            process.exit(1);
          }
          done();
        });
      });
    });

    describe('         ______v_otp_w______\n',function(){

      before(function(done){
        DataMethods.load('v_otp_w',function(err){
          if(err){
            console.trace(err);
            process.exit(1);
          }
          done();
        })
      });
      it('/account/v_otp_w ==>> WEB User\n', function(done) {
        VerifyOtpParams.v_otp_w(function(err,params){
          chai.request(server)
          .post('/account/v_otp_w')
          .send(params)
          .end(function(err,res){
            if(err){
              console.log(err);
              process.exit(1);
            }
            res.should.have.status(200);
            res.should.be.json;
            res.body.should.be.a('object');
            res.body.should.have.property('success');
            res.body.success.should.equal(true);
            res.body.should.have.property('data');
            res.body.data.should.be.a('object');
            res.body.data.should.have.property('Profile');
            res.body.data.Profile.should.be.a('array');
            res.body.data.should.have.property('Group');
            res.body.data.Group.should.be.a('array');
            res.body.data.should.have.property('GroupMember');
            res.body.data.GroupMember.should.be.a('array');
            res.body.data.should.have.property('rt');
            res.body.data.rt.should.be.a('string');
            res.body.data.should.have.property('at');
            res.body.data.at.should.be.a('string');
            res.body.data.should.have.property('secret');
            res.body.data.secret.should.be.a('string');
            res.body.data.should.have.property('if_new_prof');
            res.body.data.if_new_prof.should.be.a('number');
            done();
          });
        });
      });
      after(function(done){
        DataMethods.unload(function(err){
          if(err){
            console.trace(err);
            process.exit(1);
          }
          done();
        });
      });
    });

    describe('   ______v_otp_a_____       \n',function(){

      before(function(done){
        DataMethods.load('v_otp_a',function(err){
          if(err){
            console.trace(err);
            process.exit(1);
          }
          done();
        });
      });
      it('/account/v_otp_a ==>> ANDROID User\n', function(done) {
        VerifyOtpParams.v_otp_a(function(err,params){
          if(err){
            console.trace(err);
            process.exit(1);
          }
          chai.request(server)
          .post('/account/v_otp_a')
          .send(params)
          .end(function(err,res){
            if(err){
              console.log(err);
              process.exit(1);
            }
            res.should.have.status(200);
            res.should.be.json;
            res.body.should.be.a('object');
            res.body.should.have.property('success');
            res.body.success.should.equal(true);
            res.body.should.have.property('data');
            res.body.data.should.be.a('object');
            res.body.data.should.have.property('Profile');
            res.body.data.Profile.should.be.a('array');
            res.body.data.should.have.property('Group');
            res.body.data.Group.should.be.a('array');
            res.body.data.should.have.property('GroupMember');
            res.body.data.GroupMember.should.be.a('array');
            res.body.data.should.have.property('rt');
            res.body.data.rt.should.be.a('string');
            res.body.data.should.have.property('at');
            res.body.data.at.should.be.a('string');
            res.body.data.should.have.property('secret');
            res.body.data.secret.should.be.a('string');
            res.body.data.should.have.property('if_new_prof');
            res.body.data.if_new_prof.should.be.a('number');
            done();
          });
        });
      }); 
      after(function(done){
        DataMethods.unload(function(err){
          if(err){
            console.trace(err);
            process.exit(1);
          }
          done();
        });
      });
    });
  });

  describe('    ------------- ACCOUNT TOKENS -------------\n',function(){

    describe('   ______g_rt_____       \n',function(){

      before(function(done){
        DataMethods.load('g_rt',function(err){
          if(err){
            console.trace(err);
            process.exit(1);
          }
          done();
        });
      });
      it('/account/g_rt ==>> IOS User\n', function(done) {
        AccountTokensParams.g_rt(function(err,params){
          if(err){
            console.trace(err);
            process.exit(1);
          }
          chai.request(server)
          .post('/account/g_rt')
          .send(params)
          .end(function(err,res){
            if(err){
              console.log(err);
              process.exit(1);
            }
            res.should.have.status(200);
            res.should.be.json;
            res.body.should.be.a('object');
            res.body.should.have.property('success');
            res.body.success.should.equal(true);
            res.body.should.have.property('data');
            res.body.data.should.be.a('object');
            res.body.data.should.have.property('aid');
            res.body.data.aid.should.be.a('string');
            res.body.data.should.have.property('rt');
            res.body.data.rt.should.be.a('string');
            res.body.data.should.have.property('secret');
            res.body.data.secret.should.be.a('string');
            done();
          });
        });
      }); 
      after(function(done){
        DataMethods.unload(function(err){
          if(err){
            console.trace(err);
            process.exit(1);
          }
          done();
        });
      });
    });

    describe('   ______g_at_____       \n',function(){

      before(function(done){
        DataMethods.load('g_at',function(err){
          if(err){
            console.trace(err);
            process.exit(1);
          }
          done();
        });
      });
      it('/account/g_at ==>> IOS User\n', function(done) {
        AccountTokensParams.g_at(function(err,params){
          if(err){
            console.trace(err);
            process.exit(1);
          }
          chai.request(server)
          .post('/account/g_at')
          .send(params)
          .end(function(err,res){
            if(err){
              console.log(err);
              process.exit(1);
            }
            res.should.have.status(200);
            res.should.be.json;
            res.body.should.be.a('object');
            res.body.should.have.property('success');
            res.body.success.should.equal(true);
            res.body.should.have.property('data');
            res.body.data.should.be.a('object');
            res.body.data.should.have.property('aid');
            res.body.data.aid.should.be.a('string');
            res.body.data.should.have.property('at');
            res.body.data.at.should.be.a('string');
            done();
          });
        });
      }); 
      after(function(done){
        DataMethods.unload(function(err){
          if(err){
            console.trace(err);
            process.exit(1);
          }
          done();
        });
      });
    });

    describe('   ______g_at_w_____       \n',function(){

      before(function(done){
        DataMethods.load('g_at_w',function(err){
          if(err){
            console.trace(err);
            process.exit(1);
          }
          done();
        });
      });
      it('/account/g_at_w ==>> WEB User\n', function(done) {
        AccountTokensParams.g_at_w(function(err,params){
          if(err){
            console.trace(err);
            process.exit(1);
          }
          chai.request(server)
          .post('/account/g_at_w')
          .send(params)
          .end(function(err,res){
            if(err){
              console.log(err);
              process.exit(1);
            }
            res.should.have.status(200);
            res.should.be.json;
            res.body.should.be.a('object');
            res.body.should.have.property('success');
            res.body.success.should.equal(true);
            res.body.should.have.property('data');
            res.body.data.should.be.a('object');
            res.body.data.should.have.property('aid');
            res.body.data.aid.should.be.a('string');
            res.body.data.should.have.property('at');
            res.body.data.at.should.be.a('string');
            done();
          });
        });
      }); 
      after(function(done){
        DataMethods.unload(function(err){
          if(err){
            console.trace(err);
            process.exit(1);
          }
          done();
        });
      });
    });

  });

});



