var redis_client 	= require("fakeredis").createClient("", "");
var rcq 			= require("../../../../utility/redis_chat_queue.js")(redis_client);
var _ 				= require("underscore");

var assert = require('assert');

describe('redis_chat_queue/removeall.js',function() {
	this.timeout(20000);
	describe('remove from multiple invalid pids',function() {
		var pid1 = "p1";
		var pid2 = "p2";
		var pid3 = "p3";
		var pid4 = "p44";

		describe('message exists', function() {
			before(function(done){
				var data = [];
				data.push({ pid : pid1, score: 100, data : "m1" });
				data.push({ pid : pid2, score: 102, data : "m1" });
				data.push({ pid : pid2, score: 102, data : "m2" });
				data.push({ pid : pid3, score: 100, data : "m1" });

				insert_redis_data(data,done);
			});

			it('returned count is correct', function(done) {
				rcq.removeall([pid4],"m1",function(err,cnt){
					assert(_.isEqual(cnt,0));
					done();
				});
			});

			after(function(done){
				rcq.removeAllObjs(pid1,function(){
					rcq.removeAllObjs(pid2,function(){
						rcq.removeAllObjs(pid3,function(){
							done();
						});
					});
				});
			});
		});
	});
});

describe('remove from multiple valid pids',function() {
	var pid1 = "p1";
	var pid2 = "p2";
	var pid3 = "p3";

	describe('message exists', function() {
		before(function(done){
			var data = [];
			data.push({ pid : pid1, score: 100, data : "m1" });
			data.push({ pid : pid2, score: 102, data : "m1" });
			data.push({ pid : pid2, score: 102, data : "m2" });
			data.push({ pid : pid3, score: 100, data : "m1" });

			insert_redis_data(data,done);
		});

		it('returned count is correct', function(done) {
			rcq.removeall([pid1,pid2,pid3],"m1",function(err,cnt){
				assert(_.isEqual(cnt,3));
				done();
			});
		});

		after(function(done){
			rcq.removeAllObjs(pid1,function(){
				rcq.removeAllObjs(pid2,function(){
					rcq.removeAllObjs(pid3,function(){
						done();
					});
				});
			});
		});
	});

	describe('message exists and not exists', function() {
		before(function(done){
			var data = [];
			data.push({ pid : pid1, score: 100, data : "m1" });
			data.push({ pid : pid2, score: 102, data : "m1" });
			data.push({ pid : pid2, score: 102, data : "m2" });
			data.push({ pid : pid3, score: 100, data : "m4" });

			insert_redis_data(data,done);
		});

		it('returned count is correct', function(done) {
			rcq.removeall([pid1,pid2,pid3],"m1",function(err,cnt){
				assert(_.isEqual(cnt,2));
				done();
			});
		});

		after(function(done){
			rcq.removeAllObjs(pid1,function(){
				rcq.removeAllObjs(pid2,function(){
					rcq.removeAllObjs(pid3,function(){
						done();
					});
				});
			});
		});
	});
});

var insert_redis_data = function(datas,cb) {
	var inserted = 0;

	datas.forEach(function(data){
		rcq.add(data.pid,data.score,data.data,function(){
			inserted++;
			if(inserted>=datas.length) {
				cb();
			}
		});
	});
}