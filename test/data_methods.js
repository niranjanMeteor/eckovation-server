// if(process.env.NODE_ENV != "dapi"){
// 	process.exit(1);
// }

require('shelljs/global');
var mongoose 						          = require('mongoose');
var testConfigs                   = require('./test_configs.js');

connectMongoDb();
mongoose.connection.on('error', function(err){
  console.trace("MongoDb Connection Error " + err);
  process.exit(0);
});
mongoose.connection.on('connected', function(){
  // console.log('Successfully Opened MOngoDb Connection');
});

var currDir               = __dirname;
// var dump_cmd              = 'mongorestore -u '+MONGO_UNAME+' -p '+MONGO_PASS+' --db '+DB_NAME+'  --quiet --gzip --archive=';
var dump_cmd              = 'mongorestore --host '+testConfigs.MONGO_HOST+':'+testConfigs.MONGO_PORT+' --db '+testConfigs.DB_NAME+' --noIndexRestore --quiet --gzip --archive=';

exports.unload = function(cb){
	unloadTestDatabase(function(err,resp){
		if(err){
			return cb(err,null);
		}
		return cb(null,true);
	});
};


exports.load = function(route, cb){
	var dump_file;
	switch(route){

		case 'login_w':
			dump_file             = currDir+'/dump/routes/account/login/login_w.gz';
			prepareDB(dump_file, function(err,resp){
				if(err){
					console.log(err);
					return cb(err,null);
				}
				return cb(null,true);
			});
		break;

		case 'verify_otp':
			dump_file             = currDir+'/dump/routes/account/verify_otp/verify_otp.gz';
			prepareDB(dump_file, function(err,resp){
				if(err){
					console.log(err);
					return cb(err,null);
				}
				return cb(null,true);
			});
		break;

		case 'verify_otp_v16':
			dump_file             = currDir+'/dump/routes/account/verify_otp/verify_otp_v16.gz';
			prepareDB(dump_file, function(err,resp){
				if(err){
					console.log(err);
					return cb(err,null);
				}
				return cb(null,true);
			});
		break;

		case 'v_otp_w':
			dump_file             = currDir+'/dump/routes/account/verify_otp/v_otp_w.gz';
			prepareDB(dump_file, function(err,resp){
				if(err){
					console.log(err);
					return cb(err,null);
				}
				return cb(null,true);
			});
		break;

		case 'v_otp_a':
			dump_file             = currDir+'/dump/routes/account/verify_otp/v_otp_a.gz';
			prepareDB(dump_file, function(err,resp){
				if(err){
					console.log(err);
					return cb(err,null);
				}
				return cb(null,true);
			});
		break;

		case 'g_rt':
			dump_file             = currDir+'/dump/routes/account/g_rt/g_rt.gz';
			prepareDB(dump_file, function(err,resp){
				if(err){
					console.log(err);
					return cb(err,null);
				}
				return cb(null,true);
			});
		break;

		case 'g_at':
		dump_file             = currDir+'/dump/routes/account/g_at/g_at.gz';
			prepareDB(dump_file, function(err,resp){
				if(err){
					console.log(err);
					return cb(err,null);
				}
				return cb(null,true);
			});
		break;

		case 'g_at_w':
			dump_file             = currDir+'/dump/routes/account/g_at_w/g_at_w.gz';
			prepareDB(dump_file, function(err,resp){
				if(err){
					console.log(err);
					return cb(err,null);
				}
				return cb(null,true);
			});
		break;

		case 'rt_i':
			dump_file             = currDir+'/dump/routes/auth/rt_i/rt_i.gz';
			prepareDB(dump_file, function(err,resp){
				if(err){
					console.log(err);
					return cb(err,null);
				}
				return cb(null,true);
			});
		break;

		case 'at_i':
			dump_file             = currDir+'/dump/routes/auth/at_i/at_i.gz';
			prepareDB(dump_file, function(err,resp){
				if(err){
					console.log(err);
					return cb(err,null);
				}
				return cb(null,true);
			});
		break;

		case 'at_a':
			dump_file             = currDir+'/dump/routes/auth/at_a/at_a.gz';
			prepareDB(dump_file, function(err,resp){
				if(err){
					console.log(err);
					return cb(err,null);
				}
				return cb(null,true);
			});
		break;

		case 'at_w':
			dump_file             = currDir+'/dump/routes/auth/at_w/at_w.gz';
			prepareDB(dump_file, function(err,resp){
				if(err){
					console.log(err);
					return cb(err,null);
				}
				return cb(null,true);
			});
		break;

		case 'prov_tk':
			dump_file             = currDir+'/dump/routes/auth/prov_tk/prov_tk.gz';
			prepareDB(dump_file, function(err,resp){
				if(err){
					console.log(err);
					return cb(err,null);
				}
				return cb(null,true);
			});
		break;

		case 'ack_pvtk':
			dump_file             = currDir+'/dump/routes/auth/ack_pvtk/ack_pvtk.gz';
			prepareDB(dump_file, function(err,resp){
				if(err){
					console.log(err);
					return cb(err,null);
				}
				return cb(null,true);
			});
		break;		

		case 'acc_unlk':
			dump_file             = currDir+'/dump/routes/auth/acc_unlk/acc_unlk.gz';
			prepareDB(dump_file, function(err,resp){
				if(err){
					console.log(err);
					return cb(err,null);
				}
				return cb(null,true);
			});
		break;
	}

};

function prepareDB(dump_file, cb){
	unloadTestDatabase(function(err,resp){
		if(err){
			return cb(err,null);
		}
		loadTestDatabase(dump_file, function(err,resp){
			if(err){
				return cb(err,null);
			}
			return cb(null,true);
		});
	});
}

function loadTestDatabase(testdb_dump_file, cb){
	var testdb_dump_cmd = dump_cmd+''+testdb_dump_file;
	exec(testdb_dump_cmd,function(code,status){
		if(code != 0){
			console.log(testdb_dump_cmd);
			console.log(code);
			console.log(status);
			return cb('Dump Failed to load for : '+testdb_dump_file,null);
		}
		return cb(null,true);
	});
}

function unloadTestDatabase(cb){
	mongoose.connection.db.dropDatabase(function(err, result){
		if(err){
			return cb(err,null);
		}
		return cb(null,result);
	});
}

function connectMongoDb(){
  var options = {
    server : {
      socketOptions : {
        keepAlive : 120
      }
    }
  };
  return mongoose.connect("mongodb://"+testConfigs.MONGO_HOST+":"+testConfigs.MONGO_PORT+"/"+testConfigs.DB_NAME,options);
}
