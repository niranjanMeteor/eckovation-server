var configs = {
	MONGO_HOST                          : "127.0.0.1",
	REDIS_HOST													: "127.0.0.1",
	MONGO_PORT                          : "27019",
	REDIS_PORT													: "6378",
	DB_NAME 														: "eckovation_test",
	// MONGO_UNAME													: "eck_user_test",
	MONGO_UNAME													: "",
	// MONGO_PASS													: "test_1234_INDIA_NATION"
	RABBITMQ_USER                       : "eck_rabbit",
	RABBITMQ_PASSWORD                   : "eck!!--^^(12)3_=_user",
	SERVER_PORT                         : "3004",
	ALLOWED_FAILED_VERIFY_OTP_ATTEMPTS  : 3000,
	TIME_TO_PAUSE_VERIFY_OTP_ATTEMPTS   : 1*60*1000, // 1 minute
	VALID_CALL_TESTING_NUMBERS          : ['9591999098','9999894738','8341255449','9555670865','9953933422'],
	ALLOWED_REQUEST_OTP_ATTEMPTS        : 3000,
	TIME_TO_PAUSE_REQUEST_OTP_ATTEMPTS  : 1*60*1000, // 1 minute
	ALLOWED_GROUP_JOIN_ATTEMPTS         : 3000,
	TIME_TO_PAUSE_GROUP_JOIN_ATTEMPTS   : 1*60*1000  // 1 minite
};

module.exports = configs;