var fs                        = require('fs');

var FeeConfigs = {

	CONFIGS_OVERWRITE_FILE                       : "configs_overwrite.js",
	FEE_ECKOVATION_CHARGE_TYPE                   : 2,
	FEE_TRANSACTION_CHARGE_NET                   : 30,
	FEE_TRANSACTION_CHARGE_PERCENT               : 3,
	PATHSHALA_CLIENT_ID_LENGTH                   : 12,
	PATHSHALA_CLIENT_SECRET_LENGTH               : 16,
	FEE_ORDER_ID_RANDOM_NUMBER_LENGTH            : 2,
	FEE_PAYMENT_HELPLINE_NUMBER                  : "9953933422",
	FEE_PAYMENT_HELPLINE_EMAIL                   : "billing@eckovation.com",
	FEE_RECEIPT_TEMPLATE                         : "/home/ubuntu/eckovation-server/fee/fee_receipt_template.html",
	FEE_STATS_MAIL_TEMPLATE                      : "/home/ubuntu/eckovation-server/fee/fee_stats_mail_template.html",
	FEE_RECEIPT_DIR                              : "/var/log/eckovation/fee/file"

};

var overwriteConfigFulFileName  = __dirname+'/'+FeeConfigs.CONFIGS_OVERWRITE_FILE;
if(fs.existsSync(overwriteConfigFulFileName)){
	var overwriteConfig  =  require(overwriteConfigFulFileName);
	for (var key in overwriteConfig){
		FeeConfigs[key] = overwriteConfig[key];
	}
} else {
	console.log('No Overwrite Configs File Found to overwrite any Plugin configs key');
}

module.exports = FeeConfigs;