const https             			      		= require('https');
const http             			        		= require('http');

const mail_client                       = require('../utility/mail/mail_client.js');
const configs                           = require('../utility/configs.js');

exports.pathshalaApi = function(params, server_configs, api, cb){
	var path_url;
	if(api == 1){
		path_url = server_configs.fee_dtl_url;
	} else if(api == 2){
		path_url = server_configs.fee_cb_url
	} else return cb("getFeeDetails_api_value_invalid",null);

	var options = {
	  host       : server_configs.host,
	  port       : server_configs.port,
	  path       : path_url,
	  method     : 'POST'
	};

	var postData = JSON.stringify(params);
	options.headers = {
    'Content-Type'  : 'application/json',
    'Content-Length': Buffer.byteLength(postData),
    'client_id'     : server_configs.client_id,
    'client_sec'    : server_configs.client_sec
  };

	var req = https.request(options);
	req.write(postData);
	
	var responseData = '';
	req.on('response', function(res){
	  res.on('data', function(chunk){
	   responseData += chunk;
	  });
	  res.on('end', function(){
	  	try {
	  		if(res.statusCode == 449){
	  			return cb(null,449);
	  		}
	  		if(res.statusCode != 200){
					informDevTeam(responseData, res.statusCode, params);
					return cb("pathshalaApi_returned_non_200_response",null);
				}
		    var decodedResponse = JSON.parse(responseData);
		    console.log(decodedResponse);
		    if(("success" in decodedResponse)  && decodedResponse.success == true){
		    	return cb(null,decodedResponse.data);
		    }
	    	console.log('Error In Calling Fee Details Url with params : '+params);
	    	informDevTeam(decodedResponse, res.statusCode, params);
	    	return cb(decodedResponse,null);
	    } catch(excp){
		  	console.log(excp);
		  	informDevTeam(responseData, res.statusCode, params);
		  	return cb("Exception Occured While Calling Pathshala server",null);
		  }
	  });
	});
	req.end();
	req.on('error', function(err){
		console.log('Error Occurred in Fee Detail Http Request : ');
    console.log(err);
    informDevTeam(err, "NON_200", params);
    return cb("pathshalaApi_request_error",null);
  });

	// var req = https.request(options, function(res){
	// 	if(!("statusCode" in res)){
	// 		informDevTeam(res.body, "NON_200", params);
	// 		return cb("pathshalaApi_response_did_not_have_statusCode",null);
	// 	}
	// 	if(res.statusCode != 200){
	// 		informDevTeam(res.body, res.statusCode, params);
	// 		return cb("pathshalaApi_returned_non_200_response",null);
	// 	}
	// 	// res.setEncoding('utf8');
	// 	var responseData = '';
	// 	res.on('data', function(chunk) {
	//     console.log('response data from pathshala');
	//     console.log(typeof(chunk));
	//     responseData = chunk;
	//   });
	//   res.on('end', function() {
	//     console.log('No more data in response.');
	//     try {
	// 	    // var decodedResponse = JSON.parse(responseData);
	// 	    var decodedResponse = responseData;
	// 	    console.log(Object.keys(decodedResponse));
	// 	    if(("success" in decodedResponse)  && decodedResponse.success == true){
	// 	    	return cb(null,decodedResponse.data);
	// 	    }
	//     	console.log('Error In Calling Fee Details Url with params : '+params);
	//     	return cb(decodedResponse,null);
	//     } catch(excp){
	// 	  	console.log(excp);
	// 	  	return cb("Exception Occured While Calling Pathshala server",null);
	// 	  }
	//   });
	// }); 
	// req.on('error', function(e) {
	//   console.log('error occurred from pathshala server');
	//   return cb("pathshalaApi_request_error",null);
	// });
	// req.write(postData);
	// req.end();
};


function informDevTeam(body_data, statusCode, params){
  var data = {
  	body : body_data
  };
  var subject = "Pathshala Failed for ";
  var html_string = '<h4>'+ Date()  +': Pathshala Server Failure for, ';
  if("mobile" in params){
  	html_string += 'Fee Details, '+params.mobile;
  	subject     += 'Fee Details, '+params.mobile;
  } else if("order_id" in params){
  	html_string += 'Callback Response, '+params.order_id;
  	subject     += 'Callback Response, '+params.order_id;
  }
  html_string += '</h4>';
  html_string += '<br>';
  html_string += '<h4> Status Code Returned : '+statusCode+'</h4><br>';
  html_string += '<h5>'+JSON.stringify(data)+'</h5>';
  var mailOptions = {
    from    : configs.PAYMENTS_FROM,
    to      : configs.TECH_TO,
    subject : subject, 
    html    : html_string
  };
  console.log('**********************Fee Api Call******************');
  console.log(statusCode);
  if(process.env.NODE_ENV == "ci" || process.env.NODE_ENV == "debug" || process.env.NODE_ENV == "dapi"){
    return;
  }
  mail_client.send_gen(mailOptions, function(err, response){
    if (err) {
      console.trace(err);
      console.log('Informing Dev Team about Pathshala Server Api Failure : Mail could be not sent');
    } else {
      console.log('Informing Dev Team about Pathshala Server Api Success : Mail sent succesfully');
    }
  });
}