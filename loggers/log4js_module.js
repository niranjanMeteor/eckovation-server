var log4js              	  = require('log4js');
var Log4jsConfigs         	  = require('./log4js_configs.js');

var current_date        	  = new Date();
var file_date_ext       	  = current_date.getFullYear()+'_'+(current_date.getMonth()+1)+'_'+current_date.getDate();

var socketEventsLogFile       = Log4jsConfigs.LOG_DIR_SOCKET+'/events-'+file_date_ext+'.log';
var clientsHttpLogFile        = Log4jsConfigs.LOG_DIR_CLIENTS_HTTP+'/agents_'+file_date_ext+'.log';
var clientsSocketLogFile      = Log4jsConfigs.LOG_DIR_CLIENTS_SOCKET+'/agents_'+file_date_ext+'.log';
var routesGroupErrorLog       = Log4jsConfigs.LOG_DIR_ROUTES_GROUPS+'/err_'+file_date_ext+'.log';
var routesAccountDataErrorLog = Log4jsConfigs.LOG_DIR_ROUTES_ACCOUNTDATA+'/err_'+file_date_ext+'.log';
var routesMediaErrorLog       = Log4jsConfigs.LOG_DIR_ROUTES_MEDIA+'/err_'+file_date_ext+'.log';
var routesGroupMemErrorLog    = Log4jsConfigs.LOG_DIR_ROUTES_GROUPMEMBER+'/err_'+file_date_ext+'.log';
var routesAccountErrorLog     = Log4jsConfigs.LOG_DIR_ROUTES_ACCOUNT+'/err_'+file_date_ext+'.log';
var routesProfileErrorLog     = Log4jsConfigs.LOG_DIR_ROUTES_PROFILE+'/err_'+file_date_ext+'.log';
var routesMessageErrorLog     = Log4jsConfigs.LOG_DIR_ROUTES_MESSAGE+'/err_'+file_date_ext+'.log';
var utilityGrpNotifyErrorLog  = Log4jsConfigs.LOG_DIR_UTILITY_GROUP_NOTIFY+'/err_'+file_date_ext+'.log';
var socketEventPacketLog      = Log4jsConfigs.LOG_DIR_EVENT_SOCKET_PACKET+'/packet_'+file_date_ext+'.log';
var routesAuthErrorLog        = Log4jsConfigs.LOG_DIR_ROUTES_AUTH+'/err_'+file_date_ext+'.log';
var utilityEnDeCryptErrorLog  = Log4jsConfigs.LOG_DIR_UTILITY_EN_DE_CRYPT+'/err_'+file_date_ext+'.log';
var bruteForceAccountLog      = Log4jsConfigs.LOG_DIR_BRUTE_FORCE_ACCOUNT+'/err_'+file_date_ext+'.log';
var bruteForceOtpLog          = Log4jsConfigs.LOG_DIR_BRUTE_FORCE_OTP+'/err_'+file_date_ext+'.log';
var bruteForceGrpJoinLog      = Log4jsConfigs.LOG_DIR_BRUTE_FORCE_GROUP_JOIN+'/err_'+file_date_ext+'.log';
var secureGroupErrorLog       = Log4jsConfigs.LOG_DIR_SECURE_GROUP+'/err_'+file_date_ext+'.log';
var processGmErrorLog         = Log4jsConfigs.LOG_DIR_PROCESS_GM+'/err_'+file_date_ext+'.log';
var groupFeedLog              = Log4jsConfigs.LOG_DIR_GROUP_FEED+'/groupfeed_'+file_date_ext+'.log';
var pluginLog                 = Log4jsConfigs.LOG_DIR_PLUGIN+'/plugin_'+file_date_ext+'.log';
var userqueueLog              = Log4jsConfigs.LOG_DIR_USERQUEUE+'/userqueue_'+file_date_ext+'.log';
var routesUserqueueLog        = Log4jsConfigs.LOG_DIR_ROUTES_USERQUEUE+'/userqueue_'+file_date_ext+'.log';
var routesTrxnLog             = Log4jsConfigs.LOG_DIR_ROUTES_TRXN+'/info'+file_date_ext+'.log';
var routesPaymentLog          = Log4jsConfigs.LOG_DIR_ROUTES_PAYMENT+'/error'+file_date_ext+'.log';
var memwatchLog               = Log4jsConfigs.LOG_DIR_MEMWATCH+'/memwatch'+file_date_ext+'.log';
var routesEckovationLog       = Log4jsConfigs.LOG_DIR_ECKOVATION+'/error'+file_date_ext+'.log';
var routesGroupjoinPaidLog    = Log4jsConfigs.LOG_DIR_GROUPJOIN_PAID+'/error'+file_date_ext+'.log';
var routesClientLogErrorLog   = Log4jsConfigs.LOG_DIR_CLIENTLOG+'/error'+file_date_ext+'.log';
var socketConnLogFile         = Log4jsConfigs.LOG_DIR_SOCKET_CONN+'/events-'+file_date_ext+'.log';
var thirdpartyapiLogFile      = Log4jsConfigs.LOG_DIR_THIRDPARTYAPI+'/events-'+file_date_ext+'.log';
var razorpayLogFile           = Log4jsConfigs.LOG_DIR_ROUTES_RAZORPAY+'/info-'+file_date_ext+'.log';
var grouppluginLogFile        = Log4jsConfigs.LOG_DIR_ROUTES_GROUP_PLUGIN+'/info-'+file_date_ext+'.log';
var feeLogFile                = Log4jsConfigs.LOG_DIR_ROUTES_FEE+'/info-'+file_date_ext+'.log';

log4js.configure({
  appenders: [
    { type: 'file', filename: clientsHttpLogFile,    	   category: Log4jsConfigs.CATEGORY_CLIENTS_HTTP },
    { type: 'file', filename: clientsSocketLogFile,  	   category: Log4jsConfigs.CATEGORY_CLIENTS_SOCKET },
    { type: 'file', filename: socketEventsLogFile,   	   category: Log4jsConfigs.CATEGORY_SOCKET},
    { type: 'file', filename: routesGroupErrorLog,   	   category: Log4jsConfigs.CATEGORY_ROUTES_GROUPS},
    { type: 'file', filename: routesAccountDataErrorLog,   category: Log4jsConfigs.CATEGORY_ROUTES_ACCOUNTDATA},
    { type: 'file', filename: routesMediaErrorLog,   	   category: Log4jsConfigs.CATEGORY_ROUTES_MEDIA},
    { type: 'file', filename: routesGroupMemErrorLog,      category: Log4jsConfigs.CATEGORY_ROUTES_GROUPMEMBER},
    { type: 'file', filename: routesAccountErrorLog,   	   category: Log4jsConfigs.CATEGORY_ROUTES_ACCOUNT},
    { type: 'file', filename: routesProfileErrorLog,   	   category: Log4jsConfigs.CATEGORY_ROUTES_PROFILE},
    { type: 'file', filename: routesMessageErrorLog,   	   category: Log4jsConfigs.CATEGORY_ROUTES_MESSAGE},
    { type: 'file', filename: utilityGrpNotifyErrorLog,    category: Log4jsConfigs.CATEGORY_UTILITY_GROUP_NOTIFY},
    { type: 'file', filename: socketEventPacketLog,        category: Log4jsConfigs.CATEGORY_EVENT_SOCKET_PACKET},
    { type: 'file', filename: routesAuthErrorLog,          category: Log4jsConfigs.CATEGORY_ROUTES_AUTH},
    { type: 'file', filename: utilityEnDeCryptErrorLog,    category: Log4jsConfigs.CATEGORY_UTILITY_EN_DE_CRYPT},
    { type: 'file', filename: bruteForceAccountLog,        category: Log4jsConfigs.CATEGORY_BRUTE_FORCE_ACCOUNT},
    { type: 'file', filename: bruteForceOtpLog,            category: Log4jsConfigs.CATEGORY_BRUTE_FORCE_OTP},
    { type: 'file', filename: bruteForceGrpJoinLog,        category: Log4jsConfigs.CATEGORY_BRUTE_FORCE_GROUP_JOIN},
    { type: 'file', filename: secureGroupErrorLog,         category: Log4jsConfigs.CATEGORY_SECURE_GROUP},
    { type: 'file', filename: pluginLog,                   category: Log4jsConfigs.CATEGORY_ROUTES_PLUGIN},
    { type: 'file', filename: processGmErrorLog,           category: Log4jsConfigs.CATEGORY_PROCESS_GM},
    { type: 'file', filename: groupFeedLog,                category: Log4jsConfigs.CATEGORY_ROUTES_GROUP_FEED},
    { type: 'file', filename: userqueueLog,                category: Log4jsConfigs.CATEGORY_USERQUEUE},
    { type: 'file', filename: routesUserqueueLog,          category: Log4jsConfigs.CATEGORY_ROUTES_USERQUEUE},
    { type: 'file', filename: routesTrxnLog,               category: Log4jsConfigs.CATEGORY_ROUTES_TRXN},
    { type: 'file', filename: routesPaymentLog,            category: Log4jsConfigs.CATEGORY_ROUTES_PAYMENT},
    { type: 'file', filename: memwatchLog,                 category: Log4jsConfigs.CATEGORY_MEMWATCH},
    { type: 'file', filename: routesEckovationLog,         category: Log4jsConfigs.CATEGORY_ROUTES_ECKOVATION},
    { type: 'file', filename: routesGroupjoinPaidLog,      category: Log4jsConfigs.CATEGORY_GROUPJOIN_PAID},
    { type: 'file', filename: routesClientLogErrorLog,     category: Log4jsConfigs.CATEGORY_ROUTES_CLIENTLOG},
    { type: 'file', filename: socketConnLogFile,           category: Log4jsConfigs.CATEGORY_SOCKET_CONN},
    { type: 'file', filename: thirdpartyapiLogFile,        category: Log4jsConfigs.CATEGORY_THIRDPARTYAPI},
    { type: 'file', filename: razorpayLogFile,             category: Log4jsConfigs.CATEGORY_ROUTES_RAZORPAY},
    { type: 'file', filename: grouppluginLogFile,          category: Log4jsConfigs.CATEGORY_ROUTES_GROUP_PLUGIN},
    { type: 'file', filename: feeLogFile,                  category: Log4jsConfigs.CATEGORY_ROUTES_FEE}
  ]
});

module.exports = log4js;
